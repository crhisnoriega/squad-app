# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/mgumiero9/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
#-verbose
## Retrofit
## Platform calls Class.forName on types which do not exist on Android to determine platform.
#-dontnote retrofit2.Platform
## Platform used when running on Java 8 VMs. Will not be used at runtime.
#-dontwarn retrofit2.Platform$Java8
## Retain generic type information for use by reflection by converters and adapters.
#-keepattributes Signature
## Retain declared checked exceptions for use by a Proxy instance.
#-keepattributes Exceptions
#
## Twilio
#-keep class org.webrtc.** { *; }
#-keep class com.twilio.video.** { *; }
#-keepattributes InnerClasses
#
## RabbitMQ
#-dontwarn javax.naming.directory.*
#-dontwarn javax.naming.*
#-dontwarn javax.security.sasl.*
#-dontwarn javax.security.auth.callback.NameCallback
#-dontwarn com.codahale.metrics.*
#-dontwarn java.beans.*
#-dontwarn org.slf4j.impl.*
#
## Dagger
#-dontwarn com.google.errorprone.annotations.*
#
## OkHttp
#-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#
## SearchView
#-keep class android.support.v7.widget.SearchView { *; }
#
##Branch.io
#-dontwarn com.android.installreferrer.api.**
#-dontwarn io.branch.**
#
##SendBird
#-dontwarn okhttp3.internal.platform.*
#-dontwarn org.conscrypt.*
#
#-dontwarn co.socialsquad.squad.presentation.feature.users.**