package co.socialsquad.squad.domain.model.form

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LearnMore(
    @SerializedName("text")
    val text: String,
    @SerializedName("action_screen")
    val actionScreen: ActionScreen,
) : Parcelable
