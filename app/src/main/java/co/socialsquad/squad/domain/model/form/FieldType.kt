package co.socialsquad.squad.domain.model.form

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val SHORT_TEXT = "short_text"
private const val LONG_TEXT = "long_text"
private const val EMAIL = "email"
private const val PHONE = "phone"
private const val CPF_NAME = "cpf"
private const val RG_NAME = "rg"
private const val CNPJ_NAME = "cnpj"
private const val PIX_NAME = "pix"


sealed class FieldType(val value: String) : Parcelable {

    companion object {
        fun getDefault() = ShortText
        fun getByValue(value: String) = when (value) {
            SHORT_TEXT -> ShortText
            LONG_TEXT -> LongText
            EMAIL -> Email
            PHONE -> Phone
            else -> getDefault()
        }
    }

    @Parcelize
    object ShortText : FieldType(SHORT_TEXT)

    @Parcelize
    object LongText : FieldType(LONG_TEXT)

    @Parcelize
    object Email : FieldType(EMAIL)

    @Parcelize
    object Phone : FieldType(PHONE)

    @Parcelize
    object CPF : FieldType(CPF_NAME)

    @Parcelize
    object CNPJ : FieldType(CNPJ_NAME)

    @Parcelize
    object RG : FieldType(RG_NAME)

    @Parcelize
    object PIX : FieldType(PIX_NAME)
}
