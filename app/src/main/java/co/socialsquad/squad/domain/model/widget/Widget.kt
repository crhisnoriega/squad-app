package co.socialsquad.squad.domain.model.widget

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.WidgetTypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Widget(
    @SerializedName("widget_id")
    val id: Long,
    @JsonAdapter(WidgetTypeAdapter::class)
    @SerializedName("type")
    val type: WidgetType,
    @SerializedName("skippable")
    val skippable: Boolean,
    @SerializedName("submit_button")
    val submitButton: SubmitButton,
    @SerializedName("success")
    val success: Success?
) : Parcelable
