package co.socialsquad.squad.domain.model.widget

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Success(
    @SerializedName("icon")
    val icon: SimpleMedia,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("another_submission_button")
    val anotherSubmissionButton: String?
) : Parcelable
