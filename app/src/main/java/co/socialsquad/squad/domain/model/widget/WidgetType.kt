package co.socialsquad.squad.domain.model.widget

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val FORM = "form"
private const val LEAD = "lead"
private const val SCHEDULER = "scheduler"

sealed class WidgetType(val value: String) : Parcelable {

    companion object {
        fun getDefault() = WidgetForm
        fun getByValue(value: String) = when (value) {
            FORM -> WidgetForm
            LEAD -> WidgetLead
            SCHEDULER -> WidgetScheduler
            else -> getDefault()
        }
    }

    @Parcelize
    object WidgetForm : WidgetType(FORM)

    @Parcelize
    object WidgetLead : WidgetType(LEAD)

    @Parcelize
    object WidgetScheduler : WidgetType(SCHEDULER)
}
