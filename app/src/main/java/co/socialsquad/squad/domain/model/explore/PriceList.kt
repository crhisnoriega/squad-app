package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PriceList(
    @SerializedName("title")
    val title: String,
    @SerializedName("icon")
    val icon: SimpleMedia?,
    @SerializedName("price")
    val price: Float,
    @SerializedName("formatted_price")
    val formatted_price: String
) : Parcelable