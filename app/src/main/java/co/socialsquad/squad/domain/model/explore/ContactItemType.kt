package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val PHONE = "phone"
private const val EMAIL = "email"

sealed class ContactItemType(val value: String) : Parcelable {

    companion object {
        fun getDefault() = Invalid
        fun getByValue(value: String) = when (value) {
            PHONE -> Phone
            EMAIL -> Email
            else -> getDefault()
        }
    }

    @Parcelize
    object Invalid : ContactItemType("")

    @Parcelize
    object Phone : ContactItemType(PHONE)

    @Parcelize
    object Email : ContactItemType(EMAIL)
}