package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val ADDRESS = "address"
private const val LINKS = "links"
private const val RESOURCE = "resource"
private const val PRICE = "price"
private const val CONTACT = "contact"

sealed class ComplementType(val value: String) : Parcelable {

    companion object {
        fun getDefault() = Invalid
        fun getByValue(value: String) = when (value) {
            ADDRESS -> Address
            LINKS -> Links
            RESOURCE -> Resource
            PRICE -> Price
            CONTACT -> Contact
            else -> getDefault()
        }
    }

    @Parcelize
    object Invalid : ComplementType("")

    @Parcelize
    object Links : ComplementType(LINKS)

    @Parcelize
    object Address : ComplementType(ADDRESS)

    @Parcelize
    object Resource : ComplementType(RESOURCE)

    @Parcelize
    object Price : ComplementType(PRICE)

    @Parcelize
    object Contact : ComplementType(CONTACT)
}