package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.lead.Lead
import co.socialsquad.squad.domain.model.schedule.Scheduler
import co.socialsquad.squad.domain.model.widget.Widget
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OpportunityWidget(
    @SerializedName("submission")
    val submission: SubmissionCreation,
    @SerializedName("widget")
    val widget: Widget,
    @SerializedName("lead")
    val lead: Lead?,
    @SerializedName("form")
    val form: ArrayList<Field>?,
    @SerializedName("scheduler")
    val scheduler: Scheduler?
) : Parcelable
