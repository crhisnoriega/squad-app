package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.StageStatusTypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubmissionProgress(
    @JsonAdapter(StageStatusTypeAdapter::class)
    @SerializedName("stage_status") val status: StageStatus?,
    @SerializedName("status_title") val title: String?,
    @SerializedName("stages_count") val total: Int?,
    @SerializedName("current_count") val current: Int?
) : Parcelable
