package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Submission(
        @SerializedName("id") val id: Long?,
        @SerializedName("initials") val initials: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("timestamp") val timestamp: String?,
        @SerializedName("shift") val shift: String?,
        @SerializedName("startdatetime") val startdatetime: Date?,
        @SerializedName("enddatetime") val enddatetime: Date?,
        @SerializedName("progress") val progress: SubmissionProgress?
) : Parcelable
