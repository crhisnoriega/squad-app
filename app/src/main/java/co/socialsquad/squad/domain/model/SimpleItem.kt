package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SimpleItem(
    @SerializedName("title") val title: String,
    @SerializedName("line1") val line1: String,
    @SerializedName("line2") val line2: String
) : Parcelable
