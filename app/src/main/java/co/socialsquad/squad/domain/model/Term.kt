package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Term(
    @SerializedName("item") val item: String,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String
) : Parcelable
