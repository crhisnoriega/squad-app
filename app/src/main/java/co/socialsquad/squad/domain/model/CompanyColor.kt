package co.socialsquad.squad.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompanyColor(
    val primaryColor: String,
    val secondaryColor: String? = null
) : Parcelable
