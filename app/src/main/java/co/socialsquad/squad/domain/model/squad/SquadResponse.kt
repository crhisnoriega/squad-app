package co.socialsquad.squad.domain.model.squad

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList
@Parcelize
data class SquadResponse<T : Parcelable>(
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("error")
    val error: ArrayList<String>?, // error mapping?
    @SerializedName("data")
    val data: T?
) : Parcelable