package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactGroup(
    @SerializedName("title")
    val title: String,
    @SerializedName("items")
    val items: List<ContactItem>
) : Parcelable