package co.socialsquad.squad.domain.model.leadsearch

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class LeadSearchPaginated(
    @SerializedName("previous")
    val previous: String?,
    @SerializedName("next")
    val next: String?,
    @SerializedName("items")
    val items: List<ExistingLeadSeparator>?,
    @SerializedName("empty_state")
    val emptyState: EmptyState?
) : Parcelable
