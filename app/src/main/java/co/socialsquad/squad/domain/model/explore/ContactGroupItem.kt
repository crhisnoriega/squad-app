package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactGroupItem(
    @SerializedName("email")
    val email: ContactGroup?,
    @SerializedName("phone")
    val phone: ContactGroup?,
) : Parcelable