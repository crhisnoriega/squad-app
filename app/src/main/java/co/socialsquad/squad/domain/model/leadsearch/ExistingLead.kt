package co.socialsquad.squad.domain.model.leadsearch

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.ExistingLeadTypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExistingLead(
    @SerializedName("id")
    val id: Long?,
    @JsonAdapter(ExistingLeadTypeAdapter::class)
    @SerializedName("type")
    val type: ExistingLeadType,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String?,
    @SerializedName("avatar")
    val avatar: LeadAvatar,
) : Parcelable
