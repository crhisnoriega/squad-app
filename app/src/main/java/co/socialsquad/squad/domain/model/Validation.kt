package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Validation(
    @SerializedName("header") val header: Header,
    @SerializedName("terms") val terms: List<Term>,
    @SerializedName("button") val button: String
) : Parcelable
