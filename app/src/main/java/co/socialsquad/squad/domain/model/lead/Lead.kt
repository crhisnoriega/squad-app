package co.socialsquad.squad.domain.model.lead

import android.os.Parcelable
import co.socialsquad.squad.domain.model.form.Field
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lead(
    @SerializedName("form")
    val form: ArrayList<Field>,
    @SerializedName("options")
    val options: Options
) : Parcelable
