package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FormLink(
        @SerializedName("enabled") val enabled: Boolean?,
        @SerializedName("text") val text: String?,
) : Parcelable
