package co.socialsquad.squad.domain.model.form

import android.os.Parcelable
import android.text.InputType
import co.socialsquad.squad.data.typeAdapter.FieldTypeAdapter
import co.socialsquad.squad.domain.model.OpportunityHeader
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.TextUtils
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Field(
    @SerializedName("header")
    val header: OpportunityHeader,
    @SerializedName("label")
    val label: String,
    @SerializedName("help_text")
    val helpText: String? = null,
    @SerializedName("name")
    val name: String,
    @SerializedName("validation_error")
    val validationError: String? = null,
    @SerializedName("is_mandatory")
    val isMandatory: Boolean,
    @SerializedName("field_type")
    @JsonAdapter(FieldTypeAdapter::class)
    val fieldType: FieldType,
    @SerializedName("capital_letter")
    val capitalLetter: Boolean,
    @SerializedName("validation_min")
    val validationMin: Int? = null,
    @SerializedName("validation_max")
    val validationMax: Int? = null,
    @SerializedName("additional_information")
    val additionalInformation: AdditionalInformation? = null,

    var wasConfirm: Boolean = false
) : Parcelable {

    fun inputMask(): String? {
        return when (fieldType) {
            FieldType.Phone -> MaskUtils.Mask.MOBILE
            FieldType.CPF -> MaskUtils.Mask.CPF
            FieldType.CNPJ -> MaskUtils.Mask.CNPJ
            FieldType.RG -> MaskUtils.Mask.RG
            else -> null
        }
    }

    fun inputType(): Int {
        return when (fieldType) {
            FieldType.Email -> InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            FieldType.Phone -> InputType.TYPE_CLASS_PHONE
            FieldType.CPF, FieldType.RG, FieldType.CNPJ -> InputType.TYPE_CLASS_NUMBER
            else -> {
                if (capitalLetter) {
                    InputType.TYPE_TEXT_FLAG_CAP_WORDS
                } else {
                    InputType.TYPE_CLASS_TEXT
                }
            }
        }
    }

    fun validInputType(text: String): Boolean {
        return if (text.isNotEmpty() && !text.isBlank()) {
            when (fieldType) {
                FieldType.Email -> TextUtils.isValidEmail(text)
                FieldType.Phone -> TextUtils.isValidPhone(text)
                FieldType.CPF -> TextUtils.isCPF(text)
                FieldType.CNPJ -> CNPJValidator.isCNPJ(text)
                FieldType.ShortText -> TextUtils.validateTwoWord(text)
                FieldType.PIX -> text.isNullOrBlank().not() && text.length <= 32
                else -> true
            }
        } else {
            false
        }
    }

    fun isMinimalInput(text: String): Boolean {
        return if (text.isNotEmpty() && !text.isBlank()) {
            when (fieldType) {
                FieldType.Email -> TextUtils.isMinimalEmail(text)
                FieldType.Phone -> TextUtils.isMinimalPhone(text)
                FieldType.CPF -> TextUtils.isMinimalCPF(text)
                FieldType.CNPJ -> TextUtils.isMinimalCNPJ(text)
                FieldType.RG -> TextUtils.isMinimalRG(text)
                FieldType.ShortText -> {
                    text.trim().split(" ").size >= 2
                }
                else -> true
            }
        } else {
            false
        }
    }
}
