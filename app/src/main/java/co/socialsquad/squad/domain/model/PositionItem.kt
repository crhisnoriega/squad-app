package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PositionItem(
        @SerializedName("id") val id: Long = 0,
        @SerializedName("icon") val icon: SimpleMedia?,
        @SerializedName("title") val title: String,
        @SerializedName("description") val description: String?

) : Parcelable
