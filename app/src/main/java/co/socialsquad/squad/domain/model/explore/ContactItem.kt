package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.ContactItemTypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactItem(
    @SerializedName("title")
    val title: String?,
    @SerializedName("value")
    val value: String,
    @SerializedName("type")
    @JsonAdapter(ContactItemTypeAdapter::class)
    val type: ContactItemType,
) : Parcelable