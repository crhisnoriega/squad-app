package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SummaryTimetable(
    @SerializedName("header") val header: Header,
    @SerializedName("section") val section: ScalaSection,
    @SerializedName("address") val address: SimpleItem,
    @SerializedName("button") val button: String
) : Parcelable
