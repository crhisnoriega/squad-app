package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Location
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ObjectItem
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.Date

@Parcelize
data class Timetable(
        @SerializedName("id")
        val id: Long?,
        @SerializedName("title")
        val title: String?,
        @SerializedName("position")
        val position: String?,

        @SerializedName("custom_fields")
        val custom_fields: String?,

        @SerializedName("shift")
        val shift: String?,

        @SerializedName("datetime_start")
        val datetime_start: Date?,

        @SerializedName("datetime_end")
        val datetime_end: Date?,

        @SerializedName("startdatetime")
        val startDatetime: Date?,
        @SerializedName("enddatetime")
        val endDatetime: Date?,
        @SerializedName("icon")
        val icon: SimpleMedia?,
        @SerializedName("progress") val progress: SubmissionProgress?,

        @SerializedName("initials") val initials: String?,
        @SerializedName("timestamp") val timestamp: String?,
        @SerializedName("link") val link: Link?,



        @SerializedName("exploreobject") val exploreobject: ObjectItem?

) : Parcelable