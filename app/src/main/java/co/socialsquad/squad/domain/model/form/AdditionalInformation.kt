package co.socialsquad.squad.domain.model.form

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AdditionalInformation(
    @SerializedName("supplementary_text")
    val supplementaryText: String,
    @SerializedName("featured_text")
    val featuredText: String?,
    @SerializedName("learn_more")
    val learnMore: LearnMore?
) : Parcelable
