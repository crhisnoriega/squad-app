package co.socialsquad.squad.domain.model.leadsearch

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val SEPARATOR = "separator"
private const val ITEM = "item"

sealed class ExistingLeadType(val value: String) : Parcelable {

    companion object {
        fun getByValue(value: String) = when (value) {
            SEPARATOR -> Separator
            ITEM -> Item
            else -> Invalid
        }
    }

    @Parcelize
    object Invalid : ExistingLeadType("")

    @Parcelize
    object Separator : ExistingLeadType(SEPARATOR)

    @Parcelize
    object Item : ExistingLeadType(ITEM)
}
