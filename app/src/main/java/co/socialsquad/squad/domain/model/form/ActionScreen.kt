package co.socialsquad.squad.domain.model.form

import android.os.Parcelable
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ActionScreen(
    @SerializedName("icon")
    val icon: SimpleMedia,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("action_button")
    val actionButton: Button
) : Parcelable
