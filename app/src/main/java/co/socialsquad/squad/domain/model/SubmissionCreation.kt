package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.StageStatusTypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubmissionCreation(
    @SerializedName("id")
    val id: Long,
    @JsonAdapter(StageStatusTypeAdapter::class)
    @SerializedName("status")
    val status: StageStatus,
    @SerializedName("stage")
    val stage: Long,
    @SerializedName("opportunity")
    val opportunityId: Long,
    @SerializedName("object", alternate = ["exploreobject"])
    val objectId: Long
) : Parcelable
