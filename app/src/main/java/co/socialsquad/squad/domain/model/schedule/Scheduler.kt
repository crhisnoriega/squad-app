package co.socialsquad.squad.domain.model.schedule

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreObject
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Scheduler(
    // @SerializedName("invitees")
    // val invitees: List<Invitee>,
    @SerializedName("object")
    val exploreObject: ExploreObject
) : Parcelable
