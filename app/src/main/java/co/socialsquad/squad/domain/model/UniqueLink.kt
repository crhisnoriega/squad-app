package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UniqueLink(
    @SerializedName("enabled") val enabled: Boolean? = true,
    @SerializedName("text") val text: String? = null,
    @SerializedName("url") var url: String? = null,
    @SerializedName("picture") val picture: String? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("description") val description: String? = null,
) : Parcelable
