package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.Date

@Parcelize
data class ScaleTime(
    @SerializedName("date") var date: Date,
    @SerializedName("formatted_date") val formattedDate: String,
    @SerializedName("shifts") val shifts: List<ScaleShift>
) : Parcelable
