package co.socialsquad.squad.domain.model.widget

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubmitButton(
    @SerializedName("text")
    val text: String
) : Parcelable
