package co.socialsquad.squad.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val COMPLETED = "completed"
private const val BLOCKED = "blocked"
private const val CLOSED = "closed"

sealed class StageStatus(val value: String) : Parcelable {

    companion object {
        fun getDefault() = Blocked
        fun getByValue(value: String) = when (value) {
            COMPLETED -> Completed
            BLOCKED -> Blocked
            CLOSED -> Closed
            else -> getDefault()
        }
    }

    @Parcelize
    object Completed : StageStatus(COMPLETED)

    @Parcelize
    object Blocked : StageStatus(BLOCKED)

    @Parcelize
    object Closed : StageStatus(CLOSED)
}
