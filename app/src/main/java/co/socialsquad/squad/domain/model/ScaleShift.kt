package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.Date

@Parcelize
data class ScaleShift(
        @SerializedName("id") val id: Long = 0,
        @SerializedName("startdatetime_utc") val startDate: Date,
        @SerializedName("enddatetime_utc") val endDate: Date,
        @SerializedName("available") val available: Boolean,
        @SerializedName("title") val title: String,
        @SerializedName("subtitle") val subtitle: String,
        @SerializedName("position") val positionItem: PositionItem?,
) : Parcelable
