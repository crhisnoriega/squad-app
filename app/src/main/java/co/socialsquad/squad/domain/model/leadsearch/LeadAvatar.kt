package co.socialsquad.squad.domain.model.leadsearch

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LeadAvatar(
    @SerializedName("picture")
    val picture: String?,
    @SerializedName("initials")
    val initials: String,
) : Parcelable
