package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreObject
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScalaSection(
    @SerializedName("title") val title: String,
    @SerializedName("template") @JsonAdapter(SectionTemplateTypeAdapter::class) val template: SectionTemplate,
    @SerializedName("icon") val icon: SimpleMedia,
    @SerializedName("object") val item: ExploreObject
) : Parcelable
