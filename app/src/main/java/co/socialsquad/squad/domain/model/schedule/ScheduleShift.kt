package co.socialsquad.squad.domain.model.schedule

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleShift(
    @SerializedName("max_month") val maxMonth: Int,
    @SerializedName("max_day") val maxDay: Int
) : Parcelable
