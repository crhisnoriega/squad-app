package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Opportunity<T : Parcelable>(
    @SerializedName("header") val header: OpportunityHeader,
    @SerializedName("show_all") val button: Button?,
    @SerializedName("previous") val previous: String?,
    @SerializedName("next") val next: String?,
    @SerializedName("items") val items: List<T>?,
    @SerializedName("empty_state") val emptyState: EmptyState?
) : Parcelable
