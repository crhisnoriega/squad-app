package co.socialsquad.squad.domain.model.leadsearch

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExistingLeadSeparator(
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String?,
    @SerializedName("items")
    val items: List<ExistingLead>?,
) : Parcelable
