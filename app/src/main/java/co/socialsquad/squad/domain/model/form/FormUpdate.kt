package co.socialsquad.squad.domain.model.form

import com.google.gson.annotations.SerializedName

data class FormUpdate(
    @SerializedName("data")
    val data: HashMap<String, Any>?,
)
