package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OpportunityWidgetResponse(
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("error")
    val error: List<String>, // error mapping?
    @SerializedName("data")
    val data: OpportunityWidget
) : Parcelable
