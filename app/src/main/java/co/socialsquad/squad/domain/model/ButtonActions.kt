package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ButtonActions(
        @SerializedName("link") val link: UniqueLink?,
        @SerializedName("form") val form: FormLink?
) : Parcelable
