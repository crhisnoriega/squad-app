package co.socialsquad.squad.domain.model.explore

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Price(
    @SerializedName("title")
    val title: String,
    @SerializedName("price")
    val price: Float,
    @SerializedName("formatted_price")
    val formattedPrice: String,
    @SerializedName("lists")
    val lists: List<PriceList>,
    @SerializedName("price_count")
    val priceCount: String
) : Parcelable