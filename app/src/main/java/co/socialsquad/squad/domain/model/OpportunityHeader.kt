package co.socialsquad.squad.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OpportunityHeader(
    @SerializedName("icon")
    val icon: SimpleMedia?,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String
) : Parcelable
