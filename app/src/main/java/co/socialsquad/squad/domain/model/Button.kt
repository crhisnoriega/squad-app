package co.socialsquad.squad.domain.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionIcon
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Button(
        @SerializedName("text") val text: String?,
        @SerializedName("icon") val icon: SectionIcon?,
        @SerializedName("link", alternate = ["url"]) val link: String?,
        @SerializedName("actions") val actions: ButtonActions? = null
) : Parcelable
