package co.socialsquad.squad.domain.model.lead

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Options(
    @SerializedName("icon")
    val icon: SimpleMedia,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("help_text")
    val helpText: String,
    @SerializedName("new_lead_title")
    val newLeadTitle: String,
    @SerializedName("new_lead_subtitle")
    val newLeadSubtitle: String,
    @SerializedName("existing_lead_title")
    val existingLeadTitle: String,
    @SerializedName("existing_lead_subtitle")
    val existingLeadSubtitle: String
) : Parcelable
