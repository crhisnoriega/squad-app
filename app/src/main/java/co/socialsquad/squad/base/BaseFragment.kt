package co.socialsquad.squad.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.presentation.feature.base.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.core.module.Module

abstract class BaseFragment : Fragment() {

    abstract val modules: List<Module>
    abstract val contentView: Int

    override fun onAttach(context: Context) {
        loadKoinModules(modules)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(contentView, container, false)

    override fun onDetach() {
        unloadKoinModules(modules)
        super.onDetach()
    }

    @ExperimentalCoroutinesApi
    fun <T> createFlow(flow: StateFlow<Resource<T>>) = ResourceStatusFlow(lifecycleScope, flow)
}
