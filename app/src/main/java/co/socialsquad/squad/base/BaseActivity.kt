package co.socialsquad.squad.base

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.presentation.feature.base.Resource
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.core.module.Module

abstract class BaseActivity : AppCompatActivity() {

    abstract val modules: List<Module>
    abstract val contentView: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        loadKoinModules(modules)
        super.onCreate(savedInstanceState)
        setContentView(contentView)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onDestroy() {
        unloadKoinModules(modules)
        super.onDestroy()
    }

    @ExperimentalCoroutinesApi
    fun <T> createFlow(flow: StateFlow<Resource<T>>) = ResourceStatusFlow(lifecycleScope, flow)
}
