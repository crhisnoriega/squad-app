package co.socialsquad.squad.base

import android.util.Log
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.base.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ResourceStatusFlow<T> @ExperimentalCoroutinesApi constructor(
    private val scope: CoroutineScope,
    private val flow: StateFlow<Resource<T>>
) {

    private var success: ((T?) -> Unit)? = null
    private var loading: ((T?) -> Unit)? = null
    private var empty: ((T?) -> Unit)? = null
    private var error: ((String?) -> Unit)? = null

    fun success(block: (T?) -> Unit) = apply {
        success = block
    }

    fun loading(block: (T?) -> Unit) = apply {
        loading = block
    }

    fun empty(block: (T?) -> Unit) = apply {
        empty = block
    }

    fun error(block: (String?) -> Unit) = apply {
        error = block
    }

    fun launch() {
        scope.launch {
            flow.collect {
                Log.i("timetable", "state: ${it.status}")
                when (it.status) {
                    Status.SUCCESS -> {
                        success?.invoke(it.data)
                    }
                    Status.LOADING -> {
                        loading?.invoke(it.data)
                    }
                    Status.EMPTY, Status.EMPTY_WITH_DATA -> {
                        empty?.invoke(it.data)
                    }
                    Status.ERROR -> {
                        error?.invoke(it.message)
                    }
                }
            }
        }
    }
}
