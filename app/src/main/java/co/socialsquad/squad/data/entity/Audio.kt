package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AudioList(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("next") var next: String? = null,
    @SerializedName("previous") var previous: String? = null,
    @SerializedName("results") var results: List<Audio>
)

data class Audio(
    @SerializedName("pk") val pk: Int = 0,
    @SerializedName("name") val name: String,
    @SerializedName("content") val content: String? = null,
    @SerializedName("scheduled") var scheduled: AudioSchedule? = null,
    @SerializedName("media") var media: Media?,
    @SerializedName("owner") var owner: Owner,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("audience") var audience: String,
    @SerializedName("audience_type") var audienceType: String,
    @SerializedName("can_delete") var canDelete: Boolean = false,
    @SerializedName("can_edit") var canEdit: Boolean = false,
    @SerializedName("can_share") var canShare: Boolean = false,
    @SerializedName("can_be_recommended") var canRecommend: Boolean = false,
    @SerializedName("liked") var liked: Boolean = false,
    @SerializedName("liked_users_avatars") var likedUsersAvatars: List<String>? = null,
    @SerializedName("likes_count") var likesCount: Int = 0,
    @SerializedName("total_listenings") var totalListenings: Long = 0,
    @SerializedName("listened") var listened: Boolean = false
) : Serializable

data class AudioSchedule(
    @SerializedName("pk") val pk: Int,
    @SerializedName("start_time") val startTime: String
) : Serializable
