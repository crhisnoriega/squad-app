package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Video(
    @SerializedName("file")
    val `file`: String,
    @SerializedName("height")
    val height: Int,
    @SerializedName("mime_type")
    val mimeType: String,
    @SerializedName("ord")
    val ord: Int,
    @SerializedName("pk")
    val pk: Int,
    @SerializedName("progressive")
    val progressive: Boolean,
    @SerializedName("resized_url")
    val resizedUrl: Any,
    @SerializedName("streamings")
    val streamings: List<Any>,
    @SerializedName("time_length")
    val timeLength: Int,
    @SerializedName("url")
    val url: String,
    @SerializedName("views_count")
    val viewsCount: Int,
    @SerializedName("width")
    val width: Int
)
