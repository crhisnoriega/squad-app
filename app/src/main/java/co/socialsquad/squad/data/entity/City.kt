package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class City(
    @SerializedName("code") var code: Long,
    @SerializedName("name") var name: String? = null
)
