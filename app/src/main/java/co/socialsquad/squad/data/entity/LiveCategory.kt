package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LiveCategory(
    @SerializedName("pk") var pk: Long = 0,
    @SerializedName("name") var name: String = ""
) : Serializable
