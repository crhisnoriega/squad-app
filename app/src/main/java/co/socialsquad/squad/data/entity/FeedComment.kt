package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedComment(
    @SerializedName("pk") var pk: Long,
    @SerializedName("text") var text: String?,
    @SerializedName("created_at") var createdAt: String?,
    @SerializedName("updated_at") var updatedAt: String?,
    @SerializedName("can_delete") var canDelete: Boolean = false,
    @SerializedName("can_edit") var canEdit: Boolean = false,
    @SerializedName("user_company") var userCompany: UserCompany?
)
