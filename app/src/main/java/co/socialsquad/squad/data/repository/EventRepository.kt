package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.Event
import co.socialsquad.squad.data.entity.Results
import io.reactivex.Observable

interface EventRepository {
    fun getEvents(page: Int): Observable<Results<Event>>
    fun getEvent(pk: Long): Observable<Event>
    fun confirmAttendance(pk: Long): Observable<Event>
}
