package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.explore.ComplementType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class ComplementTypeAdapter : TypeAdapter<ComplementType>() {
    override fun write(writer: JsonWriter?, value: ComplementType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(ComplementType.getDefault().value)
    }

    override fun read(reader: JsonReader?): ComplementType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> ComplementType.getByValue(it.nextString())
                else -> ComplementType.getDefault()
            }
        } ?: ComplementType.getDefault()
    }
}
