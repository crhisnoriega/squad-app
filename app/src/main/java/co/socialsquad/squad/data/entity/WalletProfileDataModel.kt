package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class WalletProfileDataModel(
        @SerializedName("wallet_title") val wallet_title: String?,
        @SerializedName("wallet_subtitle") val wallet_subtitle: String?,
        @SerializedName("revenue_title") val revenue_title: String?,
        @SerializedName("revenue_subtitle") val revenue_subtitle: String?

        )
