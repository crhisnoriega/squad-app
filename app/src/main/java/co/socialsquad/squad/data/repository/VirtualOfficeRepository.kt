package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.VirtualOfficeResponse
import io.reactivex.Observable

interface VirtualOfficeRepository {
    fun getTools(): Observable<VirtualOfficeResponse>
}
