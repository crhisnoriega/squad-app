package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class UnreadNotificationCount(
    @SerializedName("not_visualized_notifications") var notificationsUnreadCount: Int = 0,
    @SerializedName("not_listened_audios") var audioUnheardCount: Int = 0
)
