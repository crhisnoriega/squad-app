package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.FindProductRequest
import co.socialsquad.squad.data.entity.ProductsFilterRequest
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class StoreRepositoryImpl @Inject constructor(private val api: Api) : StoreRepository {
    override fun getStoreHome() =
        api.getStoreHome().onDefaultSchedulers()

    override fun getProduct(pk: Int) =
        api.getStoreProduct(pk).onDefaultSchedulers()

    override fun getSubcategories(pk: Int, page: Int) =
        api.getStoreProductsCategoryList(pk, true, page).onDefaultSchedulers()

    override fun getList(pk: Int, page: Int) =
        api.getStoreList(pk, page).onDefaultSchedulers()

    override fun getProducts(page: Int) =
        api.getStoreProducts(page).onDefaultSchedulers()

    override fun getProducts(productsFilterRequest: ProductsFilterRequest, page: Int) =
        api.postStoreProductsFilter(productsFilterRequest, page).onDefaultSchedulers()

    override fun getProductDetailCategories(pk: Int) =
        api.getStoreGroupedDetails(pk).onDefaultSchedulers()

    override fun getProductLocations(findProductRequest: FindProductRequest) =
        api.postStoreFindProduct(findProductRequest).onDefaultSchedulers()

    override fun getProductView(pk: Int) =
        api.getStoreProductsView(pk).onDefaultSchedulers()

    override fun getStores(lat: Double, lng: Double, page: Int) =
        api.getStores(lat, lng, page).onDefaultSchedulers()
}
