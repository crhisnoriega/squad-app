package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.AboutResponse
import co.socialsquad.squad.data.entity.AuthenticationRequest
import co.socialsquad.squad.data.entity.CallResumeTokenRequest
import co.socialsquad.squad.data.entity.CallResumeTokenResponse
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.ConfirmResetPasswordRequest
import co.socialsquad.squad.data.entity.EditPasswordRequest
import co.socialsquad.squad.data.entity.FindSubdomainRequest
import co.socialsquad.squad.data.entity.MagicLinkRequest
import co.socialsquad.squad.data.entity.MagicLinkResponse
import co.socialsquad.squad.data.entity.MagicLinkResumeRequest
import co.socialsquad.squad.data.entity.PasswordResetRequest
import co.socialsquad.squad.data.entity.RecipientsRequest
import co.socialsquad.squad.data.entity.RegisterUserRequest
import co.socialsquad.squad.data.entity.SigninRequest
import co.socialsquad.squad.data.entity.SubdomainsRequest
import co.socialsquad.squad.data.entity.TokenVerifyRequest
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.VerifyResetPasswordTokenRequest
import co.socialsquad.squad.data.entity.VerifyResetPasswordTokenResponse
import io.reactivex.Completable
import io.reactivex.Observable

interface LoginRepository {
    var subdomain: String?
    var squadName: String?
    var userCompany: UserCompany?
    var token: String?
    var squadColor: String?
    fun getDeviceToken(): Observable<String>?
    fun authenticate(authenticationRequest: AuthenticationRequest): Observable<UserCompany>
    fun resetPassword(passwordResetRequest: PasswordResetRequest, subdomain: String): Completable
    fun magicLink(magicLinkRequest: MagicLinkRequest): Observable<MagicLinkResponse>
    fun verifyMagicLinkToken(magicLinkResumeRequest: MagicLinkResumeRequest): Observable<List<Company>>
    fun verifyToken(tokenVerifyRequest: TokenVerifyRequest): Completable

    fun registerDevice(recipientsRequest: RecipientsRequest): Completable
    fun editPassword(editPasswordRequest: EditPasswordRequest): Completable
    fun clearSharingAppDialogTimer()
    fun logout(): Completable
    fun findSubdomain(findSubdomainRequest: FindSubdomainRequest): Observable<Company>
    fun signin(signinRequest: SigninRequest): Observable<UserCompany>
    fun authVerifyToken(tokenVerifyRequest: TokenVerifyRequest, subdomain: String?): Observable<UserCompany>
    fun authRefreshToken(tokenVerifyRequest: TokenVerifyRequest, subdomain: String?): Observable<UserCompany>
    fun register(body: RegisterUserRequest): Observable<UserCompany>
    fun callResume(body: CallResumeTokenRequest): Observable<CallResumeTokenResponse>
    fun getRegisteredCompanies(token: String): Observable<List<Company>>
    fun getSubdomains(subdomainsRequest: SubdomainsRequest): Observable<List<Company>>
    fun verifyResetPasswordToken(body: VerifyResetPasswordTokenRequest, subdomain: String): Observable<VerifyResetPasswordTokenResponse>
    fun confirmResetPassword(body: ConfirmResetPasswordRequest, subdomain: String): Observable<VerifyResetPasswordTokenResponse>
    fun about(subdomain: String): Observable<AboutResponse>
    fun aboutNoHeader(): Observable<AboutResponse>
}
