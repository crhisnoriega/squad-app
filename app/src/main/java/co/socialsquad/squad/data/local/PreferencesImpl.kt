package co.socialsquad.squad.data.local

import android.content.SharedPreferences
import java.util.*
import javax.inject.Inject

private const val SQUAD_NAME = "SQUAD_NAME"
private const val TOKEN = "TOKEN"
private const val SUBDOMAIN = "SUBDOMAIN"
private const val AUDIO_ENABLED = "AUDIO_ENABLED"
private const val USER_COMPANY = "USER_COMPANY"
private const val SQUAD_COLOR = "SQUAD_COLOR"
private const val SIGNUP_AVATAR = "SIGNUP_AVATAR"
private const val SIGNUP_EMAIL = "SIGNUP_EMAIL"
private const val MEDIA_URI = "MEDIA_URI"
private const val LIVE_PK = "LIVE_PK"
private const val SCHEDULE_IMAGE_MEDIA_URI = "SCHEDULE_IMAGE_MEDIA_URI"
private const val SCHEDULE_VIDEO_MEDIA_URI = "SCHEDULE_VIDEO_MEDIA_URI"
private const val SHARE_MEDIA_PK = "SHARE_MEDIA_PK"
private const val SHARE_MEDIA_MODEL = "SHARE_MEDIA_MODEL"
private const val AUDIO_URI = "AUDIO_AUDIO"
private const val INTERACTION_DIALOG_TIME = "INTERACTION_DIALOG_TIME"
private const val EXTERNAL_SYSTEM_USER = "EXTERNAL_SYSTEM_USER"
private const val EXTERNAL_SYSTEM_PASSWORD = "EXTERNAL_SYSTEM_PASSWORD"
private const val EXTERNAL_SYSTEM_PASSWORD_IV = "EXTERNAL_SYSTEM_PASSWORD_IV"
private const val AUDIO_LIST = "AUDIO_LIST"
private const val VIDEO_LIST = "VIDEO_LIST"
private const val EXPLORE = "EXPLORE"

class PreferencesImpl @Inject constructor(private val sharedPreferences: SharedPreferences) :
    Preferences {

    override var squadName: String?
        get() = sharedPreferences.getString(SQUAD_NAME, null)
        set(value) = sharedPreferences.edit().putString(SQUAD_NAME, value).apply()

    override var token: String?
        get() = sharedPreferences.getString(TOKEN, null)
        set(value) = sharedPreferences.edit().putString(TOKEN, value).apply()

    override var subdomain: String?
        get() = sharedPreferences.getString(SUBDOMAIN, null)
        set(value) = sharedPreferences.edit().putString(SUBDOMAIN, value).apply()

    override var audioEnabled: Boolean
        get() = sharedPreferences.getBoolean(AUDIO_ENABLED, true)
        set(value) = sharedPreferences.edit().putBoolean(AUDIO_ENABLED, value).apply()

    override var userCompany: String?
        get() = sharedPreferences.getString(USER_COMPANY, null)
        set(value) = sharedPreferences.edit().putString(USER_COMPANY, value).apply()

    override var squadColor: String?
        get() = sharedPreferences.getString(SQUAD_COLOR, null)
        set(value) = sharedPreferences.edit().putString(SQUAD_COLOR, value).apply()

    override var signupAvatar: String?
        get() = sharedPreferences.getString(SIGNUP_AVATAR, null)
        set(value) = sharedPreferences.edit().putString(SIGNUP_AVATAR, value).apply()

    override var signupEmail: String?
        get() = sharedPreferences.getString(SIGNUP_EMAIL, null)
        set(value) = sharedPreferences.edit().putString(SIGNUP_EMAIL, value).apply()

    override var mediaUri: String?
        get() = sharedPreferences.getString(MEDIA_URI, null)
        set(value) = sharedPreferences.edit().putString(MEDIA_URI, value).apply()

    override var language = "en"
        get() = Locale.getDefault().language

    override var livePk: Int
        get() = sharedPreferences.getInt(LIVE_PK, 0)
        set(value) = sharedPreferences.edit().putInt(LIVE_PK, value).apply()

    override var scheduleImageMedia: String?
        get() = sharedPreferences.getString(SCHEDULE_IMAGE_MEDIA_URI, null)
        set(value) = sharedPreferences.edit().putString(SCHEDULE_IMAGE_MEDIA_URI, value).apply()

    override var scheduleVideoMedia: String?
        get() = sharedPreferences.getString(SCHEDULE_VIDEO_MEDIA_URI, null)
        set(value) = sharedPreferences.edit().putString(SCHEDULE_VIDEO_MEDIA_URI, value).apply()

    override var sharePK: Int
        get() = sharedPreferences.getInt(SHARE_MEDIA_PK, -1)
        set(value) = sharedPreferences.edit().putInt(SHARE_MEDIA_PK, value).apply()

    override var shareModel: String?
        get() = sharedPreferences.getString(SHARE_MEDIA_MODEL, null)
        set(value) = sharedPreferences.edit().putString(SHARE_MEDIA_MODEL, value).apply()

    override var audioURI: String?
        get() = sharedPreferences.getString(AUDIO_URI, null)
        set(value) = sharedPreferences.edit().putString(AUDIO_URI, value).apply()

    override var interactionDialogTimeInMillis: Long
        get() = sharedPreferences.getLong(INTERACTION_DIALOG_TIME, 0)
        set(value) = sharedPreferences.edit().putLong(INTERACTION_DIALOG_TIME, value).apply()

    override var externalSystemPassword: String?
        get() = sharedPreferences.getString(EXTERNAL_SYSTEM_PASSWORD, null)
        set(value) = sharedPreferences.edit().putString(EXTERNAL_SYSTEM_PASSWORD, value).apply()

    override var externalSystemPasswordIV: ByteArray?
        get() {
            val preferencesString = sharedPreferences.getString(EXTERNAL_SYSTEM_PASSWORD_IV, null)
            return preferencesString?.let {
                val split = it.substring(1, it.length - 1).split(", ")
                val array = ByteArray(split.size)
                for (i in split.indices) {
                    array[i] = java.lang.Byte.parseByte(split[i])
                }
                array
            }
        }
        set(value) = sharedPreferences.edit()
            .putString(EXTERNAL_SYSTEM_PASSWORD_IV, value?.let { Arrays.toString(it) }).apply()

    override var audioList: String?
        get() = sharedPreferences.getString(AUDIO_LIST, null)
        set(value) = sharedPreferences.edit().putString(AUDIO_LIST, value).apply()

    override var videoList: String?
        get() = sharedPreferences.getString(VIDEO_LIST, null)
        set(value) = sharedPreferences.edit().putString(VIDEO_LIST, value).apply()

    override var sections: String?
        get() = sharedPreferences.getString(EXPLORE, null)
        set(value) = sharedPreferences.edit().putString(EXPLORE, value).apply()

    override fun cleanSections() {
        sharedPreferences.edit().remove(EXPLORE).apply()
    }
}
