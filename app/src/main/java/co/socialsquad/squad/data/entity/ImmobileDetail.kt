package co.socialsquad.squad.data.entity

data class ImmobileDetail(
    val pk: Int,
    val name: String,
    val content: String,
    val media: Media,
    val details: List<Detail>
) {
    data class Detail(val title: String, val description: String) {
        data class Media(val pk: Int, val mime_type: String, val url: String)
    }
}
