package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Trip(
    @SerializedName("pk") val pk: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("promotion_text") val promotionText: String? = null,
    @SerializedName("content") val content: String? = null,
    @SerializedName("cover") val cover: Media? = null,
    @SerializedName("created_at") val created_at: String? = null,
    @SerializedName("updated_at") val updated_at: String? = null
)
