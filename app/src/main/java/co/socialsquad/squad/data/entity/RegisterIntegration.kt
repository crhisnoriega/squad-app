package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RegisterIntegration(
    @SerializedName("token") val token: String,
    @SerializedName("user_company") val userCompany: UserCompany
)
