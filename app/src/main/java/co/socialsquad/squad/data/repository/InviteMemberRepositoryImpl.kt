package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.InviteMembersRequest
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class InviteMemberRepositoryImpl @Inject constructor(
    private val api: Api
) :
    InviteMemberRepository {

    override fun inviteMembers(body: List<InviteMembersRequest>) = api.inviteMembers(body).onDefaultSchedulers()
}
