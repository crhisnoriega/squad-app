package co.socialsquad.squad.data

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.database.SquadDataBase
import co.socialsquad.squad.data.gateway.ChatGateway
import co.socialsquad.squad.data.gateway.EjabberdChat
import co.socialsquad.squad.data.local.LocalStorage
import co.socialsquad.squad.data.local.LocalStorageImpl
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.local.PreferencesImpl
import co.socialsquad.squad.data.remote.*
import co.socialsquad.squad.data.repository.*
import co.socialsquad.squad.data.service.FirebaseMessagingService
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.data.typeAdapter.SectionTypeAdapter
import co.socialsquad.squad.data.utils.DateDeserializer
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import co.socialsquad.squad.presentation.feature.squad.SquadRepository
import co.socialsquad.squad.presentation.feature.squad.SquadRepositoryImpl
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rabbitmq.client.ConnectionFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

private const val TIMEOUT = 90
private const val WRITE_TIMEOUT = 180

@Module(includes = [DataModule.Providers::class])
@Suppress("unused")
interface DataModule {

    @ContributesAndroidInjector
    fun firebaseMessagingService(): FirebaseMessagingService

    @Binds
    @Singleton
    fun preferences(impl: PreferencesImpl): Preferences

    @Binds
    @Singleton
    fun localStorage(impl: LocalStorageImpl): LocalStorage

    @Binds
    @Singleton
    fun loginRepository(impl: LoginRepositoryImpl): LoginRepository

    @Binds
    @Singleton
    fun socialRepository(impl: SocialRepositoryImpl): SocialRepository

    @Binds
    @Singleton
    fun liveRepository(impl: LiveRepositoryImpl): LiveRepository

    @Binds
    @Singleton
    fun inviteMembersRepository(impl: InviteMemberRepositoryImpl): InviteMemberRepository

    @Binds
    @Singleton
    fun profileRepository(impl: ProfileRepositoryImpl): ProfileRepository

    @Binds
    @Singleton
    fun storeRepository(impl: StoreRepositoryImpl): StoreRepository

    @Binds
    @Singleton
    fun signupRepository(impl: SignupRepositoryImpl): SignupRepository

    @Binds
    @Singleton
    fun segmentRepository(impl: SegmentRepositoryImpl): SegmentRepository

    @Binds
    @Singleton
    fun platformRepository(impl: PlatformRepositoryImpl): PlatformRepository

    @Binds
    @Singleton
    fun squadRepository(impl: SquadRepositoryImpl): SquadRepository

    @Binds
    @Singleton
    fun audioRepository(impl: AudioRepositoryImpl): AudioRepository

    @Binds
    @Singleton
    fun dashboardRepository(impl: DashboardRepositoryImpl): DashboardRepository

    @Binds
    @Singleton
    fun videoRepository(impl: VideoRepositoryImpl): VideoRepository

    @Binds
    @Singleton
    fun eventRepository(impl: EventRepositoryImpl): EventRepository

    @Binds
    @Singleton
    fun documentRepository(impl: DocumentRepositoryImpl): DocumentRepository

    @Binds
    @Singleton
    fun chatRepository(impl: ChatRepositoryImpl): ChatRepository

    @Binds
    @Singleton
    fun searchRepository(impl: SearchRepositoryImpl): SearchRepository

    @Binds
    @Singleton
    fun tripRepository(impl: TripRepositoryImpl): TripRepository

    @Binds
    @Singleton
    fun callRepository(callRepositoryImpl: CallRepositoryImpl): CallRepository

    @Binds
    @Singleton
    fun feedbackRepository(impl: FeedbackRepositoryImpl): FeedbackRepository

    @Binds
    @Singleton
    fun virtualOfficeRepository(impl: VirtualOfficeRepositoryImpl): VirtualOfficeRepository

    @Binds
    @Singleton
    fun postPasswordRepository(impl: TermsRepositoryImpl): TermsRepository

    @Binds
    @Singleton
    fun immobileRepository(impl: ImmobilesRepositoryImpl): ImmobilesRepository

    @Binds
    @Singleton
    fun opportunityRepository(impl: OpportunityRepositoryImpl): OpportunityRepository

    @Module
    class Providers {

        @Provides
        @Singleton
        fun api(gson: Gson, okHttpClient: OkHttpClient): Api = Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(Api::class.java)

        @Provides
        @Singleton
        fun apiIntegration(gson: Gson, okHttpClient: OkHttpClient): ApiIntegration =
                Retrofit.Builder()
                        .baseUrl(BuildConfig.API_URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
                        .create(ApiIntegration::class.java)

        @Provides
        @Singleton
        fun apiUpload(
                gson: Gson,
                @Named("timeoutlessOkHttpClient") okHttpClient: OkHttpClient
        ): ApiUpload =
                Retrofit.Builder()
                        .baseUrl(BuildConfig.API_URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
                        .create(ApiUpload::class.java)

        @Provides
        @Singleton
        fun gson(): Gson = GsonBuilder()
                .registerTypeAdapter(Date::class.java, DateDeserializer())
                .registerTypeAdapter(SectionTemplate::class.java, SectionTemplateTypeAdapter())
                .registerTypeAdapter(SectionType::class.java, SectionTypeAdapter())
                .create()

        @Provides
        fun okHttpClient(
                httpLoggingInterceptor: HttpLoggingInterceptor,
                authenticationInterceptor: AuthenticationInterceptor,
                crashlyticsInterceptor: CrashlyticsInterceptor,
                cacheInterceptor: CacheInterceptor,
                cache: Cache
        ): OkHttpClient = with(OkHttpClient.Builder()) {
            followRedirects(true)
            followSslRedirects(true)
            connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            addInterceptor(authenticationInterceptor)
            addInterceptor(httpLoggingInterceptor)
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(StethoInterceptor())
            }
            addInterceptor(crashlyticsInterceptor)
            addNetworkInterceptor(cacheInterceptor)
            cache(null)
            build()
        }

        @Named("timeoutlessOkHttpClient")
        @Provides
        fun timeoutlessOkHttpClient(
                httpLoggingInterceptor: HttpLoggingInterceptor,
                authenticationInterceptor: AuthenticationInterceptor,
        ): OkHttpClient = with(OkHttpClient.Builder()) {
            connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            readTimeout(0, TimeUnit.SECONDS)
            writeTimeout(0, TimeUnit.SECONDS)
            addInterceptor(authenticationInterceptor)
            addInterceptor(httpLoggingInterceptor)
            if (BuildConfig.DEBUG) {
                addNetworkInterceptor(StethoInterceptor())
            }

            build()
        }

        @Provides
        fun httpLoggingInterceptor(): HttpLoggingInterceptor {
            val level = BODY
            return HttpLoggingInterceptor().setLevel(level)
        }

        @Provides
        fun authenticationInterceptor(preferences: Preferences) =
                AuthenticationInterceptor(preferences)

        @Provides
        fun crashlyticsInterceptor() =
                CrashlyticsInterceptor()

        @Provides
        fun cacheInterceptor(context: Context) = CacheInterceptor(context)

        @Provides
        fun cache(context: Context) = Cache(context.cacheDir, 10 * 1024 * 1024L) // 10MB Cache

        @Provides
        @Singleton
        fun connectionFactory() = ConnectionFactory().apply {
            isAutomaticRecoveryEnabled = true
            username = BuildConfig.RABBITMQ_USER
            password = BuildConfig.RABBITMQ_PASSWORD
            host = BuildConfig.RABBITMQ_HOST
            virtualHost = BuildConfig.RABBITMQ_VIRTUAL_HOST
            port = BuildConfig.RABBITMQ_PORT
        }

        @Provides
        fun sharedPreferences(context: Context): SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context)

        @Provides
        @Singleton
        fun contentResolver(context: Context): ContentResolver = context.contentResolver

        @Provides
        @Singleton
        fun getDatabase(context: Context) =
                Room.databaseBuilder(
                        context.applicationContext,
                        SquadDataBase::class.java,
                        "squad_database"
                )
                        .fallbackToDestructiveMigration()
                        .build()

        @Provides
        @Singleton
        fun getMessageDao(squadDataBase: SquadDataBase) = squadDataBase.messageDao()

        @Provides
        @Singleton
        fun getChatRoomsDao(squadDataBase: SquadDataBase) = squadDataBase.chatRoomsDao()

        @Provides
        @Singleton
        fun getChatInfoDao(squadDataBase: SquadDataBase) = squadDataBase.chatInfoDao()

        @Provides
        @Singleton
        fun chatGateway(
                loginRepository: LoginRepository,
                chatRepository: ChatRepository
        ): ChatGateway = EjabberdChat(loginRepository, chatRepository)
    }
}
