package co.socialsquad.squad.data.typeAdapter.genericStringAdapter

interface GettableByValue<out T> {
    fun getByValue(value: String): T
}
