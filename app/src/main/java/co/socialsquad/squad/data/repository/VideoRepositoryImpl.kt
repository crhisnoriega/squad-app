package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Completable
import javax.inject.Inject

class VideoRepositoryImpl @Inject constructor(
    private val api: Api,
    private val preferences: Preferences,
    private val gson: Gson
) : VideoRepository {

    override fun getVideoList(page: Int) = api.getVideoList(page).onDefaultSchedulers()

    override fun markAsViewed(pk: Long): Completable = api.getMediaViewCount(pk).onDefaultSchedulers()

    override fun markVideoRecommendationAsViewed(pk: Long): Completable = api.postFeedVisualized(pk).onDefaultSchedulers()

    override fun saveVideo(videoViewModel: VideoListItemViewModel) {
        videoList = videoList?.apply {
            find { it.pk == videoViewModel.pk }?.let { remove(it) }
            add(videoViewModel)
        }
    }

    override fun getSavedVideo(pk: Int) = videoList?.find { it.pk == pk }

    private var videoList: MutableList<VideoListItemViewModel>?
        get() {
            if (preferences.videoList == null) preferences.videoList = gson.toJson(mutableListOf<VideoListItemViewModel>())
            val type = object : TypeToken<MutableList<VideoListItemViewModel>>() {}.type
            return gson.fromJson(preferences.videoList, type) as MutableList<VideoListItemViewModel>?
        }
        set(list) {
            preferences.videoList = gson.toJson(list)
        }
}
