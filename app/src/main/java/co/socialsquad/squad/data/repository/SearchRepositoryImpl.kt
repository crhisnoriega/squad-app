package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(private val api: Api) : SearchRepository {
    override fun search(searchTerm: String) = api.search(searchTerm).onDefaultSchedulers()
}
