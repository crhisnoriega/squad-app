package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PersonalPointsData(
        @SerializedName("title") val title: String?,
        @SerializedName("subtitle") val subtitle: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("points_system_id") val points_system_id: String?,
) : Parcelable
