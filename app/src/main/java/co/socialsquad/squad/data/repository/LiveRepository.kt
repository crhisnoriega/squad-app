package co.socialsquad.squad.data.repository

import android.net.Uri
import co.socialsquad.squad.data.entity.AttendingUsers
import co.socialsquad.squad.data.entity.FeedComment
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.data.entity.LiveCategories
import co.socialsquad.squad.data.entity.LiveCommentRequest
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.ScheduledLiveCommentRequest
import co.socialsquad.squad.data.entity.StartLiveRequest
import co.socialsquad.squad.data.entity.request.FeedCommentsResponse
import co.socialsquad.squad.data.remote.ProgressRequestBody
import io.reactivex.Completable
import io.reactivex.Observable
import java.io.File

interface LiveRepository {
    var livePk: Int?
    var scheduleImageMedia: Uri?
    var scheduleVideoMedia: Uri?

    fun getLive(pk: Int): Observable<Live>
    fun getLiveCategories(): Observable<LiveCategories>
    fun createLive(liveCreateRequest: LiveCreateRequest): Observable<Live>
    fun endLive(pk: Int): Completable
    fun comment(pk: Int, liveCommentRequest: LiveCommentRequest): Completable
    fun joinLive(pk: Int): Completable
    fun leaveLive(pk: Int): Completable
    fun postLive(pk: Int): Completable
    fun deleteLive(pk: Int): Completable

    fun connectQueue(queue: String): Observable<QueueItem>
    fun disconnectQueue()

    fun scheduleMediaUpload(pk: Int, fileUri: Uri, callback: ProgressRequestBody.UploadCallbacks, isVideo: Boolean): Completable
    fun createPhotoFile(): File
    fun createVideoFile(): File
    fun createGalleryFile(mimeType: String): File
    fun deleteCreateFiles()
    fun confirmScheduledLiveAttendance(pk: Int): Observable<Live>
    fun startLive(startLiveRequest: StartLiveRequest): Observable<Live>
    fun getAttendingUsers(pk: Int, page: Int, type: String): Observable<AttendingUsers>
    fun commentLive(scheduledLiveCommentRequest: ScheduledLiveCommentRequest): Observable<FeedComment>
    fun getLiveComments(livePk: Int, page: Int): Observable<FeedCommentsResponse>
    fun deleteLiveComment(pk: Long): Completable
    fun deleteCreatedVideo()
    fun getLives(page: Int, ended: Boolean): Observable<Results<Live>>
    fun markMediaViewed(pk: Long): Completable
    fun markLiveRecommendationAsViewed(pk: Long): Completable
}
