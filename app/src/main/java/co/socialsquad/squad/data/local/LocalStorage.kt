package co.socialsquad.squad.data.local

import java.io.File

interface LocalStorage {
    fun createTemporaryImageFile(): File
    fun deleteTemporaryImageFile()
    fun createTemporaryVideoFile(): File
    fun deleteTemporaryVideoFile()
    fun createTemporaryGalleryFile(mimeType: String): File
    fun deleteTemporaryGalleryFile()
    fun createTemporaryAudioFile(): File
    fun deleteTemporaryAudioFile()
}
