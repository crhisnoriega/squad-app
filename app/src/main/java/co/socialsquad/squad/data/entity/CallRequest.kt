package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class CallRequest(
    @SerializedName("receiver_pk") val receiverPk: Int,
    @SerializedName("key") val uuid: String,
    @SerializedName("type") val type: String
)
