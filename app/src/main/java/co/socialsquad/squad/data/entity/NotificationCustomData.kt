package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class NotificationCustomData(
    @SerializedName("pk") val pk: Int,
    @SerializedName("model") val model: String? = null,
    @SerializedName("author_pk") val authorPk: Int,
    @SerializedName("media_pk") val mediaPk: Int
)
