package co.socialsquad.squad.data.remote

import co.socialsquad.squad.data.local.Preferences
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val preferences: Preferences) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        var builder = request.newBuilder()
        builder.addHeader("Platform", "Android")
        with(preferences) {
            subdomain?.let { builder.header("Squad-Subdomain", it) }
            token?.let { builder.addHeader("Authorization", "JWT $it") }
            language.let { builder.addHeader("Squad-Language", it) }
        }

        request = builder.build()

        var response = chain.proceed(request)

        if (response.code == 308) {
            request = response.header("Location", "")?.let {
                request.newBuilder()
                    .url(it)
                    .build()
            }!!

            response = chain.proceed(request)
        }

        if(response.code == 400){

        }

        return response
    }
}
