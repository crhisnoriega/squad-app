package co.socialsquad.squad.data.repository

import android.util.Log
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.entity.CallQueueItem
import co.socialsquad.squad.data.entity.CallRequest
import co.socialsquad.squad.data.entity.CallingQueueItem
import co.socialsquad.squad.data.entity.CancelQueueItem
import co.socialsquad.squad.data.entity.CancelVideo
import co.socialsquad.squad.data.entity.Candidate
import co.socialsquad.squad.data.entity.CandidateQueueItem
import co.socialsquad.squad.data.entity.CandidatesRemoveQueueItem
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.entity.SDP
import co.socialsquad.squad.data.entity.SdpQueueItem
import co.socialsquad.squad.data.entity.SendVideo
import co.socialsquad.squad.data.entity.VideoCallingQueueItem
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.IOException
import javax.inject.Inject

class CallRepositoryImpl @Inject constructor(
    private val api: Api,
    private val preferences: Preferences,
    private val loginRepository: LoginRepository,
    private val gson: Gson,
    private val connectionFactory: ConnectionFactory
) : CallRepository {

    private var connection: Connection? = null
    private var channel: Channel? = null
    private var channelEmitter: Channel? = null
    private var queueNameEmitter: String? = null

    private inner class Consumer(
        channel: Channel?,
        private val emitter: ObservableEmitter<CallQueueItem>
    ) : DefaultConsumer(channel) {

        @Throws(IOException::class)
        override fun handleDelivery(
            consumerTag: String?,
            envelope: Envelope?,
            properties: AMQP.BasicProperties?,
            body: ByteArray?
        ) {
            val json = body?.let { String(it) }
            try {
                gson.fromJson(json, CallQueueItem::class.java)?.let {
                    deserializeByType(json, it.type)?.let { emitter.onNext(it) }
                }
            } catch (e: JsonParseException) {
                Log.e("Parsing error", e.stackTrace.size.toString())
                e.printStackTrace()
            }
        }

        private fun deserializeByType(json: String?, type: String?): CallQueueItem? {
            return when {
                type.equals(CallQueueItem.SDP, ignoreCase = true) -> {
                    val sdpQueueItem = gson.fromJson(json, SdpQueueItem::class.java)
                    sdpQueueItem.sdp = gson.fromJson(sdpQueueItem.payload, SDP::class.java)
                    sdpQueueItem
                }
                type.equals(CallQueueItem.CANDIDATE, ignoreCase = true) -> {
                    val candidateQueueItem = gson.fromJson(json, CandidateQueueItem::class.java)
                    candidateQueueItem.candidate =
                        gson.fromJson(candidateQueueItem.payload, Candidate::class.java)
                    candidateQueueItem
                }
                type.equals(CallQueueItem.CALL, ignoreCase = true) -> {
                    val callQueueItem = gson.fromJson(json, CallingQueueItem::class.java)
                    callQueueItem.profile =
                        gson.fromJson(callQueueItem.payload, Profile::class.java)
                    callQueueItem
                }
                type.equals(CallQueueItem.VIDEOCALL, ignoreCase = true) -> {
                    val callQueueItem = gson.fromJson(json, VideoCallingQueueItem::class.java)
                    callQueueItem.profile =
                        gson.fromJson(callQueueItem.payload, Profile::class.java)
                    callQueueItem
                }
                type.equals(CallQueueItem.CANCEL, ignoreCase = true) -> gson.fromJson(
                    json,
                    CancelQueueItem::class.java
                )
                type.equals(CallQueueItem.CANDIDATESREMOVE, ignoreCase = true) -> {
                    val candidatesRemoveQueueItem =
                        gson.fromJson(json, CandidatesRemoveQueueItem::class.java)
                    candidatesRemoveQueueItem.candidates = gson.fromJson<List<String>>(
                        candidatesRemoveQueueItem.payload,
                        object : TypeToken<List<String>>() {}.type
                    ).map { gson.fromJson(it, Candidate::class.java) }
                    candidatesRemoveQueueItem
                }
                type.equals(CallQueueItem.CANCELVIDEO, ignoreCase = true) -> gson.fromJson(
                    json,
                    CancelVideo::class.java
                )
                type.equals(CallQueueItem.SENDVIDEO, ignoreCase = true) -> gson.fromJson(
                    json,
                    SendVideo::class.java
                )
                else -> null
            }
        }
    }

    override fun connectQueue() =
        Observable.create { emitter: ObservableEmitter<CallQueueItem> ->
            try {
                if (BuildConfig.DEBUG) connectionFactory.apply {
                    username = BuildConfig.RABBITMQ_USER
                    password = BuildConfig.RABBITMQ_PASSWORD
                    host = BuildConfig.RABBITMQ_HOST
                    virtualHost = BuildConfig.RABBITMQ_VIRTUAL_HOST
                    port = BuildConfig.RABBITMQ_PORT
                }
                connection = connectionFactory.newConnection()
                loginRepository.userCompany?.user?.chatId?.let {
                    channel = connection?.createChannel()
                    channel?.exchangeDeclare(it, "fanout", true)
                    val queueName = channel?.queueDeclare()?.queue
                    channel?.queueBind(queueName, it, "")
                    val consumer = Consumer(channel, emitter)
                    channel?.basicConsume(queueName, true, consumer)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.onError(e)
            }
        }.onDefaultSchedulers()

    val compositeDisposable = CompositeDisposable()

    var currentCallId: String? = null

    override fun connectToEmitterQueue(queueId: String): PublishSubject<CallQueueItem> {
        val remoteQueue = PublishSubject.create<CallQueueItem>()
        compositeDisposable.add(
            remoteQueue
                .doOnSubscribe {
                    channelEmitter = connection?.createChannel()
                    channelEmitter?.exchangeDeclare(queueId, "fanout", true)
                    queueNameEmitter = channelEmitter?.queueDeclare()?.queue
                    channelEmitter?.queueBind(queueNameEmitter, queueId, "")
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext {
                    if (it is CallingQueueItem) {
                        it.receiverPk?.let { receiverPk ->
                            if (currentCallId == null) {
                                currentCallId = it.uuid
                                api.createCall(CallRequest(receiverPk, it.uuid, "a"))
                                    .onDefaultSchedulers()
                                    .subscribe({}, { currentCallId = null }, {})
                            }
                        }
                    }
                    if (it is VideoCallingQueueItem) {
                        it.receiverPk?.let { receiverPk ->
                            if (currentCallId == null) {
                                currentCallId = it.uuid
                                api.createCall(CallRequest(receiverPk, it.uuid, "v"))
                                    .onDefaultSchedulers()
                                    .subscribe({}, { currentCallId = null }, {})
                            }
                        }
                    }
                    if (it is CancelQueueItem) {
                        currentCallId = null
                    }
                }
                .map { queueItem ->
                    when (queueItem) {
                        is SdpQueueItem -> {
                            queueItem.sdp?.let {
                                queueItem.payload = gson.toJson(it, SDP::class.java)
                            }
                            gson.toJson(queueItem, SdpQueueItem::class.java)
                        }
                        is CandidateQueueItem -> {
                            queueItem.candidate?.let {
                                queueItem.payload = gson.toJson(it, Candidate::class.java)
                            }
                            gson.toJson(queueItem, CandidateQueueItem::class.java)
                        }
                        is CallingQueueItem -> {
                            loginRepository.userCompany?.let {
                                queueItem.payload = gson.toJson(
                                    Profile(
                                        it.pk,
                                        it.user.firstName,
                                        it.user.lastName,
                                        it.user.avatar,
                                        it.qualification,
                                        it.qualificationBadge,
                                        it.user.chatId
                                    ), Profile::class.java
                                )
                            }
                            gson.toJson(queueItem, CallingQueueItem::class.java)
                        }
                        is VideoCallingQueueItem -> {
                            loginRepository.userCompany?.let {
                                queueItem.payload = gson.toJson(
                                    Profile(
                                        it.pk,
                                        it.user.firstName,
                                        it.user.lastName,
                                        it.user.avatar,
                                        it.qualification,
                                        it.qualificationBadge,
                                        it.user.chatId
                                    ), Profile::class.java
                                )
                            }
                            gson.toJson(queueItem, VideoCallingQueueItem::class.java)
                        }
                        is CancelQueueItem -> gson.toJson(queueItem, CancelQueueItem::class.java)
                        is CandidatesRemoveQueueItem -> {
                            queueItem.candidates?.let {
                                val candidates = it.map { gson.toJson(it, Candidate::class.java) }
                                queueItem.payload = gson.toJson(
                                    candidates,
                                    object : TypeToken<List<String>>() {}.type
                                )
                            }
                            gson.toJson(queueItem, CandidatesRemoveQueueItem::class.java)
                        }
                        is CancelVideo -> gson.toJson(queueItem, CancelVideo::class.java)
                        is SendVideo -> gson.toJson(queueItem, SendVideo::class.java)
                        else -> ""
                    }
                }
                .filter { !it.isEmpty() }
                .map { it.toByteArray() }
                .subscribe(
                    {
                        channelEmitter?.basicPublish(queueId, queueNameEmitter, null, it)
                    },
                    {
                        it.printStackTrace()
                    },
                    {
                        channelEmitter = null
                        compositeDisposable.clear()
                    }
                )
        )
        return remoteQueue
    }

    override fun clearCurrentCall() {
        currentCallId = null
    }
}
