package co.socialsquad.squad.data.entity

data class CallResumeTokenRequest(val t: String)
