package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.Feedback
import co.socialsquad.squad.data.entity.FeedbackAnswer
import io.reactivex.Completable
import io.reactivex.Observable

interface FeedbackRepository {
    fun getFeedback(): Observable<Feedback>
    fun sendAnswer(feedbackAnswer: FeedbackAnswer): Completable
}
