package co.socialsquad.squad.data.repository

import android.content.ContentResolver
import android.net.Uri
import android.webkit.MimeTypeMap
import co.socialsquad.squad.data.entity.FeedCommentRequest
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.UnreadNotificationCount
import co.socialsquad.squad.data.local.LocalStorage
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiUpload
import co.socialsquad.squad.data.remote.ProgressRequestBody
import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import java.io.File
import javax.inject.Inject

class SocialRepositoryImpl @Inject constructor(
    private val api: Api,
    private val apiUpload: ApiUpload,
    private val preferences: Preferences,
    private val localStorage: LocalStorage,
    private val contentResolver: ContentResolver
) : SocialRepository {

    override var audioEnabled: Boolean
        get() = preferences.audioEnabled
        set(audioEnabled) {
            preferences.audioEnabled = audioEnabled
        }

    override var createMedia: Uri?
        get() = Uri.parse(preferences.mediaUri)
        set(createMediaUri) {
            preferences.mediaUri = createMediaUri.toString()
        }

    override fun getFeed(page: Int, width: Int) = api.getFeed(page, width).onDefaultSchedulers()

    override fun getFeedUsersLiked(postPk: Int, page: Int) = api.getPostLikedUsers(postPk, page).onDefaultSchedulers()

    override fun getSegmentation(pk: Int) = api.getFeedSegmentation(pk).onDefaultSchedulers()

    override fun postFeed(feedRequest: FeedRequest) = api.postFeed(feedRequest).onDefaultSchedulers()

    override fun postFeedUpload(pk: Int, fileUri: Uri, callback: ProgressRequestBody.UploadCallbacks): Completable {
        val file = File(fileUri.path)

        val mediaType = if (fileUri.scheme == ContentResolver.SCHEME_CONTENT) {
            val type = contentResolver.getType(fileUri) ?: return Completable.error(Throwable())
            type.toMediaTypeOrNull()
        } else {
            val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString())
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            mimeType!!.toMediaTypeOrNull()
        }

        val progressRequestBody = ProgressRequestBody(file, mediaType, callback)
        val part = MultipartBody.Part.createFormData("files", file.name, progressRequestBody)

        return apiUpload.postFeedUpload(pk, part).onDefaultSchedulers()
    }

    override fun editPost(pk: Int, feedRequest: FeedRequest) = api.putFeed(pk, feedRequest).onDefaultSchedulers()

    override fun deleteFeed(pk: Int) = api.deleteFeed(pk).onDefaultSchedulers()

    override fun getFeedItem(pk: Int) = api.getFeedItem(pk).onDefaultSchedulers()

    override fun createPhotoFile() = localStorage.createTemporaryImageFile()

    override fun createVideoFile() = localStorage.createTemporaryVideoFile()

    override fun createGalleryFile(mimeType: String) = localStorage.createTemporaryGalleryFile(mimeType)

    override fun deleteCreateFiles() {
        localStorage.deleteTemporaryImageFile()
        localStorage.deleteTemporaryVideoFile()
        localStorage.deleteTemporaryGalleryFile()
    }

    override fun likePost(pk: Int) = api.likePost(pk).onDefaultSchedulers()

    override fun getNotifications(page: Int) = api.getNotifications(page).onDefaultSchedulers()

    override fun clickNotification(pk: Long) = api.postClickedNotification(pk).onDefaultSchedulers()

    override fun clearUnreadNotifications() = api.postClearUnreadNotifications().onDefaultSchedulers()

    override fun commentPost(feedCommentRequest: FeedCommentRequest) = api.commentPost(feedCommentRequest).onDefaultSchedulers()

    override fun getPostComments(feedPk: Int, page: Int) = api.getPostComments(feedPk, page).onDefaultSchedulers()

    override fun deleteComment(pk: Long) = api.deleteComment(pk).onDefaultSchedulers()

    override fun getUnreadItems(): Observable<UnreadNotificationCount> {
        return api.getNotVisualizedNotificationsAndAudios().onDefaultSchedulers()
    }
}
