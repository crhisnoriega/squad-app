package co.socialsquad.squad.data.local.onboarding

import android.content.Context
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Onboarding

fun onboardingInfo(context: Context): List<Onboarding> {
    val onboardingInfo = mutableListOf<Onboarding>()
    onboardingInfo.add(Onboarding("", context.getString(R.string.onboarding_step_one)))
    onboardingInfo.add(
        Onboarding(
            context.getString(R.string.communication),
            context.getString(R.string.onboarding_step_two)
        )
    )
    onboardingInfo.add(
        Onboarding(
            context.getString(R.string.content),
            context.getString(R.string.onboarding_step_three)
        )
    )
    onboardingInfo.add(
        Onboarding(
            context.getString(R.string.workflow),
            context.getString(R.string.onboarding_step_four)
        )
    )
    onboardingInfo.add(
        Onboarding(
            context.getString(R.string.notification),
            context.getString(R.string.onboarding_step_five)
        )
    )
    onboardingInfo.add(
        Onboarding(
            context.getString(R.string.identity),
            context.getString(R.string.onboarding_step_six)
        )
    )
    return onboardingInfo
}
