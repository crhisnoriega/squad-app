package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.opportunity.OpportunityActionResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityInformationResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityListResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmission
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetailsResponse
import io.reactivex.Single

interface OpportunityRepository {
    fun opportunityAction(opportunityId: Int): Single<OpportunityActionResponse>
    fun createOpportunity(opportunitySubmission: OpportunitySubmission): Single<OpportunitySubmissionResponse>
    fun opportunitySubmissions(
        opportunityId: Int,
        immobileId: Int,
        objectType: OpportunityObjectType
    ): Single<OpportunityListResponse>

    fun opportunitySubmissionDetails(
        opportunityId: Int,
        objectId: Int,
        submissionId: Int,
        objectType: OpportunityObjectType
    ): Single<SubmissionDetailsResponse>

    fun opportunityDetails(
        opportunityId: Int
    ): Single<OpportunityInformationResponse>

    fun opportunityStageDetails(
        stageId: Int
    ): Single<OpportunityInformationResponse>
}
