package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class Call(val pk: Int)

open class CallQueueItem(
    @SerializedName("type") val type: String,
    @SerializedName("uuid") open var uuid: String
) {
    companion object {
        val CALL = "call"
        val VIDEOCALL = "videoCall"
        val CANDIDATE = "candidate"
        val CANDIDATESREMOVE = "candidatesRemoved"
        val SDP = "sdp"
        val CANCEL = "cancel"
        val FAILURE = "fail"
        val BUSY = "busy"
        val SENDVIDEO = "sendVideo"
        val CANCELVIDEO = "cancelVideo"
    }
}

class VideoCallingQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("createdAt") val createdAt: Date?,
    @SerializedName("payload") var payload: String? = null,
    var profile: Profile? = null,
    var receiverPk: Int? = null
) : CallQueueItem(CallQueueItem.VIDEOCALL, callUuid)

class CallingQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("createdAt") val createdAt: Date?,
    @SerializedName("payload") var payload: String? = null,
    var profile: Profile? = null,
    var receiverPk: Int? = null
) : CallQueueItem(CallQueueItem.CALL, callUuid)

class CancelQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("createdAt") val createdAt: Date?
) : CallQueueItem(CallQueueItem.CANCEL, callUuid)

class SdpQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("payload") var payload: String? = null,
    var sdp: SDP? = null,
    @SerializedName("createdAt") val createdAt: Date?
) : CallQueueItem(CallQueueItem.SDP, callUuid)

class CandidateQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("payload") var payload: String? = null,
    var candidate: Candidate? = null,
    @SerializedName("createdAt") val createdAt: Date?
) : CallQueueItem(CallQueueItem.CANDIDATE, callUuid)

class CandidatesRemoveQueueItem(
    callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("payload") var payload: String? = null,
    var candidates: List<Candidate>? = null,
    @SerializedName("createdAt") val createdAt: Date?
) : CallQueueItem(CallQueueItem.CANDIDATESREMOVE, callUuid)

class CandidatesList(var candidates: List<Candidate>? = null)

class Candidate(
    @SerializedName("sdp") val sdp: String,
    @SerializedName("sdpMid") val sdpMid: String,
    @SerializedName("sdpMLineIndex") val sdpMLineIndex: Int
)

class SDP(
    @SerializedName("type") val type: Int,
    @SerializedName("sdp") val sdp: String
)

class Profile(
    @SerializedName("pk") var pk: Int,
    @SerializedName("firstName") var firstName: String? = null,
    @SerializedName("lastName") var lastName: String? = null,
    @SerializedName("avatar") var avatar: String? = null,
    @SerializedName("qualification") var qualification: String? = null,
    @SerializedName("qualificationBadge") var qualificationBadge: String? = null,
    @SerializedName("chatId") var chatId: String? = null
) : Serializable

class SendVideo(
    var callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("createdAt") val createdAt: Date?,
    @SerializedName("payload") var payload: String = ""
) : CallQueueItem(CallQueueItem.SENDVIDEO, callUuid)

class CancelVideo(
    var callUuid: String,
    @SerializedName("chatId") val chatId: String?,
    @SerializedName("createdAt") val createdAt: Date?,
    @SerializedName("payload") var payload: String = ""
) : CallQueueItem(CallQueueItem.CANCELVIDEO, callUuid)
