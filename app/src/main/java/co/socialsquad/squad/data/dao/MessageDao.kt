package co.socialsquad.squad.data.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import co.socialsquad.squad.data.entity.database.CHAT_MESSAGES_TABLE_NAME
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatMessageStatus
import io.reactivex.Maybe

@Dao
interface MessageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chatMessage: ChatMessage)

    @Query("DELETE from $CHAT_MESSAGES_TABLE_NAME")
    fun deleteAllMessages()

    @Query("SELECT * FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl ORDER BY date DESC")
    fun getMessages(channelUrl: String): DataSource.Factory<Int, ChatMessage>

    @Query("SELECT message_id, message, owner_id, channel_url, max(date) as date, status FROM $CHAT_MESSAGES_TABLE_NAME GROUP BY channel_url")
    fun getLastMessages(): Maybe<List<ChatMessage>>

    @Query("SELECT message_id, message, owner_id, channel_url, max(date) as date, status FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl LIMIT 1")
    fun getLastMessages(channelUrl: String): Maybe<ChatMessage>

    @Query("SELECT message_id, message, owner_id, channel_url, min(date) as date, status FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl LIMIT 1")
    fun getFirstMessages(channelUrl: String): Maybe<ChatMessage>

    @Update
    fun updateMessages(vararg items: ChatMessage)

    @Query("UPDATE $CHAT_MESSAGES_TABLE_NAME SET message_id = :id, message  = :message, owner_id  = :ownerId, channel_url  = :channelUrl, date  = :newDate, status  = :status WHERE owner_id  = :ownerId AND date  = :originalDate")
    fun updateByDateAndOwner(
        id: String,
        message: String,
        ownerId: String,
        channelUrl: String,
        newDate: Long,
        status: String,
        originalDate: Long
    )

    @Query("UPDATE $CHAT_MESSAGES_TABLE_NAME SET message_id = :id, message  = :message, owner_id  = :ownerId, channel_url  = :channelUrl, date  = :date, status  = :status WHERE message_id = :id")
    fun updateById(
        id: String,
        message: String,
        ownerId: String,
        channelUrl: String,
        date: Long,
        status: String
    )

    @Query("UPDATE $CHAT_MESSAGES_TABLE_NAME SET status = :read WHERE channel_url = :channelUrl AND status = 'UNREAD'")
    fun updateStatusToRead(channelUrl: String, read: String = ChatMessageStatus.READ.name)

    @Query("SELECT COUNT(message_id) FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl")
    fun getMessageCount(channelUrl: String): Maybe<Int>

    @Query("SELECT COUNT(message_id) FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl AND status = 'UNREAD'")
    fun getUnreadMessageCount(channelUrl: String): Maybe<Int>

    @Query("SELECT COUNT(message_id) FROM $CHAT_MESSAGES_TABLE_NAME WHERE status = 'UNREAD'")
    fun getUnreadMessageCount(): Maybe<Int>

    @Query("SELECT * FROM $CHAT_MESSAGES_TABLE_NAME WHERE channel_url = :channelUrl AND message_id = :id ORDER BY date DESC")
    fun getMessage(channelUrl: String, id: String): Maybe<ChatMessage>
}
