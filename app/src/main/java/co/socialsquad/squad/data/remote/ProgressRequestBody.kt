package co.socialsquad.squad.data.remote

import android.os.Handler
import android.os.Looper
import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream

private const val DEFAULT_BUFFER_SIZE = 2048

class ProgressRequestBody(
    private val file: File,
    private val mediaType: MediaType?,
    private val listener: UploadCallbacks
) : RequestBody() {

    interface UploadCallbacks {
        fun onProgressUpdate(percentage: Int)
    }

    override fun contentType() = mediaType

    override fun contentLength() = file.length()

    override fun writeTo(sink: BufferedSink) {
        val fileLength = file.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val fileInputStream = FileInputStream(file)
        var uploaded: Long = 0

        fileInputStream.use { inputStream ->
            var read = inputStream.read(buffer)
            val handler = Handler(Looper.getMainLooper())

            while (read != -1) {
                handler.post(ProgressUpdater(uploaded, fileLength))
                uploaded += read.toLong()
                sink.write(buffer, 0, read)
                read = inputStream.read(buffer)
            }
        }
    }

    private inner class ProgressUpdater(private val uploaded: Long, private val total: Long) : Runnable {
        override fun run() = listener.onProgressUpdate((100 * uploaded / total).toInt())
    }
}
