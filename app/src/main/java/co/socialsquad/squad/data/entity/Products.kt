package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Product(
    @SerializedName("pk") var pk: Int = -1,
    @SerializedName("name") var name: String,
    @SerializedName("pid") var pid: Int,
    @SerializedName("full_name") var fullName: String,
    @SerializedName("dosage") var dosage: String? = null,
    @SerializedName("buy_points") var buyPoints: Int? = null,
    @SerializedName("price") var price: Double? = null,
    @SerializedName("medias") var medias: List<Media>? = null,
    @SerializedName("categories") var categories: List<ProductCategory>? = null,
    @SerializedName("content") var content: String? = null,
    @SerializedName("grouped_details") var detailGroups: List<ProductDetailGroup>? = null,
    @SerializedName("discount") var discount: Double = 0.toDouble(),
    @SerializedName("discount_value") var discountValue: Double = 0.toDouble(),
    @SerializedName("has_discount") var hasDiscount: Boolean = false,
    @SerializedName("types") var types: Types? = null,
    @SerializedName("company_color") var companyColor: String? = null,
    @SerializedName("has_details") var hasDetails: Boolean = false
)

data class ProductCategory(
    @SerializedName("pk") var pk: Int,
    @SerializedName("name") var name: String
) : Serializable

data class ProductDetailGroup(
    @SerializedName("name") var name: String,
    @SerializedName("content") var content: String,
    @SerializedName("icon_url") var iconUrl: String,
    @SerializedName("cover_url") var coverUrl: String,
    @SerializedName("details") var details: List<ProductDetail>,
    @SerializedName("count_name") var countName: String,
    @SerializedName("media") var media: Media? = null
) : Serializable

data class ProductDetail(
    @SerializedName("name") var name: String,
    @SerializedName("snippet") var snippet: String? = null,
    @SerializedName("detail") var detail: String,
    @SerializedName("medias") var medias: List<Media>? = null,
    @SerializedName("links") var links: List<String>? = null,
    @SerializedName("company_color") var companyColor: String
) : Serializable

data class ProductsFilterRequest(
    @SerializedName("qrcode") var qrcode: String? = null,
    @SerializedName("barcode") var barcode: String? = null,
    @SerializedName("price") var price: Price? = null,
    @SerializedName("buy-points") var buyPoints: BuyPoints? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("category") var category: Int? = null,
    @SerializedName("list") var list: Int? = null
) {
    data class Price(
        @SerializedName("max") val max: Double,
        @SerializedName("min") val min: Double
    )

    data class BuyPoints(
        @SerializedName("max") val max: Int,
        @SerializedName("min") val min: Int
    )
}

data class FindProductRequest(
    @SerializedName("pid") val pid: Int,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lng") val lng: Double
)
