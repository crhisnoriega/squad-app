package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class WalletProfileModel(
        @SerializedName("data") val data: WalletProfileDataModel?,
        @SerializedName("show_wallet") val show_wallet: Boolean?,
        @SerializedName("show_revenue") val show_revenue: Boolean?
)
