package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RankingProfileModel(
        @SerializedName("ranking_id") val rank_id: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("subtitle") val subtitle: String?,
        @SerializedName("participants") val participants: String?,
        @SerializedName("points") val points: String?,
        @SerializedName("position") val position: String?,
        @SerializedName("multiple_rankings") val multiple_rankings: Boolean?,
        @SerializedName("number_of_ranks") val number_of_ranks: Int?,
        @SerializedName("user_in_rank") val user_in_rank: Boolean? = false,

        )
