package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Requirement(
    @SerializedName("pk") val pk: Int? = null,
    @SerializedName("description") val description: String,
    @SerializedName("icon") val icon: String,
    @SerializedName("name") val name: String,
    @SerializedName("promotion_value") val promotion_value: Float,
    @SerializedName("value") val value: Float
)
