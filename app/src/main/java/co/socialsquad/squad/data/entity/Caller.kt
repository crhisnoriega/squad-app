package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Caller(
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("pk")
    val pk: Int,
    @SerializedName("qualification")
    val qualification: String,
    @SerializedName("user")
    val user: User
)
