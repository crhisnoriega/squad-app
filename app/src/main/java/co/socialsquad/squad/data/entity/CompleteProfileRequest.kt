package co.socialsquad.squad.data.entity

data class CompleteProfileRequest(val user: User, val pk: Int) {
    data class User(val first_name: String, val last_name: String, val phones: List<Phone>, val email: String) {
        data class Phone(val category: String, val number: String)
    }
}
