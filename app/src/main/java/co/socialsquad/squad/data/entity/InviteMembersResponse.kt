package co.socialsquad.squad.data.entity

data class InviteMembersResponse(val pk: Int, val email: String, val status: String, val status_display: String)
