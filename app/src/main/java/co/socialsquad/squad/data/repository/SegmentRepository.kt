package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.AudienceType
import co.socialsquad.squad.data.entity.Members
import co.socialsquad.squad.data.entity.MembersRequest
import co.socialsquad.squad.data.entity.Metrics
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.SearchMembersRequest
import co.socialsquad.squad.data.entity.SegmentationPotentialReach
import co.socialsquad.squad.data.entity.SegmentationReachRequest
import co.socialsquad.squad.data.entity.SegmentationSearchRequest
import co.socialsquad.squad.data.entity.Values
import io.reactivex.Observable

interface SegmentRepository {
    fun getTypes(): Observable<Results<AudienceType>>
    fun getMembers(groupPk: Int, page: Int, membersRequest: MembersRequest): Observable<Members>
    fun getMetrics(): Observable<Metrics>
    fun getValues(field: String, page: Int): Observable<Values>
    fun getSegmentationMembers(page: Int): Observable<Members>
    fun getValuesFiltered(segmentationSearchRequest: SegmentationSearchRequest, page: Int): Observable<Values>
    fun postSearchSegmentationMembers(page: Int, searchMembersRequest: SearchMembersRequest): Observable<Members>
    fun getPotentialReach(segmentationReachRequest: SegmentationReachRequest): Observable<SegmentationPotentialReach>
}
