package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class Post(
    @SerializedName("pk") val pk: Int = 0,
    @SerializedName("content") val content: String? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("medias") var medias: List<Media>? = null,
    @SerializedName("author") val author: Author,
    @SerializedName("type") var type: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("updated_at") val updatedAt: String? = null,
    @SerializedName("live") val live: Live? = null,
    @SerializedName("type_display") val typeDisplay: String? = null,
    @SerializedName("visualized") var visualized: Boolean = false,
    @SerializedName("can_delete") var canDelete: Boolean = false,
    @SerializedName("can_edit") var canEdit: Boolean = false,
    @SerializedName("can_share") var canShare: Boolean = false,
    @SerializedName("can_be_recommended") var canRecommend: Boolean = false,
    @SerializedName("segmentation") var segmentation: Segmentation? = null,
    @SerializedName("audience_type") var audienceType: String,
    @SerializedName("lives") var lives: List<Live>? = null,
    @SerializedName("liked") var liked: Boolean = false,
    @SerializedName("liked_users_avatars") var likedUsersAvatars: List<String>? = null,
    @SerializedName("likes_count") var likesCount: Int = 0,
    @SerializedName("comments_count") var commentsCount: Int = 0,
    @SerializedName("cover") val cover: Media? = null
) : Serializable {
    companion object {
        const val TYPE_MEDIA = "M"
        const val TYPE_POST_LIVE = "PL"
        const val TYPE_LIVES = "LS"
        const val TYPE_VIDEO = "V"
        const val TYPE_SUCCESS_STORY = "ST"
    }
}

class ScheduledPostRequest(
    @SerializedName("post_time") val startTime: Date? = null
) : Serializable
