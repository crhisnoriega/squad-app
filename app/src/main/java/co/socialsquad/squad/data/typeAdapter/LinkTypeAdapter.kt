package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class LinkTypeAdapter : TypeAdapter<LinkType>() {
    override fun write(writer: JsonWriter?, value: LinkType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.nullValue()
    }

    override fun read(reader: JsonReader?): LinkType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> LinkType.getByValue(it.nextString())
                else -> LinkType.Invalid
            }
        } ?: LinkType.Invalid
    }
}
