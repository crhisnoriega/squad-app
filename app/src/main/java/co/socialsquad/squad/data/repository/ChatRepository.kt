package co.socialsquad.squad.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import co.socialsquad.squad.data.entity.ChatList
import co.socialsquad.squad.data.entity.ChatRoom
import co.socialsquad.squad.data.entity.ChatSearchRequest
import co.socialsquad.squad.data.entity.database.ChatInfoModel
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.presentation.feature.chat.MessageViewModel
import io.reactivex.Maybe
import io.reactivex.Observable

interface ChatRepository {
    fun getChatList(): Observable<ChatList>
    fun createChatRoom(pk: Int): Observable<ChatRoom>
    fun search(chatSearchRequest: ChatSearchRequest): Observable<ChatList>
    fun storeMessage(chatMessage: ChatMessage)
    fun updateStoredMessage(chatMessage: ChatMessage)
    fun updateStoredMessageByDate(chatMessage: ChatMessage, dataBaseDate: Long)
    fun getMessagesFromChatRoom(context: Context, channelUrl: String): LiveData<PagedList<MessageViewModel>>
    fun getLastStoredMessages(): Maybe<List<ChatMessage>>
    fun getLastMessage(channelUrl: String): Maybe<ChatMessage>
    fun getChatRoom(): Maybe<List<ChatRoomModel>>
    fun getChatRoom(pk: Int): Maybe<ChatRoomModel>
    fun getChatRoomLiveData(pk: Int): LiveData<ChatRoomModel>
    fun getChatRoom(channelUrl: String): Maybe<ChatRoomModel>
    fun storeChatRoom(chatRooms: List<ChatRoomModel>)
    fun storeChatRoom(chatRooms: ChatRoomModel)
    fun markStoredMessagesAsRead(channelUrl: String)
    fun getLiveDataRooms(): LiveData<List<ChatRoomModel>>
    fun updateLastMessage(channelUrl: String, message: String, unreadCount: Int, elapsedTime: Long)
    fun updateUnreadCount(unreadCount: Int)
    fun getChatInfoLiveData(): LiveData<ChatInfoModel>
    fun getFirstMessage(channelUrl: String): Maybe<ChatMessage>
    fun getTotalMessagesUnreadCount(): Maybe<Int>
    fun getMessagesUnreadCount(channelUrl: String): Maybe<Int>
    fun hasMessageStored(channelUrl: String): Maybe<Boolean>
    fun getLastSync(): Observable<Long>
}
