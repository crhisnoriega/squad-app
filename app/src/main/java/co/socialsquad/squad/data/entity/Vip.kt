package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Vip(
    @SerializedName("full_name")
    val fullName: String? = null,
    @SerializedName("pk")
    val pk: Int? = null,
    @SerializedName("qualification")
    val qualification: String? = null,
    @SerializedName("user")
    val user: User? = null
)
