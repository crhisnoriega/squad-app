package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.data.entity.ImmobileResponse
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import io.reactivex.Observable
import javax.inject.Inject

class ImmobilesRepositoryImpl @Inject constructor(
    private val api: Api
) :
    ImmobilesRepository {

    override fun immobiles(page: Int): Observable<ImmobileResponse> = api.immobiles(page).onDefaultSchedulers()

    override fun immobileDetail(immobileId: Int): Observable<List<ImmobileDetail>> = api.immobilesDetails(immobileId).onDefaultSchedulers()
}
