package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.InviteMembersRequest
import co.socialsquad.squad.data.entity.InviteMembersResponse
import io.reactivex.Observable

interface InviteMemberRepository {
    fun inviteMembers(body: List<InviteMembersRequest>): Observable<List<InviteMembersResponse>>
}
