package co.socialsquad.squad.data.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone
import kotlin.jvm.Throws

internal class DateDeserializer : JsonDeserializer<Date?> {

    private val dateFormats = arrayOf(
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
        "yyyy-MM-dd'T'HH:mm:ss'Z'",
        "yyyy-MM-dd HH:mm:ss'Z'",
        "yyyy-MM-dd"
    )
    private val dateFormatters: List<SimpleDateFormat> = dateFormats.map {
        SimpleDateFormat(it).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
    }

    @Throws(JsonParseException::class)
    override fun deserialize(
        element: JsonElement,
        type: Type?,
        jsonDeserializationContext: JsonDeserializationContext?
    ): Date? {
        dateFormatters.forEach {
            try {
                return it.parse(element.asString)
            } catch (e: ParseException) {
            }
        }
        return null
    }
}
