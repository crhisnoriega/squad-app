package co.socialsquad.squad.data.local

import android.content.Context
import android.webkit.MimeTypeMap
import java.io.File
import javax.inject.Inject

private const val TEMP_IMAGE_FILENAME = "TEMP_IMAGE.jpg"
private const val TEMP_VIDEO_FILENAME = "TEMP_VIDEO.mp4"
private const val TEMP_GALLERY_FILENAME = "TEMP_GALLERY_FILENAME."
private const val TEMP_AUDIO_FILENAME = "TEMP_AUDIO."

class LocalStorageImpl @Inject constructor(context: Context) : LocalStorage {

    private val cacheDir = context.cacheDir

    override fun createTemporaryImageFile(): File {
        return createFile(TEMP_IMAGE_FILENAME)
    }

    override fun deleteTemporaryImageFile() {
        deleteFile(TEMP_IMAGE_FILENAME)
    }

    override fun createTemporaryVideoFile(): File {
        return createFile(TEMP_VIDEO_FILENAME)
    }

    override fun deleteTemporaryVideoFile() {
        deleteFile(TEMP_VIDEO_FILENAME)
    }

    override fun createTemporaryGalleryFile(mimeType: String): File {
        val extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType)
        return createFile(TEMP_GALLERY_FILENAME + extension)
    }

    override fun deleteTemporaryGalleryFile() {
        deleteFile(TEMP_GALLERY_FILENAME)
    }

    private fun createFile(fileName: String): File {
        val file = File(cacheDir, fileName)
        if (file.exists()) file.delete()
        return file
    }

    private fun deleteFile(fileName: String) {
        val file = File(cacheDir, fileName)
        if (file.exists()) file.delete()
    }

    override fun createTemporaryAudioFile(): File {
        return createFile(TEMP_AUDIO_FILENAME + "m4a")
    }

    override fun deleteTemporaryAudioFile() {
        deleteFile(TEMP_AUDIO_FILENAME + "m4a")
    }
}
