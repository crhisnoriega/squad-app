package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class LiveCommentRequest(@SerializedName("text") val text: String)
