package co.socialsquad.squad.data.remote

import co.socialsquad.squad.presentation.feature.profile.edit.data.MediaResponse
import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path

interface ApiUpload {

    @Multipart
    @POST("endpoints/user/avatar/upload/{pk}/")
    fun postAvatarUpload(@Path("pk") pk: Int, @Part part: MultipartBody.Part): Completable

    @Multipart
    @POST("endpoints/media")
    fun uploadMedia(@Part part: MultipartBody.Part): Observable<MediaResponse>


    @Multipart
    @POST("endpoints/media/upload/feed/{pk}/")
    fun postFeedUpload(@Path("pk") pk: Int, @Part part: MultipartBody.Part): Completable

    @Multipart
    @POST("endpoints/media/upload/schedule/{pk}/{field}")
    fun postScheduleUpload(@Path("pk") pk: Int, @Path("field") field: String, @Part part: MultipartBody.Part): Completable

    @Multipart
    @POST("/endpoints/media/upload/audio/{pk}/")
    fun postAudioUpload(@Path("pk") pk: Int, @Part part: MultipartBody.Part): Completable
}
