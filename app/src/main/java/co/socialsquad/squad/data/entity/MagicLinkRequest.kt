package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class MagicLinkRequest(
    @SerializedName("email") val email: String
)
