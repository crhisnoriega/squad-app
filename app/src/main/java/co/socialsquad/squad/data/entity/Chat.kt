package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChatList(
    @SerializedName("sections") var sections: List<ChatSection>,
    @SerializedName("contacts") var contacts: List<UserCompany>
)

data class ChatSection(
    @SerializedName("title") val title: String,
    @SerializedName("chats") var chats: List<ChatRoom>
)

data class ChatRoom(
    @SerializedName("unread") val unreadCount: Int = 0,
    @SerializedName("channel_url") val channelUrl: String,
    @SerializedName("chat_with") val chatWith: UserCompany
) : Serializable
