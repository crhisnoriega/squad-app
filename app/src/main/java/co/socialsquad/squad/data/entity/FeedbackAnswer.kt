package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedbackAnswer(
    @SerializedName("pk") val pk: Int,
    @SerializedName("sections") val sections: List<FeedbackAnswerSection>
)
