package co.socialsquad.squad.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import io.reactivex.Maybe

@Dao
interface ChatRoomDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chatRoomModel: ChatRoomModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg chatRoomModel: ChatRoomModel)

    @Query("SELECT * FROM CHAT_ROOMS_TABLE_NAME")
    fun getStoredRooms(): Maybe<List<ChatRoomModel>>

    @Query("SELECT * FROM CHAT_ROOMS_TABLE_NAME ORDER BY elapsedTime DESC")
    fun getStoredRoomsAsLiveData(): LiveData<List<ChatRoomModel>>

    @Query("SELECT * FROM CHAT_ROOMS_TABLE_NAME WHERE pk = :userPk")
    fun getStoredRoom(userPk: Int): Maybe<ChatRoomModel>

    @Query("SELECT * FROM CHAT_ROOMS_TABLE_NAME WHERE pk = :userPk")
    fun getStoredRoomAsLiveData(userPk: Int): LiveData<ChatRoomModel>

    @Query("SELECT * FROM CHAT_ROOMS_TABLE_NAME WHERE channelUrl = :channelUrl")
    fun getStoredRoom(channelUrl: String): Maybe<ChatRoomModel>

    @Query("UPDATE CHAT_ROOMS_TABLE_NAME set lastMessage = :message, unreadCount = :unreadCount, elapsedTime = :elapsedTime WHERE channelUrl = :channelUrl")
    fun updateLastMessage(channelUrl: String, message: String, unreadCount: Int, elapsedTime: Long)
}
