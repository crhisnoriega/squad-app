package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.widget.WidgetType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class WidgetTypeAdapter : TypeAdapter<WidgetType>() {
    override fun write(writer: JsonWriter?, value: WidgetType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(WidgetType.getDefault().value)
    }

    override fun read(reader: JsonReader?): WidgetType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> WidgetType.getByValue(it.nextString())
                else -> WidgetType.getDefault()
            }
        } ?: WidgetType.getDefault()
    }
}
