package co.socialsquad.squad.data.entity

class ChatStateMessage(val status: ChatStatus, val channel: String)

enum class ChatStatus {
    ACTIVE,
    COMPOSING,
    INACTIVE
}
