package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

// data class Search(
//        @SerializedName("events") val events: Results<Event>? = null,
//        @SerializedName("videos") val videos: Results<Post>? = null,
//        @SerializedName("audios") val audios: Results<Audio>? = null,
//        @SerializedName("products") val products: Results<Product>? = null,
//        @SerializedName("lives") val lives: Results<Live>? = null,
//        @SerializedName("user_companies") val userCompanies: Results<UserCompany>? = null,
//        @SerializedName("stores") val stores: Results<Store>? = null,
//        @SerializedName("documents") val documents: Results<Document>? = null,
//        @SerializedName("trips") val trips: Results<Trip>? = null,
//        @SerializedName("immobile") val immobiles: Results<Immobile>? = null
// )

data class SearchV2(
    val data: List<Data>? = listOf(),
    val success: Boolean? = false
)

data class Data(
    val button: Button? = Button(),
    val id: Int? = 0,
    val items: List<Item>? = listOf(),
    val template: String? = "",
    val title: String? = "",
    val type: String? = ""
)

data class Button(
    val link: String? = "",
    val text: String? = ""
)

data class Item(
    @SerializedName("custom_fields")
    val customFields: String? = "",
    val id: Int? = 0,
    val link: Link? = Link(),
    val media: SearchMedia? = SearchMedia(),
    @SerializedName("short_description")
    val shortDescription: String? = "",
    @SerializedName("short_title")
    val shortTitle: String? = "",
    val snippet: String? = ""
)

data class Link(
    val id: Int? = 0,
    val type: String? = "",
    val url: String? = ""
)

data class SearchMedia(
    val url: String? = ""
)
