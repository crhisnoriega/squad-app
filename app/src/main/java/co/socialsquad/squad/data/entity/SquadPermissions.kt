package co.socialsquad.squad.data.entity

object SquadPermissions {
    const val CREATION = "CREATION"
    const val CREATION_ACTION = "CREATION.ACTION"
}

fun UserCompany?.hasPermission(permission: String): Boolean {
    return this?.squadPermissions?.let { permission in it } ?: false
}
