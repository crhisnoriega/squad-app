package co.socialsquad.squad.data.repository

import android.net.Uri
import co.socialsquad.squad.data.entity.Audio
import co.socialsquad.squad.data.entity.Cities
import co.socialsquad.squad.data.entity.CompleteProfileRequest
import co.socialsquad.squad.data.entity.Event
import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.data.entity.Neighborhoods
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.entity.RequirementExplanations
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.States
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo
import co.socialsquad.squad.presentation.feature.profile.edit.data.CEPResponse
import co.socialsquad.squad.presentation.feature.profile.edit.data.MediaResponse
import co.socialsquad.squad.presentation.feature.profile.edit.data.UserCompanyDTO
import io.reactivex.Completable
import io.reactivex.Observable

interface ProfileRepository {
    var avatar: Uri?
    fun getStates(): Observable<States>
    fun getCities(stateCode: Long): Observable<Cities>
    fun getNeighborhoods(cityCode: Long): Observable<Neighborhoods>
    fun editUser(pk: Int, userCompany: UserCompany): Observable<UserCompany>
    fun editUserV2(pk: Int, userCompany: UserCompanyDTO): Observable<UserCompany>
    fun uploadAvatar(pk: Int, avatarUri: Uri): Completable
    fun getProfile(): Observable<UserCompany>
    fun getPosts(page: Int): Observable<Feed>
    fun getPosts(pk: Int, page: Int): Observable<Feed>
    fun getProfile(pk: Int): Observable<UserCompany>
    fun getQualification(): Observable<List<Qualification>>
    fun getQualifications(): Observable<List<Qualification>>
    fun postRecommendation(recommendation: RecommendationRequest): Completable
    fun getQualification(pk: Int): Observable<List<Qualification>>
    fun getQualificationRecommendationVideos(pk: Int, page: Int): Observable<Results<Post>>
    fun getQualificationRecommendationEvents(pk: Int, page: Int): Observable<Results<Event>>
    fun getQualificationRecommendationAudio(pk: Int, page: Int): Observable<Results<Audio>>
    fun getQualificationRecommendationLives(pk: Int, page: Int): Observable<Results<Live>>
    fun getQualificationRequirement(pk: Int): Observable<RequirementExplanations>
    fun completeProfile(pk: Int, request: CompleteProfileRequest): Observable<UserCompany>
    fun queryCEP(cep: String): Observable<CEPResponse>
    fun uploadMedia(media: Uri): Observable<MediaResponse>
    fun getBanksData(): Observable<List<BankInfo>>
}
