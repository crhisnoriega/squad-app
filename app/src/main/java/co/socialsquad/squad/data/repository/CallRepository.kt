package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.CallQueueItem
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface CallRepository {
    fun connectQueue(): Observable<CallQueueItem>
    fun connectToEmitterQueue(queueId: String): PublishSubject<CallQueueItem>
    fun clearCurrentCall()
}
