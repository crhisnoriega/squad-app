package co.socialsquad.squad.data.entity

import java.io.Serializable

enum class Recommendation : Serializable { AUDIO, FEED, LIVE }
