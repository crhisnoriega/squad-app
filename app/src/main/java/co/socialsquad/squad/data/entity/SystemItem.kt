package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SystemItem(
    @SerializedName("title") var title: String,
    @SerializedName("image") var imageUrl: String
) : Serializable
