package co.socialsquad.squad.data.repository

import android.content.ContentResolver
import android.net.Uri
import android.webkit.MimeTypeMap
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.entity.Comment
import co.socialsquad.squad.data.entity.End
import co.socialsquad.squad.data.entity.EndLiveRequest
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.data.entity.LiveCategories
import co.socialsquad.squad.data.entity.LiveCommentRequest
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.Participant
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.ScheduledLiveCommentRequest
import co.socialsquad.squad.data.entity.StartLiveRequest
import co.socialsquad.squad.data.entity.Stats
import co.socialsquad.squad.data.local.LocalStorage
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiUpload
import co.socialsquad.squad.data.remote.ProgressRequestBody
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.annotations.NonNull
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import java.io.File
import java.io.IOException
import javax.inject.Inject

class LiveRepositoryImpl @Inject constructor(
    private val api: Api,
    private val apiUpload: ApiUpload,
    private val preferences: Preferences,
    private val connectionFactory: ConnectionFactory,
    private val gson: Gson,
    private val localStorage: LocalStorage,
    private val contentResolver: ContentResolver
) : LiveRepository {

    private var connection: Connection? = null

    override var scheduleImageMedia: Uri?
        get() = preferences.scheduleImageMedia?.let { Uri.parse(it) }
        set(scheduleImageMedia) {
            preferences.scheduleImageMedia = scheduleImageMedia?.toString()
        }

    override var scheduleVideoMedia: Uri?
        get() = preferences.scheduleVideoMedia?.let { Uri.parse(it) }
        set(scheduleVideoMedia) {
            preferences.scheduleVideoMedia = scheduleVideoMedia?.toString()
        }

    override fun connectQueue(queue: String) =
        Observable.create { emitter: ObservableEmitter<QueueItem> ->
            try {
                connectionFactory.apply {
                    username = BuildConfig.RABBITMQ_USER
                    password = BuildConfig.RABBITMQ_PASSWORD
                    host = BuildConfig.RABBITMQ_HOST
                    virtualHost = BuildConfig.RABBITMQ_VIRTUAL_HOST
                    port = BuildConfig.RABBITMQ_PORT
                }
                connection = connectionFactory.newConnection()
                val channel = connection?.createChannel()
                val consumer = Consumer(channel, emitter)
                channel?.basicConsume(queue, true, consumer)
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.onError(e)
            }
        }.onDefaultSchedulers()

    override fun disconnectQueue() {
        connection?.apply { Thread { abort() }.start() }
    }

    override fun comment(pk: Int, liveCommentRequest: LiveCommentRequest) =
        api.postLiveComment(pk, liveCommentRequest).onDefaultSchedulers()

    override fun getLive(pk: Int) = api.getLive(pk).onDefaultSchedulers()

    override fun endLive(pk: Int): Completable =
        api.endLive(pk, EndLiveRequest()).onDefaultSchedulers()

    override fun getLiveCategories(): Observable<LiveCategories> =
        api.getLiveCategories().onDefaultSchedulers()

    override fun createLive(liveCreateRequest: LiveCreateRequest) =
        api.postLive(liveCreateRequest).onDefaultSchedulers()

    override fun joinLive(pk: Int) = api.postLiveJoin(pk).onDefaultSchedulers()

    override fun leaveLive(pk: Int) = api.postLiveLeave(pk).onDefaultSchedulers()

    override fun postLive(pk: Int) = api.postLivePost(pk).onDefaultSchedulers()

    override fun deleteLive(pk: Int) = api.deleteLive(pk).onDefaultSchedulers()

    override var livePk: Int?
        get() = if (preferences.livePk == 0) null else preferences.livePk
        set(value) {
            preferences.livePk = value ?: 0
        }

    private inner class Consumer(
        channel: Channel?,
        private val emitter: ObservableEmitter<QueueItem>
    ) : DefaultConsumer(channel) {

        @Throws(IOException::class)
        override fun handleDelivery(
            consumerTag: String?,
            envelope: Envelope?,
            properties: AMQP.BasicProperties?,
            body: ByteArray?
        ) {
            val json = body?.let { String(it) }

            try {
                gson.fromJson(json, QueueItem::class.java)?.let {
                    deserializeByType(json, it.type)?.let { emitter.onNext(it) }
                }
            } catch (e: JsonParseException) {
                e.printStackTrace()
            }
        }

        private fun deserializeByType(@NonNull json: String?, @NonNull type: String?): QueueItem? {
            return when {
                type.equals(QueueItem.COMMENT, ignoreCase = true) -> gson.fromJson(
                    json,
                    Comment::class.java
                )
                type.equals(QueueItem.PARTICIPANT, ignoreCase = true) -> gson.fromJson(
                    json,
                    Participant::class.java
                )
                type.equals(QueueItem.STATS, ignoreCase = true) -> gson.fromJson(
                    json,
                    Stats::class.java
                )
                type.equals(QueueItem.END, ignoreCase = true) -> gson.fromJson(
                    json,
                    End::class.java
                )
                else -> null
            }
        }
    }

    override fun scheduleMediaUpload(
        pk: Int,
        fileUri: Uri,
        callback: ProgressRequestBody.UploadCallbacks,
        isVideo: Boolean
    ): Completable {
        val file = File(fileUri.path)

        val mediaType = if (fileUri.scheme == ContentResolver.SCHEME_CONTENT) {
            val type = contentResolver.getType(fileUri) ?: return Completable.error(Throwable())
            type.toMediaTypeOrNull()
        } else {
            val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString())
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            mimeType!!.toMediaTypeOrNull()
        }

        val progressRequestBody = ProgressRequestBody(file, mediaType, callback)
        val part = MultipartBody.Part.createFormData("files", file.name, progressRequestBody)

        return apiUpload.postScheduleUpload(
            pk,
            if (isVideo) {
                "video"
            } else {
                "photo"
            },
            part
        ).onDefaultSchedulers()
    }

    override fun startLive(startLiveRequest: StartLiveRequest) =
        api.startLive(startLiveRequest.pk, startLiveRequest).onDefaultSchedulers()

    override fun createPhotoFile() = localStorage.createTemporaryImageFile()

    override fun createVideoFile() = localStorage.createTemporaryVideoFile()

    override fun createGalleryFile(mimeType: String) =
        localStorage.createTemporaryGalleryFile(mimeType)

    override fun deleteCreateFiles() {
        localStorage.deleteTemporaryImageFile()
        localStorage.deleteTemporaryVideoFile()
        localStorage.deleteTemporaryGalleryFile()
    }

    override fun deleteCreatedVideo() {
        localStorage.deleteTemporaryVideoFile()
    }

    override fun confirmScheduledLiveAttendance(pk: Int): Observable<Live> {
        return api.liveConfirmAttendance(pk).onDefaultSchedulers()
    }

    override fun getAttendingUsers(pk: Int, page: Int, type: String) =
        api.getAttendingUsers(pk, page, type).onDefaultSchedulers()

    override fun commentLive(scheduledLiveCommentRequest: ScheduledLiveCommentRequest) =
        api.commentLive(scheduledLiveCommentRequest).onDefaultSchedulers()

    override fun getLiveComments(livePk: Int, page: Int) =
        api.getLiveComments(livePk, page).onDefaultSchedulers()

    override fun deleteLiveComment(pk: Long) = api.deleteLiveComment(pk).onDefaultSchedulers()

    override fun getLives(page: Int, ended: Boolean): Observable<Results<Live>> =
        api.getLives(ended, page).onDefaultSchedulers()

    override fun markMediaViewed(pk: Long) = api.getMediaViewCount(pk).onDefaultSchedulers()

    override fun markLiveRecommendationAsViewed(pk: Long): Completable =
        api.postFeedVisualized(pk).onDefaultSchedulers()
}
