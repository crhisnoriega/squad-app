package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RecognitionProfileModel(
        @SerializedName("recognitions_single_before") val recognitions_single_before: RecognitionData?,
        @SerializedName("recognitions_single_after") val recognitions_single_after: RecognitionData?,
        @SerializedName("recognitions_multiple") val recognitions_multiple: RecognitionData?

        )
