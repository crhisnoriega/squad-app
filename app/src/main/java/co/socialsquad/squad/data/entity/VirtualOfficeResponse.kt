package co.socialsquad.squad.data.entity

data class VirtualOfficeResponse(val response: List<VirtualOffice>) {

    data class VirtualOffice(val pk: Int, val title: String, val order: Int, val tools: List<Tool>) {

        data class Tool(val pk: Int, val title: String, val order: Int, val url: String?, val category: Int)
    }
}
