package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RecipientsRequest(
    @SerializedName("token") var token: String,
    @SerializedName("platform") var platform: String
) {
    companion object {
        const val PLATFORM = "android"
    }
}
