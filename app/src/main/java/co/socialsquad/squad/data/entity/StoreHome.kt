package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class StoreHome(
    @SerializedName("banners") var banners: kotlin.collections.List<Banner>? = null,
    @SerializedName("categories") var categories: kotlin.collections.List<Categories.Category>? = null,
    @SerializedName("lists") var lists: kotlin.collections.List<List>? = null
) {
    data class Banner(
        @SerializedName("list") var list: List? = null,
        @SerializedName("category") var category: Categories.Category? = null,
        @SerializedName("product") var product: Product? = null,
        @SerializedName("media") var media: Media,
        @SerializedName("action") var action: String? = null,
        @SerializedName("pk") var pk: Int = 0
    )

    data class Categories(
        @SerializedName("count") var count: Int = 0,
        @SerializedName("next") var next: String? = null,
        @SerializedName("previous") var previous: String? = null,
        @SerializedName("results") var results: kotlin.collections.List<Category>? = null
    ) {
        data class Category(
            @SerializedName("pk") var pk: Int = 0,
            @SerializedName("name") var name: String,
            @SerializedName("cover") var cover: Media? = null,
            @SerializedName("ico") var icon: Media? = null,
            @SerializedName("has_subcategories") var hasSubcategories: Boolean = false,
            @SerializedName("subcategories") var subCategories: kotlin.collections.List<Category>? = null
        )
    }

    data class List(
        @SerializedName("pk") var pk: Int = 0,
        @SerializedName("title") var title: String,
        @SerializedName("products") var products: kotlin.collections.List<Product>,
        @SerializedName("type") var type: String? = null,
        @SerializedName("order") var order: Int,
        @SerializedName("arrangement") var arrangement: String,
        @SerializedName("media") var media: Media?,
        @SerializedName("caption") var caption: String?
    )
}
