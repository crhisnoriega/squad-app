package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.opportunity.OpportunityActionResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityInformationResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityListResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmission
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetailsResponse
import co.socialsquad.squad.data.remote.Api
import io.reactivex.Single
import javax.inject.Inject

class OpportunityRepositoryImpl @Inject constructor(
    private val api: Api
) : OpportunityRepository {

    override fun opportunityAction(opportunityId: Int): Single<OpportunityActionResponse> {
        return api.opportunityAction(opportunityId)
    }

    override fun opportunitySubmissions(
        opportunityId: Int,
        immobileId: Int,
        objectType: OpportunityObjectType
    ): Single<OpportunityListResponse> {
        return api.opportunitySubmissions(opportunityId, immobileId, objectType.type)
    }

    override fun opportunitySubmissionDetails(
        opportunityId: Int,
        objectId: Int,
        submissionId: Int,
        objectType: OpportunityObjectType
    ): Single<SubmissionDetailsResponse> {

        return api.opportunitySubmissionDetails(
            opportunityId,
            objectId,
            submissionId,
            objectType.type
        )
    }

    override fun createOpportunity(opportunitySubmission: OpportunitySubmission): Single<OpportunitySubmissionResponse> =
        api.createOpportunity(opportunitySubmission)

    override fun opportunityDetails(opportunityId: Int): Single<OpportunityInformationResponse> {
        return api.opportunityInfo(opportunityId)
    }

    override fun opportunityStageDetails(stageId: Int): Single<OpportunityInformationResponse> {
        return api.opportunityStageInfo(stageId)
    }
}
