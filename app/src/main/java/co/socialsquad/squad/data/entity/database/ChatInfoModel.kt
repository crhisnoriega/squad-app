package co.socialsquad.squad.data.entity.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val CHAT_INFO_TABLE_NAME = "chat_info"

@Entity(tableName = CHAT_INFO_TABLE_NAME)
data class ChatInfoModel(
    @PrimaryKey var configId: Long,
    @ColumnInfo(name = "message_unread_count") var unreadCount: Int,
    @ColumnInfo(name = "lastSync") var lastSync: Long
)
