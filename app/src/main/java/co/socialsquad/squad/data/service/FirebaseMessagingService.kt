package co.socialsquad.squad.data.service

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.CallNotification
import co.socialsquad.squad.data.entity.LikeNotification
import co.socialsquad.squad.data.entity.LiveNotification
import co.socialsquad.squad.data.entity.NotificationFirebase
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.entity.RecipientsRequest
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_ACTION_RECEIVE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_ACTION
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_CALL_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_QUEUE_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_TYPE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_USER
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_AUDIO
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_VIDEO
import co.socialsquad.squad.presentation.feature.call.CallRoomActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.splash.SplashActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import dagger.android.AndroidInjection
import java.util.concurrent.ExecutionException
import javax.inject.Inject

private const val LIVE_NOTIFICATION_ID = 1
private const val FEED_NOTIFICATION_ID = 2
private const val AUDIO_NOTIFICATION_ID = 3
private const val SCHEDULED_LIVE_NOTIFICATION_ID = 4

class FirebaseMessagingService : com.google.firebase.messaging.FirebaseMessagingService() {

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var loginRepository: LoginRepository

    @Inject
    lateinit var notificationManager: NotificationManager

    override fun onCreate() = AndroidInjection.inject(this)

    override fun onNewToken(token: String) {
        if (loginRepository.token == null || token == null) return

        loginRepository.registerDevice(RecipientsRequest(token, RecipientsRequest.PLATFORM))
            .onErrorComplete()
            .subscribe()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage == null) return

        val remoteMessageData = remoteMessage.data
        val remoteMessageNotification = remoteMessage.notification
        if (remoteMessageData == null && remoteMessageNotification == null) return

        if (remoteMessageNotification != null) {
            val contentText = remoteMessageNotification.body
            setupNotification(contentText)
        } else if (remoteMessageData.isNotEmpty()) {
            val json = gson.toJson(remoteMessageData)
            val notification = gson.fromJson(json, NotificationFirebase::class.java)

            when (notification.model?.toLowerCase()) {
                Share.Live.name.toLowerCase() -> setupLiveNotification(json)
                Share.ScheduledLive.name.toLowerCase() -> setupScheduledLiveNotification(json)
                Share.Feed.name.toLowerCase() -> setupFeedNotification(json)
                Share.Audio.name.toLowerCase() -> setupAudioNotification(json)
                Share.Call.name.toLowerCase() -> setupCall(json)
                else -> if (notification.message != null) setupNotification(notification.message)
            }
        }
    }

    private fun setupNotification(contentText: String?) {
        val builder = NotificationCompat.Builder(this, getString(R.string.notification_channel_general_id))
        val intent = Intent(this, NavigationActivity::class.java)
        builder.setNotificationInfo(this, getString(R.string.app_name), contentText, intent)
        notificationManager.notificate(builder, LIVE_NOTIFICATION_ID)
    }

    private fun setupFeedNotification(json: String) {
        if (loginRepository.token == null) return

        val feedNotification = gson.fromJson(json, LikeNotification::class.java)

        val builder = NotificationCompat.Builder(this, getString(R.string.notification_channel_general_id))
        val intent = Intent(this, SplashActivity::class.java).apply {
            putExtra(SplashActivity.POST_PK_FROM_NOTIFICATION, feedNotification.feedPk.toInt())
        }
        builder.setNotificationInfo(this, getString(R.string.app_name), feedNotification.message, intent)
        notificationManager.notificate(builder, generateNotificationID(FEED_NOTIFICATION_ID, feedNotification.feedPk.toInt()))
    }

    private fun setupLiveNotification(json: String) {
        if (loginRepository.token == null) return

        val liveNotification = gson.fromJson(json, LiveNotification::class.java)

        val builder = NotificationCompat.Builder(this, getString(R.string.notification_channel_general_id))
        val intent = Intent(this, SplashActivity::class.java).apply {
            putExtra(SplashActivity.LIVE_PK_FROM_NOTIFICATION, liveNotification.pk.toInt())
        }
        builder.setNotificationInfo(this, getString(R.string.app_name), liveNotification.message, intent)
        liveNotification.avatar?.let {
            builder.setLargeIcon(applicationContext, it)
        }
        notificationManager.notificate(builder, generateNotificationID(LIVE_NOTIFICATION_ID, liveNotification.pk.toInt()))
    }

    private fun setupScheduledLiveNotification(json: String) {
        if (loginRepository.token == null) return

        val scheduledLiveNotification = gson.fromJson(json, LiveNotification::class.java)

        val builder = NotificationCompat.Builder(this, getString(R.string.notification_channel_general_id))
        val intent = Intent(this, SplashActivity::class.java).apply {
            putExtra(SplashActivity.SCHEDULED_LIVE_PK_FROM_NOTIFICATION, scheduledLiveNotification.pk.toInt())
        }
        builder.setNotificationInfo(this, getString(R.string.app_name), scheduledLiveNotification.message, intent)
        scheduledLiveNotification.avatar?.let {
            builder.setLargeIcon(applicationContext, it)
        }
        notificationManager.notificate(builder, generateNotificationID(SCHEDULED_LIVE_NOTIFICATION_ID, scheduledLiveNotification.pk.toInt()))
    }

    private fun setupAudioNotification(json: String?) {
        if (loginRepository.token == null) return

        val audioNotification = gson.fromJson(json, LiveNotification::class.java)
        val builder = NotificationCompat.Builder(this, getString(R.string.notification_channel_general_id))
        val intent = Intent(this, SplashActivity::class.java).apply {
            putExtra(SplashActivity.AUDIO_PK_FROM_NOTIFICATION, audioNotification.pk.toInt())
        }
        builder.setNotificationInfo(this, getString(R.string.app_name), audioNotification.message, intent)
        audioNotification.avatar?.let {
            builder.setLargeIcon(applicationContext, it)
        }
        notificationManager.notificate(builder, generateNotificationID(AUDIO_NOTIFICATION_ID, audioNotification.pk.toInt()))
    }

    private fun generateNotificationID(typeID: Int, pk: Int) = (typeID.toString() + pk.toString()).toInt()

    private fun setupCall(json: String) {
        if (loginRepository.token == null) return
        val callNotification = gson.fromJson(json, CallNotification::class.java)
        val dialogIntent = Intent(baseContext, CallRoomActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
            with(callNotification) {
                putExtra(CALL_ROOM_EXTRA_QUEUE_ID, this.chatId)
                putExtra(CALL_ROOM_EXTRA_ACTION, CALL_ROOM_ACTION_RECEIVE)
                putExtra(CALL_ROOM_EXTRA_USER, Profile(authorPk, fullName, "", avatar, "", ""))
                putExtra(CALL_ROOM_EXTRA_CALL_ID, this.uuid)
                if (type.equals("v", ignoreCase = true)) {
                    putExtra(CALL_ROOM_EXTRA_TYPE, CALL_TYPE_VIDEO)
                } else {
                    putExtra(CALL_ROOM_EXTRA_TYPE, CALL_TYPE_AUDIO)
                }
            }
        }
        application.startActivity(dialogIntent)
    }
}

fun NotificationCompat.Builder.setNotificationInfo(context: Context, title: String, content: String?, intent: Intent) {
    setContentTitle(title)
    setContentText(content)
    setStyle(NotificationCompat.BigTextStyle())
    color = ColorUtils.parse(context, R.color.darkjunglegreen_28)
    setSmallIcon(R.drawable.ic_notification_squad)
    setAutoCancel(true)
    val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    setContentIntent(pendingIntent)
}

fun NotificationCompat.Builder.setLargeIcon(context: Context, imageUrl: String) {
    try {
        val bitmap = Glide.with(context)
            .asBitmap()
            .load(imageUrl)
            .circleCrop()
            .submit()
            .get()
        setLargeIcon(bitmap)
    } catch (e: InterruptedException) {
        e.printStackTrace()
    } catch (e: ExecutionException) {
        e.printStackTrace()
    }
}

fun NotificationManager.notificate(builder: NotificationCompat.Builder, id: Int) {
    notify(id, builder.build())
}
