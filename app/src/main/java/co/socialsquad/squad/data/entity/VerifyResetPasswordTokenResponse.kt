package co.socialsquad.squad.data.entity

data class VerifyResetPasswordTokenResponse(val status: String? = null)
