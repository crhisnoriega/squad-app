package co.socialsquad.squad.data.entity

data class EmailsToInvite(var email: String = "", var isInvalidEmail: Boolean = false)
