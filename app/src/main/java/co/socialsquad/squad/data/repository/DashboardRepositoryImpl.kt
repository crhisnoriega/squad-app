package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class DashboardRepositoryImpl @Inject constructor(private val api: Api) : DashboardRepository {
    override fun getDashboard() = api.getChartList().onDefaultSchedulers()
}
