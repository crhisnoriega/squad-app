package co.socialsquad.squad.data.entity

import co.socialsquad.squad.presentation.feature.profile.edit.data.MediaItem
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class User(
    @SerializedName("pk") var pk: Int = 0,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("last_name") var lastName: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("chat_id") var chatId: String? = null,
    @SerializedName("is_active") var isActive: Boolean = false,
    @SerializedName("phones") var phones: List<Phone>? = null,
    @SerializedName("avatar") var avatar: String? = null,
    @SerializedName("is_newsletter") var isNewsletter: Boolean = false,
    @SerializedName("gender") var gender: String? = null,
    @SerializedName("birthday") var birthday: String? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("state") var state: State? = null,
    @SerializedName("city") var city: City? = null,
    @SerializedName("neighborhood") var neighborhood: Neighborhood? = null,
    @SerializedName("street") var street: String? = null,
    @SerializedName("number") var number: String? = null,
    @SerializedName("complement") var complement: String? = null,
    @SerializedName("zipcode") var zipcode: String? = null,
    @SerializedName("contact_email") var contactEmail: String? = null,
    @SerializedName("contact_phone") var contactPhone: String? = null,
    @SerializedName("location_id") var locationId: String? = null,
    @SerializedName("position_id") var positionId: String? = null,
    @SerializedName("locale") var locale: String? = null,
    @SerializedName("timezone") var timezone: String? = null,
    @SerializedName("cover") var cover: String? = null,
    @SerializedName("rg") var rg: String? = null,
    @SerializedName("cpf") var cpf: String? = null,
    @SerializedName("marital_status") var maritalStatus: String? = null,
    @SerializedName("marital_status_display") var maritalStatusDisplay: String? = null,
    @SerializedName("is_perfil_complete") var isPerfilComplete: Boolean = false,

    @SerializedName("address_state") var address_state: String? = null,
    @SerializedName("address_city") var address_city: String? = null,
    @SerializedName("address_neighborhood") var address_neighborhood: String? = null,

    @SerializedName("pix") var pix: String? = null,

    @SerializedName("bank_account_type") var bank_account_type: String? = null,
    @SerializedName("bank_account_name") var bank_account_name: String? = null,
    @SerializedName("bank_account_document") var bank_account_document: String? = null,
    @SerializedName("bank_account_bank") var bank_account_bank: String? = null,
    @SerializedName("bank_account_branch") var bank_account_branch: String? = null,
    @SerializedName("bank_account_number") var bank_account_number: String? = null,
    @SerializedName("bank_account_digit") var bank_account_digit: String? = null,
    @SerializedName("bank_account_savings") var bank_account_savings: Boolean? = null,


    @SerializedName("rg_front_image") var rg_front_image: MediaItem?,
    @SerializedName("rg_back_image") var rg_back_image: MediaItem?


) : Serializable {
    val fullName: String
        get() {
            var fullName = ""
            if (firstName != null) fullName += firstName!! + " "
            if (lastName != null) fullName += lastName
            return fullName
        }
}
