package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class Live(
    @SerializedName("pk") var pk: Int = 0,
    @SerializedName("post_live_pk") var postLivePk: Int = 0,
    @SerializedName("sid") var sid: String,
    @SerializedName("name") var name: String,
    @SerializedName("owner") var owner: Owner,
    @SerializedName("started_at") var startedAt: String? = null,
    @SerializedName("created_at") var createdAt: String,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("active") var active: Boolean = false,
    @SerializedName("category") var category: LiveCategory,
    @SerializedName("content") var content: String? = null,
    @SerializedName("hls_url") var hlsUrl: String,
    @SerializedName("scheduled") var schedule: Scheduled? = null,
    @SerializedName("rtmp_url") var rtmpUrl: String,
    @SerializedName("aspect_ratio_width") val aspectRatioWidth: Int,
    @SerializedName("aspect_ratio_height") val aspectRatioHeight: Int,
    @SerializedName("comment_queue") var commentQueue: String? = null,
    @SerializedName("streaming") var streaming: Boolean = false,
    @SerializedName("medias") var medias: List<Media>? = null,
    @SerializedName("viewer_count") var viewerCount: Int? = null,
    @SerializedName("total_attending_users") var totalAttendingUsers: Int? = null,
    @SerializedName("attending_users_avatars") var attendingUsersAvatars: List<String>? = null,
    @SerializedName("is_logged_user_attending") var isAttending: Boolean? = null,
    @SerializedName("questions_count") var questionsCount: Int = 0,
    @SerializedName("can_edit") var canEdit: Boolean = false,
    @SerializedName("can_delete") var canDelete: Boolean = false,
    @SerializedName("can_share") var canShare: Boolean = false,
    @SerializedName("can_be_recommended") var canRecommend: Boolean = false,
    @SerializedName("visualized") var visualized: Boolean = false,
    @SerializedName("audience_type") var audienceType: String
) : Serializable

class Owner(
    @SerializedName("pk") var pk: Int = 0,
    @SerializedName("user") var user: User,
    @SerializedName("qualification") var qualification: String
) : Serializable

open class QueueItem(@SerializedName("type") val type: String) {
    companion object {
        val COMMENT = "C"
        val PARTICIPANT = "P"
        val STATS = "S"
        val END = "E"
    }
}

class Comment(
    @SerializedName("pk") val pk: Int,
    @SerializedName("text") val text: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("participant") val participant: Participant.Participant,
    @SerializedName("time_display") val timeDisplay: String
) : QueueItem(QueueItem.COMMENT)

class Participant(
    @SerializedName("participant") var participant: Participant
) : QueueItem(QueueItem.PARTICIPANT) {
    inner class Participant(
        @SerializedName("pk") var pk: Int,
        @SerializedName("user_company") var userCompany: UserCompany,
        @SerializedName("active") var active: Boolean = false,
        @SerializedName("time_display") var timeDisplay: String
    )
}

class Stats(
    @SerializedName("participants") var participants: Int,
    @SerializedName("streaming") var streaming: Boolean = false
) : QueueItem(QueueItem.STATS)

class Scheduled(
    @SerializedName("pk") val pk: Int? = null,
    @SerializedName("start_time") val startTime: Date? = null,
    @SerializedName("end_time") val endTime: Date? = null,
    @SerializedName("photo") val photo: Media? = null,
    @SerializedName("video") val video: Media? = null
) : Serializable

class End : QueueItem(QueueItem.END)

class ScheduledRequest(
    @SerializedName("start_time") val startTime: Date? = null,
    @SerializedName("end_time") val end_time: Date? = null
) : Serializable
