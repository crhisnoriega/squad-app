package co.socialsquad.squad.data.entity.integration

import com.google.gson.annotations.SerializedName

class RecruitRequest(
    @SerializedName("subdomain") var subdomain: String? = null,
    @SerializedName("cid") var cid: String? = null
)
