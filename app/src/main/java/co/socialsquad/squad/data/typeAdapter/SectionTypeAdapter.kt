package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SectionTypeAdapter : TypeAdapter<SectionType>() {
    override fun write(writer: JsonWriter?, value: SectionType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(SectionType.getDefault().value)
    }

    override fun read(reader: JsonReader?): SectionType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> SectionType.getByValue(it.nextString())
                else -> SectionType.getDefault()
            }
        } ?: SectionType.getDefault()
    }
}
