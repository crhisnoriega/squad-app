package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.FindProductRequest
import co.socialsquad.squad.data.entity.Product
import co.socialsquad.squad.data.entity.ProductDetailGroup
import co.socialsquad.squad.data.entity.ProductsFilterRequest
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.Store
import co.socialsquad.squad.data.entity.StoreHome
import io.reactivex.Completable
import io.reactivex.Observable

interface StoreRepository {
    fun getStoreHome(): Observable<StoreHome>
    fun getProduct(pk: Int): Observable<Product>
    fun getSubcategories(pk: Int, page: Int): Observable<StoreHome.Categories>
    fun getList(pk: Int, page: Int): Observable<Results<Product>>
    fun getProducts(page: Int): Observable<Results<Product>>
    fun getProducts(productsFilterRequest: ProductsFilterRequest, page: Int = 1): Observable<Results<Product>>
    fun getProductDetailCategories(pk: Int): Observable<Results<ProductDetailGroup>>
    fun getProductLocations(findProductRequest: FindProductRequest): Observable<Results<Store>>
    fun getProductView(pk: Int): Completable
    fun getStores(lat: Double, lng: Double, page: Int): Observable<Results<Store>>
}
