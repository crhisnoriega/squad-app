package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class EventRepositoryImpl @Inject constructor(private val api: Api) : EventRepository {

    override fun getEvents(page: Int) = api.getEvents(page).onDefaultSchedulers()

    override fun getEvent(pk: Long) = api.getEvent(pk).onDefaultSchedulers()

    override fun confirmAttendance(pk: Long) = api.postEventConfirm(pk).onDefaultSchedulers()
}
