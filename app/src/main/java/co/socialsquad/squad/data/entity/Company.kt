package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Company(
    @SerializedName("name") var name: String? = null,
    @SerializedName("subdomain") var subdomain: String? = null,
    @SerializedName("size") var size: String? = null,
    @SerializedName("website") var website: String? = null,
    @SerializedName("segment_id") var segmentId: String? = null,
    @SerializedName("industry_id") var industryId: String? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("state") var state: String? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("department") var department: String? = null,
    @SerializedName("contact_name") var contactName: String? = null,
    @SerializedName("contact_email") var contactEmail: String? = null,
    @SerializedName("position") var position: String? = null,
    @SerializedName("contact_phone") var contactPhone: String? = null,
    @SerializedName("avatar") var avatar: String? = null,
    @SerializedName("logo") var logo: String? = null,
    @SerializedName("business_type_display") var businessTypeDisplay: String? = null,
    @SerializedName("business_type") var businessType: String? = null,
    @SerializedName("primary_color") var primaryColor: String? = null,
    @SerializedName("secondary_color") var secondaryColor: String? = null,
    @SerializedName("text_color") var textColor: String? = null,
    @SerializedName("virtual_office_url") var virtualOfficeUrl: String? = null,
    @SerializedName("web_view") var webView: WebView? = null,
    @SerializedName("explore_audios") var exploreAudios: Boolean? = null,
    @SerializedName("explore_immobile") var exploreImmobile: Boolean? = null,
    @SerializedName("explore_documents") var exploreDocuments: Boolean? = null,
    @SerializedName("explore_events") var exploreEvents: Boolean? = null,
    @SerializedName("explore_videos") var exploreVideos: Boolean? = null,
    @SerializedName("explore_lives") var exploreLives: Boolean? = null,
    @SerializedName("explore_products") var exploreProducts: Boolean? = null,
    @SerializedName("explore_stores") var exploreStores: Boolean? = null,
    @SerializedName("explore_user_companies") var exploreUserCompanies: Boolean? = null,
    @SerializedName("explore_trips") var exploreTrips: Boolean? = null,
    @SerializedName("call_logo_android") var callLogo: String? = null,
    @SerializedName("call_gradient") var callGradient: List<String>? = null,
    @SerializedName("feature_feed") var featureFeed: Boolean? = null,
    @SerializedName("feature_products") var featureProducts: Boolean? = null,
    @SerializedName("feature_prod_avail") var featureProdAvail: Boolean? = null,
    @SerializedName("feature_intranet") var featureIntranet: Boolean? = null,
    @SerializedName("feature_dashboard") var featureDashboard: Boolean? = null,
    @SerializedName("feature_qualification") var featureQualification: Boolean? = null,
    @SerializedName("feature_chat") var featureChat: Boolean? = null,
    @SerializedName("company_url") var companyUrl: String? = null,
    @SerializedName("invitation_type") var invitationType: String? = null,
    @SerializedName("invitation_token") var invitationToken: String? = null,
    @SerializedName("about_text_color") var aboutTextColor: String? = null,
    @SerializedName("about_gradient") var aboutGradient: List<String>? = null,
    @SerializedName("about_footer_background") var aboutFooterBackground: String? = null,
    @SerializedName("is_public") var isPublic: Boolean? = null,
    @SerializedName("description_text") var descriptionText: String? = null,
    @SerializedName("login_token") var loginToken: String? = null
) : Serializable {

    fun isUniqueValue(): Boolean = !(invitationType != null && invitationType == "unique")

    companion object {
        const val DEFAULT_PRIMARY_COLOR = "#282828"
    }
}

enum class BusinessType { fra, mlm }
