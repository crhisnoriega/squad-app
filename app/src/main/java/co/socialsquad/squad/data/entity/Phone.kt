package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class Phone(
    @SerializedName("number") var number: String? = null,
    @SerializedName("category") var category: String? = null
)
