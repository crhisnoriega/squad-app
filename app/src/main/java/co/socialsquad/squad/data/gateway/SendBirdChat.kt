// package co.socialsquad.squad.data.gateway
//
// import co.socialsquad.squad.data.entity.database.ChatMessage
// import co.socialsquad.squad.data.entity.database.ChatMessageStatus
// import co.socialsquad.squad.data.entity.database.ChatRoomModel
// import co.socialsquad.squad.data.repository.CHAT_PAGE_LIMIT
// import co.socialsquad.squad.data.repository.ChatRepository
// import co.socialsquad.squad.data.repository.LoginRepository
// import co.socialsquad.squad.presentation.util.toUTC
// import com.google.firebase.iid.FirebaseInstanceId
// import com.sendbird.android.*
// import io.reactivex.Observable
// import io.reactivex.ObservableEmitter
// import io.reactivex.disposables.CompositeDisposable
// import io.reactivex.schedulers.Schedulers
// import java.util.*
// import javax.inject.Inject
//
// const val SENDBIRD_INJECTION_NAME = "SendBird"
// const val SENDBIRD_CONNECTION_HANDLER = "connection_handler"
// const val CHAT_ROOM_GET_HANDLER = "room_get_handler"
//
// class SendBirdChat @Inject constructor(loginRepository: LoginRepository,
//                   val chatRepository: ChatRepository) : ChatGateway {
//
//    private val currentUserChatId = loginRepository.userCompany?.user?.chatId
//    private val compositeDisposable = CompositeDisposable()
//
//    override fun login(userId: String): Observable<String> {
//        return Observable.create { emitter: ObservableEmitter<String> ->
//            SendBird.connect(userId) { user, e ->
//                if (e != null) {
//                    emitter.onError(e)
//                } else {
//                    emitter.onNext(user.userId)
//                    FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
//                        SendBird.registerPushTokenForCurrentUser(it.token, SendBird.RegisterPushTokenWithStatusHandler { _, eTolken ->
//                            if (eTolken != null) {
//                                return@RegisterPushTokenWithStatusHandler
//                            }
//                        })
//                    }
//                }
//            }
//        }
//    }
//
//    override fun logout(onDisconnected: () -> Unit) {
//        SendBird.disconnect {
//            onDisconnected()
//        }
//    }
//
//    override fun connectToChatChannel(chatRoom: ChatRoomModel): Observable<ChatRoomModel> {
//        return Observable.create { emitter: ObservableEmitter<ChatRoomModel> ->
//            val room = chatRoom
//            val groupChannelGetHandler = GroupChannel.GroupChannelGetHandler { groupChannel, e ->
//                                if (e != null) {
//                    emitter.onError(e)
//                    return@GroupChannelGetHandler
//                }
//                chatChannels[room.channelUrl!!] = groupChannel
//                groupChannel.members?.findLast { it.userId != currentUserChatId }?.let {
//                    room.lastSeen = it.lastSeenAt
//                    room.isOnline = it.connectionStatus == User.ConnectionStatus.ONLINE
//                }
//                val lastMessage = (groupChannel.lastMessage as? UserMessage)
//                lastMessage?.let {
//                    room.lastMessage = it.message
//                    room.elapsedTime = it.createdAt
//                }
//                room.unreadCount = groupChannel.unreadMessageCount
//                emitter.onNext(room)
//            }
//            if (SendBird.getConnectionState() == SendBird.ConnectionState.OPEN) {
//                GroupChannel.getChannel(room.channelUrl, groupChannelGetHandler)
//            } else if (SendBird.getConnectionState() == SendBird.ConnectionState.CLOSED) {
//                SendBird.connect(chatRoom.chatId, SendBird.ConnectHandler { user, e ->
//                    if (e != null) {
//                        return@ConnectHandler
//                    }
//                    GroupChannel.getChannel(room.channelUrl, groupChannelGetHandler)
//                })
//            }
//        }
//    }
//
//    @Throws(Throwable::class)
//    override fun setSenderObservable(channelUrl: String, sendMessageObservable: Observable<String>): Observable<ChatMessage> {
//        if (chatChannels[channelUrl] == null){
//            throw Throwable("You have to connect to chat channel before. Calls connectToChatChannel " +
//                    "and wait for the observable return the first result before call this method")
//        }
//        return Observable.create { emitter: ObservableEmitter<ChatMessage> ->
//            compositeDisposable.add(sendMessageObservable
//                    .observeOn(Schedulers.io())
//                    .subscribeOn(Schedulers.io())
//                    .map { ChatMessage(0, null, it, currentUserChatId!!, channelUrl, Date().toUTC().time, ChatMessageStatus.SENDING.name) }
//                    .subscribe ({
//                        chatChannels[channelUrl]!!.sendUserMessage(it.message) { userMessage, e ->
//                            if (e != null) {
//                                it.status = ChatMessageStatus.ERROR.name
//                            }
//                            it.id = userMessage.messageId
//                            it.status = ChatMessageStatus.READ.name
//                            it.date = userMessage.createdAt
//                            emitter.onNext(it)
//                        }
//                    },{emitter.onError(it)}, {emitter.onComplete()})
//            )
//        }
//    }
//
//    override fun getMessages(): Observable<ChatMessage> {
//        return Observable.create { emitter: ObservableEmitter<ChatMessage> ->
//            SendBird.addChannelHandler(CHAT_ROOM_GET_HANDLER, object : SendBird.ChannelHandler() {
//                override fun onMessageReceived(baseChannel: BaseChannel, message: BaseMessage) {
//                    (message as? UserMessage)?.let {
//                        val chatMessage = ChatMessage(0, it.messageId, it.message, it.sender.userId, message.channelUrl, message.createdAt, ChatMessageStatus.UNREAD.name)
//                        emitter.onNext(chatMessage)
//                    }
//                }
//            })
//        }
//    }
//
//    override fun getOldMessages(channelUrl: String): Observable<ChatMessage> {
//        if (chatChannels[channelUrl] == null){
//            throw Throwable("You have to connect to chat channel before. Calls connectToChatChannel " +
//                    "and wait for the observable return the first result before call this method")
//        }
//        val chatChannel = chatChannels[channelUrl]!!
//        return Observable.create{ emitter: ObservableEmitter<ChatMessage> ->
//            getMessagesFromChannel(chatChannel, emitter)
//        }
//    }
//
//    private fun getMessagesFromChannel(chatChannel: GroupChannel, emitter: ObservableEmitter<ChatMessage>) {
//        val prevMessageListQuery = chatChannel.createPreviousMessageListQuery()
//        prevMessageListQuery?.load(CHAT_PAGE_LIMIT, true) { messages, e ->
//            parseMessages(e, emitter, messages, prevMessageListQuery)
//        }
//    }
//
//    private fun parseMessages(e: SendBirdException?, emitter: ObservableEmitter<ChatMessage>, messages: MutableList<BaseMessage>, prevMessageListQuery: PreviousMessageListQuery) {
//        if (e != null) {
//            emitter.onError(e)
//            return
//        }
//        messages.forEach {
//            val message = it as? UserMessage
//            message?.let {
//                val chatMessage = ChatMessage(0, message.messageId, message.message, message.sender.userId, message.channelUrl, message.createdAt, ChatMessageStatus.UNREAD.name)
//                emitter.onNext(chatMessage)
//            }
//        }
//        if (messages.size == CHAT_PAGE_LIMIT) {
//            prevMessageListQuery.load(CHAT_PAGE_LIMIT, true) { newMessages, newError ->
//                parseMessages(newError, emitter, newMessages, prevMessageListQuery)
//            }
//        } else {
//            emitter.onComplete()
//        }
//    }
//
//    override fun getMessagesFromDate(channelUrl: String, date: Long): Observable<ChatMessage> {
//        if (chatChannels[channelUrl] == null){
//            throw Throwable("You have to connect to chat channel before. Calls connectToChatChannel " +
//                    "and wait for the observable return the first result before call this method")
//        }
//        val chatChannel = chatChannels[channelUrl]!!
//        return Observable.create{ emitter: ObservableEmitter<ChatMessage> ->
//            getMessagesFromDate(chatChannel, date, emitter)
//        }
//    }
//
//    private fun getMessagesFromDate(chatChannel: GroupChannel, date: Long, emitter: ObservableEmitter<ChatMessage>) {
//        chatChannel.getPreviousMessagesByTimestamp(date, false, CHAT_PAGE_LIMIT, true, BaseChannel.MessageTypeFilter.ALL, null) { messages, e ->
//            if (e != null) {
//                emitter.onError(e)
//                return@getPreviousMessagesByTimestamp
//            }
//            messages.forEach {
//                val message = it as? UserMessage
//                message?.let {
//                    val chatMessage = ChatMessage(0, message.messageId, message.message, message.sender.userId, message.channelUrl, message.createdAt, ChatMessageStatus.UNREAD.name)
//                    emitter.onNext(chatMessage)
//                }
//            }
//            if (messages.size == CHAT_PAGE_LIMIT) {
//                getMessagesFromDate(chatChannel, messages.last().createdAt, emitter)
//            } else {
//                emitter.onComplete()
//            }
//        }
//    }
//
//    override fun markAsRead(channelUrl: String){
//        chatChannels[channelUrl]?.markAsRead()
//    }
//
//    override fun getUnreadCount(): Observable<Int> {
//        return Observable.create { emitter: ObservableEmitter<Int> ->
//            SendBird.getTotalUnreadMessageCount(object : GroupChannel.GroupChannelTotalUnreadMessageCountHandler {
//                override fun onResult(messageCount: Int, sendBirdException: SendBirdException?) {
//                    if (sendBirdException != null) {
//                        return
//                    }
//                    emitter.onNext(messageCount)
//                    emitter.onComplete()
//                }
//            })
//        }
//    }
//
//    override fun getUnreadCount(channelUrl: String): Int {
//        return chatChannels[channelUrl]?.unreadMessageCount?:0
//    }
//
//    override fun updateRoom(chatRoomModel: ChatRoomModel): ChatRoomModel {
//        chatChannels[chatRoomModel.channelUrl]?.let {groupChannel ->
//            groupChannel.members?.findLast { it.userId != currentUserChatId }?.let {
//                chatRoomModel.lastSeen = it.lastSeenAt
//                chatRoomModel.isOnline = it.connectionStatus == User.ConnectionStatus.ONLINE
//            }
//            val lastMessage = (groupChannel.lastMessage as? UserMessage)
//            lastMessage?.let {
//                chatRoomModel.lastMessage = it.message
//                chatRoomModel.elapsedTime = it.createdAt
//            }
//            chatRoomModel.unreadCount = groupChannel.unreadMessageCount
//        }
//        return chatRoomModel
//    }
//
//    override fun disconnectRoomAndChannel(channelUrl: String){
//        SendBird.removeChannelHandler(SENDBIRD_CONNECTION_HANDLER)
//        SendBird.removeConnectionHandler(SENDBIRD_CONNECTION_HANDLER)
//        chatChannels.remove(channelUrl)
//    }
//
//    override fun disconnect(){
//        SendBird.disconnect {
//            chatChannels.clear()
//            compositeDisposable.clear()
//            compositeDisposable.dispose()
//        }
//    }
//
//    companion object{
//        private val chatChannels = mutableMapOf<String, GroupChannel>()
//    }
//
// }
