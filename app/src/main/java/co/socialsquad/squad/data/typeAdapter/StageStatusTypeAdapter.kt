package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.StageStatus
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class StageStatusTypeAdapter : TypeAdapter<StageStatus>() {
    override fun write(writer: JsonWriter?, value: StageStatus?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(StageStatus.getDefault().value)
    }

    override fun read(reader: JsonReader?): StageStatus {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> StageStatus.getByValue(it.nextString())
                else -> StageStatus.getDefault()
            }
        } ?: StageStatus.getDefault()
    }
}
