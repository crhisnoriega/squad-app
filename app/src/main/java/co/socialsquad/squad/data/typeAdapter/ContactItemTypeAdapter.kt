package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.explore.ComplementType
import co.socialsquad.squad.domain.model.explore.ContactItemType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class ContactItemTypeAdapter : TypeAdapter<ContactItemType>() {
    override fun write(writer: JsonWriter?, value: ContactItemType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(ComplementType.getDefault().value)
    }

    override fun read(reader: JsonReader?): ContactItemType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> ContactItemType.getByValue(it.nextString())
                else -> ContactItemType.getDefault()
            }
        } ?: ContactItemType.getDefault()
    }
}
