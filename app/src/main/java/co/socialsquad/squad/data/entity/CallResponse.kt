package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class CallResponse(
    @SerializedName("caller")
    val caller: Caller,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("duration")
    val duration: String,
    @SerializedName("receiver")
    val receiver: Caller,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_display")
    val statusDisplay: String
)
