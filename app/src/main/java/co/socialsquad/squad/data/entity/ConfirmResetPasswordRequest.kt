package co.socialsquad.squad.data.entity

data class ConfirmResetPasswordRequest(val token: String, val password: String)
