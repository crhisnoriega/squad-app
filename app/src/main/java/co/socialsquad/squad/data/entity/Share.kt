package co.socialsquad.squad.data.entity

enum class Share { ScheduledLive, Live, Feed, Audio, Product, Event, Call, Immobile }
