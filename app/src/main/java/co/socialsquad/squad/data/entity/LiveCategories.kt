package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class LiveCategories(@SerializedName("results") var categories: List<LiveCategory>)
