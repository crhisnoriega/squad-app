package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.form.FieldType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class FieldTypeAdapter : TypeAdapter<FieldType>() {
    override fun write(writer: JsonWriter?, value: FieldType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(FieldType.getDefault().value)
    }

    override fun read(reader: JsonReader?): FieldType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> FieldType.getByValue(it.nextString())
                else -> FieldType.getDefault()
            }
        } ?: FieldType.getDefault()
    }
}
