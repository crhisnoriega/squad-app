package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class WebView {
    @SerializedName("avatar_disabled")
    var notEnabledAvatar: String? = null
    @SerializedName("avatar_enabled")
    var enabledAvatar: String? = null
}
