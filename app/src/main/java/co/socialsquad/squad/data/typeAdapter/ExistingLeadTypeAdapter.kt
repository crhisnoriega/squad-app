package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class ExistingLeadTypeAdapter : TypeAdapter<ExistingLeadType>() {
    override fun write(writer: JsonWriter?, value: ExistingLeadType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.nullValue()
    }

    override fun read(reader: JsonReader?): ExistingLeadType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> ExistingLeadType.getByValue(it.nextString())
                else -> ExistingLeadType.Invalid
            }
        } ?: ExistingLeadType.Invalid
    }
}
