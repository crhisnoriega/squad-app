package co.socialsquad.squad.data.entity.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val CHAT_ROOM_TABLE_NAME = "chat_response"

@Entity(tableName = CHAT_ROOM_TABLE_NAME)
data class ChatResponse(
    @PrimaryKey val id: Long = 1,
    @ColumnInfo(name = "response") val response: String
)
