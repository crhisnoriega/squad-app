package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class LikeNotification(
    @SerializedName("pk") var feedPk: String,
    @SerializedName("liker_pk") var likerPk: String? = null,
    @SerializedName("message") var message: String? = null
)
