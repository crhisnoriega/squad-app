package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Types(
    @SerializedName("B") var b: String? = null,
    @SerializedName("I") var i: String? = null,
    @SerializedName("R") var r: String? = null,
    @SerializedName("T") var t: String? = null,
    @SerializedName("H") var h: String? = null,
    @SerializedName("S") var s: String? = null,
    @SerializedName("Q") var q: String? = null,
    @SerializedName("A") var a: String? = null
) : Serializable
