package co.socialsquad.squad.data.entity

data class EndLiveRequest(val active: Boolean = false)
