package co.socialsquad.squad.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import co.socialsquad.squad.data.dao.ChatInfoDao
import co.socialsquad.squad.data.dao.ChatRoomDao
import co.socialsquad.squad.data.dao.MessageDao
import co.socialsquad.squad.data.entity.database.ChatInfoModel
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatRoomModel

@Database(entities = [ChatMessage::class, ChatRoomModel::class, ChatInfoModel::class], version = 4, exportSchema = false)
abstract class SquadDataBase : RoomDatabase() {
    abstract fun messageDao(): MessageDao
    abstract fun chatRoomsDao(): ChatRoomDao
    abstract fun chatInfoDao(): ChatInfoDao
}
