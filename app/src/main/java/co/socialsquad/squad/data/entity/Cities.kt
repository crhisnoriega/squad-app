package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class Cities {

    @SerializedName("cities")
    var cities: List<City>? = null
}
