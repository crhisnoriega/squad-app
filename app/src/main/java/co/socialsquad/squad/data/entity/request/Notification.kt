package co.socialsquad.squad.data.entity.request

import co.socialsquad.squad.data.entity.Author
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.data.entity.NotificationCustomData
import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("pk") val pk: Long,
    @SerializedName("message") val message: String? = null,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("template_message") val templateMessage: String? = null,
    @SerializedName("custom_data") val customData: NotificationCustomData? = null,
    @SerializedName("media") val media: Media? = null,
    @SerializedName("author") val author: Author? = null,
    @SerializedName("clicked") var clicked: Boolean = true
)
