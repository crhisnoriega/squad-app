package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Event(
    @SerializedName("all_squad") val allSquad: Boolean? = null,
    @SerializedName("audience") val audience: String? = null,
    @SerializedName("audience_type") val audienceType: String? = null,
    @SerializedName("buy_link") val buyLink: String? = null,
    @SerializedName("category") val category: Category? = null,
    @SerializedName("confirmations_count") val confirmationsCount: Int? = null,
    @SerializedName("confirmed") val confirmed: Boolean? = null,
    @SerializedName("confirmed_users_avatars") val confirmedUsersAvatars: List<String>? = null,
    @SerializedName("content") val content: String? = null,
    @SerializedName("cover") val cover: Media? = null,
    @SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("end_date") val endDate: String? = null,
    @SerializedName("free") val free: Boolean? = null,
    @SerializedName("location") val location: Location? = null,
    @SerializedName("owner") val owner: Owner? = null,
    @SerializedName("pk") val pk: Int,
    @SerializedName("segmentation") val segmentation: Segmentation? = null,
    @SerializedName("short_title") val shortTitle: String? = null,
    @SerializedName("speakers") val speakers: List<Speaker>? = null,
    @SerializedName("start_date") val startDate: String? = null,
    @SerializedName("tickets") val tickets: String? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("video") val video: Video? = null,
    @SerializedName("vip") val vip: List<Vip>? = null,
    @SerializedName("can_share") val canShare: Boolean? = null
)
