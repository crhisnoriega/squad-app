package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LiveCreateRequest(
    @SerializedName("name") val name: String,
    @SerializedName("content") val content: String,
    @SerializedName("category") val category: LiveCategory,
    @SerializedName("segmentation") val segmentation: SegmentationRequest? = null,
    @SerializedName("aspect_ratio_width") var aspectRatioWidth: Int? = null,
    @SerializedName("aspect_ratio_height") var aspectRatioHeight: Int? = null,
    @SerializedName("scheduled") var schedule: ScheduledRequest? = null
) : Serializable
