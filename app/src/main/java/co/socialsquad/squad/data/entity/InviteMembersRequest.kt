package co.socialsquad.squad.data.entity

data class InviteMembersRequest(val email: String)
