package co.socialsquad.squad.data.entity

data class CreateUserResponse(val status: String? = null)
