package co.socialsquad.squad.data.entity

import android.os.Parcelable
import co.socialsquad.squad.presentation.custom.ViewMediaType
import co.socialsquad.squad.presentation.custom.multimedia.MultiMediaDataType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Media(
        @SerializedName("pk") var pk: Int?,
        @SerializedName("file") var file: String?,
        @SerializedName("mime_type") var mimeType: String,
        @SerializedName("url") var url: String,
        @SerializedName("streamings") var streamings: List<Streaming>? = null,
        @SerializedName("width") var width: Int = 0,
        @SerializedName("height") var height: Int = 0,
        @SerializedName("resized_url") var resizedUrl: String? = null,
        @SerializedName("thumbnail") var thumbnail: String? = null,
        @SerializedName("time_length") var timeLength: Long = 0,
        @SerializedName("views_count") var viewsCount: Long = 0,
        @SerializedName("visualized") var visualized: Boolean? = false,
        @SerializedName("progressive") var progressive: Boolean? = null,
        @SerializedName("ord") var ord: Int? = null,
        var currentPosition: Long = 0,
        var isViewHolderVisible: Boolean = false
) : Parcelable, Serializable, ViewMediaType {

    companion object {
        const val TYPE_VIDEO = "video"
        const val TYPE_IMAGE = "image"
        const val TYPE_AUDIO = "audio"
    }

    override fun getViewType(): Int {
        if (isVideoType())
            return MultiMediaDataType.VIDEO.value
        if (isImageType())
            return MultiMediaDataType.IMAGE.value
        if (isAudioType())
            return MultiMediaDataType.AUDIO.value
        return MultiMediaDataType.NOT_SUPORTED.value
    }

    override fun isVideoType(): Boolean {
        return mimeType?.contains(TYPE_VIDEO, true) && streamings?.let { streaming ->
            streaming.isNotEmpty() && streaming[0].thumbnailUrl != null && streaming[0].playlistUrl != null
        }!!
    }

    override fun isImageType(): Boolean {
        mimeType?.let {
            return it.contains(TYPE_IMAGE, true)
        }
        return false
    }

    override fun isAudioType(): Boolean {
        mimeType?.let {
            return it.contains(TYPE_AUDIO, true)
        }
        return false
    }
}
