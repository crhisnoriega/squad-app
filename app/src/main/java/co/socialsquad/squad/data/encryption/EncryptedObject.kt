package co.socialsquad.squad.data.encryption

data class EncryptedObject(val iv: ByteArray, val encryptedMessage: String) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EncryptedObject

        if (!iv.contentEquals(other.iv)) return false
        if (encryptedMessage != other.encryptedMessage) return false

        return true
    }

    override fun hashCode(): Int {
        var result = iv.contentHashCode()
        result = 31 * result + encryptedMessage.hashCode()
        return result
    }
}
