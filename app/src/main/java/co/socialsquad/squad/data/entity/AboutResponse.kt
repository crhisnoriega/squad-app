package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class AboutResponse(
    @SerializedName("introduction") val description_text: String,
    val footer_text: String,
    val media: Media,
    val company: Company,
    val topics: List<Topics>
) {

    data class Topics(
        val pk: String,
        val title: String,
        val description: String,
        val items: List<Item>
    ) {

        data class Item(
            val pk: String,
            val title: String,
            val icon: String,
            val description: String
        )
    }
}
