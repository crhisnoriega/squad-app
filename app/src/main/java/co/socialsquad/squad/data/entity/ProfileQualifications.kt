package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class ProfileQualifications(
    @SerializedName("results")
    val qualifications: List<Qualification>
)
