package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.MembersRequest
import co.socialsquad.squad.data.entity.SearchMembersRequest
import co.socialsquad.squad.data.entity.SegmentationReachRequest
import co.socialsquad.squad.data.entity.SegmentationSearchRequest
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class SegmentRepositoryImpl @Inject constructor(private val api: Api) : SegmentRepository {
    override fun getTypes() =
        api.getAudienceTypes().onDefaultSchedulers()

    override fun getMembers(groupPk: Int, page: Int, membersRequest: MembersRequest) =
        api.getMembers(groupPk, page, membersRequest).onDefaultSchedulers()

    override fun getMetrics() =
        api.getSegmentationAvailable().onDefaultSchedulers()

    override fun getValues(field: String, page: Int) =
        api.getSegmentationAvailableRange(field, page).onDefaultSchedulers()

    override fun getSegmentationMembers(page: Int) =
        api.getSegmentationMembers(page).onDefaultSchedulers()

    override fun postSearchSegmentationMembers(page: Int, searchMembersRequest: SearchMembersRequest) =
        api.postSegmentationSearchMembers(page, searchMembersRequest).onDefaultSchedulers()

    override fun getValuesFiltered(segmentationSearchRequest: SegmentationSearchRequest, page: Int) =
        api.getSegmentationSearchValues(segmentationSearchRequest, page).onDefaultSchedulers()

    override fun getPotentialReach(segmentationReachRequest: SegmentationReachRequest) =
        api.postSegmentationReach(segmentationReachRequest).onDefaultSchedulers()
}
