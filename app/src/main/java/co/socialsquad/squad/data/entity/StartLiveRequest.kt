package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class StartLiveRequest(
    @SerializedName("pk") val pk: Int,
    @SerializedName("aspect_ratio_height") val height: Int,
    @SerializedName("aspect_ratio_width") val width: Int
) : Serializable
