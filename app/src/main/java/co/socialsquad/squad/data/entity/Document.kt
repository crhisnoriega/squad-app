package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Document(
    @SerializedName("pk") val pk: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("owner") val owner: Owner? = null,
    @SerializedName("file") val file: String? = null,
    @SerializedName("content") val content: String? = null,
    @SerializedName("cover") val cover: Media? = null,
    @SerializedName("created_at") val created_at: String? = null,
    @SerializedName("updated_at") val updated_at: String? = null,
    @SerializedName("audience") val audience: String? = null,
    @SerializedName("audience_type") val audience_type: String? = null,
    @SerializedName("visualizations") val visualizations: Int? = null
)
