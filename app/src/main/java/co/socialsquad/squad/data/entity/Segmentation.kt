package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Segmentation(
    @SerializedName("members") val members: List<UserCompany>? = null,
    @SerializedName("queries") val queries: List<Query>? = null,
    @SerializedName("downline") val downline: Boolean
) : Serializable

data class Query(
    @SerializedName("field") val field: String,
    @SerializedName("display_name") val displayName: String,
    @SerializedName("values") val values: List<Value>
) : Serializable

data class SegmentationPotentialReach(
    @SerializedName("potential_reach") val potentialReach: Int,
    @SerializedName("total") val total: Int,
    @SerializedName("bars") val bars: Int
)

data class SegmentationRequest(
    @SerializedName("members") val members: List<Int>? = null,
    @SerializedName("queries") val queries: List<SegmentationQueryRequest>? = null,
    @SerializedName("downline") val downline: Boolean = false
) : Serializable

data class SegmentationQueryRequest(
    @SerializedName("field") val field: String,
    @SerializedName("values") val values: List<String>
) : Serializable

data class SegmentationSearchRequest(
    @SerializedName("field") var field: String? = null,
    @SerializedName("value") val value: String
)

data class SegmentationCreateRequest(
    @SerializedName("name") val name: String,
    @SerializedName("text") val text: String,
    @SerializedName("content") val content: String,
    @SerializedName("queries") val queries: List<SegmentationQueryRequest>
)

data class SegmentationReachRequest(
    @SerializedName("queries") val queries: List<SegmentationQueryRequest>
)
