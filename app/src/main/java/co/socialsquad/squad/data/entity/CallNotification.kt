package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class CallNotification(
    @SerializedName("author_pk") val authorPk: Int,
    @SerializedName("message") val message: String,
    @SerializedName("pk") val pk: Long,
    @SerializedName("avatar") val avatar: String?,
    @SerializedName("chat_id") val chatId: String,
    @SerializedName("full_name") val fullName: String?,
    @SerializedName("key") val uuid: String,
    @SerializedName("type") val type: String?
)
