package co.socialsquad.squad.data.entity.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val CHAT_MESSAGES_TABLE_NAME = "chat_messages"

@Entity(tableName = CHAT_MESSAGES_TABLE_NAME)
data class ChatMessage(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "message_id") var id: String,
    @ColumnInfo(name = "message") var message: String,
    @ColumnInfo(name = "owner_id") var ownerId: String,
    @ColumnInfo(name = "channel_url") var channelUrl: String,
    @ColumnInfo(name = "date") var date: Long,
    @ColumnInfo(name = "status") var status: String
)

enum class ChatMessageStatus { SENDING, ERROR, UNREAD, READ }
