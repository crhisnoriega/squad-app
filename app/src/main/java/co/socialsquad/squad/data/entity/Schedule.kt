package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

data class Schedule(
    @SerializedName("start_time") val startTime: Date? = null,
    @SerializedName("end_time") val query: Date? = null,
    @SerializedName("photo") val photo: Media? = null,
    @SerializedName("video") val video: Media? = null,
    @SerializedName("pk") val pk: Int? = null
) : Serializable
