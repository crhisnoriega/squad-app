package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.ChartList
import io.reactivex.Observable

interface DashboardRepository {

    fun getDashboard(): Observable<ChartList>
}
