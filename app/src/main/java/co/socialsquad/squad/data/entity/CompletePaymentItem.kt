package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class CompletePaymentItem(
    @SerializedName("visible") var visible: Boolean?,
    @SerializedName("completed") var completed: Boolean?,
    @SerializedName("method") var method: String?

):Parcelable