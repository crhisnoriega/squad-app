package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RequirementExplanations(
    @SerializedName("feed") val feed: Post,
    @SerializedName("hints") val hints: List<Explanation>
)

data class Explanation(
    @SerializedName("pk") val pk: Int,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("order") val order: Int
)
