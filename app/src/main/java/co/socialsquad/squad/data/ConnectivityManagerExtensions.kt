package co.socialsquad.squad.data

import android.net.ConnectivityManager

fun ConnectivityManager.isConnected() = (activeNetworkInfo != null && activeNetworkInfo.isConnected)
