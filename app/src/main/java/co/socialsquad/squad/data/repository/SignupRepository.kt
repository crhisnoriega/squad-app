package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.CreateUserResponse
import co.socialsquad.squad.data.entity.CustomFieldRequest
import co.socialsquad.squad.data.entity.RegisterIntegration
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.entity.SystemList
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.UserCompanyCID
import co.socialsquad.squad.data.entity.UserCreateRequest
import co.socialsquad.squad.data.entity.integration.RecruitRequest
import io.reactivex.Completable
import io.reactivex.Observable

interface SignupRepository {
    var email: String?
    fun createUser(userCreateRequest: UserCreateRequest): Observable<CreateUserResponse>
    fun registerIntegration(registerIntegrationRequest: RegisterIntegrationRequest): Observable<RegisterIntegration>
    fun getRecruit(recruitRequest: RecruitRequest): Observable<UserCompany>
    fun editUserCID(pk: Int, cid: UserCompanyCID): Observable<UserCompany>
    fun setCustomField(field: String, customFieldRequest: CustomFieldRequest): Completable
    fun getSystemList(): Observable<SystemList>
}
