package co.socialsquad.squad.data.entity.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val CHAT_ROOMS_TABLE_NAME = "chat_rooms_table_name"

@Entity(tableName = CHAT_ROOMS_TABLE_NAME)
data class ChatRoomModel(
    @PrimaryKey var pk: Int = 0,
    @ColumnInfo(name = "channelUrl") var channelUrl: String? = null,
    @ColumnInfo(name = "avatarUrl") var avatarUrl: String? = null,
    @ColumnInfo(name = "can_call") var canCall: Boolean = false,
    @ColumnInfo(name = "name") var name: String? = null,
    @ColumnInfo(name = "qualification") var qualification: String? = null,
    @ColumnInfo(name = "qualificationBadge") var qualificationBadge: String? = null,
    @ColumnInfo(name = "chatId") var chatId: String? = null,
    @ColumnInfo(name = "lastMessage") var lastMessage: String? = null,
    @ColumnInfo(name = "elapsedTime") var elapsedTime: Long? = null,
    @ColumnInfo(name = "unreadCount") var unreadCount: Int = 0,
    @ColumnInfo(name = "isOnline") var isOnline: Boolean = false,
    @ColumnInfo(name = "lastSeen") var lastSeen: Long? = null,
    @ColumnInfo(name = "section") var section: String,
    @ColumnInfo(name = "section_order") val sectionOrder: Int
) : Serializable
