package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class MagicLinkResumeRequest(
    @SerializedName("token") val token: String
)
