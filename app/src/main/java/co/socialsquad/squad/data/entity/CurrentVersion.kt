package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class CurrentVersion(

    @SerializedName("current_version")
    val currentVersion: String? = null,

    @SerializedName("platform")
    val platform: String? = null
)
