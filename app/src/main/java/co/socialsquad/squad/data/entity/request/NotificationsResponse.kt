package co.socialsquad.squad.data.entity.request

import com.google.gson.annotations.SerializedName

data class NotificationsResponse(
    @SerializedName("count") var count: Int,
    @SerializedName("next") var next: String? = null,
    @SerializedName("results") var results: List<Notification>? = null
)
