package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.CurrentVersion
import io.reactivex.Observable

interface PlatformRepository {
    fun getVersion(): Observable<CurrentVersion>
}
