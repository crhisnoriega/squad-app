@file:Suppress("DEPRECATION")

package co.socialsquad.squad.data.encryption

import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.annotation.RequiresApi
import java.math.BigInteger
import java.security.Key
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.PrivateKey
import java.util.Calendar
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.GCMParameterSpec
import javax.inject.Inject
import javax.security.auth.x500.X500Principal

class SimpleWordEncrypt @Inject constructor(private val context: Context) {

    private val keyStoreType = "AndroidKeyStore"
    private val keyParameterAlias = "SquadIntegrationUser"
    private val aesDecryptionMode = "AES/GCM/NoPadding"
    private var transformationAsymmetric = "RSA/ECB/PKCS1Padding"

    init {
        generateKey()
    }

    private fun generateKey() {
        if (!hasStoredKey()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                generateKeyM()
            } else {
                generateKeyPreM()
            }
        }
    }

    private fun hasStoredKey(): Boolean {
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null)
        return keyStore.containsAlias(keyParameterAlias)
    }

    fun removeAndroidKeyStoreKey() = try {
        KeyStore.getInstance(keyStoreType).deleteEntry(keyParameterAlias)
    } catch (t: Throwable) {
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun generateKeyM() {
        val kpg = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES, keyStoreType
        )
        kpg.init(
                KeyGenParameterSpec.Builder(
                        keyParameterAlias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                        .build()
        )
        kpg.generateKey()
    }

    private fun generateKeyPreM() {
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.YEAR, 20)

        val builder = KeyPairGeneratorSpec.Builder(context)
                .setAlias(keyParameterAlias)
                .setSerialNumber(BigInteger.ONE)
                .setSubject(X500Principal("CN=$keyParameterAlias CA Certificate"))
                .setStartDate(startDate.time)
                .setEndDate(endDate.time)

        try {
            val kpg = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore")
            kpg.initialize(builder.build())
            kpg.genKeyPair()
        } catch (e: Exception) {
            val kpg = KeyPairGenerator.getInstance("RSA", "AndroidKeyStoreBCWorkaround")
            kpg.initialize(builder.build())
            kpg.genKeyPair()
        }
    }

    private fun getSecretKey(): Key? {
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null)
        return keyStore.getKey(keyParameterAlias, null)
    }

    private fun getAndroidKeyStoreAsymmetricKeyPair(): KeyPair? {
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        val privateKey = keyStore.getKey(keyParameterAlias, null) as PrivateKey?
        val publicKey = keyStore.getCertificate(keyParameterAlias)?.publicKey

        return if (privateKey != null && publicKey != null) {
            KeyPair(publicKey, privateKey)
        } else {
            null
        }
    }

    fun encrypt(string: String): EncryptedObject? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptDataM(string)
        } else {
            encryptData(string)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun encryptDataM(string: String): EncryptedObject? {
        val c = Cipher.getInstance(aesDecryptionMode)
        c.init(Cipher.ENCRYPT_MODE, getSecretKey())
        val encodedBytes = c.doFinal(string.toByteArray())
        val encryption = Base64.encodeToString(encodedBytes, Base64.DEFAULT)
        return encryption?.let { EncryptedObject(c.iv, encryption) }
    }

    private fun encryptData(string: String): EncryptedObject? {
        val cipher: Cipher = Cipher.getInstance(transformationAsymmetric)
        cipher.provider.name
        cipher.init(Cipher.ENCRYPT_MODE, getAndroidKeyStoreAsymmetricKeyPair()?.public)
        val bytes = cipher.doFinal(string.toByteArray())
        return EncryptedObject(byteArrayOf(0), Base64.encodeToString(bytes, Base64.DEFAULT))
    }

    fun decrypt(encryptedObject: EncryptedObject): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decryptDataM(encryptedObject)
        } else {
            decryptData(encryptedObject.encryptedMessage)
        }
    }

    private fun decryptDataM(encryptedObject: EncryptedObject): String {
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null)
        val secretKeyEntry = keyStore.getEntry(keyParameterAlias, null) as KeyStore.SecretKeyEntry
        val key = secretKeyEntry.secretKey

        val cipher = Cipher.getInstance(aesDecryptionMode)
        val spec = GCMParameterSpec(128, encryptedObject.iv)
        cipher.init(Cipher.DECRYPT_MODE, key, spec)
        val message = Base64.decode(encryptedObject.encryptedMessage, Base64.DEFAULT)
        return String(cipher.doFinal(message))
    }

    private fun decryptData(message: String): String {
        val cipher: Cipher = Cipher.getInstance(transformationAsymmetric)
        cipher.provider.name
        cipher.init(Cipher.DECRYPT_MODE, getAndroidKeyStoreAsymmetricKeyPair()?.private)
        val encryptedData = Base64.decode(message, Base64.DEFAULT)
        val decodedData = cipher.doFinal(encryptedData)
        return String(decodedData)
    }
}
