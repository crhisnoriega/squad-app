package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.TermsResponse
import io.reactivex.Observable

interface TermsRepository {
    fun terms(): Observable<TermsResponse>
}
