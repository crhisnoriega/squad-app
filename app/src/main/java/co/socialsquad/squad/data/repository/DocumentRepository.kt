package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.Document
import co.socialsquad.squad.data.entity.Results
import io.reactivex.Completable
import io.reactivex.Observable

interface DocumentRepository {
    fun getDocuments(): Observable<Results<Document>>
    fun markDocumentViewed(pk: Int): Completable
}
