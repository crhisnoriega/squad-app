package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class LiveNotification(
    @SerializedName("pk") var pk: String,
    @SerializedName("avatar") var avatar: String? = null,
    @SerializedName("message") var message: String? = null
)
