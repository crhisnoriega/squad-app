package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class Neighborhood(
    @SerializedName("code") var code: Long,
    @SerializedName("name") var name: String? = null
)
