package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedbackAnswerQuestion(
    @SerializedName("pk") val pk: Int,
    @SerializedName("values") val values: List<String>
)
