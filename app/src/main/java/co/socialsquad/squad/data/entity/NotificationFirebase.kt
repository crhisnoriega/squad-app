package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class NotificationFirebase(
    @SerializedName("model") var model: String? = null,
    @SerializedName("message") var message: String? = null
)
