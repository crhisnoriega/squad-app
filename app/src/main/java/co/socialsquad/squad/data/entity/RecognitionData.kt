package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecognitionData(
        @SerializedName("multiple_plans") val multiple_plans: Boolean?,
        @SerializedName("title") val title: String?,
        @SerializedName("subtitle") val subtitle: String?,
        @SerializedName("user_in_recognitions") val user_in_recognitions: Boolean?,
        @SerializedName("plan_id") val plan_id: String?,
        @SerializedName("current_user_level") val current_user_level: String?,
        @SerializedName("icon") val icon: String?
) : Parcelable
