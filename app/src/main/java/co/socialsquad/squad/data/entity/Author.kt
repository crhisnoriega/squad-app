package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Author(
    @SerializedName("pk") var pk: Int,
    @SerializedName("user") var user: User,
    @SerializedName("qualification") var qualification: String?
) : Serializable
