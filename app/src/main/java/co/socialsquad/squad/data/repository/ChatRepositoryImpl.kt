package co.socialsquad.squad.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import co.socialsquad.squad.data.dao.ChatInfoDao
import co.socialsquad.squad.data.dao.ChatRoomDao
import co.socialsquad.squad.data.dao.MessageDao
import co.socialsquad.squad.data.entity.ChatRoom
import co.socialsquad.squad.data.entity.ChatSearchRequest
import co.socialsquad.squad.data.entity.database.ChatInfoModel
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatMessageStatus
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_LOAD_MESSAGES_FROM_DATE
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_MARK_AS_READ
import co.socialsquad.squad.data.service.CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA
import co.socialsquad.squad.data.service.CHAT_SERVICE_ROOM_URL_EXTRA
import co.socialsquad.squad.data.service.ChatService
import co.socialsquad.squad.presentation.feature.chat.MessageViewModel
import co.socialsquad.squad.presentation.feature.chat.MyMessageViewModel
import co.socialsquad.squad.presentation.feature.chat.OtherPersonMessageViewModel
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.Date
import javax.inject.Inject

const val CHAT_PAGE_LIMIT = 50

class ChatRepositoryImpl @Inject constructor(
    private val api: Api,
    private val messageDao: MessageDao,
    private val chatRoomDao: ChatRoomDao,
    private val chatInfoDao: ChatInfoDao,
    loginRepository: LoginRepository
) : ChatRepository {

    private val loggedUser = loginRepository.userCompany?.user?.chatId

    override fun getChatList() = api.getChatList().onDefaultSchedulers()
    override fun createChatRoom(pk: Int): Observable<ChatRoom> = api.postChatRoom(pk).onDefaultSchedulers()
    override fun search(chatSearchRequest: ChatSearchRequest) = api.postChat(chatSearchRequest).onDefaultSchedulers()
    override fun getMessagesFromChatRoom(context: Context, channelUrl: String) = LivePagedListBuilder(
        messageDao.getMessages(channelUrl).mapByPage {
            it.map { chatMessage ->
                if (chatMessage.ownerId == loggedUser) {
                    val id = "1" + chatMessage.date.toString()
                    MyMessageViewModel(id.toLong(), chatMessage.date, chatMessage.message, chatMessage.status == ChatMessageStatus.READ.name)
                } else {
                    val id = "2" + chatMessage.date.toString()
                    OtherPersonMessageViewModel(id.toLong(), chatMessage.date, chatMessage.message, chatMessage.status == ChatMessageStatus.READ.name)
                }
            }
        },
        CHAT_PAGE_LIMIT
    ).setBoundaryCallback(MessageBoundaryCallback(channelUrl, context)).build()

    override fun getLastStoredMessages() =
        messageDao.getLastMessages().map {
            it.distinctBy { it.channelUrl }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    override fun getLastMessage(channelUrl: String) = messageDao.getLastMessages(channelUrl)

    override fun getFirstMessage(channelUrl: String) = messageDao.getFirstMessages(channelUrl)

    override fun hasMessageStored(channelUrl: String): Maybe<Boolean> = messageDao.getMessageCount(channelUrl).map { it > 0 }

    @SuppressLint("CheckResult")
    override fun storeMessage(chatMessage: ChatMessage) {
        Observable.just(chatMessage)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { message ->
                    messageDao.insert(message)
                },
                { it.printStackTrace() }
            )
    }

    @SuppressLint("CheckResult")
    override fun updateStoredMessage(chatMessage: ChatMessage) {
        Observable.just(chatMessage)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { message ->
                    messageDao.updateById(message.id, message.message, message.ownerId, message.channelUrl, message.date, message.status)
                },
                { it.printStackTrace() }
            )
    }

    override fun updateStoredMessageByDate(chatMessage: ChatMessage, dataBaseDate: Long) {
        Observable.just(chatMessage)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .map {
                messageDao.updateByDateAndOwner(chatMessage.id, chatMessage.message, chatMessage.ownerId, chatMessage.channelUrl, chatMessage.date, chatMessage.status, dataBaseDate)
            }
            .subscribe()
    }

    override fun getLiveDataRooms() = chatRoomDao.getStoredRoomsAsLiveData()

    override fun getChatRoom() = chatRoomDao.getStoredRooms()

    override fun getChatRoom(pk: Int): Maybe<ChatRoomModel> = chatRoomDao.getStoredRoom(pk)

    override fun getChatRoomLiveData(pk: Int) = chatRoomDao.getStoredRoomAsLiveData(pk)

    override fun getChatRoom(channelUrl: String): Maybe<ChatRoomModel> = chatRoomDao.getStoredRoom(channelUrl)

    override fun storeChatRoom(chatRooms: List<ChatRoomModel>) = chatRoomDao.insert(*chatRooms.toTypedArray())

    override fun storeChatRoom(chatRooms: ChatRoomModel) = chatRoomDao.insert(chatRooms)

    override fun updateLastMessage(channelUrl: String, message: String, unreadCount: Int, elapsedTime: Long) {
        chatRoomDao.updateLastMessage(channelUrl, message, unreadCount, elapsedTime)
    }

    override fun updateUnreadCount(unreadCount: Int) {
        Log.e("UPDATE", unreadCount.toString())
        return chatInfoDao.insertInfo(ChatInfoModel(1, unreadCount, Date().time))
    }

    override fun getTotalMessagesUnreadCount() = messageDao.getUnreadMessageCount()

    override fun getMessagesUnreadCount(channelUrl: String) = messageDao.getUnreadMessageCount(channelUrl)

    override fun getChatInfoLiveData() = chatInfoDao.getChatInfoLiveData(1)

    override fun getLastSync() = Observable.create<Long> { emitter ->
        try {
            emitter.onNext(chatInfoDao.getLastSync(1) ?: -1)
        } catch (t: Throwable) {
            emitter.onNext(-1)
        }
    }

    override fun markStoredMessagesAsRead(channelUrl: String) {
        return messageDao.updateStatusToRead(channelUrl)
    }

    class MessageBoundaryCallback(private val channelUrl: String, private val context: Context) : PagedList.BoundaryCallback<MessageViewModel>() {

        override fun onZeroItemsLoaded() {}

        override fun onItemAtEndLoaded(itemAtEnd: MessageViewModel) {
            val intent = Intent(context, ChatService::class.java).apply {
                action = CHAT_SERVICE_ACTION_LOAD_MESSAGES_FROM_DATE
                putExtra(CHAT_SERVICE_ROOM_URL_EXTRA, channelUrl)
                putExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA, itemAtEnd.date)
            }
            context.startService(intent)
        }

        override fun onItemAtFrontLoaded(itemAtFront: MessageViewModel) {
            Log.e("onItemAtFront", itemAtFront.read.toString())
            if (!itemAtFront.read) {
                val intent = Intent(context, ChatService::class.java).apply {
                    action = CHAT_SERVICE_ACTION_MARK_AS_READ
                    putExtra(CHAT_SERVICE_ROOM_URL_EXTRA, channelUrl)
                    putExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA, itemAtFront.date)
                }
                context.startService(intent)
            }
        }
    }
}
