package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class SuccessStories(
    @SerializedName("pk") val pk: Long,
    @SerializedName("author") val author: Author,
    @SerializedName("content") val content: String?,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("cover") val cover: Media?,
    @SerializedName("medias") val medias: List<Media>?,
    @SerializedName("title") val title: String?,
    @SerializedName("type") val type: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("visualizations") val visualizations: Long
)
