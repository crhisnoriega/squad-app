package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CompleteYourProfile(
    @SerializedName("profile") var profile: CompleteProfileItem?,
    @SerializedName("payment") var payment: CompletePaymentItem?,
    @SerializedName("context") var context: CompleteContextItem?,
    @SerializedName("completed") var completed: Boolean?,

)