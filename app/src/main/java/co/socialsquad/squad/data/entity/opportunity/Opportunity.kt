package co.socialsquad.squad.data.entity.opportunity

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

enum class OpportunityObjectType(val type: String) {
    REAL_STATE("immobile")
}

@Parcelize
data class Opportunity(
    @SerializedName("id")
    val id: Int,

    @SerializedName("title")
    val title: String,

    @SerializedName("submission_list")
    var opportunityList: OpportunityList,
    var parentObjectId: Int
) : Parcelable

@Parcelize
data class OpportunityActionResponse(
    @SerializedName("opportunity_action")
    val opportunityAction: OpportunityAction
) : Parcelable

@Parcelize
data class OpportunityAction(
    @SerializedName("button")
    val button: Button,

    @SerializedName("form")
    val form: Form
) : Parcelable

@Parcelize
data class Button(
    @SerializedName("icon")
    val icon: Media,

    @SerializedName("name")
    val name: String
) : Parcelable

@Parcelize
data class Form(

    @SerializedName("success")
    val success: Success,

    @SerializedName("sections")
    val sections: List<Section>

) : Parcelable

@Parcelize
data class Success(

    @SerializedName("icon")
    val icon: Media?,

    @SerializedName("title")
    val title: String,

    @SerializedName("subtitle")
    val subtitle: String,

    @SerializedName("another_submission_button")
    val anotherSubmissionButton: String
) : Parcelable

@Parcelize
data class Section(

    @SerializedName("order")
    val order: Int,

    @SerializedName("header")
    val header: Header,

    @SerializedName("fields")
    val fields: List<Field>
) : Parcelable

@Parcelize
data class Header(
    @SerializedName("icon")
    val icon: Media?,
    @SerializedName("media")
    val media: Media?,
    @SerializedName("title")
    val title: String,
    @SerializedName("subtitle")
    val subtitle: String?,
    @SerializedName("description")
    val description: String?
) : Parcelable

@Parcelize
data class Field(

    @SerializedName("order")
    val order: Int,

    @SerializedName("id")
    val id: Int,

    @SerializedName("label")
    val label: String,

    @SerializedName("capital_letter")
    val capitalLetter: Boolean,

    @SerializedName("text_helper")
    val textHelper: String,

    @SerializedName("error_message")
    val errorMessage: String,

    @SerializedName("field_name")
    val fieldName: String,

    @SerializedName("field_type")
    val fieldType: String,

    @SerializedName("input_type")
    val inputType: String,

    @SerializedName("field_mask")
    val fieldMask: String?,

    @SerializedName("is_mandatory")
    val isMandatory: Boolean,

    @SerializedName("validation_min")
    val validationMin: Int?,

    @SerializedName("validation_max")
    val validationMax: Int?,

    @SerializedName("prevent_duplication")
    val preventDuplication: Boolean,

    @SerializedName("additional_text")
    val additionalText: String?,

    @SerializedName("additional_text_emphasized")
    val additionalTextEmphasized: String?
) : Parcelable

@Parcelize
data class OpportunityListResponse(
    @SerializedName("opportunity_list")
    val opportunityList: OpportunityList
) : Parcelable

@Parcelize
data class OpportunityList(
    @SerializedName("submission_count")
    val submissionCount: Int,
    @SerializedName("size")
    val size: Int,

    @SerializedName("header")
    val header: Header,

    @SerializedName("empty_state")
    val opportunityEmptyState: OpportunityEmptyState?,
    @SerializedName("opportunity_objects")
    private val _opportunityObjects: List<OpportunityObject>?
) : Parcelable {
    val opportunityObjects: List<OpportunityObject>
        get() = _opportunityObjects ?: emptyList()
}

@Parcelize
data class OpportunityObject(
    @SerializedName("id")
    val id: Int,

    @SerializedName("primary_initials")
    val primaryInitials: String,

    @SerializedName("primary_field")
    val primaryField: String,

    @SerializedName("status")
    val status: String,

    @SerializedName("type")
    val type: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("stages_title")
    val stagesTitle: String,
    @SerializedName("timestamp")
    val timestamp: String,
    @SerializedName("progress")
    val progress: OpportunityObjectProgress
) : Parcelable

@Parcelize
data class OpportunityObjectProgress(
    @SerializedName("status_title")
    val statusTitle: String,
    @SerializedName("stage_status")
    val status: ObjectStagesStatus,
    @SerializedName("stages_count")
    val stagesCount: Int,
    @SerializedName("current_count")
    val currentCount: Int
) : Parcelable

@Parcelize
data class OpportunityEmptyState(

    @SerializedName("text")
    val text: String,

    @SerializedName("button_text")
    val buttonText: String,

    @SerializedName("icon")
    val icon: Media

) : Parcelable

@Parcelize
data class OpportunityInformationResponse(
    @SerializedName("information")
    val info: Information
) : Parcelable

@Parcelize
data class Information(
    @SerializedName("header")
    val header: Header,
    @SerializedName("details")
    val details: InformationDetails
) : Parcelable

@Parcelize
data class InformationDetails(
    @SerializedName("type")
    val type: InformationType,
    @SerializedName("title")
    val title: String,
    @SerializedName("stages")
    val stages: List<InformationStage>
) : Parcelable

enum class InformationType {
    @SerializedName("timeline")
    TIMELINE,

    @SerializedName("bullet_point")
    BULLET_POINTS
}

@Parcelize
data class InformationStage(
    @SerializedName("id")
    val id: Int,
    @SerializedName("order")
    val order: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("comission")
    val comission: Comission?
) : Parcelable

@Parcelize
data class OpportunitySubmissionResponse(
    @SerializedName("id")
    val submissionId: Int,

    @SerializedName("opportunity")
    val opportunityId: Int,

    @SerializedName("object_id")
    val objectId: Int,

    @SerializedName("object_type")
    val objectType: String

) : Parcelable

@Parcelize
data class SubmissionDetailsResponse(
    @SerializedName("submission")
    val submissionDetails: SubmissionDetails
) : Parcelable

@Parcelize
data class SubmissionDetails(
    @SerializedName("id")
    val id: Int,

    @SerializedName("primary_initials")
    val primaryInitials: String,

    @SerializedName("primary_field")
    val primaryField: String,

    @SerializedName("status")
    val status: SubmissionStatus,

    @SerializedName("type")
    val type: SubmissionDetailsType,

    @SerializedName("title")
    val title: String,

    @SerializedName("stages_title")
    val stagesTitle: String,

    @SerializedName("stages")
    val stages: List<SubmissionStages>
) : Parcelable

@Parcelize
data class SubmissionStages(
    @SerializedName("id")
    val id: Int,

    @SerializedName("order")
    val order: Int,

    @SerializedName("current")
    val current: Boolean,

    @SerializedName("status")
    var status: ObjectStagesStatus,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("timestamp")
    val timestamp: String? = "",

    @SerializedName("comission")
    val comission: Comission,

    @SerializedName("issue")
    val issue: Issue?
) : Parcelable

enum class SubmissionDetailsType {
    @SerializedName("timeline")
    TIMELINE,

    @SerializedName("bullet_points")
    BULLET,
}

enum class SubmissionStatus {
    @SerializedName("active")
    ACTIVE,

    @SerializedName("blocked")
    BLOCKED,

    @SerializedName("closed")
    CLOSED
}

enum class ObjectStagesStatus {
    @SerializedName("pending")
    PENDING,

    @SerializedName("completed")
    COMPLETED,

    @SerializedName("blocked")
    BLOCKED,

    @SerializedName("closed")
    CLOSED,
}

@Parcelize
data class Comission(
    @SerializedName("text")
    val text: String,

    @SerializedName("color")
    val color: String
) : Parcelable

@Parcelize
data class Issue(
    @SerializedName("title")
    val title: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("action_text")
    val actionText: String,

    @SerializedName("action_code")
    val actionCode: String
) : Parcelable

@Parcelize
data class OpportunitySubmission(
    @SerializedName("object_type")
    val objectType: String?,

    @SerializedName("object_id")
    val objectId: Int?,

    @SerializedName("fields")
    val fieldValues: List<FormFieldValue>
) : Parcelable

@Parcelize
data class FormFieldValue(
    @SerializedName("id")
    val id: Int,

    @SerializedName("value")
    val value: String
) : Parcelable
