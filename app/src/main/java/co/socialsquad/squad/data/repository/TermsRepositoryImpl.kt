package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.TermsResponse
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import io.reactivex.Observable
import javax.inject.Inject

class TermsRepositoryImpl @Inject constructor(private val api: Api) : TermsRepository {

    override fun terms(): Observable<TermsResponse> = api.terms().onDefaultSchedulers()
}
