package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class EditPasswordRequest(
    @SerializedName("old_password") val oldPassword: String? = null,
    @SerializedName("new_password") val newPassword: String? = null
)
