package co.socialsquad.squad.data.entity.request

import co.socialsquad.squad.data.entity.ScheduledRequest
import co.socialsquad.squad.data.entity.SegmentationRequest
import com.google.gson.annotations.SerializedName

data class AudioRequest(
    @SerializedName("name") var name: String? = null,
    @SerializedName("content") var content: String? = null,
    @SerializedName("segmentation") var segmentation: SegmentationRequest? = null,
    @SerializedName("scheduled") var scheduled: ScheduledRequest? = null
)
