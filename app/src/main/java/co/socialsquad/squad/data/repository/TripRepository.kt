package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.Trip
import io.reactivex.Observable

interface TripRepository {
    fun getTrips(): Observable<Results<Trip>>
}
