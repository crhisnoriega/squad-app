package co.socialsquad.squad.data.repository

import android.net.Uri
import co.socialsquad.squad.data.entity.Audio
import co.socialsquad.squad.data.entity.AudioList
import co.socialsquad.squad.data.entity.PostLikesResponse
import co.socialsquad.squad.data.entity.request.AudioRequest
import co.socialsquad.squad.data.remote.ProgressRequestBody
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import java.io.File

interface AudioRepository {
    var recordedAudio: Uri?

    fun createAudio(audioRequest: AudioRequest): Observable<Audio>
    fun sendAudioFile(pk: Int, fileUri: Uri, callback: ProgressRequestBody.UploadCallbacks): Completable
    fun createGalleryFile(mimeType: String): File
    fun deleteGalleryFile()
    fun deleteAudio(pk: Int): Completable
    fun likeAudio(pk: Int): Completable
    fun getAudioLikes(pk: Int, page: Int): Observable<PostLikesResponse>
    fun getAudioPlaybackUsers(pk: Int, page: Int, type: String): Observable<PostLikesResponse>
    fun getAudioList(page: Int): Observable<AudioList>
    fun getAudio(pk: Int): Observable<Audio>
    fun markAsListened(pk: Int): Completable
    fun createTemporaryAudioFile(): File
    fun deleteTemporaryAudioFile()
    fun saveAudio(audioViewModel: AudioViewModel)
    fun getSavedAudio(pk: Int): AudioViewModel?
}
