package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class ScheduledLiveCommentRequest(
    @SerializedName("live_pk") var pk: Int,
    @SerializedName("text") var text: String
)
