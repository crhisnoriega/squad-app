package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChartList(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("next") var next: String? = null,
    @SerializedName("previous") var previous: String? = null,
    @SerializedName("results") var results: List<Chart>
)

data class Chart(
    @SerializedName("pk") val pk: Long = 0,
    @SerializedName("title") val title: String,
    @SerializedName("title_unit") val titleUnit: String? = null,
    @SerializedName("value") var totalValue: Long = 0,
    @SerializedName("avatar") var sourceLogoUrl: String,
    @SerializedName("source") var source: String,
    @SerializedName("points") var points: List<Long>? = null
) : Serializable
