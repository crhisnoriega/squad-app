package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.CustomFieldRequest
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.entity.SystemList
import co.socialsquad.squad.data.entity.UserCompanyCID
import co.socialsquad.squad.data.entity.UserCreateRequest
import co.socialsquad.squad.data.entity.integration.RecruitRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiIntegration
import io.reactivex.Observable
import javax.inject.Inject

class SignupRepositoryImpl @Inject constructor(
    private val api: Api,
    private val apiIntegration: ApiIntegration,
    private val preferences: Preferences
) : SignupRepository {

    override var email: String?
        get() = preferences.signupEmail
        set(email) {
            preferences.signupEmail = email
        }

    override fun createUser(userCreateRequest: UserCreateRequest) =
        api.postUserCreate(userCreateRequest).onDefaultSchedulers()

    override fun registerIntegration(registerIntegrationRequest: RegisterIntegrationRequest) =
        api.postRegisterIntegration(registerIntegrationRequest).onDefaultSchedulers()

    override fun getRecruit(recruitRequest: RecruitRequest) =
        apiIntegration.postRecruit(recruitRequest).onDefaultSchedulers()

    override fun editUserCID(pk: Int, cid: UserCompanyCID) =
        api.patchUserCompanyCID(pk, cid).onDefaultSchedulers()

    override fun setCustomField(field: String, customFieldRequest: CustomFieldRequest) =
        api.postCustomField(field, customFieldRequest).onDefaultSchedulers()

    override fun getSystemList(): Observable<SystemList> =
        api.getSystemList().onDefaultSchedulers()
}
