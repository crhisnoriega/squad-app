package co.socialsquad.squad.data.repository

import android.content.ContentResolver
import android.net.Uri
import co.socialsquad.squad.data.entity.CompleteProfileRequest
import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiUpload
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo
import co.socialsquad.squad.presentation.feature.profile.edit.data.MediaResponse
import co.socialsquad.squad.presentation.feature.profile.edit.data.UserCompanyDTO
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(
    private val api: Api,
    private val preferences: Preferences,
    private val apiUpload: ApiUpload,
    private val contentResolver: ContentResolver
) : ProfileRepository {

    override var avatar: Uri?
        get() = preferences.signupAvatar?.let { Uri.parse(it) }
        set(avatarUri) {
            preferences.signupAvatar = avatarUri?.toString()
        }

    override fun getStates() =
        api.getStates().onDefaultSchedulers()

    override fun getCities(stateCode: Long) =
        api.getCities(stateCode).onDefaultSchedulers()

    override fun getNeighborhoods(cityCode: Long) =
        api.getNeighborhoods(cityCode).onDefaultSchedulers()

    override fun editUser(pk: Int, userCompany: UserCompany) =
        api.patchUserCompany(pk, userCompany).onDefaultSchedulers()

    override fun editUserV2(pk: Int, userCompany: UserCompanyDTO) =
        api.patchUserCompanyV2(pk, userCompany).onDefaultSchedulers()

    override fun uploadAvatar(pk: Int, avatarUri: Uri): Completable {
        val file = File(avatarUri.path)

        val mediaType = FileUtils.getMediaType(avatarUri, contentResolver)
        if (mediaType == null) Completable.error(Throwable())

        val requestBody = RequestBody.create(mediaType, file)

        val part = MultipartBody.Part.createFormData("files", file.name, requestBody)

        return apiUpload.postAvatarUpload(pk, part).onDefaultSchedulers()
    }

    override fun uploadMedia(media: Uri): Observable<MediaResponse> {
        val file = File(media.path)

        val mediaType = FileUtils.getMediaType(media, contentResolver)
        if (mediaType == null) Completable.error(Throwable())

        val requestBody = RequestBody.create(mediaType, file)

        val part = MultipartBody.Part.createFormData("file", file.name, requestBody)

        return apiUpload.uploadMedia(part).onDefaultSchedulers()
    }

    override fun getProfile(): Observable<UserCompany> =
        api.getProfile().onDefaultSchedulers()

    override fun getPosts(page: Int): Observable<Feed> =
        api.getProfileFeed(page).onDefaultSchedulers()

    override fun getProfile(pk: Int): Observable<UserCompany> =
        api.getProfile(pk).onDefaultSchedulers()

    override fun getPosts(pk: Int, page: Int): Observable<Feed> =
        api.getProfileFeed(pk, page).onDefaultSchedulers()

    override fun getQualification() =
        api.getQualificationProfile().map { it.qualifications }.onDefaultSchedulers()

    override fun getQualifications() =
        api.getQualifications().map { it.qualifications }.onDefaultSchedulers()

    override fun postRecommendation(recommendation: RecommendationRequest) =
        api.postRecommendation(recommendation).onDefaultSchedulers()

    override fun getQualification(pk: Int) =
        api.getQualificationProfile(pk).map { it.qualifications }.onDefaultSchedulers()

    override fun getQualificationRecommendationVideos(pk: Int, page: Int) =
        api.getQualificationRecommendationVideos(pk, page).onDefaultSchedulers()

    override fun getQualificationRecommendationEvents(pk: Int, page: Int) =
        api.getQualificationRecommendationEvents(pk, page).onDefaultSchedulers()

    override fun getQualificationRecommendationAudio(pk: Int, page: Int) =
        api.getQualificationRecommendationAudio(pk, page).onDefaultSchedulers()

    override fun getQualificationRecommendationLives(pk: Int, page: Int) =
        api.getQualificationRecommendationLives(pk, page).onDefaultSchedulers()

    override fun getQualificationRequirement(pk: Int) =
        api.getQualificationsRequirementsExplanations(pk).onDefaultSchedulers()

    override fun completeProfile(
        pk: Int,
        request: CompleteProfileRequest
    ): Observable<UserCompany> =
        api.completeProfile(pk, request).onDefaultSchedulers()

    override fun queryCEP(cep: String) =
        api.queryCEP("http://viacep.com.br/ws/$cep/json/").onDefaultSchedulers()

    override fun getBanksData(): Observable<List<BankInfo>> =
        api.fetchBanksInfo().onDefaultSchedulers()
}
