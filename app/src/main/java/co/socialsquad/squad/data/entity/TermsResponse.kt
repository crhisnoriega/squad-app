package co.socialsquad.squad.data.entity

data class TermsResponse(val results: List<Result>) {
    data class Result(
        val pk: Int,
        val title: String,
        val description: String,
        val updated_at: String,
        var items: MutableList<Item>
    ) {
        data class Item(val title: String?, val description: String)
    }
}
