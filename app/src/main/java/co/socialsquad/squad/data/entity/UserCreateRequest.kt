package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class UserCreateRequest(
    @SerializedName("email") val email: String
)
