package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class CompleteContextItem(
    @SerializedName("visible") var visible: Boolean?,
    @SerializedName("completed") var completed: Boolean?,
        ) : Parcelable