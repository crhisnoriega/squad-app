package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class MagicLinkDeeplink(
    @SerializedName("+url") val url: String,
    @SerializedName("~creation_source") val creation_source: String,
    @SerializedName("one_time_use") val one_time_use: String,
    @SerializedName("~id") val id: String,
    @SerializedName("user_email") val email: String,
    @SerializedName("first_name") val firstName: String?,
    @SerializedName("last_name") val lastName: String?,
    @SerializedName("cell_phone") val phone: String?,
    val subdomain: String,
    @SerializedName("~channel") val channel: String,
    val token: String,
    @SerializedName("+domain") val domain: String,
    @SerializedName("+click_timestamp") val click_timestamp: String,
    @SerializedName("+referrer") val referrer: String,
    @SerializedName("+clicked_branch_link") val clicked_branch_link: String,
    @SerializedName("+match_guaranteed") val match_guaranteed: String,
    @SerializedName("+is_first_session") val is_first_session: String
)
