package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class SubdomainsRequest(
    @SerializedName("email") val title: String
)
