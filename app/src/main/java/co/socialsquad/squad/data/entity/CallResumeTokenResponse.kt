package co.socialsquad.squad.data.entity

data class CallResumeTokenResponse(val detail: String, val token: String?)
