package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedbackItem(
    @SerializedName("pk") val pk: Int?,
    @SerializedName("text") val text: String?,
    @SerializedName("icon") val icon: String?
)
