package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class States(
    @SerializedName("states") var states: List<State>? = null
)
