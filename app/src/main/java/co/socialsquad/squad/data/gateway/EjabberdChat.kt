package co.socialsquad.squad.data.gateway

import android.util.Log
import co.socialsquad.squad.data.entity.ChatStateMessage
import co.socialsquad.squad.data.entity.ChatStatus
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatMessageStatus
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.repository.ChatRepository
import co.socialsquad.squad.data.repository.LoginRepository
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.jivesoftware.smack.AbstractXMPPConnection
import org.jivesoftware.smack.chat2.Chat
import org.jivesoftware.smack.chat2.ChatManager
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.packet.Presence
import org.jivesoftware.smack.roster.PresenceEventListener
import org.jivesoftware.smack.roster.Roster
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jivesoftware.smackx.carbons.CarbonManager
import org.jivesoftware.smackx.carbons.packet.CarbonExtension
import org.jivesoftware.smackx.chatstates.ChatState
import org.jivesoftware.smackx.chatstates.ChatStateManager
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension
import org.jivesoftware.smackx.delay.packet.DelayInformation
import org.jivesoftware.smackx.forward.packet.Forwarded
import org.jivesoftware.smackx.iqlast.LastActivityManager
import org.jivesoftware.smackx.mam.MamManager
import org.jivesoftware.smackx.ping.PingManager
import org.jivesoftware.smackx.sid.StableUniqueStanzaIdManager
import org.jivesoftware.smackx.sid.element.OriginIdElement
import org.jxmpp.jid.BareJid
import org.jxmpp.jid.EntityBareJid
import org.jxmpp.jid.FullJid
import org.jxmpp.jid.Jid
import org.jxmpp.jid.impl.JidCreate
import java.util.Date
import java.util.UUID
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class EjabberdChat @Inject constructor(
    loginRepository: LoginRepository,
    val chatRepository: ChatRepository
) : ChatGateway {

    private val compositeDisposable = CompositeDisposable()
    private val currentUserChatId = loginRepository.userCompany?.user?.chatId
    private var xmppConnection: AbstractXMPPConnection? = null
    private var roster: Roster? = null
    private var chatMessager: ChatManager? = null
    private var lastActivityManager: LastActivityManager? = null
    private var carbonManager: CarbonManager? = null
    private var publisherChatState: PublishSubject<ChatStateMessage> = PublishSubject.create()
    private var publisherRoomStatus: PublishSubject<Pair<String, Boolean>> = PublishSubject.create()

    override fun login(userId: String): Observable<String> {
        val configuration = XMPPTCPConnectionConfiguration.builder()
            .setUsernameAndPassword(userId, userId)
            .setXmppDomain("jabber.squad.com.br")
            .setHost("jabber.squad.com.br")
            .setPort(5222)
            .build()

        return Observable.create { emitter: ObservableEmitter<String> ->
            compositeDisposable.add(
                Observable.just(XMPPTCPConnection(configuration))
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .map { it.connect() }
                    .doOnNext { xmppConnection = it }
                    .subscribe(
                        {
                            it.login()
                            chatMessager = ChatManager.getInstanceFor(xmppConnection)
                            roster = Roster.getInstanceFor(xmppConnection)
                            roster?.isRosterLoadedAtLogin = true
                            roster?.subscriptionMode = Roster.SubscriptionMode.accept_all
                            val stanzaIdManager = StableUniqueStanzaIdManager.getInstanceFor(xmppConnection)
                            stanzaIdManager.enable()
                            emitter.onNext(userId)
                            roster?.addPresenceEventListener(object : PresenceEventListener {
                                override fun presenceAvailable(address: FullJid?, availablePresence: Presence?) {
                                    publisherRoomStatus.onNext(Pair(address?.localpartOrNull.toString(), true))
                                }

                                override fun presenceSubscribed(address: BareJid?, subscribedPresence: Presence?) {
                                    publisherRoomStatus.onNext(Pair(address?.localpartOrNull.toString(), true))
                                }

                                override fun presenceError(address: Jid?, errorPresence: Presence?) {}

                                override fun presenceUnsubscribed(address: BareJid?, unsubscribedPresence: Presence?) {
                                    publisherRoomStatus.onNext(Pair(address?.localpartOrNull.toString(), false))
                                }

                                override fun presenceUnavailable(address: FullJid?, presence: Presence?) {
                                    publisherRoomStatus.onNext(Pair(address?.localpartOrNull.toString(), false))
                                }
                            })
                            lastActivityManager = LastActivityManager.getInstanceFor(xmppConnection)
                            lastActivityManager?.enable()
                            carbonManager = CarbonManager.getInstanceFor(xmppConnection)
                            carbonManager?.enableCarbonsAsync { exception -> exception.printStackTrace() }
                            val pingManager = PingManager.getInstanceFor(xmppConnection)
                            pingManager.pingInterval = 60
                        },
                        {
                            it.printStackTrace()
                        },
                        {}
                    )
            )
        }
    }

    override fun logout(onDisconnected: () -> Unit) {
        Thread(
            Runnable {
                try {
                    xmppConnection?.disconnect()
                } catch (t: Throwable) {
                    t.printStackTrace()
                }
            }
        ).start()
        onDisconnected()
    }

    override fun disconnect() {
        compositeDisposable.clear()
    }

    override fun connectToChatChannel(chatRoom: ChatRoomModel): Observable<ChatRoomModel> {
        val userJid = JidCreate.entityBareFrom(chatRoom.chatId + "@jabber.squad.com.br")
        chatMessager?.chatWith(userJid)?.let {
            chats[chatRoom.channelUrl!!] = it
        }
        return Observable.create<ChatRoomModel> { emitter ->
            emitter.onNext(updateRoom(chatRoom))
        }
    }

    override fun setSenderObservable(channelUrl: String, sendMessageObservable: Observable<String>): Observable<ChatMessage> {
        return Observable.create { emitter: ObservableEmitter<ChatMessage> ->
            compositeDisposable.add(
                sendMessageObservable
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .map { ChatMessage(UUID.randomUUID().toString().toUpperCase(), it, currentUserChatId!!, channelUrl, Date().time, ChatMessageStatus.SENDING.name) }
                    .subscribe(
                        {
                            try {
                                val message = Message(JidCreate.entityBareFrom("$channelUrl@jabber.squad.com.br"))
                                val stanzaExtensionFilter = OriginIdElement(it.id)
                                message.addExtension(stanzaExtensionFilter)
                                message.body = it.message
                                message.type = Message.Type.chat
                                val chatStateElement = ChatStateExtension(ChatState.active)
                                message.addExtension(chatStateElement)
                                chats[channelUrl]?.send(message)
                            } catch (throwable: Throwable) {
                                it.status = ChatMessageStatus.ERROR.name
                            } finally {
                                emitter.onNext(it)
                            }
                        },
                        {
                            emitter.onError(it)
                        },
                        {}
                    )
            )
        }
    }

    override fun getUnreadCount(): Observable<Int> {
        return Observable.create<Int> { emitter ->
            emitter.onNext(-1)
        }
    }

    override fun getRoomObserver(): Observable<Pair<String, Boolean>> = publisherRoomStatus

    override fun disconnectRoomAndChannel(channelUrl: String) {
        chats.remove(channelUrl)
    }

    override fun getUnreadCount(channelUrl: String): Int {
        return -1
    }

    override fun getMessages(): Observable<ChatMessage> {
        return Observable.create<ChatMessage> { emitter ->
            chatMessager?.addIncomingListener { from, message, chat ->
                toChatMessage(message, emitter, chat.xmppAddressOfChatPartner.localpart.asUnescapedString())
            }
            chatMessager?.addOutgoingListener { _, message, chat ->
                toChatMessage(message, emitter, chat.xmppAddressOfChatPartner.localpart.asUnescapedString())
                processStateExtension(message, chat.xmppAddressOfChatPartner)
            }
            carbonManager?.addCarbonCopyReceivedListener { direction, carbonCopy, wrappingMessage ->
                if (direction == CarbonExtension.Direction.sent) {
                    toChatMessage(carbonCopy, emitter, wrappingMessage.to.localpartOrNull?.asUnescapedString() ?: "")
                }
            }
        }
    }

    private fun processStateExtension(message: Message, user: EntityBareJid) {
        if (message.hasExtension(ChatStateExtension.NAMESPACE)) {
            val chatExtension = message.getExtension(ChatStateExtension.NAMESPACE)
            val state = ChatState.valueOf(chatExtension.elementName)
            publisherChatState.onNext(ChatStateMessage(state.toChatStatus(), user.localpart.asUnescapedString()))
        }
    }

    override fun markAsRead(channelUrl: String) {
        val chatStateManager = ChatStateManager.getInstance(xmppConnection)
        chats[channelUrl]?.let { chat ->
            chatStateManager.setCurrentState(ChatState.active, chat)
        }
    }

    override fun getChatStateListener(): Observable<ChatStateMessage> {
        val chatStateManager = ChatStateManager.getInstance(xmppConnection)
        chatStateManager.addChatStateListener { chat, state, message ->
            chat.xmppAddressOfChatPartner.localpart?.let { channel ->
                //                publisherChatState.onNext(ChatStateMessage(state.toChatStatus(), channel.asUnescapedString()))
            }
        }
        return publisherChatState
    }

    override fun getMessagesBeforeDate(channelUrl: String, date: Long): Observable<ChatMessage> {
        val mamManager = MamManager.getInstanceFor(xmppConnection)
        val queryArgs = MamManager.MamQueryArgs.builder()
            .limitResultsToJid(JidCreate.entityBareFrom("$channelUrl@jabber.squad.com.br"))
            .limitResultsBefore(Date(date))
            .setResultPageSize(50)
            .queryLastPage()
            .build()
        val mutableResponse = mutableListOf<ChatMessage>()
        try {
            val archive = mamManager.queryArchive(queryArgs)
            var read = false
            mutableResponse.addAll(
                archive.page.forwarded.map { message ->
                    val chatMessage = mapToChatMessage(message, channelUrl, read)
                    read = chatMessage.status == ChatMessageStatus.READ.name
                    chatMessage
                }
            )
        } catch (t: Throwable) {}
        return Observable.fromIterable(mutableResponse)
    }

    override fun getMessagesFromDate(date: Long): Observable<ChatMessage> {

        val mamManager = MamManager.getInstanceFor(xmppConnection)
        val queryArgsBuilder = MamManager.MamQueryArgs.builder()
            .queryLastPage()
            .setResultPageSize(50)
        if (date > 0) {
            queryArgsBuilder.limitResultsSince(Date(date))
        } else {
            queryArgsBuilder.limitResultsBefore(Date())
        }
        Log.e("getMessages", date.toString())
        var queryArgs = queryArgsBuilder.build()
        val mutableResponse = mutableListOf<ChatMessage>()
        try {
            var archive = mamManager.queryArchive(queryArgs)
            var read = false
            Log.e("page", archive.page.forwarded.size.toString() + " messageCount: " + archive.messageCount + " listSize:" + archive.messages.size)
            mutableResponse.addAll(
                archive.page.forwarded.map { message ->
                    val chatMessage = mapToChatMessage(message, null, read)
                    read = chatMessage.status == ChatMessageStatus.READ.name
                    chatMessage
                }
            )
            while (!archive.isComplete) {
                queryArgs = MamManager.MamQueryArgs.builder()
                    .limitResultsBefore(Date(mutableResponse.last().date))
                    .setResultPageSize(50)
                    .build()
                archive = mamManager.queryArchive(queryArgs)
                mutableResponse.addAll(
                    archive.page.forwarded.map { message ->
                        val chatMessage = mapToChatMessage(message, null, read)
                        read = chatMessage.status == ChatMessageStatus.READ.name
                        chatMessage
                    }
                )
            }
        } catch (t: Throwable) {}
        Log.e("Result", mutableResponse.size.toString())
        return Observable.fromIterable(mutableResponse)
    }

    private fun toChatMessage(message: Message, emitter: ObservableEmitter<ChatMessage>, channelUrl: String) {
        val extension = DelayInformation.from(message)
        val date = extension?.stamp
        val originId = if (OriginIdElement.hasOriginId(message)) OriginIdElement.getOriginId(message).id else if (!message.stanzaId.isNullOrEmpty()) message.stanzaId else null
        currentUserChatId?.let { currentUserId ->
            originId?.let { id ->
                emitter.onNext(
                    ChatMessage(
                        id, message.body,
                        message.from?.localpartOrNull?.asUnescapedString()
                            ?: currentUserId,
                        channelUrl,
                        date?.time
                            ?: Date().time,
                        if (message.from?.localpartOrNull?.asUnescapedString().isNullOrEmpty()) ChatMessageStatus.SENDING.name else ChatMessageStatus.UNREAD.name
                    )
                )
            }
        }
    }

    private fun mapToChatMessage(forwarded: Forwarded, channelUrl: String?, readSignal: Boolean): ChatMessage {
        val message = Forwarded.extractMessagesFrom(listOf(forwarded))[0]
        val messageDate = forwarded.delayInformation.stamp
        val originId = if (OriginIdElement.hasOriginId(message)) OriginIdElement.getOriginId(message).id else message.stanzaId
        var readed = false
        if (message.hasExtension(ChatStateExtension.NAMESPACE) && message.from.localpartOrNull.toString() == currentUserChatId) {
            val chatExtension = message.getExtension(ChatStateExtension.NAMESPACE)
            val state = ChatState.valueOf(chatExtension.elementName)
            val chatStateMessage = ChatStateMessage(state.toChatStatus(), message.from.localpartOrNull.asUnescapedString())
            publisherChatState.onNext(chatStateMessage)
            readed = chatStateMessage.status.name == ChatStatus.ACTIVE.name
        }
        val channel = channelUrl ?: if (message.to.localpartOrNull?.asUnescapedString() == currentUserChatId) message.from.localpartOrNull.asUnescapedString() else message.to.localpartOrNull?.asUnescapedString()
        return ChatMessage(originId, message.body, message.from.localpartOrNull.asUnescapedString(), channel!!, messageDate.time, if (readSignal) ChatMessageStatus.READ.name else if (readed) ChatMessageStatus.READ.name else ChatMessageStatus.UNREAD.name)
    }

    override fun updateRoom(chatRoomModel: ChatRoomModel): ChatRoomModel {
        val mamManager = MamManager.getInstanceFor(xmppConnection)
        val userJid = JidCreate.entityBareFrom("${chatRoomModel.channelUrl}@jabber.squad.com.br")
        val queryArgs = MamManager.MamQueryArgs.builder()
            .limitResultsToJid(userJid)
            .setResultPageSize(1)
            .queryLastPage()
            .build()
        try {
            val archive = mamManager.queryArchive(queryArgs)
            archive?.page?.forwarded?.forEach {
                chatRoomModel.elapsedTime = it.delayInformation.stamp.time
                chatRoomModel.lastMessage = Forwarded.extractMessagesFrom(listOf(it))[0]?.body
            }
        } catch (t: Throwable) {}
        val presence = roster?.getPresence(userJid)
        chatRoomModel.isOnline = presence?.type == Presence.Type.available
        try {
            val last = lastActivityManager?.getLastActivity(userJid)?.lastActivity ?: 0
            chatRoomModel.lastSeen = Date().time - TimeUnit.MILLISECONDS.convert(last, TimeUnit.SECONDS)
        } catch (t: Throwable) {
            chatRoomModel.lastSeen = 0
        }
        return chatRoomModel
    }

    companion object {
        private val chats = mutableMapOf<String, Chat>()
    }
}

fun ChatState.toChatStatus(): ChatStatus {
    return when {
        this.name == "active" -> ChatStatus.ACTIVE
        this.name == "composing" -> ChatStatus.COMPOSING
        else -> ChatStatus.INACTIVE
    }
}
