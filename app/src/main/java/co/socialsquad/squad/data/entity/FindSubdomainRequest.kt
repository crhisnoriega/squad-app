package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class FindSubdomainRequest(
    @SerializedName("subdomain") val subdomain: String
)
