package co.socialsquad.squad.data.local

interface Preferences {
    var squadName: String?
    var token: String?
    var subdomain: String?
    var audioEnabled: Boolean
    var userCompany: String?
    var squadColor: String?
    var signupAvatar: String?
    var signupEmail: String?
    var mediaUri: String?
    var language: String
    var livePk: Int
    var scheduleImageMedia: String?
    var scheduleVideoMedia: String?
    var sharePK: Int
    var shareModel: String?
    var audioURI: String?
    var interactionDialogTimeInMillis: Long
    var externalSystemPassword: String?
    var externalSystemPasswordIV: ByteArray?
    var audioList: String?
    var videoList: String?
    var sections: String?
    fun cleanSections()
}
