package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedCommentRequest(
    @SerializedName("feed_pk") var pk: Int,
    @SerializedName("text") var text: String
)
