package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class DocumentRepositoryImpl @Inject constructor(private val api: Api) : DocumentRepository {
    override fun getDocuments() = api.getDocuments().onDefaultSchedulers()

    override fun markDocumentViewed(pk: Int) = api.getDocumentsView(pk).onDefaultSchedulers()
}
