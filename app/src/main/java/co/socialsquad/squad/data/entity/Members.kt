package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Members(
    @SerializedName("count") var count: Int? = null,
    @SerializedName("next") var next: String? = null,
    @SerializedName("previous") var previous: String? = null,
    @SerializedName("results") var results: List<UserCompany>? = null
)
