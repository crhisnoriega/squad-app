package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("cover")
    val cover: Any? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("icon")
    val icon: Any? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("pk")
    val pk: Int? = null
)
