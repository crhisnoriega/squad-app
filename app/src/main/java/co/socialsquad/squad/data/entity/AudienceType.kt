package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class AudienceType(
    @SerializedName("title") val title: String?,
    @SerializedName("content") val content: String?,
    @SerializedName("type") val type: String?,
    @SerializedName("icon") val icon: String?
) {
    fun type() = Type.values().find { it.value == type }

    enum class Type(val value: String) {
        Public("p"),
        Downline("d"),
        Metrics("f"),
        Members("m")
    }
}
