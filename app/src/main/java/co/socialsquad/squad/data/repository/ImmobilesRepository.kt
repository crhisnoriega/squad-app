package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.data.entity.ImmobileResponse
import io.reactivex.Observable

interface ImmobilesRepository {
    fun immobiles(page: Int): Observable<ImmobileResponse>
    fun immobileDetail(immobileId: Int): Observable<List<ImmobileDetail>>
}
