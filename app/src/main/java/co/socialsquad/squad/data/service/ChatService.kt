package co.socialsquad.squad.data.service

import android.app.IntentService
import android.app.Service
import android.content.Intent
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.entity.ChatList
import co.socialsquad.squad.data.entity.database.ChatMessageStatus
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.gateway.ChatGateway
import co.socialsquad.squad.data.repository.ChatRepository
import co.socialsquad.squad.data.repository.LoginRepository
import dagger.android.AndroidInjection
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val CHAT_SERVICE_NAME: String = "CHAT_SERVICE_NAME"

const val CHAT_SERVICE_ACTION_START = "chat_service_action_start"
const val CHAT_SERVICE_ACTION_OPEN_ROOM = "chat_service_action_open_room"
const val CHAT_SERVICE_ACTION_MARK_AS_READ = "chat_service_action_mark_as_read"
const val CHAT_SERVICE_ACTION_SEND_MESSAGE = "chat_service_action_send_message"
const val CHAT_SERVICE_ACTION_STOP = "chat_service_action_stop"

const val CHAT_SERVICE_ROOM_URL_EXTRA = "chat_service_room_url_extra"
const val CHAT_SERVICE_ROOM_SERIALIZABLE_EXTRA = "chat_service_room_serializable_extra"
const val CHAT_SERVICE_MESSAGE_EXTRA = "chat_service_message_extra"
const val CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA = "chat_service_last_message_date_extra"
const val CHAT_SERVICE_ACTION_LOAD_MESSAGES_FROM_DATE = "chat_service_action_load_messages_from_date"

class ChatService : IntentService(CHAT_SERVICE_NAME) {

    @Inject lateinit var chatGateway: ChatGateway
    @Inject lateinit var loginRepository: LoginRepository
    @Inject lateinit var chatRepository: ChatRepository

    val compositeDisposable = CompositeDisposable()

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!BuildConfig.DEBUG) {
            return Service.START_NOT_STICKY
        }
        onHandleIntent(intent)
        return Service.START_STICKY
    }

    override fun onHandleIntent(intent: Intent?) {
        if (!BuildConfig.DEBUG) {
            return
        }
        if (!logged) {
            chatSetup()
        }
        when (intent?.action) {
            CHAT_SERVICE_ACTION_START -> chatSetup()
            CHAT_SERVICE_ACTION_STOP -> chatLogout()
            CHAT_SERVICE_ACTION_SEND_MESSAGE -> sendMessage(intent)
            CHAT_SERVICE_ACTION_OPEN_ROOM -> openRoom(intent)
            CHAT_SERVICE_ACTION_LOAD_MESSAGES_FROM_DATE -> getOldMessagesFromDate(intent)
            CHAT_SERVICE_ACTION_MARK_AS_READ -> markAsRead(intent)
            else -> chatSetup()
        }
    }

    private fun chatLogout() {
        chatGateway.logout {
            stopSelf()
            compositeDisposable.clear()
        }
    }

    private fun chatSetup() {
        loginRepository.userCompany?.user?.chatId?.let {
            chatGateway.login(it)
                .subscribe(
                    {
                        logged = true
                        connectRooms()
                        getMessages()
                        getRoomStatus()
                        getOldMessages()
                    },
                    {
                        it.printStackTrace()
                    }
                )
        }
    }

    private fun updateUnreadCount() {
        chatRepository.updateUnreadCount(0)
//        compositeDisposable.add(
//                chatRepository.getTotalMessagesUnreadCount()
//                        .doOnComplete { chatRepository.updateUnreadCount(0) }
//                        .observeOn(Schedulers.io())
//                        .subscribeOn(Schedulers.io())
//                        .subscribe({
//                            chatRepository.updateUnreadCount(it)
//                        }, { it.printStackTrace() }, {})
//        )
    }

    private fun connectRooms() {
        compositeDisposable.add(
            chatRepository.getChatList()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .map { mapResults(it) }
                .doOnNext { roomList -> chatRepository.storeChatRoom(roomList.filter { it.channelUrl.isNullOrEmpty() }) }
                .flatMap { Observable.fromIterable(it) }
                .filter { !it.channelUrl.isNullOrEmpty() }
                .flatMap { chatGateway.connectToChatChannel(it) }
                .observeOn(Schedulers.io())
                .doOnNext { chatRepository.storeChatRoom(it) }
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { roomModel ->
                        val senderObservable = PublishSubject.create<String>()
                        sendChannels[roomModel.channelUrl!!] = senderObservable
                        compositeDisposable.add(
                            chatGateway.setSenderObservable(roomModel.channelUrl!!, senderObservable)
                                .observeOn(Schedulers.io())
                                .subscribeOn(Schedulers.io())
                                .subscribe(
                                    { chatMessage ->
                                        chatRepository.storeMessage(chatMessage)
                                    },
                                    { it.printStackTrace() }, {}
                                )
                        )
                    },
                    { it.printStackTrace() }, {}
                )
        )
    }

    private fun mapResults(chatList: ChatList): List<ChatRoomModel> {
        val contactsModels = mutableListOf<ChatRoomModel>()
        chatList.sections.forEach { section ->
            section.chats.forEach {
                contactsModels.add(
                    ChatRoomModel(
                        it.chatWith.pk,
                        it.chatWith.user.chatId,
                        it.chatWith.user.avatar,
                        it.chatWith.canCall(),
                        it.chatWith.fullName,
                        it.chatWith.qualification,
                        it.chatWith.qualificationBadge,
                        it.chatWith.user.chatId,
                        "",
                        null,
                        it.unreadCount,
                        false,
                        0,
                        section.title,
                        chatList.sections.indexOf(section)
                    )
                )
            }
        }
        chatList.contacts.filter { contact -> contactsModels.none { it.pk == contact.pk } }
            .forEach { contacts ->
                contactsModels.add(
                    ChatRoomModel(
                        contacts.pk,
                        "",
                        contacts.user.avatar,
                        contacts.canCall(),
                        contacts.fullName,
                        contacts.qualification,
                        contacts.qualificationBadge,
                        contacts.user.chatId,
                        "",
                        null,
                        0,
                        false,
                        0,
                        "contacts",
                        Int.MAX_VALUE
                    )
                )
            }
        return contactsModels
    }

    private fun getMessages() {
        compositeDisposable.add(
            chatGateway.getMessages()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { chatMessage ->
                        chatRepository.storeMessage(chatMessage)
                        updateRoom(chatMessage.channelUrl)
                        updateUnreadCount()
                    },
                    { it.printStackTrace() }, {}
                )
        )
    }

    private fun getRoomStatus() {
        compositeDisposable.add(
            chatGateway.getRoomObserver()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .flatMapMaybe { newState ->
                    chatRepository.getChatRoom(newState.first)
                        .map {
                            it.isOnline = newState.second
                            it
                        }
                }
                .subscribe(
                    {
                        chatRepository.storeChatRoom(it)
                    },
                    { it.printStackTrace() }
                )
        )
    }

    private fun sendMessage(intent: Intent) {
        if (!intent.hasExtra(CHAT_SERVICE_ROOM_URL_EXTRA) || !intent.hasExtra(CHAT_SERVICE_MESSAGE_EXTRA)) {
            return
        }
        val channelUrl = intent.getStringExtra(CHAT_SERVICE_ROOM_URL_EXTRA)
        val message = intent.getStringExtra(CHAT_SERVICE_MESSAGE_EXTRA).trim()
        if (channelUrl.isEmpty() || message.isEmpty()) {
            return
        }
        sendChannels[channelUrl]?.onNext(message)
    }

    private fun openRoom(intent: Intent) {
        if (!intent.hasExtra(CHAT_SERVICE_ROOM_SERIALIZABLE_EXTRA)) {
            return
        }
        val room = intent.getSerializableExtra(CHAT_SERVICE_ROOM_SERIALIZABLE_EXTRA) as? ChatRoomModel
            ?: return
        if (room.channelUrl.isNullOrEmpty() || sendChannels[room.channelUrl ?: ""] == null) {
            connectToRoom(room)
            return
        }
    }

    private fun updateRoom(channelUrl: String) {
        compositeDisposable.add(
            chatRepository.getChatRoom(channelUrl)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .map { chatGateway.updateRoom(it) }
                .flatMap { room ->
                    chatRepository.getMessagesUnreadCount(channelUrl)
                        .map {
                            room.unreadCount = it
                            room
                        }
                }
                .subscribe {
                    chatRepository.storeChatRoom(it)
                }
        )
    }

    private fun connectToRoom(room: ChatRoomModel) {
        compositeDisposable.add(
            chatRepository.createChatRoom(room.pk)
                .flatMap { chatGateway.connectToChatChannel(room) }
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .doOnNext { chatRepository.storeChatRoom(it) }
                .subscribe(
                    { roomModel ->
                        val senderObservable = PublishSubject.create<String>()
                        sendChannels[roomModel.channelUrl!!] = senderObservable
                        compositeDisposable.add(
                            chatGateway.setSenderObservable(roomModel.channelUrl!!, senderObservable)
                                .observeOn(Schedulers.io())
                                .subscribeOn(Schedulers.io())
                                .subscribe({ chatMessage -> chatRepository.storeMessage(chatMessage) }, { it.printStackTrace() }, {})
                        )
                    },
                    {
                        it.printStackTrace()
                    },
                    {}
                )
        )
    }

    private fun getOldMessages() {
        compositeDisposable.add(
            chatRepository.getLastSync()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .flatMap { chatGateway.getMessagesFromDate(it) }
                .doOnError { it.printStackTrace() }
                .filter { !it.id.isBlank() }
                .subscribe(
                    { chatRepository.storeMessage(it); updateUnreadCount() },
                    { it.printStackTrace() },
                    { }
                )
        )
    }

    private fun getOldMessagesFromDate(intent: Intent) {
        if (!intent.hasExtra(CHAT_SERVICE_ROOM_URL_EXTRA) && !(intent.hasExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA))) {
            return
        }
        val room = intent.getStringExtra(CHAT_SERVICE_ROOM_URL_EXTRA)
            ?: return
        val date = intent.getLongExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA, -1)
        if (date == -1L) return
        if (sendChannels.containsKey(room)) {
            compositeDisposable.add(
                chatGateway.getMessagesBeforeDate(room, date)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .filter { !it.id.isBlank() }
                    .map { it.status = ChatMessageStatus.READ.name; it }
                    .subscribe(
                        {
                            chatRepository.storeMessage(it)
                        },
                        {
                            it.printStackTrace()
                        },
                        {
                            updateRoom(room)
                        }
                    )
            )
        } else {
            compositeDisposable.add(
                Observable.just(intent)
                    .delay(2, TimeUnit.SECONDS)
                    .firstElement()
                    .subscribe {
                        getOldMessagesFromDate(it)
                    }
            )
        }
    }

    private fun markAsRead(intent: Intent) {
        if (!intent.hasExtra(CHAT_SERVICE_ROOM_URL_EXTRA) && !(intent.hasExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA))) {
            return
        }
        val room = intent.getStringExtra(CHAT_SERVICE_ROOM_URL_EXTRA)
            ?: return
        val date = intent.getLongExtra(CHAT_SERVICE_LAST_MESSAGE_DATE_EXTRA, -1)
        if (date == -1L) return
        compositeDisposable.add(
            Observable.just(Pair(room, date))
                .map { chatRepository.markStoredMessagesAsRead(it.first); it }
                .subscribe({ updateRoom(it.first) }, {})
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        chatLogout()
        logged = false
        compositeDisposable.clear()
    }

    companion object {
        private var sendChannels = mutableMapOf<String, PublishSubject<String>>()
        private var logged = false
    }
}
