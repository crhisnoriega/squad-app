package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class PlatformRepositoryImpl @Inject constructor(private val api: Api) : PlatformRepository {
    private val platformName = "android"

    override fun getVersion() = api.getLastVersion(platformName).onDefaultSchedulers()
}
