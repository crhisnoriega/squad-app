package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Store(
    @SerializedName("pk") val pk: Int,
    @SerializedName("sid") val sid: String,
    @SerializedName("name") val name: String,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lng") val lng: Double,
    @SerializedName("address") val address: String,
    @SerializedName("neighborhood") val neighborhood: String? = null,
    @SerializedName("state") val state: String,
    @SerializedName("phone") val phone: String? = null,
    @SerializedName("cep") val cep: String,
    @SerializedName("count") val count: Int,
    @SerializedName("photo") val photo: Media? = null,
    @SerializedName("number") val number: String? = null
)
