package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class MagicLinkResponse(@SerializedName("email") val email: String)
