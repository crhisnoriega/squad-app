package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class PostPasswordRequest(
    @SerializedName("password") var password: String,
    @SerializedName("token") var token: String
)
