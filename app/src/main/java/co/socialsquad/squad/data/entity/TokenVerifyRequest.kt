package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class TokenVerifyRequest(@SerializedName("token") val token: String)
