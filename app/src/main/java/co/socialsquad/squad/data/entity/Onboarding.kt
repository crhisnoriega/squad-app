package co.socialsquad.squad.data.entity

data class Onboarding(val title: String?, val description: String)
