package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Metrics(
    @SerializedName("results") var results: List<Metric>? = null,
    @SerializedName("count") var count: Int
)

data class Metric(
    @SerializedName("pk") val pk: Int,
    @SerializedName("field") val field: String,
    @SerializedName("values") val values: List<Value>? = null,
    @SerializedName("members") val members: Int,
    @SerializedName("multipick") var multipick: Boolean,
    @SerializedName("display_name") val displayName: String,
    @SerializedName("name") val name: String
) : Serializable

data class Values(
    @SerializedName("field") var field: String,
    @SerializedName("count") var count: Int,
    @SerializedName("page") var page: Int,
    @SerializedName("results") var results: List<Value>? = null,
    @SerializedName("next") var next: String? = null
)

data class Value(
    @SerializedName("value") val value: String,
    @SerializedName("members") val members: Int,
    @SerializedName("icon") var icon: String? = null,
    @SerializedName("display_value") var displayValue: String
) : Serializable
