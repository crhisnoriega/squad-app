package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Qualification(
    @SerializedName("pk") val pk: Int,
    @SerializedName("actual") val actual: Boolean,
    @SerializedName("target") val target: Boolean,
    @SerializedName("icon") val icon: String,
    @SerializedName("name") val name: String,
    @SerializedName("progress") val progress: Double,
    @SerializedName("prizes") val prizes: List<Prize>,
    @SerializedName("requirements") val requirements: List<Requirement>,
    @SerializedName("bonus") val bonus: String?,
    @SerializedName("success_stories") val successStories: List<SuccessStories>,
    @SerializedName("suggestions") val suggestions: List<Suggestions>
)
