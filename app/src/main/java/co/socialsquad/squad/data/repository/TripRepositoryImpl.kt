package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class TripRepositoryImpl @Inject constructor(private val api: Api) : TripRepository {
    override fun getTrips() = api.getTrips().onDefaultSchedulers()
}
