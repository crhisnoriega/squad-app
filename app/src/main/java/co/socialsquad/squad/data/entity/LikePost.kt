package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.util.Date

data class LikePost(
    @SerializedName("user_company") var userCompany: UserCompany? = null,
    @SerializedName("created_at") var createdAt: Date? = null,
    @SerializedName("updated_at") var updated_at: Date? = null
)
