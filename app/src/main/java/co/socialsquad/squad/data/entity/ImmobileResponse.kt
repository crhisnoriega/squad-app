package co.socialsquad.squad.data.entity

data class ImmobileResponse(val results: List<Immobile>, val count: Int)
