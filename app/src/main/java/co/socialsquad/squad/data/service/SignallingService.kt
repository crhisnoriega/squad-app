package co.socialsquad.squad.data.service

import android.app.IntentService
import android.content.Intent
import co.socialsquad.squad.data.entity.CallingQueueItem
import co.socialsquad.squad.data.entity.CancelQueueItem
import co.socialsquad.squad.data.entity.Candidate
import co.socialsquad.squad.data.entity.CandidateQueueItem
import co.socialsquad.squad.data.entity.SDP
import co.socialsquad.squad.data.entity.SdpQueueItem
import co.socialsquad.squad.data.entity.VideoCallingQueueItem
import co.socialsquad.squad.data.repository.CallRepository
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_ACTION_RECEIVE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_ACTION
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_CALL_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_QUEUE_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_TYPE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_USER
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_AUDIO
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_VIDEO
import co.socialsquad.squad.presentation.feature.call.CallRoomActivity
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

const val CALL_SERVICE_NAME = "CallServiceQueue"
const val CALL_SERVICE_ACTION_START = "call_service_action_start"
const val CALL_SERVICE_ACTION_STOP_CALL = "call_service_action_stop_call"

class SignallingService : IntentService(CALL_SERVICE_NAME) {

    @Inject
    lateinit var callRepository: CallRepository

    private val compositeDisposable = CompositeDisposable()

    private var isInCall = false
    private var sdp: SDP? = null
    private var candidate: Candidate? = null
    private var started = false

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onHandleIntent(intent: Intent?) {
        when (intent?.action) {
            CALL_SERVICE_ACTION_START -> { startRabbitQueue() }
            CALL_SERVICE_ACTION_STOP_CALL -> {
                isInCall = false
                sdp = null
                candidate = null
            }
        }
    }

    private fun startRabbitQueue() {
        if (started) return
        started = true
        compositeDisposable.add(
            callRepository.connectQueue()
                .subscribe(
                    { queueItem ->
                        when (queueItem) {
                            is CallingQueueItem -> {
                                if (!isInCall) {
                                    isInCall = true
                                    val dialogIntent = Intent(baseContext, CallRoomActivity::class.java).apply {
                                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        putExtra(CALL_ROOM_EXTRA_QUEUE_ID, queueItem.chatId)
                                        putExtra(CALL_ROOM_EXTRA_ACTION, CALL_ROOM_ACTION_RECEIVE)
                                        putExtra(CALL_ROOM_EXTRA_USER, queueItem.profile)
                                        putExtra(CALL_ROOM_EXTRA_CALL_ID, queueItem.uuid)
                                        putExtra(CALL_ROOM_EXTRA_TYPE, CALL_TYPE_AUDIO)
                                    }
                                    application.startActivity(dialogIntent)
                                }
                            }

                            is VideoCallingQueueItem -> {
                                if (!isInCall) {
                                    isInCall = true
                                    val dialogIntent = Intent(baseContext, CallRoomActivity::class.java).apply {
                                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                        putExtra(CALL_ROOM_EXTRA_QUEUE_ID, queueItem.chatId)
                                        putExtra(CALL_ROOM_EXTRA_ACTION, CALL_ROOM_ACTION_RECEIVE)
                                        putExtra(CALL_ROOM_EXTRA_USER, queueItem.profile)
                                        putExtra(CALL_ROOM_EXTRA_CALL_ID, queueItem.uuid)
                                        putExtra(CALL_ROOM_EXTRA_TYPE, CALL_TYPE_VIDEO)
                                    }
                                    application.startActivity(dialogIntent)
                                }
                            }

                            is SdpQueueItem -> if (!isInCall) {
                                sdp = queueItem.sdp
                            }
                            is CandidateQueueItem -> if (!isInCall) {
                                candidate = queueItem.candidate
                            }
                            is CancelQueueItem -> {
                                isInCall = false
                                sdp = null
                                candidate = null
                            }
                        }
                    },
                    {
                        it.printStackTrace()
                    },
                    {
                        started = false
                    }
                )
        )
    }
}
