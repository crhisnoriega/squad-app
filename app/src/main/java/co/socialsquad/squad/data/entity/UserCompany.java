package co.socialsquad.squad.data.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nullable;

public class UserCompany implements Serializable {

    @SerializedName("pk")
    private int pk;

    @SerializedName("token")
    private String token;

    @SerializedName("user")
    private User user;

    @SerializedName("company")
    private Company company;

    @SerializedName("is_employee")
    private boolean isEmployee;

    @SerializedName("content_curator")
    private boolean isCurator;

    @SerializedName("can_see_qualification")
    private boolean canSeeQualification;

    @SerializedName("can_create_camera")
    private boolean canCreateCamera;

    @SerializedName("can_create_live")
    private boolean canCreateLive;

    @SerializedName("can_create_audio")
    private boolean canCreateAudio;

    @SerializedName("can_create_product")
    private boolean canCreateProduct;

    @SerializedName("can_create_video")
    private boolean canCreateVideo;

    @SerializedName("can_create_photo")
    private boolean canCreatePhoto;

    @SerializedName("can_create_live_product")
    private boolean canCreateLiveProduct;

    @SerializedName("can_create_live_comment")
    private boolean canCreateLiveComment;

    @SerializedName("can_call")
    private boolean canCall;

    @SerializedName("is_active")
    private boolean isActive;

    @SerializedName("cover_color")
    private String coverColor;

    @SerializedName("network_count")
    private int networkCount;

    @SerializedName("role_display")
    private String roleDisplay;

    @SerializedName("role")
    private String role;

    @SerializedName("cid")
    private String cid;

    @SerializedName("sponsor_cid")
    private String sponsorCid;

    @SerializedName("qualification")
    private String qualification;

    @SerializedName("is_company_active")
    private boolean isCompanyActive;

    @SerializedName("multilevel_system")
    private String multilevelSystem;

    @SerializedName("bio")
    private String bio;

    @SerializedName("qualification_badge")
    private String qualificationBadge;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("is_password_set")
    private boolean isPasswordSet;

    @SerializedName("ranking")
    private RankingProfileModel ranking;

    @SerializedName("recognition2")
    private RecognitionProfileModel recognition2;

    @SerializedName("recognition")
    private RecognitionData recognition;

    @SerializedName("wallet_tools")
    private WalletProfileModel wallet_tools;

    @SerializedName("personal_points")
    private PersonalPointsData personal_points;

    @SerializedName("complete_your_profile")
    private CompleteYourProfile complete_your_profile;

    @SerializedName("salesforce_id")
    private String salesforce_id;

    public String getSalesforce_id() {
        return salesforce_id;
    }

    public void setSalesforce_id(String salesforce_id) {
        this.salesforce_id = salesforce_id;
    }

    public PersonalPointsData getPersonal_points() {
        return personal_points;
    }

    public CompleteYourProfile getComplete_your_profile() {
        return complete_your_profile;
    }

    public void setComplete_your_profile(CompleteYourProfile complete_your_profile) {
        this.complete_your_profile = complete_your_profile;
    }

    public void setPersonal_points(PersonalPointsData personal_points) {
        this.personal_points = personal_points;
    }

    public WalletProfileModel getWallet_tools() {
        return wallet_tools;
    }

    public void setWallet_tools(WalletProfileModel wallet_tools) {
        this.wallet_tools = wallet_tools;
    }

    public RecognitionData getRecognition() {
        return recognition;
    }

    public void setRecognition(RecognitionData recognition) {
        this.recognition = recognition;
    }

    public RecognitionProfileModel getRecognition2() {
        return recognition2;
    }

    public void setRecognition2(RecognitionProfileModel recognition) {
        this.recognition2 = recognition;
    }

    public RankingProfileModel getRanking() {
        return ranking;
    }

    public void setRanking(RankingProfileModel ranking) {
        this.ranking = ranking;
    }

    @SerializedName("squadpermissions")
    @Nullable
    private List<String> squadPermissions;

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean canCreateCamera() {
        return canCreateCamera;
    }

    public void canCreateCamera(boolean canCreateCamera) {
        this.canCreateCamera = canCreateCamera;
    }

    public boolean canCreateLive() {
        return canCreateLive;
    }

    public boolean canCreateAudio() {
        return canCreateAudio;
    }

    public void setCanCreateAudio(boolean canCreateAudio) {
        this.canCreateAudio = canCreateAudio;
    }

    public void canCreateLive(boolean canCreateLive) {
        this.canCreateLive = canCreateLive;
    }

    public boolean canCreateProduct() {
        return canCreateProduct;
    }

    public void canCreateProduct(boolean canCreateProduct) {
        this.canCreateProduct = canCreateProduct;
    }

    public boolean canCreateVideo() {
        return canCreateVideo;
    }

    public void canCreateVideo(boolean canCreateVideo) {
        this.canCreateVideo = canCreateVideo;
    }

    public boolean canCreatePhoto() {
        return canCreatePhoto;
    }

    public void canCreatePhoto(boolean canCreatePhoto) {
        this.canCreatePhoto = canCreatePhoto;
    }

    public boolean canCreateLiveProduct() {
        return canCreateLiveProduct;
    }

    public void canCreateLiveProduct(boolean canCreateLiveProduct) {
        this.canCreateLiveProduct = canCreateLiveProduct;
    }

    public boolean canCreateLiveComment() {
        return canCreateLiveComment;
    }

    public void canCreateLiveComment(boolean canCreateLiveComment) {
        this.canCreateLiveComment = canCreateLiveComment;
    }

    public boolean canCall() {
        return canCall;
    }

    public void canCall(boolean canCall) {
        this.canCall = canCall;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCoverColor() {
        return coverColor;
    }

    public void setCoverColor(String coverColor) {
        this.coverColor = coverColor;
    }

    public int getNetworkCount() {
        return networkCount;
    }

    public void setNetworkCount(int networkCount) {
        this.networkCount = networkCount;
    }

    public String getRoleDisplay() {
        return roleDisplay;
    }

    public void setRoleDisplay(String roleDisplay) {
        this.roleDisplay = roleDisplay;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getSponsorCid() {
        return sponsorCid;
    }

    public void setSponsorCid(String sponsorCid) {
        this.sponsorCid = sponsorCid;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public boolean isCompanyActive() {
        return isCompanyActive;
    }

    public void setCompanyActive(boolean companyActive) {
        isCompanyActive = companyActive;
    }

    public String getMultilevelSystem() {
        return multilevelSystem;
    }

    public void setMultilevelSystem(String multilevelSystem) {
        this.multilevelSystem = multilevelSystem;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean isEmployee() {
        return isEmployee;
    }

    public void setEmployee(boolean employee) {
        isEmployee = employee;
    }

    public boolean isCurator() {
        return isCurator;
    }

    public void setCurator(boolean curator) {
        isCurator = curator;
    }

    public boolean getCanSeeQualification() {
        return canSeeQualification;
    }

    public void setCanSeeQualification(boolean canSee) {
        canSeeQualification = canSee;
    }

    public String getQualificationBadge() {
        return qualificationBadge;
    }

    public void setQualificationBadge(String qualificationBadge) {
        this.qualificationBadge = qualificationBadge;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean getIsPasswordSet() {
        return isPasswordSet;
    }

    public void setIsPasswordSet(boolean isPasswordSet) {
        this.isPasswordSet = isPasswordSet;
    }

    @Nullable
    public List<String> getSquadPermissions() {
        return squadPermissions;
    }
}
