package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserCompanyCID(
    @SerializedName("pk") var pk: Int,
    @SerializedName("cid") var cid: String? = null
) : Serializable
