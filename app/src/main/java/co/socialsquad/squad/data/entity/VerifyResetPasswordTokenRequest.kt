package co.socialsquad.squad.data.entity

data class VerifyResetPasswordTokenRequest(val token: String)
