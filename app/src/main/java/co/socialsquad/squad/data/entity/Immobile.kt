package co.socialsquad.squad.data.entity

import android.os.Parcelable
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

data class Immobile(
    val active: String,
    val pk: Int,
    val active_display: String,
    val address_cep: String,
    val address_city: String,
    val address_district: String,
    val address_number: String,
    val address_complement: String?,
    val address_state: String,
    val address_street: String,
    val all_squad: String,
    val audience: String,
    val audience_type: String?,
    val name: String,
    val name_long: String,
    val owner: Owner,
    val lat: Double,
    val lng: Double,
    val detail: String,
    val room_number: String?,
    val start_price: String,
    val square_meters: String?,
    val garage_number: String?,
    val suite_number: String?,
    val can_share: Boolean,
    val medias: List<Media>,
    @SerializedName("opportunities")
    val opportunities: List<Opportunity>? = emptyList()
) : Serializable {

    @Parcelize
    data class Media(
        val file: String,
        val height: Int,
        val width: Int,
        val mime_type: String,
        val pk: Int,
        var url: String,
        val streamings: List<Streaming>
    ) : Parcelable, Serializable

    data class Owner(
        val can_call: Boolean,
        val full_name: String,
        val pk: Int,
        val qualification: String,
        val user: User
    ) : Serializable {
        data class User(
            val avatar: String,
            val chat_id: String,
            val email: String,
            val first_name: String,
            val last_name: String,
            val pk: String
        )
    }
}
