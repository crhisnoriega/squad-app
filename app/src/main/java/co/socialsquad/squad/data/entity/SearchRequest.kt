package co.socialsquad.squad.data.entity

class SearchRequest(private val query: String, private val search_type: String) {
    constructor(query: String, type: Type) : this(query, type.type)

    enum class Type(val type: String) {
        All("all"),
        Products("products"),
        Immobiles("immobile"),
        Lives("lives"),
        Videos("videos"),
        Audios("audios"),
        Events("events"),
        Stores("stores"),
        Documents("documents"),
        Trips("trips")
    }
}
