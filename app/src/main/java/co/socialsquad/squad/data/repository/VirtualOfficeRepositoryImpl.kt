package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class VirtualOfficeRepositoryImpl@Inject constructor (private val api: Api) : VirtualOfficeRepository {

    override fun getTools() = api.getTools().onDefaultSchedulers()
}
