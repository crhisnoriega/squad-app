package co.socialsquad.squad.data.remote

import co.socialsquad.squad.data.entity.AboutResponse
import co.socialsquad.squad.data.entity.AttendingUsers
import co.socialsquad.squad.data.entity.AudienceType
import co.socialsquad.squad.data.entity.Audio
import co.socialsquad.squad.data.entity.AudioList
import co.socialsquad.squad.data.entity.AuthenticationRequest
import co.socialsquad.squad.data.entity.CallRequest
import co.socialsquad.squad.data.entity.CallResponse
import co.socialsquad.squad.data.entity.CallResumeTokenRequest
import co.socialsquad.squad.data.entity.CallResumeTokenResponse
import co.socialsquad.squad.data.entity.ChartList
import co.socialsquad.squad.data.entity.ChatList
import co.socialsquad.squad.data.entity.ChatRoom
import co.socialsquad.squad.data.entity.ChatSearchRequest
import co.socialsquad.squad.data.entity.Cities
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.CompleteProfileRequest
import co.socialsquad.squad.data.entity.ConfirmResetPasswordRequest
import co.socialsquad.squad.data.entity.CreateUserResponse
import co.socialsquad.squad.data.entity.CurrentVersion
import co.socialsquad.squad.data.entity.CustomFieldRequest
import co.socialsquad.squad.data.entity.Document
import co.socialsquad.squad.data.entity.EditPasswordRequest
import co.socialsquad.squad.data.entity.EndLiveRequest
import co.socialsquad.squad.data.entity.Event
import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.entity.FeedComment
import co.socialsquad.squad.data.entity.FeedCommentRequest
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.Feedback
import co.socialsquad.squad.data.entity.FeedbackAnswer
import co.socialsquad.squad.data.entity.FindProductRequest
import co.socialsquad.squad.data.entity.FindSubdomainRequest
import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.data.entity.ImmobileResponse
import co.socialsquad.squad.data.entity.InviteMembersRequest
import co.socialsquad.squad.data.entity.InviteMembersResponse
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.data.entity.LiveCategories
import co.socialsquad.squad.data.entity.LiveCommentRequest
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.MagicLinkRequest
import co.socialsquad.squad.data.entity.MagicLinkResponse
import co.socialsquad.squad.data.entity.MagicLinkResumeRequest
import co.socialsquad.squad.data.entity.Members
import co.socialsquad.squad.data.entity.MembersRequest
import co.socialsquad.squad.data.entity.Metrics
import co.socialsquad.squad.data.entity.Neighborhoods
import co.socialsquad.squad.data.entity.PasswordResetRequest
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.PostLikesResponse
import co.socialsquad.squad.data.entity.Product
import co.socialsquad.squad.data.entity.ProductDetailGroup
import co.socialsquad.squad.data.entity.ProductsFilterRequest
import co.socialsquad.squad.data.entity.ProfileQualifications
import co.socialsquad.squad.data.entity.RecipientsRequest
import co.socialsquad.squad.data.entity.RegisterIntegration
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.entity.RegisterUserRequest
import co.socialsquad.squad.data.entity.RequirementExplanations
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.data.entity.ScheduledLiveCommentRequest
import co.socialsquad.squad.data.entity.SearchMembersRequest
import co.socialsquad.squad.data.entity.SearchV2
import co.socialsquad.squad.data.entity.Segmentation
import co.socialsquad.squad.data.entity.SegmentationPotentialReach
import co.socialsquad.squad.data.entity.SegmentationReachRequest
import co.socialsquad.squad.data.entity.SegmentationSearchRequest
import co.socialsquad.squad.data.entity.SigninRequest
import co.socialsquad.squad.data.entity.StartLiveRequest
import co.socialsquad.squad.data.entity.States
import co.socialsquad.squad.data.entity.Store
import co.socialsquad.squad.data.entity.StoreHome
import co.socialsquad.squad.data.entity.SubdomainsRequest
import co.socialsquad.squad.data.entity.SystemList
import co.socialsquad.squad.data.entity.TermsResponse
import co.socialsquad.squad.data.entity.TokenVerifyRequest
import co.socialsquad.squad.data.entity.Trip
import co.socialsquad.squad.data.entity.UnreadNotificationCount
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.UserCompanyCID
import co.socialsquad.squad.data.entity.UserCreateRequest
import co.socialsquad.squad.data.entity.Values
import co.socialsquad.squad.data.entity.VerifyResetPasswordTokenRequest
import co.socialsquad.squad.data.entity.VerifyResetPasswordTokenResponse
import co.socialsquad.squad.data.entity.VirtualOfficeResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityActionResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityInformationResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunityListResponse
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmission
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetailsResponse
import co.socialsquad.squad.data.entity.request.AudioRequest
import co.socialsquad.squad.data.entity.request.FeedCommentsResponse
import co.socialsquad.squad.data.entity.request.Notification
import co.socialsquad.squad.data.entity.request.NotificationsResponse
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo
import co.socialsquad.squad.presentation.feature.profile.edit.data.CEPResponse
import co.socialsquad.squad.presentation.feature.profile.edit.data.UserCompanyDTO
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface Api {

    // Authentication

    @GET("auth/profile/")
    fun getProfile(): Observable<UserCompany>

    @GET("auth/profile/{pk}/")
    fun getProfile(@Path("pk") pk: Int): Observable<UserCompany>

    @POST("auth/password_reset/")
    fun postPasswordReset(
        @Body passwordResetRequest: PasswordResetRequest,
        @Header("Squad-Subdomain") subdomain: String
    ): Completable

    @POST("auth/magiclink/request/")
    fun postMagicLink(@Body magicLinkRequest: MagicLinkRequest): Observable<MagicLinkResponse>

    @POST("auth/magiclink/resume/")
    fun resumeMagicLink(@Body magicLinkResumeRequest: MagicLinkResumeRequest): Observable<List<Company>>

    @POST("auth/subdomain/jwt-verify/")
    fun postTokenVerify(@Body tokenVerifyRequest: TokenVerifyRequest): Completable

    @POST("auth/token/verify/")
    fun tokenVerify(
        @Body tokenVerifyRequest: TokenVerifyRequest,
        @Header("Squad-Subdomain") subdomain: String? = null
    ): Observable<UserCompany>

    @POST("auth/token/signin/")
    fun postSignin(@Body signinRequest: SigninRequest): Observable<UserCompany>

    @POST("auth/token/refresh/")
    fun tokenRefresh(
            @Body tokenVerifyRequest: TokenVerifyRequest,
            @Header("Squad-Subdomain") subdomain: String? = null
    ): Observable<UserCompany>

    @POST("auth/jwt-signin/")
    fun postSignIn(@Body authenticationRequest: AuthenticationRequest): Observable<UserCompany>

    @GET("auth/logout/")
    fun getLogout(): Completable


    @POST("endpoints/invitations/resume/")
    fun callResumeToken(@Body body: CallResumeTokenRequest): Observable<CallResumeTokenResponse>

    @POST("endpoints/register/")
    fun registerUser(@Body signinRequest: RegisterUserRequest): Observable<UserCompany>

    @POST("auth/find/subdomain/")
    fun postFindSubdomain(@Body findSubdomainRequest: FindSubdomainRequest): Observable<Company>

    // Sign in

    @POST("endpoints/recipient/")
    fun postRecipients(@Body recipientsRequest: RecipientsRequest): Completable

    // Sign up

    @POST("endpoints/domain/register/")
    fun postUserCreate(@Body userCreateRequest: UserCreateRequest): Observable<CreateUserResponse>

    @PATCH("endpoints/user-company/{pk}/")
    fun patchUserCompany(
        @Path("pk") pk: Int,
        @Body userCompany: UserCompany
    ): Observable<UserCompany>


    @PATCH("endpoints/user-company/{pk}/")
    fun patchUserCompanyV2(
        @Path("pk") pk: Int,
        @Body userCompany: UserCompanyDTO
    ): Observable<UserCompany>

    @PATCH("endpoints/user-company/{pk}/")
    fun patchUserCompanyCID(@Path("pk") pk: Int, @Body cid: UserCompanyCID): Observable<UserCompany>

    @POST("endpoints/register/integration/")
    fun postRegisterIntegration(@Body registerIntegrationRequest: RegisterIntegrationRequest): Observable<RegisterIntegration>

    @GET("endpoints/terms/")
    fun terms(): Observable<TermsResponse>

    // Profile

    @GET("endpoints/states/")
    fun getStates(): Observable<States>

    @GET("endpoints/cities/")
    fun getCities(@Query("code") stateCode: Long): Observable<Cities>

    @GET("endpoints/neighborhoods/")
    fun getNeighborhoods(@Query("city") cityCode: Long): Observable<Neighborhoods>

    @POST("endpoints/change-password/")
    fun changePassword(@Body editPasswordRequest: EditPasswordRequest): Completable

    @GET("endpoints/profile/feed/")
    fun getProfileFeed(@Query("page") page: Int): Observable<Feed>

    @GET("endpoints/profile/feed/{pk}/")
    fun getProfileFeed(@Path("pk") pk: Int, @Query("page") page: Int): Observable<Feed>

    // Feed

    @GET("endpoints/feed/")
    fun getFeed(@Query("page") page: Int, @Query("width") width: Int): Observable<Feed>

    @GET("endpoints/domain/company/{token}/")
    fun getRegisteredCompanies(@Path("token") token: String): Observable<List<Company>>

    @POST("auth/subdomains/")
    fun getSubdomains(@Body subdomainsRequest: SubdomainsRequest): Observable<List<Company>>

    @GET("endpoints/company/feed/")
    fun getCompanyFeed(@Query("page") page: Int): Observable<Feed>

    @GET("endpoints/feed/{post_pk}/likes/")
    fun getPostLikedUsers(
        @Path("post_pk") pk: Int,
        @Query("page") page: Int
    ): Observable<PostLikesResponse>

    @POST("endpoints/feed/")
    fun postFeed(@Body feedRequest: FeedRequest): Observable<Post>

    @PUT("endpoints/feed/{pk}/")
    fun putFeed(@Path("pk") pk: Int, @Body feedRequest: FeedRequest): Observable<Post>

    @DELETE("endpoints/feed/{pk}/")
    fun deleteFeed(@Path("pk") pk: Int): Completable

    @GET("endpoints/feed/segmentation/{pk}/")
    fun getFeedSegmentation(@Path("pk") pk: Int): Observable<Segmentation>

    @POST("endpoints/feed/{pk}/like/")
    fun likePost(@Path("pk") postPk: Int): Observable<Post>

    @GET("endpoints/feed/{pk}/")
    fun getFeedItem(@Path("pk") pk: Int): Observable<Post>

    @POST("endpoints/feed/{pk}/visualized/")
    fun postFeedVisualized(@Path("pk") pk: Long): Completable

    @POST("auth/password_reset/validate_token/")
    fun verifyResetPasswordToken(
        @Body body: VerifyResetPasswordTokenRequest,
        @Header("Squad-Subdomain") subdomain: String
    ): Observable<VerifyResetPasswordTokenResponse>

    @POST("/auth/password_reset/confirm/")
    fun confirmResetPassword(
        @Body body: ConfirmResetPasswordRequest,
        @Header("Squad-Subdomain") subdomain: String
    ): Observable<VerifyResetPasswordTokenResponse>

    // Live

    @GET("endpoints/lives/{pk}/")
    fun getLive(@Path("pk") pk: Int): Observable<Live>

    @POST("endpoints/lives/{pk}/comment/")
    fun postLiveComment(
        @Path("pk") pk: Int,
        @Body liveCommentRequest: LiveCommentRequest
    ): Completable

    @POST("endpoints/lives/{pk}/join/")
    fun postLiveJoin(@Path("pk") pk: Int): Completable

    @POST("endpoints/lives/{pk}/leave/")
    fun postLiveLeave(@Path("pk") pk: Int): Completable

    @POST("endpoints/lives/{pk}/post/")
    fun postLivePost(@Path("pk") pk: Int): Completable

    @POST("endpoints/lives/")
    fun postLive(@Body liveCreateRequest: LiveCreateRequest): Observable<Live>

    @GET("endpoints/live/category/")
    fun getLiveCategories(): Observable<LiveCategories>

    @PATCH("endpoints/lives/{pk}/")
    fun endLive(@Path("pk") pk: Int, @Body endLiveRequest: EndLiveRequest): Completable

    @DELETE("endpoints/lives/{pk}/")
    fun deleteLive(@Path("pk") pk: Int): Completable

    @POST("endpoints/lives/{pk}/confirm-attendance/")
    fun liveConfirmAttendance(@Path("pk") pk: Int): Observable<Live>

    @POST("endpoints/lives/{pk}/start/")
    fun startLive(@Path("pk") pk: Int, @Body startLiveRequest: StartLiveRequest): Observable<Live>

    @GET("endpoints/lives/{pk}/attending-users/")
    fun getAttendingUsers(
        @Path("pk") pk: Int,
        @Query("page") page: Int,
        @Query("type") type: String
    ): Observable<AttendingUsers>

    @GET("endpoints/lives/")
    fun getLives(
        @Query("ended") ended: Boolean = false,
        @Query("page") page: Int
    ): Observable<Results<Live>>

    // Segmentation

    @GET("endpoints/audience/types/")
    fun getAudienceTypes(): Observable<Results<AudienceType>>

    @POST("endpoints/company/segmentation/query/{groupId}/")
    fun getMembers(
        @Path("groupId") groupId: Int,
        @Query("page") page: Int,
        @Body membersRequest: MembersRequest
    ): Observable<Members>

    @GET("endpoints/company/segmentation/available/")
    fun getSegmentationAvailable(): Observable<Metrics>

    @GET("endpoints/company/segmentation/available/range/{field}/")
    fun getSegmentationAvailableRange(
        @Path("field") field: String,
        @Query("page") page: Int
    ): Observable<Values>

    @GET("endpoints/downline/")
    fun getSegmentationMembers(@Query("page") page: Int): Observable<Members>

    @POST("endpoints/downline/search/")
    fun postSegmentationSearchMembers(
        @Query("page") page: Int,
        @Body searchMembersRequest: SearchMembersRequest
    ): Observable<Members>

    @POST("endpoints/company/segmentation/available/search/values/")
    fun getSegmentationSearchValues(
        @Body segmentationSearchRequest: SegmentationSearchRequest,
        @Query("page") page: Int
    ): Observable<Values>

    @POST("endpoints/company/segmentation/reach/")
    fun postSegmentationReach(@Body segmentationReachRequest: SegmentationReachRequest): Observable<SegmentationPotentialReach>

    // Store

    @GET("endpoints/store/home/")
    fun getStoreHome(): Observable<StoreHome>

    @GET("endpoints/store/products/")
    fun getStoreProducts(@Query("page") page: Int): Observable<Results<Product>>

    @POST("endpoints/store/products/filter/")
    fun postStoreProductsFilter(
        @Body productsFilterRequest: ProductsFilterRequest,
        @Query("page") page: Int
    ): Observable<Results<Product>>

    @GET("endpoints/store/grouped-details/{pk}/")
    fun getStoreGroupedDetails(@Path("pk") pk: Int): Observable<Results<ProductDetailGroup>>

    @GET("endpoints/store/products/{pk}/")
    fun getStoreProduct(@Path("pk") pk: Int): Observable<Product>

    @GET("endpoints/store/category/list/{pk}/")
    fun getStoreProductsCategoryList(
        @Path("pk") pk: Int,
        @Query("flat") flat: Boolean,
        @Query("page") page: Int
    ): Observable<StoreHome.Categories>

    @GET("endpoints/store/list/paginate/{pk}/")
    fun getStoreList(@Path("pk") pk: Int, @Query("page") page: Int): Observable<Results<Product>>

    @POST("endpoints/store/find/product/")
    fun postStoreFindProduct(@Body findProductRequest: FindProductRequest): Observable<Results<Store>>

    @GET("endpoints/store/products/view/{pk}/")
    fun getStoreProductsView(@Path("pk") pk: Int): Completable

    @GET("endpoints/stores/")
    fun getStores(
        @Query("lat") lat: Double,
        @Query("lng") lng: Double,
        @Query("page") page: Int
    ): Observable<Results<Store>>

    // Platform

    @GET("endpoints/platforms/{platform}/")
    fun getLastVersion(@Path("platform") platform: String): Observable<CurrentVersion>

    // Comments

    @POST("endpoints/feed/comment/")
    fun commentPost(@Body feedCommentRequest: FeedCommentRequest): Observable<FeedComment>

    @POST("endpoints/live/comment/")
    fun commentLive(@Body scheduledLiveCommentRequest: ScheduledLiveCommentRequest): Observable<FeedComment>

    @GET("endpoints/feed/{feed_pk}/comments/")
    fun getPostComments(
        @Path("feed_pk") pk: Int,
        @Query("page") page: Int
    ): Observable<FeedCommentsResponse>

    @GET("endpoints/live/{live_pk}/comments/")
    fun getLiveComments(
        @Path("live_pk") pk: Int,
        @Query("page") page: Int
    ): Observable<FeedCommentsResponse>

    @DELETE("endpoints/feed/comment/{pk}/")
    fun deleteComment(@Path("pk") pk: Long): Completable

    @DELETE("endpoints/live/comment/{pk}/")
    fun deleteLiveComment(@Path("pk") pk: Long): Completable

    // Notifications

    @GET("endpoints/notifications/not-visualized/")
    fun getNotVisualizedNotificationsAndAudios(): Observable<UnreadNotificationCount>

    @GET("endpoints/notifications/")
    fun getNotifications(@Query("page") page: Int): Observable<NotificationsResponse>

    @POST("endpoints/notifications/{pk}/clicked/")
    fun postClickedNotification(@Path("pk") pk: Long): Observable<Notification>

    @POST("endpoints/notifications/visualized/")
    fun postClearUnreadNotifications(): Completable

    // Audio

    @GET("endpoints/audio/")
    fun getAudioList(@Query("page") page: Int): Observable<AudioList>

    @GET("endpoints/audio/{pk}/")
    fun getAudio(@Path("pk") pk: Int): Observable<Audio>

    @DELETE("endpoints/audio/{pk}/")
    fun deleteAudio(@Path("pk") pk: Int): Completable

    @POST("endpoints/audio/{pk}/like/")
    fun likeAudio(@Path("pk") pk: Int): Completable

    @GET("endpoints/audio/{pk}/likes/")
    fun getAudioLikes(@Path("pk") pk: Int, @Query("page") page: Int): Observable<PostLikesResponse>

    @GET("endpoints/audio/{pk}/listeners/")
    fun getAudioPlaybackUsers(
        @Path("pk") pk: Int,
        @Query("page") page: Int,
        @Query("type") type: String
    ): Observable<PostLikesResponse>

    @POST("endpoints/audio/")
    fun postAudio(@Body audioRequest: AudioRequest): Observable<Audio>

    @POST("endpoints/audio/{pk}/listened/")
    fun postAudioListened(@Path("pk") pk: Int): Completable

    // Media

    @GET("endpoints/media/{pk}/view-count/")
    fun getMediaViewCount(@Path("pk") pk: Long): Completable

    // Dashboard

    @GET("endpoints/dashboard/")
    fun getChartList(): Observable<ChartList>

    // Videos

    @GET("endpoints/explore/videos/")
    fun getVideoList(@Query("page") page: Int): Observable<Results<Post>>

    // Set Custom Field

    @POST("endpoints/company/custom/field/{field}/set/")
    fun postCustomField(
        @Path("field") field: String,
        @Body customFieldRequest: CustomFieldRequest
    ): Completable

    // System selection List

    @GET("endpoints/systems/")
    fun getSystemList(): Observable<SystemList>

    // Qualification

    @GET("endpoints/qualifications/profile/{pk}/")
    fun getQualificationProfile(@Path("pk") pk: Int): Observable<ProfileQualifications>

    @GET("endpoints/qualifications/profile/")
    fun getQualificationProfile(): Observable<ProfileQualifications>

    @GET("endpoints/qualifications/")
    fun getQualifications(): Observable<ProfileQualifications>

    @POST("endpoints/qualifications/recommend/")
    fun postRecommendation(@Body recommendationRequest: RecommendationRequest): Completable

    @GET("endpoints/content/group/{pk}/list/")
    fun getQualificationRecommendationVideos(
        @Path("pk") pk: Int,
        @Query("page") page: Int
    ): Observable<Results<Post>>

    @GET("endpoints/content/group/{pk}/list/")
    fun getQualificationRecommendationEvents(
        @Path("pk") pk: Int,
        @Query("page") page: Int
    ): Observable<Results<Event>>

    @GET("endpoints/content/group/{pk}/list/")
    fun getQualificationRecommendationAudio(
        @Path("pk") pk: Int,
        @Query("page") page: Int
    ): Observable<Results<Audio>>

    @GET("endpoints/content/group/{pk}/list/")
    fun getQualificationRecommendationLives(
        @Path("pk") pk: Int,
        @Query("page") page: Int
    ): Observable<Results<Live>>

    @GET("endpoints/qualifications/requirements/explanations/{requirement_pk}/")
    fun getQualificationsRequirementsExplanations(@Path("requirement_pk") requirementPk: Int): Observable<RequirementExplanations>

    @PUT("endpoints/user-company/{pk}/")
    fun completeProfile(
        @Path("pk") pk: Int,
        @Body request: CompleteProfileRequest
    ): Observable<UserCompany>

    // Events

    @GET("endpoints/events/")
    fun getEvents(@Query("page") page: Int): Observable<Results<Event>>

    @GET("endpoints/events/{pk}/")
    fun getEvent(@Path("pk") pk: Long): Observable<Event>

    @POST("endpoints/events/{pk}/confirm/")
    fun postEventConfirm(@Path("pk") pk: Long): Observable<Event>

    // Chat

    @GET("endpoints/chat/")
    fun getChatList(): Observable<ChatList>

    @POST("endpoints/chat/")
    fun postChat(@Body chatSearchRequest: ChatSearchRequest): Observable<ChatList>

    @POST("endpoints/chat/create/{pk}/")
    fun postChatRoom(@Path("pk") pk: Int): Observable<ChatRoom>

    // CALL
    @POST("endpoints/call/")
    fun createCall(@Body callRequest: CallRequest): Observable<CallResponse>

    // Search

    @GET("endpoints/explore/search")
    fun search(
        @Query("search_term") searchTerm: String
    ): Observable<SearchV2>

    // Documents

    @GET("endpoints/documents/")
    fun getDocuments(): Observable<Results<Document>>

    @GET("endpoints/documents/{pk}/view/")
    fun getDocumentsView(@Path("pk") pk: Int): Completable

    // Trips

    @GET("endpoints/trips/")
    fun getTrips(): Observable<Results<Trip>>

    // Feedback

    @GET("endpoints/feedback/")
    fun getFeedback(): Observable<Feedback>

    @POST("endpoints/feedback/answer/")
    fun postFeedbackAnswer(@Body feedbackAnswer: FeedbackAnswer): Completable

    @GET("endpoints/{model}/{pk}/downline/{seen}/")
    fun getDownlineViewOrNor(
        @Path("model") model: String,
        @Path("pk") pk: Int,
        @Path("seen") type: String,
        @Query("page") page: Int
    ): Observable<Results<UserCompany>>

    @GET("endpoints/virtualoffice/")
    fun getTools(): Observable<VirtualOfficeResponse>

    @POST("endpoints/invitations/")
    fun inviteMembers(@Body body: List<InviteMembersRequest>): Observable<List<InviteMembersResponse>>

    @GET("endpoints/realestate/list/")
    fun immobiles(@Query("page") page: Int): Observable<ImmobileResponse>

    @GET("endpoints/immobile/grouped-details/{pk}/")
    fun immobilesDetails(@Path("pk") pk: Int): Observable<List<ImmobileDetail>>

    @GET("endpoints/about/infos")
    fun about(@Header("Squad-Subdomain") subdomain: String): Observable<AboutResponse>

    @GET("endpoints/about/infos")
    fun aboutNoHeader(): Observable<AboutResponse>

    @GET("endpoints/opportunity/{opportunityId}/{objectType}/{immobileId}/submissions")
    fun opportunitySubmissions(
        @Path("opportunityId") opportunityId: Int,
        @Path("immobileId") immobileId: Int,
        @Path("objectType") objectType: String
    ): Single<OpportunityListResponse>

    @GET("endpoints/opportunity/{opportunityId}/{objectType}/{immobileId}/submissions/{submissionId}")
    fun opportunitySubmissionDetails(
        @Path("opportunityId") opportunityId: Int,
        @Path("immobileId") immobileId: Int,
        @Path("submissionId") submissionId: Int,
        @Path("objectType") objectType: String
    ): Single<SubmissionDetailsResponse>

    @GET("endpoints/opportunity/{opportunityId}/action")
    fun opportunityAction(
        @Path("opportunityId") opportunityId: Int
    ): Single<OpportunityActionResponse>

    @GET("endpoints/opportunity/{opportunityId}/info")
    fun opportunityInfo(
        @Path("opportunityId") opportunityId: Int
    ): Single<OpportunityInformationResponse>

    @GET("endpoints/opportunity/stage/{stageId}/info")
    fun opportunityStageInfo(
        @Path("stageId") stageId: Int
    ): Single<OpportunityInformationResponse>

    @POST("/endpoints/opportunity/submission/")
    fun createOpportunity(@Body opportunitySubmission: OpportunitySubmission): Single<OpportunitySubmissionResponse>

    @GET
    fun queryCEP(@Url url:String): Observable<CEPResponse>

    @GET("/endpoints/wallet-tools/bank")
    fun fetchBanksInfo(): Observable<List<BankInfo>>
}
