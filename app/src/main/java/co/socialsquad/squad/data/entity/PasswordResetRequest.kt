package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class PasswordResetRequest(@SerializedName("email") var email: String)
