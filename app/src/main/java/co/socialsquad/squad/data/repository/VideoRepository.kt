package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.Results
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewModel
import io.reactivex.Completable
import io.reactivex.Observable

interface VideoRepository {
    fun getVideoList(page: Int): Observable<Results<Post>>
    fun markAsViewed(pk: Long): Completable
    fun markVideoRecommendationAsViewed(pk: Long): Completable
    fun saveVideo(videoViewModel: VideoListItemViewModel)
    fun getSavedVideo(pk: Int): VideoListItemViewModel?
}
