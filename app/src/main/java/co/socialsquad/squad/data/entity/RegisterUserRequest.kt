package co.socialsquad.squad.data.entity

data class RegisterUserRequest(val user: User, var token: String, var is_finish_corporate: Boolean) {
    data class User(
        val first_name: String? = null,
        val last_name: String? = null,
        val cell_phone: List<String?>? = null,
        val email: String? = null,
        var password: String? = null
    )
}
