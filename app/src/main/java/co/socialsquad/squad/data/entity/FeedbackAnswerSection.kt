package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedbackAnswerSection(
    @SerializedName("pk") val pk: Int,
    @SerializedName("questions") val questions: List<FeedbackAnswerQuestion>
)
