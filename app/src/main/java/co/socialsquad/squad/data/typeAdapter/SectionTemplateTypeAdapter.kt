package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class SectionTemplateTypeAdapter : TypeAdapter<SectionTemplate>() {
    override fun write(writer: JsonWriter?, value: SectionTemplate?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.value(SectionTemplate.getDefault().value)
    }

    override fun read(reader: JsonReader?): SectionTemplate {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> SectionTemplate.getByValue(it.nextString())
                else -> SectionTemplate.getDefault()
            }
        } ?: SectionTemplate.getDefault()
    }
}
