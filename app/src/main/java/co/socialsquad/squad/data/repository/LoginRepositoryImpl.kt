package co.socialsquad.squad.data.repository

import android.util.Log
import co.socialsquad.squad.data.entity.*
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.presentation.App
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val gson: Gson,
    private val api: Api,
    private val preferences: Preferences
) : LoginRepository {

    override var squadName: String?
        get() = preferences.squadName
        set(value) {
            preferences.squadName = value
        }

    override var subdomain: String?
        get() = with(preferences) {
            subdomain
        }
        set(value) {
            with(preferences) {
                subdomain = value
            }
        }

    override var userCompany: UserCompany?
        get() {
            val userCompany = gson.fromJson(preferences.userCompany, UserCompany::class.java)
            if (App.companyColor != null) {
                userCompany?.company?.let {
                    App.companyColor = CompanyColor(
                        it.primaryColor ?: Company.DEFAULT_PRIMARY_COLOR,
                        it.secondaryColor
                    )
                }
            }

            if (App.avatars != null) {
                userCompany?.company?.webView?.let {
                    App.avatars = it
                }
            }
            return userCompany
        }
        set(value) {
            App.companyColor = CompanyColor(
                value?.company?.primaryColor ?: Company.DEFAULT_PRIMARY_COLOR,
                value?.company?.secondaryColor
            )
            App.avatars = value?.company?.webView
            preferences.userCompany = gson.toJson(value)
        }

    override var token: String?
        get() = with(preferences) {
            token
        }
        set(value) {
            Log.i("token", "token: " + value)
            with(preferences) {
                token = value
            }
        }

    override var squadColor: String?
        get() = preferences.squadColor
        set(value) {
            preferences.squadColor = value
        }

    override fun getDeviceToken(): Observable<String>? =

        Observable.create { emitter ->
            FirebaseInstanceId.getInstance().instanceId
                .addOnSuccessListener { emitter.onNext(it.token) }
                .addOnFailureListener { emitter.onError(Throwable()) }
                .addOnCanceledListener { emitter.onError(Throwable()) }
                .addOnCompleteListener { emitter.onComplete() }
        }

    override fun authenticate(authenticationRequest: AuthenticationRequest) =
        api.postSignIn(authenticationRequest).onDefaultSchedulers()

    override fun resetPassword(passwordResetRequest: PasswordResetRequest, subdomain: String) =
        api.postPasswordReset(passwordResetRequest, subdomain).onDefaultSchedulers()

    override fun magicLink(magicLinkRequest: MagicLinkRequest) =
        api.postMagicLink(magicLinkRequest).onDefaultSchedulers()

    override fun verifyMagicLinkToken(magicLinkResumeRequest: MagicLinkResumeRequest) =
        api.resumeMagicLink(magicLinkResumeRequest).onDefaultSchedulers()

    override fun verifyToken(tokenVerifyRequest: TokenVerifyRequest): Completable {
        Log.i("token", "called")
        return api.postTokenVerify(tokenVerifyRequest).onDefaultSchedulers()
    }

    override fun registerDevice(recipientsRequest: RecipientsRequest): Completable {
        Log.i("token", "recipientsRequest: ${Gson().toJson(recipientsRequest)}")
        return api.postRecipients(recipientsRequest).onDefaultSchedulers()
    }

    override fun editPassword(editPasswordRequest: EditPasswordRequest) =
        api.changePassword(editPasswordRequest).onDefaultSchedulers()

    override fun logout() = api.getLogout().onDefaultSchedulers()

    override fun clearSharingAppDialogTimer() {
        preferences.interactionDialogTimeInMillis = 0L
    }

    override fun findSubdomain(findSubdomainRequest: FindSubdomainRequest) =
        api.postFindSubdomain(findSubdomainRequest).onDefaultSchedulers()

    override fun signin(signinRequest: SigninRequest) =
        api.postSignin(signinRequest).onDefaultSchedulers()

    override fun authVerifyToken(
        tokenVerifyRequest: TokenVerifyRequest,
        subdomain: String?
    ): Observable<UserCompany> =
        api.tokenVerify(tokenVerifyRequest, subdomain).onDefaultSchedulers()

    override fun authRefreshToken(
        tokenVerifyRequest: TokenVerifyRequest,
        subdomain: String?
    ) = api.tokenRefresh(tokenVerifyRequest, subdomain).onDefaultSchedulers()

    override fun register(body: RegisterUserRequest): Observable<UserCompany> =
        api.registerUser(body).onDefaultSchedulers()

    override fun callResume(body: CallResumeTokenRequest): Observable<CallResumeTokenResponse> =
        api.callResumeToken(body).onDefaultSchedulers()

    override fun getRegisteredCompanies(token: String): Observable<List<Company>> =
        api.getRegisteredCompanies(token).onDefaultSchedulers()

    override fun getSubdomains(subdomainsRequest: SubdomainsRequest): Observable<List<Company>> =
        api.getSubdomains(subdomainsRequest).onDefaultSchedulers()

    override fun verifyResetPasswordToken(
        body: VerifyResetPasswordTokenRequest,
        subdomain: String
    ) =
        api.verifyResetPasswordToken(body, subdomain).onDefaultSchedulers()

    override fun confirmResetPassword(
        body: ConfirmResetPasswordRequest,
        subdomain: String
    ): Observable<VerifyResetPasswordTokenResponse> =
        api.confirmResetPassword(body, subdomain).onDefaultSchedulers()

    override fun about(subdomain: String): Observable<AboutResponse> =
        api.about(subdomain).onDefaultSchedulers()

    override fun aboutNoHeader(): Observable<AboutResponse> =
        api.aboutNoHeader().onDefaultSchedulers()
}
