package co.socialsquad.squad.data.remote

import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.integration.RecruitRequest
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiIntegration {
    @POST("/recruit/")
    fun postRecruit(@Body recruitRequest: RecruitRequest): Observable<UserCompany>
}
