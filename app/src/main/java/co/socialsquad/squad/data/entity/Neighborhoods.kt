package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class Neighborhoods(
    @SerializedName("neighborhoods") var neighborhoods: List<Neighborhood>? = null
)
