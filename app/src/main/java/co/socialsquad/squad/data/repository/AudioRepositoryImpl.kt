package co.socialsquad.squad.data.repository

import android.content.ContentResolver
import android.net.Uri
import android.webkit.MimeTypeMap
import co.socialsquad.squad.data.entity.Audio
import co.socialsquad.squad.data.entity.request.AudioRequest
import co.socialsquad.squad.data.local.LocalStorage
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiUpload
import co.socialsquad.squad.data.remote.ProgressRequestBody
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import java.io.File
import javax.inject.Inject

class AudioRepositoryImpl @Inject constructor(
    private val api: Api,
    private val upload: ApiUpload,
    private val localStorage: LocalStorage,
    private val contentResolver: ContentResolver,
    private val preferences: Preferences,
    private val gson: Gson
) : AudioRepository {
    override var recordedAudio: Uri?
        get() = Uri.parse(preferences.audioURI)
        set(createMediaUri) {
            preferences.audioURI = createMediaUri.toString()
        }

    override fun createAudio(audioRequest: AudioRequest) = api.postAudio(audioRequest).onDefaultSchedulers()

    override fun sendAudioFile(pk: Int, fileUri: Uri, callback: ProgressRequestBody.UploadCallbacks): Completable {
        val file = File(fileUri.path)

        val mediaType = if (fileUri.scheme == ContentResolver.SCHEME_CONTENT) {
            val type = contentResolver.getType(fileUri) ?: throw Throwable("Unresolved Part")
            type.toMediaTypeOrNull()
        } else {
            val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString())
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
                ?: throw Throwable("Unresolved mime type")
            mimeType.toMediaTypeOrNull()
        }

        val progressRequestBody = ProgressRequestBody(file, mediaType, callback)
        val part = MultipartBody.Part.createFormData("files", file.name, progressRequestBody)

        return upload.postAudioUpload(pk, part).onDefaultSchedulers()
    }

    override fun createGalleryFile(mimeType: String) = localStorage.createTemporaryGalleryFile(mimeType)

    override fun deleteGalleryFile() = localStorage.deleteTemporaryGalleryFile()

    override fun deleteAudio(pk: Int) = api.deleteAudio(pk).onDefaultSchedulers()

    override fun likeAudio(pk: Int) = api.likeAudio(pk).onDefaultSchedulers()

    override fun getAudioLikes(pk: Int, page: Int) = api.getAudioLikes(pk, page).onDefaultSchedulers()

    override fun getAudioPlaybackUsers(pk: Int, page: Int, type: String) = api.getAudioPlaybackUsers(pk, page, type).onDefaultSchedulers()

    override fun getAudioList(page: Int) = api.getAudioList(page).onDefaultSchedulers()

    override fun getAudio(pk: Int): Observable<Audio> = api.getAudio(pk).onDefaultSchedulers()

    override fun markAsListened(pk: Int): Completable = api.postAudioListened(pk).onDefaultSchedulers()

    override fun createTemporaryAudioFile() = localStorage.createTemporaryAudioFile()

    override fun deleteTemporaryAudioFile() = localStorage.deleteTemporaryAudioFile()

    override fun saveAudio(audioViewModel: AudioViewModel) {
        audioList = audioList?.apply {
            find { it.pk == audioViewModel.pk }?.let { remove(it) }
            add(audioViewModel)
        }
    }

    override fun getSavedAudio(pk: Int) = audioList?.find { it.pk == pk }

    private var audioList: MutableList<AudioViewModel>?
        get() {
            if (preferences.audioList == null) preferences.audioList = gson.toJson(mutableListOf<AudioViewModel>())
            val type = object : TypeToken<MutableList<AudioViewModel>>() {}.type
            return gson.fromJson(preferences.audioList, type) as MutableList<AudioViewModel>?
        }
        set(list) {
            preferences.audioList = gson.toJson(list)
        }
}
