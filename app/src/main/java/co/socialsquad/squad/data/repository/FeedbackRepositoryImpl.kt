package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.FeedbackAnswer
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import javax.inject.Inject

class FeedbackRepositoryImpl @Inject constructor(private val api: Api) : FeedbackRepository {
    override fun getFeedback() =
        api.getFeedback().onDefaultSchedulers()

    override fun sendAnswer(feedbackAnswer: FeedbackAnswer) =
        api.postFeedbackAnswer(feedbackAnswer).onDefaultSchedulers()
}
