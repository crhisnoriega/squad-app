package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class SigninRequest(
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String
)
