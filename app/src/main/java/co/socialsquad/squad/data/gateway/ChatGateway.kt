package co.socialsquad.squad.data.gateway

import co.socialsquad.squad.data.entity.ChatStateMessage
import co.socialsquad.squad.data.entity.database.ChatMessage
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import io.reactivex.Observable

interface ChatGateway {
    fun login(userId: String): Observable<String>
    fun logout(onDisconnected: () -> Unit)
    fun connectToChatChannel(chatRoom: ChatRoomModel): Observable<ChatRoomModel>
    fun setSenderObservable(channelUrl: String, sendMessageObservable: Observable<String>): Observable<ChatMessage>
    fun getUnreadCount(): Observable<Int>
    fun disconnectRoomAndChannel(channelUrl: String)
    fun disconnect()
    fun getUnreadCount(channelUrl: String): Int
    fun getMessages(): Observable<ChatMessage>
    fun markAsRead(channelUrl: String)
    fun updateRoom(chatRoomModel: ChatRoomModel): ChatRoomModel
    fun getChatStateListener(): Observable<ChatStateMessage>
    fun getRoomObserver(): Observable<Pair<String, Boolean>>
    fun getMessagesFromDate(date: Long): Observable<ChatMessage>
    fun getMessagesBeforeDate(channelUrl: String, date: Long): Observable<ChatMessage>
}
