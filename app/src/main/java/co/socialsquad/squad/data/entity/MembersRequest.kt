package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class MembersRequest(@SerializedName("value") val value: String)

data class SearchMembersRequest(@SerializedName("search") val value: String)
