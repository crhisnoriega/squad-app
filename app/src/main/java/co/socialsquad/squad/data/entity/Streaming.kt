package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Streaming(
    @SerializedName("pk") var pk: Int = 0,
    @SerializedName("file_url") var fileUrl: String? = null,
    @SerializedName("thumbnail_url") var thumbnailUrl: String?,
    @SerializedName("job") var job: Job? = null,
    @SerializedName("playlist_url") var playlistUrl: String?
) : Parcelable, Serializable
