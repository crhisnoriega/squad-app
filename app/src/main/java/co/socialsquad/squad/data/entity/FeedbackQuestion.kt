package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedbackQuestion(
    @SerializedName("pk") val pk: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("content") val content: String?,
    @SerializedName("icon") val icon: String?,
    @SerializedName("type") val type: String?,
    @SerializedName("items") val items: List<FeedbackItem?>?
) {
    fun type() = Type.values().find { it.value == type }

    enum class Type(val value: String) {
        OpenText("OT"),
        SingleChoice("SC"),
        MultipleChoice("MC")
    }
}
