package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Job(
    @SerializedName("id") var id: String,
    @SerializedName("state") var state: String? = null,
    @SerializedName("state_display") var stateDisplay: String? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("created_at") var createdAt: String,
    @SerializedName("last_modified") var lastModified: String? = null
) : Serializable
