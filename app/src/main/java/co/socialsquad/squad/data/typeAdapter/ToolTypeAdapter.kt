package co.socialsquad.squad.data.typeAdapter

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class ToolTypeAdapter : TypeAdapter<RankingType>() {
    override fun write(writer: JsonWriter?, value: RankingType?) {
        value?.let {
            writer?.value(it.value)
        } ?: writer?.nullValue()
    }

    override fun read(reader: JsonReader?): RankingType {
        return reader?.let {
            return when (reader.peek()) {
                JsonToken.STRING -> RankingType.getByValue(it.nextString())
                else -> RankingType.Invalid
            }
        } ?: RankingType.Invalid
    }
}
