package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Location(
    @SerializedName("address")
    val address: String? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("lat")
    val lat: Double? = null,
    @SerializedName("lng")
    val lng: Double? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("pk")
    val pk: Int? = null
)
