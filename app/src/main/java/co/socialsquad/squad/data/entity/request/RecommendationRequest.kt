package co.socialsquad.squad.data.entity.request

import com.google.gson.annotations.SerializedName

data class RecommendationRequest(
    @SerializedName("obj_pk") var objectPk: Int = -1,
    @SerializedName("model") var model: String? = null,
    @SerializedName("qualification_pks") var qualifications: List<Int> = listOf()
)
