package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class State(
    @SerializedName("code") var code: Long,
    @SerializedName("name") var name: String? = null,
    @SerializedName("sigla") var sigla: String? = null
)
