package co.socialsquad.squad.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class CompleteProfileItem(
    @SerializedName("visible") var visible: Boolean?,
    @SerializedName("completed") var completed: Boolean?,
    @SerializedName("rg") var rg: Boolean?,
    @SerializedName("cpf") var cpf: Boolean?,
    @SerializedName("birthday") var birthday: Boolean?,
    @SerializedName("gender") var gender: Boolean?,
    @SerializedName("address") var address: Boolean?

):Parcelable