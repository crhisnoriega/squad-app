package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Prize(
    @SerializedName("category") val category: String,
    @SerializedName("description") val description: String,
    @SerializedName("name") val name: String,
    @SerializedName("media") val media: Media?
)
