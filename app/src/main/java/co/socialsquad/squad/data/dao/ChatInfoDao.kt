package co.socialsquad.squad.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.socialsquad.squad.data.entity.database.CHAT_INFO_TABLE_NAME
import co.socialsquad.squad.data.entity.database.ChatInfoModel
import io.reactivex.Maybe

@Dao
interface ChatInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertInfo(chatInfoDao: ChatInfoModel)

    @Query("SELECT * FROM $CHAT_INFO_TABLE_NAME WHERE configId = :configId")
    fun getChatInfo(configId: Int): Maybe<ChatInfoModel>

    @Query("SELECT * FROM $CHAT_INFO_TABLE_NAME WHERE configId = :configId")
    fun getChatInfoLiveData(configId: Int): LiveData<ChatInfoModel>

    @Query("SELECT lastSync FROM $CHAT_INFO_TABLE_NAME WHERE configId = :configId")
    fun getLastSync(configId: Int): Long?
}
