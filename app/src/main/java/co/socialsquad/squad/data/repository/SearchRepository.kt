package co.socialsquad.squad.data.repository

import co.socialsquad.squad.data.entity.SearchV2
import io.reactivex.Observable

interface SearchRepository {
    fun search(searchTerm: String): Observable<SearchV2>
}
