package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class FeedRequest(
    @SerializedName("title") val title: String? = null,
    @SerializedName("content") val content: String? = null,
    @SerializedName("type") val type: String,
    @SerializedName("segmentation") val segmentation: SegmentationRequest? = null,
    @SerializedName("category") val category: LiveCategory? = null,
    @SerializedName("scheduled") var scheduled: ScheduledPostRequest? = null
)
