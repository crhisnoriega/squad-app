package co.socialsquad.squad.data.utils

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

class TagWorker @Inject constructor(private val analytics: FirebaseAnalytics) {

    companion object {
        // HINODE ID CONFIRMATION
        const val TAG_SCREEN_VIEW_CID_CONFIRMATION = "screen_view_cid_confirmation"
        const val TAG_TAP_CID_CONFIRMATION = "tap_cid_confirmation"

        // NAVIGATION
        const val TAG_SCREEN_VIEW_NAVIGATION = "screen_view_navigation"

        // AUTH
        const val TAG_SCREEN_VIEW_RESET_PASSWORD = "screen_view_reset_password"
        const val TAG_SCREEN_VIEW_MAGIC_LINK = "screen_view_magic_link"
        const val TAG_TAP_LOGOUT = "tap_logout"
        const val TAG_TAP_CHANGE_PASSWORD = "tap_change_pass"
        const val TAG_TAP_SHARE_APP = "tap_share_app"

        // SOCIAL
        const val TAG_SCREEN_VIEW_SOCIAL = "screen_view_social"
        const val TAG_TAP_CREATE_POST = "tap_create_post"
        const val TAG_TAP_CREATE_CAMERA_POST = "tap_create_camera_post"
        const val TAG_TAP_CREATE_MOVIE_POST = "tap_create_movie_post"
        const val TAG_TAP_CREATE_GALLERY_POST = "tap_create_gallery_post"
        const val TAG_TAP_CREATE_LIVE_POST = "tap_create_live_post"
        const val TAG_TAP_SOCIAL_LIKE_POST = "tap_social_like_post"
        const val TAG_TAP_SOCIAL_SHARE_POST = "tap_social_share_post"

        // SOCIAL CREATE
        const val TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO = "tap_schedule_promotional_video"
        const val TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_CLEAR = "tap_schedule_promotional_video_clear"
        const val TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_EDIT = "tap_schedule_promotional_video_edit"
        const val TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_PLAY = "tap_schedule_promotional_video_play"
        const val TAG_TAP_SCHEDULE_PROMOTIONAL_IMAGE = "tap_schedule_promotional_image"

        // POSTS
        const val TAG_TAP_POST_DETAIL_COMMENT = "tap_post_detail_comment"
        const val TAG_TAP_POST_DETAIL_SHARE = "tap_post_detail_share"
        const val TAG_TAP_POST_SEGMENT_PUBLIC = "tap_post_segment_public"
        const val TAG_TAP_POST_SEGMENT_MEMBERS = "tap_post_segment_members"
        const val TAG_TAP_POST_SEGMENT_METRICS = "tap_post_segment_metrics"
        const val TAG_TAP_POST_SEGMENT_DOWNLINE = "tap_post_segment_downline"

        // ACTIVE LIVE
        const val TAG_TAP_LIVE_COMMENT = "tap_live_comment"
        const val TAG_TAP_LIVE_SHARE = "tap_live_share"

        // EXPLORE
        const val TAG_SCREEN_VIEW_EXPLORE = "screen_view_explore"

        // PRODUCTS
        const val TAG_SCREEN_VIEW_PRODUCT_LIST = "screen_view_product_list"
        const val TAG_SCREEN_VIEW_PRODUCT_DETAIL = "screen_view_product_detail"
        const val TAG_SCREEN_VIEW_MAP = "screen_view_map"
        const val TAG_TAP_DISTRIBUTOR_PHONE = "tap_distributor_phone"

        // NOTIFICATIONS
        const val TAG_SCREEN_VIEW_NOTIFICATIONS = "screen_view_notifications"
        const val TAG_NOTIFICATION_CLICK_NOTIFICATIONS_SCREEN =
            "notification_click_notifications_screen"

        // PROFILE
        const val TAG_SCREEN_VIEW_PROFILE = "screen_view_profile"
        const val TAG_SCREEN_VIEW_PROFILE_EDIT = "screen_view_profile_edit"

        // COMPANY FEED
        const val TAG_SCREEN_VIEW_COMPANY_FEED = "screen_view_company_feed"
        const val TAG_TAP_COMPANY_LIKE_POST = "tap_company_feed_like_post"
        const val TAG_TAP_COMPANY_SHARE_POST = "tap_company_feed_share_post"

        // DROPS
        const val TAG_SCREEN_VIEW_DROP_LIST = "screen_view_drop_list"
        const val TAG_SCREEN_VIEW_DROP_DETAIL = "screen_view_drop_detail"
        const val TAG_API_RESPONSE_DROP_UPLOAD = "api_response_drop_upload"
        const val TAG_TAP_CREATE_RECORD_DROP = "tap_create_record_drop"
        const val TAG_TAP_CREATE_GALLERY_DROP = "tap_create_gallery_drop"
        const val TAG_TAP_LIKE_DROP = "tap_like_drop"
        const val TAG_TAP_PLAY_DROP = "tap_play_drop"
        const val TAG_TAP_DELETE_DROP = "tap_delete_drop"

        // SCHEDULED LIVE
        const val TAG_SCREEN_VIEW_SCHEDULED_LIVE_DETAILS = "screen_view_scheduled_live_details"
        const val TAG_API_RESPONSE_SCHEDULED_LIVE_DELETE = "api_response_scheduled_live_delete"
        const val TAG_TAP_LIVE_CONFIRM_ATTENDANCE = "tap_live_confirm_attendance"
        const val TAG_TAP_LIVE_DELETE = "tap_live_delete"
        const val TAG_TAP_SCHEDULED_LIVE_COMMENT = "tap_scheduled_live_comment"
        const val TAG_TAP_SCHEDULED_LIVE_DELETE_COMMENT = "tap_scheduled_live_delete_comment"
        const val TAG_TAP_SCHEDULED_LIVE_SHARE = "tap_scheduled_live_share"

        // SYSTEM SELECTION
        const val TAG_SCREEN_VIEW_SYSTEM_SELECTION = "screen_view_system_selection"

        // CHAT
        const val TAG_SCREEN_VIEW_CHAT_LIST = "screen_view_chat_list"
        const val TAG_SCREEN_VIEW_CHAT_ROOM = "screen_view_chat_room"

        // Dashboard
        const val TAG_SCREEN_VIEW_DASHBOARD = "screen_view_dashboard"

        // Lives
        const val TAG_SCREEN_VIEW_LIVE_LIST = "screen_view_live_list"

        // Lives
        const val TAG_SCREEN_VIEW_VIDEO_LIST = "screen_view_video_list"
        const val TAG_TAP_PLAY_VIDEO = "tap_play_video"

        // Qualification
        const val TAG_SCREEN_VIEW_PROFILE_QUALIFICATION = "screen_view_profile_qualification"
        const val TAG_TAP_QUALIFICATION_DETAIL = "tap_qualification_detail"
        const val TAG_SCREEN_VIEW_QUALIFICATION_RECOMMENDATIONS =
            "screen_view_qualification_recommendations"
        const val TAG_SCREEN_VIEW_QUALIFICATION_REQUIREMENT =
            "screen_view_qualification_requirement"

        // User List
        const val TAG_SCREEN_VIEW_DROP_PLAYBACK_LIST = "screen_view_drop_playback_list"
        const val TAG_SCREEN_VIEW_CONFIRMED_ATTENDANCE_LIST =
            "screen_view_confirmed_attendance_list"
        const val TAG_SCREEN_VIEW_POST_LIKE_LIST = "screen_view_post_like_list"
        const val TAG_SCREEN_VIEW_DROP_LIKE_LIST = "screen_view_drop_like_list"
        const val TAG_SCREEN_VIEW_SPEAKER_LIST = "screen_view_speaker_list"
        const val TAG_SCREEN_VIEW_VIP_LIST = "screen_view_vip_list"
        const val TAG_SCREEN_VIEW_EVENT_CONFIRMED_LIST = "screen_view_event_confirmed_list"
        const val TAG_SCREEN_VIEW_DOCUMENT_VIEWER_LIST = "screen_view_document_viewer_list"
        const val TAG_SCREEN_VIEW_FEED_VISUALIZATION_LIST = "screen_view_feed_visualization_list"
        const val TAG_SCREEN_VIEW_LIVE_VIEWS_LIST = "screen_view_live_views_list"
        const val TAG_TAP_ATTENDING_LIVE_SEEN_TAB = "tap_attending_live_seen_tab"
        const val TAG_TAP_ATTENDING_LIVE_NOT_SEEN_TAB = "tap_attending_live_not_seen_tab"
        const val TAG_TAP_DROP_PLAYBACKS_SEEN_TAB = "tap_drop_playbacks_seen_tab"
        const val TAG_TAP_DROP_PLAYBACKS_NOT_SEEN_TAB = "tap_drop_playbacks_not_seen_tab"
        const val TAG_REQUEST_DROP_PLAYBACK_LIST = "request_drop_playback_list"
        const val TAG_TAP_EVENT_CONFIRMED_SEEN_TAB = "tap_event_confirmed_seen_tab"
        const val TAG_TAP_EVENT_CONFIRMED_NOT_SEEN_TAB = "tap_event_confirmed_not_seen_tab"
        const val TAG_TAP_FEED_VISUALIZATION_NOT_SEEN_TAB = "tap_feed_visualization_not_seen_tab"
        const val TAG_TAP_FEED_VISUALIZATION_SEEN_TAB = "tap_feed_visualization_seen_tab"
        const val TAG_TAP_DOCUMENT_VIEWER_SEEN_TAB = "tap_documentviewer_seen_tab"
        const val TAG_TAP_DOCUMENT_VIEWER_NOT_SEEN_TAB = "tap_documentviewer_not_seen_tab"
        const val TAG_TAP_UNKNOW_NOT_SEEN_TAB = "tap_unknow_not_seen_tab"
        const val TAG_TAP_UNKNOW_SEEN_TAB = "tap_unknow_seen_tab"

        // Event List
        const val TAG_SCREEN_VIEW_EVENT_LIST = "screen_view_event_list"
        const val TAG_TAP_EVENT_SHARE = "tap_event_share"
        const val TAG_TAP_EVENT_CONFIRM_ATTENDANCE = "tap_event_confirm_attendance"
        const val TAG_TAP_EVENT_CANCEL_ATTENDANCE = "tap_event_cancel_attendance"

        // Franchise List
        const val TAG_SCREEN_VIEW_STORE_LIST = "screen_view_store_list"
        const val TAG_TAP_STORE_CALL = "tap_store_call"

        // Document List
        const val TAG_SCREEN_VIEW_DOCUMENT_LIST = "screen_view_document_list"
        const val TAG_SCREEN_VIEW_TRIP_LIST = "screen_view_trip_list"

        // Call
        const val TAG_SCREEN_VIEW_CALL_ROOM = "screen_view_call_room"

        // Feedback
        const val TAG_SCREEN_VIEW_FEEDBACK = "screen_view_feedback"

        // Login Subdomain
        const val TAG_SCREEN_VIEW_LOGIN_SUBDOMAIN = "screen_view_login_subdomain"

        // Login Email
        const val TAG_SCREEN_VIEW_LOGIN_EMAIL = "screen_view_login_email"

        // Login Password
        const val TAG_SCREEN_VIEW_LOGIN_PASSWORD = "screen_view_login_password"
    }

    fun tagEvent(tag: String, params: Bundle = Bundle()) {
//        analytics.logEvent(tag, params)
    }

    fun tagLoginEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "api")
//        tagEvent(FirebaseAnalytics.Event.LOGIN, bundle)
    }

    fun tagSignUpEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "api")
//        tagEvent(FirebaseAnalytics.Event.SIGN_UP, bundle)
    }
}
