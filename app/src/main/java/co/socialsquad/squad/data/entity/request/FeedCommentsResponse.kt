package co.socialsquad.squad.data.entity.request

import co.socialsquad.squad.data.entity.FeedComment
import com.google.gson.annotations.SerializedName

class FeedCommentsResponse(
    @SerializedName("count") var count: Int,
    @SerializedName("next") var next: String? = null,
    @SerializedName("results") var results: List<FeedComment>? = null
)
