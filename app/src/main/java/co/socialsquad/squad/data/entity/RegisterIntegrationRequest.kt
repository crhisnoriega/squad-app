package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class RegisterIntegrationRequest(
    @SerializedName("cid") val cid: Long,
    @SerializedName("password") val password: String,
    @SerializedName("email") val email: String
)
