package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class Feedback(
    @SerializedName("pk") val pk: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("content") val content: String?,
    @SerializedName("icon") val icon: String?,
    @SerializedName("sections") val sections: List<FeedbackSection?>?
)
