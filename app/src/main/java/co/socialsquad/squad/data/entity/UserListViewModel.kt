package co.socialsquad.squad.data.entity

import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel

class UserListViewModel(
    val completed: Boolean,
    val cont: Int,
    val users: List<UserViewModel>
)
