package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

class Suggestions(
    @SerializedName("pk") val pk: Int,
    @SerializedName("title") val title: String?,
    @SerializedName("content") val content: String?,
    @SerializedName("icon") val icon: String?,
    @SerializedName("total") val total: Int,
    @SerializedName("progress") val progress: Int,
    @SerializedName("type") val type: String
)
