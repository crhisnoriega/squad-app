package co.socialsquad.squad.data.entity

import com.google.gson.annotations.SerializedName

data class CustomFieldRequest(
    @SerializedName("value") var value: String
)
