package co.socialsquad.squad.data.repository

import android.net.Uri
import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.entity.FeedComment
import co.socialsquad.squad.data.entity.FeedCommentRequest
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.PostLikesResponse
import co.socialsquad.squad.data.entity.Segmentation
import co.socialsquad.squad.data.entity.UnreadNotificationCount
import co.socialsquad.squad.data.entity.request.FeedCommentsResponse
import co.socialsquad.squad.data.entity.request.Notification
import co.socialsquad.squad.data.entity.request.NotificationsResponse
import co.socialsquad.squad.data.remote.ProgressRequestBody
import io.reactivex.Completable
import io.reactivex.Observable
import java.io.File

interface SocialRepository {
    var audioEnabled: Boolean
    var createMedia: Uri?
    fun getFeed(page: Int, width: Int): Observable<Feed>
    fun getSegmentation(pk: Int): Observable<Segmentation>
    fun postFeed(feedRequest: FeedRequest): Observable<Post>
    fun postFeedUpload(pk: Int, fileUri: Uri, callback: ProgressRequestBody.UploadCallbacks): Completable
    fun editPost(pk: Int, feedRequest: FeedRequest): Observable<Post>
    fun deleteFeed(pk: Int): Completable
    fun createPhotoFile(): File
    fun createVideoFile(): File
    fun createGalleryFile(mimeType: String): File
    fun deleteCreateFiles()
    fun likePost(pk: Int): Observable<Post>
    fun getFeedUsersLiked(postPk: Int, page: Int): Observable<PostLikesResponse>
    fun getFeedItem(pk: Int): Observable<Post>
    fun getNotifications(page: Int): Observable<NotificationsResponse>
    fun clickNotification(pk: Long): Observable<Notification>
    fun clearUnreadNotifications(): Completable
    fun commentPost(feedCommentRequest: FeedCommentRequest): Observable<FeedComment>
    fun getPostComments(feedPk: Int, page: Int): Observable<FeedCommentsResponse>
    fun deleteComment(pk: Long): Completable
    fun getUnreadItems(): Observable<UnreadNotificationCount>
}
