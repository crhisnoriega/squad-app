package co.socialsquad.squad.presentation.feature.store.product

import co.socialsquad.squad.presentation.custom.ViewModel

interface ProductView {
    fun setupTitle(title: String)
    fun setupList(color: String?)
    fun setItems(items: ArrayList<ViewModel>)
    fun checkPermissions(permissions: Array<String>): Boolean
    fun requestPermissions(requestCode: Int, permissions: Array<String>)
    fun showLoading()
    fun hideLoading()
    fun showMessage(resId: Int, onDismissListener: () -> Unit = {})
    fun createLocationRequest()
    fun verifyLocationServiceAvailability()
}
