package co.socialsquad.squad.presentation.feature.searchv2

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.SectionsViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionList
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.feature.searchv2.results.SearchResultsFragment
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.getScreenWidth
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.view_search_toolbar.tToolbar
import kotlinx.android.synthetic.main.view_search_toolbar.tlTabs
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import kotlinx.android.synthetic.main.view_search_toolbar_with_cancel.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import javax.inject.Inject


class SearchActivity : BaseDaggerActivity(), HasSupportFragmentInjector {

    @ExperimentalCoroutinesApi
    private val sectionsViewModel: SectionsViewModel by viewModel()

    @ExperimentalCoroutinesApi
    private val searchViewModel: SearchViewModel by viewModel()

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var loginRepository: LoginRepository

    private val fragments = mutableListOf<SearchResultsFragment>()

    override fun supportFragmentInjector() = fragmentInjector

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    companion object {
        var animationWasStated = false
    }

    override fun onPause() {
        super.onPause()
        // overridePendingTransition(0, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setupSectionsObserver()
        setupResultsObserver()
        setupResultsBySectionObserver()
        setupEmptySectionSearch()
        sectionsViewModel.fetchSections()
        setupToolbar()
    }

    private var wasClear = false


    private fun setupToolbar() {
        setSupportActionBar(tToolbar)

        // ibClear.visibility = View.VISIBLE
        ibClear.setOnClickListener {
            fragments.forEach {
                it.didSearch = false
                it.setupEmptyState()
            }
            wasClear = true
            etSearch.setText("")
            ibClear.visibility = View.GONE
        }

        cancel_search.setOnClickListener {
            fragments.forEach {
                it.didSearch = false
            }
            doFadeOutAnimation()
        }

        cancel_search_text.setOnClickListener {
            fragments.forEach {
                it.didSearch = false
            }
            doFadeOutAnimation()
        }

        etSearch.apply {
            requestFocus()
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                ) {
                }

                override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                    if (wasClear.not()) {
                        tryDoSearch(text.toString())
                    }
                    wasClear = false
                }
            })
        }
    }

    private val handler = Handler()

    private fun tryDoSearch(query: String) {
        Log.i("search", "do searching... $query")

        // show loading
        var currentFragment = fragments[vpContent.currentItem]
        currentFragment.setupLoadingState(query)

        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            fragments.forEach {
                it.didSearch = false
            }
            ibClear.visibility = View.VISIBLE
            searchQuery(query)
        }, 800)
    }

    private fun showKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
    }

    private fun setupSections(sections: List<Section>) {
        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(
                                    ContextCompat.getColor(
                                            context,
                                            R.color.darkjunglegreen_28
                                    )
                            )
                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_bold.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        } else {
                            setTextColor(ContextCompat.getColor(context, R.color.oldlavender))
                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_medium.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        }
                    }
                }
            }
        })

        vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                tlTabs.getTabAt(position)?.select()
                searchQuery(etSearch.text.toString())
            }
        })

        // Setup First Tab (All)
        fragments.add(setupEmptyFragment(null))

        tlTabs.addTab(setupNewTab(null))

        // Setup Other Tabs
        sections.forEach { sec ->
            fragments.add(setupEmptyFragment(sec))
            tlTabs.addTab(setupNewTab(sec.title))
        }

        tlTabs.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if (tlTabs.measuredWidth < getScreenWidth()) {
            tlTabs.setPadding(0, 0, 0, 0)
        }

        vpContent.adapter = ExplorePagerAdapter(supportFragmentManager, fragments.toTypedArray())

        loginRepository.userCompany?.company?.primaryColor?.apply {
            tlTabs.setSelectedTabIndicatorColor(ColorUtils.parse(this))
        }

        vpContent.offscreenPageLimit = fragments.size + 1
        doFadeInAnimation()

    }

    @ExperimentalCoroutinesApi
    private fun setupEmptyFragment(section: Section?): SearchResultsFragment {
        return SearchResultsFragment.newInstance(
                section,
                loginRepository.userCompany?.company?.primaryColor.orEmpty()
        )
    }

    private fun setupNewTab(title: String?): TabLayout.Tab {
        return tlTabs.newTab().apply {
            val view = layoutInflater.inflate(R.layout.view_search_tab, tlTabs, false).apply {
                findViewById<TextView>(R.id.tvTitle)?.text =
                        title ?: getString(R.string.search_item_all)
            }
            view.measure(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            tlTabs.layoutParams.height = view.measuredHeight
            customView = view
        }
    }

    private fun setupListener(): SearchListener {
        return object : SearchListener {
            override fun onItemClicked(link: Link) {
                startActivity(ExploreItemObjectActivity.newInstance(this@SearchActivity, link))
            }

            override fun onShowAllClicked(sectionId: Int) {

                setResult(2119, Intent().apply {
                    putExtra("sectionId", sectionId)
                })

                finish()

                /*sectionsViewModel.sections().value.data?.sections?.forEachIndexed { idx, it ->
                    if (it.id == sectionId) {
                        tlTabs.getTabAt(idx + 1)?.select()
                        return
                    }
                }*/
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun searchQuery(query: String) {
        if (query.isEmpty()) {
            fragments[vpContent.currentItem].setupEmptyState()
        } else {
            var currentFragment = fragments[vpContent.currentItem]
            Log.i("search", "didSearch ${currentFragment.didSearch}")

            if (currentFragment.didSearch) {
                return
            }
            currentFragment.setupLoadingState(query)

            if (vpContent.currentItem == 0) {
                searchViewModel.fetchResutsBySection(query, currentTab = vpContent.currentItem)
            } else {
                val sections = sectionsViewModel.sections().value.data as SectionList
                searchViewModel.fetchResutsBySection(
                        query,
                        sections.sections[vpContent.currentItem - 1].id,
                        currentTab = vpContent.currentItem
                )
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupSectionsObserver() {
        lifecycleScope.launch {
            val value = sectionsViewModel.sections()
            value.collect { it ->
                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.let { setupSections(it.sections) }
                    }
                    Status.LOADING -> {
                        // TODO criar shimmer de carregamento ou loader
                        // progressBar.visibility = View.VISIBLErecyclerView.visibility = View.GONE
                    }
                    Status.ERROR -> {
                        // Handle Error progressBar.visibility = View.GONE
                        Toast.makeText(
                                this@SearchActivity,
                                it.message,
                                Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupResultsObserver() {
        lifecycleScope.launch {
            val value = searchViewModel.results()
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.sections?.let { sections ->
                            fragments[vpContent.currentItem].setupResults(
                                    sections,
                                    vpContent.currentItem == 0,
                                    setupListener()
                            )
                        } ?: run {
                            fragments[vpContent.currentItem].setupEmptyState()
                        }
                    }
                    Status.ERROR -> {
                        // Handle Error progressBar.visibility = View.GONE
                        Toast.makeText(
                                this@SearchActivity,
                                it.message,
                                Toast.LENGTH_LONG
                        ).show()
                    }
                    Status.EMPTY -> {
                        fragments[vpContent.currentItem].setupEmptyState()
                    }

                    Status.NOT_FOUND -> fragments[vpContent.currentItem].setupNotFoundState(etSearch.editableText.toString())
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupEmptySectionSearch() {
        lifecycleScope.launch {
            val emptySearch = searchViewModel.emptySection()
            emptySearch.collect {
                when (it.status) {
                    Status.EMPTY_WITH_DATA -> {
                        fragments[it.data!!].setupEmptyState()
                    }
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupResultsBySectionObserver() {

        searchViewModel.myresul.observe(this, Observer {

            Log.i("search", "myresul live data ${Gson().toJson(it)}")

            when (it.status) {
                Status.NOT_FOUND -> fragments[vpContent.currentItem].setupNotFoundState(etSearch.editableText.toString())
            }
        })

        lifecycleScope.launch {
            val sectionValue = searchViewModel.resultsBySection()
            sectionValue.collect {

                Log.i("search", "resultsBySection flow ${Gson().toJson(it)}")
                when (it.status) {
                    Status.SUCCESS -> {
                        if (it.data?.list?.items.isNullOrEmpty()) {
                            fragments[vpContent.currentItem].setupEmptyState()
                        } else {
                            val sections = sectionsViewModel.sections().value
                            sections.data?.sections?.get(vpContent.currentItem - 1).let { item ->
                                if (item != null) {
                                    fragments[vpContent.currentItem].setupResults(
                                            sections = listOf(
                                                    Section(
                                                            id = item.id,
                                                            title = item.title,
                                                            icon = item.icon,
                                                            template = SectionTemplate.getByValue(
                                                                    sectionValue.value.data?.list?.template
                                                            ),
                                                            emptyState = item.emptyState,
                                                            sectionLoadingState = item.sectionLoadingState,
                                                            items = sectionValue.value.data?.list?.items.orEmpty()
                                                    )
                                            ),
                                            hasHeaderAndFooter = false,
                                            listener = setupListener()
                                    )
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        // Handle Error progressBar.visibility = View.GONE
                        Toast.makeText(
                                this@SearchActivity,
                                it.message,
                                Toast.LENGTH_LONG
                        ).show()
                    }
                    Status.EMPTY -> {
                        fragments[vpContent.currentItem].setupEmptyState()
                    }

                    Status.NOT_FOUND -> fragments[vpContent.currentItem].setupNotFoundState(etSearch.editableText.toString())
                }
            }
        }
    }

    private fun doFadeInAnimation() {
        Handler().postDelayed({
            etSearch.requestFocus()
            showKeyboard(this@SearchActivity, etSearch)

        }, 300)

        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 600
        anim.repeatMode = Animation.REVERSE
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {

            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })

        vpContent.startAnimation(anim)
        tlTabs.startAnimation(anim)
    }

    private fun doFadeOutAnimation() {
        val anim = AlphaAnimation(1.0f, 0.0f)
        anim.duration = 50
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                tlTabs.visibility = View.GONE
                supportFinishAfterTransition()
            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })

        tlTabs.startAnimation(anim)
    }

}
