package co.socialsquad.squad.presentation.feature.live.creator

import android.content.DialogInterface
import co.socialsquad.squad.data.entity.QueueItem

interface LiveCreatorView {
    fun startStream(url: String)
    fun addQueueItem(queueItem: QueueItem)
    fun keepScreenOn(enabled: Boolean)
    fun close()
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun showLoading()
    fun hideLoading()
    fun showCloseConfirmationDialog()
    fun showStreamingState(time: Long)
    fun showConnectingState()
    fun showFinishedState()
    fun setParticipantCount(count: Int)
}
