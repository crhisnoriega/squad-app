package co.socialsquad.squad.presentation.feature.newlead.repository

import co.socialsquad.squad.domain.model.form.FormUpdate
import kotlinx.coroutines.flow.Flow

interface NewLeadRepository {
    fun formUpdate(submissionId: Long, widgetId: Long, formUpdate: FormUpdate): Flow<String>
}
