package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel

class ProductLocationLoadingViewHolder<T : ViewModel>(itemView: View) : ViewHolder<T>(itemView) {
    override fun bind(viewModel: T) {}
    override fun recycle() {}
}
