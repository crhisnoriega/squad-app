package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters

import androidx.collection.SparseArrayCompat

fun SparseArrayCompat<ViewTypeDelegateAdapter>.addAll(vararg delegates: Pair<Int, ViewTypeDelegateAdapter>) {
    delegates.forEach {
        this.put(it.first, it.second)
    }
}
