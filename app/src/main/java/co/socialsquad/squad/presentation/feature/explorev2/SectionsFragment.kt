package co.socialsquad.squad.presentation.feature.explorev2

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.ExploreStatePagerAdapter
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Explore
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemMapFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemMapPermissionFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreViewType
import co.socialsquad.squad.presentation.feature.searchv2.SearchActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.sections_fragment.*
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import javax.inject.Inject
import kotlin.math.abs


class SectionsFragment : Fragment() {

    private val viewModel: SectionsViewModel by viewModel()

    companion object {
        const val REQUEST_ENABLE_LOCATION = 1918
        var EXPANDED_TOOLBAR: Boolean = false
    }

    @Inject
    lateinit var loginRepository: LoginRepository
    private val indicatorColor by lazy {
        loginRepository.userCompany?.company?.primaryColor
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.sections_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        setupSectionsListeners()
        setupToolbar()
        setupBtnMap()

        viewModel.fetchSections()

        tabLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            EXPANDED_TOOLBAR = abs(verticalOffset) - tabLayout.totalScrollRange != 0
        })
    }

    private fun setupBtnMap() {
        rlMap.setOnClickListener {
            getSectionAtPosition(tlTabs.selectedTabPosition)?.let { section ->
                changeToMapView(tlTabs.selectedTabPosition, section)
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            btnMapa.compoundDrawableTintList = ColorUtils.stateListOf(indicatorColor!!)
        }

        btnMapa.typeface = Typeface.createFromAsset(requireContext().assets, "fonts/roboto_medium.ttf")

    }

    private fun getSectionAtPosition(position: Int): Section? {
        return viewModel.sections().value.data?.sections?.get(position)
    }

    private fun setupSectionsListeners() {
        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(
                                    ContextCompat.getColor(
                                            context,
                                            R.color.darkjunglegreen_28
                                    )
                            )
                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_bold.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        } else {
                            this.setTextColor(ContextCompat.getColor(context, R.color.oldlavender))

                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_medium.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        }
                    }
                }
            }
        })

        indicatorColor?.let {
            tlTabs.setSelectedTabIndicatorColor(ColorUtils.parse(it))
        }

        vpContent.registerOnPageChangeCallback(onPageChangeListener)
    }

    private val onPageChangeListener = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            tlTabs.getTabAt(position)?.select()
            rlMap.isVisible = false
            var fragment = (vpContent.adapter as ExploreStatePagerAdapter).getItem(position)
            if (fragment is ExploreItemFragment) {
                rlMap.isVisible = fragment.mapsVisible
            }
        }
    }

    private fun removeProp(name: String) {
        var shared = requireActivity().getSharedPreferences("map", Context.MODE_PRIVATE)
        shared.edit().remove(name).commit()
    }

    private fun addTabs() {
        tlTabs.removeAllTabs()
        viewModel.sections().value.data?.sections?.forEach {

            removeProp("zoom${it.id}")
            removeProp("lat${it.id}")
            removeProp("lng${it.id}")

            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_explore_tab, tlTabs, false).apply {
                    Glide.with(context).load(it.icon.url)
                            .crossFade()
                            .into(findViewById(R.id.ivIcon))
                    findViewById<TextView>(R.id.tvTitle)?.text = it.title
                }
                view.measure(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                )
                customView = view
            }
            tlTabs.addTab(tab)
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.statusBarColor = Color.WHITE
        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_ENABLE_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults.size == 2
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getSectionAtPosition(tlTabs.selectedTabPosition)?.let { section ->
                    changeToMapView(tlTabs.selectedTabPosition, section)
                }
            }
        }
    }

    private fun changeToMapView(position: Int, section: Section) {
        if (checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            changeView(position, section, ExploreViewType.MapPermission)
        } else {
            changeView(position, section, ExploreViewType.Map)
        }
    }


    private fun createItemFragment(section: Section) = ExploreItemFragment.newInstance(
            section = section,
            link = "/endpoints/explore/${section.id}",
            template = section.template,
            loadingState = section.sectionLoadingState,
            emptyState = section.emptyState,
            sectionName = section.title
    ).apply {
        callback = callbackItensFragment
    }

    private fun createItemMapFragment(section: Section) = ExploreItemMapFragment
            .newInstance(
                    section = section,
                    link = "/endpoints/explore/${section.id}",
                    template = section.template,
                    loadingState = section.sectionLoadingState,
                    emptyState = section.emptyState,
                    sectionIcon = section.icon
            ).apply {
                callbackActions = callbackActionsLista
            }

    private fun createItemMapPermissionFragment() =
            ExploreItemMapPermissionFragment
                    .newInstance {
                        requestPermissions(
                                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest
                                        .permission.ACCESS_FINE_LOCATION),
                                REQUEST_ENABLE_LOCATION
                        )
                    }

    private fun changeView(position: Int, section: Section, exploreViewType: ExploreViewType) {

        //addTabs()

        vpContent.isUserInputEnabled = false;
        when (exploreViewType) {
            ExploreViewType.List -> {
            }
            ExploreViewType.Map -> rlMap.isVisible = false
            ExploreViewType.MapPermission -> rlMap.isVisible = false
        }
        //vpContent.offscreenPageLimit = fragments.size

        val newFragment = when (exploreViewType) {
            ExploreViewType.List -> createItemFragment(section)
            ExploreViewType.Map -> createItemMapFragment(section)
            ExploreViewType.MapPermission -> createItemMapPermissionFragment()
        }


        var fragments = (vpContent.adapter as ExploreStatePagerAdapter).fragments

        Log.i("restore", "fragments before: $fragments")

        fragments.removeAt(position)
        fragments.add(position, newFragment)

        Log.i("restore", "fragments after: $fragments")

        vpContent.adapter?.notifyItemChanged(position)


        //  vpContent.currentItem = position


        /*var fragments = mutableListOf<Fragment>()

        (vpContent.adapter as ExploreStatePagerAdapter).fragments.forEachIndexed { index, fragment ->
            if (index == position) {
                when (exploreViewType) {
                    ExploreViewType.List -> fragments.add(createItemFragment(section))
                    ExploreViewType.Map -> fragments.add(createItemMapFragment(section))
                    ExploreViewType.MapPermission -> fragments.add(createItemMapPermissionFragment())
                }
            } else {
                if (fragment is ExploreItemFragment) {
                    getSectionAtPosition(index)?.let { section ->
                        fragments.add(createItemFragment(section))
                    }
                }
                if (fragment is ExploreItemMapFragment) {
                    getSectionAtPosition(index)?.let { section ->
                        fragments.add(createItemMapFragment(section))
                    }
                }
                if (fragment is ExploreItemMapPermissionFragment) {
                    fragments.add(createItemMapPermissionFragment())
                }

            }
        }*/

        //vpContent.adapter = ExploreStatePagerAdapter(childFragmentManager, fragments.toMutableList(), lifecycle)


    }

    private var callbackActionsLista = object : ExploreItemMapFragment.OnActionsListener {
        override fun onListClick() {
            getSectionAtPosition(tlTabs.selectedTabPosition)?.let { section ->
                changeView(tlTabs.selectedTabPosition, section, ExploreViewType.List)
            }
        }
    }

    private var callbackItensFragment = object : ExploreItemFragment.OnItemLoadedListener {
        override fun onItemLoaded(explore: Explore) {
            onPageChangeListener.onPageSelected(tlTabs.selectedTabPosition)
        }
    }

    private fun setupToolbar() {
        etSearch.apply {
            isFocusable = false
            isFocusableInTouchMode = false
            isClickable = false
            isLongClickable = false
            isEnabled = false
        }
        tToolbar.setOnClickListener { openSearch() }
    }

    private fun openSearch() {
        var searchBar = view?.findViewById<View>(R.id.search_bar_container)

        val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                requireActivity(),
                androidx.core.util.Pair(searchBar as View, searchBar?.transitionName)
        ).toBundle()


        val intent = Intent(context, SearchActivity::class.java)
        startActivityForResult(intent, 2117, bundle)
        // activity?.overridePendingTransition(0, 0);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            2117 -> {
                try {
                    if (data?.hasExtra("sectionId")!!) {
                        viewModel.sections().value.data?.sections?.forEachIndexed { index, element ->
                            Log.i("showall", index.toString())
                            Log.i("showall", element.toString())
                            if (element.id == data.getIntExtra("sectionId", -1)) {
                                tlTabs.getTabAt(index)?.select()
                            }
                        }
                    }
                } catch (e: Exception) {
                    FirebaseCrashlytics.getInstance().recordException(e)
                }
            }
        }
    }

    private fun setupObserver() {
        lifecycleScope.launch {
            val value = viewModel.sections()
            value.collect {

                Log.i("restore", "status: ${it.status}")
                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.let { sectionList ->
                            addTabs()

                            val fragments = mutableListOf<Fragment>()

                            sectionList.sections?.forEach { section ->
                                fragments.add(createItemFragment(section))
                            }

                            vpContent.adapter = ExploreStatePagerAdapter(childFragmentManager, fragments.toMutableList(), lifecycle)
                            vpContent.isUserInputEnabled = false
                            vpContent.offscreenPageLimit = fragments.size + 1
                        }
                    }
                    Status.LOADING -> {
                        // TODO criar shimmer de carregamento ou loader
//                        progressBar.visibility = View.VISIBLE
//                        recyclerView.visibility = View.GONE
                    }
                    Status.ERROR -> {
                        // Handle Error
//                        progressBar.visibility = View.GONE
                        Toast.makeText(
                                context,
                                it.message,
                                Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }
}
