package co.socialsquad.squad.presentation.feature.call

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.service.CALL_SERVICE_ACTION_STOP_CALL
import co.socialsquad.squad.data.service.SignallingService
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_call_room.*
import org.webrtc.RendererCommon
import javax.inject.Inject

const val CALL_ROOM_EXTRA_QUEUE_ID = "CALL_ROOM_EXTRA_QUEUE_ID"
const val CALL_ROOM_EXTRA_CALL_ID = "CALL_ROOM_EXTRA_CALL_ID"
const val CALL_ROOM_EXTRA_ACTION = "CALL_ROOM_EXTRA_ACTION"
const val CALL_ROOM_EXTRA_USER = "CALL_ROOM_EXTRA_USER"
const val CALL_ROOM_EXTRA_TYPE = "CALL_ROOM_EXTRA_TYPE"
const val CALL_TYPE_VIDEO = "CALL_TYPE_VIDEO"
const val CALL_TYPE_AUDIO = "CALL_TYPE_AUDIO"
const val CALL_ROOM_ACTION_RECEIVE = "RECEIVING_CALL"
const val CALL_ROOM_ACTION_CALL = "CALLING"

class CallRoomActivity : AppCompatActivity(), CallRoomView {

    companion object {
        const val CAMERA_AUDIO_PERMISSION_REQUEST = 1
    }

    @Inject
    lateinit var presenter: CallRoomPresenter
    private var audioManager: AudioManager? = null
    private var savedMicrophoneState: Boolean? = null
    private var savedAudioMode: Int? = null

    private var action: String? = null

    private var callType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            setTurnScreenOn(true)
            setShowWhenLocked(true)
        } else {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            )
        }
        super.onCreate(savedInstanceState)

        callType = intent.getStringExtra(CALL_ROOM_EXTRA_TYPE)

        setContentView(R.layout.activity_call_room)

        audioManager = this.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        savedAudioMode = audioManager?.mode
        audioManager?.mode = AudioManager.MODE_IN_COMMUNICATION

        savedMicrophoneState = audioManager?.isMicrophoneMute
        audioManager?.isMicrophoneMute = false
        action = intent.getStringExtra(CALL_ROOM_EXTRA_ACTION)
        action?.let { action ->
            (intent.getSerializableExtra(CALL_ROOM_EXTRA_USER) as? Profile)?.let {
                presenter.onCreate(
                    this, CallingManager.ViewRenders(svrLocalRenderer, svrRemoteRenderer),
                    intent.getStringExtra(CALL_ROOM_EXTRA_QUEUE_ID), it.pk,
                    action, callType, intent.getStringExtra(CALL_ROOM_EXTRA_CALL_ID)
                )
                setCaller(it)
            }
        }
        handlePermissions()
        bAnswer.setOnClickListener {
            presenter.answerCall()
            it.isEnabled = false
        }
        bCancel.setOnClickListener {
            val intent = Intent(this, SignallingService::class.java).apply {
                action = CALL_SERVICE_ACTION_STOP_CALL
            }
            startService(intent)
            presenter.stopCall()
        }
        bSpeaker.setOnClickListener {
            presenter.toggleSpeaker()
        }
        bVideo.setOnClickListener {
            presenter.toggleVideo()
        }
        bVideoBlock.setOnClickListener {
            presenter.toggleVideo()
        }
        bMute.setOnClickListener {
            presenter.mute()
        }
        bMuteVideo.setOnClickListener {
            presenter.mute()
        }
        bChangeCamera.setOnClickListener {
            presenter.changeCamera()
        }
    }

    override fun setCompanyImage(url: String) {
        Glide.with(this)
            .load(url)
            .crossFade()
            .into(ivCompanyLogo)
    }

    override fun setCaller(profile: Profile) {
        Glide.with(this)
            .load(profile.avatar)
            .circleCrop()
            .crossFade()
            .into(ivUserAvatar)
        Glide.with(this)
            .load(profile.qualificationBadge)
            .circleCrop()
            .crossFade()
            .into(ivUserQualification)
        tvUserName.text = getString(R.string.user_name, profile.firstName, profile.lastName)
        tvQualification.text = profile.qualification
        Glide.with(this)
            .load(profile.avatar)
            .circleCrop()
            .crossFade()
            .into(ivUserAvatarSmall)
        Glide.with(this)
            .load(profile.qualificationBadge)
            .circleCrop()
            .crossFade()
            .into(ivUserQualificationSmall)
        tvUserNameSmall.text = getString(R.string.user_name, profile.firstName, profile.lastName)
        tvQualificationSmall.text = profile.qualification
    }

    private fun handlePermissions() {
        val canAccessCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        val canRecordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
        if (!canAccessCamera || !canRecordAudio) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO), CAMERA_AUDIO_PERMISSION_REQUEST)
        } else {
            startVideoSession()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_AUDIO_PERMISSION_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                    startVideoSession()
                } else {
                    finish()
                }
                return
            }
        }
    }

    private fun startVideoSession() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.black)
        presenter.onStartVideoSession()
    }

    override fun setupLocalRenderer(callManager: CallingManager) {
        svrLocalRenderer.init(callManager.renderContext, null)
        svrLocalRenderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT)
        svrLocalRenderer.setZOrderMediaOverlay(true)
        svrLocalRenderer.setEnableHardwareScaler(true)
    }

    override fun setupRemoteRenderer(callManager: CallingManager) {
        svrRemoteRenderer.init(callManager.renderContext, null)
        svrRemoteRenderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
        svrRemoteRenderer.setEnableHardwareScaler(true)
    }

    override fun onDestroy() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        super.onDestroy()
        presenter.onDestroy()
        svrLocalRenderer?.release()
        svrRemoteRenderer?.release()

        if (savedAudioMode !== null) {
            audioManager?.mode = savedAudioMode!!
        }
        if (savedMicrophoneState != null) {
            audioManager?.isMicrophoneMute = savedMicrophoneState!!
        }
    }

    override fun endCall() {
        runOnUiThread {
            finish()
        }
    }

    override fun failedCall() {
        Thread(
            Runnable {
                runOnUiThread {
                    showStatusMessage("FAILED")
                }
                Thread.sleep(2000)
                runOnUiThread {
                    endCall()
                }
            }
        )
    }

    override fun showStatusMessage(message: String) {
        runOnUiThread {
            tvStatus.text = message
            tvStatus.visibility = View.VISIBLE
            chStatus.visibility = View.GONE
        }
    }

    override fun showStatusMessage(messageId: Int) {
        showStatusMessage(getString(messageId))
    }

    override fun callReady() {
        runOnUiThread {
            llcall.visibility = View.VISIBLE
            if (action == CALL_ROOM_ACTION_RECEIVE) {
                bAnswer.isEnabled = true
            }
            if (callType == CALL_TYPE_VIDEO) {
                changeLayoutToVideo()
            } else {
                changeLayoutToAudio()
            }
        }
    }

    override fun callAnswered() {
        runOnUiThread {
            bAnswer.visibility = View.GONE
            bCancel.setImageResource(R.drawable.ic_call_hang_down)
            if (callType == CALL_TYPE_VIDEO) {
                clacVideo.visibility = View.VISIBLE
            } else {
                llAc.visibility = View.VISIBLE
            }
        }
    }

    override fun hideVideoLocal() {
        runOnUiThread {
            svrLocalRenderer.visibility = View.INVISIBLE
        }
    }

    override fun showVideoLocal() {
        runOnUiThread {
            svrLocalRenderer.visibility = View.VISIBLE
            bVideoBlock.setImageResource(R.drawable.ic_call_block_cam)
        }
    }

    override fun changeVideoIconToAllow() {
        runOnUiThread {
            bVideoBlock.setImageResource(R.drawable.ic_call_video)
        }
    }

    override fun changeVideoIconToBlock() {
        runOnUiThread {
            bVideoBlock.setImageResource(R.drawable.ic_call_block_cam)
        }
    }

    override fun setScreenBackground(colors: List<String>) {
        val toColors = colors.map { Color.parseColor(it) }
        val gradientDrawable = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, toColors.toIntArray())
        clCallScreen.background = gradientDrawable
    }

    override fun callConnected() {
        runOnUiThread {
            tvStatus.visibility = View.INVISIBLE
            chStatus.visibility = View.VISIBLE
            chStatus.base = SystemClock.elapsedRealtime()
            chStatus.start()
            if (callType == CALL_TYPE_VIDEO) {
                clUserProfileSmall.visibility = View.INVISIBLE
            }
        }
    }

    override fun setSpeakerButtonStatus(speakerOn: Boolean) {
        runOnUiThread {
            if (speakerOn) {
                bSpeaker.setImageResource(R.drawable.ic_call_speaker_on)
            } else {
                bSpeaker.setImageResource(R.drawable.ic_call_speaker_off)
            }
        }
    }

    override fun setMicrophoneButtonStatus(micOn: Boolean) {
        runOnUiThread {
            if (micOn) {
                bMute.setImageResource(R.drawable.ic_call_mic_activated)
                bMuteVideo.setImageResource(R.drawable.ic_call_mic_activated)
            } else {
                bMute.setImageResource(R.drawable.ic_call_mic_muted)
                bMuteVideo.setImageResource(R.drawable.ic_call_mic_muted)
            }
        }
    }

    override fun changeLayoutToVideo() {
        runOnUiThread {
            if (svrRemoteRenderer.visibility != View.VISIBLE) {
                ConstraintSet().apply {
                    clone(clCallScreen)
                    clear(R.id.llcall, ConstraintSet.TOP)
                    clear(R.id.llcall, ConstraintSet.BOTTOM)
                    val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, resources.displayMetrics).toInt()
                    connect(R.id.llcall, ConstraintSet.BOTTOM, R.id.clacVideo, ConstraintSet.TOP, margin)
                    applyTo(clCallScreen)
                }
                llAc.visibility = View.GONE
                clacVideo.visibility = View.VISIBLE
                svrRemoteRenderer.visibility = View.VISIBLE
            }
        }
    }

    override fun changeLayoutToAudio() {
        runOnUiThread {
            if (svrRemoteRenderer.visibility != View.INVISIBLE) {
                ConstraintSet().apply {
                    clone(clCallScreen)
                    clear(R.id.llcall, ConstraintSet.TOP)
                    clear(R.id.llcall, ConstraintSet.BOTTOM)
                    val margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32F, resources.displayMetrics).toInt()
                    connect(R.id.llcall, ConstraintSet.TOP, R.id.llAc, ConstraintSet.BOTTOM, margin)
                    applyTo(clCallScreen)
                }
                llAc.visibility = View.VISIBLE
                clacVideo.visibility = View.GONE
                svrRemoteRenderer.visibility = View.INVISIBLE
                svrLocalRenderer.visibility = View.INVISIBLE
            }
        }
    }

    override fun showSmallUserInfo() {
        runOnUiThread {
            clUserProfileSmall.visibility = View.VISIBLE
            clUserProfile.visibility = View.INVISIBLE
        }
    }

    override fun showNormalUserInfo() {
        runOnUiThread {
            clUserProfileSmall.visibility = View.INVISIBLE
            clUserProfile.visibility = View.VISIBLE
        }
    }

    override fun hideUserInfos() {
        runOnUiThread {
            clUserProfileSmall.visibility = View.INVISIBLE
            clUserProfile.visibility = View.INVISIBLE
        }
    }
}
