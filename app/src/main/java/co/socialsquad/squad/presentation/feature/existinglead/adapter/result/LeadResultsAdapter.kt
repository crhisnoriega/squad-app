package co.socialsquad.squad.presentation.feature.existinglead.adapter.result

import android.os.Handler
import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadSeparator
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.lead.ExistingLeadDelegateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.separator.SeparatorDelegateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM

class LeadResultsAdapter(colorPrimary: String?, checkedListener: CheckedListener) :
        RecyclerView.Adapter<ResultsViewHolder>() {

    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    var leads: MutableList<ExistingLeadVM>? = mutableListOf()
    private var selectedLead: ExistingLeadVM? = null

    private val rbCheckedListener = object : CheckedListener {
        override fun onChecked(checked: Boolean, existingLeadVM: ExistingLeadVM) {
            leads?.let {
                it.map { lead ->
                    lead.checked = lead == existingLeadVM
                    if (lead.checked) {
                        selectedLead = existingLeadVM
                    }
                }

                Handler().postDelayed(
                        {
                            notifyDataSetChanged()
                        },
                        1
                )
            }
            checkedListener.onChecked(checked, existingLeadVM)
        }
    }

    interface CheckedListener {
        fun onChecked(checked: Boolean, existingLeadVM: ExistingLeadVM)
    }

    init {
        delegateAdapters.addAll(
                LeadResultType.Separator.value to SeparatorDelegateAdapter(),
                LeadResultType.Item.value to ExistingLeadDelegateAdapter(
                        colorPrimary,
                        rbCheckedListener
                )
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {
        return delegateAdapters.get(viewType)?.onCreateViewHolder(parent)
                ?: run { throw IllegalStateException("ViewType não conhecido, valor em questão: $viewType") }
    }

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        leads?.get(position)?.let {
            delegateAdapters.get(getItemViewType(position))
                    ?.onBindViewHolder(holder, it)
                    ?: run { throw IllegalStateException("ViewType não reconhecido") }
        } ?: run { throw IllegalStateException("ViewType não reconhecido") }
    }

    override fun getItemViewType(position: Int): Int {
        return leads?.get(position)?.getViewType() ?: run { LeadResultType.Invalid.value }
    }

    override fun getItemCount(): Int {
        return leads?.size ?: run { 0 }
    }


    fun addGrouping(newLeadsPage: List<ExistingLeadSeparator>) {

        var results: MutableList<ExistingLeadVM> = mutableListOf<ExistingLeadVM>()

        newLeadsPage.forEach { group ->
            results.add(ExistingLeadVM(newSeparator(group), null, false))
            group.items?.forEach {
                results.add(ExistingLeadVM(null, it, false))
            }
        }

//        leads?.forEachIndexed { index, vm ->
//            when (vm.getViewType()) {
//                LeadResultType.Separator.value -> {
//                        if()
//                }
//            }
//        }

        leads?.addAll(results)
        notifyDataSetChanged()
    }


    private fun newSeparator(it: ExistingLeadSeparator): ExistingLeadSeparator {
        return ExistingLeadSeparator(
                it.title,
                it.subtitle,
                null
        )
    }

    fun getSelectedLead() = selectedLead
}
