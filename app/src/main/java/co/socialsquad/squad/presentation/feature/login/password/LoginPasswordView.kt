package co.socialsquad.squad.presentation.feature.login.password

import co.socialsquad.squad.data.entity.Company

interface LoginPasswordView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(resId: Int)
    fun enableNextButton(enabled: Boolean)
    fun openNavigation()
    fun openLoginAlternative(subdomain: String, email: String, company: Company?)
    fun setupHelpText(subdomain: String)
    fun openProfileScreen()
    fun setupHeader(company: Company?)
}
