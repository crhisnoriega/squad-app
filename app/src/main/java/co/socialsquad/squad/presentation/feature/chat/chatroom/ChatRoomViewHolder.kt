package co.socialsquad.squad.presentation.feature.chat.chatroom

import android.view.View
import androidx.lifecycle.Observer
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_chat_room_item.view.*

class ChatRoomViewHolder(
    itemView: View,
    private var onClickListener: ViewHolder.Listener<ChatRoomViewModel>
) : ViewHolder<ChatRoomViewModel>(itemView) {

    private val glide = Glide.with(itemView)

    override fun bind(viewModel: ChatRoomViewModel) {
        with(itemView) {
            short_title_lead.text = viewModel.name

            tvRole.text = viewModel.qualification
            if (!viewModel.lastMessageMutable.value.isNullOrEmpty()) {
                tvLastMessage.text = viewModel.lastMessageMutable.value
                viewModel.elapsedTime.value?.let { tvElapsedTime.text = " • ".plus(it.elapsedTime(context, true)) }
                clLastMessageAndElapsedTime.visibility = View.VISIBLE
            } else {
                clLastMessageAndElapsedTime.visibility = View.GONE
            }

            if (viewModel.unreadCount > 0) {
                tvUnread.text = viewModel.unreadCount.toString()
                tvUnread.visibility = View.VISIBLE
            } else {
                tvUnread.visibility = View.GONE
            }

            vOnlineBadge.visibility = if (viewModel.isOnline) View.VISIBLE else View.GONE

            glide.load(viewModel.avatarUrl)
                .circleCrop()
                .crossFade()
                .into(itemView.ivAvatar)

            clRoomContainer.setOnClickListener { onClickListener.onClick(viewModel) }

            viewModel.companyColor?.let {
                if (it.isNotEmpty()) tvUnread.backgroundTintList = ColorUtils.stateListOf(it)
            }
            viewModel.lastMessageMutable.observe(
                this@ChatRoomViewHolder,
                Observer { lastMessage ->
                    if (!lastMessage.isNullOrEmpty()) {
                        tvLastMessage.text = lastMessage
                        viewModel.elapsedTime.value?.let { tvElapsedTime.text = " • ".plus(it.elapsedTime(context, true)) }
                        clLastMessageAndElapsedTime.visibility = View.VISIBLE
                    } else {
                        clLastMessageAndElapsedTime.visibility = View.GONE
                    }
                }
            )
        }
    }

    override fun recycle() {}
}
