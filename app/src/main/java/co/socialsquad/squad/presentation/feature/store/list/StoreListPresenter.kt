package co.socialsquad.squad.presentation.feature.store.list

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.pm.PackageManager
import android.location.Location
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.StoreRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_STORE_LIST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_STORE_CALL
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private const val REQUEST_CODE_PERMISSIONS_LOCATION = 77

class StoreListPresenter @Inject constructor(
    private val view: StoreListView,
    private val tagWorker: TagWorker,
    private val storeRepository: StoreRepository,
    loginRepository: LoginRepository,
    private val fusedLocationProviderClient: FusedLocationProviderClient
) {

    private val compositeDisposable = CompositeDisposable()
    private val companyColor = loginRepository.userCompany?.company?.primaryColor
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

    private var page = 1
    private var isLoading = false
    private var isComplete = false
    private var isWaitingToSubscribe = false
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    fun onViewCreated() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_STORE_LIST)
        view.setupList(companyColor)
        verifyAndSubscribe()
    }

    fun onRefresh() {
        compositeDisposable.clear()
        isComplete = false
        page = 1
        verifyAndSubscribe()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) {
            isLoading = true
            view.startLoading()
            subscribe()
        }
    }

    fun onVisibleToUser() {
        view.verifyLocationServiceAvailability()
    }

    private fun verifyAndSubscribe() {
        isLoading = true
        view.startLoading()
        view.verifyLocationServiceAvailability()
        isWaitingToSubscribe = true
    }

    private fun subscribe() {
        isWaitingToSubscribe = false
        compositeDisposable.add(
            storeRepository.getStores(latitude, longitude, page)
                .doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }
                .doOnNext { if (it.next == null) isComplete = true }
                .map { it.results.orEmpty().map { StoreViewModel(it, latitude, longitude) } }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            val shouldClearList = page == 1
                            view.addItems(it, shouldClearList)
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }

    fun onCallClicked() {
        tagWorker.tagEvent(TAG_TAP_STORE_CALL)
    }

    fun onRequestPermissionsClicked() {
        isLoading = true
        view.startLoading()
        view.showList()
        view.requestPermissions(REQUEST_CODE_PERMISSIONS_LOCATION, permissions)
    }

    fun onLocationDisabledClicked() {
        isLoading = true
        view.startLoading()
        view.showList()
        view.verifyLocationServiceAvailability()
    }

    fun onLocationServiceAvailability(enabled: Boolean) {
        if (enabled) {
            if (view.checkPermissions(permissions)) getLastLocation()
            else showPermissionsDenied()
        } else showLocationDisabled()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { onLastLocationReceived(it) }
    }

    private fun onLastLocationReceived(location: Location?) {
        location?.let {
            latitude = it.latitude
            longitude = it.longitude
            if (isWaitingToSubscribe) subscribe()
        } ?: getFineLocation()
    }

    private fun getFineLocation() {
        view.createLocationRequest()
    }

    fun onRequestPermissionsResult(grantResults: IntArray) {
        if (grantResults.isEmpty() || grantResults.any { it == PackageManager.PERMISSION_DENIED }) {
            showPermissionsDenied()
        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun onLocationRequestCreated(locationRequest: LocationRequest, pendingIntent: PendingIntent?) {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, pendingIntent).addOnSuccessListener {
            getLastLocation()
        }.addOnFailureListener {
            showPermissionsDenied()
        }
    }

    private fun showPermissionsDenied() {
        compositeDisposable.clear()
        view.showPermissionsDenied()
        view.stopLoading()
        isLoading = false
    }

    private fun showLocationDisabled() {
        compositeDisposable.clear()
        view.showLocationDisabled()
        view.stopLoading()
        isLoading = false
    }
}
