package co.socialsquad.squad.presentation.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.exifinterface.media.ExifInterface
import com.bumptech.glide.load.resource.bitmap.TransformationUtils

object BitmapUtils {

    fun normalizeExifRotation(context: Context, uri: Uri, bitmap: Bitmap = BitmapFactory.decodeFile(uri.path)): Bitmap {
        val inputStream = context.contentResolver.openInputStream(uri)
        val exif: ExifInterface? = ExifInterface(inputStream!!)
        // val exif = new ExifInterface(uri.getPath());

        val exifRotation = exif?.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            ?: ExifInterface.ORIENTATION_UNDEFINED

        val rotationInDegrees = when (exifRotation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }

        return TransformationUtils.rotateImage(bitmap, rotationInDegrees)
    }
}
