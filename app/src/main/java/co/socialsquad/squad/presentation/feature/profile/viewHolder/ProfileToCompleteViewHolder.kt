package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.animation.ValueAnimator
import android.view.View
import androidx.core.animation.addListener
import androidx.fragment.app.FragmentManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.ProfilePictureActivity
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_card_complete_profile.view.*


const val PROFILE_TO_COMPLETE_VIEW_HOLDER_ID = R.layout.item_card_complete_profile

class ProfileToCompleteViewHolder(
    val fragmentManager: FragmentManager,
    itemView: View,
    val animate: Boolean? = false,
    private val completeCallback: () -> Unit,
) :
    ViewHolder<ProfileToCompleteViewModel>(itemView) {

    private fun animate() {
        increaseViewSize(itemView)
    }

    private fun increaseViewSize(view: View) {
        val valueAnimator = ValueAnimator.ofInt(view.measuredHeight, 0)
        valueAnimator.duration = 800L
        valueAnimator.addUpdateListener {
            val animatedValue = valueAnimator.animatedValue as Int
            val layoutParams = view.layoutParams
            layoutParams.height = animatedValue
            view.layoutParams = layoutParams

        }
        valueAnimator.addListener(onEnd = {
            completeCallback.invoke()
        })

        valueAnimator.start()
    }

    override fun recycle() {
    }

    override fun bind(viewModel: ProfileToCompleteViewModel) {

        // context
        itemView.contextLayout.isVisible = viewModel.completeYourProfile?.context?.visible!!
        itemView.context_complete_check.isVisible =
            viewModel.completeYourProfile?.context?.completed!!
        itemView.context_incomplete_arrow.isVisible =
            viewModel.completeYourProfile?.context?.completed?.not()!!
        itemView.contextLayout.isClickable =
            viewModel.completeYourProfile?.context?.completed!!

        if (viewModel.completeYourProfile?.context?.completed!!.not()) {
            itemView.contextLayout.setOnClickListener {
                CompleteProfileActivity.showForm(
                    itemView.context,
                    ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_CONTEXT_ITEM
                )
            }
        }


        // profile
        itemView.profileLayout.isVisible = viewModel.completeYourProfile?.profile?.visible!!
        itemView.profile_incomplete_arrow.isVisible =
            viewModel.completeYourProfile?.context?.completed!! &&
                    viewModel.completeYourProfile?.profile?.completed!!.not()

        itemView.profile_complete_check.isVisible =
            viewModel.completeYourProfile?.profile?.completed!!
        if (viewModel.completeYourProfile?.profile?.completed?.not()!!) {
            itemView.profile_locked.isVisible = itemView.profile_incomplete_arrow.isVisible.not()
        }

        itemView.profileLayout.isClickable =
            itemView.profile_incomplete_arrow.isVisible

        // if (itemView.profile_incomplete_arrow.isVisible.not())
        itemView.profileLayout.setOnClickListener {
            CompleteProfileActivity.showForm(
                itemView.context,
                ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PROFILE_ITEM
            )
        }


        // payment
        itemView.paymentLayout.isVisible = viewModel.completeYourProfile?.payment?.visible!!
        itemView.payment_incomplete_arrow.isVisible =
            viewModel.completeYourProfile?.context?.completed!! &&
                    viewModel.completeYourProfile?.profile?.completed!! &&
                    viewModel.completeYourProfile?.payment?.completed!!.not()
        itemView.payment_complete_check.isVisible =
            viewModel.completeYourProfile?.payment?.completed!!

        if (viewModel.completeYourProfile?.payment?.completed?.not()!!) {
            itemView.payment_locked.isVisible = itemView.payment_incomplete_arrow.isVisible.not()
        }

        itemView.paymentLayout.isClickable = itemView.payment_incomplete_arrow.isVisible
        // if (itemView.payment_incomplete_arrow.isVisible)
            itemView.paymentLayout.setOnClickListener {
                CompleteProfileActivity.showForm(
                    itemView.context,
                    ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_ITEM
                )
            }


        if (viewModel.animate!!) {
            animate()
        }
    }
}
