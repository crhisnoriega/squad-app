package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_input_image.view.*

class InputImage(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    val imageView
        get() = ivImage as ImageView?

    val drawable
        get() = imageView?.drawable

    init {
        View.inflate(context, R.layout.view_input_image, this)
        setupStyledAttributes(attrs)
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputImage)) {
            bEdit.text = getString(R.styleable.InputImage_name)
        }
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        (ivImage as ImageView).setOnInputListener(onInputListener)
    }

    override fun setOnClickListener(l: View.OnClickListener?) {
        bEdit.setOnClickListener(l)
    }

    override fun isValid(valid: Boolean) {}

    class ImageView : AppCompatImageView, InputView {
        private var onInputListener: InputView.OnInputListener? = null

        constructor(context: Context) : super(context)

        constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

        constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

        override fun setImageDrawable(drawable: Drawable?) {
            super.setImageDrawable(drawable)
            if ((context as? AppCompatActivity)?.isFinishing == false) onInputListener?.onInput()
        }

        override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
            this.onInputListener = onInputListener
        }

        override fun isValid(valid: Boolean) {}
    }
}
