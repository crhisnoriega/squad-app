package co.socialsquad.squad.presentation.feature.social.create.segment.members

import dagger.Binds
import dagger.Module

@Module
abstract class SegmentGroupMembersModule {
    @Binds
    internal abstract fun view(
        segmentGroupMembersActivity: SegmentGroupMembersActivity
    ): SegmentGroupMembersView
}
