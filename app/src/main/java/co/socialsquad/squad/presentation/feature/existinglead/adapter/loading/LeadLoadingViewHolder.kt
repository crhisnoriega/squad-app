package co.socialsquad.squad.presentation.feature.existinglead.adapter.loading

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate

class LeadLoadingViewHolder(val parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_lead_loading_state)) {

    fun bind() {
    }
}
