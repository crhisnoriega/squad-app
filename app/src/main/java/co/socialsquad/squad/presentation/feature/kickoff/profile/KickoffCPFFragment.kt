package co.socialsquad.squad.presentation.feature.kickoff.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import kotlinx.android.synthetic.main.fragment_kickoff_cpf.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class KickoffCPFFragment(
    val title: String,
    val subtitle: String,
    val icon: String,
    val field: Field,
    val value: String
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupContent()
        setupNextButton()
        setupSkipButton()
        setupBack()
        setupHeader()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_kickoff_cpf, container, false)

    private fun setupHeader() {
        // txtTitle.text = title
        // txtSubtitle.text = subtitle

    }

    private fun setupBack() {
        ibBack.setOnClickListener {
            viewModel.state.postValue(StateNav("picture", true))
        }
    }

    private fun setupContent() {
        edtField.setRequiredField(field.isMandatory)
        edtField.requestComponentFocus()

        edtField.setEditOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btnConfirmar.performClick()
            }
            false
        })

        edtField.addEditTextChangedListener(
            listener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(fieldValue: Editable?) {
                    if (field.isMinimalInput(fieldValue.toString())) {
                        checkValidField(fieldValue.toString())
                    } else {
                        txtValidationError.text = ""
                        txtHelpText.isVisible = true
                        txtValidationError.isVisible = false
                        btnConfirmar.isEnabled = false
                    }
                }
            }
        )
        field.inputMask()?.let {
            edtField.setEditInputMask(it)
        }
        edtField.setEditInputType(field.inputType())
        // -> edtField.setInnerPadding(16, 5, 16, 5)
        edtField.hint = field.label

        edtField.text = value

//        field.additionalInformation?.let {
//
//            txtSupplementary.text = it.supplementaryText
//
//            it.featuredText?.apply {
//                txtFeaturedText.text = this
//            } ?: run {
//                txtFeaturedText.isVisible = false
//            }
//        } ?: run {
//            llTxtFooter.isVisible = false
//        }


    }

    private fun checkValidField(fieldValue: String) {

        val needValidationMin: Boolean = field.validationMin != null
        val needValidationMax: Boolean = field.validationMax != null

        val validMin = !needValidationMin ||
                (needValidationMin && fieldValue.length >= field.validationMin!!)
        val validMax = !needValidationMax ||
                (needValidationMax && fieldValue.length <= field.validationMax!!)

        var validContentRequired = field.validInputType(fieldValue)

        var validated = (validMin && validMax && validContentRequired)

        field.validationError?.let {
            if (!validated) {
                txtValidationError.text = it
                txtHelpText.isVisible = false
                txtValidationError.isVisible = true
            } else {
                txtValidationError.text = ""
                txtHelpText.isVisible = true
                txtValidationError.isVisible = false
            }
        } ?: run {
            txtHelpText.isVisible = true
            txtValidationError.isVisible = false
        }

        btnConfirmar.isEnabled = validated
    }

    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    private fun setupNextButton() {
        btnConfirmar.text = "Completar Cadastro"

        btnConfirmar.setOnClickListener {

            viewModel.saveCPF(edtField.text)

            btnConfirmar.isEnabled = false

            showLoading()

            Handler().postDelayed({
                val intent = Intent(requireActivity(), NavigationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                activity?.finish()
            }, 2000)
        }

    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
