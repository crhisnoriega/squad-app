package co.socialsquad.squad.presentation.feature.profile

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.navigation.ReselectedListener
import co.socialsquad.squad.presentation.feature.profile.edit.ProfileEditActivity
import co.socialsquad.squad.presentation.feature.profile.edit.dialog.CompletedProfileDialog
import co.socialsquad.squad.presentation.feature.profile.pages.ProfileEmptyPublishFragment
import co.socialsquad.squad.presentation.feature.profile.pages.ProfilePublishGridFragment
import co.socialsquad.squad.presentation.feature.profile.qualification.ProfileQualificationActivity
import co.socialsquad.squad.presentation.feature.profile.viewHolder.*
import co.socialsquad.squad.presentation.feature.settings.SettingsActivity
import co.socialsquad.squad.presentation.feature.social.*
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.RecyclerViewUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.view_toolbar.*
import javax.inject.Inject


private const val REQUEST_CODE_CHANGED_IMAGE = 1

class ProfileFragment : Fragment(), ProfileView, ReselectedListener {

    @Inject
    lateinit var profilePresenter: ProfilePresenter


    private var socialAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var currentMostVisibleItemPosition = -1

    private var socialDeleteDialog: SocialDeleteDialog? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onDestroy() {
        profilePresenter.onDestroy()
        super.onDestroy()
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        view?.visibility = if (!isVisibleToUser) {
            setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
            rvPosts?.stopScroll()
            View.INVISIBLE
        } else {
            setMostVisibleItemPosition(currentMostVisibleItemPosition, -1)
            View.VISIBLE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        setupSwipeRefresh()
        setupList()

        setupTabs()
        profilePresenter.onViewCreated()
    }


    private fun setupTabs() {
        var userComnpanyJson =
            PreferenceManager.getDefaultSharedPreferences(activity).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        tlTabsPosts.setSelectedTabIndicatorColor(ColorUtils.parse(userCompany.company?.primaryColor!!))
    }

    fun doRefresh() {
        localposts?.let {
            showPosts(it)
        }
        viewPagerPosts.adapter?.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        Log.i("log", "onResume $this")
        if (doRefresh) {
            doRefresh = false
            Handler().postDelayed({ profilePresenter.onViewCreated() }, 500)
        }

        if (reloadCompleteProfile) {

            profilePresenter.reloadInfo()

            if (profilePresenter.isCompleted()) {
                var dialog = CompletedProfileDialog() {
                    var model = socialAdapter?.getItemByPosition(
                        2
                    )
                    (model as ProfileToCompleteViewModel).animate = true
                    socialAdapter?.notifyDataSetChanged()


                }
                dialog.show(childFragmentManager, "completed")
            } else {

            }

            reloadCompleteProfile = false
        }


        setMostVisibleItemPosition(currentMostVisibleItemPosition, -1)
        activity?.window?.statusBarColor = Color.WHITE
        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    override fun onStart() {
        super.onStart()
        Log.i("log", "onStart $this")
    }

    override fun onPause() {
        super.onPause()
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
    }

    private fun setupToolbar() {
        tToolbar.apply {
            menu.clear()
            inflateMenu(R.menu.menu_profile)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.profile_mi_settings -> {
                        profilePresenter.onSettingsSelected()
                        true
                    }
                    else -> true
                }
            }
        }
    }

    private fun setupSwipeRefresh() {
        //srlContainer.setOnRefreshListener { profilePresenter.onRefresh() }
    }

    private fun setupList() {
        activity?.let {
            socialAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is ProfilePointsViewModel -> PROFILE_POINTS_VIEW_HOLDER_ID
                    is ProfileWalletViewModel -> PROFILE_WALLET_VIEW_HOLDER_ID
                    is ProfileRevenueViewModel -> PROFILE_REVENUE_VIEW_HOLDER_ID
                    is ProfileRankingViewModel -> R.layout.item_card_ranking_profile
                    is ProfileRecognitionViewModel -> R.layout.item_card_recognition_profile
                    is ProfileViewModel -> R.layout.view_profile_header
                    is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                    is ProfileAboutViewModel -> PROFILE_ABOUT_VIEW_HOLDER_ID
                    is PostVideoViewModel -> SOCIAL_POST_VIDEO_VIEW_MODEL_ID
                    is PostImageViewModel -> SOCIAL_POST_IMAGE_VIEW_MODEL_ID
                    is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                    is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                    is ProfileToCompleteViewModel -> PROFILE_TO_COMPLETE_VIEW_HOLDER_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                    return when (viewType) {
                        PROFILE_TO_COMPLETE_VIEW_HOLDER_ID -> ProfileToCompleteViewHolder(
                            this@ProfileFragment.childFragmentManager,
                            view
                        ) {
                            Handler().postDelayed({
                                socialAdapter?.removeItem(2)
                            }, 100)
                        }

                        PROFILE_POINTS_VIEW_HOLDER_ID -> ProfilePointsViewHolder(view)

                        PROFILE_REVENUE_VIEW_HOLDER_ID -> ProfileRevenueViewHolder(view)

                        PROFILE_WALLET_VIEW_HOLDER_ID -> ProfileWalletViewHolder(view)

                        R.layout.item_card_recognition_profile -> ProfileRecognitionViewHolder(view)

                        R.layout.item_card_ranking_profile -> ProfileRankingViewHolder(view)

                        R.layout.view_profile_header -> ProfileHeaderViewHolder(
                            view,
                            onEditProfileListener
                        )
                        SOCIAL_POST_VIDEO_VIEW_MODEL_ID -> PostVideoViewHolder(
                            view,
                            onVideoClickedListener,
                            onAudioClickedListener,
                            onDeleteListener,
                            onEditListener,
                            onLikeClickListener,
                            onUsersLikedListener,
                            onCommentListener,
                            onCommentListListener
                        )
                        SOCIAL_POST_IMAGE_VIEW_MODEL_ID -> PostImageViewHolder(
                            view,
                            onDeleteListener, onEditListener, onLikeClickListener,
                            onUsersLikedListener, onCommentListener, onCommentListListener
                        )
                        LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                        HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                        PROFILE_ABOUT_VIEW_HOLDER_ID -> ProfileAboutViewHolder(
                            view,
                            onAboutItemClicked
                        )
                        DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                        else -> throw IllegalArgumentException()
                    }
                }
            })
            linearLayoutManager = PrefetchingLinearLayoutManager(it)
            rvPosts?.apply {
                adapter = socialAdapter
                layoutManager = linearLayoutManager
                setItemViewCacheSize(0)
                addOnScrollListener(EndlessScrollListener())
                setHasFixedSize(true)
                isFocusable = false
            }
        }
    }

    private val onAboutItemClicked = object : ViewHolder.Listener<ProfileAboutViewModel> {
        override fun onClick(viewModel: ProfileAboutViewModel) {
            val intent = Intent(activity, ProfileQualificationActivity::class.java)
            startActivity(intent)
        }
    }

    private val onCommentListListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL,
                        viewModel
                    )
                    is PostVideoViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL,
                        viewModel
                    )
                    is PostImageViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL,
                        viewModel
                    )
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
            }
            startActivity(intent)
        }
    }

    private val onCommentListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL,
                        viewModel
                    )
                    is PostVideoViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL,
                        viewModel
                    )
                    is PostImageViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL,
                        viewModel
                    )
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
                putExtra(PostDetailsActivity.IS_COMMENTING, true)
            }
            startActivity(intent)
        }
    }

    private val onUsersLikedListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.POST_LIKE)
            }
            startActivity(intent)
        }
    }
    private val onEditProfileListener = object : ViewHolder.Listener<ProfileViewModel> {
        override fun onClick(viewModel: ProfileViewModel) {
            startActivityForResult(
                Intent(activity, ProfileEditActivity::class.java),
                REQUEST_CODE_CHANGED_IMAGE
            )
        }
    }

    private val onDeleteListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onDeleteSelected(viewModel)
        }
    }

    fun onDeleteSelected(viewModel: PostViewModel) {
        activity?.let {
            socialDeleteDialog = SocialDeleteDialog(
                it,
                object : SocialDeleteDialog.Listener {
                    override fun onNoClicked() {
                        hideDeleteDialog()
                    }

                    override fun onYesClicked() {
                        profilePresenter.onDeleteClicked(viewModel)
                        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
                    }
                }
            )

            if (!it.isFinishing) socialDeleteDialog?.show()
        }
    }

    private val onLikeClickListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            profilePresenter.likePost(viewModel)
        }
    }

    private val onEditListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onEditSelected(viewModel)
        }
    }

    fun onEditSelected(viewModel: PostViewModel) {
        profilePresenter.onEditSelected(viewModel)
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<PostVideoViewModel> {
        override fun onClick(viewModel: PostVideoViewModel) {
            onVideoClicked(viewModel.hlsUrl)
        }
    }

    fun onVideoClicked(url: String) {
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
        val intent = Intent(activity, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    private val onAudioClickedListener = object : ViewHolder.Listener<AudioToggledViewModel> {
        override fun onClick(viewModel: AudioToggledViewModel) {
            onAudioToggled(viewModel.audioEnabled)
            val items = socialAdapter?.getItemFromType(PostVideoViewModel::class.java)
            items?.forEach {
                it.audioEnabled = viewModel.audioEnabled
                socialAdapter?.update(it, false)
            }
        }
    }

    override fun onAudioToggled(audioEnabled: Boolean) {
        profilePresenter.onAudioToggled(audioEnabled)
    }

    override fun stopRefreshing() {
        // srlContainer.isRefreshing = false
    }

    override fun openSettings() {
        startActivity(Intent(activity, SettingsActivity::class.java))
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(activity, getString(resId), onDismissListener)
    }

    override fun showProfile(profileViewModel: ProfileViewModel, useQualification: Boolean) {
        tToolbar.title = ""
        //tvTitle.text = "           ${profileViewModel.firstName} ${profileViewModel.lastName} "
        tvTitle.text = "           Member"

        var completeModel = ProfileToCompleteViewModel(profileViewModel.completeYourProfile)

        socialAdapter?.setItems(
            mutableListOf(
                profileViewModel,
                DividerViewModel,

                ).apply {

                var toShow = if (reloadCompleteProfile) true else completeModel.isComplete().not()

                if (toShow) {
                    add(completeModel)
                }

                profileViewModel.wallet_tools?.show_wallet?.let { toShow ->
                    if (toShow.not())
                        this.add(
                            ProfileWalletViewModel(
                                profileViewModel.wallet_tools?.data?.wallet_title!!,
                                profileViewModel.wallet_tools?.data?.wallet_subtitle!!
                            )
                        )
                }

                profileViewModel.wallet_tools?.show_revenue?.let { toShow ->
                    if (toShow.not())
                        this.add(
                            ProfileRevenueViewModel(
                                profileViewModel.wallet_tools.data?.revenue_title!!,
                                profileViewModel.wallet_tools.data?.revenue_subtitle!!
                            )
                        )

                }

                profileViewModel.personal_points?.let {
                    this.add(
                        ProfilePointsViewModel(
                            it.title ?: "",
                            it.subtitle ?: "",
                            it.icon ?: "",
                            it.points_system_id ?: ""
                        )
                    )
                }

                profileViewModel.recognition?.multiple_plans?.let {
                    profileViewModel.recognition?.let {
                        this.add(
                            ProfileRecognitionViewModel(it)
                        )
                    }
                }


                profileViewModel.ranking?.number_of_ranks?.let {
                    if (it > 0)
                        this.add(
                            ProfileRankingViewModel(
                                profileViewModel.ranking?.title,
                                profileViewModel.ranking?.subtitle,
                                number_of_ranks = profileViewModel.ranking?.number_of_ranks,
                                icon = profileViewModel.ranking?.icon,
                                participants = profileViewModel.ranking?.participants,
                                points = profileViewModel.ranking?.points,
                                user_in_rank = profileViewModel.ranking?.user_in_rank
                                    ?: true,
                                position = profileViewModel.ranking?.position,
                                rank_id = profileViewModel.ranking?.rank_id
                            )
                        )
                }

            })
        socialAdapter?.addItems(
            mutableListOf<ViewModel>().apply {
//                    if (useQualification) {
//                        add(HeaderViewModel(getString(R.string.profile_about_title)))
//                        add(ProfileAboutViewModel(profileViewModel.pk, profileViewModel.qualification.toString(), getString(R.string.qualification_title), profileViewModel.qualificationBadge.orEmpty(), true))
//                    }
            }
        )
    }

    private var localposts: List<SocialViewModel>? = null

    override fun showPosts(posts: List<SocialViewModel>) {
        localposts = posts
        Log.i("posts", Gson().toJson(posts))
        configureViewPager(posts)
    }

    override fun showLoading() {
        llLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        llLoading.visibility = View.GONE
        //srlContainer.visibility = View.VISIBLE
    }

    override fun hideDeleteDialog() {
        socialDeleteDialog?.dismiss()
    }

    override fun showLoadingMore() {
        socialAdapter?.startLoading()
    }

    override fun hideLoadingMore() {
        socialAdapter?.stopLoading()
    }

    override fun showEditMedia(post: PostViewModel) {
        val intent = Intent(activity, SocialCreateMediaActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_MEDIA)
    }

    override fun showEditLive(post: PostLiveViewModel) {
        val intent = Intent(activity, SocialCreateLiveActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_LIVE)
    }

    override fun removePost(viewModel: ViewModel) {
        socialAdapter?.remove(viewModel)
    }

    private inner class EndlessScrollListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 8

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            linearLayoutManager?.apply {
                if (findLastVisibleItemPosition() > itemCount - visibleThreshold) {
                    profilePresenter.onScrolledBeyondVisibleThreshold()
                }

                val rect = Rect()
                recyclerView.getGlobalVisibleRect(rect)
                val containerHeight = rect.height()
                val referencePoint = containerHeight / 2 + rect.top

                val firstVisibleItemPosition = findFirstVisibleItemPosition()
                val lastVisibleItemPosition = findLastVisibleItemPosition()

                val previousMostVisibleItemPosition = currentMostVisibleItemPosition

                var mostVisibleItemPosition = firstVisibleItemPosition
                var leastDifferenceFromReferencePoint = containerHeight
                for (i in firstVisibleItemPosition..lastVisibleItemPosition) {
                    val viewHolder = recyclerView.findViewHolderForAdapterPosition(i)
                    if (viewHolder != null) {
                        val r = Rect()
                        viewHolder.itemView.getGlobalVisibleRect(r)
                        val difference = Math.abs(r.centerY() - referencePoint)
                        if (difference < leastDifferenceFromReferencePoint) {
                            leastDifferenceFromReferencePoint = difference
                            mostVisibleItemPosition = i
                        }
                    }
                }

                if (previousMostVisibleItemPosition != mostVisibleItemPosition) {
                    currentMostVisibleItemPosition = mostVisibleItemPosition
                    setMostVisibleItemPosition(
                        mostVisibleItemPosition,
                        previousMostVisibleItemPosition
                    )
                }
            }
        }
    }

    private fun setMostVisibleItemPosition(
        mostVisibleItemPosition: Int,
        previousMostVisibleItemPosition: Int
    ) {
        if (!userVisibleHint) {
            stopVideo(previousMostVisibleItemPosition)
            return
        }
        stopVideo(previousMostVisibleItemPosition)
        playVideo(mostVisibleItemPosition)
    }

    private fun stopVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.stop()
        }
    }

    private fun playVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play()
        }
    }

    private fun continueVideo(position: Int, playbackPosition: Long) {
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play(playbackPosition)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_CHANGED_IMAGE && resultCode == RESULT_OK) {
            //  srlContainer.isRefreshing = true
            socialAdapter?.setItems(emptyList())
            profilePresenter.onRefresh()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun updatePost(post: PostViewModel) {}

    override fun onReselected() {
        linearLayoutManager?.let {
            activity?.let { activity ->
                RecyclerViewUtils.scrollToTop(
                    activity,
                    it
                )
            }
        }
    }

    private fun configureViewPager(posts1: List<SocialViewModel>) {
        feed.isVisible = true
        var fragments = mutableListOf<Fragment>()
        if (posts1.isEmpty()) {
            fragments.add(ProfileEmptyPublishFragment())
            fragments.add(ProfileEmptyPublishFragment())
        } else {
            fragments.add(ProfilePublishGridFragment.newInstance("grid").apply {
                posts = posts1
            })
            fragments.add(ProfilePublishGridFragment.newInstance("list").apply {
                posts = posts1
            })
        }

        viewPagerPosts.offscreenPageLimit = 2
        viewPagerPosts.adapter = object : FragmentStateAdapter(requireActivity()) {
            override fun getItemCount() = fragments.size

            override fun createFragment(position: Int) = fragments[position]
        }

        TabLayoutMediator(tlTabsPosts, viewPagerPosts, false, false) { tab, position ->
            when (position) {

                0 -> {
                    val view =
                        layoutInflater.inflate(R.layout.tab_view_profile_grid, tlTabsPosts, false)
                    view.measure(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    tab.customView = view
                }

                1 -> {
                    val view =
                        layoutInflater.inflate(R.layout.tab_view_profile_list, tlTabsPosts, false)
                    view.measure(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    tab.customView = view
                    var icon = tab.customView?.findViewById<ImageView>(R.id.ivIcon)
                    icon?.setColorFilter(ColorUtils.parse("#939393"), PorterDuff.Mode.SRC_IN)
                }
            }

        }.attach()

        tlTabsPosts.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var icon = tab.customView?.findViewById<ImageView>(R.id.ivIcon)
                icon?.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                var icon = tab.customView?.findViewById<ImageView>(R.id.ivIcon)
                icon?.setColorFilter(ColorUtils.parse("#939393"), PorterDuff.Mode.SRC_IN)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    companion object {
        var doRefresh: Boolean = false
        var reloadCompleteProfile: Boolean = false
    }
}
