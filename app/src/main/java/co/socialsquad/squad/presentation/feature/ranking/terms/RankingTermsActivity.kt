package co.socialsquad.squad.presentation.feature.ranking.terms

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.feature.ranking.di.RankingListModule
import co.socialsquad.squad.presentation.feature.ranking.model.RankingItem
import co.socialsquad.squad.presentation.feature.ranking.model.RankingTerms
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_ranking_terms.*
import org.koin.core.module.Module


class RankingTermsActivity : BaseActivity() {

    companion object {
        fun start(context: Context, rankingItem: RankingItem?, rankingTerms: RankingTerms?) {
            context.startActivity(Intent(context, RankingTermsActivity::class.java).apply {
                putExtra("terms", rankingTerms)
                putExtra("rank", rankingItem)
            })
        }
    }

    override val modules: List<Module> = listOf(RankingListModule.instance)
    override val contentView: Int = R.layout.activity_ranking_terms

    private val terms by extra<RankingTerms>("terms")
    private val rank by extra<RankingItem>("rank")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = rank.title

        titleTerms.text = terms.title
        lastUpdateTerms.text = terms.last_update
        descriptionTerms.text = terms.introduction

        terms.sections?.get(0)?.let {
            titleSection1.text = it.title
            textSection1.text = it.content
        }

        terms.sections?.get(1)?.let {
            titleSection2.text = it.title
            textSection2.text = it.content
        }

        terms.sections?.get(2)?.let {
            titleSection3.text = it.title
            textSection3.text = it.content
        }
        terms.sections?.get(3)?.let {
            titleSection4.text = it.title
            textSection4.text = it.content
        }
        terms.sections?.get(4)?.let {
            titleSection5.text = it.title
            textSection5.text = it.content
        }

        terms.sections?.get(5)?.let {
            titleSection6.text = it.title
            textSection6.text = it.content
        }

        terms.sections?.get(6)?.let {
            titleSection7.text = it.title
            textSection7.text = it.content
        }

        terms.sections?.get(7)?.let {
            titleSection8.text = it.title
            textSection8.text = it.content
        }

        closeArea.setOnClickListener {
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}