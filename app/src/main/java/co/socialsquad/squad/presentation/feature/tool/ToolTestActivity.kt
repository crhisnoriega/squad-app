package co.socialsquad.squad.presentation.feature.tool

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.App
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionIcon
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Tool
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ToolContent
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.tool.submission.OpportunitySubmissionListActivity
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.activity_tool_test.*
import kotlinx.android.synthetic.main.activity_tool_test_item.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.module.Module

private const val TOOL_ITEM_VIEW_ID = R.layout.activity_tool_test_item
private val companyColor by lazy {
    App.companyColor
}

class ToolTestActivity : BaseActivity() {

    override val modules: List<Module> = listOf()
    override val contentView = R.layout.activity_tool_test
    private val iconSize = resources.getDimensionPixelSize(R.dimen.button_icon_size)

    private val adapterTool = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is ToolViewHolderModel -> TOOL_ITEM_VIEW_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            TOOL_ITEM_VIEW_ID -> ToolViewHolder(view)
            else -> throw IllegalArgumentException()
        }
    })

    private val tools = listOf(
            Tool(
                    id = 1,
                    type = RankingType.Oportunidade,
                    button = Button(
                            text = "Indicar",
                            icon = SectionIcon(url = "https://d1gpti4ogccykg.cloudfront.net/opportunity_referral_3x.png"),
                            link = null
                    ),
                    content = ToolContent(
                            icon = SimpleMedia(url = "https://d1gpti4ogccykg.cloudfront.net/opportunity_referral_3x.png"),
                            title = "Indicações",
                            subtitle = "Suas indicações para esse imóvel",
                            link = "/opportunity/1/object/99/submission?limit=3"
                    )
            ),
            Tool(
                    id = 2,
                    type = RankingType.Oportunidade,
                    button = Button(
                            text = "Agendar",
                            icon = SectionIcon(url = "https://d1gpti4ogccykg.cloudfront.net/opportunity_scheduling_3x.png"),
                            link = null
                    ),
                    content = ToolContent(
                            icon = SimpleMedia(url = "https://d1gpti4ogccykg.cloudfront.net/opportunity_scheduling_3x.png"),
                            title = "Agendamentos",
                            subtitle = "Seus agendamentos para esse imóvel",
                            link = "/opportunity/2/object/99/submission?limit=3"
                    )
            )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        rvTools.adapter = adapterTool
        adapterTool.setItems(
                tools.map {
                    ToolViewHolderModel(it)
                }
        )

        // tools.bind(tool = tool, color = companyColor)
        // tools.onSubmissionClickListener = {}
        // tools.onEmptyClickListener = {}
        // tools.onShowAllClickListener = {
        //     SubmissionListActivity.start(this, it)
        // }

        tools.forEachIndexed { index, tool ->
            when (index) {
                0 -> {
                    loadIcon(tool.button?.icon?.url, btnTool1)
                    tool.button?.text?.let {
                        btnTool1.text = it
                        btnTool1.isVisible = true
                    } ?: run { btnTool1.isVisible = false }
                }
                1 -> {
                    loadIcon(tool.button?.icon?.url, btnTool2)
                    tool.button?.text?.let {
                        btnTool2.text = it
                        btnTool2.isVisible = true
                    } ?: run { btnTool2.isVisible = false }
                }
                2 -> {
                    loadIcon(tool.button?.icon?.url, btnTool3)
                    tool.button?.text?.let {
                        btnTool3.text = it
                        btnTool3.isVisible = true
                    } ?: run { btnTool3.isVisible = false }
                }
            }
        }
    }

    private fun loadIcon(url: String?, view: AppCompatButton) {
        url?.let {
            Glide.with(this)
                    .asBitmap()
                    .load(it)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) = Unit

                        override fun onResourceReady(
                                resource: Bitmap,
                                transition: Transition<in Bitmap>?
                        ) {
                            val drawable = BitmapDrawable(resources, resource)
                            drawable.setBounds(0, 0, iconSize, iconSize)
                            view.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    drawable,
                                    null,
                                    null,
                                    null
                            )
                        }
                    })
        }
    }
}

class ToolViewHolder(itemView: View) : ViewHolder<ToolViewHolderModel>(itemView) {
    @ExperimentalCoroutinesApi
    override fun bind(viewModel: ToolViewHolderModel) {
        itemView.tool.bind(objectType = "", objectId = 1, tool = viewModel.tool, color = companyColor)
        // tools.bind(tool = tool, color = companyColor)
        itemView.tool.onSubmissionClickListener = {}
        itemView.tool.onEmptyClickListener = {}
        itemView.tool.onShowAllOpportunitySubmission = {
            OpportunitySubmissionListActivity.start("", itemView.context, it)
        }
    }

    override fun recycle() {
    }
}

class ToolViewHolderModel(val tool: Tool) : ViewModel
