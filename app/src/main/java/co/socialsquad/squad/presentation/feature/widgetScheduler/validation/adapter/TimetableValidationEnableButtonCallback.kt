package co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter

interface TimetableValidationEnableButtonCallback {
    fun changeButtonState(enable: Boolean)
}