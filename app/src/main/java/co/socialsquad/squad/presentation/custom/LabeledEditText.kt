package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.linearlayout_labeled_edittext.view.*

class LabeledEditText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), View.OnClickListener, InputView {

    private var onInputListener: InputView.OnInputListener? = null

    var text: String
        get() = etInput.text.toString()
        set(text) {
            etInput.setText(text)
        }

    val editText: EditText
        get() = etInput

    val isEmpty: Boolean
        get() = etInput.text.toString().isEmpty()

    init {
        val view = View.inflate(context, R.layout.linearlayout_labeled_edittext, this)
        setupStyledAttributes(attrs)
        setupViews(view)
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.LabeledEditText)

        val text = styledAttributes.getString(R.styleable.LabeledEditText_android_text)
        tvLabel.setText(text)

        val hint = styledAttributes.getString(R.styleable.LabeledEditText_android_hint)
        etInput.setHint(hint)

        val inputType = styledAttributes.getInt(
            R.styleable.LabeledEditText_android_inputType,
            EditorInfo.TYPE_TEXT_FLAG_CAP_SENTENCES
        )
        etInput.inputType = inputType or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

        val imeOptions = styledAttributes.getInt(
            R.styleable.LabeledEditText_android_imeOptions,
            EditorInfo.IME_ACTION_NEXT
        )
        etInput.imeOptions = imeOptions

        isEnabled = styledAttributes.getBoolean(
            R.styleable.LabeledEditText_android_enabled,
            true
        )
        etInput.isEnabled = isEnabled

        styledAttributes.recycle()
    }

    private fun setupViews(view: View) {
        if (isEnabled) view.setOnClickListener(this)

        etInput.setOnClickListener { etInput.setSelection(etInput.length()) }
        etInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (onInputListener != null) onInputListener!!.onInput()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        setBackgroundResource(R.drawable.ripple_labeled_view_background)
    }

    override fun onClick(v: View) {
        etInput.requestFocus()
        showKeyboard(context, etInput)
    }

    private fun showKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    fun length(): Int {
        return etInput.text.length
    }

    override fun isValid(valid: Boolean) {
        setBackgroundResource(
            if (valid)
                R.drawable.ripple_labeled_view_background
            else
                R.drawable.ripple_labeled_invalid_view_background
        )
    }
}
