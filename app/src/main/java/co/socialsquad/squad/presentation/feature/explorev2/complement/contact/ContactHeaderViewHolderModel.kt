package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.os.Parcelable
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContactHeaderViewHolderModel(val title: String) : ViewModel, Parcelable