package co.socialsquad.squad.presentation.feature.splash

import dagger.Binds
import dagger.Module

@Module
abstract class SplashModule {
    @Binds
    internal abstract fun view(splashActivity: SplashActivity): SplashView
}
