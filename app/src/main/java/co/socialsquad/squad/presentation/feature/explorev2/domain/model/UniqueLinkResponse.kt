package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UniqueLinkResponse(
        @SerializedName("success") val type: Boolean?,
        @SerializedName("data") val data: UniqueLinkData?,
        @SerializedName("error") val error: String?
):Parcelable