package co.socialsquad.squad.presentation.feature.opportunity.opportunity_list

import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType

/**
 * Every object detail presenter that should have the opportunities list, should implement this
 * interface
 */
interface OpportunityListPresenter {

    fun getOpportunityAction(opportunity: Opportunity)

    fun getOpportunityInfo(opportunity: Opportunity)

    fun getSubmissionDetails(
        opportunityId: Int,
        objectId: Int,
        submissionId: Int,
        objectType: OpportunityObjectType
    )
}
