package co.socialsquad.squad.presentation.feature.opportunityv2.pages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityStage
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder.DividerOpportunityStageViewHolder
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder.OpportunityStageViewHolder
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder.OpportunityStageViewModel
import co.socialsquad.squad.presentation.util.argument
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_opportunity_page_stages.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class OpportunityPageStages : Fragment() {
    private val stagesJson: String by argument(ARG_PARAM1)
    private var stages: List<OpportunityStage>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_opportunity_page_stages, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        stages = Gson().fromJson(stagesJson, Array<OpportunityStage>::class.java).toList()

        setupStagesList()


    }

    private fun setupStagesList() {
        rvStages.apply {
            layoutManager = LinearLayoutManager(context)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    var stageModel = viewModel as OpportunityStageViewModel
                    return when (stageModel.stage.status) {
                        "divider" -> R.layout.item_card_opportunity_stage_divider
                        "divider2" -> R.layout.item_card_opportunity_stage_divider_2
                        "pending" -> R.layout.item_card_opportunity_stage_pending
                        "completed" -> R.layout.item_card_opportunity_stage_aproved
                        "blocked" -> R.layout.item_card_opportunity_stage_blocked
                        "closed" -> R.layout.item_card_opportunity_stage_complete
                        else -> R.layout.item_card_opportunity_stage_pending
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_card_opportunity_stage_pending -> OpportunityStageViewHolder(view)
                    R.layout.item_card_opportunity_stage_aproved -> OpportunityStageViewHolder(view)
                    R.layout.item_card_opportunity_stage_complete -> OpportunityStageViewHolder(view)
                    R.layout.item_card_opportunity_stage_blocked -> OpportunityStageViewHolder(view)
                    R.layout.item_card_opportunity_stage_divider -> DividerOpportunityStageViewHolder(view)
                    R.layout.item_card_opportunity_stage_divider_2 -> DividerOpportunityStageViewHolder(view)
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter.apply {
                var stagesModels = stages?.map {
                    OpportunityStageViewModel(it, stages?.indexOf(it) == 0, (stages?.indexOf(it) == stages?.size!! - 1))
                }?.toMutableList()

                stagesModels?.add(0, OpportunityStageViewModel(OpportunityStage(status = "divider"), isFirst = false, isLast = false))
                stagesModels?.add(OpportunityStageViewModel(OpportunityStage(status = "divider2"), isFirst = false, isLast = false))

                setItems(stagesModels!!)

                rootLayout.requestLayout()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        rootLayout.requestLayout()
    }

    override fun onStart() {
        super.onStart()

    }

    fun requestLayout() {
        rootLayout.requestLayout()
    }

    companion object {
        @JvmStatic
        fun newInstance(stages: List<OpportunityStage>) =
                OpportunityPageStages().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, Gson().toJson(stages))
                    }
                }
    }
}