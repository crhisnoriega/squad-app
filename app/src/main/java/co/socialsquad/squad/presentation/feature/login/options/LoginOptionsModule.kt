package co.socialsquad.squad.presentation.feature.login.options

import dagger.Module

@Module
interface LoginOptionsModule {
    fun view(view: LoginOptionsActivity)
}
