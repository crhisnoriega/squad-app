package co.socialsquad.squad.presentation.feature.login.subdomain

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FindSubdomainRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_LOGIN_SUBDOMAIN
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginSubdomainPresenter @Inject constructor(
    private val view: LoginSubdomainView,
    private val loginRepository: LoginRepository,
    private val preferences: Preferences,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var subdomain: String = ""

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_LOGIN_SUBDOMAIN)
        clearData()
    }

    private fun clearData() {
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.subdomain = null
        loginRepository.clearSharingAppDialogTimer()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onNextClicked() {
        val request = FindSubdomainRequest(subdomain)

        compositeDisposable.add(
            loginRepository.findSubdomain(request)
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        loginRepository.subdomain = subdomain
                        view.openLoginEmail(subdomain)
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.login_subdomain_error)
                    }
                )
        )
    }

    fun onAlternativeClicked() {
        view.openLoginAlternative(subdomain)
    }

    fun onInputChanged(subdomain: String) {
        this.subdomain = subdomain.trim()

        val isValid = subdomain.isNotBlank()
        view.enableNextButton(isValid)
    }
}
