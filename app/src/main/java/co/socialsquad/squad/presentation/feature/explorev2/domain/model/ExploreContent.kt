package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.data.typeAdapter.SectionTypeAdapter
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreContent(
    @SerializedName("id") val id: Int,
    @SerializedName("template") @JsonAdapter(SectionTemplateTypeAdapter::class) val template: SectionTemplate,
    @SerializedName("title") val title: String,
    @SerializedName("subtitle") val subtitle: String? = null,
    @SerializedName("link") val link: Link? = null,
    @SerializedName("button") val button: Button? = null,
    @SerializedName("type") @JsonAdapter(SectionTypeAdapter::class) val type: SectionType,
    @SerializedName("items") var items: List<ContentItem>,
    @SerializedName("scroll_time") val scrollTime: Int,
    @SerializedName("auto_scroll") val autoScroll: Boolean
) : Parcelable
