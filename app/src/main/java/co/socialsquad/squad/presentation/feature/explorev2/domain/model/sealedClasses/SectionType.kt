package co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val VALUE_BANNER = "banner"
private const val VALUE_CATEGORY = "category"
private const val VALUE_VERTICAL_COLLECTION = "vertical_collection"
private const val VALUE_HORIZONTAL_COLLECTION = "horizontal_collection"
private const val VALUE_LIST = "list"
private const val VALUE_EMPTY = "empty"

sealed class SectionType(val value: String) : Parcelable {

    companion object {
        fun getDefault() = HorizontalCollection
        fun getByValue(value: String) = when (value) {
            VALUE_BANNER -> Banner
            VALUE_CATEGORY -> Category
            VALUE_VERTICAL_COLLECTION -> VerticalCollection
            VALUE_HORIZONTAL_COLLECTION -> HorizontalCollection
            VALUE_LIST -> List
            VALUE_EMPTY -> Empty
            else -> getDefault()
        }
    }

    @Parcelize
    object Banner : SectionType(VALUE_BANNER)

    @Parcelize
    object Category : SectionType(VALUE_CATEGORY)

    @Parcelize
    object VerticalCollection : SectionType(VALUE_VERTICAL_COLLECTION)

    @Parcelize
    object HorizontalCollection : SectionType(VALUE_HORIZONTAL_COLLECTION)

    @Parcelize
    object List : SectionType(VALUE_LIST)

    @Parcelize
    object Empty : SectionType(VALUE_EMPTY)
}
