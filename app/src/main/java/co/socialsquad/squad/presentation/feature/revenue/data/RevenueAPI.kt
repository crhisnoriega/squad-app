package co.socialsquad.squad.presentation.feature.revenue.data

import co.socialsquad.squad.presentation.feature.ranking.model.RankingResponse
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import co.socialsquad.squad.presentation.feature.recognition.model.PlanSummaryResponse
import co.socialsquad.squad.presentation.feature.revenue.model.EventsResponse
import co.socialsquad.squad.presentation.feature.revenue.model.StreamsResponse
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface RevenueAPI {

    @GET("/endpoints/revenue/streams")
    suspend fun fetchStreams(): StreamsResponse

    @GET("/endpoints/revenue/entries")
    suspend fun fetchEvents(): EventsResponse


}