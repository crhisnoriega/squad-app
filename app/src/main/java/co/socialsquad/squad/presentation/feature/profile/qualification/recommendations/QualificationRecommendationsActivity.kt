package co.socialsquad.squad.presentation.feature.profile.qualification.recommendations

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Recommendation
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.audio.AUDIO_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.audio.list.AudioListItemViewHolder
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import co.socialsquad.squad.presentation.feature.event.list.EVENT_LIST_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.event.list.EventListItemViewHolder
import co.socialsquad.squad.presentation.feature.live.list.LIVE_LIST_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.live.list.LiveListItemViewHolder
import co.socialsquad.squad.presentation.feature.live.list.LiveListItemViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.feature.video.list.VIDEO_LIST_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewHolder
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewModel
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_qualification_recommendations.*
import kotlinx.android.synthetic.main.toolbar_kickoff.*
import javax.inject.Inject

class QualificationRecommendationsActivity : BaseDaggerActivity(), QualificationRecommendationsView {
    companion object {
        const val EXTRA_RECOMMENDATION_TYPE = "EXTRA_RECOMMENDATION_TYPE"
        const val EXTRA_RECOMMENDATION_PK = "EXTRA_RECOMMENDATION_PK"
        const val EXTRA_RECOMMENDATION_PROGRESS = "EXTRA_RECOMMENDATION_PROGRESS"
        const val EXTRA_RECOMMENDATION_TOTAL = "EXTRA_RECOMMENDATION_TOTAL"
        const val REQUEST_CODE_RECOMMEND_LIVE = 4
        const val REQUEST_CODE_RECOMMEND_AUDIO = 3
        const val REQUEST_CODE_RECOMMEND_VIDEO = 2
        const val REQUEST_CODE_VIDEO_POSITION = 1
    }

    @Inject
    lateinit var presenter: QualificationRecommendationsPresenter
    private var adapter: RecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualification_recommendations)
        setupList()
        presenter.onCreate(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_VIDEO_POSITION -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.getLongExtra(VideoActivity.EXTRA_PLAYBACK_POSITION, 0L)?.let {
                        presenter.onVideoReturned(it)
                    }
                }
            }
            REQUEST_CODE_RECOMMEND_LIVE -> {
                (data?.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                    presenter.onRecommend(it.values.map { it.value }, Recommendation.LIVE)
                }
            }
            REQUEST_CODE_RECOMMEND_AUDIO -> {
                (data?.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                    presenter.onRecommend(it.values.map { it.value }, Recommendation.AUDIO)
                }
            }
            REQUEST_CODE_RECOMMEND_VIDEO -> {
                (data?.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                    presenter.onRecommend(it.values.map { it.value }, Recommendation.FEED)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun setupToolbar(titleId: Int, progress: Int, total: Int) {
        setSupportActionBar(tToolbar)
        tvToolbarTitle.text = getString(titleId)
        tvToolbarSubtitle.text = getString(R.string.progress, progress, total)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is EventViewModel -> EVENT_LIST_ITEM_VIEW_MODEL_ID
                is AudioViewModel -> AUDIO_VIEW_MODEL_ID
                is LiveListItemViewModel -> LIVE_LIST_ITEM_VIEW_MODEL_ID
                is VideoListItemViewModel -> VIDEO_LIST_ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                EVENT_LIST_ITEM_VIEW_MODEL_ID -> EventListItemViewHolder(view)
                AUDIO_VIEW_MODEL_ID -> AudioListItemViewHolder(view, onRecommendAudioListener)
                LIVE_LIST_ITEM_VIEW_MODEL_ID -> LiveListItemViewHolder(view, onLiveClickedListener, onRecommendLiveListener)
                VIDEO_LIST_ITEM_VIEW_MODEL_ID -> VideoListItemViewHolder(view, onVideoClickedListener, onRecommendVideoListener)
                else -> throw IllegalArgumentException()
            }
        })
        rvRecommendations.apply {
            adapter = this@QualificationRecommendationsActivity.adapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    rvRecommendations?.layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            addItemDecoration(ListDividerItemDecoration(context))
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
        setupSwipeRefresh()
    }

    private val onRecommendAudioListener = object : ViewHolder.Listener<AudioViewModel> {
        override fun onClick(viewModel: AudioViewModel) {
            presenter.onRecommendingAudio(viewModel)
            val segmentName = getString(R.string.qualification_title)
            val metric = MetricViewModel("qualification", segmentName, listOf(), true, null)
            val intent = Intent(this@QualificationRecommendationsActivity, SegmentValuesActivity::class.java)
            intent.putExtra(EXTRA_METRICS, metric)
            startActivityForResult(intent, REQUEST_CODE_RECOMMEND_AUDIO)
        }
    }

    private val onRecommendLiveListener = object : ViewHolder.Listener<LiveListItemViewModel> {
        override fun onClick(viewModel: LiveListItemViewModel) {
            presenter.onRecommendingLive(viewModel)
            val segmentName = getString(R.string.qualification_title)
            val metric = MetricViewModel("qualification", segmentName, listOf(), true, null)
            val intent = Intent(this@QualificationRecommendationsActivity, SegmentValuesActivity::class.java)
            intent.putExtra(EXTRA_METRICS, metric)
            startActivityForResult(intent, REQUEST_CODE_RECOMMEND_LIVE)
        }
    }

    private val onRecommendVideoListener = object : ViewHolder.Listener<VideoListItemViewModel> {
        override fun onClick(viewModel: VideoListItemViewModel) {
            presenter.onRecommendingVideo(viewModel)
            val segmentName = getString(R.string.qualification_title)
            val metric = MetricViewModel("qualification", segmentName, listOf(), true, null)
            val intent = Intent(this@QualificationRecommendationsActivity, SegmentValuesActivity::class.java)
            intent.putExtra(EXTRA_METRICS, metric)
            startActivityForResult(intent, REQUEST_CODE_RECOMMEND_VIDEO)
        }
    }

    private fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener { presenter.onRefresh() }
    }

    private val onLiveClickedListener = object : ViewHolder.Listener<LiveListItemViewModel> {
        override fun onClick(viewModel: LiveListItemViewModel) {
            presenter.onLiveClicked(viewModel)
        }
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<VideoListItemViewModel> {
        override fun onClick(viewModel: VideoListItemViewModel) {
            presenter.onVideoClicked(viewModel)
        }
    }

    override fun startLoading() {
        adapter?.startLoading()
        if (!srlLoading.isRefreshing) srlLoading.isEnabled = false
    }

    override fun stopLoading() {
        adapter?.stopLoading()
        srlLoading.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun addItems(items: List<ViewModel>, shouldClearList: Boolean) {
        if (shouldClearList) adapter?.setItems(items) else adapter?.addItems(items)
    }

    override fun updateItem(viewModel: VideoListItemViewModel) {
        adapter?.update(viewModel)
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit?) {
        ViewUtils.showDialog(this, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun stopRefreshing() {
        srlLoading.apply {
            isRefreshing = false
            isEnabled = true
        }
    }

    override fun openVideo(url: String) {
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }
}
