package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.ranking.model.PrizeData

class ItemPrizeRankingViewModel(
        val data: PrizeData) : ViewModel {
}