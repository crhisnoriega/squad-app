package co.socialsquad.squad.presentation.feature.chat.chatroom

import androidx.lifecycle.MutableLiveData
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable
import java.util.Date

const val CHAT_ROOM_ITEM_VIEW_MODEL_ID = R.layout.view_chat_room_item

class ChatRoomViewModel(
    val pk: Int = 0,
    val companyColor: String?,
    val channelUrl: String? = null,
    val avatarUrl: String? = null,
    val name: String? = null,
    val qualification: String? = null,
    val qualificationBadge: String? = null,
    var chatId: String? = null,
    var canCall: Boolean = false,
    var unreadCount: Int = 0
) : ViewModel, Serializable {
    constructor(chatRoom: ChatRoomModel, companyColor: String?) : this(
        pk = chatRoom.pk,
        companyColor = companyColor,
        channelUrl = chatRoom.channelUrl,
        avatarUrl = chatRoom.avatarUrl,
        name = chatRoom.name,
        qualification = chatRoom.qualification,
        qualificationBadge = chatRoom.qualificationBadge,
        chatId = chatRoom.chatId,
        unreadCount = chatRoom.unreadCount,
        canCall = chatRoom.canCall
    ) {
        isOnline = chatRoom.isOnline
        lastSeen = Date(chatRoom.lastSeen ?: 0)
        lastMessageMutable.value = chatRoom.lastMessage
        elapsedTime.value = Date(chatRoom.elapsedTime ?: Date().time)
    }

    val lastMessageMutable: MutableLiveData<String?> = MutableLiveData()
    val elapsedTime: MutableLiveData<Date?> = MutableLiveData()

    var isOnline: Boolean = false
    var lastSeen: Date? = null
        set(value) {
            field = value?.takeIf { it.time != 0L }
        }
}
