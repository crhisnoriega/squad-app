package co.socialsquad.squad.presentation.feature.profile.edit.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_profile_bank_result.view.*


class ItemProfileBankViewHolder(val itemView: View, val onSelect: (bankInfo: BankInfo) -> Unit) :
    ViewHolder<ItemProfileBankViewModel>(itemView) {

    override fun bind(viewModel: ItemProfileBankViewModel) {
        itemView.topDivider22.isVisible = viewModel.isFirst!!
        itemView.bankNumberLayout.backgroundTintList =
            ColorUtils.stateListOf(ColorUtils.getCompanyColor(itemView.context)!!)

        itemView.rbChecked.setOnClickListener {
            onSelect.invoke(viewModel.bankInfo)
        }

        itemView.mainLayout.setOnClickListener {
            onSelect.invoke(viewModel.bankInfo)
        }

        itemView.bankCode.text = viewModel.bankInfo.code_number
        itemView.bankName.text = viewModel.bankInfo.short_name

        itemView.rbChecked.isChecked = viewModel.bankInfo.isChecked
    }

    override fun recycle() {
    }
}