package co.socialsquad.squad.presentation.feature.social.details

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.AudioToggledViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialDeleteDialog
import co.socialsquad.squad.presentation.feature.social.SocialFragment.Companion.REQUEST_CODE_EDIT_LIVE
import co.socialsquad.squad.presentation.feature.social.SocialFragment.Companion.REQUEST_CODE_EDIT_MEDIA
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.comment.COMMENT_VIEW_ID
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewHolder
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewModel
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_post_details.*
import kotlinx.android.synthetic.main.view_comment_box.*
import kotlinx.android.synthetic.main.view_social_post_content.*
import kotlinx.android.synthetic.main.view_social_post_interactions.*
import javax.inject.Inject

class PostDetailsActivity : BaseDaggerActivity(), PostDetailsView {

    companion object {
        const val POST_DETAIL_PK = "POST_DETAIL_PK"
        const val POST_DETAIL_LIVE_VIEW_MODEL = "POST_DETAIL_LIVE_VIEW_MODEL"
        const val POST_DETAIL_VIDEO_VIEW_MODEL = "POST_DETAIL_VIDEO_VIEW_MODEL"
        const val POST_DETAIL_IMAGE_VIEW_MODEL = "POST_DETAIL_IMAGE_VIEW_MODEL"
        const val IS_COMMENTING = "IS_COMMENTING"
    }

    @Inject
    lateinit var postDetailsPresenter: PostDetailsPresenter

    private var commentAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager = LinearLayoutManager(this)
    private var socialDeleteDialog: SocialDeleteDialog? = null
    private var commentsCount = 0
    private var likesCount = 0
    private var postImageViewHolder: PostImageViewHolder? = null
    private var postVideoViewHolder: PostVideoViewHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_details)
        setupToolbar()
        setupCommentList()
        postDetailsPresenter.onCreate(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK || data == null) return

        if (requestCode == SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN) {
            val playbackPosition = data.getLongExtra(VideoActivity.EXTRA_PLAYBACK_POSITION, 0)
            postVideoViewHolder?.play(playbackPosition)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(tToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = ""
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun showImagePost(postImageViewModel: PostImageViewModel, currentUserAvatar: String?) {
        supportActionBar?.title = getString(R.string.social_post_photo)

        iImage.visibility = View.VISIBLE
        postImageViewHolder = PostImageViewHolder(
            iImage,
            onDeleteListener,
            onEditListener,
            onLikeListener,
            onLikeCountListener,
            onCommentListener,
            onCommentCountListener,
            onShareListener
        ).apply { bind(postImageViewModel) }
        setupComment(postImageViewModel, currentUserAvatar)
    }

    override fun showVideoPost(postVideoViewModel: PostVideoViewModel, currentUserAvatar: String?) {
        if (postVideoViewModel is PostLiveViewModel) {
            supportActionBar?.title = getString(R.string.social_post_live_video)
            tvTitle.visibility = View.VISIBLE
            tvTitle.text = postVideoViewModel.title
        } else {
            supportActionBar?.title = getString(R.string.social_post_video)
        }

        iVideo.visibility = View.VISIBLE
        postVideoViewHolder = PostVideoViewHolder(
            iVideo,
            onVideoClickedListener,
            onAudioClickedListener,
            onDeleteListener,
            onEditListener,
            onLikeListener,
            onLikeCountListener,
            onCommentListener,
            onCommentCountListener,
            onShareListener
        ).apply {
            bind(postVideoViewModel)
            play()
        }
        setupComment(postVideoViewModel, currentUserAvatar)
    }

    private val onShareListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            postDetailsPresenter.onShareClicked()
        }
    }

    private val onCommentCountListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {}
    }

    private val onCommentListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            showCommentInput()
        }
    }

    private val onLikeListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            postDetailsPresenter.likePost(viewModel)
        }
    }

    private val onLikeCountListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@PostDetailsActivity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.POST_LIKE)
            }
            startActivity(intent)
        }
    }

    private val onDeleteListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onDeleteSelected()
        }
    }

    private val onEditListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onEditSelected(viewModel)
        }
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<PostVideoViewModel> {
        override fun onClick(viewModel: PostVideoViewModel) {
            onVideoClicked(viewModel.hlsUrl)
        }
    }

    private val onAudioClickedListener = object : ViewHolder.Listener<AudioToggledViewModel> {
        override fun onClick(viewModel: AudioToggledViewModel) {
            onAudioToggled(viewModel.audioEnabled)
        }
    }

    private fun onEditSelected(viewModel: PostViewModel) {
        postDetailsPresenter.onEditSelected(viewModel)
    }

    private fun onDeleteSelected() {
        socialDeleteDialog = SocialDeleteDialog(
            this,
            object : SocialDeleteDialog.Listener {
                override fun onNoClicked() {
                    hideDeleteDialog()
                }

                override fun onYesClicked() {
                    postDetailsPresenter.onDeleteClicked()
                }
            }
        )

        if (!isFinishing) socialDeleteDialog?.show()
    }

    override fun hideDeleteDialog() {
        socialDeleteDialog?.dismiss()
    }

    private fun onVideoClicked(url: String) {
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    private fun onAudioToggled(audioEnabled: Boolean) {
        postDetailsPresenter.onAudioToggled(audioEnabled)
    }

    private fun setupComment(viewModel: PostViewModel, currentUserAvatar: String?) {
        if (intent.getBooleanExtra(IS_COMMENTING, false)) {
            showCommentInput()
        }

        currentUserAvatar?.let {
            Glide.with(this)
                .load(it)
                .circleCrop()
                .crossFade()
                .into(ivCommenterAvatar)
        }

        ibSendComment.apply {
            isEnabled = false
            setOnClickListener {
                it.isEnabled = false
                postDetailsPresenter.commentPost(viewModel, etCommentText.text.toString())
            }
        }

        etCommentText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                setupSendImage(!s.isEmpty())
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun showCommentInput() {
        clCommentInput.visibility = View.VISIBLE
        etCommentText.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etCommentText, SHOW_IMPLICIT)
    }

    private fun setupSendImage(isEnabled: Boolean) {
        ibSendComment.isEnabled = isEnabled
        val drawableResId = if (isEnabled) R.drawable.ic_send_active else R.drawable.ic_send_inactive
        ibSendComment.setImageDrawable(getDrawable(drawableResId))
    }

    override fun showLoading() {
        nsvPost.visibility = View.GONE
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        nsvPost.visibility = View.VISIBLE
        pbLoading.visibility = View.GONE
    }

    override fun showEditMedia(postViewModel: PostViewModel) {
        val intent = Intent(this, SocialCreateMediaActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, postViewModel)
        startActivityForResult(intent, REQUEST_CODE_EDIT_MEDIA)
    }

    override fun showEditLive(postViewModel: PostLiveViewModel) {
        val intent = Intent(this, SocialCreateLiveActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, postViewModel)
        startActivityForResult(intent, REQUEST_CODE_EDIT_LIVE)
    }

    override fun showMessage(message: Int, onDismissListener: (() -> Unit)) {
        ViewUtils.showDialog(this, getString(message), DialogInterface.OnDismissListener { onDismissListener() })
    }

    private fun hideCommentBox() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etCommentText.windowToken, 0)
        etCommentText.text.clear()
        etCommentText.clearFocus()
        llInteractionsText.requestFocus()
    }

    private fun setupCommentText(commentCount: Int) {
        val viewHolder = postImageViewHolder ?: postVideoViewHolder
        viewHolder?.apply {
            val textView = itemView.findViewById<TextView>(R.id.tvCommentCount)
            when {
                commentCount > 1 -> {
                    textView.visibility = View.VISIBLE
                    textView.text = getString(R.string.social_comment_count_multiple, commentCount)
                }
                commentCount == 1 -> {
                    textView.visibility = View.VISIBLE
                    textView.text = getString(R.string.social_comment_count_one, commentCount)
                }
                else -> textView.visibility = View.GONE
            }
            commentsCount = commentCount
            itemView.findViewById<LinearLayout>(R.id.llInteractionsText).visibility =
                if (commentCount > 0 || likesCount > 0) View.VISIBLE else View.GONE
        }
    }

    private val onDeleteCommentListener = object : ViewHolder.Listener<CommentViewModel> {
        override fun onClick(viewModel: CommentViewModel) {
            commentsCount--
            setupCommentText(commentsCount)
            postDetailsPresenter.deleteComment(viewModel)
        }
    }

    private fun setupCommentList() {
        commentAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is CommentViewModel -> COMMENT_VIEW_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                COMMENT_VIEW_ID -> CommentViewHolder(view, onDeleteCommentListener)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvComments?.apply {
            adapter = commentAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(0)
            addItemDecoration(ListDividerItemDecoration(context))
            setHasFixedSize(false)
            isNestedScrollingEnabled = false
        }

        nsvPost.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { nsv, _, scrollY, _, _ ->
                if (scrollY == (nsv.getChildAt(0).measuredHeight - nsv.measuredHeight)) {
                    postDetailsPresenter.onScrolledBeyondVisibleThreshold()
                }
            }
        )

        srlComments.setOnRefreshListener {
            postDetailsPresenter.onRefresh()
            srlComments.isRefreshing = true
        }
    }

    override fun setComments(comments: List<CommentViewModel>) {
        commentAdapter?.apply {
            setItems(comments)
            addItems(listOf(DividerViewModel))
        }
    }

    override fun addComments(comments: List<CommentViewModel>) {
        commentAdapter?.apply {
            remove(DividerViewModel)
            addItems(comments)
            addItems(listOf(DividerViewModel))
        }
    }

    override fun insertNewComment(commentViewModel: CommentViewModel) {
        ibSendComment.isEnabled = true
        hideCommentBox()
        nsvPost.smoothScrollTo(0, rvComments.top)
        commentsCount++
        setupCommentText(commentsCount)
        commentAdapter?.addItem(commentViewModel, 0, true)
    }

    override fun updateComment(commentViewModel: CommentViewModel) {
        commentAdapter?.update(commentViewModel)
    }

    override fun removeComment(viewModel: CommentViewModel) {
        ibSendComment.isEnabled = true
        hideCommentBox()
        commentAdapter?.remove(viewModel, true)
    }

    override fun updateCommentCount(count: Int) {
        commentsCount = count
        runOnUiThread { setupCommentText(commentsCount) }
    }

    override fun showLoadingComments() {
        commentAdapter?.startLoading()
    }

    override fun hideLoadingComments() {
        commentAdapter?.stopLoading()
        srlComments.isRefreshing = false
    }

    override fun onBackPressed() {
        hideCommentBox()
        super.onBackPressed()
    }

    override fun onStop() {
        super.onStop()
        postVideoViewHolder?.stop()
    }
}
