package co.socialsquad.squad.presentation.feature.newlead.repository

import co.socialsquad.squad.domain.model.form.FormUpdate
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@ExperimentalCoroutinesApi
class NewLeadRepositoryImp(private val api: NewLeadAPI) : NewLeadRepository {

    override fun formUpdate(submissionId: Long, widgetId: Long, formUpdate: FormUpdate): Flow<String> {
        return flow {
            api.formUpdate(submissionId, widgetId, formUpdate)
            emit("success")
        }
    }
}
