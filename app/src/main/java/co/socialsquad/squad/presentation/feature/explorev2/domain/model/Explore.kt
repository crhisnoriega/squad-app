package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Explore(
    @SerializedName("url") val id: Int,
    @SerializedName("enable_map_view") val enableMapView: Boolean,
    @SerializedName("boundaries") val boundaries: Boundaries,
    @SerializedName("title") val title: String,
    @SerializedName("media") val media: Media,
    @SerializedName("contents") var contents: List<ExploreContent> = listOf(),
    @SerializedName("list") val list: ExploreList
) : Parcelable
