package co.socialsquad.squad.presentation.feature.company

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel

const val SELECTION_VIEW_MODEL_ID = R.layout.view_list_item_option

class SelectionItemViewModel(
    val title: String? = null,
    val imageUrl: String? = null,
    var selected: Boolean = false
) : ViewModel
