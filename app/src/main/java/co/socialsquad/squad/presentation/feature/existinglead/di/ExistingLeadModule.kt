package co.socialsquad.squad.presentation.feature.existinglead.di

import co.socialsquad.squad.presentation.feature.existinglead.ExistingLeadViewModel
import co.socialsquad.squad.presentation.feature.existinglead.repository.ExistingLeadAPI
import co.socialsquad.squad.presentation.feature.existinglead.repository.ExistingLeadRepository
import co.socialsquad.squad.presentation.feature.existinglead.repository.ExistingLeadRepositoryImp
import co.socialsquad.squad.presentation.feature.newlead.di.NewLeadModule
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object ExistingLeadModule {

    val repository = module {
        single {
            provideAPI(get())
        }

        single<ExistingLeadRepository> {
            ExistingLeadRepositoryImp(get())
        }
    }

    val instance = listOf(
        module {
            viewModel { ExistingLeadViewModel(get(), get()) }
        },
        repository, NewLeadModule.repository
    )

    private fun provideAPI(retrofit: Retrofit): ExistingLeadAPI = retrofit.create(ExistingLeadAPI::class.java)
}
