package co.socialsquad.squad.presentation.feature.login.password

import dagger.Binds
import dagger.Module

@Module
abstract class LoginPasswordModule {

    @Binds
    abstract fun view(view: LoginPasswordActivity): LoginPasswordView
}
