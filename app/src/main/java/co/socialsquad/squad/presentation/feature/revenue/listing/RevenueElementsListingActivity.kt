package co.socialsquad.squad.presentation.feature.revenue.listing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import co.socialsquad.squad.presentation.feature.points.viewHolder.*
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.ranking.viewholder.*
import co.socialsquad.squad.presentation.feature.revenue.ObjectRevenueSummaryActivity
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueLeadViewHolder
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueLeadViewModel
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourceDetailViewHolder
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourceDetailViewModel
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.activity_elements_listing.*
import kotlinx.coroutines.launch
import org.koin.core.module.Module


enum class REVENUE_LIST_TYPE(val title: String) {
    CAMPAINS("Campanhas"), LEADS("Leads")
}

class RevenueElementsListingActivity : BaseActivity() {

    companion object {

        var doRefresh: Boolean = false

        const val ELEMENT_TYPE = "element_type"
        const val ELEMENT_TITLE = "element_title"
        const val ITENS_SHIMMER = "ITEMS_SHIMMER"

        fun start(
            context: Context,
            elementType: REVENUE_LIST_TYPE,
            itemsShimmer: Int = 0,
            titleAPI: String
        ) {
            context.startActivity(
                Intent(
                    context,
                    RevenueElementsListingActivity::class.java
                ).apply {
                    putExtra(ELEMENT_TYPE, elementType)
                    putExtra(ELEMENT_TITLE, titleAPI)
                    putExtra(ITENS_SHIMMER, itemsShimmer)
                })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_revenue_elements_listing

    private val elementType: REVENUE_LIST_TYPE by extra(ELEMENT_TYPE)
    private val title: String by extra(ELEMENT_TITLE)
    private val itemsShimmer: Int by extra(ITENS_SHIMMER)

    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

//        itemsShimmer?.let {
//            if (it > 0) configureAsLoading(it)
//        }

        when (elementType) {
            // REVENUE_LIST_TYPE.CAMPAINS -> viewModel.fetchSystemPoints()
        }

        configureElementList()
    }


    override fun onResume() {
        Log.i("list", "onResume: $doRefresh")
        super.onResume()
        if (doRefresh) {
            doRefresh = false

            factoryAdapter.items.clear()

            ProfileFragment.doRefresh = true
            when (elementType) {
//                REVENUE_LIST_TYPE.CAMPAINS -> viewModel.fetchSystemPoints()
            }
        }
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = title
    }

    private val handler = Handler()
    private var endPage = false

    private fun configureObservers() {
        lifecycleScope.launch {
//            val value = viewModel.systemsPoints
//            value.collect {
//
//                when (it.status) {
//                    Status.SUCCESS -> {
//                        configureElementList()
//                    }
//                    Status.ERROR -> {
//                    }
//                }
//            }
        }
    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                Shimmer.AlphaHighlightBuilder()
                    .setBaseAlpha(1f)
                    .setIntensity(0f)
                    .build()
            )
            it.stopShimmer()
            it.clearAnimation()
        }
    }

    val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel): Int {
            return when (viewModel) {
                is ItemHeaderListingViewModel -> R.layout.item_listing_header

                is ItemRevenueSourceDetailViewModel -> R.layout.item_revenue_source_listing

                is ItemRevenueLeadViewModel -> R.layout.item_revenue_lead_status

                is ItemShowAllRankingViewModel -> R.layout.ranking_separator

                else -> throw IllegalArgumentException()
            }
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            R.layout.item_listing_header -> ItemHeaderPointsViewHolder(view)

            R.layout.item_revenue_source_listing -> ItemRevenueSourceDetailViewHolder(view) {
                var intent = ObjectRevenueSummaryActivity.newInstance(
                    this@RevenueElementsListingActivity,
                    Link(1, LinkType.Object, "/explore/object/164")
                )
                startActivity(intent)
            }

            R.layout.item_revenue_lead_status -> ItemRevenueLeadViewHolder(view) {

            }

            R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
            }
            else -> throw IllegalArgumentException()
        }
    })

    private val _layoutManager =
        LinearLayoutManager(
            this@RevenueElementsListingActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

    private fun configureElementList() {
        stopShimmer(listShimmer)

        rvElements.apply {
            layoutManager = _layoutManager

            if (page == 1) {
                adapter = factoryAdapter
            }

            var elements = mutableListOf<ViewModel>()


            if (page == 1) {
                elements.add(ItemShowAllRankingViewModel("", ""))
            }


            when (elementType) {

                REVENUE_LIST_TYPE.CAMPAINS -> {
                    elements.add(ItemRevenueSourceDetailViewModel(true))
                    elements.add(ItemRevenueSourceDetailViewModel(false))
                    elements.add(ItemRevenueSourceDetailViewModel(false))
                }


                REVENUE_LIST_TYPE.LEADS -> {
                    elements.add(ItemRevenueLeadViewModel(false))
                    elements.add(ItemRevenueLeadViewModel(false))
                    elements.add(ItemRevenueLeadViewModel(false))
                    elements.add(ItemRevenueLeadViewModel(true))
                }

                else -> mutableListOf<ViewModel>()
            }





            factoryAdapter.addItems(elements)

            addOnScrollListener(EndlessScrollListener(20, _layoutManager, {
                when (elementType) {


                }
            }))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}