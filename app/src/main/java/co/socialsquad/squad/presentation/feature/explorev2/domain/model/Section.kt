package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Section(
        @SerializedName("id") val id: Int,
        @SerializedName("title") val title: String,
        @SerializedName("icon") val icon: SimpleMedia,
        @SerializedName("template") @JsonAdapter(SectionTemplateTypeAdapter::class) val template: SectionTemplate,
        @SerializedName("empty_state") val emptyState: EmptyState,
        @SerializedName("loading_state") val sectionLoadingState: List<SectionLoadingState>,
        @SerializedName("items") val items: List<ExploreObject>? = listOf()
) : Parcelable
