package co.socialsquad.squad.presentation.feature.existinglead.adapter.result.lead

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.LeadResultsAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ResultsViewHolder
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.inflate
import com.bumptech.glide.Glide

class ExistingLeadViewHolder(val parent: ViewGroup, private val colorPrimary: String?, private val onCheckedChangeListener: LeadResultsAdapter.CheckedListener) :
    ResultsViewHolder(parent.inflate(R.layout.item_lead_result)) {

    private var tvName: TextView = view.findViewById(R.id.short_title_lead)
    private var tvSubTitle: TextView = view.findViewById(R.id.short_description_lead)
    private var tvContactText: TextView = view.findViewById(R.id.tvContactText)
    private var rlContactCircle: RelativeLayout = view.findViewById(R.id.rlContactCircle)
    private var imgExistingLead: ImageView = view.findViewById(R.id.imgSection2)
    private var rbExistingLead: RadioButton = view.findViewById(R.id.rbExistingLead)

    override fun bind(item: ExistingLeadVM) {

        view.setOnClickListener {
            onCheckedChangeListener.onChecked(true, item)
        }

        colorPrimary?.let {
            when (val background: Drawable = rlContactCircle.background) {
                is ShapeDrawable -> {
                    background.paint.color = ColorUtils.parse(it)
                }
                is GradientDrawable -> {
                    background.setColor(ColorUtils.parse(it))
                }
                is ColorDrawable -> {
                    background.color = ColorUtils.parse(it)
                }
            }
        }

        item.existingLead?.let { existingLead ->
            tvName.text = existingLead.title

            existingLead.avatar.picture?.let {

                imgExistingLead.isVisible = true
                rlContactCircle.isVisible = false

                Glide.with(view.context)
                    .load(it)
                    .crossFade()
                    .into(imgExistingLead)
            } ?: run {

                imgExistingLead.isVisible = false
                rlContactCircle.isVisible = true

                tvContactText.text = existingLead.avatar.initials
            }

            existingLead.subtitle?.let {
                tvSubTitle.text = it
            } ?: run {
                tvSubTitle.text = ""
            }
        }

        rbExistingLead.setOnCheckedChangeListener(null)

        rbExistingLead.isChecked = item.checked

        rbExistingLead.setOnCheckedChangeListener { _, checked ->
            onCheckedChangeListener.onChecked(checked, item)
        }
    }
}
