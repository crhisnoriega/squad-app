package co.socialsquad.squad.presentation.feature.users

import android.content.Intent
import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.BusinessType
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import javax.inject.Inject

class UserListPresenter @Inject constructor(
    private val userListView: UserListView,
    private val userInteractor: UserInteractor,
    private val tagWorker: TagWorker,
    val loginRepository: LoginRepository
) {

    companion object {
        const val SEEN_USERS_TAB_POSITION = 0
        const val NOT_SEEN_USERS_TAB_POSITION = 1

        const val TYPE_SEEN = "seen"
        const val TYPE_NOT_SEEN = "not-seen"
    }

    private val businessType = loginRepository.userCompany?.company?.businessType

    private val compositeDisposable = CompositeDisposable()

    enum class Type : Serializable { CONFIRMED, POST_LIKE, AUDIO_LIKE, AUDIO_PLAYBACK, VIP, SPEAKERS, FEED_VISUALIZATION, EVENTS_CONFIRMED, DOCUMENTS_VISUALIZATION, LIVE_VISUALIZATION }

    private var type: Type? = null
    private var pk: Int? = null
    private var currentPage: Int = 1
    private var isTabNumberSet = false
    private var isCompleted = false
    private var isLoading = false
    private var currentTab: Int = 0

    private var seenString: Int? = null
    private var notSeenString: Int? = null

    private val observable
        get() = pk?.let {
            val segment = if (currentTab == NOT_SEEN_USERS_TAB_POSITION) TYPE_NOT_SEEN else TYPE_SEEN
            when (type) {
                Type.CONFIRMED, Type.LIVE_VISUALIZATION -> {
                    userInteractor.getAttendingUsers(it, currentPage, segment)
                }
                Type.POST_LIKE -> {
                    userInteractor.getLikedUsers(it, currentPage)
                }
                Type.AUDIO_LIKE -> {
                    userInteractor.getAudioLikedUsers(it, currentPage)
                }
                Type.AUDIO_PLAYBACK -> {
                    userInteractor.getAudioPlaybackDownlineUsers(it, currentPage, segment)
                }
                Type.EVENTS_CONFIRMED -> {
                    userInteractor.getAttendingEventUsers(it, currentPage, segment)
                }
                Type.DOCUMENTS_VISUALIZATION -> {
                    userInteractor.getDocumentViewedUsers(it, currentPage, segment)
                }
                Type.FEED_VISUALIZATION -> {
                    userInteractor.getFeedVisualizationUsers(it, currentPage, segment)
                }
                else -> throw IllegalArgumentException()
            }
        }

    private val tabsObservableComplete
        get() = pk?.let { pk ->
            val complementaryTab = when (currentTab) {
                SEEN_USERS_TAB_POSITION -> TYPE_NOT_SEEN
                NOT_SEEN_USERS_TAB_POSITION -> TYPE_SEEN
                else -> null
            }
            complementaryTab?.let {
                when (type) {
                    Type.CONFIRMED, Type.LIVE_VISUALIZATION -> {
                        userInteractor.getAttendingUsersCount(pk, it)
                    }
                    Type.AUDIO_PLAYBACK -> {
                        userInteractor.getAudioPlaybackUsersCount(pk, it)
                    }
                    Type.EVENTS_CONFIRMED -> {
                        userInteractor.getAttendingEventUsersCount(pk, it)
                    }
                    Type.DOCUMENTS_VISUALIZATION -> {
                        userInteractor.getDocumentViewedUsersCount(pk, it)
                    }
                    Type.FEED_VISUALIZATION -> {
                        userInteractor.getFeedVisualizationUsersCount(pk, it)
                    }
                    else -> Observable.empty()
                }
            }
        }

    fun onCreate(intent: Intent) {
        pk = intent.getIntExtra(UserListActivity.QUERY_ITEM_PK, 0)
        type = intent.getSerializableExtra(UserListActivity.USER_LIST_TYPE) as? Type
        val subtitleStringId = if (businessType?.toLowerCase() == BusinessType.mlm.name) R.string.members_of_downline_users else R.string.members_of_team_users
        when (type) {
            Type.CONFIRMED -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_CONFIRMED_ATTENDANCE_LIST)
                userListView.setupToolbar(R.string.confirmed_users_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_presence_confirmed
                notSeenString = R.string.user_list_presence_not_confirmed
            }
            Type.LIVE_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_LIVE_VIEWS_LIST)
                userListView.setupToolbar(R.string.user_list_visualizations_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_visualized
                notSeenString = R.string.user_list_not_visualized
            }
            Type.POST_LIKE -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_POST_LIKE_LIST)
                userListView.setupToolbar(R.string.likes_title)
                getUsers(currentPage)
            }
            Type.AUDIO_LIKE -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_DROP_LIKE_LIST)
                userListView.setupToolbar(R.string.likes_title)
                getUsers(currentPage)
            }
            Type.AUDIO_PLAYBACK -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_DROP_PLAYBACK_LIST)
                userListView.setupToolbar(R.string.audio_playbacks_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_listened
                notSeenString = R.string.user_list_not_listened
            }
            Type.SPEAKERS -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_SPEAKER_LIST)
                userListView.setupToolbar(R.string.event_detail_speakers)
                val array = intent.getSerializableExtra(UserListActivity.EXTRA_USER_LIST) as Array<UserViewModel>
                userListView.setItems(array.toList())
            }
            Type.VIP -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_VIP_LIST)
                userListView.setupToolbar(R.string.event_detail_vip)
                val array = intent.getSerializableExtra(UserListActivity.EXTRA_USER_LIST) as Array<UserViewModel>
                userListView.setItems(array.toList())
            }
            Type.EVENTS_CONFIRMED -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_EVENT_CONFIRMED_LIST)
                userListView.setupToolbar(R.string.confirmed_users_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_presence_confirmed
                notSeenString = R.string.user_list_presence_not_confirmed
            }
            Type.DOCUMENTS_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_DOCUMENT_VIEWER_LIST)
                userListView.setupToolbar(R.string.user_list_visualizations_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_visualized
                notSeenString = R.string.user_list_not_visualized
            }
            Type.FEED_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_FEED_VISUALIZATION_LIST)
                userListView.setupToolbar(R.string.user_list_visualizations_title, subtitleStringId)
                userListView.showTabLayout()
                getUsers(currentPage)
                seenString = R.string.user_list_visualized
                notSeenString = R.string.user_list_not_visualized
            }
            else -> throw IllegalArgumentException()
        }
        seenString?.let { seen -> notSeenString?.let { userListView.setupTabs(seen, it, loginRepository.squadColor) } }
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isCompleted) when (type) {
            Type.CONFIRMED, Type.POST_LIKE, Type.AUDIO_LIKE, Type.AUDIO_PLAYBACK, Type.DOCUMENTS_VISUALIZATION, Type.EVENTS_CONFIRMED, Type.FEED_VISUALIZATION -> {
                val pageParams = Bundle()
                pageParams.putInt("page", currentPage)
                tagWorker.tagEvent(TagWorker.TAG_REQUEST_DROP_PLAYBACK_LIST, pageParams)
                getUsers(currentPage)
            }
            else -> {
            }
        }
    }

    fun getUsers(page: Int) {
        observable?.let { observable ->
            compositeDisposable.add(
                observable
                    .doOnSubscribe {
                        isLoading = true
                        userListView.startLoading()
                    }.doOnTerminate {
                        userListView.stopLoading()
                        userListView.stopRefreshing()
                        isLoading = false
                    }
                    .doOnNext { isCompleted = it.completed }
                    .doOnNext { userList ->
                        if (isTabNumberSet) return@doOnNext
                        isTabNumberSet = true
                        when (currentTab) {
                            SEEN_USERS_TAB_POSITION -> {
                                tabsObservableComplete?.let { observable ->
                                    compositeDisposable.add(
                                        observable.subscribe(
                                            {
                                                setAllTabCount(userList.cont)
                                                setDownlineTabCount(it)
                                            },
                                            {}
                                        )
                                    )
                                }
                            }
                            NOT_SEEN_USERS_TAB_POSITION -> {
                                tabsObservableComplete?.let { observable ->
                                    compositeDisposable.add(
                                        observable.subscribe(
                                            {
                                                setAllTabCount(it)
                                                setDownlineTabCount(userList.cont)
                                            },
                                            {}
                                        )
                                    )
                                }
                            }
                        }
                    }
                    .map { it.users }
                    .subscribe(
                        {
                            if (page == 1) {
                                userListView.setItems(it)
                            } else {
                                userListView.addItems(it)
                            }
                            currentPage++
                        },
                        {
                            it.printStackTrace()
                            userListView.showMessage(R.string.audio_playback_list_error, null)
                            isCompleted = true
                        }
                    )
            )
        }
    }

    private fun getAllUsersList() {
        when (type) {
            Type.CONFIRMED, Type.LIVE_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_ATTENDING_LIVE_SEEN_TAB)
            }
            Type.AUDIO_PLAYBACK -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_DROP_PLAYBACKS_SEEN_TAB)
            }
            Type.EVENTS_CONFIRMED -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_EVENT_CONFIRMED_SEEN_TAB)
            }
            Type.DOCUMENTS_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_DOCUMENT_VIEWER_SEEN_TAB)
            }
            Type.FEED_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_FEED_VISUALIZATION_SEEN_TAB)
            }
            else -> tagWorker.tagEvent(TagWorker.TAG_TAP_UNKNOW_SEEN_TAB)
        }
        getUsers(1)
    }

    private fun getDownlineUsersList() {
        when (type) {
            Type.CONFIRMED, Type.LIVE_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_ATTENDING_LIVE_NOT_SEEN_TAB)
            }
            Type.AUDIO_PLAYBACK -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_DROP_PLAYBACKS_NOT_SEEN_TAB)
            }
            Type.EVENTS_CONFIRMED -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_EVENT_CONFIRMED_NOT_SEEN_TAB)
            }
            Type.DOCUMENTS_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_DOCUMENT_VIEWER_NOT_SEEN_TAB)
            }
            Type.FEED_VISUALIZATION -> {
                tagWorker.tagEvent(TagWorker.TAG_TAP_FEED_VISUALIZATION_NOT_SEEN_TAB)
            }
            else -> tagWorker.tagEvent(TagWorker.TAG_TAP_UNKNOW_NOT_SEEN_TAB)
        }

        getUsers(1)
    }

    fun onTabSelected(position: Int) {
        currentTab = position
        onRefresh()
    }

    fun onRefresh() {
        isCompleted = false
        isTabNumberSet = false
        when (type) {
            Type.CONFIRMED, Type.POST_LIKE, Type.AUDIO_LIKE, Type.AUDIO_PLAYBACK, Type.EVENTS_CONFIRMED, Type.DOCUMENTS_VISUALIZATION, Type.FEED_VISUALIZATION, Type.LIVE_VISUALIZATION -> {
                compositeDisposable.clear()
                currentPage = 1
                when (currentTab) {
                    SEEN_USERS_TAB_POSITION -> getAllUsersList()
                    NOT_SEEN_USERS_TAB_POSITION -> getDownlineUsersList()
                    else -> throw IllegalArgumentException()
                }
            }
            else -> {
            }
        }
    }

    private fun setAllTabCount(count: Int) {
        seenString?.let { userListView.setTotalSeen(it, count) }
    }

    private fun setDownlineTabCount(count: Int) {
        notSeenString?.let { userListView.setTotalNotSeen(it, count) }
    }
}
