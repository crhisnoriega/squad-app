package co.socialsquad.squad.presentation.feature.login.alternative.success

import dagger.Module

@Module
interface LoginAlternativeSuccessModule {
    fun view(view: LoginAlternativeSuccessActivity)
}
