package co.socialsquad.squad.presentation.feature.opportunity.opportunity_list

import co.socialsquad.squad.data.entity.opportunity.Information
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails

/**
 * Every object detail view that should have the opportunities list, should implement this
 * interface
 */
interface OpportunityListView {
    fun onOpportunityDetailsClick(submissionDetails: SubmissionDetails)
    fun onOpportunityInfoClick(info: Information)
}
