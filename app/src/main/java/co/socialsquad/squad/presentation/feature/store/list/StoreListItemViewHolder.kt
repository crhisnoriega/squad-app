package co.socialsquad.squad.presentation.feature.store.list

import android.text.Spannable
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.intentDial
import co.socialsquad.squad.presentation.util.intentMaps
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_list_item.view.*

class StoreListItemViewHolder(
    itemView: View,
    private val onCallClickedListener: ViewHolder.Listener<StoreViewModel>
) : ViewHolder<StoreViewModel>(itemView) {
    override fun bind(viewModel: StoreViewModel) {
        itemView.apply {
            Glide.with(context)
                .load(viewModel.image)
                .roundedCorners(context)
                .crossFade()
                .into(ivImage)

            short_title_lead.text = viewModel.name

            tvAddress.text = viewModel.address

            val directionsText = itemView.context.getString(R.string.stores_list_directions)
            val spannable = Spannable.Factory.getInstance().newSpannable(itemView.context.getString(R.string.stores_list_distance, viewModel.distance, directionsText))
            spannable.setSpan(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        itemView.context.intentMaps(viewModel.latitude, viewModel.longitude, viewModel.name)
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        ds.linkColor = ContextCompat.getColor(itemView.context, R.color.orange)
                        super.updateDrawState(ds)
                        ds.isUnderlineText = false
                    }
                },
                spannable.length - directionsText.length, spannable.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE
            )
            tvDistance.text = spannable
            tvDistance.movementMethod = LinkMovementMethod.getInstance()
            ibCall.visibility = if (viewModel.phone.isNullOrEmpty()) View.GONE else View.VISIBLE
            ibCall.setOnClickListener {
                onCallClickedListener.onClick(viewModel)
                viewModel.phone?.let {
                    context.intentDial(it)
                }
            }
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivImage)
    }
}
