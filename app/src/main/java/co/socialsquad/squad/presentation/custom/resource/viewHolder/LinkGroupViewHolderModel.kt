package co.socialsquad.squad.presentation.custom.resource.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkGroup
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkV2

data class LinkGroupViewHolderModel(val linkGroup: LinkGroup) : ViewModel