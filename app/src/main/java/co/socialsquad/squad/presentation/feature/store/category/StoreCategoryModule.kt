package co.socialsquad.squad.presentation.feature.store.category

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class StoreCategoryModule {
    @Binds
    abstract fun view(storeCategoryActivity: StoreCategoryActivity): StoreCategoryView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
