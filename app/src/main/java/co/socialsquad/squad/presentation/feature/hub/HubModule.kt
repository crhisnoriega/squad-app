package co.socialsquad.squad.presentation.feature.hub

import co.socialsquad.squad.presentation.feature.hub.dashboard.DashboardFragment
import co.socialsquad.squad.presentation.feature.hub.dashboard.DashboardModule
import co.socialsquad.squad.presentation.feature.hub.intranet.IntranetFragment
import co.socialsquad.squad.presentation.feature.hub.intranet.IntranetModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface HubModule {
    @ContributesAndroidInjector(modules = [DashboardModule::class])
    fun dashboardFragment(): DashboardFragment

    @ContributesAndroidInjector(modules = [IntranetModule::class])
    fun intranetFragment(): IntranetFragment

    @Binds
    fun view(view: HubFragment): HubView
}
