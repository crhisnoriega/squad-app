package co.socialsquad.squad.presentation.custom

import android.content.DialogInterface
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.argument
import co.socialsquad.squad.presentation.util.isVisible
import co.socialsquad.squad.presentation.util.setTextColorFromRes
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.custom_dialog.*

class CustomDialog : DialogFragment() {

    private val config by argument<CustomDialogConfig>(ARG_CONFIG)
    var listener = CustomDialogListener()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.custom_dialog,
        container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // adding radius on dialog
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.let { window ->
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            window.decorView.setBackgroundResource(android.R.color.transparent)
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window.attributes.windowAnimations = R.style.DialogAnimationFade
        }
        setupViews()
        setupListeners()
        dialog?.setCanceledOnTouchOutside(config.dismissOnClickOutside)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        listener.onDismiss()
    }

    private fun setupViews() {
        with(ivDialogIcon) {
            isVisible = config.iconRes != null
            config.iconRes?.let { this.setImageResource(it) }
        }

        tvTitle.setText(config.titleRes)
        tvDescription.setText(config.descriptionRes)

        with(tvButtonOne) {
            divider1.isVisible = config.buttonOneConfig.showButton
            isVisible = config.buttonOneConfig.showButton
            if (config.buttonOneConfig.buttonTextRes != -1) {
                setText(config.buttonOneConfig.buttonTextRes)
            }
            setTextColorFromRes(context, config.buttonOneConfig.buttonTextColor)
        }

        with(tvButtonTwo) {
            divider2.isVisible = config.buttonTwoConfig.showButton
            isVisible = config.buttonTwoConfig.showButton
            if (config.buttonTwoConfig.buttonTextRes != -1) {
                setText(config.buttonTwoConfig.buttonTextRes)
            }
            setTextColorFromRes(context, config.buttonTwoConfig.buttonTextColor)
        }

        with(tvButtonThree) {
            divider3.isVisible = config.buttonThreeConfig.showButton
            isVisible = config.buttonThreeConfig.showButton
            if (config.buttonThreeConfig.buttonTextRes != -1) {
                setText(config.buttonThreeConfig.buttonTextRes)
            }
            setTextColorFromRes(context, config.buttonThreeConfig.buttonTextColor)
        }
    }

    private fun setupListeners() {
        tvButtonOne.setOnClickListener { listener.buttonOneClick() }
        tvButtonTwo.setOnClickListener { listener.buttonTwoClick() }
        tvButtonThree.setOnClickListener { listener.buttonThreeClick() }
    }

    companion object {
        private const val ARG_CONFIG = "config"
        fun newInstance(config: CustomDialogConfig): CustomDialog {
            return CustomDialog().apply {
                val args = Bundle()
                args.putParcelable(ARG_CONFIG, config)
                this.arguments = args
            }
        }
    }
}

class CustomDialogListener(
    val buttonOneClick: () -> Unit = {},
    val buttonTwoClick: () -> Unit = {},
    val buttonThreeClick: () -> Unit = {},
    val onDismiss: () -> Unit = {}
)

@Parcelize
data class CustomDialogButtonConfig(
    val showButton: Boolean = false,
    val buttonTextRes: Int = -1,
    val buttonTextColor: Int = R.color.black
) : Parcelable

@Parcelize
data class CustomDialogConfig(
    val titleRes: Int,
    val descriptionRes: Int,
    val iconRes: Int? = null,
    val buttonOneConfig: CustomDialogButtonConfig = CustomDialogButtonConfig(),
    val buttonTwoConfig: CustomDialogButtonConfig = CustomDialogButtonConfig(),
    val buttonThreeConfig: CustomDialogButtonConfig = CustomDialogButtonConfig(),
    val dismissOnClickOutside: Boolean = true
) : Parcelable
