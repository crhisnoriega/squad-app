package co.socialsquad.squad.presentation.feature.social.create.segment.members

import co.socialsquad.squad.presentation.feature.social.UserViewModel

interface SegmentGroupMembersView {
    fun startLoading()
    fun stopLoading()
    fun addMembers(members: List<UserViewModel>)
    fun hideEmptyState()
    fun showEmptyState(text: String)
    fun setAddButtonEnabled(enabled: Boolean)
    fun finishActivity(members: List<UserViewModel>)
    fun setMembers(members: List<UserViewModel>)
}
