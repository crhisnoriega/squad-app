package co.socialsquad.squad.presentation.feature.settings

import dagger.Binds
import dagger.Module

@Module
abstract class SettingsModule {
    @Binds
    abstract fun view(settingsActivity: SettingsActivity): SettingsView
}
