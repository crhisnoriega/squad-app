package co.socialsquad.squad.presentation.feature.immobiles

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Immobile
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.feature.immobiles.details.ImmobilesDetailsActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_immobiles.*
import javax.inject.Inject

class ImmobilesFragment : Fragment(), ImmobilesView {

    @Inject
    lateinit var presenter: ImmobilesPresenter

    private lateinit var immobilesAdapter: ImmobilesAdapter
    private var primaryColor = ""

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_immobiles, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.apply { immobilesAdapter = ImmobilesAdapter(this@ImmobilesFragment::onImmobileClicked, this) }
        presenter.immobiles()
        setupRecyclerView()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    private fun setupRecyclerView() {
        immobilesList.run {
            activity?.apply {
                layoutManager = LinearLayoutManager(this)
                adapter = immobilesAdapter

                addOnScrollListener(
                    EndlessScrollListener(
                        2,
                        layoutManager as LinearLayoutManager,
                        presenter::immobiles
                    )
                )
            }
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun setupImmobiles(immobiles: List<Immobile>, secondaryColor: String?, squadColor: String?) {
        squadColor?.apply { primaryColor = this }
        immobilesAdapter.immobiles(immobiles, secondaryColor)
    }

    private fun onImmobileClicked(immobile: String) {
        activity?.apply {
            val intent = Intent(this, ImmobilesDetailsActivity::class.java)
            intent.putExtra(ImmobilesDetailsActivity.extraImmobile, immobile)
            intent.putExtra(ImmobilesDetailsActivity.extraPrimaryColor, primaryColor)
            //startActivity(intent)
        }
    }
}
