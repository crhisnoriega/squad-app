package co.socialsquad.squad.presentation.feature.tool.timetable

import android.util.Log
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class OpportunityTimetableViewModel(
        private val repository: ToolsRepository,
        private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseViewModel() {

    private var nextPage: String? = null

    @ExperimentalCoroutinesApi
    val opportunity: StateFlow<Resource<Opportunity<Timetable>>>
        get() = _opportunity

    @ExperimentalCoroutinesApi
    private val _opportunity = MutableStateFlow<Resource<Opportunity<Timetable>>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    fun fetchOpportunities(link: String) {
        viewModelScope.launch {
            _opportunity.value = Resource.loading(null)
            repository.fetchOpportunitiesTimetable("/endpoints$link")
                    .flowOn(dispatcher)
                    .catch { e ->
                        _opportunity.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        it?.let { result ->
                            if (result.items.isNullOrEmpty()) {
                                _opportunity.value = Resource.empty()
                            } else {
                                nextPage =
                                        if (result.next.isNullOrBlank()) null else "/endpoints${result.next}"
                                _opportunity.value = Resource.success(it)
                            }
                        }
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun getNextPage() {
        Log.i("tag13", "nextPage: " + nextPage)
        nextPage?.let { page ->
            viewModelScope.launch {
                fetchOpportunities(page)
            }
        }
    }
}
