package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue


@Parcelize
data class CriteriasResponse(
        @SerializedName("next") val next: String?,
        @SerializedName("previous") val previous: String?,
        @SerializedName("count") val count: Int?,
        @SerializedName("error") var error: @RawValue Any? = null,
        @SerializedName("results") val results: List<CriteriaData>?
) : Parcelable