package co.socialsquad.squad.presentation.feature.navigation

import android.text.format.DateUtils
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.FeedbackRepository
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_DASHBOARD
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_EXPLORE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_NAVIGATION
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_NOTIFICATIONS
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_PROFILE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_SOCIAL
import co.socialsquad.squad.presentation.feature.feedback.FeedbackViewModel
import io.reactivex.disposables.CompositeDisposable
import java.util.Calendar
import java.util.Timer
import java.util.TimerTask
import javax.inject.Inject

private const val TIME_TO_CHECK_NOTIFICATIONS = DateUtils.MINUTE_IN_MILLIS
private const val TIME_TO_SHOW_INTERACTION_DIALOG = DateUtils.DAY_IN_MILLIS

class NavigationPresenter @Inject constructor(
    private val navigationView: NavigationView,
    private val liveRepository: LiveRepository,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository,
    private val feedbackRepository: FeedbackRepository,
    private val preferences: Preferences,
    private val tagWorker: TagWorker
) {
    private val timer = Timer()
    private val compositeDisposable = CompositeDisposable()
    private var hasNewNotifications = false

    fun onCreate() {
        checkActiveLive()
        checkFeedback()
        openSharedItem()
        scheduleTimerForNotificationsUpdates()
        if (BuildConfig.DEBUG) {
            loginRepository.userCompany?.user?.chatId?.let {
                navigationView.startChatService(it)
            }
        }
        showHubIfEnabled()
    }

    private fun showHubIfEnabled() {
        val isDashboardEnabled = loginRepository.userCompany?.company?.featureDashboard ?: false
        val isIntranetEnabled = loginRepository.userCompany?.company?.featureIntranet ?: false

        if (isDashboardEnabled || isIntranetEnabled) {
            loginRepository.userCompany?.company?.webView?.let {
                navigationView.setupHub(it.notEnabledAvatar, it.enabledAvatar)
            }
        }
    }

    private fun checkFeedback() {
        compositeDisposable.add(
            feedbackRepository.getFeedback()
                .subscribe(
                    { if (it.pk != null) navigationView.openFeedback(FeedbackViewModel(it)) },
                    { it.printStackTrace() }
                )
        )
    }

    fun onRestart() {
        updateNotificationBadge()
    }

    fun onResume() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_NAVIGATION)
    }

    fun onNotificationSynced() {
        updateNotificationBadge()
    }

    fun onFragmentSet(navigationItem: NavigationItem) {
        when (navigationItem) {
            NavigationItem.SOCIAL -> tagWorker.tagEvent(TAG_SCREEN_VIEW_SOCIAL)
            NavigationItem.EXPLORE -> tagWorker.tagEvent(TAG_SCREEN_VIEW_EXPLORE)
            NavigationItem.NOTIFICATION -> tagWorker.tagEvent(TAG_SCREEN_VIEW_NOTIFICATIONS)
            NavigationItem.PROFILE -> tagWorker.tagEvent(TAG_SCREEN_VIEW_PROFILE)
            NavigationItem.HUB -> tagWorker.tagEvent(TAG_SCREEN_VIEW_DASHBOARD)
        }
    }

    fun onUnreadNotificationsCheck() = hasNewNotifications

    fun onDestroy() {
        timer.cancel()
        compositeDisposable.dispose()
    }

    private fun scheduleTimerForNotificationsUpdates() {
        val notificationsCheckTask = object : TimerTask() {
            override fun run() {
                updateNotificationBadge()
            }
        }

        timer.schedule(notificationsCheckTask, 0L, TIME_TO_CHECK_NOTIFICATIONS)
    }

    private fun checkActiveLive() {
        liveRepository.livePk?.let {
            compositeDisposable.add(
                liveRepository.getLive(it)
                    .subscribe {
                        if (it.active) with(it) {
                            navigationView.openLiveCreator(
                                pk, rtmpUrl,
                                commentQueue
                                    ?: "",
                                startedAt
                            )
                        } else {
                            liveRepository.livePk = null
                        }
                    }
            )
        }
    }

    private fun openSharedItem() {
        if (preferences.shareModel != null && preferences.sharePK > 0) {
            navigationView.openSharedActivity(preferences.shareModel!!, preferences.sharePK)
            preferences.shareModel = null
            preferences.sharePK = -1
        }
    }

    private fun updateNotificationBadge() {
        compositeDisposable.add(
            socialRepository.getUnreadItems()
                .subscribe(
                    {
                        if (it.notificationsUnreadCount > 0) {
                            hasNewNotifications = true
                            navigationView.showNotificationBadge(loginRepository.squadColor)
                        } else {
                            hasNewNotifications = false
                            navigationView.hideNotificationBadge()
                        }
                    },
                    { it.printStackTrace() }
                )
        )
    }

    private fun aWholeDayPassed(): Boolean {
        val rightNow: Long = Calendar.getInstance().timeInMillis
        return preferences.interactionDialogTimeInMillis + TIME_TO_SHOW_INTERACTION_DIALOG < rightNow
    }
}
