package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemShowAllRankingTopBottomViewModel(
        val title: String,
        val type: String)
    : ViewModel {
}