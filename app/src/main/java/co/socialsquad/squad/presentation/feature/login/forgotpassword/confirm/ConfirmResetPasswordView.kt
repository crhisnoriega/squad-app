package co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm

interface ConfirmResetPasswordView {

    fun showLoading()
    fun hideLoading()
    fun showSuccessDialog()
    fun openProfileScreen()
    fun openNavigation()
    fun showMessage(resId: Int)
}
