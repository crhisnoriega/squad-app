package co.socialsquad.squad.presentation.feature.terms

import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.TermsResponse

interface TermsView {

    fun setUpdatedAt(day: String, month: Int?, year: String)
    fun setTerms(terms: TermsResponse.Result)
    fun showLoading()
    fun hideLoading()
    fun showMessage(resId: Int)
    fun moveToKickoff()
    fun setupHeader(company: Company?)
    fun showDialogError(subject: Int, message: Int, dialogDescription: Int, code: String)
}
