package co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode

interface KickoffHinodeView {
    fun showMessage(resId: Int)
    fun showLoading()
    fun hideLoading()
    fun showErrorMessageFailedToRegisterUser()
    fun setNextEnabled(enabled: Boolean)
    fun goToNextFragment()
}
