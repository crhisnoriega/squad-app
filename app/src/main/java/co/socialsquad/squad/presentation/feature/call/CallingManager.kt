package co.socialsquad.squad.presentation.feature.call

import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.PowerManager
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.CallQueueItem
import co.socialsquad.squad.data.entity.CallingQueueItem
import co.socialsquad.squad.data.entity.CancelQueueItem
import co.socialsquad.squad.data.entity.CancelVideo
import co.socialsquad.squad.data.entity.Candidate
import co.socialsquad.squad.data.entity.CandidateQueueItem
import co.socialsquad.squad.data.entity.CandidatesRemoveQueueItem
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.entity.SDP
import co.socialsquad.squad.data.entity.SdpQueueItem
import co.socialsquad.squad.data.entity.SendVideo
import co.socialsquad.squad.data.entity.VideoCallingQueueItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import org.webrtc.AudioSource
import org.webrtc.AudioTrack
import org.webrtc.Camera1Enumerator
import org.webrtc.Camera2Enumerator
import org.webrtc.CameraEnumerationAndroid
import org.webrtc.CameraVideoCapturer
import org.webrtc.DefaultVideoDecoderFactory
import org.webrtc.DefaultVideoEncoderFactory
import org.webrtc.EglBase
import org.webrtc.IceCandidate
import org.webrtc.MediaConstraints
import org.webrtc.MediaStream
import org.webrtc.PeerConnection
import org.webrtc.PeerConnectionFactory
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import org.webrtc.SurfaceTextureHelper
import org.webrtc.SurfaceViewRenderer
import org.webrtc.VideoSource
import org.webrtc.VideoTrack
import java.util.Date
import java.util.UUID
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

enum class VideoCallStatus(val label: Int) {
    READY(R.string.call_state_ready),
    UNKNOWN(R.string.call_state_unknown),
    CONNECTING(R.string.call_state_connecting),
    CALLING(R.string.call_state_calling),
    FAILED(R.string.call_state_failed),
    CONNECTED(R.string.call_state_connected),
    FINISHED(R.string.call_state_finished);
}

class CallingManager(
    private val context: Context,
    private val internalQueueId: String,
    private val receiverPk: Int,
    private var callUUID: String? = null,
    private val queueObserver: Observable<CallQueueItem>,
    private val queueEmitter: PublishSubject<CallQueueItem>,
    private val viewRenders: ViewRenders,
    private val onCallStatusChanged: (VideoCallStatus) -> Unit,
    private val callMode: CallMode,
    private val callViewInterface: CallViewInterface
) {

    enum class CallMode { OFFER, ANSWER }

    private val TAG = "CALLING_MANAGER"

    private val compositeDisposable = CompositeDisposable()
    private val mainHandler = Handler(Looper.getMainLooper())
    private val newThreadExecutor = Executors.newSingleThreadExecutor()

    private val eglBase = EglBase.create()
    private var factory: PeerConnectionFactory
    private var peerConnection: PeerConnection? = null
    private var videoCapturer: CameraVideoCapturer? = null
    private var audioManager: AudioManager? = null
    private var videoSource: VideoSource? = null
    private var audioSource: AudioSource? = null
    val renderContext: EglBase.Context get() = eglBase.eglBaseContext

    private lateinit var poweManager: PowerManager
    private var proximityWakeLock: PowerManager.WakeLock? = null
    private var sensorManager: SensorManager
    private var proximitySensor: Sensor? = null
    private var vibrator: Vibrator

    private val VIDEO_TRACK_LABEL = "remoteVideoTrack"
    private val AUDIO_TRACK_LABEL = "remoteAudioTrack"

    private var callingObservable: Disposable? = null
    private var callType: String? = null

    private var isCapturingVideo = false
    private var microphoneOn: Boolean = true
    private var loudSpeakerOn: Boolean = false
    private var cameraOn: Boolean = false
    private var showLocalView: Boolean = false
    private var showRemoteView: Boolean = false
    private var remoteSource: SourceSignal? = null
    private var localSource: SourceSignal? = null

    val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
    val ringtone = RingtoneManager.getRingtone(context, uri)

    private var mediaConstraints = MediaConstraints().apply {
        mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
        mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
        optional.add(MediaConstraints.KeyValuePair("RtpDataChannels", "true"))
    }

    private var proximitySensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

        override fun onSensorChanged(event: SensorEvent?) {
            if (event?.sensor?.type == Sensor.TYPE_PROXIMITY) {
                if (event.values[0] == 0F) {
                    audioSpeaker(false)
                }
            }
        }
    }

    init {
        val createInitializationOptions = PeerConnectionFactory.InitializationOptions.builder(context).createInitializationOptions()
        PeerConnectionFactory.initialize(createInitializationOptions)
        factory = PeerConnectionFactory.builder()
            .setVideoDecoderFactory(DefaultVideoDecoderFactory(eglBase.eglBaseContext))
            .setVideoEncoderFactory(DefaultVideoEncoderFactory(eglBase.eglBaseContext, true, true))
            .setOptions(PeerConnectionFactory.Options())
            .createPeerConnectionFactory()
        this.onCallStatusChanged(VideoCallStatus.CALLING)
        sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        proximitySensor?.let {
            sensorManager.registerListener(proximitySensorEventListener, it, SensorManager.SENSOR_DELAY_NORMAL)
        }
        vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    fun connect(callType: String?) {
        newThreadExecutor.execute { generateConnection(callType) } // Really good to execute in another thread
    }

    private var timerObservable: Disposable? = null

    private fun generateConnection(callType: String?) {
        this.callType = callType
        compositeDisposable.add(queueObserver.subscribe({ onMessageReceived(it) }, { it.printStackTrace() }))
        val urlArray = arrayListOf("turn:jabber.squad.com.br:3478", "stun:jabber.squad.com.br:3478")
        val iceServers = arrayListOf(
            PeerConnection.IceServer.builder(urlArray)
                .setUsername("admin")
                .setPassword("Squad@2018!")
                .createIceServer()
        )

        val rtcCfg = PeerConnection.RTCConfiguration(iceServers)
        rtcCfg.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        rtcCfg.sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN
        val rtcEvents = SimpleRTCEventHandler(this::handleLocalIceCandidate, this::addRemoteStream, this::removeRemoteStream, this::onIceConnectionChange, this::onIceCandidatesRemoved)
        peerConnection = factory.createPeerConnection(rtcCfg, rtcEvents)
        setupMediaDevices()
        enableProximityLock()
        if (this.callMode == CallMode.OFFER) {
            if (callType == CALL_TYPE_VIDEO) {
                startVideo()
                createVideoCall()
            } else {
                createCall()
            }
            onCallStatusChanged(VideoCallStatus.READY)
        } else {
            if (audioManager?.ringerMode == AudioManager.RINGER_MODE_NORMAL) {
                ringtone.play()
            } else if (audioManager?.ringerMode == AudioManager.RINGER_MODE_VIBRATE) {
                vibratePhone()
            }
            if (callType == CALL_TYPE_VIDEO) {
                startVideo()
            }
            onCallStatusChanged(VideoCallStatus.CONNECTING)
            timerObservable = Observable.timer(6, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (ringtone.isPlaying) ringtone.stop()
                    stopVibrate()
                    disableProximityLock()
                    this.onCallStatusChanged(VideoCallStatus.FINISHED)
                }
        }
    }

    private fun onIceCandidatesRemoved(candidates: Array<out IceCandidate>) {
        callUUID?.let {
            queueEmitter.onNext(CandidatesRemoveQueueItem(it, internalQueueId, null, candidates.map { candidate -> Candidate(candidate.sdp, candidate.sdpMid, candidate.sdpMLineIndex) }, Date()))
        }
    }

    private fun onIceConnectionChange(newState: PeerConnection.IceConnectionState) {
        when (newState) {
            PeerConnection.IceConnectionState.CONNECTED -> onCallStatusChanged(VideoCallStatus.CONNECTED)
            PeerConnection.IceConnectionState.DISCONNECTED -> onCallStatusChanged(VideoCallStatus.CONNECTING)
            PeerConnection.IceConnectionState.COMPLETED -> onCallStatusChanged(VideoCallStatus.FINISHED)
            PeerConnection.IceConnectionState.FAILED -> onCallStatusChanged(VideoCallStatus.FAILED)
            PeerConnection.IceConnectionState.CLOSED -> onCallStatusChanged(VideoCallStatus.FINISHED)
            else -> onCallStatusChanged(VideoCallStatus.UNKNOWN)
        }
    }

    private fun handleLocalIceCandidate(candidate: IceCandidate) {
        Log.w(TAG, "Local ICE candidate: $candidate")
        callUUID?.let {
            queueEmitter.onNext(CandidateQueueItem(it, internalQueueId, null, Candidate(candidate.sdp, candidate.sdpMid, candidate.sdpMLineIndex), Date()))
        }
    }

    private fun addRemoteStream(stream: MediaStream) {
        Log.w(TAG, "Got remote stream: $stream")
        newThreadExecutor.execute {
            if (stream.videoTracks.isNotEmpty()) {
                remoteVideoTrack = stream.videoTracks.first()
                remoteVideoTrack?.setEnabled(true)
                if (callType == CALL_TYPE_VIDEO) {
                    showLocalView = true
                    localSource = SourceSignal.CAMERA
                    remoteSource = SourceSignal.REMOTE
                    updateRender()
                }
            }
        }
    }

    private fun removeRemoteStream(@Suppress("UNUSED_PARAMETER") _stream: MediaStream) {
        // We lost the stream, lets finish
        Log.w(TAG, "Bye")
        onCallStatusChanged(VideoCallStatus.FINISHED)
    }

    private var maxSupported: CameraEnumerationAndroid.CaptureFormat? = null
    private var localVideoTrack: VideoTrack? = null
    private var remoteVideoTrack: VideoTrack? = null

    private fun setupMediaDevices() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val camera2 = Camera2Enumerator(context)
            if (camera2.deviceNames.isNotEmpty()) {
                val selectedDevice = camera2.deviceNames.firstOrNull(camera2::isFrontFacing)
                    ?: camera2.deviceNames.first()
                videoCapturer = camera2.createCapturer(selectedDevice, null)
                val supportedFormats = camera2.getSupportedFormats(selectedDevice)?.filter { it.framerate.max > 24 && it.height < 1081 }?.sortedBy { it.height }
                maxSupported = supportedFormats?.last()
            }
        }

        if (videoCapturer == null) {
            val camera1 = Camera1Enumerator(true)
            val selectedDevice = camera1.deviceNames.firstOrNull(camera1::isFrontFacing)
                ?: camera1.deviceNames.first()
            videoCapturer = camera1.createCapturer(selectedDevice, null)
            maxSupported = camera1.getSupportedFormats(selectedDevice).filter { it.framerate.max > 24 && it.height < 1081 }.sortedBy { it.height }.last()
        }

        val surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", renderContext)
        videoSource = factory.createVideoSource(videoCapturer?.isScreencast ?: false)
        videoSource?.capturerObserver?.let { videoCapturer?.initialize(surfaceTextureHelper, context, it) }
        localVideoTrack = factory.createVideoTrack(VIDEO_TRACK_LABEL, videoSource)
        localVideoTrack?.addSink(viewRenders.localView)
        peerConnection?.addTrack(localVideoTrack, listOf("video0"))

        audioSource = factory.createAudioSource(this.mediaConstraints)
        val audioTrack = factory.createAudioTrack(AUDIO_TRACK_LABEL, audioSource)
        peerConnection?.addTrack(audioTrack)

        audioManager = context.getSystemService(AUDIO_SERVICE) as (AudioManager)
        audioManager?.mode = AudioManager.MODE_IN_COMMUNICATION
        audioManager?.isMicrophoneMute = false
    }

    fun answerCall() {
        if (ringtone.isPlaying) ringtone.stop()
        stopVibrate()
        this.onCallStatusChanged(VideoCallStatus.CONNECTING)
        peerConnection?.createOffer(sdpObserver, mediaConstraints)
        updateRender()
    }

    fun createCall() {
        this.onCallStatusChanged(VideoCallStatus.CALLING)
        callUUID = UUID.randomUUID().toString()
        callingObservable = Observable.interval(1, TimeUnit.SECONDS).subscribe {
            callUUID?.let { queueEmitter.onNext(CallingQueueItem(it, internalQueueId, Date(), null, null, receiverPk)) }
        }
        callingObservable?.let { compositeDisposable.add(it) }
    }

    fun createVideoCall() {
        this.onCallStatusChanged(VideoCallStatus.CALLING)
        callUUID = UUID.randomUUID().toString()
        callingObservable = Observable.interval(1, TimeUnit.SECONDS).subscribe {
            callUUID?.let { queueEmitter.onNext(VideoCallingQueueItem(it, internalQueueId, Date(), null, null, receiverPk)) }
        }
        callingObservable?.let { compositeDisposable.add(it) }
    }

    private val sdpObserver = object : SdpObserver {
        override fun onSetFailure(reason: String?) {
            Log.e(TAG, reason.toString())
        }

        override fun onCreateSuccess(sessionDescription: SessionDescription?) {
            sessionDescription?.let {
                peerConnection?.setLocalDescription(
                    SDPSetCallback { error ->
                        error?.let { Log.e(TAG, "SetLocalDescription: $it") }
                        callUUID?.let { uuid -> queueEmitter.onNext(SdpQueueItem(uuid, internalQueueId, null, SDP(it.type.ordinal, it.description), Date())) }
                    },
                    it
                )
            }
        }

        override fun onSetSuccess() {
        }

        override fun onCreateFailure(reason: String?) {
            Log.e(TAG, "Error creating offer: $reason")
        }
    }

    private fun onMessageReceived(queueItem: CallQueueItem) {
        if (!queueItem.uuid.equals(callUUID, ignoreCase = true)) {
            if (queueItem is CallingQueueItem && callUUID == null) {
                callUUID = queueItem.uuid
            } else {
                return
            }
        }
        when (queueItem) {
            is CallingQueueItem -> {
                timerObservable?.dispose()
                onCallStatusChanged(VideoCallStatus.READY)
                onCallStatusChanged(VideoCallStatus.CONNECTING)
                queueItem.profile?.let { callViewInterface.fillContactInformation(it) }
            }

            is VideoCallingQueueItem -> {
                timerObservable?.dispose()
                onCallStatusChanged(VideoCallStatus.READY)
                onCallStatusChanged(VideoCallStatus.CONNECTING)
                queueItem.profile?.let { callViewInterface.fillContactInformation(it) }
                remoteSource = SourceSignal.CAMERA
                localSource = null
                updateRender()
            }

            is SdpQueueItem -> {
                callingObservable?.dispose()
                queueItem.sdp?.let {
                    val map = SessionDescription.Type.values().associateBy(SessionDescription.Type::ordinal)
                    handleRemoteDescriptor(SessionDescription(map[it.type], it.sdp))
                }
            }

            is CandidateQueueItem -> {
                queueItem.candidate?.let { handleRemoteCandidate(it.sdpMLineIndex, it.sdpMid, it.sdp) }
            }

            is CancelQueueItem -> {
                if (ringtone.isPlaying) ringtone.stop()
                stopVibrate()
                disableProximityLock()
                this.onCallStatusChanged(VideoCallStatus.FINISHED)
            }
            is CandidatesRemoveQueueItem -> {
                queueItem.candidates?.let {
                    val arrayOfIceCandidates = it.map { IceCandidate(it.sdpMid, it.sdpMLineIndex, it.sdp) }.toTypedArray()
                    peerConnection?.removeIceCandidates(arrayOfIceCandidates)
                }
            }
            is CancelVideo -> {
                if (remoteSource == SourceSignal.REMOTE) {
                    remoteSource = null
                }
                if (localSource == SourceSignal.CAMERA) {
                    localSource = null
                    remoteSource = SourceSignal.CAMERA
                }
                updateRender()
            }

            is SendVideo -> {
                if (remoteSource == SourceSignal.CAMERA) {
                    localSource = SourceSignal.CAMERA
                }
                remoteSource = SourceSignal.REMOTE
                updateRender()
            }
        }
    }

    private fun handleRemoteDescriptor(sdp: SessionDescription) {
        peerConnection?.setRemoteDescription(
            SDPSetCallback { setError ->
                if (setError != null) {
                    Log.e(TAG, "setRemoteDescription failed: $setError")
                } else {
                    if (callMode == CallMode.OFFER) {
                        peerConnection?.createAnswer(sdpObserver, mediaConstraints)
                    }
                }
            },
            sdp
        )
    }

    private fun handleRemoteCandidate(label: Int, id: String, strCandidate: String) {
        Log.w(TAG, "Got remote ICE candidate $strCandidate")
        newThreadExecutor.execute {
            val candidate = IceCandidate(id, label, strCandidate)
            peerConnection?.addIceCandidate(candidate)
        }
    }

    fun terminate() {
        compositeDisposable.clear()
        if (ringtone.isPlaying) ringtone.stop()
        stopVibrate()
        disableProximityLock()
        sensorManager.unregisterListener(proximitySensorEventListener)
        callUUID?.let { queueEmitter.onNext(CancelQueueItem(it, internalQueueId, Date())) }
        queueEmitter.onComplete()
        try {
            videoCapturer?.stopCapture()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        videoSource?.dispose()
        audioSource?.dispose()
        videoCapturer?.dispose()
        peerConnection?.dispose()
        factory.dispose()
        eglBase.release()
    }

    data class ViewRenders(val localView: SurfaceViewRenderer?, val remoteView: SurfaceViewRenderer?)

    fun toggleMic() {
        peerConnection?.senders?.filter { it.track() is AudioTrack }?.map { it.track() as AudioTrack }?.forEach {
            (it.setEnabled(!it.enabled()))
            microphoneOn = it.enabled()
            updateRender()
        }
    }

    fun toggleSpeaker() {
        audioManager?.let {
            audioSpeaker(!it.isSpeakerphoneOn)
        }
    }

    fun audioSpeaker(speakerOn: Boolean) {
        Thread(
            Runnable {
                audioManager?.let {
                    it.isSpeakerphoneOn = speakerOn
                    loudSpeakerOn = speakerOn
                    updateRender()
                }
            }
        ).start()
    }

    private fun enableProximityLock() {
        if (proximityWakeLock != null) {
            return
        }
        poweManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        proximityWakeLock = poweManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "squad:call")
        proximityWakeLock?.acquire(20 * 60 * 1000L)
    }

    private fun disableProximityLock() {
        if (proximityWakeLock != null) {
            proximityWakeLock?.release()
            proximityWakeLock = null
        }
    }

    @Suppress("DEPRECATION")
    private fun vibratePhone() {
        val pattern = longArrayOf(0, 400, 800, 600, 800, 800, 800, 1000)
        val amplitudes = intArrayOf(0, 255, 0, 255, 0, 255, 0, 255)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(pattern, amplitudes, 0))
        } else {
            vibrator.vibrate(pattern, 0)
        }
    }

    private fun stopVibrate() {
        try {
            vibrator.cancel()
        } catch (t: Throwable) {
        }
    }

    fun changeCaptureCamera() {
        videoCapturer?.switchCamera(object : CameraVideoCapturer.CameraSwitchHandler {
            override fun onCameraSwitchDone(swiched: Boolean) {}

            override fun onCameraSwitchError(error: String?) {
                Log.e("CAMERA_SWITCH_ERROR", error.toString())
            }
        })
    }

    private fun startVideo() {
        isCapturingVideo = false
        toggleCamera()
    }

    fun toggleCamera() {
        if (isCapturingVideo) {
            stopCapture()
            callUUID?.let { queueEmitter.onNext(CancelVideo(it, internalQueueId, Date(), "")) }
            if (remoteSource == SourceSignal.REMOTE) {
                localSource = null
            } else {
                remoteSource = null
            }
        } else {
            startCapture()
            callUUID?.let { queueEmitter.onNext(SendVideo(it, internalQueueId, Date(), "")) }
            if (remoteSource == SourceSignal.REMOTE) {
                localSource = SourceSignal.CAMERA
            } else {
                localSource = null
                remoteSource = SourceSignal.CAMERA
            }
        }
        updateRender()
    }

    private fun startCapture() {
        if (maxSupported == null) {
            videoCapturer?.startCapture(640, 480, 30)
        }
        maxSupported?.let { videoCapturer?.startCapture(it.width, it.height, it.framerate.max) }
        isCapturingVideo = true
        cameraOn = true
    }

    private fun stopCapture() {
        videoCapturer?.stopCapture()
        isCapturingVideo = false
        cameraOn = false
    }

    private fun localRenderSource() {
        when (localSource) {
            SourceSignal.REMOTE -> {
                try {
                    remoteVideoTrack?.removeSink(viewRenders.remoteView)
                } catch (t: Throwable) {
                    Log.w("REMOVE LOCAL SINK", t.message)
                }
                remoteVideoTrack?.addSink(viewRenders.localView)
                showLocalView = true
            }
            SourceSignal.CAMERA -> {
                try {
                    localVideoTrack?.removeSink(viewRenders.remoteView)
                } catch (t: Throwable) {
                    Log.w("REMOVE LOCAL SINK", t.message)
                }
                localVideoTrack?.addSink(viewRenders.localView)
                showLocalView = true
            }
            else -> {
                showLocalView = false
                if (!showRemoteView)
                    enableProximityLock()
            }
        }
    }

    private fun remoteRenderSource() {
        when (remoteSource) {
            SourceSignal.REMOTE -> {
                disableProximityLock()
                try {
                    remoteVideoTrack?.removeSink(viewRenders.localView)
                } catch (t: Throwable) {
                    Log.w("REMOVE REMOTE SINK", t.message)
                }
                remoteVideoTrack?.addSink(viewRenders.remoteView)
                showRemoteView = true
            }
            SourceSignal.CAMERA -> {
                disableProximityLock()
                try {
                    localVideoTrack?.removeSink(viewRenders.localView)
                } catch (t: Throwable) {
                    Log.w("REMOVE REMOTE SINK", t.message)
                }
                localVideoTrack?.addSink(viewRenders.remoteView)
                showRemoteView = true
            }
            else -> {
                showRemoteView = false
                if (!showLocalView)
                    enableProximityLock()
            }
        }
    }

    private fun updateRender() {
        mainHandler.post {
            localRenderSource()
            remoteRenderSource()
            callViewInterface.renderCallActionsState(microphoneOn, loudSpeakerOn, cameraOn, showLocalView, showRemoteView)
        }
    }

    private enum class SourceSignal { REMOTE, CAMERA }

    interface CallViewInterface {
        fun fillContactInformation(profile: Profile)
        fun renderCallActionsState(microphoneOn: Boolean, loudSpeakerOn: Boolean, cameraOn: Boolean, showLocalView: Boolean, showRemoteView: Boolean)
    }
}
