package co.socialsquad.squad.presentation.feature.revenue

import android.animation.ValueAnimator
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.View.MeasureSpec
import android.view.animation.AccelerateDecelerateInterpolator
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.*
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinkDetails
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextResourcesDetails
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectViewModel
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemHeaderListingViewModel
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemHeaderPointsViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingTopBottomViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingTopBottomViewModel
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewModel
import co.socialsquad.squad.presentation.feature.revenue.listing.REVENUE_LIST_TYPE
import co.socialsquad.squad.presentation.feature.revenue.listing.RevenueElementsListingActivity
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourceViewModel
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourcesViewHolder
import co.socialsquad.squad.presentation.feature.webview.WebViewActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.extra
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.Behavior.DragCallback
import kotlinx.android.synthetic.main.activity_profile_revenue.*
import kotlinx.android.synthetic.main.activity_revenue_object_detail.*
import kotlinx.android.synthetic.main.activity_revenue_object_detail.toolbar_title
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

private const val TOOLS_ADAPTER_VIEW_ID = R.layout.explore_item_tool
private const val TOOLS_DIVIDER_VIEW_ID = R.layout.item_tool_divider

class ObjectRevenueSummaryActivity : AppCompatActivity(), RichTextListener {

    private val viewModel: ExploreItemObjectViewModel by viewModel()
    private var selectedResource: RichTextResourcesDetails? = null
    private var buttonPool: MutableList<ButtonVM> = mutableListOf()

    private val link: Link by extra(ARGUMENT_LINK)
    lateinit var publicLink: String

    companion object {
        const val ARGUMENT_LINK = "ARGUMENT_LINK"

        var doRefresh = false

        fun newInstance(context: Context, link: Link): Intent {
            return Intent(context, ObjectRevenueSummaryActivity::class.java).apply {
                putExtra(ARGUMENT_LINK, link)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (doRefresh) {
            toolbar.visibility = View.GONE
            llLoading.visibility = View.VISIBLE
            viewModel.fetchObject(link)
            doRefresh = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_revenue_object_detail)
        setupObserver()
        setupObserverRichtext()
        setupObserverRichTextDownloadFile()
        setupPublicLinkObserver()
        setupToolbar()
        configureAppLayout()

        configureToolsList()

        rv_explore_object_rich_text.isVisible = false
        llLoading.isVisible = true

        viewModel.fetchObject(link)
    }

    private fun configureToolsList() {
        val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int {
                return when (viewModel) {
                    is ItemHeaderListingViewModel -> R.layout.item_listing_header

                    is ItemRevenueSourceViewModel -> R.layout.item_revenue_sources

                    is ItemShowAllRankingViewModel -> R.layout.ranking_separator

                    is ItemShowAllRankingTopBottomViewModel -> R.layout.ranking_separator_top_bottom

                    else -> throw IllegalArgumentException()
                }
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                R.layout.item_listing_header -> ItemHeaderPointsViewHolder(view)

                R.layout.item_revenue_sources -> ItemRevenueSourcesViewHolder(view) {
                    RevenueElementsListingActivity.start(
                        this@ObjectRevenueSummaryActivity,
                        REVENUE_LIST_TYPE.LEADS,
                        0,
                        "Indicações"
                    )
                }

                R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
                }

                R.layout.ranking_separator_top_bottom -> ItemSeparatorRankingTopBottomViewHolder(
                    view
                ) {

                }

                else -> throw IllegalArgumentException()
            }
        })

        rvTools.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            adapter = factoryAdapter

            var elements = mutableListOf<ViewModel>()

            elements.add(ItemShowAllRankingViewModel("", " "))
            elements.add(ItemHeaderListingViewModel("Oportunidades"))
            elements.add(ItemShowAllRankingTopBottomViewModel("", " "))

            factoryAdapter.addItems(elements)
        }

    }

    private fun configureAppLayout() {
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->


        })

        if (app_bar.layoutParams != null) {
            val layoutParams: CoordinatorLayout.LayoutParams =
                app_bar.layoutParams as CoordinatorLayout.LayoutParams
            val appBarLayoutBehaviour = AppBarLayout.Behavior()
            appBarLayoutBehaviour.setDragCallback(object : DragCallback() {
                override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                    return false
                }

            })
            layoutParams.behavior = appBarLayoutBehaviour
        }

        nestedScroll.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->

            Log.i("scroll", "scrollX: $scrollX scrollY: $scrollY")

            val scrollBounds = Rect()
            nestedScroll.getHitRect(scrollBounds)
            Log.i("scroll", "isVisible: ${levelName.getLocalVisibleRect(scrollBounds)}")

            if (levelName.getLocalVisibleRect(scrollBounds).not()) {
                subtitleHeader.text = levelName.text
                subtitleHeader.isVisible = true
                bottomHeaderDividerCollapsedObject.visibility = View.VISIBLE
                //bottomHeaderToolbar.isVisible = true
            } else {
                subtitleHeader.isVisible = false
                bottomHeaderDividerCollapsedObject.visibility = View.GONE
                //bottomHeaderToolbar.isVisible = false
            }
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupObserver() {
        lifecycleScope.launch {
            val value = viewModel.objectItem()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        Handler().postDelayed({
                            Log.i(
                                "scroll",
                                "height: ${app_bar.height} min height: ${app_bar.minimumHeight}"
                            )
                        }, 500)

                        toolbar.visibility = View.VISIBLE
                        mainContainer.setBackgroundColor(resources.getColor(R.color._eeeff4))
                        //loading.isVisible = false
                        llLoading.isVisible = false
                        // loadingToolbar.isVisible = false
                        nestedScroll.isVisible = true
                        resource.data?.let {
                            setupData(it)
                        }
                    }
                    Status.LOADING -> {
                        // loading.isVisible = true
                        // loadingToolbar.isVisible = true
                        nestedScroll.isVisible = false
                    }
                    Status.ERROR -> finish()
                    Status.EMPTY -> finish()
                }
            }
        }
    }


    private fun setupPublicLinkObserver() {
        lifecycleScope.launch {
            viewModel.uniqueLink.observe(this@ObjectRevenueSummaryActivity, Observer { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.i("link2", resource.data?.data?.link)
                        resource.data?.data?.link?.let {
                            publicLink = it
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> finish()
                    Status.EMPTY -> finish()
                }
            })
        }
    }

    private fun setupObserverRichtext() {
        lifecycleScope.launch {
            val value = viewModel.richTextAction()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        // loading.isVisible = false

                        val dialogBuilder = AlertDialog.Builder(
                            this@ObjectRevenueSummaryActivity,
                            R.style.TransparentDialog
                        )
                        val dialogView = layoutInflater.inflate(R.layout.dialog_informative, null)
                        dialogView.findViewById<TextView>(R.id.title).text =
                            getString(R.string.object_detail_rich_text_dialog_title)
                        dialogView.findViewById<TextView>(R.id.levelDescription).text =
                            getString(R.string.object_detail_rich_text_dialog_subtitle)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val alertDialog = dialogBuilder.create()

                        dialogView.findViewById<TextView>(R.id.backLogin).setOnClickListener {
                            alertDialog.dismiss()
                            finish()
                        }

                        alertDialog.show()
                    }
                    Status.LOADING -> {
                        // loading.isVisible = true
                    }
                    Status.ERROR -> {
                        // loading.isVisible = false
                        Toast.makeText(
                            this@ObjectRevenueSummaryActivity,
                            getString(R.string.object_detail_rich_text_fail_confirmation),
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                    Status.EMPTY -> {
                        // loading.isVisible = false
                    }
                }
            }
        }
    }

    private fun setupObserverRichTextDownloadFile() {
        lifecycleScope.launch {
            val value = viewModel.richTextDownload()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        llLoading.isVisible = false

                        resource.data?.let {

                            try {
                                val directory = cacheDir
                                val file = File(
                                    directory,
                                    "tempfile.${MimeTypeMap.getFileExtensionFromUrl(selectedResource?.url)}"
                                )
                                FileUtils.createFromInputStream(file, resource.data.byteStream())
                                val uri = FileProvider.getUriForFile(
                                    this@ObjectRevenueSummaryActivity,
                                    BuildConfig.APPLICATION_ID + ".fileprovider",
                                    file
                                )
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.setDataAndType(uri, selectedResource?.getMimeType())
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                startActivity(intent)
                            } catch (ignored: ActivityNotFoundException) {
                                Toast.makeText(
                                    this@ObjectRevenueSummaryActivity,
                                    getString(R.string.activity_not_found),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                    Status.LOADING -> {
                        llLoading.isVisible = true
                    }
                    Status.ERROR -> {
                        llLoading.isVisible = false
                        Toast.makeText(
                            this@ObjectRevenueSummaryActivity,
                            getString(R.string.object_detail_rich_text_fail_download),
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                    Status.EMPTY -> {
                        llLoading.isVisible = false
                    }
                }
            }
        }
    }

    private fun setupData(objectItem: ObjectItem) {
        toolbar.title = ""
        objectItem.section?.let {
            toolbar_title.apply {
                visibility = View.VISIBLE
                text = it.title
            }
        }

        buildDescriptionText(objectItem.longDescription)

        snippet.text = objectItem.snippet
        snippet.setTextColor(ColorUtils.stateListOf(ColorUtils.getCompanyColor(this)!!))
        levelName.text = objectItem.longTitle
        custom_fields_lead.text = objectItem.customFields


        toolbar.visibility = View.VISIBLE

        objectItem.medias?.let {
            multiMediaPager.bind(it, this)
        }

        rv_explore_object_rich_text.apply {
            layoutManager = LinearLayoutManager(context)
            adapter =
                RichTextAdapter(
                    this@ObjectRevenueSummaryActivity,
                    this@ObjectRevenueSummaryActivity
                )
        }

        objectItem.richText?.let {
            rv_explore_object_rich_text.isVisible = false

            it.button?.let { button ->
                buttonPool.add(
                    ButtonVM(button) {
                        button.link?.let {
                            viewModel.putRichTextAction(button)
                        }
                    }
                )
            }

            it.formattedItems?.apply {
                if (this.isNotEmpty()) {
                    it.button?.let { button ->
                        buttonPool.add(
                            ButtonVM(button) {
                                button.link?.let {
                                    viewModel.putRichTextAction(button)
                                }
                            }
                        )
                    }
                    rv_explore_object_rich_text.isVisible = true
                    (rv_explore_object_rich_text.adapter as RichTextAdapter).addRichText(this)
                }
            }
        }
    }


    private fun buildDescriptionText(longDescription: String?) {

        // makeTextViewResizable(fullDescription, 3, "ver mais")
        longDescription?.let { it ->

            fullDescription.text = it
            fullDescriptionExpandable.text = it

            fullDescription.setOnClickListener {

                fullDescription.visibility = View.GONE
                fullDescriptionExpandable.visibility = View.VISIBLE

                fullDescriptionExpandable.apply {

                    var collapsedHeight = this.measuredHeight

                    this.measure(
                        MeasureSpec.makeMeasureSpec(this.measuredWidth, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                    )

                    // set maxLines to MAX Integer, so we can calculate the expanded height

                    // set maxLines to MAX Integer, so we can calculate the expanded height
                    this.maxLines = Int.MAX_VALUE

                    // measure expanded height

                    // measure expanded height
                    measure(
                        MeasureSpec.makeMeasureSpec(this.measuredWidth, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                    )

                    val expandedHeight = this.measuredHeight

                    // animate from collapsed height to expanded height

                    // animate from collapsed height to expanded height
                    val valueAnimator = ValueAnimator.ofInt(collapsedHeight, expandedHeight)
                    valueAnimator.addUpdateListener { animation ->
                        this.height = animation.animatedValue as Int
                    }

                    valueAnimator.interpolator = AccelerateDecelerateInterpolator()

                    // start the animation
                    valueAnimator
                        .setDuration(500)
                        .start()
                }
            }
        }
    }

    override fun onRichTextLinkClicked(link: RichTextLinkDetails) {
        link.url?.let {
            startActivity(
                WebViewActivity.newInstance(
                    this,
                    it,
                    link.title?.run { this } ?: ""
                )
            )
        }
    }

    override fun onRichTextObjectClicked(link: Link) {
        link.url?.let {
            startActivity(newInstance(this, link))
        }
    }

    override fun onRichTextResourceClicked(resource: RichTextResourcesDetails) {
        selectedResource = resource
        resource.url?.apply {
            viewModel.getFileRichTextResource(this)
        } ?: run {
            Toast.makeText(
                this,
                getString(R.string.object_detail_rich_text_fail_mime),
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
