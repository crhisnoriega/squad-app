package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.view_qualification_prizes_list_item.view.*

const val QUALIFICATION_PRIZES_LIST_VIEW_MODEL_ID = R.layout.view_qualification_prizes_list_item

class QualificationPrizesListViewHolder(itemView: View) : ViewHolder<QualificationPrizesListViewModel>(itemView) {

    override fun bind(viewModel: QualificationPrizesListViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = QUALIFICATION_PRIZE_VIEW_MODEL_ID

            override fun getHolder(viewType: Int, view: View): ViewHolder<PrizeViewModel> = QualificationPrizeViewHolder(view)
        }
        itemView.rvPrizes.apply {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(viewModel.prizes) }
            layoutManager = LinearLayoutManager(itemView.context)
        }
    }

    override fun recycle() {
    }
}
