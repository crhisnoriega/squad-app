package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.adapter.BankEmptyStateAdapter
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemProfileBankViewHolder
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemProfileBankViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemProfileEmptyViewHolder
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemProfileEmptyViewModel
import kotlinx.android.synthetic.main.fragment_edit_profile_bank_form.*
import kotlinx.android.synthetic.main.view_search_toolbar_input_plain.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ProfileBankChooseFragment(
    var isInCompleteProfile: Boolean? = false
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupNextButton()
        setupSkipButton()
        setupBack()
        setupHeader()

        viewModel.banksInfo.observe(requireActivity(), Observer {
            setupContent(it)
        })
        viewModel.fetchBanksInfo()

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.searchBank(s.toString())
                if (s.toString().isNullOrBlank()) {
                    ibClear.visibility = View.GONE
                } else {
                    ibClear.visibility = View.VISIBLE
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        ibClear.setOnClickListener {
            viewModel.searchBank("")
            ibClear.visibility = View.GONE
            etSearch.setText("")
            hideKeyboard(requireActivity())
        }

        if (isInCompleteProfile!!) {
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_teddoc))
        }


        hideKeyboard(
            requireActivity()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_bank_form, container, false)

    private fun setupHeader() {
        // txtTitle.text = title
        // txtSubtitle.text = subtitle

    }

    private fun setupBack() {
        ibBack.setOnClickListener {
            viewModel.state.postValue(StateNav("previous", true))
            (activity as? CompleteProfileActivity)?.previous()
        }
    }

    private fun setupContent(banks: List<BankInfo>) {
        if (banks.isNullOrEmpty()) {
            bankList.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = BankEmptyStateAdapter(etSearch.text.toString())
            }

            return
        }

        var banksModels = banks.mapIndexed { index, it ->
            ItemProfileBankViewModel(it, index == 0)
        }.toMutableList<ViewModel>()

        banksModels.add(0, ItemProfileEmptyViewModel())
        banksModels.add(ItemProfileEmptyViewModel())

        var factory = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int {
                return when (viewModel) {
                    is ItemProfileEmptyViewModel -> R.layout.item_profile_bank_empty
                    is ItemProfileBankViewModel -> R.layout.item_profile_bank_result
                    else -> throw IllegalArgumentException()
                }
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    R.layout.item_profile_bank_result -> ItemProfileBankViewHolder(view) { bank ->
                        banksModels.forEach {
                            (it as? ItemProfileBankViewModel)?.bankInfo?.isChecked = false
                        }

                        bank.isChecked = true
                        btnConfirmar.isEnabled = true

                        bankList.adapter?.notifyDataSetChanged()

                        viewModel.selectBankInfo = bank
                    }

                    R.layout.item_profile_bank_empty -> ItemProfileEmptyViewHolder(view)
                    else -> throw IllegalArgumentException()
                }
            }
        })


        bankList.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            adapter = factory
        }

        factory.setItems(
            banksModels
        )
    }


    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    private fun setupNextButton() {
        btnConfirmar.setOnClickListener {
            viewModel.state.value = StateNav("next", false)
            (activity as? CompleteProfileActivity)?.next()
        }
    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
