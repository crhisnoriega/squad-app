package co.socialsquad.squad.presentation.custom

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel

interface ViewModel

interface ViewHolderFactory {
    fun getType(viewModel: ViewModel): Int
    fun getHolder(viewType: Int, view: View): ViewHolder<*>
}

abstract class ViewHolder<in T : ViewModel>(itemView: View) :
    RecyclerView.ViewHolder(itemView),
    LifecycleOwner {
    interface Listener<T> {
        fun onClick(viewModel: T)
    }

    abstract fun bind(viewModel: T)
    abstract fun recycle()

    private val lifecycleRegistry = LifecycleRegistry(this)

    init {
        lifecycleRegistry.markState(Lifecycle.State.INITIALIZED)
    }

    fun onAppear() {
        lifecycleRegistry.markState(Lifecycle.State.CREATED)
    }

    fun onDisappear() {
        lifecycleRegistry.markState(Lifecycle.State.DESTROYED)
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }
}

class RecyclerViewAdapter(private val viewHolderFactory: ViewHolderFactory) :
    RecyclerView.Adapter<ViewHolder<ViewModel>>() {

    private var recyclerView: RecyclerView? = null

    val items = arrayListOf<ViewModel>()
    private var loading = LoadingViewModel()

    fun startLoading(search: String = "") {
        if (items.contains(loading)) {
            items.remove(loading)
        }
        loading = LoadingViewModel(search)
        items.add(loading)
        notifyDataSetChanged()
    }

    fun stopLoading() {
        if (items.remove(loading)) {
            notifyDataSetChanged()
        }
    }

    override fun onViewAttachedToWindow(holder: ViewHolder<ViewModel>) {
        super.onViewAttachedToWindow(holder)
        holder.onAppear()
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder<ViewModel>) {
        super.onViewDetachedFromWindow(holder)
        holder.onDisappear()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<ViewModel> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return viewHolderFactory.getHolder(viewType, view) as ViewHolder<ViewModel>
    }

    override fun onBindViewHolder(holder: ViewHolder<ViewModel>, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = viewHolderFactory.getType(items[position])

    fun setItems(items: List<ViewModel>) {
        with(this@RecyclerViewAdapter.items) {
            clear()
            addAll(items)
        }
        notifyDataSetChanged()
    }

    fun removeItem(index:Int){
        this@RecyclerViewAdapter.items.removeAt(index)
        notifyDataSetChanged()
    }

    fun addItems(items: List<ViewModel>) {
        val firstInsertionPosition = this@RecyclerViewAdapter.items.size + 1
        this@RecyclerViewAdapter.items.addAll(items)
        notifyItemRangeInserted(firstInsertionPosition, this@RecyclerViewAdapter.items.size)
    }

    fun addItem(item: ViewModel, position: Int, refreshNext: Boolean = false) {
        if (position < 0) return
        this@RecyclerViewAdapter.items.add(position, item)
        notifyItemInserted(position)
        if (refreshNext) notifyItemChanged(position + 1)
    }

    fun remove(viewModel: ViewModel, refreshNext: Boolean = false) {
        val position = items.indexOf(viewModel)
        items.remove(viewModel)
        notifyItemRemoved(position)
        if (refreshNext) notifyItemChanged(position)
    }

    fun removeLast(viewModel: ViewModel) {
        val position = items.lastIndexOf(viewModel)
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onViewRecycled(holder: ViewHolder<ViewModel>) {
        holder.recycle()
    }

    fun update(viewModel: ViewModel, refreshItens: Boolean = true) = with(items) {
        val index = items.indexOf(viewModel)
        if (index < 0) return
        items[index] = viewModel
        if (refreshItens) notifyItemChanged(index)
    }

    fun getItemByPosition(position: Int) = items[position]

    fun getItem(viewModel: ViewModel): ViewModel? {
        val index = items.indexOf(viewModel)
        if (index < 0) return null
        return items[index]
    }

    fun removeAllFromPosition(position: Int) {
        if (position >= items.size) return
        val ranged = items.size - position
        for (i in position until items.size) items.removeAt(position)
        notifyItemRangeRemoved(position, ranged)
    }

    fun <R : ViewModel> getItemFromType(kClass: Class<R>): List<R> {
        return items.filterIsInstance(kClass)
    }
}
