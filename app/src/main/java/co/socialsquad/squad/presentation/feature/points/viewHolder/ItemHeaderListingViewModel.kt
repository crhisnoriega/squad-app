package co.socialsquad.squad.presentation.feature.points.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemHeaderListingViewModel(
    val title: String,
    val showDivider: Boolean? = false
) : ViewModel {

}