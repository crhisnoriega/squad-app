package co.socialsquad.squad.presentation.feature.explorev2.components.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide

class ObjectListItemsAdapter(
        val context: Context,
        val template: SectionTemplate,
        val items: List<ListItem>,
        val indicatorColor: String,
        val onItemSelected: (ListItem, SectionTemplate) -> Unit
) : RecyclerView.Adapter<ObjectListItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (template) {
            SectionTemplate.Square -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_vertical_rectangular_horizontal_item,
                            parent,
                            false
                    )
            )
            SectionTemplate.Circle -> ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.view_session_list_circle_item, parent, false)
            )
            SectionTemplate.RectangleHorizontal -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_vertical_rectangular_horizontal_item,
                            parent,
                            false
                    )
            )
            SectionTemplate.RectangleVertical -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_vertical_rectangular_vertical_item,
                            parent,
                            false
                    )
            )
            else -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_vertical_rectangular_horizontal_item,
                            parent,
                            false
                    )
            )
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listItem = items[position]

        listItem.media?.let {
            Glide.with(context).load(it.url).centerCrop().crossFade().into(holder.image)
        }
        holder.stretch.text = listItem.snippet
        holder.stretch.setTextColor(ColorUtils.parse(indicatorColor))
        holder.short.text = listItem.shortTitle
        holder.subtitle.text = listItem.shortDescription
        listItem.customFields?.let {
            holder.customFields.isVisible = true
            holder.customFields.text = it
        }

        holder.divideraaa.isVisible = position != 0

        if (listItem.link != null) {
            holder.container.setOnClickListener { onItemSelected(listItem, template) }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.image)
        val stretch: TextView = itemView.findViewById(R.id.stretch)
        val short: TextView = itemView.findViewById(R.id.tvShort)
        val subtitle: TextView = itemView.findViewById(R.id.status)
        val customFields: TextView = itemView.findViewById(R.id.custom_fields_lead)
        val container: ViewGroup = itemView.findViewById(R.id.container)
        val divideraaa: View = itemView.findViewById(R.id.divideraaa)
    }
}
