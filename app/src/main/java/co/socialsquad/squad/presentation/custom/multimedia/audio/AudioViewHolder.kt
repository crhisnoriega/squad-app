package co.socialsquad.squad.presentation.custom.multimedia.audio

import android.net.Uri
import android.util.Log
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.util.inflate

class AudioViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?) : MediaViewHolder(parent.inflate(R.layout.view_multi_media_pager_audio), lifecycleOwner) {

    private val mAudioView: AudioView = view.findViewById(R.id.audioView)

    private var mUri: Uri? = null
    private var mVisible: Boolean = true

    override fun bind(media: Media, label: String?, isFirst: Boolean, showLabel: Boolean) {
        mUri = null
        mVisible = media.isViewHolderVisible

        mAudioView.release()

        mAudioView.apply {

            media.thumbnail?.apply {
                loadThumbnail(this)
            }

            media.streamings?.let { streaming ->

                if (streaming.isNotEmpty()) {

                    streaming[0].apply {

                        playlistUrl?.apply {
                            mUri = Uri.parse(this)
                            Log.i("audio", "isFirst $isFirst")

                            prepareToRun()

                        }
                    }
                }
            }

            showLabel?.let {
                if(it) setLabel(label)
            }

        }
    }

    private fun prepareToRun() {
        mUri?.let {
            mAudioView.apply {
                prepare(it)
                Log.i("crhisn", "$this visible $mVisible")
                if (mVisible) {
                    play(10L, muted = false)
                    pause()
                } else {
                    pause()
                }
            }
        }
    }

    override fun onResume() {
    }

    override fun onPause() {
        mAudioView.apply {
            pause()
        }
    }

    override fun onStop() {
        mAudioView.apply {
            pause()
        }
    }

    override fun onDestroy() {
        mAudioView.apply {
            release()
        }
    }
}
