package co.socialsquad.squad.presentation.feature.notification.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel

object NotificationLoadingHeaderViewModel : ViewModel
object NotificationLoadingContentViewModel : ViewModel

const val ITEM_HEADER_LOADING = R.layout.view_header_loading
const val ITEM_NOTIFICATION_CONTENT_LOADING = R.layout.view_notification_item_loading_view

class NotificationLoadingHeaderViewHolder(itemView: View) :
    ViewHolder<NotificationLoadingHeaderViewModel>(itemView) {
    override fun bind(viewModel: NotificationLoadingHeaderViewModel) {
    }

    override fun recycle() {
    }
}

class NotificationLoadingContentViewHolder(itemView: View) :
    ViewHolder<NotificationLoadingContentViewModel>(itemView) {
    override fun bind(viewModel: NotificationLoadingContentViewModel) {
    }

    override fun recycle() {
    }
}
