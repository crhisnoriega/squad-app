package co.socialsquad.squad.presentation.feature.base.di

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.local.PreferencesImpl
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.remote.ApiUpload
import co.socialsquad.squad.data.remote.AuthenticationInterceptor
import co.socialsquad.squad.data.remote.CrashlyticsInterceptor
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.LoginRepositoryImpl
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.repository.ProfileRepositoryImpl
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.data.typeAdapter.SectionTypeAdapter
import co.socialsquad.squad.data.utils.DateDeserializer
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.presentation.App
import co.socialsquad.squad.presentation.custom.tool.ToolWidgetViewModel
import co.socialsquad.squad.presentation.feature.explorev2.SectionsViewModel
import co.socialsquad.squad.presentation.feature.explorev2.data.ExploreAPI
import co.socialsquad.squad.presentation.feature.explorev2.data.ExploreNotAuthAPI
import co.socialsquad.squad.presentation.feature.explorev2.data.ExploreNotAuthRepositoryImp
import co.socialsquad.squad.presentation.feature.explorev2.data.ExploreRepositoryImp
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepositoryNotAuth
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemViewModel
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.opportunityv2.repository.OpportunityDetailsRepository
import co.socialsquad.squad.presentation.feature.opportunityv2.repository.OpportunityDetailsRepositoryImpl
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.OpportunityDetailsViewModel
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.RankingListViewModel
import co.socialsquad.squad.presentation.feature.points.data.PersonalPointAPI
import co.socialsquad.squad.presentation.feature.points.repository.PersonalPointsRepository
import co.socialsquad.squad.presentation.feature.points.repository.PersonalPointsRepositoryImpl
import co.socialsquad.squad.presentation.feature.points.viewModel.PersonalPointsViewModel
import co.socialsquad.squad.presentation.feature.ranking.data.RankingAPI
import co.socialsquad.squad.presentation.feature.ranking.data.RankingAPIV2
import co.socialsquad.squad.presentation.feature.ranking.repository.RankingRepository
import co.socialsquad.squad.presentation.feature.ranking.repository.RankingRepositoryImpl
import co.socialsquad.squad.presentation.feature.recognition.data.RecognitionAPI

import co.socialsquad.squad.presentation.feature.recognition.repository.RecognitionRepository
import co.socialsquad.squad.presentation.feature.recognition.repository.RecognitionRepositoryImpl
import co.socialsquad.squad.presentation.feature.recognition.viewmodel.RecognitionViewModel
import co.socialsquad.squad.presentation.feature.revenue.data.RevenueAPI
import co.socialsquad.squad.presentation.feature.revenue.repository.RevenueRepository
import co.socialsquad.squad.presentation.feature.revenue.repository.RevenueRepositoryImp
import co.socialsquad.squad.presentation.feature.revenue.viewModel.RevenueViewModel
import co.socialsquad.squad.presentation.feature.searchv2.SearchViewModel
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepository
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepositoryImpl
import co.socialsquad.squad.presentation.feature.tool.repository.remote.ToolApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.sin

private const val TIMEOUT = 90
private const val WRITE_TIMEOUT = 180

val mainModule = module {

    // Data
    factory<ExploreRepository> { ExploreRepositoryImp(get()) }
    factory<ExploreRepositoryNotAuth> { ExploreNotAuthRepositoryImp(get()) }
    factory<ToolsRepository> { ToolsRepositoryImpl(get()) }

    factory { provideToolApi(get()) }
    factory { provideExploreApi(get()) }
    factory { provideRankingV2Api(get()) }
    factory { provideRecognitionApi(get()) }
    factory { providePersonalPointsApi(get()) }
    factory { provideAPI(get()) }
    factory { provideAPIUpload(get()) }
    factory { provideRevenueAPI(get()) }
    factory {
        provideExploreNotAuthApi(
            provideRetrofit(
                provideOkHttp(
                    false,
                    get(),
                    androidContext()
                )
            )
        )
    }
    factory { contentResolver(androidContext()) }
    factory { provideGson() }

    factory<OpportunityDetailsRepository> { OpportunityDetailsRepositoryImpl(get()) }
    factory<RankingRepository> { RankingRepositoryImpl(get()) }
    factory<RecognitionRepository> { RecognitionRepositoryImpl(get()) }
    factory<PersonalPointsRepository> { PersonalPointsRepositoryImpl(get()) }
    factory<RevenueRepository> { RevenueRepositoryImp(get()) }

    factory<ProfileRepository> { ProfileRepositoryImpl(get(), get(), get(), get()) }
    factory<LoginRepository> { LoginRepositoryImpl(get(), get(), get()) }

    // ViewModel
    viewModel { RankingListViewModel(get()) }
    viewModel { RecognitionViewModel(get()) }
    viewModel { SectionsViewModel(get(), get()) }
    viewModel { ExploreItemViewModel(get()) }
    viewModel { OpportunityDetailsViewModel(get()) }
    viewModel { ExploreItemObjectViewModel(get(), get()) }
    viewModel { SearchViewModel(get()) }
    viewModel { ToolWidgetViewModel(get()) }
    viewModel { PersonalPointsViewModel(get()) }
    viewModel { ProfileEditViewModel(get(), get()) }
    viewModel { RevenueViewModel(get()) }
}

val networkModule = module {
    single { provideOkHttp(true, get(), androidContext()) }
    factory { provideRetrofit(get()) }
}

val providersModule = module {
    factory { provideSharedPreferences(get()) }
    factory { providePreferences(get()) }
    factory {
        CompanyColor(
            App.companyColor?.primaryColor ?: "#282828",
            App.companyColor?.secondaryColor
        )
    }
}

fun provideGson(): Gson {
    return GsonBuilder()
        .enableComplexMapKeySerialization()
        .registerTypeAdapter(Date::class.java, DateDeserializer())
        .registerTypeAdapter(SectionTemplate::class.java, SectionTemplateTypeAdapter())
        .registerTypeAdapter(SectionType::class.java, SectionTypeAdapter())
        .create()
}

fun provideRankingApi(retrofit: Retrofit): RankingAPI = retrofit.create(RankingAPI::class.java)
fun provideRankingV2Api(retrofit: Retrofit): RankingAPIV2 =
    retrofit.create(RankingAPIV2::class.java)

fun provideRecognitionApi(retrofit: Retrofit): RecognitionAPI =
    retrofit.create(RecognitionAPI::class.java)

fun provideRevenueAPI(retrofit: Retrofit): RevenueAPI =
    retrofit.create(RevenueAPI::class.java)

fun providePersonalPointsApi(retrofit: Retrofit): PersonalPointAPI =
    retrofit.create(PersonalPointAPI::class.java)

fun provideOkHttp(
    auth: Boolean,
    preferences: Preferences,
    context: Context
): OkHttpClient {

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level =
        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

    val builder = OkHttpClient.Builder()
    builder.connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
    builder.readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
    builder.writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
    builder.addInterceptor(loggingInterceptor)
    builder.addInterceptor(CrashlyticsInterceptor())

    var cache = Cache(File(context.cacheDir, "retrofit_cache"), 60 * 60 * 1000L)
    builder.cache(cache)

    if (auth)
        builder.addInterceptor(AuthenticationInterceptor(preferences))

    return builder.build()
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(provideGson()))
        .client(okHttpClient)
        .build()
}


fun provideSharedPreferences(context: Context): SharedPreferences =
    PreferenceManager.getDefaultSharedPreferences(context)

fun providePreferences(sharedPreferences: SharedPreferences): Preferences =
    PreferencesImpl(sharedPreferences)

fun provideExploreApi(retrofit: Retrofit): ExploreAPI = retrofit.create(ExploreAPI::class.java)

fun provideToolApi(retrofit: Retrofit): ToolApi = retrofit.create(ToolApi::class.java)

fun provideExploreNotAuthApi(retrofit: Retrofit): ExploreNotAuthAPI =
    retrofit.create(ExploreNotAuthAPI::class.java)

fun provideAPI(retrofit: Retrofit): Api = retrofit.create(Api::class.java)

fun provideAPIUpload(retrofit: Retrofit): ApiUpload = retrofit.create(ApiUpload::class.java)

fun contentResolver(context: Context): ContentResolver = context.contentResolver
