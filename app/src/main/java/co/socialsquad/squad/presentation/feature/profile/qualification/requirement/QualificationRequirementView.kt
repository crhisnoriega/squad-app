package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import co.socialsquad.squad.presentation.custom.ViewModel

interface QualificationRequirementView {
    fun startLoading()
    fun stopLoading()
    fun setItems(items: List<ViewModel>)
    fun setupToolbar(title: String)
}
