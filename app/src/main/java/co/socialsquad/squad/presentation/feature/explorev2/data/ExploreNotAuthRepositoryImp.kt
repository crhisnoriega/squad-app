package co.socialsquad.squad.presentation.feature.explorev2.data

import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepositoryNotAuth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody

@ExperimentalCoroutinesApi
class ExploreNotAuthRepositoryImp(private val exploreNotAuthAPI: ExploreNotAuthAPI) : ExploreRepositoryNotAuth {

    override fun getFileRichTextResource(fileUrl: String): Flow<ResponseBody> {
        return flow {
            val result = exploreNotAuthAPI.getFileRichTextResource(fileUrl)
            emit(result)
        }
    }
}
