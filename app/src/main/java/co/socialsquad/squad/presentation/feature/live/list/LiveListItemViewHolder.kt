package co.socialsquad.squad.presentation.feature.live.list

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.toDate
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_live_list_item.view.*

class LiveListItemViewHolder(
    itemView2: View,
    private val onLiveClickedListener: Listener<LiveListItemViewModel>,
    private val onRecommendListener: Listener<LiveListItemViewModel>
) : ViewHolder<LiveListItemViewModel>(itemView2) {
    override fun bind(viewModel: LiveListItemViewModel) {
        with(viewModel) model@{
            itemView.apply {
                setOnClickListener { onLiveClickedListener.onClick(this@model) }

                Glide.with(this)
                    .load(imageUrl)
                    .roundedCorners(context)
                    .crossFade()
                    .into(ivImage)

                tvTitle.text = title
                tvDescription.text = description

                tvAuthor.apply {
                    text = userName
                    setOnClickListener { openUser(viewModel.userPk.toInt()) }
                }

                val viewCountResId = if (viewModel.viewCount == 1) R.string.view_count_singular else R.string.view_count_plural
                val viewCount = context.getString(viewCountResId, TextUtils.toUnitSuffix(viewModel.viewCount.toLong()))
                val timestamp = viewModel.date?.toDate()?.elapsedTime(context)
                tvViewCountAndTimestamp.text = listOfNotNull(viewCount, timestamp).joinToString(" • ")
                tvViewCountAndTimestamp.setOnClickListener {
                    val intent = Intent(itemView.context, UserListActivity::class.java).apply {
                        putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                        putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.LIVE_VISUALIZATION)
                    }
                    itemView.context.startActivity(intent)
                }
                tvDuration.text = duration.toDurationString()
                setupOptions(this@model)
            }
        }
    }

    private fun setupOptions(viewModel: LiveListItemViewModel) {
        itemView.apply {
            val options = object : ArrayList<Int>() {
                init {
                    if (viewModel.canRecommend) add(R.string.social_post_option_recommend)
                }
            }

            sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
            sOptions.adapter = SocialSpinnerAdapter(
                context,
                R.layout.textview_spinner_dropdown_options_item,
                R.layout.textview_spinner_dropdown_options,
                options
            )

            sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    val item = parent.getItemAtPosition(position) ?: return

                    val itemResId = item as Int
                    when (itemResId) {
                        R.string.social_post_option_recommend -> onRecommendListener.onClick(viewModel)
                    }
                    sOptions.setSelection(0)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java)
            .putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivImage)
    }
}
