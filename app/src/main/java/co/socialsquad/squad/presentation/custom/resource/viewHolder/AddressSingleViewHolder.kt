package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Address
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_addresses_list.view.*
import kotlinx.android.synthetic.main.item_addresses_list.view.innerDivider2
import kotlinx.android.synthetic.main.item_addresses_list.view.option_tools
import kotlinx.android.synthetic.main.item_addresses_list.view.tvMessage
import kotlinx.android.synthetic.main.item_link_list.view.*

class AddressSingleViewHolder(itemView: View, var callback: (address: Address) -> Unit) : ViewHolder<AddressSingleViewHolderModel>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_addresses_list
    }

    override fun bind(viewModel: AddressSingleViewHolderModel) {
        itemView.tvAddressLine1.text = viewModel.address.address_line1
        itemView.tvAddressLine2.text = viewModel.address.address_line2
        itemView.option_tools.isVisible = false
        itemView.innerDivider2.isVisible = false
        itemView.tvMessage.isVisible = false
        itemView.mainLayout.setOnClickListener {
            callback.invoke(viewModel.address)
        }
    }

    override fun recycle() {

    }
}