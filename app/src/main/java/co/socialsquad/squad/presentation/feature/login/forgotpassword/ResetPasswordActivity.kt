package co.socialsquad.squad.presentation.feature.login.forgotpassword

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm.ConfirmResetPasswordActivity
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordActivity
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setGradientBackground
import co.socialsquad.squad.presentation.util.showKeyboard
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_forgot_password.*
import javax.inject.Inject

class ResetPasswordActivity : BaseDaggerActivity(), ResetPasswordView {

    companion object {
        const val extraSubdomain = "extra_subdomain"
        const val extraToken = "extra_token"
        const val extraEmail = "extra_email"
    }

    private var company: Company? = null
    private var squadSiteValue = ""
    private var avatarValue = ""

    @Inject
    lateinit var presenter: ResetPasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        presenter.onCreate()

        val subdomain = intent?.extras?.getString(extraSubdomain)
        subdomain?.apply {
            presenter.getSubdomain(this)
            iltInput.setHelpText("Insira a nova senha para $this")
        }

        iltInput.addTextChangedListener { next.isEnabled = it.length >= 6 }

        setupClose()
        setupNext()
    }

    override fun onResume() {
        super.onResume()
        iltInput.requestFocus()
        showKeyboard()
    }

    override fun hideLoading() {
        loading.visibility = View.GONE
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun setupHeader(company: Company) {
        this.company = company
        company.subdomain?.run {
            squadSiteValue = this
            avatarValue = this
        }

        header.visibility = View.VISIBLE
        iltInput.visibility = View.VISIBLE
        nextContainer.visibility = View.VISIBLE

        if (company.isPublic != null && company.isPublic!!) {
            company.aboutGradient?.apply {
                if (this.isNotEmpty()) {
                    setColorsConfiguration(this[0], this[1], company.aboutFooterBackground!!, company.textColor!!)
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        squadHeader.visibility = View.VISIBLE
        squadHeader.bindCompany(company)
    }

    private fun setupClose() {
        back.setOnClickListener {
            val intent = Intent(this, OnboardingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    private fun setupNext() {
        presenter.onNextClick()
        hideKeyboard()
        next.isEnabled = false
        next.setOnClickListener {
            val subdomain = intent?.extras?.getString(extraSubdomain)
            val token = intent?.extras?.getString(extraToken)
            val email = intent?.extras?.getString(extraEmail)

            subdomain.apply {
                val intent = Intent(baseContext, ConfirmResetPasswordActivity::class.java)
                intent.putExtra(ConfirmResetPasswordActivity.extraSubdomain, this)
                intent.putExtra(ConfirmResetPasswordActivity.extraToken, token)
                intent.putExtra(ConfirmResetPasswordActivity.extraPassword, iltInput.getFieldText().toString())
                intent.putExtra(ConfirmResetPasswordActivity.extraEmail, email)
                intent.putExtra(LoginPasswordActivity.extraCompany, Gson().toJson(company))
                startActivity(intent)
            }
        }
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        container.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        back.setColorFilter(color)

        iltInput.setColors(textColor)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
