package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.ProfilePictureActivity
import kotlinx.android.synthetic.main.fragment_edit_profile_account_type.*
import org.koin.android.viewmodel.ext.android.viewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3


class ProfileAccountTypeFragment(
) : Fragment() {
    private val viewModel by viewModel<ProfileEditViewModel>()

    private var radioComponents = listOf<RadioButton>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_account_type, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureButtons()
        configureObservables()
    }


    private fun configureObservables() {
    }

    private fun hideSoftKeyboard(view: View) {
        val imm =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun configureButtons() {
        btnSalvar.setOnClickListener {
            if (radioTED.isChecked) {
                ProfilePictureActivity.showForm(
                    requireContext(),
                    ProfilePictureActivity.Companion.EDIT_TYPE.BANK_TED,
                )
            }

            if (radioPix.isChecked) {
                ProfilePictureActivity.showForm(
                    requireContext(),
                    ProfilePictureActivity.Companion.EDIT_TYPE.BANK_PIX,
                )
            }
        }



        btnBack.setOnClickListener {
            activity?.finish()
        }

        radioTED.setOnClickListener {
            radioTED.isChecked = true
            radioPix.isChecked = false

            btnSalvar.isEnabled = true
        }

        radioPix.setOnClickListener {
            radioTED.isChecked = false
            radioPix.isChecked = true

            btnSalvar.isEnabled = true
        }
    }


    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    private fun canSave(canSave: Boolean) {
        btnSalvar.isEnabled = true
    }


    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}
