package co.socialsquad.squad.presentation.feature.schedule

import dagger.Binds
import dagger.Module

@Module
abstract class CreateScheduleModule {
    @Binds
    abstract fun view(CreateScheduleActivity: CreateScheduleActivity): CreateScheduleView
}
