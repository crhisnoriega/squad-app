package co.socialsquad.squad.presentation.feature.audio.list

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_audio_list_item.view.*

class AudioListItemViewHolder(
    itemView: View,
    private val onRecommendListener: Listener<AudioViewModel>
) : ViewHolder<AudioViewModel>(itemView) {

    override fun bind(viewModel: AudioViewModel) {
        with(viewModel) model@{
            itemView.apply {
                Glide.with(this)
                    .load(avatar)
                    .circleCrop()
                    .crossFade()
                    .into(ivImage)

                tvDuration.text = duration?.toDurationString()

                tvTitle.text = title

                if (!description.isNullOrBlank()) {
                    tvDescription.visibility = View.VISIBLE
                    tvDescription.text = description
                } else {
                    tvDescription.visibility = View.GONE
                }

                tvAuthor.text = author
                tvAuthor.setOnClickListener { openUserActivity(viewModel.authorPk) }

                val viewCount = context.getString(R.string.audio_playbacks, TextUtils.toUnitSuffix(playbackCount))
                val timestamp = date.toDate()?.elapsedTime(context)?.capitalize() ?: ""
                @SuppressLint("SetTextI18n")
                tvViewCountAndTimestamp.text = "$viewCount • $timestamp"
                tvViewCountAndTimestamp.setOnClickListener { openUserListActivity(this@model) }

                setOnClickListener { openAudioDetailActivity(this@model) }
                setupOptions(this@model)
            }
        }
    }

    private fun openAudioDetailActivity(viewModel: AudioViewModel) {
        val intent = Intent(itemView.context, AudioDetailActivity::class.java).apply {
            putExtra(AudioDetailActivity.EXTRA_AUDIO_VIEW_MODEL, viewModel)
        }
        itemView.context.startActivity(intent)
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, creatorPK)
        }
        itemView.context.startActivity(intent)
    }

    private fun openUserListActivity(viewModel: AudioViewModel) {
        val intent = Intent(itemView.context, UserListActivity::class.java).apply {
            putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
            putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.AUDIO_PLAYBACK)
        }
        itemView.context.startActivity(intent)
    }

    private fun setupOptions(viewModel: AudioViewModel) {
        itemView.apply {
            val options = object : ArrayList<Int>() {
                init {
                    if (viewModel.canRecommend) add(R.string.social_post_option_recommend)
                }
            }

            sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
            sOptions.adapter = SocialSpinnerAdapter(
                context,
                R.layout.textview_spinner_dropdown_options_item,
                R.layout.textview_spinner_dropdown_options,
                options
            )

            sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    val item = parent.getItemAtPosition(position) ?: return

                    val itemResId = item as Int
                    when (itemResId) {
                        R.string.social_post_option_recommend -> onRecommendListener.onClick(viewModel)
                    }
                    sOptions.setSelection(0)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
    }

    override fun recycle() {
        itemView.apply { Glide.with(this).clear(ivImage) }
    }
}
