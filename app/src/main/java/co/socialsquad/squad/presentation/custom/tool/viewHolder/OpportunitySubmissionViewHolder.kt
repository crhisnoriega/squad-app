package co.socialsquad.squad.presentation.custom.tool.viewHolder

import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.*
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tool_lead_status.view.*

class OpportunitySubmissionViewHolder(
        override val containerView: View,
        val companyColor: CompanyColor?
) :
        ViewHolder<OpportunitySubmissionViewHolderModel>(containerView), LayoutContainer {

    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_tool_lead_status

        fun newInstance(parent: ViewGroup, companyColor: CompanyColor?) =
                OpportunitySubmissionViewHolder(parent.inflate(ITEM_VIEW_MODEL_ID), companyColor)
    }

    var onSubmissionClickListener: (submission: Submission) -> Unit = {}

    override fun bind(viewModel: OpportunitySubmissionViewHolderModel) {
        with(containerView) {
            setOnClickListener {
                onSubmissionClickListener(viewModel.submission)
            }

            lastDivider.isVisible = viewModel.isLast.not()

            img_lead.apply {
                val drawable =
                        context.getDrawableCompat(R.drawable.ic_opportunity_name_bg) as LayerDrawable
                val bgDrawable = drawable.findDrawableByLayerId(R.id.opportunityBGGradient)
                        .mutate() as Drawable
                val defaultColor = context.getColorCompat(R.color.darkjunglegreen_28)
                val color =
                        companyColor?.primaryColor?.let { ColorUtils.parse(it) } ?: defaultColor

                val wrappedDrawable = DrawableCompat.wrap(bgDrawable)
                DrawableCompat.setTint(wrappedDrawable, color)
                // gradientDrawable.colors = intArrayOf(startColor, endColor)

                this.background = wrappedDrawable
            }


            // companyColor?.let {
            //     img_lead.setBackgroundColor(ColorUtils.parse(it.primaryColor))
            // }

            if (viewModel.submission.initials == null || viewModel.submission.initials.isEmpty()) {
                lbl_initials.text = "R"
            } else {
                lbl_initials.text = viewModel.submission.initials
            }
            lbl_lead_title.text = viewModel.submission.title
            lbl_lead_description.text = viewModel.submission.timestamp

            viewModel.submission.shift?.let {
                lbl_lead_description.text = it
            }

            viewModel.submission.startdatetime?.let {
                lbl_shift.text = it.toString()
            }

            if (viewModel.submission.progress != null) {
                progress_1.bind(
                        status = viewModel.submission.progress.status!!,
                        total = viewModel.submission.progress.total!!,
                        current = viewModel.submission.progress.current!!,
                        loading = false
                )
                tv_step.text = viewModel.submission.progress.title
            } else {
                progress_1.visibility = View.GONE
            }
        }
    }

    override fun recycle() {}
}
