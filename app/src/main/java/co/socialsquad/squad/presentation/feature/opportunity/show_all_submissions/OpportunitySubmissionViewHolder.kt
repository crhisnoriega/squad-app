package co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions

import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.getColorCompat
import co.socialsquad.squad.presentation.util.getDrawableCompat
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_opportunity.view.*
import kotlinx.android.synthetic.main.item_opportunity_step.view.*
import kotlinx.android.synthetic.main.item_opportunity_step_bigger.view.*

const val ITEM_OPPORTUNITY_VIEW_MODEL_ID = R.layout.item_opportunity
const val ITEM_OPPORTUNITY_STEP_VIEW_MODEL_ID = R.layout.item_opportunity_step
const val ITEM_OPPORTUNITY_STEP_BIGGER_VIEW_MODEL_ID = R.layout.item_opportunity_step_bigger

data class ItemOpportunityViewModel(
    val primaryInitials: String,
    val primaryFieldText: String,
    val timeSpan: String,
    val step: String,
    val steps: List<ItemOpportunityStepViewModel>,
    val colors: OpportunityInitialsColors? = null,
    val onClick: () -> Unit
) : ViewModel

sealed class ItemOpportunityStepViewModel(
    open val iconRes: Int,
    open val showLine: Boolean
) : ViewModel {
    data class Normal(
        override val iconRes: Int,
        override val showLine: Boolean
    ) : ItemOpportunityStepViewModel(iconRes, showLine)

    data class Bigger(
        override val iconRes: Int,
        override val showLine: Boolean
    ) : ItemOpportunityStepViewModel(iconRes, showLine)
}

class OpportunitySubmissionViewHolder(
    itemView: View
) : ViewHolder<ItemOpportunityViewModel>(itemView) {

    val viewHolderFactory = object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is ItemOpportunityStepViewModel.Normal -> ITEM_OPPORTUNITY_STEP_VIEW_MODEL_ID
            is ItemOpportunityStepViewModel.Bigger -> ITEM_OPPORTUNITY_STEP_BIGGER_VIEW_MODEL_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            ITEM_OPPORTUNITY_STEP_VIEW_MODEL_ID -> RecommendationStepViewHolder(view)
            ITEM_OPPORTUNITY_STEP_BIGGER_VIEW_MODEL_ID -> RecommendationStepBiggerViewHolder(view)
            else -> throw IllegalArgumentException()
        }
    }

    override fun bind(viewModel: ItemOpportunityViewModel) {

        val context = itemView.context

        itemView.ivNameIcon.apply {

            val drawable =
                context.getDrawableCompat(R.drawable.ic_opportunity_name_bg) as LayerDrawable
            val gradientDrawable = drawable.findDrawableByLayerId(R.id.opportunityBGGradient)
                .mutate() as GradientDrawable
            val defaultColor = context.getColorCompat(R.color.darkjunglegreen_28)
            val startColor =
                viewModel.colors?.primaryColor?.let { ColorUtils.parse(it) } ?: defaultColor
            val endColor =
                viewModel.colors?.secondaryColor?.let { ColorUtils.parse(it) } ?: startColor

            gradientDrawable.colors = intArrayOf(startColor, endColor)

            this.setImageDrawable(gradientDrawable)
        }

        itemView.tvNameIcon.setText(viewModel.primaryInitials)
        itemView.short_title_lead.setText(viewModel.primaryFieldText)
        itemView.tvTimeSpan.setText(viewModel.timeSpan)
        itemView.tvStep.setText(viewModel.step)
        itemView.rvSteps.apply {
            layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = RecyclerViewAdapter(viewHolderFactory).apply {
                addItems(viewModel.steps)
            }
            isNestedScrollingEnabled = false
            setHasFixedSize(true)
        }
        itemView.setOnClickListener { viewModel.onClick() }
    }

    override fun recycle() {
    }
}

class RecommendationStepViewHolder(itemView: View) :
    ViewHolder<ItemOpportunityStepViewModel>(itemView) {
    override fun bind(viewModel: ItemOpportunityStepViewModel) {
        itemView.ivStep.setImageResource(viewModel.iconRes)
        itemView.viewLine.isVisible = viewModel.showLine
    }

    override fun recycle() {
    }
}

class RecommendationStepBiggerViewHolder(itemView: View) :
    ViewHolder<ItemOpportunityStepViewModel>(itemView) {
    override fun bind(viewModel: ItemOpportunityStepViewModel) {
        itemView.ivStepBigger.setImageResource(viewModel.iconRes)
        itemView.viewLineBigger.isVisible = viewModel.showLine
    }

    override fun recycle() {
    }
}
