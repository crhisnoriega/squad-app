package co.socialsquad.squad.presentation.custom

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ExplorePagerAdapter : FragmentPagerAdapter {

    private var fragments: Array<Fragment>? = null
    private var titles: Array<String>? = null

    constructor(fm: FragmentManager, fragments: Array<Fragment>) : super(
        fm,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        this.fragments = fragments
    }

    override fun getItem(position: Int): Fragment {
        return fragments!![position]
    }

    override fun getCount(): Int {
        return fragments!!.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (titles != null) {
            titles!![position]
        } else super.getPageTitle(position)
    }
}
