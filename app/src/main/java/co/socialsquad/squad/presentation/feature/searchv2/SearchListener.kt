package co.socialsquad.squad.presentation.feature.searchv2

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link

interface SearchListener {
    fun onItemClicked(link: Link)
    fun onShowAllClicked(sectionId: Int)
}
