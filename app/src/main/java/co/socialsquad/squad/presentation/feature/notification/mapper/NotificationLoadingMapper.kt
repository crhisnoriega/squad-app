package co.socialsquad.squad.presentation.feature.notification.mapper

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.notification.viewHolder.NotificationLoadingContentViewModel
import co.socialsquad.squad.presentation.feature.notification.viewHolder.NotificationLoadingHeaderViewModel

object NotificationLoadingMapper {

    fun mapLoadingContent(): List<ViewModel> {
        return listOf(
            NotificationLoadingHeaderViewModel,
            NotificationLoadingContentViewModel,
            NotificationLoadingContentViewModel,
            NotificationLoadingContentViewModel,
            NotificationLoadingContentViewModel,
            NotificationLoadingContentViewModel,
            NotificationLoadingContentViewModel
        )
    }
}
