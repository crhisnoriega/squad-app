package co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode

import android.os.Bundle
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.data.utils.TagWorker
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class KickoffHinodePresenter @Inject constructor(
    private val kickoffHinodeView: KickoffHinodeView,
    private val signupRepository: SignupRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker,
    private val simpleWordEncrypt: SimpleWordEncrypt,
    private val preferences: Preferences
) {

    companion object {
        const val CREDENTIALS_HINODE_ID = "CREDENTIALS_HINODE_ID"
        const val CREDENTIALS_HINODE_PASSWORD = "CREDENTIALS_HINODE_PASSWORD"
    }

    private val compositeDisposable = CompositeDisposable()

    fun onSaveTapped(arguments: Bundle) {
        val id = arguments.getString(CREDENTIALS_HINODE_ID)
        val password = arguments.getString(CREDENTIALS_HINODE_PASSWORD)
        val email = signupRepository.email

        if (id != null && password != null && email != null) {
            val registerIntegrationRequest = RegisterIntegrationRequest(id.toLong(), password, email)
            compositeDisposable.add(
                signupRepository.registerIntegration(registerIntegrationRequest)
                    .doOnNext { storeCredentials(registerIntegrationRequest.password) }
                    .doOnSubscribe { disposable -> kickoffHinodeView.showLoading() }
                    .subscribe(
                        { (token, userCompany) ->
                            loginRepository.token = token
                            loginRepository.subdomain = userCompany.company.subdomain
                            loginRepository.userCompany = userCompany
                            loginRepository.squadColor = userCompany.company.primaryColor

                            kickoffHinodeView.hideLoading()
                            kickoffHinodeView.goToNextFragment()

                            tagWorker.tagSignUpEvent()
                        },
                        { throwable ->
                            throwable.printStackTrace()
                            kickoffHinodeView.hideLoading()
                            kickoffHinodeView.showErrorMessageFailedToRegisterUser()
                        }
                    )
            )
        }
    }

    private fun storeCredentials(password: String) {
        val encrypted = simpleWordEncrypt.encrypt(password)
        preferences.externalSystemPassword = encrypted?.encryptedMessage
        preferences.externalSystemPasswordIV = encrypted?.iv
    }

    fun onInput(id: String?, password: String?) {
        val isValid = !id.isNullOrBlank() && !password.isNullOrBlank()
        kickoffHinodeView.setNextEnabled(isValid)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
