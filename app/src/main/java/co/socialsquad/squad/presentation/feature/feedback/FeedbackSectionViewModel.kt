package co.socialsquad.squad.presentation.feature.feedback

import co.socialsquad.squad.data.entity.FeedbackSection
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable

class FeedbackSectionViewModel(
    val pk: Int,
    val title: String,
    val imageUrl: String,
    val questions: List<FeedbackQuestionViewModel>
) : ViewModel, Serializable {
    constructor(section: FeedbackSection) : this(
        section.pk ?: 0,
        section.title ?: "",
        section.icon ?: "",
        section.questions?.filterNotNull()?.map { FeedbackQuestionViewModel(it) } ?: emptyList()
    )
}
