package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SectionList(
    @SerializedName("sections") val sections: List<Section>
) : Parcelable
