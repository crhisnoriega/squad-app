package co.socialsquad.squad.presentation.feature.profile

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_profile_wallet.*
import kotlinx.android.synthetic.main.item_card_waller_profile.view.*
import org.koin.core.module.Module


private const val RESOURCES_LIST = "resources_list"
private const val RESOURCES_TITLE = "resources_title"
private const val RESOURCES_SUBTITLE = "resources_subtitle"

class ProfileWalletActivity : BaseActivity() {

    companion object {
        fun start(context: Context, title: String, subtitle: String) {
            context.startActivity(
                    Intent(context, ProfileWalletActivity::class.java).apply {
                        putExtra(RESOURCES_TITLE, title)
                        putExtra(RESOURCES_SUBTITLE, subtitle)
                    }
            )
        }
    }


    override val modules: List<Module> = listOf()
    override val contentView = R.layout.activity_profile_wallet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFlagLayoutFullscreen()


        header.setBackgroundColor(Color.parseColor(getCompanyColor()!!))
        ibBack.setOnClickListener {
            finish()
        }

        rlContactCircle2.backgroundTintList =
            ColorUtils.getCompanyColor(this)?.let { ColorUtils.stateListOf(it) }

    }


    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(this).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

}
