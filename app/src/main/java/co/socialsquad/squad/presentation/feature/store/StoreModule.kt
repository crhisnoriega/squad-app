package co.socialsquad.squad.presentation.feature.store

import dagger.Binds
import dagger.Module

@Module
abstract class StoreModule {
    @Binds
    abstract fun view(storeFragment: StoreFragment): StoreView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
