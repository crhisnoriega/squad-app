package co.socialsquad.squad.presentation.feature.social.create.live

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.LiveCategory
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.ScheduledRequest
import co.socialsquad.squad.data.entity.SegmentationQueryRequest
import co.socialsquad.squad.data.entity.SegmentationRequest
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.schedule.SocialCreateScheduledLiveActivity
import co.socialsquad.squad.presentation.util.toUTC
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.reactivex.disposables.CompositeDisposable
import java.util.Date
import javax.inject.Inject

class SocialCreateLivePresenter
@Inject constructor(
    private val socialCreateLiveView: SocialCreateLiveView,
    private val liveRepository: LiveRepository,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository,
    private val mixpanelAPI: MixpanelAPI
) {

    private val compositeDisposable = CompositeDisposable()
    private val categoryList: MutableList<LiveCategoryViewModel> = mutableListOf()
    private var post: PostLiveViewModel? = null

    fun onCreate(intent: Intent) {
        post = intent.getSerializableExtra(SocialCreateMediaActivity.EXTRA_POST) as PostLiveViewModel?
        if (post != null) {
            mixpanelAPI.track("Social / Edit / Live")
            socialCreateLiveView.setupToolbar(R.string.social_create_live_title_edit)
            setupEditLive()
        } else {
            mixpanelAPI.track("Social / Create / Live")
            socialCreateLiveView.setupToolbar(R.string.social_create_live_title_new)
        }
        getCategories()
        socialCreateLiveView.setupAudienceColor(loginRepository.squadColor)
    }

    private fun setupEditLive() {
        socialCreateLiveView.setupStartButton(R.string.social_create_live_button_confirm)
        socialCreateLiveView.hideSchedule()
        post?.apply {
            socialCreateLiveView.setupTitle(title)
            socialCreateLiveView.setupDescription(description)
            socialCreateLiveView.setupCategory(category)
            socialCreateLiveView.setupAudience(audience)
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onInput(liveCategoryViewModel: LiveCategoryViewModel?, title: String?, description: String?) {
        val conditionsMet = liveCategoryViewModel != null && !title.isNullOrBlank() && !description.isNullOrBlank()
        socialCreateLiveView.setStartButtonEnabled(conditionsMet)
    }

    private fun getCategories() {
        compositeDisposable.add(
            liveRepository.getLiveCategories()
                .doOnSubscribe { socialCreateLiveView.showLoading() }
                .doOnTerminate { socialCreateLiveView.hideLoading() }
                .subscribe(
                    { liveCategories ->
                        liveCategories.categories.mapTo(categoryList) {
                            LiveCategoryViewModel(pk = it.pk, name = it.name)
                        }
                        socialCreateLiveView.showCategories(categoryList)
                    },
                    {
                        it.printStackTrace()
                        socialCreateLiveView.showErrorMessage(R.string.social_create_live_error_failed_to_get_categories)
                    }
                )
        )
    }

    fun onStartClicked(
        liveCategoryViewModel: LiveCategoryViewModel,
        title: String,
        description: String,
        metrics: MetricsViewModel?,
        members: IntArray?,
        downline: Boolean,
        scheduleStart: Date?,
        scheduleEnd: Date?
    ) {
        val segmentationRequest = when {
            metrics != null && metrics.selected.isNotEmpty() -> {
                val queries = metrics.selected.map { SegmentationQueryRequest(it.field, it.values.map { it.value }) }
                SegmentationRequest(queries = queries)
            }
            members != null && members.isNotEmpty() -> SegmentationRequest(members = members.toList())
            downline -> SegmentationRequest(downline = true)
            else -> null
        }

        when {
            post != null -> {
                val feedRequest = FeedRequest(
                    title = title,
                    content = description,
                    type = post!!.type,
                    segmentation = segmentationRequest,
                    category = LiveCategory(liveCategoryViewModel.pk, liveCategoryViewModel.name)
                )

                compositeDisposable.add(
                    socialRepository.editPost(post!!.pk, feedRequest)
                        .doOnSubscribe { socialCreateLiveView.showLoading() }
                        .doOnTerminate { socialCreateLiveView.hideLoading() }
                        .subscribe(
                            {
                                mixpanelAPI.track("Social / Edit / Live / Success ")
                                socialCreateLiveView.finishWithResult()
                            },
                            { throwable ->
                                throwable.printStackTrace()
                                mixpanelAPI.track("Social / Edit / Live / Failure ")
                                socialCreateLiveView.showErrorMessage(R.string.social_create_media_error_failed_to_edit)
                            }
                        )
                )
            }
            scheduleStart != null -> {
                val category = LiveCategory(liveCategoryViewModel.pk, liveCategoryViewModel.name)

                val schedule = ScheduledRequest(scheduleStart.toUTC(), scheduleEnd?.toUTC())
                val liveCreateRequest = LiveCreateRequest(
                    category = category,
                    name = title,
                    content = description,
                    segmentation = segmentationRequest,
                    schedule = schedule
                )
                val intent = Intent().apply {
                    putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_LIVE_REQUEST, liveCreateRequest)
                }
                socialCreateLiveView.finishWithResult(intent)
            }
            else -> {
                val category = LiveCategory(liveCategoryViewModel.pk, liveCategoryViewModel.name)

                val liveCreateRequest = LiveCreateRequest(
                    category = category,
                    name = title,
                    content = description,
                    segmentation = segmentationRequest,
                    aspectRatioHeight = 800,
                    aspectRatioWidth = 600
                )

                mixpanelAPI.track("Social / Create / Live / Create")

                socialCreateLiveView.openLiveCreator(liveCreateRequest)
            }
        }
    }
}
