package co.socialsquad.squad.presentation.feature.existinglead.adapter.result

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM

abstract class ResultsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: ExistingLeadVM)
}
