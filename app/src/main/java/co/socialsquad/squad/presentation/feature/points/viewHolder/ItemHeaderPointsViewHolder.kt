package co.socialsquad.squad.presentation.feature.points.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible

import kotlinx.android.synthetic.main.item_listing_header.view.*


class ItemHeaderPointsViewHolder(itemView: View) :
    ViewHolder<ItemHeaderListingViewModel>(itemView) {
    override fun bind(viewModel: ItemHeaderListingViewModel) {
        itemView.headerLabel.text = viewModel.title
        itemView.bottom_divider.isVisible = viewModel.showDivider!!

    }

    override fun recycle() {

    }

}