package co.socialsquad.squad.presentation.feature.opportunity.detail

import android.content.Context
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.data.entity.opportunity.SubmissionStatus
import co.socialsquad.squad.presentation.util.getColorCompat

class OpportunitySubmissionDetailsMapper(private val context: Context) {

    fun mapStatusText(submissionDetails: SubmissionDetails): String {
        val res = when (submissionDetails.status) {
            SubmissionStatus.ACTIVE -> R.string.opportunity_status_active
            SubmissionStatus.BLOCKED -> R.string.opportunity_status_blocked
            SubmissionStatus.CLOSED -> R.string.opportunity_status_closed
        }
        return context.getString(res)
    }

    fun mapStatusColor(submissionDetails: SubmissionDetails): Int {
        val res = when (submissionDetails.status) {
            SubmissionStatus.ACTIVE -> R.color.grass
            SubmissionStatus.BLOCKED -> R.color.brownish_orange
            SubmissionStatus.CLOSED -> R.color._c7292b
        }
        return context.getColorCompat(res)
    }
}
