package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.LinkTypeAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Link(
    @SerializedName("id") val id: Int?,
    @SerializedName("type") @JsonAdapter(LinkTypeAdapter::class) val type: LinkType?,
    @SerializedName("url") val url: String?
) : Parcelable
