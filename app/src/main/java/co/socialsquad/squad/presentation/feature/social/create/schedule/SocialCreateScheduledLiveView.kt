package co.socialsquad.squad.presentation.feature.social.create.schedule

import android.content.Intent
import android.net.Uri
import java.util.Calendar
import java.util.Date

interface SocialCreateScheduledLiveView {
    fun setupViews(backgroundColor: String?, scheduleStartDate: Date?, scheduleEndDate: Date?)
    fun setupToolbar(titleResId: Int)
    fun setupFeaturedImage(imageURI: Uri)
    fun enableButton(conditionsMet: Boolean)
    fun setFinishWithResult(intent: Intent)
    fun setupPromotionalVideoValues(date: Calendar?, startTime: Date?, endTime: Date?)
    fun setupPromotionalVideo(videoUri: Uri)
    fun openVideoActivity(videoUri: Uri)
}
