package co.socialsquad.squad.presentation.feature.explorev2.objectitem

import android.animation.ValueAnimator
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.text.Layout
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.domain.model.explore.ComplementType
import co.socialsquad.squad.presentation.App
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.*
import co.socialsquad.squad.presentation.custom.tool.ToolWidgetViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.complement.contact.*
import co.socialsquad.squad.presentation.feature.explorev2.complement.price.PriceSingleViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.complement.price.PriceSingleViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.components.tools.DividerViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.components.tools.DividerViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.components.tools.ToolsViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.components.tools.ToolsViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinkDetails
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextResourcesDetails
import co.socialsquad.squad.presentation.feature.explorev2.listing.COMPLEMENT_LIST_TYPE
import co.socialsquad.squad.presentation.feature.explorev2.listing.ComplementsListingActivity
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.address.ExploreAddressDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.sharing.UniqueLinkActivity
import co.socialsquad.squad.presentation.feature.explorev2.sharing.customview.ChooseUniqueLinkSheetDialog
import co.socialsquad.squad.presentation.feature.lead.LeadActivity
import co.socialsquad.squad.presentation.feature.webview.WebViewActivity
import co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary.WidgetScheduleSummaryActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.Behavior.DragCallback
import com.google.gson.Gson
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_explore_item_object.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import javax.inject.Inject


private const val TOOLS_ADAPTER_VIEW_ID = R.layout.explore_item_tool
private const val TOOLS_DIVIDER_VIEW_ID = R.layout.item_tool_divider

class ExploreItemObjectActivity : AppCompatActivity(), RichTextListener {

    private val viewModel: ExploreItemObjectViewModel by viewModel()
    private val viewModelTool: ToolWidgetViewModel by viewModel()
    private var selectedResource: RichTextResourcesDetails? = null
    private var buttonPool: MutableList<ButtonVM> = mutableListOf()
    private var reloadMode = false

    private val link: Link by extra(ARGUMENT_LINK)
    lateinit var publicLink: String

    companion object {
        const val ARGUMENT_LINK = "ARGUMENT_LINK"

        var doRefresh = false

        fun newInstance(context: Context, link: Link): Intent {
            return Intent(context, ExploreItemObjectActivity::class.java).apply {
                putExtra(ARGUMENT_LINK, link)
            }
        }
    }

    @Inject
    lateinit var loginRepository: LoginRepository

    private val indicatorColor by lazy {
        loginRepository.userCompany?.company?.primaryColor
            ?: Integer.toHexString(ContextCompat.getColor(this, R.color.colorAccent))
    }

    override fun onResume() {
        super.onResume()
        if (doRefresh) {
            toolbar.visibility = View.GONE
            llLoading.visibility = View.VISIBLE
            viewModel.fetchObject(link)
            doRefresh = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore_item_object)
        setupObserver()
        setupObserverRichtext()
        setupObserverRichTextDownloadFile()
        setupCreateOpportunity()
        setupPublicLinkObserver()
        setupToolbar()
        configureAppLayout()

        rv_explore_object_rich_text.isVisible = false
        llConfirmButton.isVisible = false
        llLoading.isVisible = true

        viewModel.fetchObject(link)
    }


    private fun configureAppLayout() {
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->


        })

        if (app_bar.layoutParams != null) {
            val layoutParams: CoordinatorLayout.LayoutParams =
                app_bar.layoutParams as CoordinatorLayout.LayoutParams
            val appBarLayoutBehaviour = AppBarLayout.Behavior()
            appBarLayoutBehaviour.setDragCallback(object : DragCallback() {
                override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                    return false
                }

            })
            layoutParams.behavior = appBarLayoutBehaviour
        }

        nestedScroll.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->

            Log.i("scroll", "scrollX: $scrollX scrollY: $scrollY")

            val scrollBounds = Rect()
            nestedScroll.getHitRect(scrollBounds)
            Log.i("scroll", "isVisible: ${levelName.getLocalVisibleRect(scrollBounds)}")

            if (levelName.getLocalVisibleRect(scrollBounds).not()) {
                subtitleHeader.text = levelName.text
                subtitleHeader.isVisible = true
                bottomHeaderDividerCollapsedObject.visibility = View.VISIBLE
                //bottomHeaderToolbar.isVisible = true
            } else {
                subtitleHeader.isVisible = false
                bottomHeaderDividerCollapsedObject.visibility = View.GONE
                //bottomHeaderToolbar.isVisible = false
            }
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupObserver() {
        lifecycleScope.launch {
            val value = viewModel.objectItem()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        Handler().postDelayed({
                            Log.i(
                                "scroll",
                                "height: ${app_bar.height} min height: ${app_bar.minimumHeight}"
                            )
                        }, 500)

                        toolbar.visibility = View.VISIBLE
                        mainContainer.setBackgroundColor(resources.getColor(R.color._eeeff4))
                        //loading.isVisible = false
                        llLoading.isVisible = false
                        // loadingToolbar.isVisible = false
                        nestedScroll.isVisible = true
                        resource.data?.let {
                            if (!reloadMode) {
                                setupData(it)
                                reloadMode = true
                            } else {
                                reloaData(it)
                            }
                        }
                    }
                    Status.LOADING -> {
                        // loading.isVisible = true
                        // loadingToolbar.isVisible = true
                        nestedScroll.isVisible = false
                    }
                    Status.ERROR -> finish()
                    Status.EMPTY -> finish()
                }
            }
        }
    }


    private fun setupPublicLinkObserver() {
        lifecycleScope.launch {
            viewModel.uniqueLink.observe(this@ExploreItemObjectActivity, Observer { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.i("link2", resource.data?.data?.link)
                        resource.data?.data?.link?.let {
                            publicLink = it
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.ERROR -> finish()
                    Status.EMPTY -> finish()
                }
            })
        }
    }

    private fun setupObserverRichtext() {
        lifecycleScope.launch {
            val value = viewModel.richTextAction()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        // loading.isVisible = false

                        val dialogBuilder = AlertDialog.Builder(
                            this@ExploreItemObjectActivity,
                            R.style.TransparentDialog
                        )
                        val dialogView = layoutInflater.inflate(R.layout.dialog_informative, null)
                        dialogView.findViewById<TextView>(R.id.title).text =
                            getString(R.string.object_detail_rich_text_dialog_title)
                        dialogView.findViewById<TextView>(R.id.levelDescription).text =
                            getString(R.string.object_detail_rich_text_dialog_subtitle)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val alertDialog = dialogBuilder.create()

                        dialogView.findViewById<TextView>(R.id.backLogin).setOnClickListener {
                            alertDialog.dismiss()
                            finish()
                        }

                        alertDialog.show()
                    }
                    Status.LOADING -> {
                        // loading.isVisible = true
                    }
                    Status.ERROR -> {
                        // loading.isVisible = false
                        Toast.makeText(
                            this@ExploreItemObjectActivity,
                            getString(R.string.object_detail_rich_text_fail_confirmation),
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                    Status.EMPTY -> {
                        // loading.isVisible = false
                    }
                }
            }
        }
    }

    private fun setupObserverRichTextDownloadFile() {
        lifecycleScope.launch {
            val value = viewModel.richTextDownload()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        llLoading.isVisible = false

                        resource.data?.let {

                            try {
                                val directory = cacheDir
                                val file = File(
                                    directory,
                                    "tempfile.${MimeTypeMap.getFileExtensionFromUrl(selectedResource?.url)}"
                                )
                                FileUtils.createFromInputStream(file, resource.data.byteStream())
                                val uri = FileProvider.getUriForFile(
                                    this@ExploreItemObjectActivity,
                                    BuildConfig.APPLICATION_ID + ".fileprovider",
                                    file
                                )
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.setDataAndType(uri, selectedResource?.getMimeType())
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                startActivity(intent)
                            } catch (ignored: ActivityNotFoundException) {
                                Toast.makeText(
                                    this@ExploreItemObjectActivity,
                                    getString(R.string.activity_not_found),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                    Status.LOADING -> {
                        llLoading.isVisible = true
                    }
                    Status.ERROR -> {
                        llLoading.isVisible = false
                        Toast.makeText(
                            this@ExploreItemObjectActivity,
                            getString(R.string.object_detail_rich_text_fail_download),
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                    Status.EMPTY -> {
                        llLoading.isVisible = false
                    }
                }
            }
        }
    }

    private fun reloaData(objectItem: ObjectItem) {
        setupTools(objectItem)


    }

    private fun setupData(objectItem: ObjectItem) {
        toolbar.title = ""
        objectItem.section?.let {
            toolbar_title.apply {
                visibility = View.VISIBLE
                text = it.title
            }
        }

        buildDescriptionText(objectItem.longDescription)

        snippet.text = objectItem.snippet
        snippet.setTextColor(ColorUtils.stateListOf(indicatorColor))
        levelName.text = objectItem.longTitle
        custom_fields_lead.text = objectItem.customFields


        toolbar.visibility = View.VISIBLE

        objectItem.medias?.let {
            multiMediaPager.bind(it, this)
        }

        rv_explore_object_rich_text.apply {
            layoutManager = LinearLayoutManager(context)
            adapter =
                RichTextAdapter(this@ExploreItemObjectActivity, this@ExploreItemObjectActivity)
        }

        objectItem.richText?.let {
            rv_explore_object_rich_text.isVisible = false

            it.button?.let { button ->
                buttonPool.add(
                    ButtonVM(button) {
                        button.link?.let {
                            viewModel.putRichTextAction(button)
                        }
                    }
                )
            }

            it.formattedItems?.apply {
                if (this.isNotEmpty()) {
                    it.button?.let { button ->
                        buttonPool.add(
                            ButtonVM(button) {
                                button.link?.let {
                                    viewModel.putRichTextAction(button)
                                }
                            }
                        )
                    }
                    rv_explore_object_rich_text.isVisible = true
                    (rv_explore_object_rich_text.adapter as RichTextAdapter).addRichText(this)
                }
            }
        }

        if (objectItem.tools.isNotEmpty()) {
            objectItem.tools.forEach { tool ->
                tool.button?.let { button ->
                    buttonPool.add(
                        ButtonVM(button) {
                            when (tool.type) {
                                RankingType.Oportunidade -> {
                                    button.actions?.form?.enabled?.let { enableForm ->
                                        if (enableForm) {
                                            button.actions?.link?.enabled?.let { enableLink ->
                                                if (enableForm && enableLink) {

                                                    var dialog = ChooseUniqueLinkSheetDialog(
                                                        tool.content.title,
                                                        button
                                                    ) {
                                                        when (it) {
                                                            "link" -> {
                                                                button.actions?.link?.url =
                                                                    publicLink
                                                                UniqueLinkActivity.start(
                                                                    this@ExploreItemObjectActivity,
                                                                    button.actions?.link
                                                                )
                                                            }
                                                            "form" -> {
                                                                viewModelTool.createNewOpportunity(
                                                                    objectItem.id.toLong(),
                                                                    tool
                                                                )
                                                            }
                                                        }
                                                    }
                                                    dialog.show(
                                                        this.supportFragmentManager,
                                                        "choose_unique"
                                                    )


                                                } else if (enableForm) {
                                                    viewModelTool.createNewOpportunity(
                                                        objectItem.id.toLong(),
                                                        tool
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }

                                RankingType.Escala -> {
                                    viewModelTool.setSelectedTool(tool)
                                    WidgetScheduleSummaryActivity
                                        .startForResult(this, tool.id, objectItem.id)
                                }
                                RankingType.Invalid -> {
                                }
                                else -> {
                                }
                            }
                        }
                    )
                }
            }
        }

        setupTools(objectItem)
        setupResources(objectItem)
        setupButtons()

        viewModel.fetchObjectSchedule(objectItem.id.toString())
        viewModel.generatePublicLink(
            UniqueLinkRequest(
                "opportunity",
                objectItem.id.toString(),
                "1"
            )
        )
    }


    private fun setupResources(objectItem: ObjectItem) {
        var links = objectItem.complements.filter {
            it.type == ComplementType.Links
        }

        var resources = objectItem.complements.filter {
            it.type == ComplementType.Resource
        }

        var address = objectItem.complements.filter {
            it.type == ComplementType.Address
        }

        rvResources.visibility =
            if (resources.isEmpty().not() || address.isEmpty().not() || links.isEmpty().not()) {
                View.VISIBLE
            } else {
                View.GONE
            }

        //dividerBottom2.isVisible = resources.isEmpty().not() || links.isEmpty().not()

        val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is AddressSingleViewHolderModel -> AddressSingleViewHolder.ITEM_VIEW_MODEL_ID
                is ResourceSingleViewHolderModel -> ResourceSingleViewHolder.ITEM_VIEW_MODEL_ID
                is ResourceMultipleViewHolderModel -> ResourceMultipleViewHolder.ITEM_VIEW_MODEL_ID
                is PriceSingleViewHolderModel -> PriceSingleViewHolder.ITEM_VIEW_MODEL_ID
                is ContactSingleViewHolderModel -> ContactSingleViewHolder.ITEM_VIEW_MODEL_ID
                is LinkGroupViewHolderModel -> LinkGroupViewHolder.ITEM_VIEW_MODEL_ID
                is ShareViewHolderModel -> ShareViewHolder.VIEW_ITEM_POSITION_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ShareViewHolder.VIEW_ITEM_POSITION_ID -> ShareViewHolder(view) {
                    var links = objectItem.complements.filter {
                        it.type == ComplementType.Links
                    }

                    var url = ""

                    try {
                        if (links.isEmpty().not() &&
                            links[0].links != null &&
                            links[0].links!!.links != null &&
                            links[0].links!!.links!!.isEmpty().not() != null

                        ) {
                            url = links[0].links?.links?.get(0)?.link!!
                        }
                    } catch (e: Exception) {

                    }

                    Log.i("url", url)

                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_TEXT, objectItem.longTitle + " link: " + url)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(intent)
                }

                ResourceSingleViewHolder.ITEM_VIEW_MODEL_ID -> ResourceSingleViewHolder(view, {
                    ComplementsListingActivity.start(
                        context = this@ExploreItemObjectActivity,
                        elementType = COMPLEMENT_LIST_TYPE.RESOURCES,
                        elements = it.resource?.resources!!,
                        title = it.resource?.title!!,
                        subTitle = it.resource?.description!!
                    )
                }, {

                    var links = objectItem.complements.filter {
                        it.type == ComplementType.Links
                    }

                    var url = ""

                    try {
                        if (links.isEmpty().not() &&
                            links[0].links != null &&
                            links[0].links!!.links != null &&
                            links[0].links!!.links!!.isEmpty().not() != null

                        ) {
                            url = links[0].links?.links?.get(0)?.link!!
                        }
                    } catch (e: Exception) {

                    }

                    Log.i("url", url)

                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_TEXT, objectItem.longTitle + " link: " + url)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(intent)
                })

                LinkGroupViewHolder.ITEM_VIEW_MODEL_ID -> LinkGroupViewHolder(view) {
                    ComplementsListingActivity.start(
                        context = this@ExploreItemObjectActivity,
                        elementType = COMPLEMENT_LIST_TYPE.LINKS,
                        elements = it.links!!,
                        title = it.title!!,
                        subTitle = it.description!!
                    )
                }

                AddressSingleViewHolder.ITEM_VIEW_MODEL_ID -> AddressSingleViewHolder(view) {

                    Log.i("url", "${Gson().toJson(objectItem?.medias)}")
                    var image = objectItem?.medias?.filter { media ->
                        media.mimeType == "image/png" || media.mimeType == "image/jpg"
                    }


                    ExploreAddressDetailActivity.start(
                        this@ExploreItemObjectActivity,
                        it,
                        objectItem.short_title
                            ?: "",
                        objectItem?.section?.icon?.url!!,
                        if (image?.isEmpty()?.not()!!) image?.get(0)?.url else ""
                    )
                }

                PriceSingleViewHolder.ITEM_VIEW_MODEL_ID -> PriceSingleViewHolder(view) { comp ->

                }
                ContactSingleViewHolder.ITEM_VIEW_MODEL_ID -> ContactSingleViewHolder(view) {
                    val dialog = ContactOptionsDialog.newInstance(it)
                    dialog.show(supportFragmentManager, "complement_email_phone_options")
                }
                ResourceMultipleViewHolder.ITEM_VIEW_MODEL_ID -> ResourceMultipleViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })

        rvResources.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = factoryAdapter.apply {

                var itens = mutableListOf<ViewModel>()

                objectItem.complements.forEach {
                    when (it.type) {
                        ComplementType.Links -> itens.add(LinkGroupViewHolderModel(it.links!!))
                        ComplementType.Resource -> itens.add(ResourceSingleViewHolderModel(it))
                        ComplementType.Address -> itens.add(AddressSingleViewHolderModel(it.address!!))
                        ComplementType.Price -> itens.add(
                            PriceSingleViewHolderModel(
                                it.price!!,
                                it
                            )
                        )
                        ComplementType.Contact -> itens.add(ContactSingleViewHolderModel(it.contact!!))
                        else -> run {}
                    }
                }

                itens.add(ShareViewHolderModel(""))

                setItems(itens.toList())
            }

        }
    }

    private fun setupTools(objectItem: ObjectItem) {
        objectItem.tools.let { tools ->


            if (tools.isNotEmpty()) {
                rvTools.isVisible = true

                rvTools.adapter = RecyclerViewAdapter(object : ViewHolderFactory {
                    override fun getType(viewModel: ViewModel) = when (viewModel) {
                        is ToolsViewHolderModel -> TOOLS_ADAPTER_VIEW_ID
                        is DividerViewHolderModel -> TOOLS_DIVIDER_VIEW_ID
                        else -> throw IllegalArgumentException()
                    }

                    override fun getHolder(viewType: Int, view: View): ViewHolder<*> =
                        when (viewType) {
                            TOOLS_ADAPTER_VIEW_ID -> ToolsViewHolder(
                                view, objectItem.section?.title!!, objectItem.id, App
                                    .companyColor,
                                onNewSubmissionCallback = {
                                    viewModel.fetchObject(link)
                                },
                                onNewEscalaCallback = { tool ->
                                    viewModel.objectItem().value.data?.let { objectItem ->
                                        viewModelTool.setSelectedTool(tool)
                                        WidgetScheduleSummaryActivity
                                            .startForResult(
                                                this@ExploreItemObjectActivity,
                                                tool.id,
                                                objectItem.id
                                            )
                                    }
                                },
                                onNewOpportunity = { tool ->
                                    var button = tool.button!!
                                    button.actions?.form?.enabled?.let { enableForm ->
                                        if (enableForm) {
                                            button.actions?.link?.enabled?.let { enableLink ->
                                                if (enableForm && enableLink) {
                                                    var dialog = ChooseUniqueLinkSheetDialog(
                                                        objectItem.section?.title
                                                            ?: "", button
                                                    ) {
                                                        when (it) {
                                                            "link" -> {
                                                                button.actions?.link?.url =
                                                                    publicLink
                                                                UniqueLinkActivity.start(
                                                                    this@ExploreItemObjectActivity,
                                                                    button.actions?.link!!
                                                                )
                                                            }
                                                            "form" -> {
                                                                viewModelTool.createNewOpportunity(
                                                                    objectItem.id.toLong(),
                                                                    tool
                                                                )
                                                            }
                                                        }
                                                    }
                                                    dialog.show(
                                                        this@ExploreItemObjectActivity.supportFragmentManager,
                                                        "choose_unique"
                                                    )


                                                } else if (enableForm) {
                                                    viewModelTool.createNewOpportunity(
                                                        objectItem.id.toLong(),
                                                        tool
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }

                            )
                            TOOLS_DIVIDER_VIEW_ID
                            -> DividerViewHolder(view)

                            else -> throw IllegalArgumentException()
                        }
                })

                var itens = mutableListOf<ViewModel>()
                objectItem.tools.forEachIndexed { index, tool ->
                    itens.add(ToolsViewHolderModel(tool))
                    if (objectItem.tools.isNotEmpty()) {
                        itens.add(DividerViewHolderModel(index == objectItem.tools.size - 1))
                    }
                }

                (rvTools.adapter as RecyclerViewAdapter).addItems(itens)
            } else {
                rvTools.isVisible = false
                dividerItemTop.isVisible = false
                dividerBottom2l2.isVisible = false
            }
        }
    }

    private fun setupButtons() {
        if (buttonPool.isEmpty()) {
            return
        }

        llConfirmButton.isVisible = true
        // dividerBottom.isVisible = true

        llConfirmButton.addView(
            when (buttonPool.size) {
                1 -> setupOneButton(buttonPool.first())
                2 -> setupTwoButtons(buttonPool)
                3 -> setupThreeButtons(buttonPool)
                else -> setupMoreThanThreeButtons(buttonPool)
            }
        )
    }

    private fun setupMoreThanThreeButtons(buttons: List<ButtonVM>): View {
        return setupThreeButtons(buttons) // TODO
    }

    private fun setupThreeButtons(buttons: List<ButtonVM>): View {
        var view = layoutInflater.inflate(R.layout.item_explore_bottom_button_3, null)
        setupButton(
            view.findViewById(R.id.rlButton1),
            view.findViewById(R.id.btConfirm1),
            buttons[0]
        )
        setupButton(
            view.findViewById(R.id.rlButton2),
            view.findViewById(R.id.btConfirm2),
            buttons[1]
        )
        setupButton(
            view.findViewById(R.id.rlButton3),
            view.findViewById(R.id.btConfirm3),
            buttons[2]
        )
        return view
    }

    private fun setupTwoButtons(buttons: List<ButtonVM>): View {
        var view = layoutInflater.inflate(R.layout.item_explore_bottom_button_2, null)

        var btnView1: View = view.findViewById(R.id.btConfirm1)
        var buttonVM1 = buttons[0]

        when (btnView1) {
            is Button -> setupButton(view.findViewById(R.id.rlButton1), btnView1, buttonVM1)
            is TextView -> setupButton(
                view.findViewById(R.id.containerButton),
                view.findViewById(R.id.rlButton1),
                btnView1,
                buttonVM1
            )
        }

        var btnView2: View = view.findViewById(R.id.btConfirm2)
        var buttonVM2 = buttons[1]

        when (btnView2) {
            is Button -> setupButton(view.findViewById(R.id.rlButton2), btnView2, buttonVM2)
            is TextView -> setupButton(
                view.findViewById(R.id.containerButton),
                view.findViewById(R.id.rlButton2),
                btnView2,
                buttonVM2
            )
        }

        return view
    }

    private fun setupOneButton(buttonVM: ButtonVM): View {
        var view = layoutInflater.inflate(R.layout.item_explore_bottom_button_1, null)

        var btnView: View = view.findViewById(R.id.btConfirm1)

        when (btnView) {
            is Button -> setupButton(view.findViewById(R.id.rlButton1), btnView, buttonVM)
            is TextView -> setupButton(
                view.findViewById(R.id.containerButton),
                view.findViewById(R.id.rlButton1),
                btnView,
                buttonVM
            )
        }
        return view
    }

    private fun setupButton(
        constraint: ConstraintLayout,
        relativeLayout: RelativeLayout,
        button: TextView,
        buttonVM: ButtonVM
    ) {
        button.text = buttonVM.button.text
        relativeLayout.setOnClickListener(buttonVM.onClickListener)
        constraint.setOnClickListener(buttonVM.onClickListener)

        val params = RelativeLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            addRule(RelativeLayout.CENTER_VERTICAL)
            addRule(RelativeLayout.CENTER_HORIZONTAL)
        }

        button.layoutParams = params

        buttonVM.button.icon?.url?.let {
            try {
                Glide.with(this)
                    .asBitmap()
                    .apply(RequestOptions().override(64, 64))
                    .load(it)
                    .into(object : CustomTarget<Bitmap>() {

                        override fun onLoadCleared(placeholder: Drawable?) = Unit

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            val drawable = BitmapDrawable(resources, resource)

                            drawable.setBounds(0, 0, resource.width - 15, resource.height - 15)

                            button.setCompoundDrawables(
                                drawable,
                                null,
                                null,
                                null
                            )
                        }
                    })
            } catch (e: Exception) {
                Log.i("pdv", e.message, e)
            }

            button.setOnClickListener(buttonVM.onClickListener)
        }
    }

    private fun setupButton(relativeLayout: RelativeLayout, button: Button, buttonVM: ButtonVM) {
        button.text = buttonVM.button.text
        button.typeface = Typeface.createFromAsset(this.assets, "fonts/roboto_bold.ttf")

        button.setOnClickListener { buttonVM.onClickListener.invoke(it) }
        relativeLayout.setOnClickListener { buttonVM.onClickListener.invoke(it) }

        val params = RelativeLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            addRule(RelativeLayout.CENTER_VERTICAL)
            addRule(RelativeLayout.CENTER_HORIZONTAL)
        }

        button.layoutParams = params

        buttonVM.button.icon?.url?.let {
            try {
                Glide.with(this)
                    .asBitmap()
                    .load(it)
                    .apply(RequestOptions().override(64, 64))
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) = Unit

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {

                            val drawable = BitmapDrawable(resources, resource)

                            drawable.setBounds(0, 0, resource.width, resource.height)

                            button.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                drawable,
                                null,
                                null,
                                null
                            )
                        }
                    })
            } catch (e: Exception) {

            }
        }
    }

    private fun setupCreateOpportunity() {
        lifecycleScope.launch {
            viewModelTool.opportunityWidgetCreation().collect {
                when (it.status) {
                    Status.LOADING -> {
                        // llLoading.isVisible = true
                    }
                    Status.SUCCESS -> {
                        llLoading.isVisible = false
                        it.data?.apply {
                            LeadActivity.startForResult(
                                this@ExploreItemObjectActivity,
                                this,
                                LeadActivity.REQUEST_LEAD_OPTIONS,
                                toolbar_title.text.toString()
                            )
                        }
                    }
                    Status.ERROR -> {
                        llLoading.isVisible = false
                    }
                    Status.EMPTY -> {
                        llLoading.isVisible = false
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            LeadActivity.REQUEST_LEAD_OPTIONS -> {
                when (resultCode) {
                    LeadActivity.RESULT_CREATE_NEW_OPPORTUNITY -> {
                        viewModel.objectItem().value.data?.let { objectItem ->
                            viewModelTool.selectedTool().data?.let { tool ->
                                viewModelTool.createNewOpportunity(objectItem.id, tool)
                            }
                        }
                    }
                    LeadActivity.RESULT_RELOAD_OPPORTUNITY -> {
                        viewModel.fetchObject(link)
                    }
                }
            }
            WidgetScheduleSummaryActivity.REQUEST_CODE -> {
                when (resultCode) {
                    WidgetScheduleSummaryActivity.RESULT_CREATE_NEW_RESERVE -> {
                        viewModel.objectItem().value.data?.let { objectItem ->
                            viewModelTool.selectedTool().data?.let { tool ->
                                viewModelTool.setSelectedTool(tool)
                                WidgetScheduleSummaryActivity
                                    .startForResult(
                                        this@ExploreItemObjectActivity,
                                        tool.id,
                                        objectItem.id
                                    )
                            }
                        }
                    }
                    WidgetScheduleSummaryActivity.RESULT_RELOAD_RESERVE -> {
                        viewModel.fetchObject(link)
                    }
                }
            }
        }
    }

    private fun findWordPosition(text: String): Int {
        var words = text.split("\\s+".toRegex())
        var nn = if (words.size > 25) {
            25
        } else {
            words.size - 1
        }
        return text.indexOf(words[nn])
    }

    fun getVisibleText(textView: TextView?): String? {
        // test that we have a textview and it has text
        if (textView == null || TextUtils.isEmpty(textView.text)) return null
        val l: Layout? = textView.layout
        if (l != null) {
            // find the last visible position
            val end: Int = l.getLineEnd(2)
            // get only the text after that position
            return textView.text.toString().substring(0, end)
        }
        return null
    }


    fun makeTextViewResizable(tv: TextView, maxLine: Int, expandText: String) {
        if (tv.tag == null) {
            tv.tag = tv.text
        }
        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val obs = tv.viewTreeObserver
                obs.removeGlobalOnLayoutListener(this)
                if (maxLine <= 0) {
                    val lineEndIndex = tv.layout.getLineEnd(0)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                } else if (tv.lineCount >= maxLine) {
                    val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                }
            }
        })
    }


    private fun buildDescriptionText(longDescription: String?) {

        // makeTextViewResizable(fullDescription, 3, "ver mais")
        longDescription?.let { it ->

            fullDescription.text = it
            fullDescriptionExpandable.text = it

            fullDescription.setOnClickListener {

                fullDescription.visibility = View.GONE
                fullDescriptionExpandable.visibility = View.VISIBLE

                fullDescriptionExpandable.apply {

                    var collapsedHeight = this.measuredHeight

                    this.measure(
                        MeasureSpec.makeMeasureSpec(this.measuredWidth, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                    )

                    // set maxLines to MAX Integer, so we can calculate the expanded height

                    // set maxLines to MAX Integer, so we can calculate the expanded height
                    this.maxLines = Int.MAX_VALUE

                    // measure expanded height

                    // measure expanded height
                    measure(
                        MeasureSpec.makeMeasureSpec(this.measuredWidth, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                    )

                    val expandedHeight = this.measuredHeight

                    // animate from collapsed height to expanded height

                    // animate from collapsed height to expanded height
                    val valueAnimator = ValueAnimator.ofInt(collapsedHeight, expandedHeight)
                    valueAnimator.addUpdateListener { animation ->
                        this.height = animation.animatedValue as Int
                    }

                    valueAnimator.interpolator = AccelerateDecelerateInterpolator();

                    // start the animation
                    valueAnimator
                        .setDuration(500)
                        .start()
                }

            }


//            status.post {
//                if (status.lineCount > 3) {
//                    status.text = buildSpannedString {
//                        append(it.substring(0, position) + "...")
//                        color(
//                                ContextCompat.getColor(
//                                        this@ExploreItemObjectActivity,
//                                        R.color._c1c2c3
//                                )
//                        ) {
//                            append(getString(R.string.view_more))
//                        }
//                    }
//                    status.isVisible = true
//                    status.setOnClickListener {
//                        fullDescription.visibility = View.VISIBLE
//                        fullDescription.expand()
//                        status.isVisible = false
//                    }
//                }
//            }
        }
    }

    override fun onRichTextLinkClicked(link: RichTextLinkDetails) {
        link.url?.let {
            startActivity(
                WebViewActivity.newInstance(
                    this,
                    it,
                    link.title?.run { this } ?: ""
                )
            )
        }
    }

    override fun onRichTextObjectClicked(link: Link) {
        link.url?.let {
            startActivity(newInstance(this, link))
        }
    }

    override fun onRichTextResourceClicked(resource: RichTextResourcesDetails) {
        selectedResource = resource
        resource.url?.apply {
            viewModel.getFileRichTextResource(this)
        } ?: run {
            Toast.makeText(
                this,
                getString(R.string.object_detail_rich_text_fail_mime),
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
