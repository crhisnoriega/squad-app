package co.socialsquad.squad.presentation.feature.points.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.points.model.AreaData

class ItemAreaPointsViewModel(
    val areaData: AreaData
) : ViewModel {
}