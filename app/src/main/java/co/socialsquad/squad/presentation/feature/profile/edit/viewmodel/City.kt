package co.socialsquad.squad.presentation.feature.profile.edit.viewmodel

import co.socialsquad.squad.presentation.custom.InputSpinner

class City(var code: Long, var name: String?) : InputSpinner.Item {
    override val title: String? = name
}
