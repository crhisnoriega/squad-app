package co.socialsquad.squad.presentation.feature.immobiles

import co.socialsquad.squad.data.repository.ImmobilesRepositoryImpl
import co.socialsquad.squad.data.repository.LoginRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ImmobilesPresenter @Inject constructor(
    val repository: ImmobilesRepositoryImpl,
    val loginRepository: LoginRepository,
    val view: ImmobilesView
) {

    private var isLoading = false
    val compositeDisposable = CompositeDisposable()
    var currentPage = 1

    fun immobiles() {
        if (!isLoading) {
            isLoading = true
            compositeDisposable.add(
                repository.immobiles(currentPage)
                    .doOnSubscribe {
                        view.showLoading()
                    }.doOnTerminate {
                        view.hideLoading()
                        isLoading = false
                    }.subscribe(
                        {

                            currentPage++
                            view.setupImmobiles(it.results, loginRepository.userCompany?.coverColor, loginRepository.squadColor)
                        },
                        {
                            it.printStackTrace()
                        }
                    )
            )
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
