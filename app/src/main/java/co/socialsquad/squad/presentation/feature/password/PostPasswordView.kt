package co.socialsquad.squad.presentation.feature.password

import co.socialsquad.squad.data.entity.Company

interface PostPasswordView {

    fun setErrorMessage(resId: Int)
    fun showLoading()
    fun hideLoading()
    fun enableNextButton(boolean: Boolean)
    fun setTerms(terms: String)
    fun setupHeader(company: Company?)
}
