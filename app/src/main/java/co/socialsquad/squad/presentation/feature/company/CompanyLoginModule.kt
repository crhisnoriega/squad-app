package co.socialsquad.squad.presentation.feature.company

import dagger.Binds
import dagger.Module

@Module
abstract class CompanyLoginModule {
    @Binds
    abstract fun view(companyLoginActivity: CompanyLoginActivity): CompanyLoginView
}
