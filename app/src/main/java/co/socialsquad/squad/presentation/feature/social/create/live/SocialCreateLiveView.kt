package co.socialsquad.squad.presentation.feature.social.create.live

import android.content.Intent
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel

interface SocialCreateLiveView {
    fun setupToolbar(titleResId: Int)
    fun setupTitle(title: String)
    fun setupDescription(description: String)
    fun setupCategory(liveCategory: LiveCategoryViewModel)
    fun setupStartButton(textResId: Int)
    fun setupAudience(audience: AudienceViewModel)
    fun showCategories(liveCategoryViewModels: List<LiveCategoryViewModel>)
    fun openLiveCreator(liveCreateRequest: LiveCreateRequest)
    fun setStartButtonEnabled(enabled: Boolean)
    fun showErrorMessage(textResId: Int)
    fun showLoading()
    fun hideLoading()
    fun finishWithResult(intent: Intent? = null)
    fun hideSchedule()
    fun setupAudienceColor(squadColor: String?)
}
