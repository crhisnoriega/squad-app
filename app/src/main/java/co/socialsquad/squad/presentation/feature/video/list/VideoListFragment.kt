package co.socialsquad.squad.presentation.feature.video.list

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_video_list.*
import javax.inject.Inject

class VideoListFragment : Fragment(), VideoListView {

    @Inject
    lateinit var presenter: VideoListPresenter

    private var adapter: RecyclerViewAdapter? = null

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_video_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()
        setupSwipeRefresh()
        setupList()
        presenter.onCreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupToolbar() {
        setHasOptionsMenu(true)
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener {
            srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            presenter.onRefresh()
        }
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is VideoListItemViewModel -> VIDEO_LIST_ITEM_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                VIDEO_LIST_ITEM_VIEW_MODEL_ID -> VideoListItemViewHolder(view, onVideoClickedListener, onRecommendListener)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        })

        rv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@VideoListFragment.adapter
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    fun onRecommendSelected(viewModel: VideoListItemViewModel) {
        val segmentName = getString(R.string.qualification_title)
        val metric = MetricViewModel("qualification", segmentName, listOf<ValueViewModel>(), true, null)
        val intent = Intent(activity, SegmentValuesActivity::class.java)
        intent.putExtra(EXTRA_METRICS, metric)
        presenter.onRecommending(viewModel)
        startActivityForResult(intent, SegmentValuesPresenter.REQUEST_CODE_VALUES)
    }

    private val onRecommendListener = object : ViewHolder.Listener<VideoListItemViewModel> {
        override fun onClick(viewModel: VideoListItemViewModel) {
            onRecommendSelected(viewModel)
        }
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<VideoListItemViewModel> {
        override fun onClick(viewModel: VideoListItemViewModel) {
            presenter.onVideoClicked(viewModel)
        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun setItems(items: List<VideoListItemViewModel>) {
        adapter?.setItems(items)
    }

    override fun addItems(items: List<VideoListItemViewModel>) {
        adapter?.addItems(items)
    }

    override fun startLoading() {
        adapter?.startLoading()
        if (!srl.isRefreshing) srl.isEnabled = false
    }

    override fun stopLoading() {
        adapter?.stopLoading()
        srl.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun startRefreshing() {
        srl.isRefreshing = true
    }

    override fun stopRefreshing() {
        srl.isRefreshing = false
    }

    override fun showEmptyState() {
        srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.VISIBLE
    }
}
