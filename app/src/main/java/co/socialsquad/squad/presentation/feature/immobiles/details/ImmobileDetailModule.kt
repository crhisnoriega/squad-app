package co.socialsquad.squad.presentation.feature.immobiles.details

import dagger.Binds
import dagger.Module

@Module
abstract class ImmobileDetailModule {

    @Binds
    abstract fun view(view: ImmobilesDetailsActivity): ImmobilesDetailsView
}
