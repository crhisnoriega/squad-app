package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.item_ranking_show_all.view.*

class ItemShowAllRecognitionViewHolder(itemView: View, var onClick: ((model: ViewModel) -> Unit)?) : ViewHolder<ItemShowAllRecognitionViewModel>(itemView) {

    override fun bind(viewModel: ItemShowAllRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClick?.invoke(viewModel)
        }
    }

    fun setText(text1: String) {
        itemView.txtTitle.text = text1
    }

    override fun recycle() {
    }
}