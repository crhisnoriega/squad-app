package co.socialsquad.squad.presentation.feature.existinglead.adapter.searching

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class LeadSearchingAdapter(val searchTerm: String) : RecyclerView.Adapter<LeadSearchingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeadSearchingViewHolder {
        return LeadSearchingViewHolder(parent)
    }

    override fun onBindViewHolder(holder: LeadSearchingViewHolder, position: Int) = holder.bind(searchTerm)

    override fun getItemCount(): Int = 1
}
