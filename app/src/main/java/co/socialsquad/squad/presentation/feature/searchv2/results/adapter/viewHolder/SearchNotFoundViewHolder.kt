package co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import com.bumptech.glide.Glide

class SearchNotFoundViewHolder(itemView: View, val context: Context) :
        RecyclerView.ViewHolder(itemView) {

    internal fun bind(query: String) {
        itemView.findViewById<TextView>(R.id.tvDescription)?.text = itemView.resources.getString(R.string.search_no_results_description, query)

    }
}
