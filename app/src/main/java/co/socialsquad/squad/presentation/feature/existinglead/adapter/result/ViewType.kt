package co.socialsquad.squad.presentation.feature.existinglead.adapter.result

import android.os.Parcelable

interface ViewType : Parcelable {
    fun getViewType(): Int
}
