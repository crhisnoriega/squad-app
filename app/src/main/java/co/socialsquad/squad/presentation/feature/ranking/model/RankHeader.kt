package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RankHeader(
        @SerializedName("id") val id: Int?,
        @SerializedName("position_string") val position_string: String?,
        @SerializedName("ranking") val ranking: Int?,
        @SerializedName("score") val score: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("subtitle") val subtitle: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("tiebreaker") val tiebreaker: Int?,
        @SerializedName("user") val user: UserPositionData?,
        var first: Boolean = false
) : Parcelable