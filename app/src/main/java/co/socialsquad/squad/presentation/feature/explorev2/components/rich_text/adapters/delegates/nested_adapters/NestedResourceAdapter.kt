package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.nested_adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextResourcesDetails
import co.socialsquad.squad.presentation.util.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_rich_text_nested_resource.view.*

class NestedResourceAdapter(private val listener: RichTextListener, private val list: List<RichTextResourcesDetails>?) :
    RecyclerView.Adapter<NestedResourceAdapter.NestedResourceViewHolder>() {

    inner class NestedResourceViewHolder(private val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_rich_text_nested_resource)) {

        val tvResourceTitle: TextView = itemView.tv_item_rich_text_nested_title
        val tvResourceDescription: TextView = itemView.tv_item_rich_text_nested_description
        val ivIcon: ImageView = itemView.iv_item_rich_text_nested_media
        val container: ViewGroup = itemView.cl_item_rich_text_nested_resource_container

        fun bind(richTextResourcesDetails: RichTextResourcesDetails) {
            if (richTextResourcesDetails.title.isNullOrBlank()) {
                tvResourceTitle.visibility = View.GONE
            } else {
                tvResourceTitle.text = richTextResourcesDetails.title
            }

            if (richTextResourcesDetails.description.isNullOrBlank()) {
                tvResourceDescription.visibility = View.GONE
            } else {
                tvResourceDescription.text = richTextResourcesDetails.description
            }

            Glide.with(parent.context).load(richTextResourcesDetails.icon?.url).centerInside()
                .into(ivIcon)
            container.setOnClickListener {
                listener.onRichTextResourceClicked(richTextResourcesDetails)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NestedResourceViewHolder {
        return NestedResourceViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: NestedResourceViewHolder, position: Int) {
        list?.get(position)?.let { holder.bind(it) }
    }
}
