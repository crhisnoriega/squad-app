package co.socialsquad.squad.presentation.custom.tool.viewHolder

import android.util.Log
import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.inflate
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_timetable.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat

class OpportunityTimetableViewHolder(
        override val containerView: View,
        val companyColor: CompanyColor?,
) :
        ViewHolder<OpportunityTimetableViewHolderModel>(containerView), LayoutContainer {

    private val dateformatHour: DateFormat = SimpleDateFormat("HH:mm")

    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_timetable

        fun newInstance(parent: ViewGroup, companyColor: CompanyColor?) =
                OpportunityTimetableViewHolder(parent.inflate(ITEM_VIEW_MODEL_ID), companyColor)
    }

    var onSubmissionClickListener: (submission: Timetable) -> Unit = {}

    override fun bind(viewModel: OpportunityTimetableViewHolderModel) {
        Log.i("tag1", Gson().toJson(viewModel.timetable))
        with(containerView) {
            setOnClickListener {
                onSubmissionClickListener(viewModel.timetable)
            }

            Glide.with(context)
                    .load(viewModel.timetable.icon?.url)
                    .crossFade()
                    .into(img_lead)

            var dateformatLine1: DateFormat? = null
            try {
                dateformatLine1 = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM', 'YYYY")
            } catch (e: Exception) {
                try {
                    dateformatLine1 = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM', 'yyyy")
                } catch (e: Exception) {
                    dateformatLine1 = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM")
                }
            }


            lastDivider.isVisible = viewModel.isLast.not()

            lblTimetableTitle.text = viewModel.timetable.title
            lblTimetableDate1.text = dateformatLine1?.format(viewModel.timetable.startDatetime)
            lblTimetableDate2.text = "${viewModel.timetable.shift} - " +
                    "${dateformatHour.format(viewModel.timetable.startDatetime)} - " +
                    "${dateformatHour.format(viewModel.timetable.endDatetime)}"

        }
    }

    override fun recycle() {}
}
