package co.socialsquad.squad.presentation.feature.immobiles

import dagger.Binds
import dagger.Module

@Module
abstract class ImmobilesModule {
    @Binds
    abstract fun view(immobilesFragment: ImmobilesFragment): ImmobilesView
}
