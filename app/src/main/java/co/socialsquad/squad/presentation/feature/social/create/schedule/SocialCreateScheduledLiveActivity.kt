package co.socialsquad.squad.presentation.feature.social.create.schedule

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputPromotionalVideo
import co.socialsquad.squad.presentation.custom.InputVideo
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.DateTimeUtils
import co.socialsquad.squad.presentation.util.bitmapCrossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.activity_social_create_schedule_live.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.TimeZone
import javax.inject.Inject

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_CODE_PICK_VIDEO = 2

class SocialCreateScheduledLiveActivity : BaseDaggerActivity(), SocialCreateScheduledLiveView, InputView.OnInputListener {

    companion object {
        const val RESULT_CODE_SCHEDULE = 12
        const val SCHEDULE_START_DATE = "SCHEDULE_START_DATE"
        const val SCHEDULE_END_DATE = "SCHEDULE_END_DATE"
        const val SCHEDULE_FEATURE_IMAGE = "SCHEDULE_FEATURE_IMAGE"
        const val SCHEDULE_FEATURE_VIDEO = "SCHEDULE_FEATURE_VIDEO"
        const val SCHEDULE_LIVE_REQUEST = "SCHEDULE_LIVE_REQUEST"
    }

    @Inject
    lateinit var socialCreateSchedulePresenter: SocialCreateSchedulePresenter

    private var miSave: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_create_schedule_live)
        socialCreateSchedulePresenter.onCreate(intent)
        setupOnclickListener()
        setInputViews(listOf(itDate, itStartTime, itEndTime))
    }

    private fun setupOnclickListener() {
        iiPicture.setOnClickListener {
            socialCreateSchedulePresenter.onPromotionalImageClicked()
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
            startActivityForResult(Intent.createChooser(intent, null), REQUEST_CODE_PICK_IMAGE)
        }

        ivPromotionalVideo.setListener(object : InputVideo.Listener {
            override fun onClick() {
                socialCreateSchedulePresenter.onPromotionalVideoClicked()
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "video/mp4"
                startActivityForResult(intent, REQUEST_CODE_PICK_VIDEO)
            }
        })

        ipvPromotionalVideo.listener = object : InputPromotionalVideo.Listener {
            override fun onRemoveClicked() {
                ipvPromotionalVideo.imageUrl = ""
                ivPromotionalVideo.visibility = View.VISIBLE
                ipvPromotionalVideo.visibility = View.GONE
                socialCreateSchedulePresenter.clearPromotionalVideo()
                socialCreateSchedulePresenter.onInput(itDate.calendar, itStartTime.date, itEndTime.date)
            }

            override fun onPlayClicked() {
                socialCreateSchedulePresenter.openPlayer()
            }
        }
        ipvPromotionalVideo.setOnClickListener {
            socialCreateSchedulePresenter.onPromotionalVideoEditClicked()
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "video/mp4"
            startActivityForResult(intent, REQUEST_CODE_PICK_VIDEO)
        }
    }

    override fun setupViews(backgroundColor: String?, scheduleStartDate: Date?, scheduleEndDate: Date?) {
        iiPicture.badge.text = formatBadgeText(Date())
        itDate.setMinDate(System.currentTimeMillis() - 1000)
        if (!backgroundColor.isNullOrEmpty())
            iiPicture.badge.background.setColorFilter(Color.parseColor(backgroundColor), PorterDuff.Mode.SRC_ATOP)

        scheduleStartDate?.let {
            val start = Calendar.getInstance()
            start.time = scheduleStartDate
            itDate.calendar = start
            itStartTime.calendar = start
            scheduleEndDate?.let { endDate ->
                val end = Calendar.getInstance()
                end.time = endDate
                itEndTime.calendar = end
            }
            setupPromotionalVideoValues(start, it, scheduleEndDate)
        }
    }

    private fun formatBadgeText(date: Date): String {
        val sdf = DateFormat.getDateInstance(DateFormat.SHORT) as SimpleDateFormat
        sdf.applyPattern(sdf.toPattern().replace("/y", "").replace("y", ""))
        return sdf.format(date)
    }

    override fun setupFeaturedImage(imageURI: Uri) {
        Glide.with(this)
                .asBitmap()
                .load(imageURI)
                .roundedCorners(this)
                .bitmapCrossFade()
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, isFirstResource: Boolean) = false
                    override fun onResourceReady(resource: Bitmap, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        socialCreateSchedulePresenter.onResourceReady(resource)
                        return false
                    }
                })
                .into(iiPicture.imageView as ImageView)
    }

    override fun setupToolbar(titleResId: Int) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(titleResId)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_social_create_live_scheduled, menu)
        miSave = menu?.findItem(R.id.social_create_live_mi_start)
        miSave?.setOnMenuItemClickListener {
            if (itDate.calendar != null && itStartTime.date != null) {
                socialCreateSchedulePresenter.saveSchedule(itDate.calendar!!, itStartTime.date!!, itEndTime.date)
            }
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_PICK_IMAGE -> if (resultCode == RESULT_OK) {
                data?.let {
                    setupFeaturedImage(it.data!!)
                    socialCreateSchedulePresenter.onInput(itDate.calendar, itStartTime.date, itEndTime.date)
                }
            }
            REQUEST_CODE_PICK_VIDEO -> if (resultCode == RESULT_OK) {
                data?.data?.let {
                    socialCreateSchedulePresenter.onPromotionalVideoSelected(it)
                    setupPromotionalVideo(it)
                    socialCreateSchedulePresenter.onInput(itDate.calendar, itStartTime.date, itEndTime.date)
                }
            }
        }
    }

    override fun setupPromotionalVideo(videoUri: Uri) {
        ipvPromotionalVideo.imageUrl = videoUri.toString()
        ivPromotionalVideo.visibility = View.GONE
        ipvPromotionalVideo.visibility = View.VISIBLE
    }

    override fun setupPromotionalVideoValues(date: Calendar?, startTime: Date?, endTime: Date?) {
        date?.let {
            ipvPromotionalVideo.scheduledDay = DateUtils.formatDateTime(this, date.time.time, DateUtils.FORMAT_SHOW_YEAR)
        }
        var timeString = ""
        startTime?.let { timeString = android.text.format.DateFormat.getTimeFormat(this).format(startTime) }
        endTime?.let { timeString += " - " + android.text.format.DateFormat.getTimeFormat(this).format(it) }
        startTime?.let { ipvPromotionalVideo.scheduledTime = timeString + " " + Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.LONG) }
    }

    override fun setFinishWithResult(intent: Intent) {
        setResult(RESULT_CODE_SCHEDULE, intent)
        finish()
    }

    override fun enableButton(conditionsMet: Boolean) {
        miSave?.isEnabled = conditionsMet
    }

    override fun onInput() {
        itDate.calendar?.let {
            itDate.isValid(!DateTimeUtils.beforeToday(it))
            iiPicture.badge.text = formatBadgeText(it.time)
        }
        socialCreateSchedulePresenter.onInput(itDate.calendar, itStartTime.date, itEndTime.date)
    }

    override fun openVideoActivity(videoUri: Uri) {
        val intentVideo = Intent(this, VideoActivity::class.java)
        intentVideo.putExtra(VideoActivity.EXTRA_VIDEO_URL, videoUri.toString())
        startActivity(intentVideo)
    }
}
