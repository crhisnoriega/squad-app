package co.socialsquad.squad.presentation.feature.settings.about

import android.os.Bundle
import android.text.format.DateUtils
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.activity_settings_about.*

class SettingsAboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_about)
        setupToolbar()
        setupTexts()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        title = getString(R.string.settings_about_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupTexts() {
        setupVersion()
        setupMessage()
    }

    private fun setupVersion() {
        tvVersion.text = BuildConfig.VERSION_NAME
    }

    private fun setupMessage() {
        val date =
            DateUtils.formatDateTime(
                this,
                BuildConfig.BUILD_TIMESTAMP,
                DateUtils.FORMAT_NUMERIC_DATE or DateUtils.FORMAT_SHOW_YEAR
            )
        val message = getString(R.string.settings_about_message, getAmbient(), date)
        tvMessage.text = message
    }

    private fun getAmbient(): String {
        return when (BuildConfig.FLAVOR) {
            "production" -> getString(R.string.settings_about_production)
            else -> getString(R.string.settings_about_dev)
        }
    }
}
