package co.socialsquad.squad.presentation.feature.opportunityv2

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.feature.opportunityv2.adapter.OpportunityPageAdapter
import co.socialsquad.squad.presentation.feature.opportunityv2.customview.ConfirmOpportunityDeleteDialog
import co.socialsquad.squad.presentation.feature.opportunityv2.customview.OpportunityDetailsActionsBottomSheetDialog
import co.socialsquad.squad.presentation.feature.opportunityv2.model.*
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.OpportunityPageDetails
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.OpportunityPageShimmer
import co.socialsquad.squad.presentation.feature.opportunityv2.pages.OpportunityPageStages
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.OpportunityDetailsViewModel
import co.socialsquad.squad.presentation.feature.ranking.di.RankingListModule
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_opportunity_details.*
import kotlinx.android.synthetic.main.activity_opportunity_details_shimmer.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import java.text.SimpleDateFormat

class OpportunityDetailsActivity : BaseActivity() {

    override val modules: List<Module> = listOf(RankingListModule.instance)
    override val contentView = R.layout.activity_opportunity_details

    private var tabTitle = listOf("Detalhes", "Etapas")
    private val viewModel: OpportunityDetailsViewModel by viewModel()

    private var showLead: Boolean = false

    private val submissionId: String by extra(EXTRA_SUBMISSION_ID)
    private val objectType: String by extra(OBJECT_TYPE)
    private val toolType: RankingType by extra(TOOL_TYPE)
    private val initials: String by extra(INITIALS_LETTER)
    private val nameLead: String by extra(NOME_LEAD)
    private val fromScreen: String by extra(FROM_SCREEN)

    companion object {
        const val EXTRA_SUBMISSION_ID = "extra_submission_id"
        const val OBJECT_TYPE = "object_type"
        const val TOOL_TYPE = "tool_type"
        const val INITIALS_LETTER = "initials_letter"
        const val NOME_LEAD = "nome_lead"
        const val FROM_SCREEN = "fromScreen"

        fun start(toolType: RankingType,
                  objectType: String,
                  submission_id: String,
                  context: Context,
                  initials: String? = "",
                  nameLead: String? = "",
                  fromScreen: String? = ""

        ) {
            context.startActivity(Intent(context, OpportunityDetailsActivity::class.java).apply {
                putExtra(EXTRA_SUBMISSION_ID, submission_id)
                putExtra(OBJECT_TYPE, objectType)
                putExtra(TOOL_TYPE, toolType)
                putExtra(INITIALS_LETTER, initials)
                putExtra(NOME_LEAD, nameLead)
                putExtra(FROM_SCREEN, fromScreen)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupObservers1()
        setupObservers2()
        setupShimmer()

        setupToolType()

        configureTabs()

        when (toolType) {
            RankingType.Escala -> {
                viewModel.fetchDetailsTimetable(submissionId)
            }
            else -> viewModel.fetchOpportunityDetails(submissionId)
        }

    }

    private fun configureTabs() {
        tabTitle.forEachIndexed { index, it ->
            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_opportunity_details_tab, tlTabs, false).apply {
                    if (index != 0) {
                        var tv = this.findViewById<TextView>(R.id.tvTitle)
                        tv.setTextColor(ContextCompat.getColor(context, R.color.oldlavender))
                        tv.text = TextUtils.applyTypeface(
                                context,
                                "fonts/roboto_regular.ttf",
                                it.toString(),
                                it.toString()
                        )
                    } else {
                        findViewById<TextView>(R.id.tvTitle)?.text = it
                    }
                }
                view.measure(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                )
                customView = view
            }
            tlTabs.addTab(tab)
        }

        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(
                                    ContextCompat.getColor(
                                            context,
                                            R.color.darkjunglegreen_28
                                    )
                            )
                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_bold.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        } else {
                            this.setTextColor(ContextCompat.getColor(context, R.color.oldlavender))

                            text = TextUtils.applyTypeface(
                                    context,
                                    "fonts/roboto_regular.ttf",
                                    text.toString(),
                                    text.toString()
                            )
                        }
                    }
                }
            }
        })

        Handler().postDelayed({ tlTabs.selectTab(tlTabs.getTabAt(0)) }, 500)
    }

    private fun setupToolType() {
        when (toolType) {
            RankingType.Oportunidade -> {
                card_title.isVisible = true
                rectangle_title_shimmer.isVisible = true
                tabs_shimmer.isVisible = true
                showLead = true
            }
            RankingType.Schedule -> {
                circle_title2.isVisible = true
                circle_title_shimmer.isVisible = true
                tabs_shimmer.isVisible = false
                tlTabs.isVisible = false
                circle_title2.backgroundTintList = ColorStateList.valueOf(Color.parseColor(getCompanyColor()!!))
                showLead = false
            }
            RankingType.Escala -> {
                circle_title.isVisible = true
                circle_title_shimmer.isVisible = true
                tabs_shimmer.isVisible = false
                tlTabs.isVisible = false
                circle_title.backgroundTintList = ColorStateList.valueOf(Color.parseColor(getCompanyColor()!!))
                showLead = false
            }
            RankingType.Lead -> {
                circle_title2.isVisible = true
                tlTabs.isVisible = false
                circle_title.backgroundTintList = ColorStateList.valueOf(Color.parseColor(getCompanyColor()!!))
                showLead = true
            }
        }

    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(this).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }


    private fun setupBottomSheet(title: String, subtitle: String) {
        actions.setOnClickListener {
            var dialog = OpportunityDetailsActionsBottomSheetDialog(title, subtitle) {
                var dialog2 = ConfirmOpportunityDeleteDialog() {
                    when (it) {
                        "confirm" -> {
                            showLoading()
                            viewModel.deleteOpportunityDetails(submissionId)
                        }
                    }
                }
                dialog2.show(supportFragmentManager, "error")
            }
            dialog.show(supportFragmentManager, "")
        }
    }

    private fun showLoading() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.GRAY
        }

        deleteLoading.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        iconDeleteLoading.startAnimation(rotation)
    }


    private fun setupObservers1() {
        lifecycleScope.launch {
            val value = viewModel.opportunityDetails()
            value.collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let {
                            mainContainer.setBackgroundColor(resources.getColor(R.color._eeeff4))
                            view_loader.isVisible = false
                            content2.isVisible = true
                            pagerContent.isVisible = true
                            setupDetails(it)
                            setupViewPager(it, it.data?.stages!!)
                            setupBottomSheet(it.data?.title
                                    ?: "", it.data?.details?.object1?.short_title ?: "")
                        }
                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> {

                    }
                    Status.EMPTY -> finish()
                    Status.DELETE -> {
                        ExploreItemObjectActivity.doRefresh = true
                        finish()
                    }
                }
            }
        }
    }

    private fun setupObservers2() {
        lifecycleScope.launch {
            viewModel.timeTableDetails().collect { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let {
                            Log.i("tag10", "success")
                            mainContainer.setBackgroundColor(resources.getColor(R.color._eeeff4))
                            view_loader.isVisible = false
                            content2.isVisible = true
                            pagerContent.isVisible = true
                            setupDetailsAsTimeTable(it)

                            it.exploreobject?.location?.let { location ->
                                location.pin?.let { pin ->
                                    it.exploreobject?.media?.url = pin
                                }
                            }

                            setupViewPager(OpportunityDetailsModel(true, "",
                                    OpportunityData("aaa", it.exploreobject?.title, "bbbb",
                                            OpportunityDetails(OpportunityObject("id", it.exploreobject?.short_title, it.exploreobject?.short_description, it.exploreobject?.customFields, null), mutableListOf())
                                            , null, it.exploreobject?.media)),
                                    mutableListOf())
                            setupBottomSheet(it.title ?: "", it.shift ?: "")
                        }
                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> finish()
                    Status.EMPTY -> finish()
                }
            }
        }
    }


    var sdfDay = SimpleDateFormat(" • dd, yyyy\n" +
            "\'Turno\' • HH:mm - ")

    var sdfDayEnd = SimpleDateFormat("HH:mm")


    var sdfMonth = SimpleDateFormat("MMM")
    var sdfDayy = SimpleDateFormat("dd")


    private fun setupDetailsAsTimeTable(timetable: Timetable) {
        levelName.text = timetable.position
        longTurnoDay.text = timetable.shift + sdfDay.format(timetable.datetime_start) + sdfDayEnd.format(timetable.datetime_end)
        longTurnoDay.isVisible = true
        status.isVisible = true
        tvMes.text = sdfMonth.format(timetable.datetime_start).toUpperCase()
        tvContactText.text = sdfDayy.format(timetable.datetime_start)
    }

    private fun setupDetails(it: OpportunityDetailsModel) {
        Glide.with(this)
                .asBitmap()
                .load(it.data?.media?.url)
                //.apply(RequestOptions().override(64, 64))
                .into(imageOpportunity)
        levelName.text = it.data?.title
        status.text = it.data?.status

        when (it.data?.status) {
            "Bloqueada", "Encerrada" -> {
                status.setTextColor(resources.getColor(R.color._c7292b))
            }
        }

        when (toolType) {
            RankingType.Schedule -> {
                levelName.text = nameLead
            }
            else -> {
            }
        }
    }


    private fun setupViewPager(details: OpportunityDetailsModel, stages: List<OpportunityStage>) {
        var fragments = mutableListOf<Fragment>()

        fragments.add(OpportunityPageDetails.newInstance(showLead, objectType, details, initials, fromScreen).apply {
            callback = {
                when (toolType) {
                    RankingType.Oportunidade -> {
                        OpportunityDetailsActivity.start(RankingType.Lead, "Lead", it, this@OpportunityDetailsActivity)
                    }
                    RankingType.Escala -> {

                    }
                    RankingType.Lead -> {

                    }
                }
            }
        })
        fragments.add(OpportunityPageStages.newInstance(stages))
        viewPager.adapter = OpportunityPageAdapter(this, fragments)

        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(this).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        tlTabs.setSelectedTabIndicatorColor(ColorUtils.parse(userCompany.company?.primaryColor!!))
        viewPager.offscreenPageLimit = 2
//        TabLayoutMediator(tlTabs, viewPager) { tab, position ->
//            tab.text = "           ${(tabTitle[position])}          "
//        }.attach()

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                var stages = fragments[position] as? OpportunityPageStages
                stages?.let {
                    it.requestLayout()
                }
            }
        })
    }

    private fun setupShimmer() {
        var fragments = mutableListOf<Fragment>()

        when (toolType) {
            RankingType.Oportunidade -> {
                fragments.add(OpportunityPageShimmer.newInstance())
                fragments.add(OpportunityPageShimmer.newInstance())
            }
            RankingType.Escala -> {
                fragments.add(OpportunityPageShimmer.newInstance())
            }
            RankingType.Lead -> {

            }
        }


        viewPagerLoading.adapter = OpportunityPageAdapter(this, fragments)

        TabLayoutMediator(tabs_shimmer, viewPagerLoading) { tab, position ->
            tab.text = "           ${(tabTitle[position])}          "
        }.attach()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""

        when (toolType) {
            RankingType.Oportunidade -> {
                toolbar_title.text = "Oportunidade"
            }
            RankingType.Schedule -> {
                toolbar_title.text = "Agendamento"
                tvCicleName.text = initials
                levelName.text = nameLead
            }

            RankingType.Escala -> {
                toolbar_title.text = "Reserva"
            }
            RankingType.Lead -> {
                toolbar_title.text = "Lead"
            }
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}