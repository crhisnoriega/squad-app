package co.socialsquad.squad.presentation.feature.settings.password

import co.socialsquad.squad.data.entity.EditPasswordRequest
import co.socialsquad.squad.data.repository.LoginRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private const val PASSWORD_MINIMUM_LENGTH = 6

class SettingsPasswordPresenter @Inject constructor(
    private val settingsPasswordView: SettingsPasswordView,
    private val loginRepository: LoginRepository
) {
    private val compositeDisposable = CompositeDisposable()

    fun onCreate() {
        settingsPasswordView.setupToolbar()
        settingsPasswordView.setupInputListener()
        settingsPasswordView.setupKeyboardActions()
        settingsPasswordView.setupPasswordFields()
    }

    fun onInput(currentPassword: String?, newPassword: String?, confirmPassword: String?) {
        val conditionsMet = !currentPassword.isNullOrBlank() &&
            newPassword?.let { it.length >= PASSWORD_MINIMUM_LENGTH } ?: false &&
            confirmPassword?.let { confirmPassword.length >= PASSWORD_MINIMUM_LENGTH } ?: false &&
            newPassword == confirmPassword
        settingsPasswordView.setDoneButtonEnabled(conditionsMet)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onNextClicked(currentPassword: String, newPassword: String) {
        val editPasswordRequest = EditPasswordRequest(currentPassword, newPassword)

        compositeDisposable.add(
            loginRepository.editPassword(editPasswordRequest)
                .doOnSubscribe { settingsPasswordView.showLoading() }
                .doOnTerminate { settingsPasswordView.hideLoading() }
                .subscribe(
                    { settingsPasswordView.showMessageNewPasswordSet() },
                    { settingsPasswordView.showErrorMessageFailedToEditPassword() }
                )
        )
    }
}
