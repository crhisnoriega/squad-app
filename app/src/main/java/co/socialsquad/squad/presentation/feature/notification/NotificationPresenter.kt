package co.socialsquad.squad.presentation.feature.notification

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.request.Notification
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_NOTIFICATION_CLICK_NOTIFICATIONS_SCREEN
import co.socialsquad.squad.presentation.custom.OuterDivider
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.notification.viewHolder.NotificationViewModel
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.util.DateTimeUtils
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class NotificationPresenter @Inject constructor(
    private val socialRepository: SocialRepository,
    private val notificationView: NotificationView,
    private val tagWorker: TagWorker
) {

    private var compositeDisposable = CompositeDisposable()
    private var page = 1
    private var isLoading = false
    private var completed = false

    private var todayHeader = false
    private var yesterdayHeader = false
    private var previousHeader = false

    fun onViewCreated() {
        onRefresh()
    }

    fun notificationClicked(pk: Long) {
        tagWorker.tagEvent(TAG_NOTIFICATION_CLICK_NOTIFICATIONS_SCREEN)
        compositeDisposable.add(
            socialRepository.clickNotification(pk)
                .subscribe(
                    { },
                    { it.printStackTrace() }
                )
        )
    }

    private fun updateUnreadNotifications() {
        notificationView.updateNotificationBadge()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isLoading || completed) return
        compositeDisposable.add(
            socialRepository.getNotifications(page)
                .doOnSubscribe {
                    isLoading = true
                    notificationView.showLoadingMore()
                }
                .doOnNext { if (it.next == null) completed = true }
                .map { mapResults(it.results) }
                .subscribe(
                    {
                        page++
                        notificationView.addItens(it)
                    },
                    {
                        isLoading = false
                        it.printStackTrace()
                        notificationView.hideLoadingMore()
                        notificationView.hideRefreshing()
                    },
                    {
                        isLoading = false
                        notificationView.hideLoadingMore()
                        notificationView.hideRefreshing()
                    }
                )
        )
    }

    fun onRefresh() {
        page = 1
        todayHeader = false
        yesterdayHeader = false
        previousHeader = false
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getNotifications(page)
                .doOnSubscribe {
                    isLoading = true
                    notificationView.showLoadingMore()
                }
                .doOnTerminate {
                    isLoading = false
                    notificationView.hideLoadingMore()
                    notificationView.hideRefreshing()
                }
                .doOnNext { if (it.next == null) completed = true }
                .map { mapResults(it.results) }
                .flatMapCompletable {
                    page++
                    notificationView.setItems(it)
                    socialRepository.clearUnreadNotifications()
                }
                .subscribe(
                    { updateUnreadNotifications() },
                    { it.printStackTrace() }
                )
        )
    }

    private fun mapResults(results: List<Notification>?): List<ViewModel> {
        val viewModels = mutableListOf<ViewModel>()

        val notifications = results ?: emptyList()

        if (notifications.isEmpty()) {
            return emptyList()
        }

        val todayNotifications =
            notifications.filter { DateTimeUtils.isToday(DateTimeUtils.parseToCalendar(it.createdAt)!!) }

        if (todayNotifications.isNotEmpty()) {
            if (!todayHeader) {
                todayHeader = true
                viewModels.add(
                    HeaderViewModel(
                        null,
                        null,
                        R.string.notification_list_header_today
                    )
                )
            }

            val todayViewModels = todayNotifications.map { mapToViewModel(it) }
            viewModels.addAll(todayViewModels)
            viewModels.add(OuterDivider(-1))
        }

        val yesterdayNotifications =
            notifications.filter { DateTimeUtils.isYesterday(DateTimeUtils.parseToCalendar(it.createdAt)!!) }

        if (yesterdayNotifications.isNotEmpty()) {
            if (!yesterdayHeader) {
                yesterdayHeader = true
                viewModels.add(
                    HeaderViewModel(
                        null,
                        null,
                        R.string.notification_list_header_yesterday
                    )
                )
            }

            val yesterdayViewModels = yesterdayNotifications.map { mapToViewModel(it) }
            viewModels.addAll(yesterdayViewModels)
            viewModels.add(OuterDivider(-1))
        }

        val previousNotifications = notifications - todayNotifications - yesterdayNotifications
        if (previousNotifications.isNotEmpty()) {
            if (!previousHeader) {
                previousHeader = true
                viewModels.add(
                    HeaderViewModel(
                        null,
                        null,
                        R.string.notification_list_header_previous
                    )
                )
            }

            val previousViewModels = previousNotifications.map { mapToViewModel(it) }
            viewModels.addAll(previousViewModels)
            viewModels.add(OuterDivider(-1))
        }
        return viewModels
    }

    private fun mapToViewModel(notification: Notification): NotificationViewModel {
        val url = notification.media?.let {
            when {
                FileUtils.isImageMedia(it) -> it.url
                FileUtils.isVideoMedia(it) -> it.streamings?.firstOrNull()?.thumbnailUrl
                else -> ""
            }
        }
        return NotificationViewModel(
            notification.pk,
            notification.author?.user?.avatar ?: "",
            notification.templateMessage?.trim(),
            notification.createdAt,
            notification.author?.user?.firstName + " " + notification.author?.user?.lastName,
            notification.author?.pk ?: -1,
            url,
            notification.customData?.model,
            notification.customData?.pk ?: 0,
            notification.clicked
        )
    }
}
