package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import android.net.Uri
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.view_qualification_requirement_header.view.*

class QualificationRequirementHeaderViewHolder(itemView: View) : ViewHolder<QualificationRequirementHeaderViewModel>(itemView) {
    private var descriptionExpanded = false
    private var descriptionMaxLines = 3

    override fun bind(viewModel: QualificationRequirementHeaderViewModel) {
        itemView.apply {
            if (viewModel.videoUrl != null) {
                vvVideo.apply {
                    visibility = View.VISIBLE
                    loadThumbnail(viewModel.videoThumbnailUrl)
                    prepare(Uri.parse(viewModel.videoUrl))
                }
            }

            tvDescription.text = viewModel.description

            descriptionMaxLines = tvDescription.maxLines
            tvDescription.post {
                if (tvDescription.layout?.lineCount ?: 0 > 3) {
                    bExpandDescription.visibility = View.VISIBLE
                    bExpandDescription.setOnClickListener {
                        if (descriptionExpanded) {
                            tvDescription.maxLines = descriptionMaxLines
                            bExpandDescription.text = context.getString(R.string.qualification_requirement_button_expand_description)
                        } else {
                            tvDescription.maxLines = Int.MAX_VALUE
                            bExpandDescription.text = context.getString(R.string.qualification_requirement_button_collapse_description)
                        }
                        descriptionExpanded = !descriptionExpanded
                    }
                }
            }
        }
    }

    override fun recycle() {}
}
