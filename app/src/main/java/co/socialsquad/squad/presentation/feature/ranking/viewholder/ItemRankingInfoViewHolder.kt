package co.socialsquad.squad.presentation.feature.ranking.viewholder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import androidx.core.view.isVisible
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_ranking_info.view.*

class ItemRankingInfoViewHolder(
        itemView: View,
        var onClickAction: ((item: ItemRankingInfoViewModel) -> Unit))
    : ViewHolder<ItemRankingInfoViewModel>(itemView) {

    override fun bind(viewModel: ItemRankingInfoViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.rankingItem?.title
        itemView.txtSubtitle.text = viewModel.rankingItem?.description



        if (viewModel.rankingItem?.user_in_rank!!) {
            itemView.txtDescription.text = viewModel.rankingItem?.header?.position_string + " • " + viewModel.rankingItem?.header?.score
        } else {
            itemView.txtDescription.text = viewModel.rankingItem?.header?.title
        }

        itemView.icOptions.visibility = View.VISIBLE

        itemView.dividerItemTop.isVisible = viewModel.rankingItem?.first.not()
        // Glide.with(itemView.context).load(viewModel.rankingData.icon).into(itemView.imgIcon)

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.rankingItem?.icon), itemView.imgIcon)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}