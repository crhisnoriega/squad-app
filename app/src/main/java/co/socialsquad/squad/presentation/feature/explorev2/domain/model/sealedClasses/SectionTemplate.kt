package co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val VALUE_RECTANGLE_HORIZONTAL = "rectangle_horizontal"
private const val VALUE_RECTANGULAR_HORIZONTAL = "rectangular_horizontal"
private const val VALUE_RECTANGLE_VERTICAL = "rectangle_vertical"
private const val VALUE_RECTANGULAR_VERTICAL = "rectangular_vertical"
private const val VALUE_SQUARE = "square"
private const val VALUE_CIRCLE = "circle"
private const val VALUE_HEADER = "header"
private const val VALUE_FOOTER = "footer"
private const val VALUE_EMPTY_SPACE = "empty"
private const val VALUE_EMPTY_SPACE_END = "empty_end"

sealed class SectionTemplate(val value: String) : Parcelable {

    companion object {
        fun getDefault() = RectangleHorizontal
        fun getByValue(value: String?) = when (value) {
            VALUE_RECTANGLE_HORIZONTAL, VALUE_RECTANGULAR_HORIZONTAL -> RectangleHorizontal
            VALUE_RECTANGLE_VERTICAL, VALUE_RECTANGULAR_VERTICAL -> RectangleVertical
            VALUE_SQUARE -> Square
            VALUE_CIRCLE -> Circle
            VALUE_HEADER -> Header
            VALUE_FOOTER -> Footer
            VALUE_EMPTY_SPACE -> EmptySpace
            else -> getDefault()
        }
    }

    @Parcelize
    object RectangleHorizontal : SectionTemplate(VALUE_RECTANGLE_HORIZONTAL)

    @Parcelize
    object RectangleVertical : SectionTemplate(VALUE_RECTANGLE_VERTICAL)

    @Parcelize
    object Square : SectionTemplate(VALUE_SQUARE)

    @Parcelize
    object Circle : SectionTemplate(VALUE_CIRCLE)

    @Parcelize
    object Header : SectionTemplate(VALUE_HEADER)

    @Parcelize
    object Footer : SectionTemplate(VALUE_HEADER)

    @Parcelize
    object EmptySpace : SectionTemplate(VALUE_EMPTY_SPACE)


    @Parcelize
    object EmptySpaceEnd : SectionTemplate(VALUE_EMPTY_SPACE_END)
}
