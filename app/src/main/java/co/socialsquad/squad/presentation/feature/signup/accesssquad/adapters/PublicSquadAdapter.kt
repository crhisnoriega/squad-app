package co.socialsquad.squad.presentation.feature.signup.accesssquad.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import com.bumptech.glide.Glide

// class PublicSquadAdapter(val onSquadSelected: KFunction4<String, Boolean, String?, Company, Unit>) :
class PublicSquadAdapter(val onSquadSelected: (String, Boolean, String?, Company, String) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val headerType = 1000
    private val contentType = 1001

    private var companies = mutableListOf<Company>()
    private var isLogin = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == headerType) {
            HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.private_access_squad_header_list_item, parent, false))
        } else {
            ContentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.public_access_squad_list_item, parent, false))
        }
    }

    override fun getItemCount(): Int = companies.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0) {
            val viewHolder = holder as HeaderViewHolder
            viewHolder.name.text = companies[position].name
        } else {
            val companie = companies[position]
            val viewHolder = holder as ContentViewHolder
            viewHolder.name.text = companie.name

            if (isLogin || companie.loginToken != null) {
                viewHolder.site.text = companie.companyUrl
            } else {
                viewHolder.site.text = companie.descriptionText
            }

            if (companie.loginToken != null) {
                viewHolder.btnAccess.visibility = View.VISIBLE
                viewHolder.arrowAccess.visibility = View.GONE

                viewHolder.btnAccess.setOnClickListener {
                    it.isEnabled = false
                    onSquadSelected(position, companie)
                }
            } else {
                viewHolder.btnAccess.visibility = View.GONE
                viewHolder.arrowAccess.visibility = View.VISIBLE

                viewHolder.content.setOnClickListener {
                    onSquadSelected(position, companie)
                }
            }

            Glide.with(holder.itemView.context).load(companie.avatar).into(holder.image)
        }
    }

    private fun onSquadSelected(position: Int, companie: Company) {
        companies[position].subdomain?.apply {
            onSquadSelected(this, companies[position].isUniqueValue(), companie.invitationToken, companie, "Public")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            headerType
        } else {
            contentType
        }
    }

    fun addCompanies(companies: List<Company>) {
        this.companies.addAll(companies)
        notifyDataSetChanged()
    }

    fun isLogin(boolean: Boolean) {
        this.isLogin = boolean
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
    }

    class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.image)
        val name: TextView = itemView.findViewById(R.id.name)
        val site: TextView = itemView.findViewById(R.id.site)
        val content: ConstraintLayout = itemView.findViewById(R.id.nestedScroll)
        val btnAccess: CardView = itemView.findViewById(R.id.btnAccess)
        val arrowAccess: ImageButton = itemView.findViewById(R.id.arrowAccess)
    }
}
