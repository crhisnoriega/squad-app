package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupViewModel
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupsViewModel
import kotlinx.android.synthetic.main.view_product_detail_groups.view.*

class ProductDetailGroupsViewHolder(itemView: View, val listener: Listener<ProductDetailGroupViewModel>) : ViewHolder<ProductDetailGroupsViewModel>(itemView) {
    private val rvDetailGroups: RecyclerView = itemView.product_detail_groups_rv_detail_groups

    override fun bind(viewModel: ProductDetailGroupsViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ProductDetailGroupViewModel -> ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ITEM_VIEW_MODEL_ID -> ItemViewHolder(view, listener)
                else -> throw IllegalArgumentException()
            }
        }

        with(rvDetailGroups) {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(viewModel.detailGroups) }
            addItemDecoration(ListDividerItemDecoration(context))
            setItemViewCacheSize(0)
        }
    }

    override fun recycle() {}
}
