package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResourceGroup(
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("resources") val resources: List<Resource> = listOf(),
): Parcelable