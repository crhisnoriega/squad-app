package co.socialsquad.squad.presentation.feature.audio

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import androidx.media.AudioAttributesCompat
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import java.io.Serializable
import java.util.ArrayList

private const val POSITION_DELAY = 300L

object AudioManager {

    interface Listener : Serializable {
        fun onReady(isPlaying: Boolean) {}
        fun onBuffering() {}
        fun onEnded() {}
        fun onReleased() {}
        fun onPositionChanged(position: Long) {}
    }

    @SuppressLint("StaticFieldLeak")
    var player: ExoPlayer? = null

    private var listeners = arrayListOf<Listener>()
    private val handler = Handler()

    var audioViewModel: AudioViewModel? = null

    private val eventListener = object : Player.EventListener {
        override fun onSeekProcessed() {}
        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
        override fun onLoadingChanged(isLoading: Boolean) {}
        override fun onPositionDiscontinuity(reason: Int) {}
        override fun onRepeatModeChanged(repeatMode: Int) {}

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_READY -> {
                    notifyOnReady(playWhenReady)
                }
                Player.STATE_ENDED -> {
                    notifyOnEnded()
                    player?.playWhenReady = false
                    player?.seekTo(0)
                }
                Player.STATE_BUFFERING -> {
                    notifyOnBuffering()
                }
            }
        }
    }

    fun prepare(context: Context, uri: Uri, audioViewModel: AudioViewModel) {
        this.audioViewModel = audioViewModel

        release()

        val userAgent = Util.getUserAgent(context, context.applicationInfo.loadLabel(context.packageManager).toString())
        val dataSourceFactory = DefaultDataSourceFactory(context, userAgent)
        val mediaSource = ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
        val audioManager =
            context.getSystemService(Context.AUDIO_SERVICE) as android.media.AudioManager
        val audioAttributes = AudioAttributesCompat.Builder()
            .setContentType(AudioAttributesCompat.CONTENT_TYPE_SPEECH)
            .setUsage(AudioAttributesCompat.USAGE_MEDIA)
            .build()
        player = AudioFocusWrapper(
            audioAttributes, audioManager,
            SimpleExoPlayer
                .Builder(context)
                .setTrackSelector(DefaultTrackSelector(context))
                .setBandwidthMeter(DefaultBandwidthMeter.Builder(context).build())
                .build().apply {
                    addListener(eventListener)
                    prepare(mediaSource)
                    seekTo(audioViewModel.position)
                }
        )

        setupNotification(context)
        setupPositionHandler()
    }

    private fun setupNotification(context: Context) {
        val intent = Intent(context, AudioService::class.java)
        Util.startForegroundService(context, intent)
    }

    fun release() {
        handler.removeCallbacksAndMessages(null)
        notifyOnReleased()
        player?.release()
        player = null
    }

    fun toggle() {
        player?.apply { playWhenReady = !playWhenReady }
    }

    fun addListener(listener: Listener) {
        listeners.add(listener)
    }

    fun removeListener(listener: Listener) {
        @Suppress("unchecked_cast")
        val copy = listeners.clone() as ArrayList<Listener>
        copy.remove(listener)
        listeners = copy
    }

    private fun setupPositionHandler() {
        var runnable: Runnable? = null
        runnable = Runnable {
            notifyOnPositionChanged(player?.currentPosition?.takeIf { it > 0 } ?: 0)
            handler.postDelayed(runnable, POSITION_DELAY)
        }
        handler.postDelayed(runnable, POSITION_DELAY)
    }

    private fun notifyOnReady(isPlaying: Boolean) {
        listeners.forEach { it.onReady(isPlaying) }
    }

    private fun notifyOnBuffering() {
        listeners.forEach { it.onBuffering() }
    }

    private fun notifyOnEnded() {
        listeners.forEach { it.onEnded() }
    }

    private fun notifyOnReleased() {
        listeners.forEach { it.onReleased() }
    }

    private fun notifyOnPositionChanged(position: Long) {
        listeners.forEach { it.onPositionChanged(position) }
    }

    fun skipBack(interval: Long) {
        player?.apply { seekTo(currentPosition - interval) }
    }

    fun skipForward(interval: Long) {
        player?.apply {
            val position = currentPosition + interval
            if (position < duration) seekTo(position)
        }
    }
}
