package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class ProgressData(
    @SerializedName("percentage_value") val percentage_value: Int?,
    @SerializedName("numeric_value") val numeric_value: Int?,
    @SerializedName("subtitle") val subtitle: String?,
    @SerializedName("score_progress") val score_progress: Boolean?,

    @SerializedName("boolean") val boolean: Boolean?,
    @SerializedName("boolean_value") val boolean_value: Boolean?,


    ) : Parcelable