package co.socialsquad.squad.presentation.feature.live.viewer

import android.content.DialogInterface
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.presentation.feature.social.LiveViewModel

interface LiveViewerView {
    fun setupViewer(url: String)
    fun addQueueItem(comment: QueueItem)
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun showLoading()
    fun hideLoading()
    fun close()
    fun setParticipantCount(count: Int)
    fun showCommentField()
    fun keepScreenOn(enabled: Boolean)
    fun setStreamingEnded()
    fun setupShareButton(live: LiveViewModel)
    fun openVideo(url: String)
}
