package co.socialsquad.squad.presentation.custom.multimedia

import android.util.Log
import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.multimedia.audio.AudioDelegateAdapter
import co.socialsquad.squad.presentation.custom.multimedia.image.ImageDelegateAdapter
import co.socialsquad.squad.presentation.custom.multimedia.unsuported.UnsuportedDelegateAdapter
import co.socialsquad.squad.presentation.custom.multimedia.video.VideoDelegateAdapter
import com.google.gson.Gson

class MultiMediaPagerAdapter(val lifecycleOwner: LifecycleOwner) : RecyclerView.Adapter<MediaViewHolder>() {

    private var items: ArrayList<Media> = arrayListOf()
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

    init {
        delegateAdapters.addAll(
                MultiMediaDataType.NOT_SUPORTED.value to UnsuportedDelegateAdapter(),
                MultiMediaDataType.IMAGE.value to ImageDelegateAdapter(),
                MultiMediaDataType.VIDEO.value to VideoDelegateAdapter(),
                MultiMediaDataType.AUDIO.value to AudioDelegateAdapter()
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        return delegateAdapters.get(viewType)?.onCreateViewHolder(parent, lifecycleOwner)
                ?: run { throw IllegalStateException("ViewType não conhecido, valor em questão: $viewType") }
    }

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        return delegateAdapters.get(getItemViewType(position))
                ?.onBindViewHolder(holder, this.items[position], (position + 1).toString() + "/" + items.size, position == 0)
                ?: run { throw IllegalStateException("ViewType não reconhecido") }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addMedias(list: List<Media>) {
        items.clear()
        items.addAll(list)
    }

    fun playItem(position: Int) {
        items!!.forEachIndexed { index, media ->
            if (index == position) {
                media.streamings?.let {
                    media.isViewHolderVisible = true
                }
            } else {
                media.streamings?.let {
                    media.isViewHolderVisible = false
                }
            }
        }

        notifyDataSetChanged()
    }
}
