package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemShowAllRecognitionViewModel(
        val title: String,
        val type: String)
    : ViewModel {
}