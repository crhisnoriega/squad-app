package co.socialsquad.squad.presentation.feature.feedback

import dagger.Binds
import dagger.Module

@Module
interface FeedbackModule {
    @Binds
    fun view(view: FeedbackActivity): FeedbackView
}
