package co.socialsquad.squad.presentation.feature.hub.dashboard

import dagger.Binds
import dagger.Module

@Module
abstract class DashboardModule {
    @Binds
    abstract fun view(dashboardFragment: DashboardFragment): DashboardView
}
