package co.socialsquad.squad.presentation.feature.trip

import co.socialsquad.squad.data.entity.Trip
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import java.io.Serializable

class TripViewModel(
    val pk: Int,
    val imageUrl: String?,
    val videoUrl: String?,
    val videoThumbnailUrl: String?,
    val title: String?,
    val description: String?,
    val promotionText: String?,
    val color: String?
) : ViewModel, Serializable {
    constructor(trip: Trip, companyColor: String? = "") : this(
        trip.pk ?: 0,
        trip.cover?.let { if (FileUtils.isImageMedia(it)) it.url else null },
        trip.cover?.let { if (FileUtils.isVideoMedia(it)) it.streamings?.firstOrNull()?.playlistUrl else null },
        trip.cover?.let { if (FileUtils.isVideoMedia(it)) it.streamings?.firstOrNull()?.thumbnailUrl else null },
        trip.title,
        trip.content,
        trip.promotionText,
        companyColor
    )
}
