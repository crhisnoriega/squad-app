package co.socialsquad.squad.presentation.feature.explorev2.items

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.SectionsFragment
import co.socialsquad.squad.presentation.feature.explorev2.components.collectionhorizontalmap.CollectionHorizontalMapItemsAdapter
import co.socialsquad.squad.presentation.feature.explorev2.detail.ExploreItemDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.argument
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.explore_item_map_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import javax.inject.Inject

class ExploreItemMapFragment : Fragment() {

    private var markersOptions = mutableListOf<MarkerOptions>()
    private val viewModel: ExploreItemViewModel by viewModel()

    private val link: String by argument(ARGUMENT_LINK)
    private val section: Section by argument(ARGUMENT_SECTION)
    private val sectionIcon: SimpleMedia by argument(ARGUMENT_ICON)
    private val template: SectionTemplate by argument(ARGUMENT_TEMPLATE)
    private val loadingState: List<SectionLoadingState> by argument(ARGUMENT_LOADING_STATE)
    private val emptyState: EmptyState? by argument(ARGUMENT_EMPTY_STATE)
    private lateinit var map: MapView
    private lateinit var googleMap: GoogleMap
    private lateinit var bitmapSectionIcon: Bitmap
    private var lastMarker: Marker? = null
    private var lastPosition: Int = -1
    private var mapItens = mutableListOf<ExploreItemMapVM>()

    private var wasMarketClicked = false

    @Inject
    lateinit var loginRepository: LoginRepository
    private val indicatorColor by lazy {
        loginRepository.userCompany?.company?.primaryColor.orEmpty()
    }

    private var wasCentered = false

    companion object {
        private const val ARGUMENT_LINK = "ARGUMENT_LINK"
        private const val ARGUMENT_SECTION = "ARGUMENT_SECTION"
        private const val ARGUMENT_ICON = "ARGUMENT_ICON"
        private const val ARGUMENT_LOADING_STATE = "ARGUMENT_LOADING_STATE"
        private const val ARGUMENT_TEMPLATE = "ARGUMENT_TEMPLATE"
        private const val ARGUMENT_EMPTY_STATE = "ARGUMENT_EMPTY_STATE"

        private val fragments = mutableMapOf<Int, ExploreItemMapFragment>()

        @JvmStatic
        fun newInstance(
                section: Section,
                link: String,
                template: SectionTemplate = SectionTemplate.getDefault(),
                loadingState: List<SectionLoadingState> = SectionLoadingState.getDefault(),
                emptyState: EmptyState? = null,
                sectionIcon: SimpleMedia
        ): ExploreItemMapFragment {

            var fragment = fragments[section.id]

            if (fragment == null) {
                fragment = ExploreItemMapFragment().apply {
                    val args = bundleOf(
                            ARGUMENT_LINK to link,
                            ARGUMENT_ICON to sectionIcon,
                            ARGUMENT_TEMPLATE to template,
                            ARGUMENT_LOADING_STATE to ArrayList(loadingState),
                            ARGUMENT_EMPTY_STATE to emptyState,
                            ARGUMENT_SECTION to section

                    )
                    this.arguments = args
                }

                //fragments[section.id] = fragment
            }
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.explore_item_map_fragment, container, false)
        map = view.findViewById(R.id.map) as MapView
        map.onCreate(savedInstanceState)
        //retainInstance = true

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        setupMap()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()

        filler.isVisible = SectionsFragment.EXPANDED_TOOLBAR
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBtnLista()
    }

    private fun setupBtnLista() {
        rlLista.setOnClickListener {
            callbackActions?.onListClick()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            btnLista.compoundDrawableTintList = ColorUtils.stateListOf(indicatorColor!!)
        }

        btnLista.typeface = Typeface.createFromAsset(requireContext().assets, "fonts/roboto_medium.ttf")
    }

    private fun saveProp(name: String, zoom: Float) {
        var shared = requireActivity().getSharedPreferences("map", Context.MODE_PRIVATE)
        shared.edit().putFloat(name, zoom).commit()
    }


    private fun retrieveProp(name: String): Float {
        var shared = requireActivity().getSharedPreferences("map", Context.MODE_PRIVATE)
        return shared.getFloat(name, -1f)

    }

    private var lat = -1f
    private var lng = -1f
    private var zoom = -1f

    @SuppressLint("MissingPermission")
    @ExperimentalCoroutinesApi
    fun setupMap() {
        this.lat = retrieveProp("lat${section.id}")
        this.lng = retrieveProp("lng${section.id}")
        this.zoom = retrieveProp("zoom${section.id}")

        if (this::googleMap.isInitialized) {

            return
        }

        map.getMapAsync {


            googleMap = it

            Log.i("map", "lat retrieve $lat")
            if (lat != -1f && lng != -1f) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        LatLng(lat.toDouble(), lng.toDouble()), zoom
                ))
                this@ExploreItemMapFragment.wasCentered = true
            }


            googleMap.uiSettings.isMyLocationButtonEnabled = false
            googleMap.uiSettings.isZoomControlsEnabled = false
            googleMap.uiSettings.isMapToolbarEnabled = false
            googleMap.uiSettings.isCompassEnabled = false
            googleMap.uiSettings.isTiltGesturesEnabled = false
            googleMap.uiSettings.isIndoorLevelPickerEnabled = false
            googleMap.uiSettings.isRotateGesturesEnabled = false
            googleMap.isMyLocationEnabled = true
            googleMap.isTrafficEnabled = false
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            Log.i("map", "google map $googleMap")
            googleMap.setOnCameraChangeListener {
                Log.i("map", "zoom ${googleMap.cameraPosition.zoom}")
                Log.i("map", "lat ${googleMap.cameraPosition.target.latitude}")


                saveProp("zoom${section.id}", googleMap.cameraPosition.zoom)
                saveProp("lat${section.id}", googleMap.cameraPosition.target.latitude.toFloat())
                saveProp("lng${section.id}", googleMap.cameraPosition.target.longitude.toFloat())
            }

            googleMap.setOnMapLoadedCallback {
                sectionIcon.url?.let { url ->
                    Log.i("map", "url: $url")
                    Glide.with(requireContext())
                            .asBitmap()
                            .load(url)
                            .into(object : SimpleTarget<Bitmap?>() {
                                override fun onResourceReady(
                                        resource: Bitmap,
                                        transition: Transition<in Bitmap?>?
                                ) {
                                    bitmapSectionIcon = resource
                                    viewModel.fetchExplore(link, true)
                                    Log.i("map", "called after load url")
                                }
                            })
                }


            }

            googleMap.setOnMarkerClickListener { marker ->

                changeMarkerIcon(marker, true)

                changeOffsetCenter100(marker.position.latitude, marker.position.longitude)

                true
            }


            if (lat != -1f && lng != -1f) {
                changeOffsetCenter(lat.toDouble(), lng.toDouble())
            }


            googleMap.setOnMapClickListener {
                rvItens.isVisible = false
            }
        }

    }

    var mapMarker = mutableMapOf<MarkerOptions, Marker>()


    private fun changeMarkerIcon(marker: Marker, doSelect: Boolean) {
        if (lastMarker != null) {
            val markerOption = addMarkerPin(PinLocation(lastMarker?.position?.latitude!!, lastMarker?.position?.longitude!!, ""), false)
            markersOptions[lastPosition] = markerOption

            var markerLast = googleMap.addMarker(markerOption)
            markerLast.tag = lastMarker?.tag

            mapMarker[markerOption] = markerLast
        }


        val markerOption = addMarkerPin(PinLocation(marker.position.latitude, marker.position.longitude, ""), true)
        markersOptions.add(markerOption)

        var marker2 = googleMap.addMarker(markerOption)
        marker2.tag = marker.tag


        if (doSelect) {
            marker.tag.toString()?.toInt()?.let { position ->
                wasMarketClicked = true
                selectItem(position)
            }
        }
        lastMarker = marker2
        lastPosition = marker2.tag.toString().toInt()

        marker.remove()
    }

    fun createMarkerBitmapSelected(pinLocation: PinLocation?): Bitmap {
        var view = layoutInflater.inflate(
                R.layout.view_explore_location_tooltip_selected,
                null,
                false
        )

        val image = view.findViewById<ImageView>(R.id.exploreMapTooltipIcon)
        image.setImageBitmap(bitmapSectionIcon)

        return createBitmapFromView(view, 38, 41)
    }

    fun createMarkerBitmap(pinLocation: PinLocation?): Bitmap {
        var view = layoutInflater.inflate(
                R.layout.view_explore_location_tooltip,
                null,
                false
        )

        indicatorColor.let {
            val stateList = ColorUtils.stateListOf(it)
            /*view.findViewById<ImageView>(R.id.exploreMapTooltip)
                .backgroundTintList = stateList*/
        }


        val image = view.findViewById<ImageView>(R.id.exploreMapTooltipIcon)
        image.setImageBitmap(bitmapSectionIcon)

        return createBitmapFromView(view, 38, 41)
    }

    private fun createBitmapFromView(view: View, width: Int, height: Int): Bitmap {
        if (width > 0 && height > 0) {
            view.measure(
                    View.MeasureSpec.makeMeasureSpec(dpTopPx(width), View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(
                            dpTopPx(height), View.MeasureSpec.EXACTLY
                    )
            )
        }
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        val bitmap = Bitmap.createBitmap(
                view.measuredWidth,
                view.measuredHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        val background = view.background
        background?.draw(canvas)
        view.draw(canvas)
        return bitmap
    }

    private fun dpTopPx(dp: Int): Int {
        return (dp * requireContext().resources.displayMetrics.density).toInt()
    }

    private fun addMarkerPin(pinLocation: PinLocation, selected: Boolean = false): MarkerOptions {
        val latLng = LatLng(pinLocation.latitude, pinLocation.longitude)

        val bitmap = if (selected) {
            createMarkerBitmapSelected(pinLocation)
        } else {
            createMarkerBitmap(pinLocation)
        }

        return MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
    }

    private fun showLocationMarkers(explore: Explore) {

        markersOptions = arrayListOf<MarkerOptions>()


        var positions = 0

//        explore.contents.forEach { exploreContent ->
//            exploreContent.items.map { contentItem ->
//                contentItem.pinLocation?.let { pinLocation ->
//                    val markerOption = addMarkerPin(pinLocation)
//                    markersOptions.add(markerOption)
//
//                    var marker = googleMap.addMarker(markerOption)
//                    marker.tag = positions++.toInt()
//
//                    mapMarker.put(markerOption, marker)
//                }
//            }
//        }

        explore.list?.items?.forEach { listItem ->
            listItem.pinLocation?.let { pinLocation ->
                val markerOption = addMarkerPin(pinLocation)
                markersOptions.add(markerOption)

                var marker = googleMap.addMarker(markerOption)
                marker.tag = positions++.toInt()

                mapMarker.put(markerOption, marker)
            }
        }

        Log.i("map", "markersOptions: ${markersOptions.size}")
        if (markersOptions.isNotEmpty() && wasCentered.not()) {
            val latLngBoundsBuilder = LatLngBounds.Builder()
            markersOptions.forEach { latLngBoundsBuilder.include(it.position) }
            val latLngBounds = latLngBoundsBuilder.build()
            val paddingBot =
                    resources.getDimensionPixelOffset(R.dimen.padding_explore_map_paddding_bottom)
            googleMap.setPadding(0, 0, 0, paddingBot)

            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, dpTopPx(20)))

        }
    }

    fun changeOffsetCenter(latitude: Double, longitude: Double) {
        val mappoint = googleMap.projection.toScreenLocation(LatLng(latitude, longitude))
        mappoint.set(mappoint.x, mappoint.y + 200) // change these values as you need , just hard coded a value if you want you can give it based on a ratio like using DisplayMetrics  as well
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(googleMap.projection.fromScreenLocation(mappoint)))
    }

    fun changeOffsetCenter100(latitude: Double, longitude: Double) {
        val mappoint = googleMap.projection.toScreenLocation(LatLng(latitude, longitude))
        mappoint.set(mappoint.x, mappoint.y + 200) // change these values as you need , just hard coded a value if you want you can give it based on a ratio like using DisplayMetrics  as well
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(googleMap.projection.fromScreenLocation(mappoint)))
    }

    private fun selectItem(position: Int) {
        //rvItens.scrollToPosition(position)
        rvItens.currentItem = position
        rvItens.isVisible = true
    }

    fun setupObserver() {
        rvItens.isVisible = false

//        val snapHelper = GravitySnapHelper(Gravity.CENTER)
//        snapHelper.attachToRecyclerView(rvItens)
//        rvItens.addOnScrollListener(
//            EndlessScrollListener(
//                2,
//                rvItens.layoutManager as LinearLayoutManager,
//                viewModel::getNextPage
//            )
//        )

        lifecycleScope.launch {
            viewModel.explore().collect {

                Log.i("map", "collect ${it.status}")

                when (it.status) {

                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            showLocationMarkers(data)

                            data.contents.forEach { exploreContent ->
                                exploreContent.items.map { contentItem ->
                                    if (contentItem.pinLocation != null) {
                                        mapItens.add(ExploreItemMapVM(contentItem = contentItem))
                                    }
                                }
                            }

                            data.list.items.forEach { listItem ->
                                if (listItem.pinLocation != null) {
                                    mapItens.add(ExploreItemMapVM(listItem = listItem))
                                }
                            }

                            rvItens.offscreenPageLimit = 3;

                            val recyclerView = rvItens.getChildAt(0) as RecyclerView

                            recyclerView.apply {
                                val padding = 80
                                setPadding(padding.toInt(), 0, padding.toInt(), 0)
                                clipToPadding = false
                            }

                            Log.i("map", "template: $template")
                            Log.i("map", "adaoter: ${rvItens.adapter}")

                            rvItens.adapter = CollectionHorizontalMapItemsAdapter(
                                    requireContext
                                    (), template, mapItens, indicatorColor, { contentItem ->
                                if (contentItem.isObject()) {
                                    contentItem.link?.let { link ->
                                        requireContext().startActivity(
                                                ExploreItemObjectActivity
                                                        .newInstance(requireContext(), link)
                                        )
                                    }
                                } else {
                                    ExploreItemDetailActivity.start(
                                            context = requireContext(),
                                            item = contentItem,
                                            template = template,
                                            emptyState = emptyState
                                    )
                                }
                            }, { listItem ->
                                if (listItem.isObject()) {
                                    listItem.link?.let { link ->
                                        requireContext().startActivity(
                                                ExploreItemObjectActivity
                                                        .newInstance(requireContext(), link)
                                        )
                                    }
                                }
                            })

                            rvItens.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                                override fun onPageSelected(position: Int) {
                                    if (!wasMarketClicked) {
                                        var markerOption = markersOptions[position]
                                        var latLng = markerOption.position

                                        /*googleMap.animateCamera(
                                                CameraUpdateFactory.newLatLng(LatLng(latLng.latitude, latLng.longitude)),
                                                400,
                                                null
                                        )*/

                                        changeOffsetCenter(latLng.latitude, latLng.longitude)

                                        var marker = mapMarker[markerOption]
                                        Log.i("map", "onPageSelected $marker")
                                        marker?.let { marker ->
                                            changeMarkerIcon(marker, false)
                                        }
                                    }
                                    if (wasMarketClicked) {
                                        wasMarketClicked = false
                                    }

                                }

                            })
                        }
                    }
                    Status.LOADING -> {

                    }
                    Status.EMPTY -> {

                    }
                    Status.ERROR -> {

                    }
                }
            }
        }
    }

    internal var callbackActions: OnActionsListener? = null

    interface OnActionsListener {
        fun onListClick()
    }
}