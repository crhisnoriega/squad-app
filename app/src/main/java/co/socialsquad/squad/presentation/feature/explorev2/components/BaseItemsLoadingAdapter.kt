package co.socialsquad.squad.presentation.feature.explorev2.components

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.util.inflate

private const val ITEM_COUNT = 6

abstract class BaseItemsLoadingAdapter(
    private val template: SectionTemplate
) : RecyclerView.Adapter<BaseItemsLoadingAdapter.PlaceholderViewHolder>() {

    @LayoutRes
    abstract fun getLayoutRes(template: SectionTemplate): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceholderViewHolder {
        return PlaceholderViewHolder(parent.inflate(getLayoutRes(template)))
    }

    override fun getItemCount() = ITEM_COUNT
    override fun onBindViewHolder(holder: PlaceholderViewHolder, position: Int) {}

    class PlaceholderViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
