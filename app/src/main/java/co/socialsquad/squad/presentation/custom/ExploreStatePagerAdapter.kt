package co.socialsquad.squad.presentation.custom

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class ExploreStatePagerAdapter(
        fm: FragmentManager,
        val fragments: MutableList<Fragment>,
        lc: Lifecycle
) :
        FragmentStateAdapter(fm, lc) {

    private var titles: Array<String>? = null

    fun getItem(position: Int): Fragment = fragments[position]

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

    override fun containsItem(itemId: Long): Boolean {
        fragments.forEach {
            if (it.hashCode().toLong() == itemId) {
                return true
            }
        }
        return false
    }

    override fun getItemId(position: Int): Long {
        return fragments[position].hashCode().toLong()
    }


}
