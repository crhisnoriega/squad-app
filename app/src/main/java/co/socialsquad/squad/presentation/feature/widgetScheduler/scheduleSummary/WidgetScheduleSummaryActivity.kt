package co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.Header
import co.socialsquad.squad.domain.model.PositionItem
import co.socialsquad.squad.domain.model.ScalaSection
import co.socialsquad.squad.domain.model.SimpleItem
import co.socialsquad.squad.domain.model.Validation
import co.socialsquad.squad.presentation.feature.widgetScheduler.annotation.AnnotationActivity
import co.socialsquad.squad.presentation.feature.widgetScheduler.di.WidgetScheduleModule
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.schedule.WidgetSchedulerActivity
import co.socialsquad.squad.presentation.feature.widgetScheduler.schedule.WidgetSchedulerActivity.Companion.REQUEST_SELECT_DATE
import co.socialsquad.squad.presentation.feature.widgetScheduler.validation.TimetableValidationActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_widget_schedule_summary.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import java.text.SimpleDateFormat
import java.util.Date

private const val EXTRA_TIMETABLE_ID = "extra_timetable_id"
private const val EXTRA_OBJECT_ID = "extra_object_id"

class WidgetScheduleSummaryActivity : BaseActivity() {

    override val modules: List<Module> = listOf(WidgetScheduleModule.instance)
    override val contentView = R.layout.activity_widget_schedule_summary

    private val viewModel by viewModel<ScheduleSummaryViewModel>()
    private val timetableId: Long by extra(EXTRA_TIMETABLE_ID)
    private val objectId: Long by extra(EXTRA_OBJECT_ID)
    private val formatter = SimpleDateFormat("HH:mm")
    private val dateFormatInternal = SimpleDateFormat("yyyy-MM-dd")

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupObservables()
        setupListeners()
        checkEnableButton()
        viewModel.fetchScheduleSummary(timetableId = timetableId, objectId = objectId)
    }

    private fun setupListeners() {
        ibClose.setOnClickListener { finish() }
        dateClickable.setOnClickListener {
            WidgetSchedulerActivity.startForResult(
                    this, timetableId, objectId,
                    viewModel.formattedDate,
                    viewModel.positionItem,
                    viewModel.shift,
                    REQUEST_SELECT_DATE
            )
        }
        notesClickable.setOnClickListener {
            AnnotationActivity.startForResult(this, viewModel.notes)
        }
        imgDateDelete.setOnClickListener {
            viewModel.shift = null
            viewModel.positionItem = null
            viewModel.formattedDate = null
            showPositionItem(
                    viewModel.formattedDate,
                    viewModel.shift?.title,
                    viewModel.shift?.startDate,
                    viewModel.shift?.endDate,
                    viewModel.positionItem
            )
        }
        imgNotesDelete.setOnClickListener {
            viewModel.notes = ""
            showNotes(viewModel.notes)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SELECT_DATE -> {
                if (resultCode == RESULT_OK) {
                    data?.let {
                        viewModel.formattedDate =
                                Date(it.getLongExtra(WidgetSchedulerActivity.RESULT_SELECTED_DATE, 0L))
                        viewModel.positionItem =
                                it.getParcelableExtra(WidgetSchedulerActivity.RESULT_SELECTED_POSITION)
                        viewModel.shift =
                                it.getParcelableExtra(WidgetSchedulerActivity.RESULT_SELECTED_SHIFT)

                        if (viewModel.formattedDate != null
                                && viewModel.positionItem != null
                                && viewModel.shift != null
                        ) {
                            showPositionItem(
                                    viewModel.formattedDate,
                                    viewModel.shift!!.title,
                                    viewModel.shift!!.startDate,
                                    viewModel.shift!!.endDate,
                                    viewModel.positionItem
                            )
                        }
                        checkEnableButton()
                    }
                }
            }
            TimetableValidationActivity.REQUEST_VALIDATE -> {
                if (resultCode == TimetableValidationActivity.RESULT_CREATE_NEW_RESERVE ||
                        resultCode == TimetableValidationActivity.RESULT_RELOAD_RESERVE
                ) {
                    setResult(resultCode)
                    finish()
                }
            }
            AnnotationActivity.REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    data?.let {
                        viewModel.notes = it.getStringExtra(AnnotationActivity.RESULT_NOTES)
                        showNotes(viewModel.notes)
                    }
                }
            }
        }
    }

    private fun checkEnableButton() {
        btnAction.isEnabled =
                viewModel.formattedDate != null && !viewModel.shift?.title.isNullOrBlank() &&
                        viewModel.shift?.startDate != null && viewModel.shift?.endDate != null &&
                        viewModel.positionItem != null
    }

    @ExperimentalCoroutinesApi
    private fun setupObservables() {
        createFlow(viewModel.summary)
                .error {
                    // TODO
                }
                .loading {
                    view_loader.isVisible = true
                    nestedScroll.isVisible = false
                }
                .success {
                    it?.let { summary ->
                        showHeader(summary.timetable.header)
                        showSection(summary.timetable.section)
                        showAddress(summary.timetable.address)
                        showPositionItem(
                                viewModel.formattedDate,
                                viewModel.shift?.title,
                                viewModel.shift?.startDate,
                                viewModel.shift?.endDate,
                                viewModel.positionItem
                        )
                        showNotes(viewModel.notes)
                        showButton(summary.timetable.button, summary.validation)
                        view_loader.isVisible = false
                        nestedScroll.isVisible = true
                    }
                }
                .launch()
    }

    private fun showHeader(header: Header) {
        Glide.with(this)
                .load(header.icon.url)
                .crossFade()
                .into(imgHeaderDate)
        imgHeaderDate
        txtTitle.text = header.title
        txtSubtitle.text = header.subtitle
        txtHelper.text = header.helpText
    }

    private fun showSection(section: ScalaSection) {
        Glide.with(this)
                .load(section.icon.url)
                .crossFade()
                .into(imgObjectType)
        txtObjectTypeTitle.text = section.title + " *"
        txtObjectTypeShortName.text = section.item.shortTitle
        txtObjectTypeCustomFields.text = section.item.customFields
    }

    private fun showAddress(address: SimpleItem?) {
        txtAddressTitle.text = address?.title + " *"
        txtAddressLine1.text = address?.line1
        txtAddressLine2.text = address?.line2
    }

    private fun showPositionItem(
            formattedDate: Date?,
            shiftName: String?,
            startDate: Date?,
            endDate: Date?,
            item: PositionItem?
    ) {
        if (formattedDate == null || shiftName.isNullOrBlank() || startDate == null ||
                endDate == null || item == null
        ) {
            groupDateLabel.isVisible = true
            groupPosition.isVisible = false
            groupDate.isVisible = false
        } else {
            try {
                var dateformatLong = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM', 'YYYY")
                txtDateLine1.text = dateformatLong.format(formattedDate)
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                try {
                    var dateformatLong = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM', 'yyyy")
                    txtDateLine1.text = dateformatLong.format(formattedDate)
                } catch (e: Exception) {
                    FirebaseCrashlytics.getInstance().recordException(e)

                    try {
                        var dateformatLong = SimpleDateFormat("EEEE' - 'dd 'de 'MMMM")
                        txtDateLine1.text = dateformatLong.format(formattedDate)
                    } catch (e: Exception) {
                        FirebaseCrashlytics.getInstance().recordException(e)
                    }
                }
            }

            txtDateLine2.text =
                    "$shiftName \u2020 ${formatter.format(startDate)} - ${formatter.format(endDate)}"

            txtPositionTitle.text = item.title
            item.description?.let {
                txtPositionType.text = it
            } ?: run {
                txtPositionType.text = ""
            }

            groupDateLabel.isVisible = false
            groupPosition.isVisible = true
            groupDate.isVisible = true
        }
    }

    private fun showNotes(notes: String) {
        if (notes.isBlank()) {
            groupNotesLabel.isVisible = true
            groupNotes.isVisible = false
            txtNotesSubtitle.text = ""
        } else {
            groupNotesLabel.isVisible = false
            groupNotes.isVisible = true
            txtNotesSubtitle.text = notes
        }
    }

    private fun showButton(text: String, validation: Validation) {
        btnAction.text = text
        btnAction.setOnClickListener {
            if (viewModel.formattedDate != null && viewModel.shift != null && viewModel
                            .positionItem != null
            ) {
                TimetableValidationActivity.startForResult(
                        this,
                        timetableId,
                        objectId,
                        validation,
                        TimetableCreation(
                                dateFormatInternal.format(viewModel.formattedDate),
                                viewModel.shift!!.id,
                                viewModel.positionItem!!.id,
                                ""
                        )
                )
            }
        }
    }

    companion object {

        const val REQUEST_CODE = 1199
        const val RESULT_CREATE_NEW_RESERVE = 111
        const val RESULT_RELOAD_RESERVE = 222

        fun startForResult(activity: Activity, timetableId: Long, objectId: Long) {
            activity.startActivityForResult(
                    createIntent(activity, timetableId, objectId),
                    REQUEST_CODE
            )
        }

        private fun createIntent(context: Context, timetableId: Long, objectId: Long): Intent {
            return Intent(context, WidgetScheduleSummaryActivity::class.java).apply {
                putExtra(EXTRA_TIMETABLE_ID, timetableId)
                putExtra(EXTRA_OBJECT_ID, objectId)
            }
        }
    }
}