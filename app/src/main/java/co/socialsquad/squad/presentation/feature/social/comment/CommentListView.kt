package co.socialsquad.squad.presentation.feature.social.comment

interface CommentListView {
    fun insertNewComment(commentViewModel: CommentViewModel)
    fun removeComment(viewModel: CommentViewModel)
    fun setComments(comments: List<CommentViewModel>)
    fun addComments(comments: List<CommentViewModel>)
    fun showLoadingComments()
    fun hideLoadingComments()
    fun updateComment(commentViewModel: CommentViewModel)
    fun updateCommentCount(count: Int)
}
