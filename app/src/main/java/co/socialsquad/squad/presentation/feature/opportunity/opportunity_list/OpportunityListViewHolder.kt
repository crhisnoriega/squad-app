package co.socialsquad.squad.presentation.feature.opportunity.opportunity_list

import android.os.Parcelable
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Header
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityEmptyState
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ITEM_OPPORTUNITY_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ItemOpportunityViewModel
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.OpportunitySubmissionViewHolder
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ShowAllOpportunitySubmissionsActivity
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.item_opportunity_list.view.*

const val ITEM_OPPORTUNITY_LIST_ITEM = R.layout.item_opportunity_list

@Parcelize
data class OpportunityInitialsColors(
    val primaryColor: String?,
    val secondaryColor: String?
) : Parcelable

data class OpportunityListViewModel(
    val opportunity: Opportunity,
    val colors: OpportunityInitialsColors
) : ViewModel

class OpportunityListViewHolder(
    itemView: View,
    private val opportunityListPresenter: OpportunityListPresenter,
    private val objectType: OpportunityObjectType
) : ViewHolder<OpportunityListViewModel>(itemView) {

    override fun recycle() {
    }

    override fun bind(viewModel: OpportunityListViewModel) = with(itemView) {

        setupHeader(viewModel.opportunity.opportunityList.header)
        setupButtons(viewModel.opportunity)
        if (viewModel.opportunity.opportunityList.opportunityObjects.isNullOrEmpty()) {
            groupListEmpty.isVisible = true
            groupObjectsList.isVisible = false
            llShowAllObjects.isVisible = false
            setupEmptyState(viewModel.opportunity.opportunityList.opportunityEmptyState!!)
        } else {
            groupListEmpty.isVisible = false
            groupObjectsList.isVisible = true
            setupObjectsList(viewModel)
        }
    }

    private fun setupHeader(header: Header) = with(itemView) {
        tvTitle.setText(header.title)
        tvSubtitle.setText(header.subtitle)

        // TODO set icon from url
    }

    private fun setupEmptyState(emptyState: OpportunityEmptyState) = with(itemView) {
        tvEmptyMessage.setText(emptyState.text)
        btEmptyAction.setText(emptyState.buttonText)

        // TODO set icon from url
    }

    private fun setupObjectsList(viewModel: OpportunityListViewModel) = with(itemView) {

        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ItemOpportunityViewModel -> ITEM_OPPORTUNITY_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ITEM_OPPORTUNITY_VIEW_MODEL_ID -> OpportunitySubmissionViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        }

        val showAllButtonVisible =
            viewModel.opportunity.opportunityList.opportunityObjects.size > viewModel.opportunity.opportunityList.size

        with(rvObjects) {
            layoutManager = LinearLayoutManager(context)
            adapter = RecyclerViewAdapter(viewHolderFactory).apply {

                val items = viewModel.opportunity.opportunityList.opportunityObjects
                    .map {
                        OpportunityListMapper(
                            context,
                            opportunityListPresenter,
                            viewModel.opportunity.id,
                            viewModel.opportunity.parentObjectId,
                            objectType,
                            viewModel.colors
                        ).mapOpportunityObject(it)
                    }
                    .take(viewModel.opportunity.opportunityList.size)
                addItems(items)
            }
            setHasFixedSize(true)

            addItemDecoration(ListDividerItemDecoration(context, showAllButtonVisible))
        }

        llShowAllObjects.isVisible = showAllButtonVisible
    }

    private fun setupButtons(opportunity: Opportunity) = with(itemView) {
        btEmptyAction.setOnClickListener {
            opportunityListPresenter.getOpportunityAction(opportunity)
        }

        llShowAllObjects.setOnClickListener {

        }

        ivInformationIcon.setOnClickListener {
            opportunityListPresenter.getOpportunityInfo(opportunity)
        }
    }
}
