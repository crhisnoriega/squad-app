package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.SEGMENT_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_segment_metrics.*
import javax.inject.Inject

class SegmentMetricsActivity : BaseDaggerActivity(), SegmentMetricsView {

    @Inject
    lateinit var segmentMetricsPresenter: SegmentMetricsPresenter

    private lateinit var rvMetrics: RecyclerView
    private lateinit var srlRefresh: SwipeRefreshLayout

    private var metricsAdapter: RecyclerViewAdapter? = null
    private var loading: AlertDialog? = null
    private var miFinish: MenuItem? = null

    override fun onDestroy() {
        segmentMetricsPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        segmentMetricsPresenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segment_metrics)

        rvMetrics = segment_metrics_rv_metrics
        srlRefresh = segment_metrics_srl_refresh

        segmentMetricsPresenter.onCreate(intent)
    }

    override fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = getString(R.string.segment_metrics_title)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_segment_metrics, menu)
        miFinish = menu.findItem(R.id.segment_metrics_mi_finish)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.segment_metrics_mi_finish -> {
            segmentMetricsPresenter.onFinishClicked()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun setupList() {
        metricsAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is MetricViewModel -> SEGMENT_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                SEGMENT_VIEW_MODEL_ID -> MetricViewHolder(view, onMetricClicked)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        with(rvMetrics) {
            layoutManager = LinearLayoutManager(this@SegmentMetricsActivity)
            adapter = metricsAdapter
            setItemViewCacheSize(0)
            setHasFixedSize(true)
            addItemDecoration(ListDividerItemDecoration(context))
        }
    }

    val onMetricClicked = object : ViewHolder.Listener<MetricViewModel> {
        override fun onClick(viewModel: MetricViewModel) {
            segmentMetricsPresenter.onMetricClicked(viewModel)
        }
    }

    override fun setupSwipeRefresh() {
        srlRefresh.isEnabled = false
        srlRefresh.setOnRefreshListener { segmentMetricsPresenter.onRefresh() }
    }

    override fun showErrorMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(textResId), onDismissListener)
    }

    override fun setItems(metrics: List<MetricViewModel>) {
        val list = metrics.toMutableList<ViewModel>()
        metricsAdapter?.setItems(list)
    }

    override fun enableRefreshing() {
        srlRefresh.isEnabled = true
    }

    override fun stopRefreshing() {
        srlRefresh.isRefreshing = false
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun openSegmentMetric(metric: MetricViewModel, requestCode: Int) {
        val intent = Intent(this, SegmentValuesActivity::class.java)
        intent.putExtra(EXTRA_METRICS, metric)
        startActivityForResult(intent, requestCode)
    }

    override fun finishWithResult(resultCode: Int, intent: Intent) {
        setResult(resultCode, intent)
        finish()
    }

    override fun updateItem(itemViewModel: MetricViewModel) {
        metricsAdapter?.update(itemViewModel)
    }

    override fun enableFinishButton(enabled: Boolean) {
        miFinish?.isEnabled = enabled
    }

    override fun setPotentialReachArcColor(color: String) {
        rcReach.setStrokeColor(color)
    }

    override fun setPotentialReachValue(reach: PotentialReachViewModel) {
        rcReach.setReach(reach.circles)
        tvPotentialReach.text = getString(R.string.audience_potential_reach_members, reach.reachedMembers, reach.totalMember)
    }
}
