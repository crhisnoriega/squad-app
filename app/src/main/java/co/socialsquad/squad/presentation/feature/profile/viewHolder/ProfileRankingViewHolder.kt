package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.net.Uri
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.ranking.RankingActivity
import co.socialsquad.squad.presentation.feature.ranking.listing.ElementsListingActivity
import co.socialsquad.squad.presentation.feature.ranking.listing.RANKING_LIST_TYPE
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemRankingInfoViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.item_card_points_profile.view.*
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.*
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.containerLayout
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.info
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.rlContactCircle2
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.short_description
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.short_title


class ProfileRankingViewHolder(itemView: View) : ViewHolder<ProfileRankingViewModel>(itemView) {


    override fun recycle() {
    }

    override fun bind(viewModel: ProfileRankingViewModel) {
        itemView.info.setOnClickListener {
            //RankingActivity.start(itemView.context)
            if (viewModel.number_of_ranks?.toInt()!! > 1) {
                ElementsListingActivity.start(
                        itemView.context,
                        RANKING_LIST_TYPE.RANKINK_INFO,
                        listOf<ItemRankingInfoViewModel>(), "", isInRank = viewModel.user_in_rank, itemsShimmer = viewModel.number_of_ranks!!,
                        titleAPI = viewModel.title!!
                )
            } else {
                RankingActivity.start(itemView.context, viewModel.rank_id)
            }


        }

        itemView.containerLayout.setOnClickListener {
            if (viewModel.number_of_ranks?.toInt()!! > 1) {
                ElementsListingActivity.start(
                        itemView.context,
                        RANKING_LIST_TYPE.RANKINK_INFO,
                        listOf<ItemRankingInfoViewModel>(), "", isInRank = viewModel.user_in_rank, itemsShimmer = viewModel.number_of_ranks!!,
                        titleAPI = viewModel.title!!
                )
            } else {
                RankingActivity.start(itemView.context, viewModel.rank_id)
            }

//            if (viewModel.number_of_ranks?.toInt()!! > 1) {
//                ElementsListingActivity.start(itemView.context, RANKING_LIST_TYPE.RANKINK_INFO, listOf<ItemRankingInfoViewModel>(), "", isInRank = false, itemsShimmer = viewModel.number_of_ranks!!)
//            } else {
//                RankingActivity.start(itemView.context, viewModel.rank_id)
//            }
        }

        itemView.short_title.text = viewModel.title
        itemView.short_description.text = viewModel.subtitle


        itemView.rlContactCircle2.backgroundTintList =
            ColorUtils.getCompanyColor(itemView.context)?.let { ColorUtils.stateListOf(it) }

//        if (viewModel.user_in_rank) {
//            if (viewModel.number_of_ranks?.toInt()!! <= 1) {
//                itemView.short_description.text = viewModel.position + " • " + viewModel.points
//            } else {
//                itemView.short_description.text = viewModel.subtitle
//            }
//        } else {
//            itemView.short_description.text = viewModel.participants
//        }

//        if (viewModel.icon?.endsWith("png")!!) {
//            Glide.with(itemView.context).load(viewModel.icon)
//                    .circleCrop()
//                    .into(itemView.imgSection2)
//        } else {
//            GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.icon), itemView.imgSection2)
//        }
    }
}
