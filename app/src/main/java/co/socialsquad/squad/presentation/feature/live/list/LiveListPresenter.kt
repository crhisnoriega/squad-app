package co.socialsquad.squad.presentation.feature.live.list

import android.app.Activity
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.entity.Recommendation
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LiveListPresenter @Inject constructor(
    private val view: LiveListView,
    private val profileRepository: ProfileRepository,
    private val liveRepository: LiveRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var qualifications: List<Qualification> = listOf()
    private var currentRecommending: LiveListItemViewModel? = null
    private var page = 1
    private var isLoading = true
    private var isComplete = false

    fun onViewCreated() {
        tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_LIVE_LIST)
        subscribe()
        getQualifications()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
        getQualifications()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            liveRepository.getLives(page, ended = true)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }
                .doOnNext { if (it.next == null) isComplete = true }
                .map { it.results.orEmpty().map { LiveListItemViewModel(it) } }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            val shouldClearList = page == 1
                            view.addItems(it, shouldClearList)
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.live_list_error_get_lives)
                    }
                )
        )
    }

    private fun getQualifications() {
        compositeDisposable.add(
            profileRepository.getQualifications()
                .subscribe(
                    { qualifications = it },
                    { it.printStackTrace() }
                )
        )
    }

    fun onLiveClicked(viewModel: LiveListItemViewModel) {
        compositeDisposable.add(
            liveRepository.markMediaViewed(viewModel.videoPk)
                .subscribe({}, Throwable::printStackTrace)
        )
        view.openVideo(viewModel.videoUrl)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED || data == null) return

        if (requestCode == SegmentValuesPresenter.REQUEST_CODE_VALUES) {
            (data.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                recommendLive(it.values.map { it.value })
            }
        }
    }

    fun onRecommending(viewModel: LiveListItemViewModel) {
        currentRecommending = viewModel
    }

    private fun recommendLive(values: List<String>) {
        val pks = mutableListOf<Int>()
        values.forEach { name ->
            qualifications.findLast { it.name == name }?.let {
                pks.add(it.pk)
            }
        }
        currentRecommending?.let {
            val recommendation = RecommendationRequest(it.pk, Recommendation.LIVE.name.toLowerCase(), pks)
            recommend(recommendation)
        }
    }

    private fun recommend(recommendation: RecommendationRequest) {
        compositeDisposable.add(
            profileRepository.postRecommendation(recommendation)
                .subscribe(
                    { view.showMessage(R.string.profile_recommendation_success) },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.profile_recommendation_error)
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }
}
