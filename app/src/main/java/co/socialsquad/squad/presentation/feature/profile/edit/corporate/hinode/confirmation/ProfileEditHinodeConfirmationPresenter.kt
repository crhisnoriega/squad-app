package co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation

import android.os.Bundle
import co.socialsquad.squad.data.entity.UserCompanyCID
import co.socialsquad.squad.data.entity.integration.RecruitRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_CID_CONFIRMATION
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CID_CONFIRMATION
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProfileEditHinodeConfirmationPresenter @Inject
constructor(
    private val profileEditHinodeConfirmationView: ProfileEditHinodeConfirmationView,
    private val loginRepository: LoginRepository,
    private val signupRepository: SignupRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_CID_CONFIRMATION, Bundle())
    }

    fun onInput(id: String) {
        profileEditHinodeConfirmationView.setConfirmButtonEnabled(!id.isEmpty())
    }

    fun onConfirmClicked(cid: String) {
        val recruitRequest = RecruitRequest()
        recruitRequest.subdomain = loginRepository.subdomain
        recruitRequest.cid = cid
        tagWorker.tagEvent(TAG_TAP_CID_CONFIRMATION, Bundle())

        compositeDisposable.add(
            signupRepository.getRecruit(recruitRequest)
                .flatMap { userCompany1 ->
                    val userCompany = loginRepository.userCompany
                    userCompany!!.cid = userCompany1.cid
                    userCompany.sponsorCid = userCompany1.sponsorCid
                    userCompany.qualification = userCompany1.qualification
                    val pk = userCompany.pk

                    val cidRequestModel = UserCompanyCID(pk, cid)
                    signupRepository.editUserCID(pk, cidRequestModel)
                }
                .doOnSubscribe { profileEditHinodeConfirmationView.showLoading() }
                .doOnTerminate { profileEditHinodeConfirmationView.hideLoading() }
                .subscribe(
                    { userCompany ->
                        loginRepository.userCompany = userCompany
                        profileEditHinodeConfirmationView.showNavigation()
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        profileEditHinodeConfirmationView.showErrorMessageFailedToVerifyId()
                    }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
