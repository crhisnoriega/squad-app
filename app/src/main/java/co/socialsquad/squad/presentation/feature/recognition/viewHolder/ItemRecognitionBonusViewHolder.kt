package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_recognition_bonus.view.*


class ItemRecognitionBonusViewHolder(
        itemView: View,
        var onClickAction: ((item: ItemRecognitionBonusViewModel) -> Unit))
    : ViewHolder<ItemRecognitionBonusViewModel>(itemView) {

    override fun bind(viewModel: ItemRecognitionBonusViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        viewModel.bonusData?.highlight?.let {
            itemView.txtTitle.isVisible = true
            itemView.txtTitle.text = it
        }


        itemView.txtSubtitle.text = viewModel.bonusData.title
        itemView.txtSubtitle2.text = viewModel.bonusData.description?.trim()

        itemView.bonusCover.visibility = View.VISIBLE

        itemView.dividerItemTop.isVisible = viewModel.bonusData.first.not()
        // Glide.with(itemView.context).load(viewModel.rankingData.icon).into(itemView.imgIcon)

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.bonusData.media_cover), itemView.bonusCover)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}