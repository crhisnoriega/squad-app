package co.socialsquad.squad.presentation.feature.hub.dashboard

import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.DashboardRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.feature.hub.dashboard.charts.ChartViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DashboardPresenter @Inject constructor(
    private val loginRepository: LoginRepository,
    private val dashboardRepository: DashboardRepository,
    private val view: DashboardView
) {
    private var isLoading = false

    private val compositeDisposable = CompositeDisposable()

    fun onViewCreated() {
        subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            dashboardRepository.getDashboard()
                .doOnSubscribe {
                    isLoading = true
                    view.showLoading()
                }.doOnTerminate {
                    view.hideLoading()
                    isLoading = false
                }.subscribe(
                    { chartList ->
                        val list = chartList.results.map {
                            ChartViewModel(it, loginRepository.squadColor)
                        }
                        view.setItems(list)
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.hub_dashboard_get_error)
                    }
                )
        )
    }

    fun onRefresh() {
        compositeDisposable.clear()
        view.setItems(listOf())
        subscribe()
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
