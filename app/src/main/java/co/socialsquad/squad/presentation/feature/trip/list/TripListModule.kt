package co.socialsquad.squad.presentation.feature.trip.list

import dagger.Binds
import dagger.Module

@Module
interface TripListModule {
    @Binds
    fun view(view: TripListFragment): TripListView
}
