package co.socialsquad.squad.presentation.feature.points

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.points.listing.POINTS_LIST_TYPE
import co.socialsquad.squad.presentation.feature.points.listing.PointsElementsListingActivity
import co.socialsquad.squad.presentation.feature.points.model.AreaData
import co.socialsquad.squad.presentation.feature.points.model.CriteriaData
import co.socialsquad.squad.presentation.feature.points.model.SystemData
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemAreaPointsViewHolder
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemAreaPointsViewModel
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemCriteriaPointsViewHolder
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemCriteriaPointsViewModel
import co.socialsquad.squad.presentation.feature.points.viewModel.PersonalPointsViewModel
import co.socialsquad.squad.presentation.feature.ranking.details.RankingCriteriaDetailsActivity
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import co.socialsquad.squad.presentation.feature.recognition.viewHolder.ItemShowAllRecognitionViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_explore_item_object.*
import kotlinx.android.synthetic.main.activity_points.*
import kotlinx.android.synthetic.main.activity_points.levelName
import kotlinx.android.synthetic.main.activity_points.nestedScroll
import kotlinx.android.synthetic.main.activity_points.toolbar
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module


class PointsActivity : BaseActivity() {
    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_points

    private var currentPage: Int = 0

    private val viewModel by viewModel<PersonalPointsViewModel>()
    private val systemDataExtra by extra<SystemData>(SYSTEM_DATA)
    private val points_system_id by extra<String>(POINTS_SYSTEM_ID)

    private lateinit var systemData: SystemData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureObservers()
        configureToolbar()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.WHITE
        }

        try {
            if (systemDataExtra != null) {
                systemData = systemDataExtra
            }
        } catch (e: java.lang.IllegalArgumentException) {

        }

        if (this::systemData.isInitialized) {
            loadSystemData()
        } else {
            viewModel.fetchSystemPointsById(points_system_id)
        }

    }

    private fun loadSystemData() {
        updateHeader(
            systemData.title!!,
            systemData.points_system_header.subtitle!!,
            systemData.points_system_header.description!!
        )

        tvValue.text = systemData.points_system_header.title
        tvValue.setTextColor(ColorUtils.parse(ColorUtils.getCompanyColor(this)!!))
        levelName.text = systemData.points_system_header.subtitle

        if (systemData.area?.size!! > 1) {
            criteriasGroup.visibility = View.GONE
            configureAreasList(systemData.area)
        } else {
            areaGroup.visibility = View.GONE
            configureCriteriasList(systemData.area?.get(0)?.criteria?.data!!)
        }

        Handler().postDelayed({
            positionsShimmer.visibility = View.GONE
        }, 2000)
    }

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.systemsData
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        systemData = it.data!!
                        loadSystemData()
                    }
                    Status.ERROR -> {

                    }
                }

            }
        }


    }

    private fun populateData(planData: PlanData?) {
        var currentPosition = 1000
        planData?.levels?.data?.forEachIndexed { index, levelData ->
            levelData.user_info?.current_level?.let {
                if (it) {
                    currentPosition = index
                }
            }
            levelData.isBlocked = index > currentPosition

//            if (planData.user_in_plan!!) {
//                levelData.requirements?.data?.forEach {
//                    it.completed = (index < currentPosition)
//                }
//            } else {
//                levelData.requirements?.data?.forEach {
//                    it.completed = false
//                }
//            }
        }
    }

    private fun configureCriteriasList(criterias: List<CriteriaData>) {
        criteriaLabel.text = "Critérios"


        rvCriterias.apply {
            layoutManager =
                LinearLayoutManager(this@PointsActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemCriteriaPointsViewModel -> R.layout.item_points_criteria
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> =
                    when (viewType) {
                        R.layout.item_points_criteria -> ItemCriteriaPointsViewHolder(view) {
                            RankingCriteriaDetailsActivity.start(
                                this@PointsActivity,
                                it.criteriaData?.id,
                                false,
                                systemData.area?.get(0)?.id

                            )
                        }

                        R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(
                            view
                        ) {

                        }

                        else -> throw IllegalArgumentException()
                    }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ViewModel>()

            criterias?.forEach {
                elements.add(ItemCriteriaPointsViewModel(it))
            }

            factoryAdapter.addItems(elements)
        }
    }


    private fun configureAreasList(areas: List<AreaData>?) {
        areasLabel.text = "Areas"


        rvAreas.apply {
            layoutManager =
                LinearLayoutManager(this@PointsActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemCriteriaPointsViewModel -> R.layout.item_points_criteria
                        is ItemAreaPointsViewModel -> R.layout.item_points_area
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> =
                    when (viewType) {

                        R.layout.item_points_area -> ItemAreaPointsViewHolder(view) {

                            PointsElementsListingActivity.start(
                                this@PointsActivity,
                                POINTS_LIST_TYPE.CRITERIA,
                                2,
                                "Area",
                                it.areaData
                            )
                        }
                        R.layout.item_points_criteria -> ItemCriteriaPointsViewHolder(view) {
                            RankingCriteriaDetailsActivity.start(
                                this@PointsActivity,
                                null,
                                false,
                                it.criteriaData?.id
                            )
                        }

                        R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(
                            view
                        ) {

                        }

                        else -> throw IllegalArgumentException()
                    }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ViewModel>()

            if (areas != null) {
                elements.addAll(
                    areas.map {
                        ItemAreaPointsViewModel(it)
                    }
                )
            }

            factoryAdapter.addItems(elements)
        }
    }


    private fun updateHeader(actionBartitle: String, title: String, subtitle: String) {
        tvTitle.text = actionBartitle
        levelDescription.text = subtitle
        levelName.text = title
        levelDescription.text = subtitle
        tvTitle.text = actionBartitle

        if (systemData.area?.size!! > 1) {
            tvSubtitleCollapsed.text = title
        } else {
            tvSubtitleCollapsed.text =
                systemData.area?.get(0)?.area_header?.criteria_quantity
        }


        nestedScroll.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->

            Log.i("scroll", "scrollX: $scrollX scrollY: $scrollY")

            val scrollBounds = Rect()
            nestedScroll.getHitRect(scrollBounds)
            Log.i("scroll", "isVisible: ${levelName.getLocalVisibleRect(scrollBounds)}")

            tvSubtitleCollapsed.isVisible = levelName.getLocalVisibleRect(scrollBounds).not()
        }

    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)


        toolbar.title = ""

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun showLoading() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.GRAY
        }

        optInRecLoading.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        iconRecLoading.startAnimation(rotation)
    }


    companion object {
        const val SYSTEM_DATA = "SYSTEM_DATA"
        const val POINTS_SYSTEM_ID = "POINTS_SYSTEM_ID"
        const val OBJECT_TYPE = "object_type"
        const val TOOL_TYPE = "tool_type"
        const val INITIALS_LETTER = "initials_letter"
        const val NOME_LEAD = "nome_lead"

        fun start(
            context: Context, systemData: SystemData? = null, points_system_id: String? = null
        ) {
            context.startActivity(Intent(context, PointsActivity::class.java).apply {
                putExtra(SYSTEM_DATA, systemData)
                putExtra(POINTS_SYSTEM_ID, points_system_id)
            })
        }
    }
}

class CarouselViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val levelImage = itemView.findViewById<ImageView>(R.id.levelImage)
    val circularProgressbar = itemView.findViewById<ProgressBar>(R.id.circularProgressbar)
}