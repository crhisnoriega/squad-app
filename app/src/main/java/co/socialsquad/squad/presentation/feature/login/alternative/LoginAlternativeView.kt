package co.socialsquad.squad.presentation.feature.login.alternative

import co.socialsquad.squad.data.entity.Company

interface LoginAlternativeView {
    fun setHintText(messageStringId: Int)
    fun setFieldText(text: String)
    fun setErrorMessage(messageStringId: Int)
    fun setErrorMessage(text: String)
    fun showSuccessScreen(type: LoginAlternativePresenter.Type)
    fun setHelpText(messageStringId: Int, parameter: String? = null)
    fun startLoading()
    fun stopLoading()
    fun setupHeader(company: Company?)
}
