package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
        @SerializedName("lat") val latitude: Double,
        @SerializedName("lng") val longitude: Double,
        @SerializedName("pin") val pin: String?
) : Parcelable
