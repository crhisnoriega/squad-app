package co.socialsquad.squad.presentation.feature.profile.edit

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.hardware.Camera
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.profile.DoubleClickListener
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.bitmapCrossFade
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.fragment_edit_profile_picture.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.util.*

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3

enum class CAMERA_STATUS {
    OPEN_CAMERA, CLOSE_CAMERA, REQUEST_PERMISSION, WITH_OUT_PERMISSION, PERMISSION, PICTURE_TAKEN
}

class ProfilePictureFragment(
) : Fragment() {
    private var imageCapture: ImageCapture? = null
    private var mCamera: Camera? = null

    private var status = CAMERA_STATUS.WITH_OUT_PERMISSION

    private val viewModel by viewModel<ProfileEditViewModel>()

    private var isFront = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureButtons()
        configureObservables()
        configureAvatarImage()
    }

    private fun configureAvatarImage() {
        var urlAvatar = viewModel.userCompany?.user?.avatar
        if (urlAvatar.isNullOrBlank().not()) {
            Glide.with(requireContext()).load(urlAvatar).circleCrop().into(image_loaded)
            image_loaded.isVisible = true
            camera_place_holder.isVisible = false
        }
    }

    private fun configureObservables() {
        viewModel.state.observe(requireActivity(), androidx.lifecycle.Observer {
            when (it.name) {
                "finish" -> activity?.finish()
            }
        })

    }

    private fun configureButtons() {
        btnSalvar.setOnClickListener {
            //viewModel.saveAvatar()
            activity?.setResult(1001, Intent().apply {
                putExtra("PICTURE", viewModel.currentPhoto)
            })
            activity?.finish()
        }

        camera_layout.setOnClickListener(object : DoubleClickListener() {
            override fun onDoubleClick() {
                isFront = if (isFront) {
                    startCamera(CameraSelector.DEFAULT_BACK_CAMERA)
                    false
                } else {
                    startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                    true
                }
            }
        })

        btnBack.setOnClickListener {
            activity?.finish()
        }


        btn_take_picture.setOnClickListener {

            when (status) {
                CAMERA_STATUS.PERMISSION, CAMERA_STATUS.WITH_OUT_PERMISSION -> {
                    if (checkPermission().not()) {
                        status = CAMERA_STATUS.WITH_OUT_PERMISSION
                        requestPermissions(
                            arrayOf(Manifest.permission.CAMERA),
                            REQUEST_PERMISSION_PICTURE
                        )
                    } else {
                        status = CAMERA_STATUS.PERMISSION
                        startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                    }
                }

                CAMERA_STATUS.OPEN_CAMERA -> takePhoto()

                CAMERA_STATUS.PICTURE_TAKEN -> {
                    doButtonTakePhoto()
                    startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                }
            }
        }

        btn_open_gallery.setOnClickListener {
            requestGallery()
        }


    }

    private fun doButtonTakePhoto() {
        btn_take_picture.text = "Tirar Foto"
        btn_take_picture.background =
            (context?.getDrawable(R.drawable.shape_button_kickoff_take_photo))
        btn_take_picture.setTextColor(Color.WHITE)
    }

    private fun doButtonTOpenCamera() {
        btn_take_picture.text = "Abrir Câmera"
        btn_take_picture.setBackgroundDrawable(context?.getDrawable(R.drawable.shape_button_kickoff))
        btn_take_picture.setTextColor(ColorUtils.parse("#757575"))
    }

    private fun doButtonAnotherPhoto() {
        btn_take_picture.text = "Tirar Outra Foto"
        btn_take_picture.setBackgroundDrawable(context?.getDrawable(R.drawable.shape_button_kickoff))
        btn_take_picture.setTextColor(ColorUtils.parse("#757575"))
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
    }

    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    private fun canSave(canSave: Boolean) {
        btnSalvar.isEnabled = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_picture, container, false)

    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_PERMISSION_PICTURE -> startCamera(
                CameraSelector.DEFAULT_FRONT_CAMERA
            )

            REQUEST_TAKE_PICTURE -> {
                val imageBitmap = data?.extras?.get("data") as Bitmap
//                Glide.with(this)
//                    .asBitmap()
//                    .load(imageBitmap)
//                    .bitmapCrossFade()
//                    .circleCrop()
//                    .into(img_picture)
//                btnConfirmar.isEnabled = true
            }

            REQUEST_CODE_PICK_IMAGE -> if (resultCode == AppCompatActivity.RESULT_OK) {
                data?.data?.let {
                    Glide.with(this)
                        .asBitmap()
                        .load(it)
                        .bitmapCrossFade()
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                isFirstResource: Boolean
                            ) = false

                            override fun onResourceReady(
                                resource: Bitmap,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                dataSource: DataSource,
                                isFirstResource: Boolean
                            ): Boolean {
                                val directory = activity?.cacheDir
                                val file = File(
                                    activity?.cacheDir,
                                    "avatar-${UUID.randomUUID().toString()}.jpg"
                                )

                                val inputStream = activity?.contentResolver?.openInputStream(it)!!
                                FileUtils.createFromInputStream(file, inputStream)

                                viewModel.currentPhoto = file
                                Log.i("camera", "set camera on result")
                                // profileEditPresenter.onResourceReady(uri)
                                // setResult(Activity.RESULT_OK, Intent())

                                canSave(true)

                                Handler().postDelayed({ doButtonTOpenCamera() }, 600)

                                return false
                            }
                        })
                        .circleCrop()
                        .into(image_loaded)

                    viewFinder.visibility = View.GONE
                    image_loaded.visibility = View.VISIBLE

                    status = CAMERA_STATUS.PICTURE_TAKEN

                }
            }
        }
    }

    private fun startCamera(cameraSelector: CameraSelector) {

        camera_layout_aaa.visibility = View.VISIBLE
        camera_place_holder.visibility = View.GONE
        viewFinder.visibility = View.VISIBLE
        image_loaded.visibility = View.GONE

        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                .setTargetResolution(Size(400, 600))
                .build()


            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageCapture
                )

                doButtonTakePhoto()

                status = CAMERA_STATUS.OPEN_CAMERA
            } catch (exc: Exception) {
                Log.e("camera", "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    override fun onResume() {
        super.onResume()

        doButtonTOpenCamera()
        status = CAMERA_STATUS.WITH_OUT_PERMISSION

        Log.i("camera", "onResume")

        Handler().postDelayed({
            if (viewModel.currentPhoto != null) {

                val savedUri = Uri.fromFile(viewModel.currentPhoto)

                Glide.with(requireActivity())
                    .asBitmap()
                    .load(savedUri)
                    .bitmapCrossFade()
                    .circleCrop()
                    .into(image_loaded)

                viewFinder.visibility = View.GONE
                image_loaded.visibility = View.VISIBLE

                doButtonAnotherPhoto()
            }
        }, 500)

    }

    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // Create time-stamped output file to hold the image
        val photoFile = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "avatar-${UUID.randomUUID().toString()}.jpg"
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile)
            .setMetadata(ImageCapture.Metadata().apply {
                isReversedHorizontal = true
            }).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e("camera", "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)

                    viewFinder.visibility = View.GONE
                    image_loaded.visibility = View.VISIBLE

                    Glide.with(requireActivity())
                        .asBitmap()
                        .load(savedUri)
                        .bitmapCrossFade()
                        .circleCrop()
                        .into(image_loaded)

                    doButtonAnotherPhoto()

                    status = CAMERA_STATUS.PICTURE_TAKEN

                    viewModel.currentPhoto = photoFile
                    canSave(true)
                }
            })
    }


}
