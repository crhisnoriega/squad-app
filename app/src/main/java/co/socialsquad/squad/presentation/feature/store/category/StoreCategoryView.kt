package co.socialsquad.squad.presentation.feature.store.category

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.ProductListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListItemViewModel

interface StoreCategoryView {
    fun setupToolbar(title: String)
    fun setupCover(image: String)
    fun setupList()
    fun setupSwipeRefresh()
    fun setItems(categories: List<ViewModel>)
    fun showMessage(resId: Int)
    fun showLoading()
    fun hideLoading()
    fun openCategory(category: StoreCategoryListItemViewModel)
    fun openProducts(productListViewModel: ProductListViewModel)
}
