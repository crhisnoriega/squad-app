package co.socialsquad.squad.presentation.feature.opportunity

import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityInfoActivity
import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityInfoView
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ShowAllOpportunitySubmissionsActivity
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ShowAllOpportunitySubmissionsView
import dagger.Binds
import dagger.Module

@Module
abstract class OpportunityModule {
    @Binds
    abstract fun view(activity: OpportunityActivity): OpportunityView

    @Binds
    abstract fun showAllOpportunitySubmissionsView(activity: ShowAllOpportunitySubmissionsActivity): ShowAllOpportunitySubmissionsView

    @Binds
    abstract fun opportunityInfoView(activity: OpportunityInfoActivity): OpportunityInfoView
}
