package co.socialsquad.squad.presentation.feature.hub.intranet

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.VirtualOfficeResponse
import co.socialsquad.squad.presentation.feature.hub.intranet.webview.IntranetWebviewActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_intranet.*
import javax.inject.Inject

class IntranetFragment : Fragment(), IntranetView {

    companion object {
        const val REQUEST_CODE_COMPANY_LOGIN = 123
    }

    @Inject
    lateinit var presenter: IntranetPresenter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_intranet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (::presenter.isInitialized) presenter.onVisibilityChanged(isVisibleToUser)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode)
    }

    override fun showCompanyLoginScreen() {
//        startActivityForResult(Intent(activity, CompanyLoginActivity::class.java), REQUEST_CODE_COMPANY_LOGIN)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun setupWebView(webUrl: String, client: WebViewClient) {
//        wvOffice.apply {
//            webViewClient = client
//            settings.javaScriptEnabled = true
//            loadUrl(webUrl)
//            setOnKeyListener { _, keyCode, _ ->
//                if (keyCode == KeyEvent.KEYCODE_BACK && wvOffice.canGoBack()) {
//                    wvOffice.goBack()
//                    true
//                } else false
//            }
//        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun showLoading() {
        vLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        if (vLoading != null) {
            vLoading.visibility = View.GONE
        }
    }

    override fun setupOffices(tools: MutableList<VirtualOfficeResponse.VirtualOffice.Tool>, secondaryColor: String?) {
        activity?.apply {
            val adapter = IntranetAdapter(tools, this@IntranetFragment::onToolClicked, this, secondaryColor)
            toolsList.layoutManager = LinearLayoutManager(this)
            toolsList.adapter = adapter
        }
    }

    private fun onToolClicked(url: String, tool: String, secondaryColor: String?) {
        startActivity(IntranetWebviewActivity.newInstance(requireContext(), url, tool, secondaryColor))
    }
}
