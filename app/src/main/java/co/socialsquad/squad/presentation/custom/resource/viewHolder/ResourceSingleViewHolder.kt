package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_resource_list.view.*

class ResourceSingleViewHolder(itemView: View, var callback: (component: Complement) -> Unit, var share: (component: Complement) -> Unit?) : ViewHolder<ResourceSingleViewHolderModel>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_resource_list

    }

    override fun bind(viewModel: ResourceSingleViewHolderModel) {
        itemView.tvTitle.text = viewModel.complement.resource?.title
        itemView.tvModified.text = viewModel.complement.resource?.description
        itemView.setOnClickListener {
            callback.invoke(viewModel.complement)
        }
        /*itemView.share_icon.setOnClickListener {
            share.invoke(viewModel.complement)
        }*/
        itemView.bottomDivider.visibility = View.GONE
        itemView.ivInformationIcon.visibility = View.VISIBLE
    }

    override fun recycle() {

    }
}