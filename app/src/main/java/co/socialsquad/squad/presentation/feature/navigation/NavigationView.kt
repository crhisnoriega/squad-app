package co.socialsquad.squad.presentation.feature.navigation

import android.view.View
import co.socialsquad.squad.presentation.feature.feedback.FeedbackViewModel

interface NavigationView {
    fun openLiveCreator(pk: Int, rtmpUrl: String, commentQueue: String, startedAt: String?)
    fun openSharedActivity(shareModel: String, sharePK: Int)
    fun showNotificationBadge(primaryColor: String?)
    fun hideNotificationBadge()
    fun setupHub(inactiveImageUrl: String?, activeImageUrl: String?)
    fun socialTabSelected(v: View?)
    fun exploreTabSelected(v: View?)
    fun hubTabSelected(v: View?)
    fun notificationsTabSelected(v: View?)
    fun profileTabSelected(v: View?)
    fun showSystemSelection()
    fun openFeedback(feedbackViewModel: FeedbackViewModel)
    fun startChatService(chatId: String)
}
