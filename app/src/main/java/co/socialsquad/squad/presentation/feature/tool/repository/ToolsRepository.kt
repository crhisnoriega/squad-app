package co.socialsquad.squad.presentation.feature.tool.repository

import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.OpportunityWidgetResponse
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.domain.model.SubmissionCreationResponse
import co.socialsquad.squad.domain.model.Timetable
import kotlinx.coroutines.flow.Flow

interface ToolsRepository {
    fun fetchOpportunitiesSubmission(link: String): Flow<Opportunity<Submission>?>
    fun fetchOpportunitiesTimetable(link: String): Flow<Opportunity<Timetable>?>
    fun fetchDetailsTimetable(submissionId: String): Flow<Timetable?>
    fun createNewOpportunity(objectId: Long, opportunityId: Long): Flow<SubmissionCreationResponse>
    fun getNextWidget(submissionId: Long): Flow<OpportunityWidgetResponse>
}
