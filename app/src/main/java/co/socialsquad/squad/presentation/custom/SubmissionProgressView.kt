package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.StageStatus

private const val DEFAULT_LOADING = true
private const val DEFAULT_TOTAL_STEP = 2
private const val DEFAULT_CURRENT_STEP = 1
private const val DEFAULT_STATUS = 0
private val STAGE_STATUS_LIST =
    listOf(StageStatus.Completed, StageStatus.Blocked, StageStatus.Closed)
private val wrapLayoutParams = LinearLayout.LayoutParams(
    LinearLayout.LayoutParams.WRAP_CONTENT,
    LinearLayout.LayoutParams.WRAP_CONTENT
).apply {
    gravity = Gravity.CENTER
}

class SubmissionProgressView(context: Context, attrs: AttributeSet?) :
    LinearLayout(context, attrs) {

    private var loading: Boolean = DEFAULT_LOADING
    private var stepTotal: Int = DEFAULT_TOTAL_STEP
    private var stepCurrent: Int = DEFAULT_CURRENT_STEP
    private var status: StageStatus = StageStatus.getDefault()

    init {
        setupStyledAttributes(attrs)
        orientation = HORIZONTAL
        gravity = Gravity.CENTER
        bind(status = status, total = stepTotal, current = stepCurrent)
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.SubmissionStatus)) {
            loading =
                getBoolean(R.styleable.SubmissionStatus_loading, DEFAULT_LOADING)
            stepTotal =
                getInt(R.styleable.SubmissionStatus_stepTotal, DEFAULT_TOTAL_STEP)
            stepCurrent =
                getInt(R.styleable.SubmissionStatus_stepCurrent, DEFAULT_CURRENT_STEP)
            status =
                STAGE_STATUS_LIST[getInt(R.styleable.SubmissionStatus_status, DEFAULT_STATUS)]
            recycle()
        }
    }

    fun bind(status: StageStatus, total: Int, current: Int = -1, loading: Boolean = DEFAULT_LOADING) {
        removeAllViews()
        this.loading = loading
        for (i in 1..total) {
            val ivStatus = newImageView()
            ivStatus.setImageResource(
                when {
                    loading -> R.drawable.ic_submission_progress_loading
                    current > i -> R.drawable.ic_submission_progress_active
                    current == i ->
                        when (status) {
                            StageStatus.Completed -> R.drawable.ic_submission_status_completed
                            StageStatus.Blocked -> R.drawable.ic_submission_status_blocked
                            StageStatus.Closed -> R.drawable.ic_submission_status_closed
                        }
                    else -> R.drawable.ic_submission_progress_inactive
                }
            )
            addView(ivStatus)
            if (i in 1 until total) {
                val ivLine = newImageView()
                ivLine.setImageResource(R.drawable.ic_submission_progress_line)
                addView(ivLine)
            }
        }
    }

    private fun newImageView() = ImageView(context).apply {
        id = View.generateViewId()
        layoutParams = wrapLayoutParams
    }
}
