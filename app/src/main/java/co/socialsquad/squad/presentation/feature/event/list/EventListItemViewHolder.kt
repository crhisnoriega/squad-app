package co.socialsquad.squad.presentation.feature.event.list

import android.content.Intent
import android.text.format.DateUtils
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import co.socialsquad.squad.presentation.feature.event.detail.EventDetailActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_event_list_item.view.*

const val EVENT_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_event_list_item
const val EVENT_LIST_ITEM_LOADING = R.layout.view_event_list_item_loading

class EventListItemViewHolder(itemView: View) : ViewHolder<EventViewModel>(itemView) {
    override fun bind(viewModel: EventViewModel) {
        itemView.apply {
            var dateString = ""
            viewModel.startDate?.let {
                dateString += DateUtils.formatDateTime(context, it.time, DateUtils.FORMAT_NO_YEAR)
            }
            viewModel.endDate?.let {
                dateString += " - " + DateUtils.formatDateTime(context, it.time, DateUtils.FORMAT_NO_YEAR)
            }
            tvEventDate.text = dateString
            viewModel.companyColor?.let { tvEventDate.setTextColor(ColorUtils.parse(it)) }

            tvEventName.text = viewModel.title

            tvEventPlace.text = context.getString(R.string.event_list_item_type_location, viewModel.category, viewModel.location)

            Glide.with(context)
                .load(viewModel.cover)
                .roundedCorners(context)
                .crossFade()
                .into(ivEventCover)

            setOnClickListener {
                val intent = Intent(itemView.context, EventDetailActivity::class.java).apply {
                    putExtra(EventDetailActivity.EXTRA_EVENT_VIEW_MODEL, viewModel)
                }
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivEventCover)
    }
}
