package co.socialsquad.squad.presentation.feature.squad.social

import dagger.Binds
import dagger.Module

@Module
abstract class SocialSquadModule {
    @Binds
    abstract fun socialSquadView(socialActivity: SocialSquadActivity): SocialSquadView
}
