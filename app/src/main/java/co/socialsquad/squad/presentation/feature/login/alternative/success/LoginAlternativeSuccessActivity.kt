package co.socialsquad.squad.presentation.feature.login.alternative.success

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.feature.login.alternative.EXTRA_LOGIN_ALTERNATIVE_TYPE
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativePresenter
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailActivity
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import kotlinx.android.synthetic.main.activity_login_reset_success.*
import javax.inject.Inject

class LoginAlternativeSuccessActivity : BaseDaggerActivity() {

    @Inject
    lateinit var analytics: Analytics

    companion object {
        const val EXTRA_EMAIL = "EMAIL"
        const val EXTRA_CREATE_ACCOUNT = "extra_create_account"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFlagLayoutFullscreen()
        setContentView(R.layout.activity_login_reset_success)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        supportActionBar?.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left))

        val type = intent.getSerializableExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE) as? LoginAlternativePresenter.Type

        if (intent.hasExtra(EXTRA_EMAIL)) {
            val email = intent.getStringExtra(EXTRA_EMAIL)
            setupDescription(email, type)
            enterManually.visibility = if (type != LoginAlternativePresenter.Type.RESET_PASSWORD) View.VISIBLE else View.GONE
        }

        if (intent.hasExtra(EXTRA_CREATE_ACCOUNT)) {
            hasAccount.visibility = View.VISIBLE
        }

        openEmailApp.setOnClickListener {
            sendAnalyticsOpenEmailApp(type)
            val intent = Intent(Intent.ACTION_MAIN).apply {
                addCategory(Intent.CATEGORY_APP_EMAIL)
            }
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }

        enterManually.setOnClickListener {
            sendAnalyticsOpenManually(type)
            startActivity(Intent(this, LoginEmailActivity::class.java))
        }

        sendAnalyticsView(type)
    }

    private fun sendAnalyticsOpenEmailApp(type: LoginAlternativePresenter.Type?) {
        when (type) {
            LoginAlternativePresenter.Type.CREATE_ACCOUNT -> {
                analytics.sendEvent(Analytics.SIGNUP_VERIFY_EMAIL_TAP_OPEN_EMAIL)
            }
            LoginAlternativePresenter.Type.MAGIC_LINK -> {
                analytics.sendEvent(Analytics.MAGIC_LINK_VERIFY_EMAIL_TAP_OPEN_EMAIL)
            }
            LoginAlternativePresenter.Type.RESET_PASSWORD -> {
                analytics.sendEvent(Analytics.RESET_PASSWORD_VERIFY_EMAIL_TAP_OPEN_EMAIL)
            }
        }
    }

    private fun sendAnalyticsOpenManually(type: LoginAlternativePresenter.Type?) {
        when (type) {
            LoginAlternativePresenter.Type.CREATE_ACCOUNT -> {
                analytics.sendEvent(Analytics.SIGNUP_VERIFY_EMAIL_TAP_ENTER_MANUALLY)
            }
            LoginAlternativePresenter.Type.MAGIC_LINK -> {
                analytics.sendEvent(Analytics.MAGIC_LINK_VERIFY_EMAIL_TAP_ENTER_MANUALLY)
            }
        }
    }

    private fun sendAnalyticsView(type: LoginAlternativePresenter.Type?) {
        when (type) {
            LoginAlternativePresenter.Type.CREATE_ACCOUNT -> {
                analytics.sendEvent(Analytics.SIGNUP_VERIFY_EMAIL_SCREEN_VIEW)
            }
            LoginAlternativePresenter.Type.MAGIC_LINK -> {
                analytics.sendEvent(Analytics.MAGIC_LINK_VERIFY_EMAIL_SCREEN_VIEW)
            }
            LoginAlternativePresenter.Type.RESET_PASSWORD -> {
                analytics.sendEvent(Analytics.RESET_PASSWORD_VERIFY_EMAIL_SCREEN_VIEW)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupDescription(email: String, type: LoginAlternativePresenter.Type?) {
        val resId = when (type) {
            LoginAlternativePresenter.Type.MAGIC_LINK -> R.string.login_alternative_success_description_link
            LoginAlternativePresenter.Type.CREATE_ACCOUNT -> R.string.login_alternative_success_create_account
            else -> R.string.login_alternative_success_description_reset
        }
        val description = getString(resId, email)
        val spannableString = TextUtils.applyTypeface(this, "fonts/gotham_bold.ttf", description, email)
        tvDescription.text = spannableString
    }
}
