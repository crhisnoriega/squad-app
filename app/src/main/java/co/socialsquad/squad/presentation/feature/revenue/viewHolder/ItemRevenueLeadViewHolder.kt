package co.socialsquad.squad.presentation.feature.revenue.viewHolder

import android.view.View
import co.socialsquad.squad.domain.model.StageStatus
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_revenue_lead_status.view.*

class ItemRevenueLeadViewHolder(
    val itemView: View,
    val callback: (viewModel: ItemRevenueLeadViewModel) -> Unit
) :
    ViewHolder<ItemRevenueLeadViewModel>(itemView) {
    override fun bind(viewModel: ItemRevenueLeadViewModel) {
        itemView.mainLayout.setOnClickListener {
            callback.invoke(viewModel)
        }
        itemView.lastDivider.isVisible = viewModel.isLast.not()

        itemView.progress_1.bind(
            status = StageStatus.Completed,
            total = 4,
            current = 3,
            loading = false
        )
    }

    override fun recycle() {

    }
}