package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.recognition.model.SuccessStoryData

class ItemSuccessHistoryRecognitionViewModel(
       val successStoryData: SuccessStoryData
) : ViewModel {
}