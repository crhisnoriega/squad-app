package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.requirement.QualificationRequirementActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_qualification_requirement_item.view.*
import java.text.NumberFormat
import java.util.Locale

const val QUALIFICATION_REQUIREMENT_VIEW_MODEL_ID = R.layout.view_qualification_requirement_item

class QualificationRequirementViewHolder(itemView: View) : ViewHolder<RequirementViewModel>(itemView) {
    override fun bind(viewModel: RequirementViewModel) {
        Glide.with(itemView.context)
            .load(viewModel.icon)
            .into(itemView.ivRequirement)
        itemView.short_title_lead.text = viewModel.name
        itemView.tvDescription.text = viewModel.description
        val currentValue = NumberFormat.getNumberInstance(Locale.getDefault()).format(viewModel.value).toString()
        val promotionValue = NumberFormat.getNumberInstance(Locale.getDefault()).format(viewModel.targetValue).toString()
        itemView.tvProgress.text = itemView.context.getString(R.string.qualification_requirement_progress, currentValue, promotionValue)
        viewModel.tintColor?.let { itemView.pbProgress.progressTintList = ColorUtils.stateListOf(it) }

        itemView.pbProgress.progress = viewModel.percentage

        itemView.setOnClickListener { openQualificationRequirement(viewModel) }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivRequirement)
        itemView.vlbiDivider.visibility = View.GONE
    }

    private fun openQualificationRequirement(viewModel: RequirementViewModel) {
        val intent = Intent(itemView.context, QualificationRequirementActivity::class.java)
            .putExtra(QualificationRequirementActivity.EXTRA_REQUIREMENT_VIEW_MODEL, viewModel)
        itemView.context.startActivity(intent)
    }
}
