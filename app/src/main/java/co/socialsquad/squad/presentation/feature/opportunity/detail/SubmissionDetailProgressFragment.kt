package co.socialsquad.squad.presentation.feature.opportunity.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.fragment_recommendation_progress.*

class SubmissionDetailProgressFragment : Fragment() {

    private lateinit var submissionStagesAdapter: SubmissionStagesAdapter
    private val submissionDetails: SubmissionDetails by extra(OpportunitySubmissionDetailActivity.EXTRA_SUBMISSION_DETAIL)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_recommendation_progress, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        activity?.apply { submissionStagesAdapter = SubmissionStagesAdapter(this, submissionDetails.stages) }
        rvStages.run {
            layoutManager = LinearLayoutManager(context)
            adapter = submissionStagesAdapter
        }
    }
}
