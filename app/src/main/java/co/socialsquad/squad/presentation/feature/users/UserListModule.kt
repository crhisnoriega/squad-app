package co.socialsquad.squad.presentation.feature.users

import dagger.Binds
import dagger.Module

@Module
abstract class UserListModule {
    @Binds
    abstract fun view(userListActivity: UserListActivity): UserListView
}
