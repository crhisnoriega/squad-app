package co.socialsquad.squad.presentation.feature.social.create.segment

import java.io.Serializable

data class PotentialReachViewModel(
    val circles: Int,
    val totalMember: Int,
    val reachedMembers: Int
) : Serializable
