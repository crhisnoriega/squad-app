package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class MediaItem(
    @SerializedName("id") var id: Int?,
    @SerializedName("url") var url: String?
) : Parcelable {
}