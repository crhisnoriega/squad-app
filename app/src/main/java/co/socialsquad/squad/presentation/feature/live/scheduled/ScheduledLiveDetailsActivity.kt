package co.socialsquad.squad.presentation.feature.live.scheduled

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.format.DateFormat
import android.text.format.DateUtils
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.live.creator.LiveCreatorActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.feature.social.REQUEST_UPDATE_FEED
import co.socialsquad.squad.presentation.feature.social.SocialDeleteDialog
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.comment.COMMENT_VIEW_ID
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewHolder
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialShareViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.bitmapCrossFade
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_scheduled_live_details.*
import kotlinx.android.synthetic.main.view_comment_box.*
import kotlinx.android.synthetic.main.view_social_share.*
import java.util.Date
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ScheduledLiveDetailsActivity : BaseDaggerActivity(), ScheduledLiveDetailsView {

    companion object {
        const val SCHEDULED_LIVE_PK = "QUERY_ITEM_PK"
        const val SCHEDULED_LIVE_VIEW_MODEL = "SCHEDULED_LIVE_VIEW_MODEL"
        const val IS_COMMENTING = "IS_COMMENTING"
    }

    @Inject
    lateinit var scheduledLiveDetailsPresenter: ScheduledLiveDetailsPresenter
    private var socialDeleteDialog: SocialDeleteDialog? = null

    private var commentAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager = LinearLayoutManager(this)
    private var commentCount: Int = 0

    private var live: LiveViewModel? = null
    private var currentUserPk: Int? = null

    private val shareListener = object : ViewHolder.Listener<ViewModel> {
        override fun onClick(viewModel: ViewModel) {
            scheduledLiveDetailsPresenter.onShareClicked()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scheduled_live_details)
        setupOnClickListeners()
        setupCommentList()
        scheduledLiveDetailsPresenter.onCreate(intent)
        llMainLayout.requestFocus()

        etCommentText.hint = getString(R.string.do_a_question)
    }

    private fun setupSendImage(isEnabled: Boolean) {
        val context = ibSendComment.context
        ibSendComment.isEnabled = isEnabled
        if (isEnabled) {
            ibSendComment.setImageDrawable(context.getDrawable(R.drawable.ic_send_active))
        } else {
            ibSendComment.setImageDrawable(context.getDrawable(R.drawable.ic_send_inactive))
        }
    }

    private fun showCommentInput() {
        clCommentInput.visibility = View.VISIBLE
        etCommentText.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etCommentText, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun setupToolbar(titleResId: Int) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(titleResId)
    }

    private fun setupComment(viewModel: LiveViewModel) {
        if (intent.getBooleanExtra(IS_COMMENTING, false)) {
            showCommentInput()
        }
        ibSendComment.isEnabled = false
        ibSendComment.setOnClickListener {
            it.isEnabled = false
            scheduledLiveDetailsPresenter.onCommentClicked(viewModel, etCommentText.text.toString())
        }
        etCommentText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                setupSendImage(!s.isEmpty())
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun setupCommentText(commentCount: Int) {

        when {
            commentCount > 1 -> {
                tvCommentCount.visibility = View.VISIBLE
                tvCommentCount.text = getString(R.string.social_question_count_multiple, commentCount)
            }
            commentCount == 1 -> {
                tvCommentCount.visibility = View.VISIBLE
                tvCommentCount.text = getString(R.string.social_question_count_one, commentCount)
            }
            else -> tvCommentCount.visibility = View.GONE
        }
    }

    private val onDeleteCommentListener = object : ViewHolder.Listener<CommentViewModel> {
        override fun onClick(viewModel: CommentViewModel) {
            commentCount--
            setupCommentText(commentCount)
            scheduledLiveDetailsPresenter.onDeleteCommentClicked(viewModel)
        }
    }

    private fun setupCommentList() {
        commentAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is CommentViewModel -> COMMENT_VIEW_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                COMMENT_VIEW_ID -> CommentViewHolder(view, onDeleteCommentListener)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvComments?.apply {
            adapter = commentAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(0)
            addItemDecoration(ListDividerItemDecoration(context))
            setHasFixedSize(false)
            isNestedScrollingEnabled = false
        }

        svScheduledLive.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { nsv, _, scrollY, _, _ ->
                if (scrollY == (nsv.getChildAt(0).measuredHeight - nsv.measuredHeight)) {
                    scheduledLiveDetailsPresenter.onScrolledBeyondVisibleThreshold()
                }
            }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (live?.canDelete == true) {
            menuInflater.inflate(R.menu.menu_scheduled_live_details, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.scheduled_live_details_mi_delete -> {
            socialDeleteDialog = SocialDeleteDialog(
                this,
                object : SocialDeleteDialog.Listener {
                    override fun onNoClicked() {
                        socialDeleteDialog?.dismiss()
                    }

                    override fun onYesClicked() {
                        scheduledLiveDetailsPresenter.onDeleteClicked()
                    }
                }
            )
            if (!this.isFinishing) socialDeleteDialog?.show()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        scheduledLiveDetailsPresenter.onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun showInvalidPk() {
        ViewUtils.showDialog(this, getString(R.string.live_message_unavailable), DialogInterface.OnDismissListener { finish() })
    }

    override fun showData(live: LiveViewModel, currentUserAvatar: String?, color: String?, currentUserPk: Int) {
        this.live = live
        this.currentUserPk = currentUserPk

        color?.let { tvScheduledType.setTextColor(ColorUtils.stateListOf(it)) }

        tvScheduledType.text = live.category
        tvScheduledCategory.text = live.title

        if (live.content.isNotBlank()) {
            var isExpanded = false
            val maxLines = tvScheduledDescription.maxLines

            tvScheduledDescription.visibility = View.VISIBLE
            tvScheduledDescription.text = live.content

            var listener: ViewTreeObserver.OnPreDrawListener? = null
            listener = ViewTreeObserver.OnPreDrawListener {
                bToggleDescription.viewTreeObserver.removeOnPreDrawListener(listener)
                if (tvScheduledDescription.layout.lineCount > maxLines) {
                    bToggleDescription.visibility = View.VISIBLE
                    bToggleDescription.setOnClickListener {
                        if (isExpanded) {
                            tvScheduledDescription.maxLines = maxLines
                            bToggleDescription.text = getString(R.string.scheduled_live_details_description_more)
                        } else {
                            tvScheduledDescription.maxLines = Int.MAX_VALUE
                            bToggleDescription.text = getString(R.string.scheduled_live_details_description_less)
                        }
                        isExpanded = !isExpanded
                    }
                }
                true
            }
            bToggleDescription.viewTreeObserver.addOnPreDrawListener(listener)
        }

        val createdByText = getString(
            R.string.scheduled_live_detail_created_by,
            live.creatorFullName
        )
        val spannable = SpannableString(createdByText)
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.orange)),
            createdByText.length - live.creatorFullName.length - 1,
            createdByText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tvScheduledCreatedBy.setText(spannable, TextView.BufferType.SPANNABLE)
        tvScheduledCreatedBy.setOnClickListener {
            val intent = Intent(this, UserActivity::class.java).apply {
                putExtra(USER_PK_EXTRA, live.creatorPK)
            }
            startActivity(intent)
        }

        live.scheduledStartDate?.let {
            val day = DateUtils.formatDateTime(this, it.time, DateUtils.FORMAT_NO_YEAR)
            val hour = DateFormat.getTimeFormat(this).format(it)
            tvDate.text = getString(R.string.scheduled_live_detail_date, day, hour)
        }

        tvAudience.text = getString(AudienceViewModel.getStringResId(live.audienceType))
        ivAudience.setImageResource(AudienceViewModel.getDrawableResId(live.audienceType))

        Glide.with(ivImage)
            .asBitmap()
            .load(live.featuredImageURL)
            .roundedCorners(this, radius = 2)
            .bitmapCrossFade()
            .into(ivImage)

        currentUserAvatar?.let {
            Glide.with(this)
                .load(it)
                .circleCrop()
                .crossFade()
                .into(ivCommenterAvatar)
        }

        live.scheduledFeaturedVideo?.let {
            if (live.scheduledFeaturedVideoWidth != null && live.scheduledFeaturedVideoHeight != null &&
                live.scheduledFeaturedVideoWidth > 0 && live.scheduledFeaturedVideoHeight > 0
            ) { // Verify if the video is processed
                setDimensionRatio(ivThumbnail, live.scheduledFeaturedVideoWidth, live.scheduledFeaturedVideoHeight)
                clVideoContainer.visibility = View.VISIBLE
                clVideoContainer.setOnClickListener { _ ->
                    val intent = Intent(this@ScheduledLiveDetailsActivity, VideoActivity::class.java)
                    intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, it)
                    startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
                }
                Glide.with(this)
                    .load(live.scheduledFeaturedThumbnail)
                    .crossFade()
                    .into(ivThumbnail)
                llFeatureContainer.visibility = View.GONE
            }
        }

        btConfirmAttendance.isEnabled = true
        llAttendContainer.isEnabled = true

        when {
            live.creatorPK == currentUserPk -> {
                setupStartLiveButton(live)
            }
            live.isAttendingLive -> {
                setupCancelAttendanceButton(live)
            }
            else -> {
                setupAttendButton(live)
            }
        }

        urslAvatars.setUserImages(live.attendeesPictures)
        urslAvatars.setOnClickListener {
            if (live.numberOfAttendants > 0) {
                val intent = Intent(this, UserListActivity::class.java).apply {
                    putExtra(UserListActivity.QUERY_ITEM_PK, live.pk)
                    putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.CONFIRMED)
                }
                startActivity(intent)
            }
        }

        llAttendContainer.setOnClickListener {
            it.isEnabled = false
            scheduledLiveDetailsPresenter.onConfirmAttendanceClicked()
        }

        llCommentContainer.setOnClickListener {
            showCommentInput()
        }

        SocialShareViewHolder(llShareContainer, live, shareListener).bind(
            ShareViewModel(live.pk, live.title, live.content, live.featuredImageURL, Share.ScheduledLive, live.canShare, live.subdomain)
        )
        updateCommentCount(live.commentsCount)
        setupComment(live)
    }

    private fun setupAttendButton(live: LiveViewModel) {
        llAttendContainer.visibility = View.VISIBLE
        tvAttend.setTextColor(ContextCompat.getColor(this, R.color.oldlavender))
        tvAttend.text = getString(R.string.scheduled_live_details_attend)
        ivAttend.setColorFilter(ContextCompat.getColor(this, R.color.oldlavender), PorterDuff.Mode.SRC_ATOP)
        urslAvatars.setText(getAttendingText(live.numberOfAttendants))
        clBottomAction.visibility = View.GONE
    }

    private fun setupCancelAttendanceButton(live: LiveViewModel) {
        llAttendContainer.visibility = View.VISIBLE
        tvAttend.setTextColor(ContextCompat.getColor(this, R.color.green))
        tvAttend.text = getString(R.string.scheduled_live_details_cancel_attendance)
        ivAttend.setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP)
        urslAvatars.setText(getAttendingText(live.numberOfAttendants))
        clBottomAction.visibility = View.GONE
    }

    private fun setupStartLiveButton(live: LiveViewModel) {
        clBottomAction.visibility = View.VISIBLE
        llAttendContainer.visibility = View.GONE
        clCommentInput.visibility = View.GONE
        llCommentContainer.visibility = View.GONE
        btConfirmAttendance.text = getString(R.string.scheduled_live_detail_start_live)
        val density = resources.displayMetrics.density
        val diffInMillies = live.scheduledStartDate!!.time - Date().time
        val diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS)
        if (diff > 10) {
            btConfirmAttendance.isEnabled = false
            tv10MinutesBefore.visibility = View.VISIBLE
            svScheduledLive.setPadding(0, 0, 0, (112 * density).toInt())
        } else {
            btConfirmAttendance.background.setColorFilter(ContextCompat.getColor(this, R.color.green), PorterDuff.Mode.SRC_ATOP)
            svScheduledLive.setPadding(0, 0, 0, (70 * density).toInt())
        }
        if (live.numberOfAttendants < 1) {
            urslAvatars.setText(getString(R.string.scheduled_live_detail_attenders_none_self))
        } else {
            urslAvatars.setText(getAttendingText(live.numberOfAttendants))
        }
    }

    private fun getAttendingText(numberOfAttendants: Int): String {
        return when {
            numberOfAttendants <= 0 -> getString(R.string.scheduled_live_detail_attenders_none)
            numberOfAttendants == 1 -> getString(R.string.scheduled_live_detail_attenders_single, numberOfAttendants)
            else -> getString(R.string.scheduled_live_detail_attenders_multiple, numberOfAttendants)
        }
    }

    private fun setupOnClickListeners() {
        btConfirmAttendance.setOnClickListener { view ->
            if (live?.creatorPK == currentUserPk) {
                scheduledLiveDetailsPresenter.onStartClicked()
            } else {
                view.isEnabled = false
                scheduledLiveDetailsPresenter.onConfirmAttendanceClicked()
            }
        }
    }

    override fun requestPermissions(requestCode: Int, permissions: Array<String>) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }

    override fun openLiveCreator() {
        live?.let {
            val intent = Intent(this, LiveCreatorActivity::class.java)
            intent.putExtra(LiveCreatorActivity.EXTRA_PK, it.pk)
            intent.putExtra(LiveCreatorActivity.EXTRA_RTMP_URL, it.url)
            intent.putExtra(LiveCreatorActivity.EXTRA_COMMENT_QUEUE, it.rtmpUrl)
            intent.putExtra(LiveCreatorActivity.EXTRA_LIVE_START_SCHEDULED, true)
            startActivity(intent)
        }
    }

    override fun finishAfterDelete() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(this, getString(resId), null)
    }

    override fun showLoading() {
        llLoadingView.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        llLoadingView.visibility = View.GONE
    }

    override fun openLiveViewer(live: LiveViewModel) {
        val intent = Intent(this, LiveViewerActivity::class.java)
        intent.putExtra(LiveViewerActivity.LIVE_VIEW_MODEL, live)
        startActivityForResult(intent, REQUEST_UPDATE_FEED)
        finish()
    }

    private fun hideComment() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etCommentText.windowToken, 0)
        etCommentText.text.clear()
        etCommentText.clearFocus()
        svScheduledLive.requestFocus()
    }

    // interface implementation for comment list view
    override fun insertNewComment(commentViewModel: CommentViewModel) {
        ibSendComment.isEnabled = true
        hideComment()
        svScheduledLive.smoothScrollTo(0, rvComments.top)
        commentCount++
        setupCommentText(commentCount)
        commentAdapter?.addItem(commentViewModel, 0, true)
    }

    override fun removeComment(viewModel: CommentViewModel) {
        ibSendComment.isEnabled = true
        hideComment()
        commentAdapter?.remove(viewModel, true)
    }

    override fun setComments(comments: List<CommentViewModel>) {
        commentAdapter?.setItems(comments)
        commentAdapter?.addItems(listOf(DividerViewModel))
    }

    override fun addComments(comments: List<CommentViewModel>) {
        commentAdapter?.remove(DividerViewModel)
        commentAdapter?.addItems(comments)
        commentAdapter?.addItems(listOf(DividerViewModel))
    }

    override fun showLoadingComments() {
        commentAdapter?.startLoading()
    }

    override fun hideLoadingComments() {
        commentAdapter?.stopLoading()
    }

    override fun updateComment(commentViewModel: CommentViewModel) {
        commentAdapter?.update(commentViewModel)
    }

    override fun updateCommentCount(count: Int) {
        commentCount = count
        runOnUiThread {
            setupCommentText(commentCount)
        }
    }

    private fun setDimensionRatio(view: View, width: Int, height: Int) {
        val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.dimensionRatio = if (width >= height) "w,$height:$width" else "w,1:1"
        view.layoutParams = layoutParams
    }
}
