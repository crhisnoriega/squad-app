package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_social_live.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class LiveViewHolder(
    itemView: View,
    private val listener: Listener<LiveViewModel>
) : ViewHolder<LiveViewModel>(itemView) {
    override fun bind(viewModel: LiveViewModel) {
        with(viewModel) {
            itemView.short_title_lead.text = creatorFullName

            Glide.with(itemView)
                .load(featuredImageURL)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.ivImage)

            if (isLive) {
                itemView.tvDuration.visibility = View.GONE
                itemView.tvBadge.visibility = View.VISIBLE
                itemView.tvCategoryOrTimestamp.text = category
            } else {
                itemView.tvBadge.visibility = View.GONE
                if (isScheduledLive) {
                    itemView.tvDuration.visibility = View.GONE
                    itemView.tvCategoryOrTimestamp.text = createTimestamp(scheduledStartDate!!)
                } else if (isVideo) {
                    itemView.tvDuration.visibility = View.VISIBLE
                    itemView.tvDuration.text = duration.toDurationString()
                    itemView.tvCategoryOrTimestamp.text = startDate!!.elapsedTime(itemView.context)
                }
            }

            itemView.setOnClickListener { listener.onClick(this) }
        }
    }

    override fun recycle() {
        Glide.with(itemView).clear(itemView.ivImage)
    }

    private fun createTimestamp(date: Date): String {
        val sdf = DateFormat.getDateInstance(DateFormat.SHORT) as SimpleDateFormat
        sdf.applyPattern(sdf.toPattern().replace("/y", "").replace("y", ""))
        val d = sdf.format(date)
        val t = DateFormat.getTimeInstance(DateFormat.SHORT).format(date)
        return itemView.context.getString(R.string.social_item_live_list_timestamp, d, t)
    }
}
