package co.socialsquad.squad.presentation.custom.multimedia

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.data.entity.Media

abstract class MediaViewHolder(val view: View, val lifecycleOwner: LifecycleOwner?) : RecyclerView.ViewHolder(view) {

    init {
        lifecycleOwner?.let {
            val observer = MultiMediaLifeCycleObserver(this)
            lifecycleOwner.lifecycle.addObserver(observer)
        }
    }

    abstract fun bind(media: Media, label: String? = "", isFirst: Boolean, showLabel: Boolean = true)
    abstract fun onResume()
    abstract fun onPause()
    abstract fun onStop()
    abstract fun onDestroy()
}
