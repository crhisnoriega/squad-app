package co.socialsquad.squad.presentation.feature.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.local.onboarding.onboardingInfo
import co.socialsquad.squad.presentation.custom.PageIndicator
import co.socialsquad.squad.presentation.feature.login.options.LoginOptionsActivity
import co.socialsquad.squad.presentation.feature.login.subdomain.LoginSubdomainActivity
import co.socialsquad.squad.presentation.feature.signup.credentials.SignupCredentialsActivity
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : BaseDaggerActivity() {

    companion object {
        val EXTRA_OPEN_ENTER_MANUALLY = "extra_open_enter_manually"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        val sectionsPagerAdapter = SectionsPagerAdapter(baseContext, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        setupIndicators()

        intent?.extras?.getString(EXTRA_OPEN_ENTER_MANUALLY)?.apply {
            if (this.isNotEmpty()) {
                startActivity(Intent(baseContext, LoginSubdomainActivity::class.java))
            }
        }

        first_steps.setOnClickListener { startActivity(Intent(this, SignupCredentialsActivity::class.java)) }
        login.setOnClickListener { startActivity(Intent(this, LoginOptionsActivity::class.java)) }
    }

    private fun setupIndicators() {
        with(indicators) {
            viewPager = view_pager
            types = (onboardingInfo(baseContext).indices).map { PageIndicator.Type.Image() }
            init()
        }
    }

    class SectionsPagerAdapter(val context: Context, fm: FragmentManager) :
        FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return OnboardingFragment.newInstance(onboardingInfo(context)[position])
        }

        override fun getCount(): Int {
            return onboardingInfo(context).size
        }
    }
}
