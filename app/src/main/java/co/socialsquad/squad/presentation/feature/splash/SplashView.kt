package co.socialsquad.squad.presentation.feature.splash

import co.socialsquad.squad.data.entity.Company

interface SplashView {
    fun setNextAsNavigation()
    fun setNextAsWalkthrough()
    fun setNextAsSignupHinodeConfirmation()
    fun setNextAsLiveViewer(pk: Int)
    fun showNextActivity()
    fun showErrorMessageFailedToConnect()
    fun showUpdateDialog()
    fun preloadImages(url: String?)
    fun setNextAsPostViewer(pk: Int)
    fun setNextAsLiveDetail(pk: Int)
    fun setNextAsAudioDetail(pk: Int)
    fun setNextAsProduct(pk: Int)
    fun setNextAsEvent(pk: Int)
    fun startCallService()
    fun openNavigation()
    fun openCompleteRegistration()
    fun moveToPassword(register: String, subdomain: String)
    fun moveToAccessSquad(token: String, email: String)
    fun moveToResetPassword(token: String, subdomain: String, email: String)
    fun openSubdomainsScreen(companies: List<Company>, email: String)
}
