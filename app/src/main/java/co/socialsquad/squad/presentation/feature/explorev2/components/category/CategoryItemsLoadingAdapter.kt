package co.socialsquad.squad.presentation.feature.explorev2.components.category

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.BaseItemsLoadingAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate

class CategoryItemsLoadingAdapter(template: SectionTemplate) : BaseItemsLoadingAdapter(template) {

    override fun getLayoutRes(template: SectionTemplate) = when (template) {
        SectionTemplate.Square -> R.layout.view_session_category_square_item_loading_state
        SectionTemplate.Circle -> R.layout.view_session_category_circle_item_loading_state
        SectionTemplate.RectangleVertical -> R.layout.view_session_category_rectangular_vertical_item_loading_state
        SectionTemplate.RectangleHorizontal -> R.layout.view_session_category_rectangular_horizontal_item_loading_state
        else -> R.layout.view_session_category_rectangular_horizontal_item_loading_state
    }
}
