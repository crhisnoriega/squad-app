package co.socialsquad.squad.presentation.feature.immobiles

import co.socialsquad.squad.data.entity.Immobile

interface ImmobilesView {
    fun showLoading()
    fun hideLoading()
    fun setupImmobiles(immobiles: List<Immobile>, secondaryColor: String?, squadColor: String?)
}
