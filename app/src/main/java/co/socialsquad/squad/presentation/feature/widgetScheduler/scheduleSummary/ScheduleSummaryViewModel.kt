package co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary

import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.PositionItem
import co.socialsquad.squad.domain.model.ScaleShift
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleDate
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.ScheduleSummaryRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import java.util.Date

class ScheduleSummaryViewModel(
    private val repository: ScheduleSummaryRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseViewModel() {

    @ExperimentalCoroutinesApi
    val summary: StateFlow<Resource<ScheduleSummary>>
        get() = _summary

    @ExperimentalCoroutinesApi
    private val _summary = MutableStateFlow<Resource<ScheduleSummary>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val scaleDatePositions: StateFlow<Resource<ScaleDate>>
        get() = _scaleDatePositions

    @ExperimentalCoroutinesApi
    private val _scaleDatePositions = MutableStateFlow<Resource<ScaleDate>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val scaleDate: StateFlow<Resource<ScaleDate>>
        get() = _scaleDate

    @ExperimentalCoroutinesApi
    private val _scaleDate = MutableStateFlow<Resource<ScaleDate>>(Resource.loading(null))

    var formattedDate: Date? = null
    var shift: ScaleShift? = null
    var notes = ""
    var positionItem: PositionItem? = null

    @ExperimentalCoroutinesApi
    fun fetchScheduleSummary(timetableId: Long, objectId: Long) {
        viewModelScope.launch {
            _summary.value = Resource.loading(null)
            repository.fetchScheduleSummary(timetableId = timetableId, objectId = objectId)
                .flowOn(dispatcher)
                .catch { e ->
                    _summary.value = Resource.error(e.toString(), null)
                }
                .collect {
                    if (it?.timetable == null) {
                        _summary.value = Resource.empty()
                    } else {
                        _summary.value = Resource.success(it)
                    }
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchScaleDate(
        timetableId: Long,
        objectId: Long,
        position: Long? = null,
        date: String? = null
    ) {
        viewModelScope.launch {
            _scaleDate.value = Resource.loading(null)
            repository.fetchScaleDate(
                timetableId = timetableId,
                objectId = objectId,
                position = position,
                date = date
            )
                .flowOn(dispatcher)
                .catch { e ->
                    _scaleDate.value = Resource.error(e.toString(), null)
                }
                .collect {
                    if (it == null) {
                        _scaleDate.value = Resource.empty()
                    } else {
                        _scaleDate.value = Resource.success(it)
                    }
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchScaleDatePositions(
        timetableId: Long,
        objectId: Long,
    ) {
        viewModelScope.launch {
            _scaleDatePositions.value = Resource.loading(null)
            repository.fetchScaleDate(
                timetableId = timetableId,
                objectId = objectId,
                position = null,
                date = null,
            )
                .flowOn(dispatcher)
                .catch { e ->
                    _scaleDatePositions.value = Resource.error(e.toString(), null)
                }
                .collect {
                    if (it == null) {
                        _scaleDatePositions.value = Resource.empty()
                    } else {
                        _scaleDatePositions.value = Resource.success(it)
                    }
                }
        }
    }
}
