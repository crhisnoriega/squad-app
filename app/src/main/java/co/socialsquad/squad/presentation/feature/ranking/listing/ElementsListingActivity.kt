package co.socialsquad.squad.presentation.feature.ranking.listing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.RankingListViewModel
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.ranking.RankingActivity
import co.socialsquad.squad.presentation.feature.ranking.details.RankingBenefitDetailsActivity
import co.socialsquad.squad.presentation.feature.ranking.details.RankingCriteriaDetailsActivity
import co.socialsquad.squad.presentation.feature.ranking.viewholder.*
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_elements_listing.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module


enum class RANKING_LIST_TYPE(val title: String) {
    POSITION("Posições"), CRITERIA("Critérios"), BENEFIT("Benefícios"), RANKINK_INFO("Rankings"), PRIZES(
        "Prêmios"
    )
}

class ElementsListingActivity : BaseActivity() {

    companion object {

        var doRefresh: Boolean = false

        const val ELEMENT_TYPE = "element_type"
        const val ELEMENT_NAME = "element_name"
        const val ELEMENT_LIST_JSON = "element_list"
        const val ELEMENT_TITLE = "element_title"
        const val RANKING_ID = "ranking_id"
        const val IS_IN_RANK = "is_in_rank"
        const val ITENS_SHIMMER = "ITEMS_SHIMMER"

        fun start(
            context: Context,
            elementType: co.socialsquad.squad.presentation.feature.ranking.listing.RANKING_LIST_TYPE,
            elements: List<*>,
            rankingId: String,
            isInRank: Boolean,
            itemsShimmer: Int = 0,
            titleAPI: String
        ) {
            context.startActivity(Intent(context, ElementsListingActivity::class.java).apply {
                putExtra(ELEMENT_TYPE, elementType)
                putExtra(ELEMENT_LIST_JSON, Gson().toJson(elements))
                putExtra(ELEMENT_TITLE, titleAPI)
                putExtra(RANKING_ID, rankingId)
                putExtra(IS_IN_RANK, isInRank)
                putExtra(ITENS_SHIMMER, itemsShimmer)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_elements_listing

    private val elementType: co.socialsquad.squad.presentation.feature.ranking.listing.RANKING_LIST_TYPE by extra(
        ELEMENT_TYPE
    )
    private val elementsList: String by extra(ELEMENT_LIST_JSON)
    private val title: String by extra(ELEMENT_TITLE)
    private val rankingId: String by extra(RANKING_ID)
    private val isInRank: Boolean by extra(IS_IN_RANK)
    private val itemsShimmer: Int by extra(ITENS_SHIMMER)

    private val viewModel by viewModel<RankingListViewModel>()

    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

        itemsShimmer?.let {
            if (it > 0) configureAsLoading(it)
        }

        when (elementType) {
            RANKING_LIST_TYPE.POSITION -> viewModel.fetchPositions(rankingId, "")
            RANKING_LIST_TYPE.CRITERIA -> viewModel.fetchCriterias(rankingId, "")
            RANKING_LIST_TYPE.BENEFIT -> viewModel.fetchBenefits()
            RANKING_LIST_TYPE.RANKINK_INFO -> viewModel.fetchRankingList()
        }
    }


    override fun onResume() {
        Log.i("list", "onResume: $doRefresh")
        super.onResume()
        if (doRefresh) {
            doRefresh = false

            factoryAdapter.items.clear()

            ProfileFragment.doRefresh = true
            when (elementType) {
                RANKING_LIST_TYPE.RANKINK_INFO -> viewModel.fetchRankingList()
            }
        }
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = title
    }

    private val handler = Handler()
    private var endPage = false

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.positionList
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        configureElementList()
                    }
                    Status.ERROR -> {
                        // backend send 400 when no has more pages
                        endPage = true

                        handler.removeCallbacksAndMessages(null)
                        handler.postDelayed({
                            factoryAdapter.addItems(
                                listOf(
                                    ItemShowAllRankingTopBottomViewModel(
                                        "",
                                        ""
                                    )
                                )
                            )
                        }, 500)


                    }
                }

            }
        }

        lifecycleScope.launch {
            val value = viewModel.criteriaList
            value.collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        configureElementList()
                    }
                }

            }
        }

        lifecycleScope.launch {
            val value = viewModel.benefitList
            value.collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        configureElementList()
                    }
                }

            }
        }

        viewModel.newRankingList.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    configureElementList()
                }
            }
        })
    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                Shimmer.AlphaHighlightBuilder()
                    .setBaseAlpha(1f)
                    .setIntensity(0f)
                    .build()
            )
            it.stopShimmer()
            it.clearAnimation()
        }
    }

    private fun configureAsLoading(itens: Int) {

        rvElements.apply {
            layoutManager = LinearLayoutManager(
                this@ElementsListingActivity,
                LinearLayoutManager.VERTICAL,
                false
            )

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                        is ItemShowAllRankingTopBottomViewModel -> R.layout.item_ranking_info_shimmer
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
                    }
                    R.layout.item_ranking_info_shimmer -> ItemSeparatorRankingTopBottomViewHolder(
                        view
                    ) {
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            factoryAdapter.setItems((0 until itens).map {
                ItemShowAllRankingTopBottomViewModel("", "") as ViewModel
            }.toMutableList().apply {
                add(0, ItemShowAllRankingViewModel("", ""))
            })
        }
    }


    private fun addToList() {

    }

    val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel): Int {
            return when (viewModel) {
                is ItemPositionRankingViewModel -> R.layout.item_ranking_position
                is ItemCriteriaRankingViewModel -> R.layout.item_ranking_criteria
                is ItemBenefitRankingViewModel -> R.layout.item_ranking_benefit
                is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                is ItemShowAllRankingTopBottomViewModel -> R.layout.ranking_separator_top_bottom
                is ItemRankingInfoViewModel -> R.layout.item_ranking_info
                else -> throw IllegalArgumentException()
            }
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            R.layout.item_ranking_position -> ItemPositionRankingViewHolder(view) {
            }
            R.layout.item_ranking_criteria -> ItemCriteriaRankingViewHolder(view) {
                RankingCriteriaDetailsActivity.start(this@ElementsListingActivity, it.id, isInRank)
            }
            R.layout.item_ranking_benefit -> ItemBenefitRankingViewHolder(view) {
                RankingBenefitDetailsActivity.start(this@ElementsListingActivity, it.id, rankingId)
            }
            R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
            }
            R.layout.ranking_separator_top_bottom -> ItemSeparatorRankingTopBottomViewHolder(view) {
            }
            R.layout.item_ranking_info -> ItemRankingInfoViewHolder(view) {
                RankingActivity.start(this@ElementsListingActivity, it.rankingItem.id.toString()!!)
            }
            else -> throw IllegalArgumentException()
        }
    })

    private val _layoutManager =
        LinearLayoutManager(this@ElementsListingActivity, LinearLayoutManager.VERTICAL, false)

    private fun configureElementList() {
        stopShimmer(listShimmer)

        rvElements.apply {
            layoutManager = _layoutManager

            if (page == 1) {
                adapter = factoryAdapter
            }

            var elements = mutableListOf<ViewModel>()

            if (page == 1) {
                elements.add(ItemShowAllRankingViewModel("", ""))
            }

            when (elementType) {

                RANKING_LIST_TYPE.POSITION -> {
                    var isFirst = true
                    viewModel.positionList.value?.data?.let {
                        it.results?.let { list ->
                            list.forEach {
                                elements.add(
                                    ItemPositionRankingViewModel(
                                        it.user?.full_name!!,
                                        it.position!!,
                                        it.score!!,
                                        it.user?.avatar,
                                        isFirst
                                    ) as ViewModel
                                )
                                isFirst = false
                            }
                        }
                    }
                }

                RANKING_LIST_TYPE.CRITERIA -> {
                    viewModel.criteriaList.value?.data?.let {

                        it.results?.let { list ->
                            list.forEachIndexed { index, it ->
                                elements.add(
                                    ItemCriteriaRankingViewModel(
                                        it.id!!,
                                        it.title!!,
                                        it.description!!,
                                        it.icon!!,
                                        it.points ?: "",
                                        isInRank = isInRank,
                                        isFirst = index == 0,
                                        showDetails = it.show_details
                                    )
                                )
                            }
                        }
                    }
                }

                RANKING_LIST_TYPE.BENEFIT -> {
                    viewModel.benefitList.value?.data?.let {
                        it.data?.list?.let { list ->
                            list.forEachIndexed { index, it ->
                                elements.add(
                                    ItemBenefitRankingViewModel(
                                        it.id!!,
                                        it.title!!,
                                        it.positions!!,
                                        it.description!!,
                                        it.mediaCover!!,
                                        index == 0
                                    )
                                )
                            }
                        }
                    }
                    elements.add(ItemShowAllRankingTopBottomViewModel("", ""))
                }

                RANKING_LIST_TYPE.RANKINK_INFO -> {
                    viewModel.newRankingList.value?.data?.let {
                        it.results?.forEachIndexed { position, rank ->
                            rank?.let { r ->
                                r.first = position == 0
                                elements.add(ItemRankingInfoViewModel(r))
                            }
                        }
                    }
                }

                else -> mutableListOf<ViewModel>()
            }

            factoryAdapter.addItems(elements)


            addOnScrollListener(EndlessScrollListener(20, _layoutManager, {
                when (elementType) {

                    RANKING_LIST_TYPE.POSITION -> {
                        var lastReponse = viewModel.positionList.value?.data

                        if (lastReponse != null && lastReponse.next.isNullOrBlank()
                                .not() && viewModel.isLoadding.not()
                        ) {
                            page++
                            viewModel.fetchPositions(rankingId, lastReponse.next!!)
                        }
                    }

                    RANKING_LIST_TYPE.CRITERIA -> {
                        var lastReponse = viewModel.criteriaList.value?.data

                        if (lastReponse != null && lastReponse.next.isNullOrBlank().not()) {
                            page++
                            viewModel.fetchCriterias(rankingId, lastReponse.next!!)
                        }
                    }
                }
            }))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}