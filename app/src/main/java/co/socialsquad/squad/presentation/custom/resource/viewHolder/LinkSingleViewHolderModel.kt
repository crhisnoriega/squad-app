package co.socialsquad.squad.presentation.custom.resource.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkV2

data class LinkSingleViewHolderModel(val link: LinkV2, val isLast: Boolean = false) : ViewModel