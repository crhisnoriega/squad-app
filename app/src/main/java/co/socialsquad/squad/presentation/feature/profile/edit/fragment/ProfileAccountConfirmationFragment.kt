package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.fragment_edit_profile_account_confirmation.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ProfileAccountConfirmationFragment(
    var isInCompleteProfile: Boolean? = false,
    var isSummary: Boolean? = false
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupSkipButton()
        setupBack()
        setupHeader()

        viewModel.saveStatus.observe(requireActivity(), Observer {
            when (it) {
                "ted" -> {
                    viewModel.state.value = StateNav("next", true)
                    (activity as? CompleteProfileActivity)?.next()
                }
            }
        })



        if (isInCompleteProfile!!) {
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_teddoc))
        }

        if (isSummary!!) {
            btnConfirmar.text = "Mudar Conta"
            txtHelpText.text = "Confirme que os dados estão corretos:"
            txtSubtitle.isVisible = false
            setupAsNextButton()
            populateContent()
        } else {
            setupAsSaveButton()
            setupContentFromModel()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isSummary?.not()!!)
            setupContentFromModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_account_confirmation, container, false)

    private fun setupHeader() {
        // txtTitle.text = title
        // txtSubtitle.text = subtitle

    }

    private fun setupBack() {
        ibBack.setOnClickListener {
            viewModel.state.postValue(StateNav("previous", true))
            (activity as? CompleteProfileActivity)?.previous()
        }
    }

    private fun populateContent() {

        bankName.text = viewModel.userCompany?.user?.bank_account_name
        accountType.text =
            if (viewModel.userCompany?.user?.bank_account_savings!!) "Conta Poupança" else "Conta Corrente"
        agencyAccount.text = viewModel.userCompany?.user?.bank_account_branch
        accountNumber.text = viewModel.userCompany?.user?.bank_account_number

        bankName.doClickable { }
        accountType.doClickable { }
        agencyAccount.doClickable { }
        accountNumber.doClickable { }

        hideKeyboard(requireActivity())

        btnConfirmar.isEnabled = true
    }

    private fun setupContentFromModel() {

        bankName.text = viewModel.selectBankInfo?.short_name
        accountType.text = if (viewModel.isSaving!!) "Conta Poupança" else "Conta Corrente"
        agencyAccount.text = viewModel.agency
        accountNumber.text = viewModel.accountNumber

        bankName.doClickable { }
        accountType.doClickable { }
        agencyAccount.doClickable { }
        accountNumber.doClickable { }

        hideKeyboard(requireActivity())

        btnConfirmar.isEnabled = true
    }


    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    private fun setupAsSaveButton() {
        btnConfirmar.setOnClickListener {
            btnConfirmar.isEnabled = false
            hideKeyboard(requireActivity())
            showLoading()
            viewModel.saveAccountInfo()
        }

    }

    private fun setupAsNextButton() {
        btnConfirmar.setOnClickListener {
            viewModel.state.value = StateNav("next", true)
            (activity as? CompleteProfileActivity)?.next()
        }
    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
