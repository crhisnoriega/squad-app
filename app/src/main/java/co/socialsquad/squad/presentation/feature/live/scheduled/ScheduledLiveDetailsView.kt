package co.socialsquad.squad.presentation.feature.live.scheduled

import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.feature.social.comment.CommentListView

interface ScheduledLiveDetailsView : CommentListView {
    fun showInvalidPk()
    fun showData(live: LiveViewModel, currentUserAvatar: String?, color: String?, currentUserPk: Int)
    fun setupToolbar(titleResId: Int)
    fun finishAfterDelete()
    fun showMessage(resId: Int)
    fun showLoading()
    fun hideLoading()
    fun openLiveViewer(live: LiveViewModel)
    fun openLiveCreator()
    fun requestPermissions(requestCode: Int, permissions: Array<String>)
}
