package co.socialsquad.squad.presentation.feature.notification.viewHolder

import android.annotation.SuppressLint
import android.content.Intent
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.View
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_notification_item_view.view.*

const val NOTIFICATION_ITEM_VIEW_ID = R.layout.view_notification_item_view

class NotificationViewHolder(itemView: View, private var onClickListener: ViewHolder.Listener<NotificationViewModel>) : ViewHolder<NotificationViewModel>(itemView) {

    private val glide = Glide.with(itemView)

    override fun bind(viewModel: NotificationViewModel) {
        itemView.lboDivider.visibility = if (adapterPosition == 0) View.VISIBLE else View.GONE

        glide.load(viewModel.avatar)
            .circleCrop()
            .crossFade()
            .into(itemView.ivAvatar)

        itemView.tvNotificationMessage.text =
            getMessageSpan(viewModel.userName ?: "", viewModel.message ?: "")

        viewModel.createdAt?.let { itemView.tvTime.text = setupTimestamp(it) }

        val backgroundColor = if (viewModel.clicked) R.color.white else R.color._f8f8f8
        itemView.clNotificationContainer.setBackgroundColor(ContextCompat.getColor(itemView.context, backgroundColor))

        itemView.setOnClickListener { onClickListener.onClick(viewModel) }

        itemView.ivAvatar.setOnClickListener { openUserActivity(viewModel.userPk) }

        viewModel.type?.let { setTypeImage(it) }
    }

    private fun setTypeImage(type: String) {
        with(itemView) {
            ivTypeIcon.setImageResource(
                when (type) {
                    Share.Live.name, Share.ScheduledLive.name ->
                        R.drawable.ic_notification_type_lives
                    Share.Feed.name ->
                        R.drawable.ic_notification_type_publications
                    Share.Audio.name ->
                        R.drawable.ic_notification_type_drops
                    Share.Product.name ->
                        R.drawable.ic_notification_type_store
                    Share.Call.name ->
                        R.drawable.ic_notification_type_call
                    else ->
                        R.drawable.ic_notification_type_publications
                }
            )
        }
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java)
            .putExtra(USER_PK_EXTRA, creatorPK)
        itemView.context.startActivity(intent)
    }

    private fun getMessageSpan(userName: String, message: String): CharSequence? {
        val position = message.indexOf("{{author}}")
        if (position == -1) return message
        val spannable = SpannableStringBuilder(message.replace("{{author}}", userName, false))
        spannable.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), position, position + userName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannable
    }

    @SuppressLint("SimpleDateFormat")
    private fun setupTimestamp(date: String): String {
        return date.toDate()?.elapsedTime(itemView.context)?.capitalize() ?: ""
    }

    override fun recycle() {
        glide.clear(itemView.ivAvatar)
    }
}
