package co.socialsquad.squad.presentation.feature.store

import co.socialsquad.squad.presentation.custom.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable

interface StoreInteractor {
    fun getStore(): Observable<List<ViewModel>>
    fun getSubcategories(pk: Int, page: Int): Observable<List<ViewModel>>
    fun getProductsByList(pk: Int, page: Int): Observable<StoreProductList>
    fun getProductsByCategory(pk: Int, page: Int): Observable<StoreProductList>
    fun getProductsByQuery(query: String, page: Int): Observable<StoreProductList>
    fun getProduct(pk: Int): Observable<StoreProductViewModel>
    fun getProductDetailGroups(pk: Int): Observable<List<ProductDetailGroupViewModel>>
    fun getProductLocations(pid: Int, latitude: Double, longitude: Double): Observable<List<StoreViewModel>>
    fun sendProductView(pk: Int): Completable
    fun getColor(): String?
}
