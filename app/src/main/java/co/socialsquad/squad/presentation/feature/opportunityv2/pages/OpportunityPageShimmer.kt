package co.socialsquad.squad.presentation.feature.opportunityv2.pages

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetails
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetailsModel
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityObject
import co.socialsquad.squad.presentation.util.argument
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_opportunity_page_details.*
import kotlinx.android.synthetic.main.item_card_opportunity.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class OpportunityPageShimmer : Fragment() {
    private val object2: OpportunityDetailsModel by argument(ARG_PARAM1)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_opportunity_pages_shimmer, container, false)

    companion object {
        @JvmStatic
        fun newInstance() =
                OpportunityPageShimmer()
    }
}