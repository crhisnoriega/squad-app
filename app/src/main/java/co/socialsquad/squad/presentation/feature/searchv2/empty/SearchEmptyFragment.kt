package co.socialsquad.squad.presentation.feature.searchv2.empty

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_empty_state.*

private const val EXTRA_EMPTY_STATE = "ARGUMENT_EMPTY_STATE"

class SearchEmptyFragment : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance(emptyState: EmptyState) = SearchEmptyFragment().apply {
            arguments = bundleOf(EXTRA_EMPTY_STATE to emptyState)
        }
    }

    private var emptyStateData: EmptyState? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_search_empty_state, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emptyStateData = arguments?.get(EXTRA_EMPTY_STATE) as EmptyState
        bind(emptyStateData!!)
    }

    private fun bind(emptyState: EmptyState) {
        tvTitle.text = emptyState.title
        tvDescription.text = emptyState.subtitle
        Glide.with(requireContext()).load(emptyState.icon.url)
            .into(ivIcon)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            emptyStateData =
                savedInstanceState.getParcelable(EXTRA_EMPTY_STATE) as? EmptyState
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(EXTRA_EMPTY_STATE, emptyStateData)
    }
}
