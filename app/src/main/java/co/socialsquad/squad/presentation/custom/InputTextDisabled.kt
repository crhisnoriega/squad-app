package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.content.Context
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlinx.android.synthetic.main.view_input_text.view.*

class InputTextDisabled(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    private var onInputListener: InputView.OnInputListener? = null

    var text: String?
        get() = etInput.text.toString()
        set(value) {
            if (value.isNullOrBlank()) return

            etInput.setText(value)

            if (etInput.visibility == View.GONE) {
                labelAnimator.start()
                etInput.visibility = View.VISIBLE
            }
        }

    val editText: EditText?
        get() = etInput

    val isBlank: Boolean
        get() = etInput.text.toString().isBlank()

    val length: Int
        get() = etInput.length()

    private val labelAnimator = ObjectAnimator
        .ofFloat(12f, 11f)
        .apply {
            duration = 200L
            addUpdateListener { tvLabel.textSize = it.animatedValue as Float }
        }

    var hint = ""
        set(value) {
            tvLabel.text = value
        }

    init {
        View.inflate(context, R.layout.view_input_text_disabled, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputText)) {
            tvLabel.text = getString(R.styleable.InputText_hint)
            etInput.inputType = getInt(R.styleable.InputText_android_inputType, etInput.inputType)
            etInput.imeOptions = getInt(R.styleable.InputText_android_imeOptions, etInput.imeOptions)

            getInt(R.styleable.InputText_android_maxLength, Int.MAX_VALUE).let {
                etInput.filters = arrayOf(InputFilter.LengthFilter(it))
            }

            if (getBoolean(R.styleable.InputText_android_singleLine, false)) {
                etInput.inputType = etInput.inputType xor InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }

            recycle()
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener { onFocusGained() }

        (etInput.parent as View).setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) onFocusGained()
        }

        etInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && isBlank) {
                etInput.setText("")
                etInput.visibility = View.GONE
                with(labelAnimator) {
                    end()
                    reverse()
                }
            }
        }

        etInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                onInputListener?.onInput()
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun onFocusGained() {
        if (etInput.visibility == View.GONE) {
            labelAnimator.start()
            etInput.visibility = View.VISIBLE
        }
        with(etInput) {
            requestFocus()
            showKeyboard()
        }
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    override fun isValid(valid: Boolean) {}
}
