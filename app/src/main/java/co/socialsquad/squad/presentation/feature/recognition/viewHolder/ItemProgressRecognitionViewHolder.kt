package co.socialsquad.squad.presentation.feature.recognition.viewHolder


import android.net.Uri
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.item_progress_recognition.view.*

class ItemProgressRecognitionViewHolder(
        itemView: View, var onClickAction: ((item: ItemProgressRecognitionViewModel) -> Unit)) : ViewHolder<ItemProgressRecognitionViewModel>(itemView) {

    override fun bind(viewModel: ItemProgressRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.requirementData.title
        itemView.txtSubtitle.text = viewModel.requirementData.description

        itemView.txtCounter.text = viewModel.requirementData?.progress?.subtitle
        itemView.approvalBadged.isVisible = viewModel.requirementData.completed!!
        itemView.progress_bar.progress = viewModel.requirementData?.progress?.numeric_value ?: 0

        itemView.iconDetails.isVisible = viewModel.requirementData.show_details!!
        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.requirementData.icon), itemView.imgIcon)

    }

    override fun recycle() {

    }

    companion object{
        const val ITEM_PROGRESS_RECOGNITION_VIEW_HOLDER_ID = R.layout.item_progress_recognition
    }
}