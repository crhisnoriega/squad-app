package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListItem(
    @SerializedName("id") val id: Int,
    @SerializedName("snippet") val snippet: String? = null,
    @SerializedName("media") val media: SimpleMedia? = null,
    @SerializedName("link") val link: Link? = null,
    @SerializedName("short_title") val shortTitle: String,
    @SerializedName("short_description") val shortDescription: String,
    @SerializedName("custom_fields") val customFields: String? = null,
    @SerializedName("location") val pinLocation: PinLocation?,
) : Parcelable {
    fun isObject() = link != null && link.type == LinkType.Object
}
