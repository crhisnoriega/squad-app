package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.ranking.model.RankingData
import co.socialsquad.squad.presentation.feature.ranking.model.RankingItem

class ItemRankingInfoViewModel(
        val rankingItem: RankingItem)
    : ViewModel {
}