package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.ranking.model.RankHeader
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SystemData(
    @SerializedName("id") val id: String?,
    @SerializedName("points_system_header") val points_system_header: RankHeader,
    @SerializedName("icon") val icon: String?,
    @SerializedName("subtitle") val subtitle: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("media_cover") val media_cover: Boolean?,
    @SerializedName("company") val company: Int?,
    @SerializedName("area") val area: List<AreaData>?,
    var first: Boolean = false
) : Parcelable