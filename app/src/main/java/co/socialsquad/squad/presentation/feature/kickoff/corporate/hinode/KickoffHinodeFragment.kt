package co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_kickoff_hinode.*
import java.util.Arrays
import javax.inject.Inject

class KickoffHinodeFragment : Fragment(), KickoffHinodeView, InputView.OnInputListener {

    interface Listener {
        fun onNextClicked()
    }

    @Inject
    lateinit var kickoffHinodePresenter: KickoffHinodePresenter

    private var listener: Listener? = null
    private var miNext: MenuItem? = null
    private var loading: AlertDialog? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        listener = activity as Listener?
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_kickoff_hinode, menu)
        miNext = menu.findItem(R.id.kickoff_hinode_mi_next)
        onInput()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.kickoff_hinode_mi_next -> {
            view?.hideKeyboard()
            val id = itId.text
            val password = itPassword.text
            val arguments = Bundle().apply {
                putString(KickoffHinodePresenter.CREDENTIALS_HINODE_ID, id)
                putString(KickoffHinodePresenter.CREDENTIALS_HINODE_PASSWORD, password)
            }
            kickoffHinodePresenter.onSaveTapped(arguments)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun goToNextFragment() {
        listener?.onNextClicked()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_kickoff_hinode, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        setInputViews(Arrays.asList<InputView>(itId, itPassword))
        setupPasswordField()
    }

    private fun setupPasswordField() {
        itPassword.editText?.apply {
            val currentTypeface = typeface
            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            transformationMethod = PasswordTransformationMethod.getInstance()
            typeface = currentTypeface
        }
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(activity, getString(resId), null)
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(activity)
        if (isVisible) loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun setNextEnabled(enabled: Boolean) {
        miNext?.isEnabled = enabled
    }

    override fun onInput() {
        kickoffHinodePresenter.onInput(itId.text, itPassword.text)
    }

    override fun showErrorMessageFailedToRegisterUser() {
        ViewUtils.showDialog(activity, getString(R.string.kickoff_terms_error_failed_to_register_user), DialogInterface.OnDismissListener {})
    }

    override fun onDestroy() {
        kickoffHinodePresenter.onDestroy()
        super.onDestroy()
    }
}
