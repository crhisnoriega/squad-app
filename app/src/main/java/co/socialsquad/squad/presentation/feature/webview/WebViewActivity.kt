package co.socialsquad.squad.presentation.feature.webview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_intranet_webview.ibBack
import kotlinx.android.synthetic.main.activity_intranet_webview.pbLoading
import kotlinx.android.synthetic.main.activity_intranet_webview.toolbarTitle
import kotlinx.android.synthetic.main.activity_intranet_webview.webView
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseDaggerActivity() {
    private val url by extra<String>(EXTRA_URL)
    private val title by extra<String?>(EXTRA_TITLE)

    companion object {
        const val EXTRA_URL = "extra_url"
        const val EXTRA_TITLE = "extra_title"

        fun newInstance(context: Context, url: String, title: String): Intent {
            return Intent(context, WebViewActivity::class.java).apply {
                putExtra(EXTRA_URL, url)
                putExtra(EXTRA_TITLE, title)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        setupWebView(url)
        toolbarTitle.text = title
        toolbarSubtitle.text = url
        window?.apply {
            decorView.systemUiVisibility = 0
            ibBack.setOnClickListener { finish() }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView(url: String) {
        webView.run {
            clearCache(true)
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    pbLoading.visibility = View.GONE
                    webView.visibility = View.VISIBLE
                }
            }
            clearHistory()
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            loadUrl(url)
            pbLoading.visibility = View.VISIBLE
        }
    }
}
