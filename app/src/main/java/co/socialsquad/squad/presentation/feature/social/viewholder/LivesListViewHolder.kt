package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.LiveListViewModel
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.feature.social.SOCIAL_LIVE_VIEW_MODEL_ID
import kotlinx.android.synthetic.main.view_social_live_list.view.*

class LivesListViewHolder(itemView: View, val listener: Listener<LiveViewModel>) : ViewHolder<LiveListViewModel>(itemView) {
    override fun bind(viewModel: LiveListViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = SOCIAL_LIVE_VIEW_MODEL_ID

            override fun getHolder(viewType: Int, view: View): ViewHolder<LiveViewModel> = LiveViewHolder(view, listener)
        }
        with(itemView.rvLives) {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(viewModel.lives) }
            isFocusable = false
        }
    }

    override fun recycle() {}
}
