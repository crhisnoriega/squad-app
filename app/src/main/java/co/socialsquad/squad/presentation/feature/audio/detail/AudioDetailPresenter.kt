package co.socialsquad.squad.presentation.feature.audio.detail

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.AudioRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_DROP_DETAIL
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_DELETE_DROP
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LIKE_DROP
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_PLAY_DROP
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AudioDetailPresenter @Inject constructor(
    loginRepository: LoginRepository,
    private val audioRepository: AudioRepository,
    private val audioDetailView: AudioDetailView,
    private val tagWorker: TagWorker
) {

    private var compositeDisposable = CompositeDisposable()

    private var audioPk: Int = -1
    private val color = loginRepository.squadColor
    private val userAvatar = loginRepository.userCompany?.user?.avatar
    private val subdomain = loginRepository.subdomain

    fun onCreate(intent: Intent) {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_DROP_DETAIL)
        if (intent.hasExtra(AudioDetailActivity.EXTRA_AUDIO_PK)) {
            getAudioDetail(intent)
        } else if (intent.hasExtra(AudioDetailActivity.EXTRA_AUDIO_VIEW_MODEL)) {
            val model = intent.getSerializableExtra(AudioDetailActivity.EXTRA_AUDIO_VIEW_MODEL) as AudioViewModel
            model.apply {
                audioRepository.getSavedAudio(pk)?.let {
                    isFinished = it.isFinished
                    position = it.position
                }
            }
            audioDetailView.stopLoading()
            audioDetailView.showAudio(model)
        }
    }

    private fun getAudioDetail(intent: Intent) {
        audioPk = intent.getIntExtra(AudioDetailActivity.EXTRA_AUDIO_PK, -1)
        if (audioPk < 0) {
            audioDetailView.showInvalidPk()
            return
        }
        compositeDisposable.add(
            audioRepository.getAudio(audioPk)
                .doOnSubscribe { audioDetailView.startLoading() }
                .doOnTerminate { audioDetailView.stopLoading() }
                .map { AudioViewModel(it, color, userAvatar, subdomain) }
                .subscribe(
                    {
                        audioDetailView.showAudio(it)
                    },
                    {
                        it.printStackTrace()
                        audioDetailView.showInvalidPk()
                    }
                )
        )
    }

    fun onDeleteClicked(audioViewModel: AudioViewModel) {
        tagWorker.tagEvent(TAG_TAP_DELETE_DROP)
        compositeDisposable.add(
            audioRepository.deleteAudio(audioViewModel.pk)
                .doOnSubscribe { audioDetailView.startLoading() }
                .doOnTerminate { audioDetailView.stopLoading() }
                .subscribe(
                    { audioDetailView.close() },
                    {
                        it.printStackTrace()
                        audioDetailView.showMessage(R.string.audio_error_delete)
                    }
                )
        )
    }

    fun onLikeClicked(pk: Int) {
        tagWorker.tagEvent(TAG_TAP_LIKE_DROP)
        compositeDisposable.add(
            audioRepository.likeAudio(pk)
                .subscribe({}, { it.printStackTrace() })
        )
    }

    fun onAudioPlayed(pk: Int) {
        tagWorker.tagEvent(TAG_TAP_PLAY_DROP)
        compositeDisposable.add(
            audioRepository.markAsListened(pk)
                .subscribe({}, { it.printStackTrace() })
        )
    }
}
