package co.socialsquad.squad.presentation.feature.feedback

import co.socialsquad.squad.data.entity.Feedback
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable

class FeedbackViewModel(
    val pk: Int,
    val title: String,
    val icon: String,
    val description: String,
    val sections: List<FeedbackSectionViewModel>
) : ViewModel, Serializable {
    constructor(feedback: Feedback) : this(
        feedback.pk ?: 0,
        feedback.title ?: "",
        feedback.icon ?: "",
        feedback.content ?: "",
        feedback.sections?.filterNotNull()?.map { FeedbackSectionViewModel(it) } ?: emptyList()
    )
}
