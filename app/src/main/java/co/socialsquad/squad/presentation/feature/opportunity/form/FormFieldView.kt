package co.socialsquad.squad.presentation.feature.opportunity.form

interface FormFieldView {
    fun enableNextButton(enabled: Boolean)
    fun setInputType(inputType: Int?)
    fun setInputMask(mask: String?)
    fun setHintText(hintText: String)
    fun setAdditionalText(additionalText: String?, additionalTextEmphasized: String?)
}
