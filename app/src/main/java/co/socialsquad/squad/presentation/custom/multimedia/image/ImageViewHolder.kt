package co.socialsquad.squad.presentation.custom.multimedia.image

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.util.inflate
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide

class ImageViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?) : MediaViewHolder(parent.inflate(R.layout.view_multi_media_pager_image), lifecycleOwner) {

    private val mImage: ImageView = view.findViewById(R.id.image)
    private val mediaCounter: TextView = view.findViewById(R.id.mediaCounter)
    private val cardMediaCounter: CardView = view.findViewById(R.id.cardMediaCounter)

    override fun bind(media: Media, label: String?, isFirst: Boolean, showLabel: Boolean) {
        Glide.with(view.context)
                .load(media.url)
                .into(mImage)
        mediaCounter.text = label
        showLabel?.let {
            cardMediaCounter.isVisible = it
        }
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {
    }
}
