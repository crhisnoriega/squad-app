package co.socialsquad.squad.presentation.feature.chat.chatroom

import android.content.Context
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.repository.ChatRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_CHAT_ROOM
import co.socialsquad.squad.presentation.feature.chat.MessageViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

const val EXTRA_CHAT_ROOM = "extra_chat_room"

class ChatRoomPresenter @Inject constructor(
    loginRepository: LoginRepository,
    private val chatRepository: ChatRepository,
    private val chatRoomView: ChatRoomView,
    private val tagWorker: TagWorker
) {

    private lateinit var chatRoomActivity: ChatRoomActivity
    private val compositeDisposable = CompositeDisposable()
    private var currentRoomModel: ChatRoomModel? = null
    private val currentUserChatId = loginRepository.userCompany?.user?.chatId
    private val companyColor = loginRepository.userCompany?.company?.primaryColor

    fun onCreate(intent: Intent, chatRoomActivity: ChatRoomActivity) {
        this.chatRoomActivity = chatRoomActivity
        tagWorker.tagEvent(TAG_SCREEN_VIEW_CHAT_ROOM)
        if (intent.hasExtra(EXTRA_CHAT_ROOM)) {
            val roomPk = intent.getIntExtra(EXTRA_CHAT_ROOM, -1)
            if (roomPk > 0) {
                compositeDisposable.add(
                    chatRepository.getChatRoom(roomPk)
                        .onDefaultSchedulers()
                        .subscribe(
                            {
                                currentRoomModel = it
                                chatRoomView.setupToolbar(ChatRoomViewModel(it, companyColor))
                                connectToRoom()
                                loadPreviousMessages(chatRoomActivity)
                                chatRepository.getChatRoomLiveData(roomPk).observe(
                                    chatRoomActivity,
                                    Observer<ChatRoomModel> { chatRoom ->
                                        chatRoom?.let {
                                            currentRoomModel = it
                                            chatRoomView.setupToolbar(ChatRoomViewModel(it, companyColor))
                                        }
                                    }
                                )
                            },
                            { it.printStackTrace() }
                        )
                )
            }
        }
    }

    private fun connectToRoom() {
        currentRoomModel?.let { chatRoomView.updateRoomService(it) }
    }

    private fun loadPreviousMessages(context: Context) {
        currentRoomModel?.channelUrl?.let {
            chatRepository.getMessagesFromChatRoom(context, it).observe(
                chatRoomActivity,
                Observer<PagedList<MessageViewModel>> { pagedList ->
                    pagedList?.let { list ->
                        chatRoomView.setItems(list)
                    }
                }
            )
        }
    }

    fun onSendMessage(message: String) {
        chatRoomView.clearTextBox()
        currentRoomModel?.let {
            chatRoomView.sendMessageToService(it.channelUrl, message)
        }
    }

    fun onBackButtonClicked() {
        chatRoomView.finishChatRoom(currentRoomModel)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
