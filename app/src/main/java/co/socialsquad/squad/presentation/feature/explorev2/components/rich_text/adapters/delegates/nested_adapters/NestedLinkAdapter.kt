package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.nested_adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinkDetails
import co.socialsquad.squad.presentation.util.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_rich_text_nested_link.view.*

class NestedLinkAdapter(private val listener: RichTextListener, private val list: List<RichTextLinkDetails>?) :
    RecyclerView.Adapter<NestedLinkAdapter.NestedLinkViewHolder>() {

    inner class NestedLinkViewHolder(private val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_rich_text_nested_link)) {

        val tvLinkTitle: TextView = itemView.tv_item_rich_text_nested_title
        val tvLinkDescription: TextView = itemView.tv_item_rich_text_nested_description
        val ivIcon: ImageView = itemView.iv_item_rich_text_nested_media
        val container: ViewGroup = itemView.cl_item_rich_text_nested_link_container

        fun bind(richTextLinkDetails: RichTextLinkDetails) {
            if (richTextLinkDetails.title.isNullOrBlank()) {
                tvLinkTitle.visibility = View.GONE
            } else {
                tvLinkTitle.text = richTextLinkDetails.title
            }

            if (richTextLinkDetails.description.isNullOrBlank()) {
                tvLinkDescription.visibility = View.GONE
            } else {
                tvLinkDescription.text = richTextLinkDetails.description
            }

            Glide.with(parent.context).load(richTextLinkDetails.icon?.url).centerInside()
                .into(ivIcon)
            container.setOnClickListener {
                listener.onRichTextLinkClicked(richTextLinkDetails)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NestedLinkViewHolder {
        return NestedLinkViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: NestedLinkViewHolder, position: Int) {
        list?.get(position)?.let { holder.bind(it) }
    }
}
