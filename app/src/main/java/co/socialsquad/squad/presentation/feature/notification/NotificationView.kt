package co.socialsquad.squad.presentation.feature.notification

import co.socialsquad.squad.presentation.custom.ViewModel

interface NotificationView {
    fun setItems(items: List<ViewModel>)
    fun addItens(items: List<ViewModel>)
    fun showRefreshing()
    fun hideRefreshing()
    fun showLoadingMore()
    fun hideLoadingMore()
    fun updateNotificationBadge()
}
