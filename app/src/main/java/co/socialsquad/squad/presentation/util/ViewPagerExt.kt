package co.socialsquad.squad.presentation.util

import android.os.Handler
import androidx.viewpager2.widget.ViewPager2

fun ViewPager2.autoScroll(interval: Long = 5000L) {
    val handler = Handler()
    var scrollPosition = 0
    val runnable = object : Runnable {
        override fun run() {
            val count = adapter?.itemCount ?: 0
            setCurrentItem(scrollPosition++ % count, true)
            handler.postDelayed(this, interval)
        }
    }
    handler.post(runnable)
}
