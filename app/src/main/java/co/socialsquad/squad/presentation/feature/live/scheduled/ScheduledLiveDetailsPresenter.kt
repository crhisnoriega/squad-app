package co.socialsquad.squad.presentation.feature.live.scheduled

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedComment
import co.socialsquad.squad.data.entity.ScheduledLiveCommentRequest
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_API_RESPONSE_SCHEDULED_LIVE_DELETE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_SCHEDULED_LIVE_DETAILS
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LIVE_CONFIRM_ATTENDANCE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LIVE_DELETE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULED_LIVE_COMMENT
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULED_LIVE_DELETE_COMMENT
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULED_LIVE_SHARE
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewModel
import co.socialsquad.squad.presentation.util.CameraUtils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private const val REQUEST_CODE_PERMISSIONS = 4

class ScheduledLiveDetailsPresenter
@Inject constructor(
    private val context: Context,
    private val scheduledLiveDetailsView: ScheduledLiveDetailsView,
    private val liveRepository: LiveRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var pk: Int = -1
    private var live: LiveViewModel? = null
    private var commentPage: Int = 1
    private var isCompleted = false
    private var isLoading = false
    private val currentUserAvatar = loginRepository.userCompany?.user?.avatar
    private val subdomain = loginRepository.subdomain

    fun onCreate(intent: Intent) {
        scheduledLiveDetailsView.setupToolbar(R.string.scheduled_live_detail_title)
        pk = intent.extras?.getInt(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_PK, -1) ?: -1
        live = intent.extras?.getSerializable(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_VIEW_MODEL) as? LiveViewModel
        if (pk > 0) {
            getDataByPK(pk)
            val params = Bundle()
            params.putString("from", "NotificationFirebase|Share")
            tagWorker.tagEvent(TAG_SCREEN_VIEW_SCHEDULED_LIVE_DETAILS, params)
            return
        }
        if (live != null) {
            scheduledLiveDetailsView.showData(
                live!!, currentUserAvatar, loginRepository.squadColor,
                loginRepository.userCompany?.pk
                    ?: 0
            )
            pk = live!!.pk
            val params = Bundle()
            params.putString("from", "Live List | Feed")
            tagWorker.tagEvent(TAG_SCREEN_VIEW_SCHEDULED_LIVE_DETAILS, params)
            showComments()
            return
        }
        scheduledLiveDetailsView.showInvalidPk()
        return
    }

    private fun getDataByPK(pk: Int) {
        compositeDisposable.add(
            liveRepository.getLive(pk)
                .onDefaultSchedulers()
                .doOnSubscribe { scheduledLiveDetailsView.showLoading() }
                .doOnComplete { scheduledLiveDetailsView.hideLoading() }
                .map { LiveViewModel(it, subdomain = subdomain) }
                .subscribe(
                    {
                        live = it
                        when {
                            it.active -> scheduledLiveDetailsView.openLiveViewer(it)
                            it.scheduledStartDate != null -> scheduledLiveDetailsView.showData(
                                it,
                                currentUserAvatar,
                                loginRepository.squadColor,
                                loginRepository.userCompany?.pk ?: 0
                            )
                            else -> scheduledLiveDetailsView.showInvalidPk()
                        }
                    },
                    {
                        it.printStackTrace()
                        scheduledLiveDetailsView.showInvalidPk()
                    },
                    {
                        showComments()
                    }
                )
        )
    }

    fun onStartClicked() {
        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
        if (checkPermissions(permissions)) {
            if (CameraUtils(context).isCameraAvailable()) {
                scheduledLiveDetailsView.openLiveCreator()
            } else {
                scheduledLiveDetailsView.showMessage(R.string.error_camera_not_available)
            }
        } else {
            scheduledLiveDetailsView.requestPermissions(REQUEST_CODE_PERMISSIONS, permissions)
        }
    }

    private fun checkPermissions(permissions: Array<String>) = permissions.all {
        ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
        if (grantResults.isEmpty() || grantResults.any { it == PackageManager.PERMISSION_DENIED }) {
            scheduledLiveDetailsView.showMessage(R.string.error_permissions_not_granted)
        } else when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> onStartClicked()
        }
    }

    fun onConfirmAttendanceClicked() {
        if (pk < 0) {
            return
        }
        tagWorker.tagEvent(TAG_TAP_LIVE_CONFIRM_ATTENDANCE)
        compositeDisposable.add(
            liveRepository.confirmScheduledLiveAttendance(pk)
                .map { LiveViewModel(it, subdomain = subdomain) }
                .subscribe(
                    {
                        scheduledLiveDetailsView.showData(
                            it,
                            currentUserAvatar,
                            loginRepository.squadColor,
                            loginRepository.userCompany?.pk ?: 0
                        )
                    },
                    { it.printStackTrace() }, {}
                )
        )
    }

    fun onDeleteClicked() {
        if (pk < 0) {
            return
        }
        tagWorker.tagEvent(TAG_TAP_LIVE_DELETE)
        compositeDisposable.clear()
        compositeDisposable.add(
            liveRepository.deleteLive(pk)
                .doOnSubscribe { scheduledLiveDetailsView.showLoading() }
                .doOnComplete { scheduledLiveDetailsView.hideLoading() }
                .subscribe(
                    {
                        scheduledLiveDetailsView.finishAfterDelete()
                        val params = Bundle()
                        params.putBoolean("success", true)
                        tagWorker.tagEvent(TAG_API_RESPONSE_SCHEDULED_LIVE_DELETE, params)
                    },
                    {
                        scheduledLiveDetailsView.showMessage(R.string.scheduled_live_details_deletion_failure)
                        val params = Bundle()
                        params.putBoolean("success", false)
                        tagWorker.tagEvent(TAG_API_RESPONSE_SCHEDULED_LIVE_DELETE, params)
                    }
                )
        )
    }

    private fun mapComments(comments: List<FeedComment>?) = comments.orEmpty().map { mapCommentsViewModel(it) }

    private fun mapCommentsViewModel(comment: FeedComment) = CommentViewModel(
        comment.pk,
        comment.userCompany?.pk ?: -1,
        comment.userCompany?.user?.avatar,
        comment.userCompany?.user?.firstName + " " + comment.userCompany?.user?.lastName,
        comment.text ?: "",
        comment.createdAt,
        comment.canDelete,
        comment.canEdit
    )

    private fun showComments() {
        commentPage = 1
        compositeDisposable.add(
            liveRepository.getLiveComments(pk, commentPage)
                .doOnSubscribe {
                    isLoading = true
                    scheduledLiveDetailsView.showLoadingComments()
                }
                .doOnComplete {
                    commentPage++
                    isLoading = false
                    scheduledLiveDetailsView.hideLoadingComments()
                }
                .doOnNext { if (it.next == null) isCompleted = true }
                .map { it.results }
                .map { mapComments(it) }
                .subscribe(
                    { scheduledLiveDetailsView.setComments(it) },
                    { it.printStackTrace() }
                )
        )
    }

    fun onCommentClicked(viewModel: LiveViewModel, text: String) {
        tagWorker.tagEvent(TAG_TAP_SCHEDULED_LIVE_COMMENT)
        val commentViewModel = CommentViewModel(
            -1,
            loginRepository.userCompany?.pk ?: -1, loginRepository.userCompany?.user?.avatar,
            loginRepository.userCompany?.user?.firstName + " " + loginRepository.userCompany?.user?.lastName,
            text, null, false, false
        )
        scheduledLiveDetailsView.insertNewComment(commentViewModel)
        compositeDisposable.add(
            liveRepository.commentLive(ScheduledLiveCommentRequest(viewModel.pk, text))
                .subscribe(
                    {
                        commentViewModel.pk = it.pk
                        commentViewModel.time = it.createdAt
                        commentViewModel.text = it.text ?: ""
                        commentViewModel.userAvatar = it.userCompany?.user?.avatar
                        commentViewModel.canDelete = it.canDelete
                        commentViewModel.canEdit = it.canEdit
                        scheduledLiveDetailsView.updateComment(commentViewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        scheduledLiveDetailsView.removeComment(commentViewModel)
                        scheduledLiveDetailsView.showMessage(R.string.social_error_failed_to_comment_post)
                    }
                )
        )
    }

    fun onDeleteCommentClicked(viewModel: CommentViewModel) {
        tagWorker.tagEvent(TAG_TAP_SCHEDULED_LIVE_DELETE_COMMENT)
        compositeDisposable.add(
            liveRepository.deleteLiveComment(viewModel.pk)
                .subscribe(
                    { scheduledLiveDetailsView.removeComment(viewModel) },
                    {
                        it.printStackTrace()
                        scheduledLiveDetailsView.showMessage(R.string.social_error_failed_to_remove_comment)
                    }
                )
        )
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isLoading || isCompleted) return
        compositeDisposable.add(
            liveRepository.getLiveComments(pk, commentPage)
                .doOnSubscribe {
                    isLoading = true
                    scheduledLiveDetailsView.showLoadingComments()
                }
                .doOnComplete {
                    commentPage++
                    isLoading = false
                    scheduledLiveDetailsView.hideLoadingComments()
                }
                .doOnNext { if (it.next == null) isCompleted = true }
                .map { it.results }
                .map { mapComments(it) }
                .subscribe(
                    { scheduledLiveDetailsView.addComments(it) },
                    { it.printStackTrace() }
                )
        )
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULED_LIVE_SHARE)
    }

    fun onRefresh() {
        compositeDisposable.clear()
        showComments()
    }
}
