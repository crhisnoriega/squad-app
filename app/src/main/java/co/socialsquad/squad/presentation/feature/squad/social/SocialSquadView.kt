package co.socialsquad.squad.presentation.feature.squad.social

import android.content.DialogInterface
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModel

interface SocialSquadView {
    fun onAudioToggled(audioEnabled: Boolean)
    fun showProfile(profileViewModel: SquadHeaderViewModel)
    fun showPosts(posts: List<SocialViewModel>)
    fun hideDeleteDialog()
    fun showLoadingMore()
    fun hideLoadingMore()
    fun stopRefreshing()
    fun showLoading()
    fun hideLoading()
    fun showEditMedia(post: PostViewModel)
    fun showEditLive(post: PostLiveViewModel)
    fun removePost(viewModel: ViewModel)
    fun updatePost(post: PostViewModel)
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?)
    fun setupToolbar(title: String, textColor: String, backgroundColor: String)
}
