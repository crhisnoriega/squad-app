package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RankingTerms(
        @SerializedName("title") val title: String?,
        @SerializedName("introduction") val introduction: String?,
        @SerializedName("last_update") val last_update: String?,
        @SerializedName("sections") val sections: List<RankingTermsSection>?,
) : Parcelable/**/