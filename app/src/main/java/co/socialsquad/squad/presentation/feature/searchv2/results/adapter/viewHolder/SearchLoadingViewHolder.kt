package co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R

class SearchLoadingViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

    internal fun bind(searchTerm: String?) {
        if (searchTerm.isNullOrBlank()) {
            itemView.findViewById<TextView>(R.id.tvDescription)?.text = "Procurando..."
        } else {
            itemView.findViewById<TextView>(R.id.tvDescription)?.text = "Procurando\n\"$searchTerm\""
        }

        var icon = itemView.findViewById<ImageView>(R.id.ivIcon)
        val rotation = AnimationUtils.loadAnimation(itemView.context, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE

        icon.startAnimation(rotation)
    }

    fun updateText(searchTerm: String?) {
        itemView.findViewById<TextView>(R.id.tvDescription)?.text = "Procurando\n\"$searchTerm\""
    }
}
