package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.view_social_post_interactions.view.*

class SocialLikesViewModel(
    var currentUserAvatar: String? = null,
    var liked: Boolean,
    var likeAvatars: List<String>? = null,
    var likeCount: Int = 0
) : ViewModel

class SocialLikesViewHolder<T : ViewModel>(
    itemView: View,
    private val viewModel: T,
    private val onLikeListener: Listener<T>,
    private val onLikeCountListener: Listener<T>
) : ViewHolder<SocialLikesViewModel>(itemView) {

    override fun bind(viewModel: SocialLikesViewModel) {
        with(viewModel) viewModel@{
            itemView.apply {
                fun updateIcon() {
                    val resId = if (liked) R.drawable.ic_heart_active else R.drawable.ic_heart_inactive
                    ivLike.setImageDrawable(context.getDrawable(resId))
                }

                fun setLikeCountText(likeCount: Int) {
                    when {
                        likeCount <= 0 -> urslAvatars.visibility = View.GONE
                        likeCount == 1 -> {
                            urslAvatars.visibility = View.VISIBLE
                            urslAvatars.setText(context.getString(R.string.social_post_one_member_liked, likeCount))
                        }
                        else -> {
                            urslAvatars.visibility = View.VISIBLE
                            urslAvatars.setText(context.getString(R.string.social_post_members_liked, likeCount))
                        }
                    }
                }
                updateIcon()

                llLikeContainer.setOnClickListener {

                    liked = !liked
                    if (liked) {
                        likeCount++
                        setLikeCountText(likeCount)
                        likeAvatars = likeAvatars?.toMutableList()?.apply { currentUserAvatar?.let { add(it) } }
                        urslAvatars.setUserImages(likeAvatars)
                    } else {
                        likeCount--
                        setLikeCountText(likeCount)
                        likeAvatars = likeAvatars?.toMutableList()?.apply { removeAll { it == currentUserAvatar } }
                        urslAvatars.setUserImages(likeAvatars)
                    }

                    onLikeListener.onClick(this@SocialLikesViewHolder.viewModel)
                    updateIcon()
                }

                setLikeCountText(likeCount)

                likeAvatars?.let { urslAvatars.setUserImages(it) }

                urslAvatars.setOnClickListener {
                    onLikeCountListener.onClick(this@SocialLikesViewHolder.viewModel)
                }
            }
        }
    }

    override fun recycle() {
        itemView.urslAvatars.recycle()
    }
}
