package co.socialsquad.squad.presentation.feature.document.list

import co.socialsquad.squad.data.repository.DocumentRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_DOCUMENT_LIST
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DocumentListPresenter @Inject constructor(
    private val view: DocumentListView,
    private val tagWorker: TagWorker,
    private val documentRepository: DocumentRepository,
    loginRepository: LoginRepository
) {

    private val compositeDisposable = CompositeDisposable()
    private var page = 1
    private var isLoading = true
    private var isComplete = false
    private val companyColor = loginRepository.userCompany?.company?.primaryColor

    fun onViewCreated() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_DOCUMENT_LIST)
        view.setupList(companyColor)
        subscribe()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            documentRepository.getDocuments()
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }
                .doOnNext { if (it.next == null) isComplete = true }
                .map { it.results.orEmpty().map { DocumentViewModel(it) } }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            val shouldClearList = page == 1
                            view.addItems(it, shouldClearList)
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                    },
                    {}
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }

    fun onDocumentClicked(viewModel: DocumentViewModel) {
        compositeDisposable.add(
            documentRepository.markDocumentViewed(viewModel.pk)
                .subscribe({}, { it.printStackTrace() })
        )
    }
}
