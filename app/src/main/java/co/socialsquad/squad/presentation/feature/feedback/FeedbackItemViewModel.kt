package co.socialsquad.squad.presentation.feature.feedback

import co.socialsquad.squad.data.entity.FeedbackItem
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable

sealed class FeedbackItemViewModel : ViewModel, Serializable {
    class FeedbackTextItemViewModel(
        val pk: Int,
        val hint: String
    ) : FeedbackItemViewModel() {
        var text = ""

        constructor(item: FeedbackItem) : this(
            item.pk ?: 0,
            item.text ?: ""
        )
    }

    class FeedbackOptionItemViewModel(
        val pk: Int,
        val title: String,
        val imageUrl: String?,
        val isMultiSelect: Boolean
    ) : FeedbackItemViewModel() {
        var isSelected = false

        constructor(item: FeedbackItem, isMultipleChoice: Boolean) : this(
            item.pk ?: 0,
            item.text ?: "",
            item.icon,
            isMultipleChoice
        )
    }
}
