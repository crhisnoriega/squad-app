package co.socialsquad.squad.presentation.feature.login.subdomain

interface LoginSubdomainView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(resId: Int)
    fun enableNextButton(enabled: Boolean)
    fun openLoginEmail(subdomain: String)
    fun openLoginAlternative(subdomain: String)
}
