package co.socialsquad.squad.presentation.feature.call

import dagger.Binds
import dagger.Module

@Module
abstract class CallRoomModule {
    @Binds
    abstract fun view(callRoomActivity: CallRoomActivity): CallRoomView
}
