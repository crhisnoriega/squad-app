package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.text.format.DateUtils
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import co.socialsquad.squad.presentation.feature.event.detail.EventDetailActivity
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_events_item.view.*

class SearchEventViewHolder(itemView: View) : ViewHolder<EventViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: EventViewModel) {
        with(viewModel) {
            glide.load(cover)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.ivImage)

            itemView.short_title_lead.text = title

            itemView.tvAuthor.apply {
                text = location
                creatorPk?.let { pk -> setOnClickListener { openUser(pk) } }
            }

            var dateString = ""
            viewModel.startDate?.let {
                dateString += DateUtils.formatDateTime(itemView.context, it.time, DateUtils.FORMAT_NO_YEAR)
            }
            viewModel.endDate?.let {
                dateString += " - " + DateUtils.formatDateTime(itemView.context, it.time, DateUtils.FORMAT_NO_YEAR)
            }
            itemView.tvLocationAndTimestamp.text = dateString

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, EventDetailActivity::class.java).apply {
                    putExtra(EventDetailActivity.EXTRA_EVENT_VIEW_MODEL, viewModel)
                }
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }
}
