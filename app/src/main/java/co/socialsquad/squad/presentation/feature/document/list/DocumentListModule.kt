package co.socialsquad.squad.presentation.feature.document.list

import dagger.Binds
import dagger.Module

@Module
interface DocumentListModule {
    @Binds
    fun view(view: DocumentListFragment): DocumentListView
}
