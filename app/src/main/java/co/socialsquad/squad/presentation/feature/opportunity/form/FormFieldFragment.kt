package co.socialsquad.squad.presentation.feature.opportunity.form

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.style.ForegroundColorSpan
import android.text.style.TypefaceSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Field
import co.socialsquad.squad.data.entity.opportunity.FormFieldValue
import co.socialsquad.squad.presentation.util.MaskUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_form_field.*
import javax.inject.Inject

class FormFieldFragment : Fragment(), FormFieldView {

    private var formFieldListener: FormFieldListener? = null

    @Inject
    lateinit var presenterForm: FormFieldPresenter

    private var field: Field? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            field = it.getParcelable(ARG_FIELD)
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        formFieldListener = activity as FormFieldListener?
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_form_field, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        field?.let { presenterForm.start(it) }
        setupField()
    }

    private fun setupField() {
        field?.let { field ->
            iltInput.addTextChangedListener { text -> presenterForm.onInputChanged(text) }
            iltInput.apply {
                presenterForm.setupField()
                setHintInput(if (field.isMandatory) "${field.label} *" else field.label)
                setHintText(field.label)
                setHelpText(field.textHelper)
                requestFocus()
            }
        }
    }

    companion object {
        private const val ARG_FIELD = "ARG_FIELD"

        @JvmStatic
        fun newInstance(field: Field) =
            FormFieldFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_FIELD, field)
                }
            }
    }

    override fun enableNextButton(enabled: Boolean) {
        formFieldListener?.onEnableNextButton(enabled)
    }

    override fun setInputType(inputType: Int?) {
        inputType?.let { type ->
            iltInput.editText?.let {
                it.inputType = type
            }
        }
    }

    override fun setInputMask(mask: String?) {
        mask?.let { mask ->
            iltInput.editText?.let {
                MaskUtils.applyMask(it, mask)
            }
        }
    }

    override fun setHintText(hintText: String) {
        iltInput.setHintText(hintText)
    }

    override fun setAdditionalText(additionalText: String?, additionalTextEmphasized: String?) {
        tvAdditionalText.text = buildSpannedString {
            additionalText?.let { append(it) }
            additionalTextEmphasized?.let {
                val typeface = Typeface.createFromAsset(context?.assets, "fonts/gotham_bold.ttf")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    inSpans(TypefaceSpan(typeface), ForegroundColorSpan(ContextCompat.getColor(tvAdditionalText.context, R.color.grass))) {
                        append(it)
                    }
                }
            }
        }
    }

    fun getFieldValue() = field?.let { FormFieldValue(id = it.id, value = iltInput.getFieldText().toString()) }

    fun isFieldMandatory() = field?.isMandatory
    fun isFieldValid() = if (presenterForm != null) presenterForm.isInputValid(iltInput.getFieldText().toString()) else false
}
