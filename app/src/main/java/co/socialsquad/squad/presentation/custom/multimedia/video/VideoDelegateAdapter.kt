package co.socialsquad.squad.presentation.custom.multimedia.video

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.custom.multimedia.ViewTypeDelegateAdapter

class VideoDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?): MediaViewHolder =
        VideoViewHolder(parent, lifecycleOwner)
}
