package co.socialsquad.squad.presentation.feature.live.list

import dagger.Binds
import dagger.Module

@Module
interface LiveListModule {
    @Binds
    fun view(liveListFragment: LiveListFragment): LiveListView
}
