package co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values

import dagger.Binds
import dagger.Module

@Module
abstract class SegmentValuesModule {
    @Binds
    abstract fun view(segmentValuesActivity: SegmentValuesActivity): SegmentValuesView
}
