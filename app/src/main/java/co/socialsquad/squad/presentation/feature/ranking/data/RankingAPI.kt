package co.socialsquad.squad.presentation.feature.ranking.data

import co.socialsquad.squad.presentation.feature.ranking.model.*
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface RankingAPI {

    @GET("/endpoints/ranking/rank/summary")
    suspend fun fetchRankingList(): RankingResponse

    @GET("/endpoints/ranking/rank/{ranking_id}/")
    suspend fun fetchRankingDetails(@Path("ranking_id") rankingId: String): RankingResponse

    @POST("/endpoints/ranking/rank/{ranking_id}/accept-terms/")
    suspend fun agreeRankingTerms(@Path("ranking_id") rankingId: String): RankingResponse

    @POST("/endpoints/ranking/rank/{ranking_id}/refuse-terms/")
    suspend fun refuseRankingTerms(@Path("ranking_id") rankingId: String): RankingResponse


    @GET("/endpoints/ranking/rank/{rank_id}/positions/")
    suspend fun fetchPositions(@Path("rank_id") rankId: String, @Query("page") page: Int): PositionsResponse

    @GET("/endpoints/ranking/criteria/?")
    suspend fun fetchCriterias(@Query("rank_id") rank_id: String): CriteriasResponse

    @GET("/endpoints/ranking/benefit")
    suspend fun fetchBenefits(): BenefitsResponse


    @GET(" /endpoints/ranking/criteria/{criteria_id}/")
    suspend fun fetchCriteriaDetails(@Path("criteria_id") criteriaId: String): CriteriaDetailResponse

    @GET(" /endpoints/ranking/benefit/{benefit_id}/")
    suspend fun fetchBenefitaDetails(@Path("benefit_id") benefitId: String): BenefitDetailResponse

}
