package co.socialsquad.squad.presentation.feature.audio.record

import android.content.Context
import android.media.MediaMetadataRetriever
import android.media.MediaRecorder
import android.net.Uri
import co.socialsquad.squad.data.repository.AudioRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.util.getDuration
import javax.inject.Inject

class AudioRecordPresenter @Inject constructor(
    private val context: Context,
    loginRepository: LoginRepository,
    audioRepository: AudioRepository,
    private val audioRecordView: AudioRecordView
) {

    private var mediaRecorder: MediaRecorder? = null
    private var userName = with(loginRepository.userCompany?.user) { listOfNotNull(this?.firstName, this?.lastName).joinToString(" ") }
    private var userAvatar = loginRepository.userCompany?.user?.avatar ?: ""
    private var color = loginRepository.squadColor
    private val temporaryAudioFile = audioRepository.createTemporaryAudioFile()
    private val temporaryAudioViewModel = AudioViewModel(
        -1,
        "",
        "",
        Uri.fromFile(temporaryAudioFile).toString(),
        userName,
        -1,
        "",
        userAvatar,
        "",
        "",
        "",
        color,
        0,
        false,
        null,
        0,
        userAvatar,
        0,
        true,
        false,
        false,
        false,
        false,
        loginRepository.subdomain
    )

    fun onRecordClicked() {
        audioRecordView.showStopButton()
        mediaRecorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC)
            setAudioChannels(2)
            setOutputFile(temporaryAudioFile.absolutePath)
            prepare()
        }
        mediaRecorder?.start()
    }

    fun onStopClicked() {
        mediaRecorder?.apply {
            stop()
            release()
        }
        setupPlayer()
        audioRecordView.showPlayer()
        audioRecordView.enableNextOption()
    }

    fun onResetClicked() {
        audioRecordView.showRecordButton()
        temporaryAudioViewModel.resetState()
        audioRecordView.disableNextOption()
    }

    fun onPostClicked() {
        mediaRecorder?.release()
        audioRecordView.openAudioCreateActivity(Uri.fromFile(temporaryAudioFile).toString())
    }

    fun onResume() {
        mediaRecorder?.let {
            setupPlayer()
            audioRecordView.showPlayer()
            audioRecordView.enableNextOption()
        }
    }

    fun onStop() {
        mediaRecorder?.release()
    }

    private fun setupPlayer() {
        temporaryAudioViewModel.duration = MediaMetadataRetriever().getDuration(context, Uri.parse(temporaryAudioFile.absolutePath))
        audioRecordView.setupPlayer(temporaryAudioViewModel)
    }
}
