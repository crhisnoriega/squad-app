package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class AreaData(
        @SerializedName("id") val id: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("media_cover") val media_cover: Boolean?,
        @SerializedName("company") val company: Int?,
        @SerializedName("criteria") val criteria: CriteriaList?,
        @SerializedName("area_header") val area_header: AreaHeaderData?,
        var first: Boolean = false
) : Parcelable