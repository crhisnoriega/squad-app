package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel

const val QUALIFICATION_EMPTY_HEADER_VIEW_HOLDER_ID = R.layout.view_qualidication_header_empty_item

class QualificationProgressEmptyHeaderViewHolder<T : ViewModel>(itemView: View) : ViewHolder<T>(itemView) {
    override fun bind(viewModel: T) {}

    override fun recycle() {}
}
