package co.socialsquad.squad.presentation.feature.signup.credentials

interface SignupCredentialsView {
    fun openKickoff()
    fun showMessage(resId: Int)
    fun showLoading()
    fun hideLoading()
    fun navigateToSuccessView()
    fun setErrorMessage(res: Int)
}
