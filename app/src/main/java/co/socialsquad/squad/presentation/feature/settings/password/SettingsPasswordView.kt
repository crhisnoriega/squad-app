package co.socialsquad.squad.presentation.feature.settings.password

interface SettingsPasswordView {
    fun setupToolbar()
    fun setupKeyboardActions()
    fun setupInputListener()
    fun setupPasswordFields()
    fun setDoneButtonEnabled(enabled: Boolean)
    fun showMessageNewPasswordSet()
    fun showErrorMessageFailedToEditPassword()
    fun showLoading()
    fun hideLoading()
}
