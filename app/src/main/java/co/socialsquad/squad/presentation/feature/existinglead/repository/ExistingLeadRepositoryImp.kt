package co.socialsquad.squad.presentation.feature.existinglead.repository

import co.socialsquad.squad.domain.model.leadsearch.LeadSearchResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow

@ExperimentalCoroutinesApi
class ExistingLeadRepositoryImp(private val api: ExistingLeadAPI) : ExistingLeadRepository {

    override fun searchExistingLead(path: String): Flow<LeadSearchResponse> {
        return flow {
            emit(api.searchExistingLead(path))
        }.debounce(500)
    }
}
