package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class StreamData(
    @SerializedName("id") val id: Int,
    @SerializedName("header") val headerData: HeaderData?,
    @SerializedName("earning_sources_explore") val earning_sources_explore: List<SectionItem>?,
    @SerializedName("events") val events: EventList?,
    var first: Boolean = false
) : Parcelable