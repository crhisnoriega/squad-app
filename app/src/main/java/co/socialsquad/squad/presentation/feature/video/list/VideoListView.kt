package co.socialsquad.squad.presentation.feature.video.list

interface VideoListView {
    fun showMessage(textResId: Int, onDismissListener: () -> Unit = {})
    fun setItems(items: List<VideoListItemViewModel>)
    fun addItems(items: List<VideoListItemViewModel>)
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun startRefreshing()
    fun stopRefreshing()
}
