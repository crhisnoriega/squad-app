package co.socialsquad.squad.presentation.util

import android.content.Context
import android.util.DisplayMetrics
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

object RecyclerViewUtils {
    private const val MILLISECONDS_PER_INCH = 10f

    fun scrollToTop(context: Context, linearLayoutManager: LinearLayoutManager) {
        val smoothScroller: RecyclerView.SmoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference() = LinearSmoothScroller.SNAP_TO_START
            override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                return (MILLISECONDS_PER_INCH / (linearLayoutManager.itemCount / 10)) / displayMetrics.densityDpi
            }
        }
        smoothScroller.targetPosition = 0
        linearLayoutManager.startSmoothScroll(smoothScroller)
    }
}
