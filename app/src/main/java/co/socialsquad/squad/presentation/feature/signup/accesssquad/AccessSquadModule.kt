package co.socialsquad.squad.presentation.feature.signup.accesssquad

import dagger.Binds
import dagger.Module

@Module
abstract class AccessSquadModule {
    @Binds
    abstract fun view(accessSquadActivity: AccessSquadActivity): AccessSquadView
}
