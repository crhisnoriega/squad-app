package co.socialsquad.squad.presentation.feature.company

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_company_login.*
import java.util.Arrays
import javax.inject.Inject

class CompanyLoginActivity : BaseDaggerActivity(), CompanyLoginView, InputView.OnInputListener {

    private var miSave: MenuItem? = null

    @Inject
    lateinit var companyLoginPresenter: CompanyLoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_login)
        setupToolbar()
        setupInputListener()
        companyLoginPresenter.onCreate()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.hub_title)
    }

    override fun fillIdField(id: String) {
        itId.text = id
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        menuInflater.inflate(R.menu.menu_profile_edit, menu)
        miSave = menu.findItem(R.id.profile_edit_mi_save)
        onInput()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.profile_edit_mi_save -> {
            itId.hideKeyboard()
            companyLoginPresenter.onSaveClicked(
                itId.text!!,
                itPassword.text!!
            )
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupInputListener() {
        setInputViews(Arrays.asList(itId as InputView, itPassword as InputView))
    }

    override fun onInput() {
        companyLoginPresenter.onInput(
            itId.text,
            itPassword.text
        )

        itId.isValid(!itId.isBlank)
        itPassword.isValid(!itPassword.isBlank)
    }

    override fun setSaveButtonEnabled(enabled: Boolean) {
        miSave?.isEnabled = enabled
    }

    fun forgotPasswordTapped(v: View) {
        val url = "https://escritorio.hinode.com.br/esqueci-senha"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    override fun closeScreen() {
        finish()
    }

    override fun setResultOK() {
        setResult(RESULT_OK)
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
    }

    override fun showLoading() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun showErrorMessageFailedToRegisterUser() {
        ViewUtils.showDialog(this, getString(R.string.kickoff_terms_error_failed_to_register_user), DialogInterface.OnDismissListener { finish() })
    }
}
