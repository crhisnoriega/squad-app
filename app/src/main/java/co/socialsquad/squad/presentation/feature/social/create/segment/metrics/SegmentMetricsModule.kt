package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import dagger.Binds
import dagger.Module

@Module
abstract class SegmentMetricsModule {
    @Binds
    abstract fun view(segmentMetricsActivity: SegmentMetricsActivity): SegmentMetricsView
}
