package co.socialsquad.squad.presentation.feature.points.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_points_system.view.*

class ItemSystemPointsViewHolder(itemView: View, var onClickAction: ((item: ItemSystemPointsViewModel) -> Unit)) : ViewHolder<ItemSystemPointsViewModel>(itemView) {
    override fun bind(viewModel: ItemSystemPointsViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.systemData.title
        itemView.txtSubtitle.text = viewModel.systemData.description

        Glide.with(itemView.context).load(viewModel.systemData.icon).into(itemView.imgIcon)
    }

    override fun recycle() {

    }

}