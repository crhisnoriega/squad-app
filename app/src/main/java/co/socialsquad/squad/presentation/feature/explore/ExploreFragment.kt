package co.socialsquad.squad.presentation.feature.explore

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.searchv2.SearchActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.getScreenWidth
import com.google.android.material.tabs.TabLayout
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_explore.tlTabs
import kotlinx.android.synthetic.main.fragment_explore.vpContent
import kotlinx.android.synthetic.main.view_search_toolbar.tToolbar
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import javax.inject.Inject

class ExploreFragment : Fragment(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var loginRepository: LoginRepository

    private fun itemsPermitted(): MutableList<ExploreItem> {
        return mutableListOf<ExploreItem>().apply {
            val company = loginRepository.userCompany?.company

            if (company?.exploreProducts == true) add(ExploreItem.Products)
            if (company?.exploreImmobile == true) add(ExploreItem.Immobiles)
            if (company?.exploreEvents == true) add(ExploreItem.Events)
            if (company?.exploreLives == true) add(ExploreItem.Lives)
            if (company?.exploreAudios == true) add(ExploreItem.Audios)
            if (company?.exploreVideos == true) add(ExploreItem.Videos)
            if (company?.exploreStores == true) add(ExploreItem.Stores)
            if (company?.exploreDocuments == true) add(ExploreItem.Documents)
            if (company?.exploreTrips == true) add(ExploreItem.Trips)
        }
    }

    private val fragments by lazy {
        itemsPermitted().map { it.getFragment() }.toTypedArray()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? =
        fragmentDispatchingAndroidInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_explore, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()
        setupTabs()
    }

    private fun setupToolbar() {
        etSearch.apply {
            isFocusable = false
            isFocusableInTouchMode = false
            isClickable = false
            isLongClickable = false
            isEnabled = false
        }
        tToolbar.setOnClickListener { openSearch() }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        vpContent?.let {
            itemsPermitted().getOrNull(it.currentItem)?.getFragment()?.userVisibleHint =
                isVisibleToUser
        }
        view?.visibility = if (!isVisibleToUser) View.INVISIBLE else View.VISIBLE
    }

    private fun setupTabs() {
        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.darkjunglegreen_28
                                )
                            )
                            TextUtils.applyTypeface(
                                context,
                                "fonts/gotham_bold.ttf",
                                text.toString(),
                                text.toString()
                            )
                        } else {
                            setTextColor(ContextCompat.getColor(context, R.color.oldlavender))
                            TextUtils.applyTypeface(
                                context,
                                "fonts/gotham_medium.ttf",
                                text.toString(),
                                text.toString()
                            )
                        }
                    }
                }
            }
        })

        loginRepository.userCompany?.company?.primaryColor?.let {
            tlTabs.setSelectedTabIndicatorColor(
                ColorUtils.parse(it)
            )
        }

        vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                tlTabs.getTabAt(position)?.select()
            }
        })

        itemsPermitted().forEach {
            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_explore_tab, tlTabs, false).apply {
                    findViewById<ImageView>(R.id.ivIcon)?.setImageResource(it.imageResId)
                    findViewById<TextView>(R.id.tvTitle)?.text = getString(it.titleResId)
                }
                view.measure(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                tlTabs.layoutParams.height = view.measuredHeight
                customView = view
            }
            tlTabs.addTab(tab)
        }

        tlTabs.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if (tlTabs.measuredWidth < activity?.getScreenWidth() ?: 0) {
            tlTabs.setPadding(0, 0, 0, 0)
        }

        vpContent.adapter = ExplorePagerAdapter(childFragmentManager, fragments)
        vpContent.offscreenPageLimit = tlTabs.tabCount
    }

    private fun openSearch() {
        val intent = Intent(context, SearchActivity::class.java)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity as Activity,
            androidx.core.util.Pair(tlTabs as View, "tabs"),
            androidx.core.util.Pair(etSearch as View, "input")
        )
        startActivity(intent, options.toBundle())
    }
}
