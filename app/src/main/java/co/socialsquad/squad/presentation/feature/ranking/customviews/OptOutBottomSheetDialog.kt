package co.socialsquad.squad.presentation.feature.ranking.customviews


import android.app.Dialog
import android.graphics.Outline
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.ranking.model.RankingItem
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_resource_opt_out.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class OptOutBottomSheetDialog(private val rankingItem: RankingItem, private val onSelect: (type: String) -> Unit) : BottomSheetDialogFragment() {

    var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null

    override fun getTheme(): Int = R.style.FarmBlocks_BottomSheetDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_resource_opt_out, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        }
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTopBordersRadius()
        super.onViewCreated(view, savedInstanceState)
        expandDialog()


        rankingName.text = rankingItem?.title

        rankPosition.text = rankingItem?.header?.score

//        if (rankingData.user_score.isNullOrBlank().not()) {
//            rankPosition.text = rankingData.user_position + " • " + rankingData.user_score
//        } else {
//            rankPosition.text = rankingData.user_position
//        }

        shareLayout.setOnClickListener {
            onSelect.invoke("share")
            dismiss()
        }

        shareLayout2.setOnClickListener {
            onSelect.invoke("share")
            dismiss()
        }

        optOutLayout.setOnClickListener {
            onSelect.invoke("optout")
            dismiss()
        }

        optOutLayout2.setOnClickListener {
            onSelect.invoke("optout")
            dismiss()
        }

        Glide.with(requireContext()).load(rankingItem?.icon).into(rankingImg)
    }


    override fun onPause() {
        isShowing = false
        super.onPause()
    }

    override fun onResume() {
        isShowing = true
        super.onResume()
    }


    override fun dismiss() {
        isShowing = false
        super.dismiss()
    }

    override fun onDestroy() {
        isShowing = false
        super.onDestroy()
    }

    private fun setTopBordersRadius() {
        val radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)
        val container = view as ViewGroup

        container.apply {
            clipToOutline = true
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    if (view != null) outline?.setRoundRect(0, 0, view.width, (view.height + 300).toInt(), radius)
                }
            }
        }
    }

    fun expandDialog() {
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(300)
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            super.show(manager, tag)
        }
    }

    companion object {
        var isShowing = false
    }

}