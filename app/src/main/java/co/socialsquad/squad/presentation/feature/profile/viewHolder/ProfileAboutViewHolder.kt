package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_profile_about_item.view.*

const val PROFILE_ABOUT_VIEW_HOLDER_ID = R.layout.view_profile_about_item

class ProfileAboutViewHolder(itemView: View, private val onClickListener: Listener<ProfileAboutViewModel>) : ViewHolder<ProfileAboutViewModel>(itemView) {
    override fun bind(viewModel: ProfileAboutViewModel) {
        with(viewModel) {
            itemView.tvTitle.text = type
            itemView.short_title_lead.text = title
            if (isClickable) {
                itemView.ivArrow.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    onClickListener.onClick(this)
                }
            } else {
                itemView.ivArrow.visibility = View.INVISIBLE
                itemView.setOnClickListener {}
            }
            Glide.with(itemView.context)
                .load(image)
                .circleCrop()
                .into(itemView.ivAboutSymbol)
        }
    }

    override fun recycle() {
    }
}
