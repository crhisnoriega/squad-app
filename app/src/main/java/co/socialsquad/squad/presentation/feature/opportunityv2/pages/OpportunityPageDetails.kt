package co.socialsquad.squad.presentation.feature.opportunityv2.pages

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetailsModel
import co.socialsquad.squad.presentation.util.argument
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_opportunity_page_details.*
import kotlinx.android.synthetic.main.item_card_opportunity.*
import kotlinx.android.synthetic.main.item_card_opportunity_lead.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"

class OpportunityPageDetails() : Fragment() {
    private val object2: OpportunityDetailsModel by argument(ARG_PARAM1)
    private val objectType: String by argument(ARG_PARAM2)
    private val initials: String by argument("initials")
    private val showLead: Boolean by argument(ARG_PARAM3)

    private val fromScreen: String by argument("from")


    var callback: ((leadId: String) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_opportunity_page_details, container, false)

    override fun onResume() {
        super.onResume()
        rootLayout2.requestLayout()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        object_type.text = objectType
        short_title.text = object2?.data?.details?.object1?.short_title
        short_description.text = object2?.data?.details?.object1?.short_description
        custom_fields.text = object2?.data?.details?.object1?.custom_fields

        object2?.data?.details?.object1?.section?.let {
            Glide.with(view.context).load(it.icon?.url).into(imgSection2)
        }

        containerLayout.setOnClickListener {
            activity?.finish()
        }

        object_type_lead.text = "Lead"

        if (object2?.data?.details?.lead != null &&
                object2?.data?.details?.lead?.isEmpty()?.not()!!) {

            short_title_lead.text = object2?.data?.details?.lead?.get(0)?.name!!
            short_description_lead.text = object2?.data?.details?.lead?.get(0)?.phone!!
            custom_fields_lead.text = object2?.data?.details?.lead?.get(0)?.email!!

            if (containerLayoutLead != null) {
                containerLayoutLead.setOnClickListener {
                    callback?.invoke(object2?.data?.details?.lead?.get(0)?.id!!)
                }
            }

            infoLead123.setOnClickListener {
                // callback?.invoke(object2?.data?.details?.lead?.get(0)?.id!!)
            }

            arrow_indicator_lead.isVisible = false
        }

        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(view.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)


        rl_contact_circle_lead.backgroundTintList = ColorStateList.valueOf(Color.parseColor(userCompany.company?.primaryColor!!))
        tv_initials_lead.text = initials

        lead2.isVisible = showLead

        when (fromScreen) {
            "object" -> {
                context_complete_check.visibility = View.GONE
            }
        }
    }


    companion object {
        @JvmStatic
        fun newInstance(showLead: Boolean, objectType: String, details: OpportunityDetailsModel, initials: String, fromScreen: String? = "") =
                OpportunityPageDetails().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM1, details)
                        putString(ARG_PARAM2, objectType)
                        putBoolean(ARG_PARAM3, showLead)
                        putString("initials", initials)
                        putString("from", fromScreen)
                    }
                }
    }
}