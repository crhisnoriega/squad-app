package co.socialsquad.squad.presentation.feature.live.creator

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import co.socialsquad.squad.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.linearlayout_live_creator_confirmation_dialog.*

class LiveCreatorConfirmationDialog(context: Context, private val listener: Listener) :
    BottomSheetDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.linearlayout_live_creator_confirmation_dialog)

        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        live_creator_confirmation_dialog_bsb_no.setOnClickListener { dismiss() }
        live_creator_confirmation_dialog_bsb_yes.setOnClickListener {
            dismiss()
            listener.onYesClicked()
        }
    }

    interface Listener {
        fun onYesClicked()
    }
}
