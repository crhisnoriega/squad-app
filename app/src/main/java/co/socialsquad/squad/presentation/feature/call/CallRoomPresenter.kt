package co.socialsquad.squad.presentation.feature.call

import android.content.Context
import android.os.Bundle
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.repository.CallRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_CALL_ROOM
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CallRoomPresenter @Inject constructor(
    private val callRoomView: CallRoomView,
    private val callRepository: CallRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {

    private var callManager: CallingManager? = null
    private var compositeDisposable = CompositeDisposable()
    private var callViewInterface = object : CallingManager.CallViewInterface {
        override fun renderCallActionsState(microphoneOn: Boolean, loudSpeakerOn: Boolean, cameraOn: Boolean, showLocalView: Boolean, showRemoteView: Boolean) {
            callRoomView.setMicrophoneButtonStatus(microphoneOn)
            callRoomView.setSpeakerButtonStatus(loudSpeakerOn)
            if (cameraOn) {
                callRoomView.changeVideoIconToBlock()
            } else {
                callRoomView.changeVideoIconToAllow()
            }
            if (showRemoteView) {
                callRoomView.changeLayoutToVideo()
            }
            if (showLocalView) {
                callRoomView.changeLayoutToVideo()
                callRoomView.showVideoLocal()
                callRoomView.hideUserInfos()
            } else {
                callRoomView.hideVideoLocal()
                if (cameraOn) {
                    callRoomView.showSmallUserInfo()
                }
            }

            if (showRemoteView && !cameraOn) {
                callRoomView.hideUserInfos()
            }

            if (!showLocalView && !showRemoteView) {
                callRoomView.showNormalUserInfo()
                callRoomView.changeLayoutToAudio()
            }
        }

        override fun fillContactInformation(profile: Profile) {
            callRoomView.setCaller(profile)
        }
    }

    private fun onStatusChanged(newStatus: VideoCallStatus) {
        when (newStatus) {
            VideoCallStatus.FAILED -> callRoomView.failedCall()
            VideoCallStatus.FINISHED -> callRoomView.endCall()
            VideoCallStatus.READY -> callRoomView.callReady()
            VideoCallStatus.UNKNOWN -> {
            }
            VideoCallStatus.CONNECTED -> showCallTimer()
            else -> callRoomView.showStatusMessage(newStatus.label)
        }
    }

    private fun showCallTimer() {
        callRoomView.callConnected()
    }

    fun onCreate(context: Context, videoRenderers: CallingManager.ViewRenders, externalCallerId: String, externalCallerPk: Int, action: String, callType: String?, callUuid: String?) {
        loginRepository.userCompany?.company?.callGradient?.let {
            callRoomView.setScreenBackground(it)
        }
        loginRepository.userCompany?.company?.callLogo?.let {
            callRoomView.setCompanyImage(it)
        }
        if (callType == CALL_TYPE_VIDEO) {
            callRoomView.showSmallUserInfo()
        } else {
            callRoomView.showNormalUserInfo()
        }
        val callMode = when (action) {
            CALL_ROOM_ACTION_CALL -> {
                callRoomView.callAnswered()
                CallingManager.CallMode.OFFER
            }
            else -> {
                CallingManager.CallMode.ANSWER
            }
        }
        loginRepository.userCompany?.user?.chatId?.let {
            callManager = CallingManager(
                context, it, externalCallerPk, callUuid,
                callRepository.connectQueue(), callRepository.connectToEmitterQueue(externalCallerId),
                videoRenderers, this::onStatusChanged, callMode, callViewInterface
            ).apply {
                connect(callType)
            }
        }

        tagWorker.tagEvent(TAG_SCREEN_VIEW_CALL_ROOM, Bundle().apply { putString("ACTION", action) })
    }

    fun onStartVideoSession() {
        callManager?.let {
            callRoomView.setupLocalRenderer(it)
            callRoomView.setupRemoteRenderer(it)
        }
    }

    fun onDestroy() {
        callRepository.clearCurrentCall()
        callManager?.terminate()
        callManager = null
    }

    fun answerCall() {
        callManager?.answerCall()
        callRoomView.callAnswered()
    }

    fun stopCall() {
        callRoomView.endCall()
        compositeDisposable.clear()
    }

    fun toggleVideo() {
        callManager?.toggleCamera()
    }

    fun mute() {
        callManager?.toggleMic()
    }

    fun toggleSpeaker() {
        callManager?.toggleSpeaker()
    }

    fun changeCamera() {
        callManager?.changeCaptureCamera()
    }
}
