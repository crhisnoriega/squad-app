package co.socialsquad.squad.presentation.util

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Camera

class CameraUtils(val context: Context) {
    fun isCameraAvailable(): Boolean {
        val hasCamera = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
        if (!hasCamera) return false

        for (i in 0 until Camera.getNumberOfCameras()) {
            try {
                Camera.open(i)?.release() ?: return false
            } catch (t: Throwable) {
                return false
            }
        }

        return true
    }
}
