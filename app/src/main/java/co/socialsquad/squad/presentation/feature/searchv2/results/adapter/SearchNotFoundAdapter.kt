package co.socialsquad.squad.presentation.feature.searchv2.results.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder.SearchNotFoundViewHolder

class SearchNotFoundAdapter(
        var query: String
) :
        RecyclerView.Adapter<SearchNotFoundViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchNotFoundViewHolder {
        return SearchNotFoundViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.fragment_searchv2_not_found_state, parent, false),
                parent.context
        )
    }


    override fun getItemCount() = 1
    override fun onBindViewHolder(holder: SearchNotFoundViewHolder, position: Int) {
        holder.bind(query)
    }

    internal fun updateSearchTerm(searchTerm: String) {
        this.query = searchTerm
        notifyDataSetChanged()
    }
}
