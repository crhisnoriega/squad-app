package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichText
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Submissions(
        @SerializedName("success") val success: Boolean
) : Parcelable
