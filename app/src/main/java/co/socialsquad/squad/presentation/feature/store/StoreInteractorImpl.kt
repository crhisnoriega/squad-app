package co.socialsquad.squad.presentation.feature.store

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FindProductRequest
import co.socialsquad.squad.data.entity.ProductsFilterRequest
import co.socialsquad.squad.data.entity.StoreHome
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.StoreRepository
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import io.reactivex.Observable
import javax.inject.Inject

class StoreInteractorImpl @Inject constructor(
    private val storeRepository: StoreRepository,
    private val loginRepository: LoginRepository
) : StoreInteractor {
    override fun getColor() = loginRepository.squadColor

    override fun getStore(): Observable<List<ViewModel>> =
        storeRepository.getStoreHome().map { store ->
            val viewModels = arrayListOf<ViewModel>()

            store.banners?.map {
                val product = it.product?.let { StoreProductViewModel(it) }
                val category = it.category?.let { mapCategory(it) }
                val list = it.list?.let { ProductListViewModel(it.pk, it.title) }
                StoreBannerViewModel(product, category, list, it.media.url)
            }?.let {
                if (it.isNotEmpty()) {
                    viewModels.add(StoreBannerListViewModel(it))
                }
            }

            store.categories?.let {
                viewModels.add(HeaderViewModel(titleResId = R.string.store_header_categories_title))
                val categories = it.map {
                    with(it) { StoreCategoryListItemViewModel(pk, name, icon?.url, cover?.url, hasSubcategories) }
                }
                viewModels.add(StoreCategoryListViewModel(categories))
            }

            store.lists?.filter { it.arrangement == "v" }?.map {
                viewModels.add(HeaderViewModel(it.title, it.caption))

                it.media?.let { viewModels.add(StoreProductListBannerViewModel(it.url)) }

                val products = it.products.map { StoreProductViewModel(it) }
                viewModels.add(StoreProductListViewModel(products))

                viewModels.add(StoreFooterViewModel(ProductListViewModel(it.pk, it.title)))
            }

            store.lists?.filterNot { it.arrangement == "v" }?.map {
                viewModels.add(HeaderViewModel(it.title, it.caption))

                it.media?.let { viewModels.add(StoreProductListBannerViewModel(it.url)) }

                it.products.map { viewModels.add(StoreProductViewModel(it)) }

                viewModels.add(StoreFooterViewModel(ProductListViewModel(it.pk, it.title)))
            }

            return@map viewModels
        }

    override fun getSubcategories(pk: Int, page: Int): Observable<List<ViewModel>> =
        storeRepository.getSubcategories(pk, page).map { return@map it.results?.let { mapSubCategories(it) } }

    override fun getProductsByList(pk: Int, page: Int): Observable<StoreProductList> {
        val productsFilterRequest = ProductsFilterRequest(list = pk)
        return storeRepository.getProducts(productsFilterRequest, page).map { StoreProductList(it.next == null, it.results?.map { StoreProductViewModel(it) }.orEmpty()) }
    }

    override fun getProductsByCategory(pk: Int, page: Int): Observable<StoreProductList> {
        val productsFilterRequest = ProductsFilterRequest(category = pk)
        return storeRepository.getProducts(productsFilterRequest, page).map { StoreProductList(it.next == null, it.results?.map { StoreProductViewModel(it) }.orEmpty()) }
    }

    override fun getProductsByQuery(query: String, page: Int): Observable<StoreProductList> {
        val productsFilterRequest = ProductsFilterRequest(name = query)
        return storeRepository.getProducts(productsFilterRequest, page).map { StoreProductList(it.next == null, it.results?.map { StoreProductViewModel(it) }.orEmpty()) }
    }

    override fun getProduct(pk: Int) = storeRepository.getProduct(pk).map { StoreProductViewModel(it) }

    override fun getProductDetailGroups(pk: Int): Observable<List<ProductDetailGroupViewModel>> =
        storeRepository.getProductDetailCategories(pk).map {
            return@map it.results?.map {
                val details = it.details.map { ProductDetailViewModel(it) }
                val media = it.media?.let { Media(it) }?.takeIf { !it.url.isBlank() }
                ProductDetailGroupViewModel(it.name, details, it.coverUrl, media)
            }
        }

    override fun getProductLocations(pid: Int, latitude: Double, longitude: Double): Observable<List<StoreViewModel>> {
        val findProductRequest = FindProductRequest(pid, latitude, longitude)
        return storeRepository.getProductLocations(findProductRequest).map {
            return@map it.results?.map { StoreViewModel(it, latitude, longitude) }?.sortedBy { it.distance }
        }
    }

    override fun sendProductView(pk: Int) = storeRepository.getProductView(pk)

    private fun mapCategory(category: StoreHome.Categories.Category) =
        with(category) { StoreCategoryListItemViewModel(pk, name, icon?.url, cover?.url, hasSubcategories) }

    private fun mapSubCategories(categories: List<StoreHome.Categories.Category>): List<ViewModel> {
        val list = mutableListOf<ViewModel>()
        categories.forEach {
            with(it) {
                if (hasSubcategories) {
                    list.add(HeaderViewModel(name))
                    list.addAll(subCategories?.map { category -> mapCategory(category) }.orEmpty())
                    list.add(DividerViewModel)
                } else {
                    list.addAll(categories.map { category -> mapCategory(category) }.orEmpty())
                    list.add(DividerViewModel)
                }
            }
        }
        return list
    }
}
