package co.socialsquad.squad.presentation.feature.chat.chatroom

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_OPEN_ROOM
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_SEND_MESSAGE
import co.socialsquad.squad.data.service.CHAT_SERVICE_MESSAGE_EXTRA
import co.socialsquad.squad.data.service.CHAT_SERVICE_ROOM_SERIALIZABLE_EXTRA
import co.socialsquad.squad.data.service.CHAT_SERVICE_ROOM_URL_EXTRA
import co.socialsquad.squad.data.service.ChatService
import co.socialsquad.squad.presentation.custom.LiveDataViewModel
import co.socialsquad.squad.presentation.custom.PagedRecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.PagedViewHolder
import co.socialsquad.squad.presentation.custom.PagedViewHolderFactory
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_ACTION_CALL
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_ACTION
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_QUEUE_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_TYPE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_USER
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_AUDIO
import co.socialsquad.squad.presentation.feature.call.CALL_TYPE_VIDEO
import co.socialsquad.squad.presentation.feature.call.CallRoomActivity
import co.socialsquad.squad.presentation.feature.chat.CHAT_MY_MESSAGE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.chat.CHAT_OTHER_PERSON_MESSAGE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.chat.MessageViewModel
import co.socialsquad.squad.presentation.feature.chat.MyMessageViewHolder
import co.socialsquad.squad.presentation.feature.chat.MyMessageViewModel
import co.socialsquad.squad.presentation.feature.chat.OtherPersonMessageViewHolder
import co.socialsquad.squad.presentation.feature.chat.OtherPersonMessageViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.viewholder.PagedLoadingViewHolder
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_chat_room.*
import kotlinx.android.synthetic.main.toolbar_chat_room.*
import kotlinx.android.synthetic.main.view_comment_box.*
import javax.inject.Inject

class ChatRoomActivity : BaseDaggerActivity(), ChatRoomView {

    @Inject
    lateinit var presenter: ChatRoomPresenter
    private var chatRoomAdapter: PagedRecyclerViewAdapter<MessageViewModel>? = null
    private val linearLayoutManager = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_room)
        setupChat()
        setupTextBox()
        presenter.onCreate(intent, this)
    }

    override fun onBackPressed() {
        presenter.onBackButtonClicked()
    }

    override fun setupToolbar(roomModel: ChatRoomViewModel) {
        setSupportActionBar(tToolbar)
        ivBackButton.setOnClickListener {
            presenter.onBackButtonClicked()
        }
        if (!roomModel.canCall) {
            ivVideoCall.visibility = View.GONE
            ivVoiceCall.visibility = View.GONE
        }
        ivVideoCall.setOnClickListener {
            openCall(
                roomModel.chatId, CALL_TYPE_VIDEO,
                Profile(roomModel.pk, roomModel.name, "", roomModel.avatarUrl, roomModel.qualification, roomModel.qualificationBadge, roomModel.chatId)
            )
        }
        ivVoiceCall.setOnClickListener {
            openCall(
                roomModel.chatId, CALL_TYPE_AUDIO,
                Profile(roomModel.pk, roomModel.name, "", roomModel.avatarUrl, roomModel.qualification, roomModel.qualificationBadge, roomModel.chatId)
            )
        }
        tvRoomTitle.text = roomModel.name
        val subtitle =
            if (roomModel.isOnline) getString(R.string.chat_room_online)
            else roomModel.lastSeen?.elapsedTime(this)
        if (subtitle.isNullOrEmpty()) {
            tvRoomSubtitle.visibility = View.GONE
        } else {
            tvRoomSubtitle.visibility = View.VISIBLE
            tvRoomSubtitle.text = subtitle
        }
        Glide.with(this)
            .load(roomModel.avatarUrl)
            .circleCrop()
            .crossFade()
            .into(ivRoomImage)
    }

    private fun openCall(callId: String?, mode: String, profile: Profile) {
        if (callId == null) {
            return
        }
        val intent = Intent(this, CallRoomActivity::class.java).apply {
            putExtra(CALL_ROOM_EXTRA_QUEUE_ID, callId)
            putExtra(CALL_ROOM_EXTRA_ACTION, CALL_ROOM_ACTION_CALL)
            putExtra(CALL_ROOM_EXTRA_TYPE, mode)
            putExtra(CALL_ROOM_EXTRA_USER, profile)
        }
        startActivity(intent)
    }

    private fun setupChat() {
        chatRoomAdapter = PagedRecyclerViewAdapter(object : PagedViewHolderFactory {
            override fun getType(viewModel: LiveDataViewModel) = when (viewModel) {
                is MyMessageViewModel -> CHAT_MY_MESSAGE_VIEW_MODEL_ID
                is OtherPersonMessageViewModel -> CHAT_OTHER_PERSON_MESSAGE_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): PagedViewHolder<*> {
                return when (viewType) {
                    CHAT_MY_MESSAGE_VIEW_MODEL_ID -> MyMessageViewHolder(view)
                    CHAT_OTHER_PERSON_MESSAGE_VIEW_MODEL_ID -> OtherPersonMessageViewHolder(view)
                    LOADING_VIEW_MODEL_ID -> PagedLoadingViewHolder<Nothing>(view)
                    else -> throw IllegalArgumentException()
                }
            }
        })
        chatRoomAdapter?.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                rvChatRoom.scrollToPosition(0)
            }
        })

        linearLayoutManager.reverseLayout = true

        rvChatRoom?.apply {
            adapter = chatRoomAdapter
            layoutManager = linearLayoutManager
        }
    }

    private fun setupTextBox() {
        ivCommenterAvatar.visibility = View.GONE
        ibSendComment.setOnClickListener {
            sendMessage()
        }
    }

    override fun setItems(messages: PagedList<MessageViewModel>) {
        chatRoomAdapter?.submitList(messages)
    }

    private fun sendMessage() {
        if (etCommentText.text.isNotBlank()) {
            presenter.onSendMessage(etCommentText.text.toString().trim())
        }
    }

    override fun sendMessageToService(channelUrl: String?, message: String) {
        val intent = Intent(this, ChatService::class.java).apply {
            action = CHAT_SERVICE_ACTION_SEND_MESSAGE
            putExtra(CHAT_SERVICE_ROOM_URL_EXTRA, channelUrl)
            putExtra(CHAT_SERVICE_MESSAGE_EXTRA, message)
        }
        startService(intent)
    }

    override fun clearTextBox() {
        etCommentText.setText("")
    }

    override fun showLoading() {
        pbLoading.visibility = View.GONE
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
    }

    override fun finishChatRoom(resultRoom: ChatRoomModel?) {
        finish()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun updateRoomService(chatRoomModel: ChatRoomModel) {
        val intent = Intent(this, ChatService::class.java).apply {
            action = CHAT_SERVICE_ACTION_OPEN_ROOM
            putExtra(CHAT_SERVICE_ROOM_SERIALIZABLE_EXTRA, chatRoomModel)
        }
        startService(intent)
    }
}
