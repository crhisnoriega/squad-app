package co.socialsquad.squad.presentation.feature.explorev2.components.banner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide

class SessionBannerImageAdapter(
    val context: Context,
    val items: List<ContentItem>,
    val onBannerSelected: (ContentItem) -> Unit
) :
    RecyclerView.Adapter<SessionBannerImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(co.socialsquad.squad.R.layout.view_session_banner_item, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contentItem = items[position]
        contentItem.media?.let { Glide.with(context).load(it.url).crossFade().into(holder.image) }

        if (contentItem.link != null) {
            holder.image.setOnClickListener { onBannerSelected(contentItem) }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(co.socialsquad.squad.R.id.image)
    }
}
