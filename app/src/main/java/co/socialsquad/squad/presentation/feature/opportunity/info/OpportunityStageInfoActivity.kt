package co.socialsquad.squad.presentation.feature.opportunity.info

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Information
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.util.extra
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_opportunity_info.ibBack
import kotlinx.android.synthetic.main.activity_opportunity_info.mediaView
import kotlinx.android.synthetic.main.activity_opportunity_info.tvDescription
import kotlinx.android.synthetic.main.activity_opportunity_info.tvTitle
import kotlinx.android.synthetic.main.activity_opportunity_info.tvToolbarTitle
import kotlinx.android.synthetic.main.fragment_recommendation_progress.rvStages

class OpportunityStageInfoActivity : AppCompatActivity() {

    private val opportunityInformation: Information by extra(EXTRA_INFO)
    private val colors: OpportunityInitialsColors by extra(EXTRA_COLORS)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opportunity_info)
        setupHeader()
        setupStages()
        opportunityInformation.header.media?.let { mediaView.setMedia(it, this) }
    }

    private fun setupHeader() {
        ibBack.setOnClickListener { finish() }
        tvToolbarTitle.text = opportunityInformation.header.title
        tvDescription.text = opportunityInformation.header.description
        tvTitle.text = opportunityInformation.details.title
    }

    private fun setupStages() {

        val infoAdapter = OpportunityInfoAdapter(
            this,
            opportunityInformation.details.stages.sortedBy { it.order },
            opportunityInformation.details.type,
            colors.primaryColor,
            false
        ) {}

        rvStages.apply {
            adapter = infoAdapter
            layoutManager = LinearLayoutManager(this@OpportunityStageInfoActivity)
            isNestedScrollingEnabled = false
        }
    }

    companion object {
        const val EXTRA_INFO = "info"
        const val EXTRA_COLORS = "submission_colors"
        fun newIntent(
            context: Context,
            info: Information,
            colors: OpportunityInitialsColors
        ): Intent {
            return Intent(context, OpportunityStageInfoActivity::class.java).apply {
                putExtra(EXTRA_INFO, info)
                putExtra(EXTRA_COLORS, colors)
            }
        }
    }
}
