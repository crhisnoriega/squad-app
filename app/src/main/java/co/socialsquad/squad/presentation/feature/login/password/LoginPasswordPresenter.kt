package co.socialsquad.squad.presentation.feature.login.password

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.RecipientsRequest
import co.socialsquad.squad.data.entity.SigninRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.feature.login.subdomain.LoginSubdomainActivity.Companion.EXTRA_LOGIN_SUBDOMAIN
import co.socialsquad.squad.presentation.util.Analytics
import com.google.gson.Gson
import io.branch.referral.Branch
import io.reactivex.disposables.CompositeDisposable
import java.net.UnknownHostException
import javax.inject.Inject

class LoginPasswordPresenter @Inject constructor(
    private val view: LoginPasswordView,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker,
    private val branch: Branch,
    private val analytics: Analytics
) {
    private var company: Company? = null
    private val compositeDisposable = CompositeDisposable()
    private var subdomain: String = ""

    private var email: String = ""
    private var password: String = ""

    fun onCreate(intent: Intent) {
        analytics.sendEvent(Analytics.SIGN_EMAIL_PASSWORD_SCREEN_VIEW)

        subdomain = intent.getStringExtra(EXTRA_LOGIN_SUBDOMAIN)
        email = intent.getStringExtra(LoginPasswordActivity.EXTRA_LOGIN_EMAIL)
        view.setupHelpText(subdomain)

        if (intent.hasExtra(LoginPasswordActivity.extraCompany)) {
            val gson = Gson()
            company = gson.fromJson(intent.extras!!.getString(LoginPasswordActivity.extraCompany), Company::class.java)
            view.setupHeader(company)
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onNextClicked() {
        analytics.sendEvent(Analytics.SIGN_EMAIL_PASSWORD_TAP_LAUNCH)

        loginRepository.subdomain = subdomain
        tagWorker.tagLoginEvent()

        val request = SigninRequest(email.toLowerCase(), password)
        compositeDisposable.add(
            loginRepository.signin(request)
                .flatMap<String?> {
                    loginRepository.squadColor = it.company.primaryColor
                    loginRepository.userCompany = it
                    loginRepository.token = it.token
                    loginRepository.getDeviceToken()
                }
                .flatMapCompletable {
                    loginRepository.registerDevice(RecipientsRequest(it, RecipientsRequest.PLATFORM))
                }
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        view.hideLoading()

                        if (loginRepository.userCompany != null && loginRepository.userCompany!!.user != null && !loginRepository.userCompany!!.user.isPerfilComplete) {
                            view.openProfileScreen()
                            return@subscribe
                        }

                        view.openNavigation()

                        loginRepository.userCompany?.user?.email?.let { branch.setIdentity(it) }
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.login_password_error)
                        loginRepository.token = null

                        if (it is UnknownHostException) {
                            analytics.sendEvent(Analytics.SIGN_EMAIL_PASSWORD_ACTION_NO_CONNECTION)
                        } else {
                            analytics.sendEvent(Analytics.SIGN_EMAIL_PASSWORD_ACTION_INVALID_PASSWORD)
                        }
                    }
                )
        )
    }

    fun onAlternativeClicked() {
        analytics.sendEvent(Analytics.SIGN_EMAIL_PASSWORD_TAP_RESET_PASS)
        view.openLoginAlternative(subdomain, email, company)
    }

    fun onInputChanged(password: String) {
        this.password = password
        val isValid = password.isNotBlank() && password.length >= 6
        view.enableNextButton(isValid)
    }
}
