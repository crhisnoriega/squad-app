package co.socialsquad.squad.presentation.feature.profile.qualification

import android.os.Bundle
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.repository.VideoRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.PrizeViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationHeaderViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.RecommendationViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.RequirementViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.SuccessStoriesViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProfileQualificationPresenter @Inject constructor(
    private val profileQualificationView: ProfileQualificationView,
    private val profileRepository: ProfileRepository,
    private val videoRepository: VideoRepository,
    loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {

    private var compositeDisposable = CompositeDisposable()
    private val color = loginRepository.squadColor ?: "#ededed"
    private val subdomain = loginRepository.subdomain

    fun onCreate(qualificationPk: Int?) {
        tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_PROFILE_QUALIFICATION)
        var pk: Int? = null
        qualificationPk?.let { pk = it }
        getQualification(pk)
    }

    private fun getQualification(pk: Int?) {
        if (pk != null) {
            compositeDisposable.add(
                profileRepository.getQualification(pk)
                    .doOnSubscribe { profileQualificationView.startLoading() }
                    .map { mapToViewModel(it) }
                    .subscribe(
                        {
                            profileQualificationView.setQualifications(it)
                        },
                        {
                            it.printStackTrace()
                        }
                    )
            )
        } else {
            compositeDisposable.add(
                profileRepository.getQualification()
                    .doOnSubscribe { profileQualificationView.startLoading() }
                    .map { mapToViewModel(it) }
                    .subscribe(
                        {
                            profileQualificationView.setQualifications(it)
                        },
                        {
                            it.printStackTrace()
                        }
                    )
            )
        }
    }

    private fun mapToViewModel(qualifications: List<Qualification>): ProfileQualificationHeaderViewModel {
        val viewModelList = mutableListOf<ProfileQualificationViewModel>()
        viewModelList.addAll(
            qualifications.map { qualification ->
                ProfileQualificationViewModel(
                    qualification.name,
                    qualification.icon,
                    (qualification.progress * 100).toInt(),
                    qualification.actual,
                    qualification.target,
                    color,
                    qualification.requirements.map {
                        RequirementViewModel(it, color)
                    },
                    qualification.suggestions.map {
                        RecommendationViewModel(
                            it.pk, it.title ?: "",
                            it.content ?: "", it.type, it.icon ?: "", it.total,
                            it.progress, (it.progress.toFloat() * 100 / it.total).toInt()
                        )
                    },
                    qualification.prizes.map {
                        PrizeViewModel(
                            it.category, it.name, it.description,
                            it.media?.url
                                ?: "",
                            color
                        )
                    },
                    qualification.successStories.map { successStories ->
                        SuccessStoriesViewModel(successStories, color, subdomain)
                    },
                    qualification.bonus
                )
            }
        )
        return ProfileQualificationHeaderViewModel(viewModelList)
    }

    fun updateDetails(qualification: String) {
        tagWorker.tagEvent(TagWorker.TAG_TAP_QUALIFICATION_DETAIL, Bundle().apply { putString("qualification", qualification) })
    }

    fun onVideoClicked(mediaPk: Long?) {
        mediaPk?.let {
            tagWorker.tagEvent(TagWorker.TAG_TAP_PLAY_VIDEO)
            compositeDisposable.add(
                videoRepository.markAsViewed(it)
                    .subscribe({}, { it.printStackTrace() })
            )
        }
    }
}
