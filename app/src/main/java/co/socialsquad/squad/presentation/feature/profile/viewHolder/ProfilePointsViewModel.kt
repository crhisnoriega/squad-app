package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ProfilePointsViewModel(
    val title: String,
    val subtitle: String,
    val icon: String,
    val point_system_id: String
) : ViewModel
