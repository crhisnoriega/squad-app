package co.socialsquad.squad.presentation.feature.store.viewholder

import android.content.Context
import android.net.Uri
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.PageIndicator
import co.socialsquad.squad.presentation.custom.VideoView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialShareViewHolder
import co.socialsquad.squad.presentation.feature.store.Media
import co.socialsquad.squad.presentation.feature.store.MediaType
import co.socialsquad.squad.presentation.feature.store.ProductOverviewViewModel
import co.socialsquad.squad.presentation.feature.video.VideoManager
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_product_overview.view.*

class ProductOverviewViewHolder(
    itemView: View,
    private val color: String? = null,
    private val onShareListener: Listener<ProductOverviewViewModel>? = null
) : ViewHolder<ProductOverviewViewModel>(itemView), LifecycleObserver {

    private var productOverviewAdapter: ProductOverviewAdapter? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        productOverviewAdapter?.resume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        productOverviewAdapter?.pause()
    }

    override fun bind(viewModel: ProductOverviewViewModel) {
        with(viewModel) {
            itemView.apply {
                medias?.let {
                    productOverviewAdapter = ProductOverviewAdapter(context, it)

                    with(vpMedia) {
                        adapter = productOverviewAdapter
                        productOverviewAdapter?.let { addOnPageChangeListener(it) }
                        offscreenPageLimit = it.size
                    }

                    if (it.size > 1) setupRadioGroup(it, color)
                }

                if (price != null) {
                    tvPrice.text = context.getString(R.string.products_item_price, price)
                } else {
                    tvPrice.visibility = View.GONE
                }

                if (points != null) {
                    tvPointsCode.text = arrayOf(context.getString(R.string.products_item_points, points), context.getString(R.string.product_overview_code, code))
                        .filterNot { it.isNullOrEmpty() }.joinToString(" • ")
                } else {
                    tvPointsCode.visibility = View.GONE
                }

                if (description != null) {
                    tvDescription.text = description
                } else {
                    tvDescription.visibility = View.GONE
                }
                SocialShareViewHolder(itemView, viewModel, onShareListener).bind(shareViewModel)
            }
        }
    }

    override fun recycle() {
        itemView.piIndicator.removeAllViews()
    }

    private fun setupRadioGroup(medias: List<Media>, color: String?) {
        with(itemView.piIndicator) {
            viewPager = itemView.vpMedia
            types = medias.map {
                when (it.type) {
                    is MediaType.Video -> PageIndicator.Type.Video()
                    else -> PageIndicator.Type.Image()
                }
            }
            color?.let { selectedColor = ColorUtils.parse(it) }
            init()
        }
    }
}

class ProductOverviewAdapter(
    private val context: Context,
    private val medias: List<Media> = listOf()
) : PagerAdapter(), ViewPager.OnPageChangeListener {
    private val videoViewSparseArray = SparseArray<VideoView>()
    private var lastPosition = 0
    private var currentPosition = 0

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val media = medias[position]
        return when (media.type) {
            is MediaType.Image -> setupImage(media, container)
            is MediaType.Video -> setupVideo(media, position, container)
        }
    }

    private fun setupImage(media: Media, container: ViewGroup): LinearLayout {
        val ll = LinearLayout(context)
        ll.setPadding(context.resources.getDimensionPixelSize(R.dimen.default_margin), 0, context.resources.getDimensionPixelSize(R.dimen.default_margin), 0)

        val iv = ImageView(context)
        iv.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        Glide.with(iv).load(media.url)
            .crossFade()
            .into(iv)

        ll.addView(iv)
        container.addView(ll)
        return ll
    }

    private fun setupVideo(media: Media, position: Int, container: ViewGroup): View {
        val ll = LinearLayout(context)
        ll.setPadding(context.resources.getDimensionPixelSize(R.dimen.default_margin), 0, context.resources.getDimensionPixelSize(R.dimen.default_margin), 0)

        val vv = VideoView(context, null)
        vv.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)

        (vv.getChildAt(0) as LinearLayout).getChildAt(0).setBackgroundResource(R.drawable.shape_product_overview_pager_background)
        (vv.getChildAt(0) as LinearLayout).getChildAt(0).clipToOutline = true

        videoViewSparseArray.put(position, vv)

        vv.loadThumbnail(media.thumbnail)
        vv.prepare(Uri.parse(media.url))

        ll.addView(vv)
        container.addView(ll)
        return ll
    }

    override fun getCount() = medias.size

    override fun isViewFromObject(view: View, `object`: Any) = view === `object`

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        videoViewSparseArray.get(position)?.video?.player?.release()
    }

    fun resume() {
        videoViewSparseArray.get(currentPosition)?.apply {
            val url = medias[currentPosition].url
            prepare(Uri.parse(url))
        }
    }

    fun pause() {
        videoViewSparseArray.get(currentPosition)?.apply { pause() }
    }

    fun release() {
        VideoManager.release()
    }

    override fun onPageSelected(position: Int) {
        lastPosition = currentPosition
        currentPosition = position

        videoViewSparseArray.get(currentPosition)?.apply {
            val url = medias[currentPosition].url
            prepare(Uri.parse(url))
        }

        videoViewSparseArray.get(lastPosition)?.apply { pause() }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
}
