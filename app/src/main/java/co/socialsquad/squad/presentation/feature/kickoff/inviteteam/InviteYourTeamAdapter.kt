package co.socialsquad.squad.presentation.feature.kickoff.inviteteam

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.EmailsToInvite
import co.socialsquad.squad.data.entity.InviteMembersRequest
import co.socialsquad.squad.presentation.custom.InputTextDisabled
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.addTextChangedListener
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlin.reflect.KFunction1

class InviteYourTeamAdapter(
    val activity: Activity,
    val setupSendInvitesButton: KFunction1<Boolean, Unit>
) :
    RecyclerView.Adapter<InviteYourTeamAdapter.ViewHolder>() {

    private val emails = mutableListOf<EmailsToInvite>()
    private var isAdded = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.invite_your_team_item, parent, false))

    override fun getItemCount(): Int = emails.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (isAdded && position == emails.size - 1) {
            holder.inputText.editText?.post {
                holder.inputText.editText?.visibility = View.VISIBLE
                holder.inputText.editText?.isEnabled = true
                holder.inputText.editText?.requestFocus()
            }

            isAdded = false
        }

        holder.container.setOnClickListener {
            holder.inputText.editText?.visibility = View.VISIBLE
            holder.inputText.editText?.isEnabled = true
            holder.inputText.editText?.requestFocus()
            holder.inputText.editText?.showKeyboard()
        }

        holder.inputText.editText?.addTextChangedListener {
            emails[position].email = it
            setupSendInvitesButton()
        }
    }

    fun setupSendInvitesButton() {
        var hasText = false
        emails.forEachIndexed { index, s ->
            if (s.email.trim().isNotEmpty()) {
                hasText = true
            }
        }

        setupSendInvitesButton(hasText)
    }

    fun hasInvalidEmail(): Boolean {
        emails.forEachIndexed { index, emailsToInvite ->
            if (emailsToInvite.email.isNotEmpty()) {
                if (!TextUtils.isValidEmail(emailsToInvite.email)) {
                    return true
                }
            }
        }

        return false
    }

    fun init() {
        val email = EmailsToInvite("", false)
        emails.add(email)
        emails.add(email)
        emails.add(email)
    }

    fun add() {
        emails.add(emails.size - 1, EmailsToInvite("", false))
        isAdded = true
        notifyItemInserted(emails.size - 1)
    }

    fun emails(): List<InviteMembersRequest> {
        val list = mutableListOf<InviteMembersRequest>()
        emails.forEach { list.add(InviteMembersRequest(it.email)) }
        return list
    }

    fun size(): Int = emails.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val inputText = itemView.findViewById<InputTextDisabled>(R.id.itText)
        val container = itemView.findViewById<LinearLayout>(R.id.container)
    }
}
