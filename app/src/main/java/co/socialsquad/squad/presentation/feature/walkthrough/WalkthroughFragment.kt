package co.socialsquad.squad.presentation.feature.walkthrough

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

private const val ARG_LAYOUT_RESOURCE = "ARG_LAYOUT_RESOURCE"

class WalkthroughFragment : Fragment() {

    companion object {
        fun newInstance(layoutResId: Int): WalkthroughFragment {
            val args = Bundle()
            args.putInt(ARG_LAYOUT_RESOURCE, layoutResId)
            val walkthroughFragment = WalkthroughFragment()
            walkthroughFragment.arguments = args
            return walkthroughFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutResId = arguments?.getInt(ARG_LAYOUT_RESOURCE)
        return layoutResId?.let { inflater.inflate(it, container, false) }
    }
}
