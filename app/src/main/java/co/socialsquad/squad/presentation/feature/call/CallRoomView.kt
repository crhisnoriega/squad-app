package co.socialsquad.squad.presentation.feature.call

import co.socialsquad.squad.data.entity.Profile

interface CallRoomView {
    fun setupLocalRenderer(callManager: CallingManager)
    fun setupRemoteRenderer(callManager: CallingManager)
    fun endCall()
    fun showStatusMessage(message: String)
    fun showStatusMessage(messageId: Int)
    fun failedCall()
    fun callReady()
    fun callAnswered()
    fun setScreenBackground(colors: List<String>)
    fun callConnected()
    fun setSpeakerButtonStatus(speakerOn: Boolean)
    fun setMicrophoneButtonStatus(micOn: Boolean)
    fun setCaller(profile: Profile)
    fun hideVideoLocal()
    fun showVideoLocal()
    fun changeLayoutToVideo()
    fun changeLayoutToAudio()
    fun showSmallUserInfo()
    fun showNormalUserInfo()
    fun hideUserInfos()
    fun setCompanyImage(url: String)
    fun changeVideoIconToAllow()
    fun changeVideoIconToBlock()
}
