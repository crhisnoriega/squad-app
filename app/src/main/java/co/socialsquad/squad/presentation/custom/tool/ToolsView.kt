package co.socialsquad.squad.presentation.custom.tool


import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.ResourceStatusFlow
import co.socialsquad.squad.domain.model.*
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunitySubmissionViewHolder
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunitySubmissionViewHolderModel
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunityTimetableViewHolder
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunityTimetableViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Tool
import co.socialsquad.squad.presentation.feature.opportunityv2.OpportunityDetailsActivity
import co.socialsquad.squad.presentation.feature.tool.submission.OpportunitySubmissionListActivity
import co.socialsquad.squad.presentation.feature.tool.timetable.OpportunityTimetableListActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_tools_widget.view.*
import kotlinx.coroutines.*
import org.koin.core.KoinComponent

private const val DEFAULT_LOADING = true
private const val ITEM_LOADING_COUNT = 3

class ToolsView(context: Context, attrs: AttributeSet?) :
        FrameLayout(context, attrs),
        KoinComponent {

    private var tool: Tool? = null
    private val viewModel = getKoin().get<ToolWidgetViewModel>()
    private val scope = CoroutineScope(Dispatchers.Main)
    private val defaultEmptyState = EmptyState(
            SimpleMedia("https://d1gpti4ogccykg.cloudfront.net/timetable_reservation.png"),
            context.getString(R.string.opportunity_empty_state_msg),
            context.getString(R.string.opportunity_empty_state_button)
    )

    @ExperimentalCoroutinesApi
    private val toolResourceFlow = ResourceStatusFlow(scope, viewModel.tools)

    @ExperimentalCoroutinesApi
    private val timeTableResourceFlow = ResourceStatusFlow(scope, viewModel.timetable)
    private val opportinityCreationResourceFlow =
            ResourceStatusFlow(scope, viewModel.opportunityWidgetCreation())

    var objectId: Long? = null
    var objectType: String? = null
    var onSubmissionClickListener: (submission: Submission) -> Unit = {}
    var onNewEscalaCallback: (tool: Tool) -> Unit = {}
    var onNewSubmissionCallback: (opportunityWidget: OpportunityWidget) -> Unit = {}
    var onEmptyClickListener: (tool: Tool) -> Unit = { tool ->

        objectId?.let { id ->
            when (tool.type) {
                RankingType.Oportunidade -> {
                    viewModel.createNewOpportunity(id, tool)
                }
                RankingType.Escala -> {
                    onNewEscalaCallback(tool)
                }
                RankingType.Invalid -> {

                }
            }
        }

    }
    var onShowAllOpportunitySubmission: (opportunity: Opportunity<Submission>) -> Unit = { opportunity ->
        OpportunitySubmissionListActivity.start(this.objectType!!, context, opportunity)
    }
    var onShowAllOpportunityTimetable: (opportunity: Opportunity<Timetable>) -> Unit = { opportunity ->
        OpportunityTimetableListActivity.start(context, opportunity)
    }
    var companyColor: CompanyColor? = null

    private val adapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is OpportunitySubmissionViewHolderModel -> OpportunitySubmissionViewHolder.ITEM_VIEW_MODEL_ID
            is OpportunityTimetableViewHolderModel ->
                OpportunityTimetableViewHolder.ITEM_VIEW_MODEL_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            OpportunitySubmissionViewHolder.ITEM_VIEW_MODEL_ID -> OpportunitySubmissionViewHolder.newInstance(
                    this@ToolsView, companyColor
            ).apply {
                onSubmissionClickListener = {
                    when (this@ToolsView.tool?.content?.title) {
                        "Indicações" -> OpportunityDetailsActivity.start(RankingType.Oportunidade, objectType!!, it.id.toString(), context, initials = it.initials, fromScreen = "object")
                        "Agendamentos" -> OpportunityDetailsActivity.start(RankingType.Schedule, objectType!!, it.id.toString(), context, it.initials, it.title)
                        "Reservas" -> OpportunityDetailsActivity.start(RankingType.Escala, objectType!!, it.id.toString(), context)
                    }

                }
            }
            OpportunityTimetableViewHolder.ITEM_VIEW_MODEL_ID -> OpportunityTimetableViewHolder.newInstance(
                    this@ToolsView, companyColor
            ).apply {
                onSubmissionClickListener = {
                    OpportunityDetailsActivity.start(RankingType.Escala, objectType!!, it.id.toString(), context)
                }
            }
            else -> throw IllegalArgumentException()
        }
    })
    private val adapterLoading =
            LoadingAdapter(R.layout.item_tool_lead_status_loading, ITEM_LOADING_COUNT)

    private var loading: Boolean = DEFAULT_LOADING

    init {
        setupStyledAttributes(attrs)
        inflateView()

        rv_leads.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_leads.setHasFixedSize(true)
    }

    @ExperimentalCoroutinesApi
    fun bind(
            objectType: String, objectId: Long, tool: Tool, color: CompanyColor?
    ) {

        this.objectId = objectId
        this.tool = tool
        this.objectType = objectType

        tool.content.icon.url?.let {
            loadToolIcon(it, img_tool)
        }
        lbl_tool_title.text = tool.content.title
        lbl_tool_subtitle.text = tool.content.subtitle

        hideAllButton()

        btn_empty_action.setOnClickListener { onEmptyClickListener(tool) }
        setupTimetableObservable(color)
        setupOpportunitiesObservable(color)
        setupOpportunitieCreationObservable()

        when (tool.content?.title) {
            "Indicações" -> {
                btn_empty_action.visibility = View.GONE
                viewModel.fetchOpportunities(tool.content.link)
            }

            "Agendamentos" -> {
                viewModel.fetchOpportunities(tool.content.link)
            }

            "Reservas" -> {
                viewModel.fetchTimetables(tool.content.link)
            }
        }
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.BaseWidget)) {
            loading = getBoolean(R.styleable.BaseWidget_loading, DEFAULT_LOADING)
        }
    }

    private fun inflateView() = View.inflate(context, R.layout.view_tools_widget, this)

    @ExperimentalCoroutinesApi
    private fun setupOpportunitiesObservable(color: CompanyColor?) {
        companyColor = color

        scope.launch {
            toolResourceFlow
                    .loading {
                        hideList()
                        hideEmptyState()
                        showLoading()
                    }
                    .success {

                        it?.button?.let { button ->
                            showAllButton(button, it)
                        } ?: hideAllButton()

                        it?.items?.let { submissions ->
                            it?.button?.let {
                                showSubmissions(submissions, it.text.isNullOrBlank().not())
                            } ?: run {
                                showSubmissions(submissions, false)
                            }
                            hideEmptyState()
                        } ?: run {
                            hideList()
                            showEmptyState(it?.emptyState ?: defaultEmptyState)
                        }
                    }
                    .empty {
                        hideList()
                        showEmptyState(it?.emptyState ?: defaultEmptyState)
                    }
                    .launch()
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupTimetableObservable(color: CompanyColor?) {
        companyColor = color

        scope.launch {
            timeTableResourceFlow
                    .loading {
                        hideList()
                        hideEmptyState()
                        showLoading()
                    }
                    .success {
                        it?.button?.let { button ->
                            showAllButtonTimetable(button, it)
                        } ?: hideAllButton()
                        it?.items?.let { submissions ->
                            showSubmissionsTimetable(submissions)
                            hideEmptyState()
                        } ?: run {
                            hideList()
                            showEmptyState(it?.emptyState ?: defaultEmptyState)
                        }
                    }
                    .empty {
                        hideList()
                        showEmptyState(it?.emptyState ?: defaultEmptyState)
                    }
                    .launch()
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupOpportunitieCreationObservable() {

        scope.launch {
            opportinityCreationResourceFlow

                    .loading {
                    }
                    .success {
                        it?.let { onNewSubmissionCallback }
                    }
                    .empty {
                    }
                    .launch()
        }
    }

    private fun showLoading() {
        rv_leads.adapter = adapterLoading
        rv_leads.isVisible = false
    }

    private fun hideList() {
        rv_leads.isVisible = false
    }

    private fun showAllButton(
            button: Button,
            opportunity: Opportunity<Submission>
    ) {
        if (!button.text.isNullOrBlank()) {
            lbl_show_all.text = button.text
            group_show_all.isVisible = true
            bg_show_all.setOnClickListener { opportunity.let(onShowAllOpportunitySubmission) }
            lbl_show_all.setOnClickListener { opportunity.let(onShowAllOpportunitySubmission) }
            iv_arrow_show_all.setOnClickListener { opportunity.let(onShowAllOpportunitySubmission) }
        } else hideAllButton()
    }

    private fun showAllButtonTimetable(
            button: Button,
            opportunity: Opportunity<Timetable>
    ) {
        if (!button.text.isNullOrBlank()) {
            lbl_show_all.text = button.text
            group_show_all.isVisible = true
            bg_show_all.setOnClickListener { opportunity.let(onShowAllOpportunityTimetable) }
            lbl_show_all.setOnClickListener { opportunity.let(onShowAllOpportunityTimetable) }
            iv_arrow_show_all.setOnClickListener { opportunity.let(onShowAllOpportunityTimetable) }
        } else hideAllButton()
    }

    private fun hideAllButton() {
        bg_show_all.setOnClickListener(null)
        bg_show_all.isVisible = false
        iv_arrow_show_all.isVisible = false
        group_show_all.isVisible = false
    }

    private fun showSubmissions(submissions: List<Submission>, showAll: Boolean) {
        rv_leads.adapter = adapter
        adapter.setItems(
                submissions.mapIndexed { index, submission ->
                    OpportunitySubmissionViewHolderModel(submission, companyColor, (index == submissions.size - 1) && showAll.not())
                }.toList()
        )
        rv_leads.isVisible = true
    }

    private fun showSubmissionsTimetable(submissions: List<Timetable>) {
        rv_leads.adapter = adapter
        adapter.setItems(
                submissions.mapIndexed { index, submission ->
                    OpportunityTimetableViewHolderModel(submission, companyColor, index == submissions.size - 1)
                }.toList()
        )
        rv_leads.isVisible = true
    }

    private fun hideEmptyState() {
        group_empty.isVisible = false
    }

    private fun showEmptyState(emptyState: EmptyState) {
        Glide.with(context)
                .load(emptyState.icon.url)
                .crossFade()
                .into(img_lead_empty)

        lbl_lead_message.text = emptyState.title
        btn_empty_action.text = emptyState.subtitle
        group_empty.isVisible = true

        when (tool?.content?.title) {
            "Indicações" -> {
                btn_empty_action.visibility = View.GONE
            }
        }

        when (tool?.type) {
            RankingType.Escala -> btn_empty_action.text = "Reservar Posição"
        }
    }

    private fun loadToolIcon(url: String, view: ImageView) {
        Glide.with(context)
                .load(url)
                // .circleCrop()
                .crossFade()
                .into(view)
    }

    override fun onSaveInstanceState(): Parcelable? {
        return super.onSaveInstanceState()
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        super.onRestoreInstanceState(state)
    }

    override fun onDetachedFromWindow() {
        scope.cancel()
        super.onDetachedFromWindow()
    }

    fun update() {

    }
}
