package co.socialsquad.squad.presentation.feature.event.list

import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.EventRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_EVENT_LIST
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EventListPresenter @Inject constructor(
    private val view: EventListView,
    private val tagWorker: TagWorker,
    private val eventRepository: EventRepository,
    loginRepository: LoginRepository
) {

    private val compositeDisposable = CompositeDisposable()
    private var page = 1
    private var isLoading = true
    private var isComplete = false
    private val companyColor = loginRepository.userCompany?.company?.primaryColor
    private val subdomain = loginRepository.subdomain

    fun onViewCreated() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_EVENT_LIST)
        view.setupList()
        subscribe(true)
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe(true)
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe(false)
    }

    private fun subscribe(containsShimmer: Boolean) {
        compositeDisposable.add(
            eventRepository.getEvents(page)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()

                    if (containsShimmer) {
                        view.showShimmer()
                    }
                }.doFinally {
                    isLoading = false
                    view.stopLoading()
                }
                .doOnNext { if (it.next == null) isComplete = true }
                .map { it.results.orEmpty().map { EventViewModel(it, companyColor, subdomain) } }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            if (containsShimmer) {
                                view.hideShimmer()
                            }

                            val shouldClearList = page == 1
                            view.addItems(it, shouldClearList)
                            page++
                        }
                    },
                    {
                        if (containsShimmer) {
                            view.hideShimmer()
                        }

                        view.showMessage(R.string.event_list_error_get_events)
                        it.printStackTrace()
                    },
                    {}
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }
}
