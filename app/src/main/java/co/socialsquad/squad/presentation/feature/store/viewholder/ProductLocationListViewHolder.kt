package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ProductLocationListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_product_location_list.view.*

class ProductLocationListViewHolder(itemView: View, val glide: RequestManager, val color: String?, val listener: Listener<StoreViewModel>) : ViewHolder<ProductLocationListViewModel>(itemView) {
    private val rvLocations: RecyclerView = itemView.product_location_list_rv_locations

    override fun bind(viewModel: ProductLocationListViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreViewModel -> PRODUCT_LOCATION_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PRODUCT_LOCATION_VIEW_MODEL_ID -> ProductLocationViewHolder(view, glide, color, listener)
                else -> throw IllegalArgumentException()
            }
        }

        with(rvLocations) {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(viewModel.list) }
            addItemDecoration(ListDividerItemDecoration(context))
            setItemViewCacheSize(0)
        }
    }

    override fun recycle() {}
}
