package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_recognition_success_history.view.*

class ItemSuccessHistoryRecognitionViewHolder(
        itemView: View, var onClickAction: ((item: ItemSuccessHistoryRecognitionViewModel) -> Unit)) : ViewHolder<ItemSuccessHistoryRecognitionViewModel>(itemView) {
    override fun bind(viewModel: ItemSuccessHistoryRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.successStoryData.title
        itemView.txtSubtitle.text = viewModel.successStoryData.description

    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}