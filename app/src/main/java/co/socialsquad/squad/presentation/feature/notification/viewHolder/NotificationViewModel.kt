package co.socialsquad.squad.presentation.feature.notification.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class NotificationViewModel(
    var pk: Long,
    var avatar: String?,
    var message: String?,
    var createdAt: String?,
    var userName: String?,
    var userPk: Int,
    var image: String?,
    var type: String?,
    var targetPk: Int,
    var clicked: Boolean
) : ViewModel
