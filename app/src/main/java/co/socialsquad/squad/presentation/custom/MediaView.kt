package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_media.view.*

class MediaView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        ConstraintLayout.inflate(context, R.layout.view_media, this)
    }

    fun setMedia(media: Media, lifecycleOwner: LifecycleOwner) {

        if (media.mimeType == "video/mp4" && !media.streamings.isNullOrEmpty()) {
            videoView.apply {
                visibility = View.VISIBLE
                loadThumbnail(media.streamings!![0].thumbnailUrl)
                val videoUri = Uri.parse(media.url)
                prepareVideo(videoUri)
                val observer = MediaLifeCycleObserver(this@MediaView, videoUri)
                lifecycleOwner.lifecycle.addObserver(observer)
            }
        } else {
            image.visibility = View.VISIBLE
            Glide.with(context)
                .load(media.url)
                .into(image)
        }
    }

    fun prepareVideo(videoUri: Uri) {
        videoView.prepare(videoUri)
    }

    fun pauseVideo() {
        videoView.pause()
    }

    class MediaLifeCycleObserver(
        private val mediaView: MediaView,
        private val videoUri: Uri
    ) : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            mediaView.prepareVideo(videoUri)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            mediaView.pauseVideo()
        }
    }
}
