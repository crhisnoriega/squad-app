package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.intentDial
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_product_location.view.*

class ProductLocationViewHolder(
    itemView: View,
    val glide: RequestManager,
    val color: String?,
    val listener: Listener<StoreViewModel>
) : ViewHolder<StoreViewModel>(itemView) {
    override fun bind(viewModel: StoreViewModel) {
        with(viewModel) {
            itemView.apply {
                glide.load(image)
                    .crossFade()
                    .into(ivImage)
                ivImage.clipToOutline = true

                short_title_lead.text = name

                val distance = itemView.context.getString(R.string.distance, distance)
                tvDistance.text = distance
                tvAddress.text = address
                tvAmount.text =
                    if (amount == 1) itemView.context.getString(R.string.product_locations_item_amount_one, amount)
                    else itemView.context.getString(R.string.product_locations_item_amount, amount)

                color?.let { tvAmount.setTextColor(ColorUtils.stateListOf(it)) }

                if (phone != null) {
                    ibPhone.setOnClickListener {
                        listener.onClick(viewModel)
                        context.intentDial(phone)
                    }
                } else {
                    ibPhone.setOnClickListener(null)
                }
            }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }
}
