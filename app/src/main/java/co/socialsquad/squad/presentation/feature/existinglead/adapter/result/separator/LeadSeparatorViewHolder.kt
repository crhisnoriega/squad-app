package co.socialsquad.squad.presentation.feature.existinglead.adapter.result.separator

import android.view.ViewGroup
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ResultsViewHolder
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM
import co.socialsquad.squad.presentation.util.inflate

class LeadSeparatorViewHolder(val parent: ViewGroup) :
    ResultsViewHolder(parent.inflate(R.layout.item_lead_result_separator)) {

    private var tvTitle: TextView = view.findViewById(R.id.tvTitle)

    override fun bind(item: ExistingLeadVM) {
        item.existingLeadSeparator?.let { existingLeadSeparator ->
            tvTitle.text = existingLeadSeparator.title.toUpperCase()
        }
    }
}
