package co.socialsquad.squad.presentation.feature.revenue.repository

import co.socialsquad.squad.presentation.feature.revenue.data.RevenueAPI
import co.socialsquad.squad.presentation.feature.revenue.model.EventsResponse
import co.socialsquad.squad.presentation.feature.revenue.model.StreamsResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface RevenueRepository {
    suspend fun fetchStreams(): Flow<StreamsResponse>
    suspend fun fetchEvents(): Flow<EventsResponse>
}

@ExperimentalCoroutinesApi
class RevenueRepositoryImp(private val revenueAPI: RevenueAPI) : RevenueRepository {
    override suspend fun fetchStreams(): Flow<StreamsResponse> {
        return flow {
            emit(revenueAPI.fetchStreams())
        }
    }

    override suspend fun fetchEvents(): Flow<EventsResponse> {
        return  flow{
            emit(revenueAPI.fetchEvents())
        }
    }


}