package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.linearlayout_bottom_sheet_button.view.*

class BottomSheetButton(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        View.inflate(context, R.layout.linearlayout_bottom_sheet_button, this)
        val styledAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.BottomSheetButton)
        val text = styledAttributes.getString(R.styleable.BottomSheetButton_android_text)
        tvTitle.text = text
        val iconResId = styledAttributes.getResourceId(R.styleable.BottomSheetButton_android_icon, 0)
        ivIcon.setImageResource(iconResId)

        styledAttributes.recycle()
    }
}
