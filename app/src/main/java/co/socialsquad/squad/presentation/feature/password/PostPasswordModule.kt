package co.socialsquad.squad.presentation.feature.password

import dagger.Binds
import dagger.Module

@Module
interface PostPasswordModule {
    @Binds
    fun view(view: PostPasswordActivity): PostPasswordView
}
