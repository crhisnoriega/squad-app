package co.socialsquad.squad.presentation.feature.audio.detail

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.OnAudioPlayedListener
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.audio.AudioManager
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_audio_details.*
import kotlinx.android.synthetic.main.view_audio_detail.*
import javax.inject.Inject

class AudioDetailActivity : BaseDaggerActivity(), AudioDetailView {

    companion object {
        const val EXTRA_AUDIO_PK = "EXTRA_AUDIO_PK"
        const val EXTRA_AUDIO_VIEW_MODEL = "EXTRA_AUDIO_VIEW_MODEL"
    }

    @Inject
    lateinit var audioDetailPresenter: AudioDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_details)
        setupToolbar()
        intent?.let { audioDetailPresenter.onCreate(it) }
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = getString(R.string.audio_detail_title)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun showInvalidPk() {
        ViewUtils.showDialog(this, getString(R.string.audio_not_found), DialogInterface.OnDismissListener { finish() })
    }

    private val onDeleteListener = object : ViewHolder.Listener<AudioViewModel> {
        override fun onClick(viewModel: AudioViewModel) {
            ViewUtils.showDialog(
                this@AudioDetailActivity,
                getString(R.string.audio_option_delete_confirmation),
                getString(R.string.live_creator_close_dialog_button_yes),
                DialogInterface.OnClickListener { _, _ ->
                    audioDetailPresenter.onDeleteClicked(viewModel)
                    AudioManager.release()
                },
                getString(R.string.live_creator_close_dialog_button_no)
            )
        }
    }

    private val onLikeListener = object : ViewHolder.Listener<AudioViewModel> {
        override fun onClick(viewModel: AudioViewModel) {
            audioDetailPresenter.onLikeClicked(viewModel.pk)
        }
    }

    private val onLikeCountListener = object : ViewHolder.Listener<AudioViewModel> {
        override fun onClick(viewModel: AudioViewModel) {
            val intent = Intent(this@AudioDetailActivity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.AUDIO_LIKE)
            }
            startActivity(intent)
        }
    }

    private val onAudioPlayedListener = object : OnAudioPlayedListener {
        override fun onPlayed(pk: Int) {
            audioDetailPresenter.onAudioPlayed(pk)
        }
    }

    override fun showAudio(audioViewModel: AudioViewModel) {
        AudioDetailViewHolder(
            clAudioDetail,
            onDeleteListener,
            onLikeListener,
            onLikeCountListener,
            onAudioPlayedListener
        ).bind(audioViewModel)
    }

    override fun close() {
        finish()
    }

    override fun showMessage(textResId: Int, onDismissListener: (() -> Unit)) {
        ViewUtils.showDialog(this, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun startLoading() {
        vLoading.visibility = View.VISIBLE
    }

    override fun stopLoading() {
        vLoading.visibility = View.GONE
        svAudioDetail.visibility = View.VISIBLE
    }
}
