package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatSpinner

class Spinner(context: Context, attrs: AttributeSet) : AppCompatSpinner(context, attrs) {
    override fun setSelection(position: Int) {
        super.setSelection(position)
        onItemReselected(position)
    }

    override fun setSelection(position: Int, animate: Boolean) {
        super.setSelection(position, animate)
        onItemReselected(position)
    }

    private fun onItemReselected(position: Int) {
        if (position == selectedItemPosition) {
            onItemSelectedListener?.onItemSelected(this, selectedView, position, selectedItemId)
        }
    }
}
