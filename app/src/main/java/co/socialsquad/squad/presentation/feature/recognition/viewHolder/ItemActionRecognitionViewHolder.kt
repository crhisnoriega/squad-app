package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.net.Uri
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.item_action_recognition.view.*


class ItemActionRecognitionViewHolder(
        itemView: View, var onClickAction: ((item: ItemActionRecognitionViewModel) -> Unit)) : ViewHolder<ItemActionRecognitionViewModel>(itemView) {
    override fun bind(viewModel: ItemActionRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.requirementData.title
        itemView.txtSubtitle.text = viewModel.requirementData.description
        itemView.approvalBadged.isVisible = viewModel.requirementData.progress?.boolean_value!!

        viewModel.requirementData.show_details?.let {
            itemView.iconNext.isVisible = it
        }

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.requirementData.icon), itemView.imgIcon)
    }

    override fun recycle() {

    }

    companion object {
        const val ITEM_ACTION_RECOGNITION_VIEW_HOLDER_ID = R.layout.item_action_recognition
    }
}