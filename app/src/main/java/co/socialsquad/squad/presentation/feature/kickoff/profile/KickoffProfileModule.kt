package co.socialsquad.squad.presentation.feature.kickoff.profile

import dagger.Binds
import dagger.Module

@Module
abstract class KickoffProfileModule {

    @Binds
    abstract fun view(kickoffProfileFragment: KickoffProfileFragment): KickoffProfileView
}
