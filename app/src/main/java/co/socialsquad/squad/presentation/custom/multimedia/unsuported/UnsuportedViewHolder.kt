package co.socialsquad.squad.presentation.custom.multimedia.unsuported

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.util.inflate

class UnsuportedViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?) : MediaViewHolder(parent.inflate(R.layout.view_multi_media_pager_unsuported), lifecycleOwner) {

    override fun bind(media: Media, label: String?, isFirst: Boolean, showLabel: Boolean) {
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {
    }
}
