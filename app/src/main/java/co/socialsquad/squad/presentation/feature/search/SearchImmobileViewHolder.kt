package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.net.Uri
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.immobiles.details.ImmobilesDetailsActivity
import co.socialsquad.squad.presentation.feature.immobiles.viewModel.ImmobileViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.view_immobile_list_item.view.*

class SearchImmobileViewHolder(
    itemView: View,
    val secondaryColor: String
) : ViewHolder<ImmobileViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: ImmobileViewModel) {
        with(viewModel) {
            glide.load(medias[0].url)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.image)

            itemView.name.text = name
            itemView.value.setTextColor(ColorUtils.parse(secondaryColor))
            itemView.value.text = start_price
            itemView.address.text = address_street.plus(", ").plus(address_district)
        }

        itemView.container.setOnClickListener {
            val gson = Gson()
            val json = gson.toJson(viewModel)
            openImmobile(json)
        }
    }

    override fun recycle() {
        glide.clear(itemView.image)
    }

    private fun openImmobile(immobile: String) {
        val intent = Intent(itemView.context, ImmobilesDetailsActivity::class.java)
        intent.putExtra(ImmobilesDetailsActivity.extraImmobile, immobile)
        //itemView.context.startActivity(intent)
    }

    private fun openExternalBrowser(fileUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(fileUrl))
        itemView.context.startActivity(intent)
    }
}
