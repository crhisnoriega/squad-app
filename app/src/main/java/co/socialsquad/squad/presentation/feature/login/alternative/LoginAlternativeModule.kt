package co.socialsquad.squad.presentation.feature.login.alternative

import dagger.Binds
import dagger.Module

@Module
abstract class LoginAlternativeModule {
    @Binds
    abstract fun view(loginAlternativeActivity: LoginAlternativeActivity): LoginAlternativeView
}
