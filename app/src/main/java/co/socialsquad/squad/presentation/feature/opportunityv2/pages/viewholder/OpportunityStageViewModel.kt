package co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityStage

class OpportunityStageViewModel(var stage: OpportunityStage, var isFirst: Boolean, var isLast: Boolean) : ViewModel {
}