package co.socialsquad.squad.presentation.util

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.preference.PreferenceManager
import androidx.core.content.ContextCompat
import co.socialsquad.squad.data.entity.UserCompany
import com.google.gson.Gson

object ColorUtils {
    fun parse(color: String) = Color.parseColor(color)

    fun parse(context: Context, color: Int) = ContextCompat.getColor(context, color)

    fun stateListOf(color: String): ColorStateList = ColorStateList.valueOf(parse(color))

    fun stateListOf(color: Int): ColorStateList = ColorStateList.valueOf(color)

    fun getCompanyColor(context: Context): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

}
