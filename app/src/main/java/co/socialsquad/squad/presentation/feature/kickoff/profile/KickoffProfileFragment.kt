
package co.socialsquad.squad.presentation.feature.kickoff.profile

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.User
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.bitmapCrossFade
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_kickoff_profile.*
import java.io.File
import java.util.Arrays
import javax.inject.Inject
import kotlin.reflect.KFunction1

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val TEMP_AVATAR_FILENAME = "avatar.jpg"

class KickoffProfileFragment(val enableButton: KFunction1<Boolean, Unit>) : Fragment(), KickoffProfileView, InputView.OnInputListener {

    private var isNextEnabled: Boolean = false

    @Inject
    lateinit var kickoffProfilePresenter: KickoffProfilePresenter

    private var miDone: MenuItem? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_kickoff_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        setupInputMasks()
        setupKeyboardActions()
        setupInputListener()
        setupOnClickListeners()
        setupDoneKeyboard()

        kickoffProfilePresenter.start()
    }

    private fun setupDoneKeyboard() {
        itMobile.editText?.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE && isNextEnabled) {
                onSaveClicked()
                false
            } else true
        }
    }

    override fun email(email: String) {
        itEmail.editText?.setText(email)
        itEmail.isEnabled = false
        itEmail.forceAnimation()
    }

    private fun setupInputMasks() {
        itFirstName.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.LETTERS) }
        itLastName.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.LETTERS) }
        itMobile.editText?.let {
            MaskUtils.applyMask(it, MaskUtils.Mask.MOBILE)
            MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_PARENTHESIS_DASH_SPACE)
        }
    }

    private fun setupKeyboardActions() {
        itLastName.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
        itMobile.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
    }

    private fun requestNewAvatar() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(Intent.createChooser(intent, null), REQUEST_CODE_PICK_IMAGE)
    }

    private fun setupInputListener() {
        setInputViews(
            Arrays.asList<InputView>(
                itFirstName,
                itLastName,
                itEmail,
                itMobile
            )
        )
    }

    private fun setupOnClickListeners() {
        ivAvatar.setOnClickListener { requestNewAvatar() }
        bEdit.setOnClickListener { requestNewAvatar() }
    }

    override fun setupInfo(user: User) {
        Glide.with(this)
            .asBitmap()
            .load(user.avatar ?: "")
            .circleCrop()
            .bitmapCrossFade()
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, isFirstResource: Boolean) = false
                override fun onResourceReady(resource: Bitmap, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    val directory = activity?.cacheDir
                    val file = File(directory, TEMP_AVATAR_FILENAME)
                    FileUtils.createFromBitmap(file, resource)

                    val uri = Uri.fromFile(file)
                    kickoffProfilePresenter.onResourceReady(uri)
                    return false
                }
            })
            .into(ivAvatar)

        itFirstName.text = TextUtils.toCamelcase(user.firstName)
        itLastName.text = TextUtils.toCamelcase(user.lastName)

        val email = if (user.contactEmail != null) user.contactEmail else user.email
        itEmail.text = email
        val phones = user.phones
        if (phones != null) {
            for (i in phones.indices) {
                val phone = phones[i]

                if (phone.category == "M") {
                    itMobile.text = MaskUtils.mask(MaskUtils.unmask(phone.number, MaskUtils.Mask.MOBILE), MaskUtils.Mask.MOBILE)
                }
            }
        }
    }

    override fun onInput() {
        kickoffProfilePresenter.onInput(
            itFirstName.text,
            itLastName.text,
            itEmail.text,
            itMobile.text
        )

        itFirstName.isValid(!itFirstName.isBlank)
        itLastName.isValid(!itLastName.isBlank)
        itEmail.isValid(TextUtils.isValidEmail(itEmail.text))
        itMobile.isValid(itMobile.length == 15)
    }

    override fun setSaveButtonEnabled(enabled: Boolean) {
        isNextEnabled = enabled
        enableButton(enabled)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_PICK_IMAGE -> if (resultCode == RESULT_OK) {
                data?.data?.let {
                    Glide.with(this)
                        .asBitmap()
                        .load(it)
                        .bitmapCrossFade()
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(e: GlideException?, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, isFirstResource: Boolean) = false
                            override fun onResourceReady(resource: Bitmap, model: Any, target: com.bumptech.glide.request.target.Target<Bitmap>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                                val directory = activity?.cacheDir
                                val file = File(directory, TEMP_AVATAR_FILENAME)
                                val inputStream = activity?.contentResolver?.openInputStream(it)!!
                                FileUtils.createFromInputStream(file, inputStream)
                                val uri = Uri.fromFile(file)
                                kickoffProfilePresenter.onResourceReady(uri)
                                activity?.setResult(RESULT_OK, Intent())
                                return false
                            }
                        })
                        .circleCrop()
                        .into(ivAvatar)
                }
            }
        }
    }

    override fun showLoading() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
    }

    override fun showInviteMembrers() {
        // activity?.apply { (this as KickoffActivity).moveToInviteMembers() }
    }

    override fun showErrorMessageFailedToRegisterUser() {
        ViewUtils.showDialog(activity, getString(R.string.kickoff_terms_error_failed_to_register_user), DialogInterface.OnDismissListener {})
    }

    override fun onDestroy() {
        kickoffProfilePresenter.onDestroy()
        super.onDestroy()
    }

    fun onSaveClicked() {
        itFirstName.hideKeyboard()
        kickoffProfilePresenter.onSaveClicked(
            itFirstName.text!!,
            itLastName.text!!,
            itEmail.text!!,
            itMobile.text!!
        )
    }
}
