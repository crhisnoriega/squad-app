package co.socialsquad.squad.presentation.custom

import androidx.recyclerview.widget.RecyclerView

class SnapHelperOneByOne : androidx.recyclerview.widget.LinearSnapHelper() {

    override fun findTargetSnapPosition(layoutManager: RecyclerView.LayoutManager?, velocityX: Int, velocityY: Int): Int {

        if (layoutManager !is androidx.recyclerview.widget.RecyclerView.SmoothScroller.ScrollVectorProvider) {
            return androidx.recyclerview.widget.RecyclerView.NO_POSITION
        }

        val currentView = findSnapView(layoutManager) ?: return androidx.recyclerview.widget.RecyclerView.NO_POSITION

        val currentPosition = layoutManager.getPosition(currentView)

        return if (currentPosition == androidx.recyclerview.widget.RecyclerView.NO_POSITION) {
            androidx.recyclerview.widget.RecyclerView.NO_POSITION
        } else currentPosition
    }
}
