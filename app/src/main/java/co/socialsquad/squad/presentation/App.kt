package co.socialsquad.squad.presentation

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.os.Build
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDexApplication
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.WebView
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.presentation.feature.base.di.mainModule
import co.socialsquad.squad.presentation.feature.base.di.networkModule
import co.socialsquad.squad.presentation.feature.base.di.providersModule
import com.facebook.stetho.Stetho
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import dagger.android.support.HasSupportFragmentInjector
import io.branch.referral.Branch
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import javax.inject.Inject

private const val SENDBIRD_APP_ID = "E4E5179B-915A-4925-BB03-543E397C8E2C"

class App : MultiDexApplication(), HasActivityInjector, HasSupportFragmentInjector, HasServiceInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingSupportFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var notificationManager: NotificationManager

    override fun activityInjector() = dispatchingActivityInjector

    override fun supportFragmentInjector() = dispatchingSupportFragmentInjector

    override fun serviceInjector() = dispatchingServiceInjector

    override fun onCreate() {
        super.onCreate()
        setupDagger()
        setupCalligraphy()
        setupStetho()
        setupBranch()
        setupNotificationChannels()
        setupKoin()
        setupSSLProvider()
    }

    private fun setupSSLProvider(){
        try {
            ProviderInstaller.installIfNeeded(this)
        } catch (e: GooglePlayServicesRepairableException) {
            GoogleApiAvailability.getInstance().showErrorNotification(this, e.connectionStatusCode)
        } catch (e: GooglePlayServicesNotAvailableException) {
            Log.e("SecurityException", "Google Play Services not available.")
        }
    }

    private fun setupKoin() {
        startKoin {
            androidLogger()
            androidContext(this@App)
            koin.loadModules(listOf(networkModule, mainModule, providersModule))
            koin.createRootScope()
        }
    }

    private fun setupBranch() {
        Branch.getAutoInstance(this)
        if (BuildConfig.DEBUG) {
            Branch.enableLogging()
        }
    }

    private fun setupStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    private fun setupDagger() {
        DaggerAppComponent.builder()
            .app(this)
            .build()
            .inject(this)
    }

    private fun setupCalligraphy() {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/gotham_medium.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
    }

    private fun setupNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(
                getString(R.string.notification_channel_general_id),
                getString(R.string.notification_channel_general_name),
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = getString(R.string.notification_channel_general_description)
                enableLights(true)
                enableVibration(true)
                notificationManager.createNotificationChannel(this)
            }

            NotificationChannel(
                getString(R.string.notification_channel_audio_id),
                getString(R.string.notification_channel_audio_name),
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = getString(R.string.notification_channel_audio_description)
                enableLights(false)
                enableVibration(false)
                setSound(null, null)
                notificationManager.createNotificationChannel(this)
            }
        }
    }

    companion object {
        var companyColor: CompanyColor = CompanyColor(Company.DEFAULT_PRIMARY_COLOR)
        var avatars: WebView? = null
    }
}
