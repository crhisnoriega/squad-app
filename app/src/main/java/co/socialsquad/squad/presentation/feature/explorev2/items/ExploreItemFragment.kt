package co.socialsquad.squad.presentation.feature.explorev2.items

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Explore
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionLoadingState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.argument
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.explore_item_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import javax.inject.Inject

class ExploreItemFragment : Fragment() {

    private val viewModel: ExploreItemViewModel by viewModel()

    private val link: String by argument(ARGUMENT_LINK)
    private val sectionName: String by argument(ARGUMENT_SECTION_NAME)
    private val template: SectionTemplate by argument(ARGUMENT_TEMPLATE)
    private val loadingState: List<SectionLoadingState> by argument(ARGUMENT_LOADING_STATE)
    private val emptyState: EmptyState? by argument(ARGUMENT_EMPTY_STATE)
    var mapsVisible = false

    @Inject
    lateinit var loginRepository: LoginRepository
    private val indicatorColor by lazy {
        loginRepository.userCompany?.company?.primaryColor.orEmpty()
    }

    private val loadingAdapter by lazy {
        ExploreItemsLoadingAdapter(template, loadingState)
    }
    private val adapter by lazy {
        ExploreItemsAdapter(requireContext(), layoutInflater, emptyState, indicatorColor)
    }

    companion object {
        private const val ARGUMENT_LINK = "ARGUMENT_LINK"
        private const val ARGUMENT_LOADING_STATE = "ARGUMENT_LOADING_STATE"
        private const val ARGUMENT_TEMPLATE = "ARGUMENT_TEMPLATE"
        private const val ARGUMENT_EMPTY_STATE = "ARGUMENT_EMPTY_STATE"
        private const val ARGUMENT_SECTION_NAME = "ARGUMENT_SECTION_NAME"

        private val fragments = mutableMapOf<Int, ExploreItemFragment>()

        @JvmStatic
        fun newInstance(
            link: String,
            template: SectionTemplate = SectionTemplate.getDefault(),
            loadingState: List<SectionLoadingState> = SectionLoadingState.getDefault(),
            emptyState: EmptyState? = null,
            sectionName: String? = ""
        ): ExploreItemFragment {

            var fragment = ExploreItemFragment().apply {
                val args = bundleOf(
                    ARGUMENT_LINK to link,
                    ARGUMENT_TEMPLATE to template,
                    ARGUMENT_LOADING_STATE to ArrayList(loadingState),
                    ARGUMENT_EMPTY_STATE to emptyState,
                    ARGUMENT_SECTION_NAME to sectionName
                )
                this.arguments = args
            }
            return fragment

        }

        @JvmStatic
        fun newInstance(
            section: Section,
            link: String,
            template: SectionTemplate = SectionTemplate.getDefault(),
            loadingState: List<SectionLoadingState> = SectionLoadingState.getDefault(),
            emptyState: EmptyState? = null,
            sectionName: String? = ""
        ): ExploreItemFragment {

            var fragment = fragments[section.id]

            if (fragment == null) {
                fragment = ExploreItemFragment().apply {
                    val args = bundleOf(
                        ARGUMENT_LINK to link,
                        ARGUMENT_TEMPLATE to template,
                        ARGUMENT_LOADING_STATE to ArrayList(loadingState),
                        ARGUMENT_EMPTY_STATE to emptyState,
                        ARGUMENT_SECTION_NAME to sectionName

                    )
                    this.arguments = args
                }

                // fragments.put(section.id, fragment)
            }


            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.explore_item_fragment, container, false)
        if (rvExploreItems != null && rvExploreItems.adapter != null && rvExploreItems.adapter is ExploreItemsAdapter) {
            adapter.clearList()
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupList()
        setupObserver()
        viewModel.fetchExplore(link, false)
    }

    @ExperimentalCoroutinesApi
    private fun setupList() {
        val layoutManager = LinearLayoutManager(context)
        rvExploreItems.apply {
            this.layoutManager = layoutManager
            addOnScrollListener(
                EndlessScrollListener(
                    5,
                    layoutManager,
                    {
                        if (viewModel.isLoading.not()) {
                            viewModel.getNextPage()
                        }
                    }
                )
            )
        }
    }

    private fun setupObserver() {
        lifecycleScope.launch {
            viewModel.explore().collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        rvExploreItems.adapter?.apply {
                            rvExploreItems.isVisible = true
                            llEmptyState.isVisible = false
                            it.data?.let { data ->
                                if (rvExploreItems.adapter is ExploreItemsLoadingAdapter) {
                                    rvExploreItems.adapter = adapter
                                    adapter.addData(data)
                                    adapter.notifyDataSetChanged()
                                } else {
                                    (rvExploreItems.adapter as ExploreItemsAdapter).addData(data)
                                }
                                mapsVisible = data.enableMapView
                                callback?.onItemLoaded(data)
                            }
                        }
                        mainContainer.setBackgroundColor(ColorUtils.parse("#eeeff4"))
                    }
                    Status.LOADING -> {
                        rvExploreItems.isVisible = true
                        llEmptyState.isVisible = false
                        if (adapter.itemCount > 0) {
                            adapter.addLoading()
                        } else {
                            rvExploreItems.adapter = loadingAdapter
                        }
                    }
                    Status.EMPTY -> {
                        rvExploreItems.isVisible = false
                        llEmptyState.isVisible = true
                        showEmptyState()
                        mainContainer.setBackgroundColor(Color.WHITE)
                    }
                    Status.ERROR -> {
                        rvExploreItems.isVisible = false
                        llEmptyState.isVisible = true
                    }
                }
            }
        }
    }

    private fun showEmptyState() {
        tvEmptyTitle.text = "Nenhum Objeto Encontrado"
        tvEmptyDescription.text = "Oops! Parece que o seu squad ainda não\n" +
                "cadastrou um objeto na seção \"${sectionName}\"."
        Glide.with(requireContext()).load(emptyState?.icon?.url)
            .into(ivEmptyIcon)
    }

    internal var callback: OnItemLoadedListener? = null
    fun setItemLoadedListener(callback: OnItemLoadedListener) {
        this.callback = callback
    }

    interface OnItemLoadedListener {
        fun onItemLoaded(explore: Explore)
    }
}
