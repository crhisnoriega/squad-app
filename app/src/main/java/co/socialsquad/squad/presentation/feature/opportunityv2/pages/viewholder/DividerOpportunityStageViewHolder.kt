package co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_card_opportunity_stage_pending.view.*


class DividerOpportunityStageViewHolder(itemView: View) :
        ViewHolder<ViewModel>(itemView) {

    override fun bind(viewModel: ViewModel) {



    }

    override fun recycle() {
    }

}