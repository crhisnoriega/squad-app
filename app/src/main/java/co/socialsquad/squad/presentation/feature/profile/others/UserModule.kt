package co.socialsquad.squad.presentation.feature.profile.others

import co.socialsquad.squad.presentation.feature.profile.ProfileInteractor
import co.socialsquad.squad.presentation.feature.profile.ProfileInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class UserModule {
    @Binds
    abstract fun view(profileOthersActivity: UserActivity): UserView

    @Binds
    abstract fun interactor(profileInteractorImpl: ProfileInteractorImpl): ProfileInteractor
}
