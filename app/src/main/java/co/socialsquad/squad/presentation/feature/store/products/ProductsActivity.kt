package co.socialsquad.squad.presentation.feature.store.products

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.searchv2.SearchActivity
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.PRODUCT_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_products.*
import javax.inject.Inject

class ProductsActivity : BaseDaggerActivity(), ProductsView {

    @Inject
    lateinit var productsPresenter: ProductsPresenter

    private lateinit var srlLoading: SwipeRefreshLayout

    private var rvProducts: RecyclerView? = null
    private var productsAdapter: RecyclerViewAdapter? = null

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_products, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.products_menu_item_search -> {
            productsPresenter.onSearchSelected()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        productsPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        rvProducts = products_rv_products
        srlLoading = products_srl_loading

        productsPresenter.onCreate(intent)
    }

    override fun setupToolbar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = title
    }

    override fun setupList(color: String?) {
        productsAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreProductViewModel -> PRODUCT_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PRODUCT_VIEW_MODEL_ID -> ProductViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        })

        rvProducts?.apply {
            layoutManager = PrefetchingLinearLayoutManager(this@ProductsActivity)
            adapter = productsAdapter
            addOnScrollListener(
                EndlessScrollListener(
                    8,
                    rvProducts?.layoutManager as LinearLayoutManager,
                    productsPresenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    override fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener { productsPresenter.onRefresh() }
    }

    override fun showMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(textResId), onDismissListener)
    }

    override fun setItems(products: List<StoreProductViewModel>) {
        productsAdapter?.setItems(products)
    }

    override fun addItems(products: List<StoreProductViewModel>) {
        productsAdapter?.addItems(products)
    }

    override fun enableRefreshing() {
        srlLoading.isEnabled = true
    }

    override fun stopRefreshing() {
        srlLoading.isRefreshing = false
    }

    override fun startLoading() {
        productsAdapter?.startLoading()
    }

    override fun stopLoading() {
        productsAdapter?.stopLoading()
    }

    override fun showSearch() {
        startActivity(Intent(this, SearchActivity::class.java))
    }
}
