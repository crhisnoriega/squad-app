package co.socialsquad.squad.presentation.custom.tool.viewHolder

import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.custom.ViewModel

data class OpportunityTimetableViewHolderModel(val timetable: Timetable, val companyColor: CompanyColor?, val isLast: Boolean = false) : ViewModel
