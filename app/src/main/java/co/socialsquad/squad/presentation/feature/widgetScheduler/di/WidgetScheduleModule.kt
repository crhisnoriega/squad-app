package co.socialsquad.squad.presentation.feature.widgetScheduler.di

import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.ScheduleSummaryRepository
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.ScheduleSummaryRepositoryImpl
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote.ScheduleSummaryApi
import co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary.ScheduleSummaryViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object WidgetScheduleModule {
    val instance = module {
        single {
            createApi(retrofit = get())
        }
        single<ScheduleSummaryRepository> {
            ScheduleSummaryRepositoryImpl(api = get())
        }
        viewModel {
            ScheduleSummaryViewModel(repository = get())
        }
    }

    private fun createApi(retrofit: Retrofit): ScheduleSummaryApi =
        retrofit.create(ScheduleSummaryApi::class.java)
}
