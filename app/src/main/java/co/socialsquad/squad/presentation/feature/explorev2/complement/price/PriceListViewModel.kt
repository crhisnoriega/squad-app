package co.socialsquad.squad.presentation.feature.explorev2.complement.price

import co.socialsquad.squad.domain.model.explore.PriceList
import co.socialsquad.squad.presentation.custom.ViewModel

data class PriceListViewModel(val priceList: PriceList) : ViewModel