package co.socialsquad.squad.presentation.feature.widgetScheduler.domain

import java.util.Date

data class ScaleTimeVO(
    val date: Date,
    val formattedDate: String,
    val shifts: List<ScaleShiftVO>,
    val isEnabled: Boolean,
    var isSelected: Boolean,
    var isActualDay: Boolean,
    var firstClickPerformed: Boolean
)
