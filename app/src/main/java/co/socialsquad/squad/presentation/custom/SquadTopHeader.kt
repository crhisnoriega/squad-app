package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_squad_top_header.view.*

class SquadTopHeader : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        View.inflate(context, R.layout.view_squad_top_header, this)
    }

    fun bindCompany(company: Company) {

        short_title_lead.text = company.name
        tvDescription.text = company.companyUrl

        company.isPublic?.let {
            if (it) {
                val color = Color.parseColor("#FFFFFF")
                short_title_lead.setTextColor(color)
                tvDescription.setTextColor(color)
                borderView.visibility = View.VISIBLE
            }
        }

        Glide.with(context)
            .load(company.avatar)
            .into(ivSquadAvatar)
    }

    fun bindFields(title: String?, description: String, url: String?) {
        short_title_lead.text = title
        tvDescription.text = description

        Glide.with(context)
            .load(url)
            .into(ivSquadAvatar)
    }
}
