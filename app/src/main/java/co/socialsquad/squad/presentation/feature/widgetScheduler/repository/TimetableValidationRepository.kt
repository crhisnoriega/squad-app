package co.socialsquad.squad.presentation.feature.widgetScheduler.repository

import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreationResponse
import kotlinx.coroutines.flow.Flow

interface TimetableValidationRepository {
    fun createSubmission(
        timetableId: Long,
        objectId: Long,
        timetableCreation: TimetableCreation
    ): Flow<TimetableCreationResponse?>
}
