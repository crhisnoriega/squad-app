package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Metric
import co.socialsquad.squad.data.entity.SegmentationQueryRequest
import co.socialsquad.squad.data.entity.SegmentationReachRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SegmentRepository
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

const val EXTRA_METRICS = "EXTRA_METRICS"
const val EXTRA_POTENTIAL_REACH = "EXTRA_POTENTIAL_REACH"

private const val REQUEST_CODE_VALUES = 40

class SegmentMetricsPresenter
@Inject constructor(
    private val segmentMetricsView: SegmentMetricsView,
    private val segmentRepository: SegmentRepository,
    loginRepository: LoginRepository
) {
    companion object {
        const val RESULT_CODE_METRIC = 41
    }

    private val compositeDisposable = CompositeDisposable()
    private val color = loginRepository.squadColor

    private var metrics: ArrayList<MetricViewModel> = arrayListOf()
    private var selectedMetrics: MutableList<MetricViewModel> = mutableListOf()
    private var potentialReach: PotentialReachViewModel? = null

    fun onCreate(intent: Intent) {
        with(segmentMetricsView) {
            setupToolbar()
            setupList()
            setupSwipeRefresh()
            color?.let { setPotentialReachArcColor(it) }
        }

        selectedMetrics = (intent.getSerializableExtra(EXTRA_METRICS) as? MetricsViewModel)?.selected.orEmpty().toMutableList()
        getMetrics()
    }

    private fun getMetrics() {
        compositeDisposable.add(
            segmentRepository.getMetrics()
                .doOnSubscribe { segmentMetricsView.showLoading() }
                .subscribe(
                    {
                        segmentMetricsView.hideLoading()
                        segmentMetricsView.enableRefreshing()
                        it.results?.map { mapMetric(it) }?.let { showMetrics(it) }
                        segmentMetricsView.enableFinishButton(selectedMetrics.isNotEmpty())
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        segmentMetricsView.showErrorMessage(
                            R.string.segment_metrics_error_get_metrics,
                            DialogInterface.OnDismissListener {
                                segmentMetricsView.hideLoading()
                                segmentMetricsView.enableRefreshing()
                            }
                        )
                    }
                )
        )
    }

    fun onRefresh() {
        compositeDisposable.clear()
        compositeDisposable.add(
            segmentRepository.getMetrics()
                .doOnTerminate { segmentMetricsView.stopRefreshing() }
                .subscribe(
                    {
                        it.results?.map { mapMetric(it) }?.let { showMetrics(it) }
                    },
                    {
                        it.printStackTrace()
                        segmentMetricsView.showErrorMessage(R.string.segment_metrics_error_refresh)
                    }
                )
        )
    }

    private fun showMetrics(metrics: List<MetricViewModel>) {
        setSelectedValues(metrics)
        this@SegmentMetricsPresenter.metrics.apply {
            clear()
            addAll(metrics)
            segmentMetricsView.setItems(this)
            getPotentialReach()
        }
    }

    private fun setSelectedValues(metrics: List<MetricViewModel>) {
        metrics.forEach { metric ->
            selectedMetrics.find { it.field == metric.field }?.let { selectedMetric ->
                metric.values.forEach { value ->
                    value.selected = selectedMetric.values.find { it.value == value.value } != null
                }
            }
        }
    }

    private fun mapMetric(metric: Metric): MetricViewModel {
        with(metric) {
            val values = selectedMetrics.find { it.field == field }?.values.orEmpty()
            return MetricViewModel(field, displayName, values, multipick, color)
        }
    }

    fun onMetricClicked(metric: MetricViewModel) {
        segmentMetricsView.openSegmentMetric(
            findSelectedMetric(metric)
                ?: metric,
            REQUEST_CODE_VALUES
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED || data == null) return

        if (requestCode == REQUEST_CODE_VALUES && resultCode == SegmentValuesPresenter.RESULT_CODE_VALUES) {
            val metric = data.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel
            metric?.let {
                if (it.values.isEmpty() || findSelectedMetric(it) != null) {
                    removeMetricByName(it.field)
                }
                if (!it.values.isEmpty()) {
                    selectedMetrics.add(it)
                }
                metrics.filter { m -> it.field == m.field }.firstOrNull()?.let { listViewModel ->
                    listViewModel.values = it.values
                    segmentMetricsView.updateItem(listViewModel)
                }
            }
            getPotentialReach()
        }
    }

    private fun findSelectedMetric(metric: MetricViewModel) = selectedMetrics.find { it.field == metric.field }

    private fun removeMetricByName(name: String) {
        selectedMetrics.removeAll { it.field == name }
    }

    fun onFinishClicked() {
        val intent = Intent()
        intent.putExtra(EXTRA_METRICS, MetricsViewModel(selectedMetrics))
        intent.putExtra(EXTRA_POTENTIAL_REACH, potentialReach)
        segmentMetricsView.finishWithResult(SegmentValuesPresenter.RESULT_CODE_VALUES, intent)
    }

    fun getPotentialReach() {
        compositeDisposable.add(
            Observable.just(selectedMetrics)
                .doOnSubscribe { segmentMetricsView.enableFinishButton(false) }
                .map { it.map { SegmentationQueryRequest(it.field, it.values.map { it.value }) } }
                .map { SegmentationReachRequest(it) }
                .flatMap { segmentRepository.getPotentialReach(it) }
                .map { PotentialReachViewModel(it.bars, it.total, it.potentialReach) }
                .doOnNext { this.potentialReach = it }
                .doOnComplete { segmentMetricsView.enableFinishButton(true) }
                .subscribe({ segmentMetricsView.setPotentialReachValue(it) }, { it.printStackTrace() })
        )
    }
}
