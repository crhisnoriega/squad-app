package co.socialsquad.squad.presentation.feature.login.forgotpassword

import co.socialsquad.squad.data.entity.FindSubdomainRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.util.Analytics
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ResetPasswordPresenter @Inject constructor(
    private val view: ResetPasswordView,
    private val loginRepository: LoginRepository,
    private val analytics: Analytics
) {

    val compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var presenter: ResetPasswordActivity

    fun getSubdomain(subdomain: String) {
        val request = FindSubdomainRequest(subdomain)

        compositeDisposable.add(
            loginRepository.findSubdomain(request)
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        view.setupHeader(it)
                        view.hideLoading()
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    fun onCreate() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_NEW_PASS_SCREEN_VIEW)
    }

    fun onNextClick() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_NEW_PASS_TAP_NEXT)
    }
}
