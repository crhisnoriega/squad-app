package co.socialsquad.squad.presentation.feature.users

import android.content.DialogInterface
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel

interface UserListView {
    fun showTabLayout()
    fun hideTabLayout()
    fun setItems(users: List<UserViewModel>)
    fun addItems(users: List<UserViewModel>)
    fun showMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?)
    fun stopRefreshing()
    fun startLoading()
    fun stopLoading()
    fun setupToolbar(titleResId: Int, subTitleResId: Int? = null)
    fun setTotalSeen(title: Int, count: Int)
    fun setTotalNotSeen(title: Int, count: Int)
    fun setupTabs(titleSeen: Int, titleNotSeen: Int, squadColor: String?)
}
