package co.socialsquad.squad.presentation.feature.store.list

import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.createLocationRequest
import co.socialsquad.squad.presentation.util.isLocationServiceEnabled
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_store_list.*
import javax.inject.Inject

class StoreListFragment : Fragment(), StoreListView {

    @Inject
    lateinit var presenter: StoreListPresenter

    private lateinit var adapter: RecyclerViewAdapter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_store_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSwipeRefresh()
        setupOnClickListeners()
        presenter.onViewCreated()
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener {
            srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            adapter.stopLoading()
            presenter.onRefresh()
        }
    }

    private fun setupOnClickListeners() {
        lPermissionsDenied.setOnClickListener { presenter.onRequestPermissionsClicked() }
        lLocationDisabled.setOnClickListener { presenter.onLocationDisabledClicked() }
    }

    override fun setupList(companyColor: String?) {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is StoreViewModel -> R.layout.view_store_list_item
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                R.layout.view_store_list_item -> StoreListItemViewHolder(view, onCallClickedListener)
                else -> throw IllegalArgumentException()
            }
        })
        rv.apply {
            adapter = this@StoreListFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    private val onCallClickedListener = object : ViewHolder.Listener<StoreViewModel> {
        override fun onClick(viewModel: StoreViewModel) {
            presenter.onCallClicked()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            presenter.onVisibleToUser()
        }
    }

    override fun startLoading() {
        if (!srl.isRefreshing) adapter.startLoading()
    }

    override fun stopLoading() {
        adapter.stopLoading()
        srl.isRefreshing = false
    }

    override fun showPermissionsDenied() {
        srl.visibility = View.GONE
        lLocationDisabled.visibility = View.GONE
        lPermissionsDenied.visibility = View.VISIBLE
    }

    override fun showLocationDisabled() {
        srl.visibility = View.GONE
        lPermissionsDenied.visibility = View.GONE
        lLocationDisabled.visibility = View.VISIBLE
    }

    override fun showList() {
        lLocationDisabled.visibility = View.GONE
        lPermissionsDenied.visibility = View.GONE
        srl.visibility = View.VISIBLE
    }

    override fun verifyLocationServiceAvailability() =
        activity?.isLocationServiceEnabled { presenter.onLocationServiceAvailability(it) }

    override fun addItems(items: List<ViewModel>, shouldClearList: Boolean) {
        if (shouldClearList) adapter.setItems(items) else adapter.addItems(items)
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit?) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter.onRequestPermissionsResult(grantResults)
    }

    override fun checkPermissions(permissions: Array<String>) =
        context?.let { context ->
            permissions.all { ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED }
        } ?: false

    override fun requestPermissions(requestCode: Int, permissions: Array<String>) {
        requestPermissions(permissions, requestCode)
    }

    override fun createLocationRequest() {
        activity?.createLocationRequest { request, intent ->
            presenter.onLocationRequestCreated(request, intent)
        }
    }

    override fun showEmptyState() {
        srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.VISIBLE
    }
}
