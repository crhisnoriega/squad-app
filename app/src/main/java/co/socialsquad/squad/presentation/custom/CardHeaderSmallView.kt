package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.linearlayout_card_header_small_view.view.*

class CardHeaderSmallView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var tvTitle: TextView
    private var tvDescription: TextView
    private var ivImage: ImageView

    init {
        inflate(context, R.layout.linearlayout_card_header_small_view, this)

        ivImage = card_header_small_view_iv_image
        tvTitle = card_header_small_view_tv_title
        tvDescription = card_header_small_view_tv_description

        val styledAttributes =
            getContext().obtainStyledAttributes(attrs, R.styleable.CardHeaderSmallView)

        val imageResId =
            styledAttributes.getResourceId(R.styleable.CardHeaderSmallView_android_icon, 0)
        ivImage.setImageResource(imageResId)

        tvTitle.text = styledAttributes.getString(R.styleable.CardHeaderSmallView_android_title)

        tvDescription.text =
            styledAttributes.getString(R.styleable.CardHeaderSmallView_android_description)

        styledAttributes.recycle()
    }

    fun setIcon(iconResID: Int) {
        ivImage.setImageResource(iconResID)
    }

    fun setTitle(title: String) {
        tvTitle.text = title
    }

    fun setDescription(description: String) {
        tvDescription.text = description
    }
}
