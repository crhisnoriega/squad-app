package co.socialsquad.squad.presentation.feature.profile.others

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.ProfileQualificationActivity
import co.socialsquad.squad.presentation.feature.profile.viewHolder.PROFILE_ABOUT_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileAboutViewHolder
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileAboutViewModel
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileHeaderViewHolder
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import co.socialsquad.squad.presentation.feature.social.AudioToggledViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SOCIAL_POST_IMAGE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.SOCIAL_POST_VIDEO_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.SocialDeleteDialog
import co.socialsquad.squad.presentation.feature.social.SocialFragment
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.SocialViewModel
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_profile_others.*
import javax.inject.Inject

const val USER_PK_EXTRA = "USER_PK_EXTRA"

class UserActivity : BaseDaggerActivity(), UserView {

    @Inject
    lateinit var userPresenter: UserPresenter

    private var socialAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var currentMostVisibleItemPosition = -1

    private var socialDeleteDialog: SocialDeleteDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_others)
        setupToolbar()
        setupList()
        setupSwipeRefresh()
        userPresenter.onCreate(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = ""
    }

    private fun setupSwipeRefresh() {
        srlContainer.setOnRefreshListener {
            userPresenter.onRefresh()
        }
    }

    private fun setupList() {
        socialAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ProfileViewModel -> R.layout.view_profile_header
                is PostVideoViewModel -> SOCIAL_POST_VIDEO_VIEW_MODEL_ID
                is PostImageViewModel -> SOCIAL_POST_IMAGE_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is ProfileAboutViewModel -> PROFILE_ABOUT_VIEW_HOLDER_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    R.layout.view_profile_header -> ProfileHeaderViewHolder(view, null)
                    SOCIAL_POST_VIDEO_VIEW_MODEL_ID -> PostVideoViewHolder(
                        view,
                        onVideoClickedListener, onAudioClickedListener, onDeleteListener,
                        onEditListener, onLikeClickListener, onUsersLikedListener, onCommentListener, onCommentListListener
                    )
                    SOCIAL_POST_IMAGE_VIEW_MODEL_ID -> PostImageViewHolder(
                        view,
                        onDeleteListener, onEditListener, onLikeClickListener,
                        onUsersLikedListener, onCommentListener, onCommentListListener
                    )
                    LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                    HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                    PROFILE_ABOUT_VIEW_HOLDER_ID -> ProfileAboutViewHolder(view, onAboutItemClicked)
                    DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)

                    else -> throw IllegalArgumentException()
                }
            }
        })
        linearLayoutManager = PrefetchingLinearLayoutManager(this)
        rvPosts?.apply {
            adapter = socialAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(0)
            addOnScrollListener(EndlessScrollListener())
            setHasFixedSize(true)
            isFocusable = false
        }
    }

    private val onAboutItemClicked = object : ViewHolder.Listener<ProfileAboutViewModel> {
        override fun onClick(viewModel: ProfileAboutViewModel) {
            val intent = Intent(this@UserActivity, ProfileQualificationActivity::class.java)
            intent.putExtra(ProfileQualificationActivity.EXTRA_QUALIFICATION_PK, viewModel.userPk)
            startActivity(intent)
        }
    }

    private val onCommentListListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@UserActivity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL, viewModel)
                    is PostVideoViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL, viewModel)
                    is PostImageViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL, viewModel)
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
            }
            startActivity(intent)
        }
    }

    private val onCommentListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@UserActivity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL, viewModel)
                    is PostVideoViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL, viewModel)
                    is PostImageViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL, viewModel)
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
                putExtra(PostDetailsActivity.IS_COMMENTING, true)
            }
            startActivity(intent)
        }
    }

    private val onUsersLikedListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@UserActivity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.POST_LIKE)
            }
            startActivity(intent)
        }
    }

    private val onDeleteListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onDeleteSelected(viewModel)
        }
    }

    fun onDeleteSelected(viewModel: PostViewModel) {
        socialDeleteDialog = SocialDeleteDialog(
            this,
            object : SocialDeleteDialog.Listener {
                override fun onNoClicked() {
                    hideDeleteDialog()
                }

                override fun onYesClicked() {
                    userPresenter.onDeleteClicked(viewModel)
                    setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
                }
            }
        )

        if (!this.isFinishing) socialDeleteDialog?.show()
    }

    private val onLikeClickListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            userPresenter.likePost(viewModel)
        }
    }

    private val onEditListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onEditSelected(viewModel)
        }
    }

    fun onEditSelected(viewModel: PostViewModel) {
        userPresenter.onEditSelected(viewModel)
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<PostVideoViewModel> {
        override fun onClick(viewModel: PostVideoViewModel) {
            onVideoClicked(viewModel.hlsUrl)
        }
    }

    fun onVideoClicked(url: String) {
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    private val onAudioClickedListener = object : ViewHolder.Listener<AudioToggledViewModel> {
        override fun onClick(viewModel: AudioToggledViewModel) {
            onAudioToggled(viewModel.audioEnabled)
        }
    }

    override fun onAudioToggled(audioEnabled: Boolean) {
        userPresenter.onAudioToggled(audioEnabled)
    }

    override fun showLoading() {
        llLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        llLoading.visibility = View.GONE
        srlContainer.visibility = View.VISIBLE
        srlContainer.isRefreshing = false
    }

    override fun showProfile(profileViewModel: ProfileViewModel) {
        this.title = "${profileViewModel.firstName} ${profileViewModel.lastName}"
        socialAdapter?.setItems(listOf(profileViewModel))
        if (profileViewModel.canSeeQualification) {
            socialAdapter?.addItems(
                listOf(
                    HeaderViewModel(getString(R.string.profile_about_title)),
                    ProfileAboutViewModel(profileViewModel.pk, profileViewModel.qualification.toString(), getString(R.string.qualification_title), profileViewModel.qualificationBadge.orEmpty(), true),
                    DividerViewModel
                )
            )
        }
    }

    override fun showPosts(posts: List<SocialViewModel>) {
        socialAdapter?.addItems(posts)
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(resId), onDismissListener)
    }

    override fun showLoadingMore() {
        socialAdapter?.startLoading()
    }

    override fun hideLoadingMore() {
        socialAdapter?.stopLoading()
    }

    override fun showEditMedia(postViewModel: PostViewModel) {
        val intent = Intent(this, SocialCreateMediaActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, postViewModel)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_MEDIA)
    }

    override fun showEditLive(postViewModel: PostLiveViewModel) {
        val intent = Intent(this, SocialCreateLiveActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, postViewModel)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_LIVE)
    }

    override fun hideDeleteDialog() {
        socialDeleteDialog?.dismiss()
    }

    override fun removePost(viewModel: PostViewModel) {
        socialAdapter?.remove(viewModel)
    }

    override fun updatePost(post: PostViewModel) {}

    private inner class EndlessScrollListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 8

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            linearLayoutManager?.apply {
                if (findLastVisibleItemPosition() > itemCount - visibleThreshold) {
                    userPresenter.onScrolledBeyondVisibleThreshold()
                }

                val rect = Rect()
                recyclerView.getGlobalVisibleRect(rect)
                val containerHeight = rect.height()
                val referencePoint = containerHeight / 2 + rect.top

                val firstVisibleItemPosition = findFirstVisibleItemPosition()
                val lastVisibleItemPosition = findLastVisibleItemPosition()

                val previousMostVisibleItemPosition = currentMostVisibleItemPosition

                var mostVisibleItemPosition = firstVisibleItemPosition
                var leastDifferenceFromReferencePoint = containerHeight
                for (i in firstVisibleItemPosition..lastVisibleItemPosition) {
                    val viewHolder = recyclerView.findViewHolderForAdapterPosition(i)
                    if (viewHolder != null) {
                        val r = Rect()
                        viewHolder.itemView.getGlobalVisibleRect(r)
                        val difference = Math.abs(r.centerY() - referencePoint)
                        if (difference < leastDifferenceFromReferencePoint) {
                            leastDifferenceFromReferencePoint = difference
                            mostVisibleItemPosition = i
                        }
                    }
                }

                if (previousMostVisibleItemPosition != mostVisibleItemPosition) {
                    currentMostVisibleItemPosition = mostVisibleItemPosition
                    setMostVisibleItemPosition(mostVisibleItemPosition, previousMostVisibleItemPosition)
                }
            }
        }
    }

    private fun setMostVisibleItemPosition(mostVisibleItemPosition: Int, previousMostVisibleItemPosition: Int) {
        stopVideo(previousMostVisibleItemPosition)
        playVideo(mostVisibleItemPosition)
    }

    private fun stopVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.stop()
        }
    }

    private fun playVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play()
        }
    }
}
