package co.socialsquad.squad.presentation.feature.users.viewHolder

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable

const val USER_VIEW_MODEL_ID = R.layout.view_user_list_item

data class UserViewModel(
    var pk: Int,
    var name: String,
    var qualification: String?,
    var avatarUrl: String?
) : ViewModel, Serializable
