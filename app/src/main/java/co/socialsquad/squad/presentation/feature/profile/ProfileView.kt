package co.socialsquad.squad.presentation.feature.profile

import android.content.DialogInterface
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModel

interface ProfileView {
    fun openSettings()
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun showProfile(profileViewModel: ProfileViewModel, useQualification: Boolean)
    fun showPosts(posts: List<SocialViewModel>)
    fun showLoading()
    fun hideLoading()
    fun hideDeleteDialog()
    fun showEditMedia(post: PostViewModel)
    fun showEditLive(post: PostLiveViewModel)
    fun removePost(viewModel: ViewModel)
    fun stopRefreshing()
    fun showLoadingMore()
    fun hideLoadingMore()
    fun onAudioToggled(audioEnabled: Boolean)
    fun updatePost(post: PostViewModel)
}
