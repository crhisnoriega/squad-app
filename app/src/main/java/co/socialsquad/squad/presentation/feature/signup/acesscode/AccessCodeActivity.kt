package co.socialsquad.squad.presentation.feature.signup.acesscode

import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlinx.android.synthetic.main.activity_access_code.*
import kotlinx.android.synthetic.main.view_login_input_text.*

class AccessCodeActivity : BaseDaggerActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_code)
        setupTexts()

        iltInput.setOnKeyboardActionListener { }
        iltInput.requestFocus()
        iltInput.showKeyboard()

        signup_credentials_b_next.isEnabled = false
        signup_credentials_ib_close.setOnClickListener { finish() }
    }

    private fun setupTexts() {
        tvLoginHelp.text = getString(R.string.insert_access_code)
    }
}
