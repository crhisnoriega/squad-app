package co.socialsquad.squad.presentation.util

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import javax.inject.Inject

class Analytics @Inject constructor(private val analytics: FirebaseAnalytics) {

    companion object {
        // Sigup
        const val SIGNUP_SCREEN_VIEW = "SignUp_Email_View"
        const val SIGNUP_TAP_NEXT = "SignUp_Email_Button_Next"
        const val SIGNUP_ACTION_INVALID_EMAIL = "SignUp_Email_Error_InvalidEmail"
        const val SIGNUP_ACTION_NO_CONNECTION = "SignUp_Email_Error_NoConnection"

        // Sigup - Email
        const val SIGNUP_VERIFY_EMAIL_SCREEN_VIEW = "SignUp_VerifyEmail_View"
        const val SIGNUP_VERIFY_EMAIL_TAP_OPEN_EMAIL = "SignUp_VerifyEmail_Button_OpenEmailApp"
        const val SIGNUP_VERIFY_EMAIL_TAP_ENTER_MANUALLY = "SignUp_VerifyEmail_Button_EnterManually"

        // Sigup - Squads
        const val SIGNUP_SQUADS_SCREEN_VIEW = "SignUp_Squads_View"
        const val SIGNUP_SQUADS_TAP_LAUNCH = "SignUp_Squads_Button_Launch"
        const val SIGNUP_SQUADS_TAP_NEXT = "SignUp_Squads_Button_Next"

        // Sigup - About
        const val SIGNUP_ABOUT_SCREEN_VIEW = "SignUp_About_View"
        const val SIGNUP_ABOUT_TAP_NEXT = "SignUp_About_Button_Next"
        const val SIGNUP_ABOUT_ACTION_SCROLL_DOWN = "SignUp_About_Scroll_Down"

        // Sigup - Password
        const val SIGNUP_CREATE_PASSWORD_SCREEN_VIEW = "SignUp_CreatePassword_View"
        const val SIGNUP_CREATE_PASSWORD_TAP_NEXT = "SignUp_CreatePassword_Button_Next"

        // Sigup - Terms
        const val SIGNUP_TERMS_SCREEN_VIEW = "SignUp_Terms_View"
        const val SIGNUP_TERMS_TAP_AGREE = "Signup_Terms_Button_Agree"
        const val SIGNUP_TERMS_ACTION_SCROLL_DOWN = "SignUp_Terms_Scroll_Down"

        // Sign
        const val SIGN_SCREEN_VIEW = "Login_Options_View"
        const val SIGN_TAP_MAGIG_LINK = "Login_Options_Button_MagicLink"
        const val SIGN_TAP_ENTER_MANUALLY = "Login_Options_Button_EnterManually"

        // Sign - Email
        const val SIGN_EMAIL_SCREEN_VIEW = "Login_Email_View"
        const val SIGN_EMAIL_TAP_NEXT = "Login_Email_Button_Next"
        const val SIGN_EMAIL_ACTION_NO_SQUAD = "Login_Email_Error_NoSquad"
        const val SIGN_EMAIL_ACTION_NO_CONNECTION = "Login_Email_Error_NoConnection"

        // Sign - Squads
        const val SIGN_SQUADS_SCREEN_VIEW = "Login_Squads_View"
        const val SIGN_SQUADS_TAP_NEXT = "Login_Squads_Button_Next"

        // Sign - Password
        const val SIGN_EMAIL_PASSWORD_SCREEN_VIEW = "Login_Password_View"
        const val SIGN_EMAIL_PASSWORD_TAP_RESET_PASS = "Login_Password_Button_ResetPassword"
        const val SIGN_EMAIL_PASSWORD_TAP_LAUNCH = "Login_Password_Button_Launch"
        const val SIGN_EMAIL_PASSWORD_ACTION_INVALID_PASSWORD = "Login_Password_Error_InvalidPassword"
        const val SIGN_EMAIL_PASSWORD_ACTION_NO_CONNECTION = "Login_Password_Error_NoConnection"

        // Magic Link - Password
        const val MAGIC_LINK_EMAIL_SCREEN_VIEW = "MagicLink_Email_View"
        const val MAGIC_LINK_EMAIL_TAP_NEXT = "MagicLink_Email_Button_Next"
        const val MAGIC_LINK_EMAIL_ACTION_INVALID_EMAIL = "MagicLink_Email_Error_InvalidEmail"
        const val MAGIC_LINK_EMAIL_ACTION_NO_SQUAD = "MagicLink_Email_Error_NoSquad"
        const val MAGIC_LINK_EMAIL_ACTION_NO_CONNECTION = "MagicLink_Email_Error_NoConnection"

        // Magic Link - Verify Email
        const val MAGIC_LINK_VERIFY_EMAIL_SCREEN_VIEW = "MagicLink_VerifyEmail_View"
        const val MAGIC_LINK_VERIFY_EMAIL_TAP_OPEN_EMAIL = "MagicLink_VerifyEmail_Button_OpenEmail"
        const val MAGIC_LINK_VERIFY_EMAIL_TAP_ENTER_MANUALLY = "MagicLink_VerifyEmail_Button_Manual"

        // Magic Link - Squads
        const val MAGIC_LINK_SQUADS_SCREEN_VIEW = "MagicLink_Squads_View"
        const val MAGIC_LINK_SQUADS_TAP_LAUNCH = "MagicLink_Squads_Button_Launch"

        // Reset Password - Password
        const val RESET_PASSWORD_EMAIL_SCREEN_VIEW = "PasswordReset_Email_View"
        const val RESET_PASSWORD_EMAIL_TAP_NEXT = "PasswordReset_Email_Button_Next"
        const val RESET_PASSWORD_EMAIL_ACTION_INVALID_EMAIL = "PasswordReset_Email_Error_InvalidEmail"
        const val RESET_PASSWORD_EMAIL_ACTION_NO_CONNECTION = "PasswordReset_Email_Error_NoConnection"

        // Reset Password - Verify Email
        const val RESET_PASSWORD_VERIFY_EMAIL_SCREEN_VIEW = "PasswordReset_VerifyEmail_View"
        const val RESET_PASSWORD_VERIFY_EMAIL_TAP_OPEN_EMAIL = "PasswordReset_Verify_Button_EmailApp"

        // Reset Password - New Password
        const val RESET_PASSWORD_NEW_PASS_SCREEN_VIEW = "PasswordReset_NewPassword_View"
        const val RESET_PASSWORD_NEW_PASS_TAP_NEXT = "PasswordReset_NewPassword_Button_Next"

        // Reset Password - Confirm New Password
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_SCREEN_VIEW = "PasswordReset_ConfirmNewPassword_View"
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_TAP_CHANGE = "PasswordReset_Confirm_Button_Change"
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_ACTION_NO_MATCH = "PasswordReset_Confirm_Error_NotMatching"
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_ACTION_RECENT_PASSWORD = "PasswordReset_Confirm_Error_Recent"

        // Reset Password - Confirm New Password Success
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_SCREEN_VIEW = "PasswordReset_Success_View"
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_TAP_ENTER = "PasswordReset_Success_Button_Launch"
        const val RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_TAP_MAIN = "PasswordReset_Success_Button_MainScreen"
    }

    fun sendEvent(name: String) {
        analytics.logEvent(name, Bundle())
    }

    fun sendEvent(name: String, params: com.google.firebase.analytics.ktx.ParametersBuilder.() -> Unit) {
        analytics.logEvent(name, params)
    }
}
