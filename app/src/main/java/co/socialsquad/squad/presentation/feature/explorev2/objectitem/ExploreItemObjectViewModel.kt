package co.socialsquad.squad.presentation.feature.explorev2.objectitem

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepositoryNotAuth
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ObjectItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.UniqueLinkRequest
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.UniqueLinkResponse
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

@ExperimentalCoroutinesApi
class ExploreItemObjectViewModel(
        private val exploreRepository: ExploreRepository,
        private val exploreRepositoryNotAuth: ExploreRepositoryNotAuth
) : ViewModel() {

    @ExperimentalCoroutinesApi
    private val objectItem = MutableStateFlow<Resource<ObjectItem>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    private val richTextAction = MutableStateFlow<Resource<String?>>(Resource.empty())

    @ExperimentalCoroutinesApi
    private val richTextDownload = MutableStateFlow<Resource<ResponseBody?>>(Resource.empty())

    @ExperimentalCoroutinesApi
    fun objectItem(): StateFlow<Resource<ObjectItem>> {
        return objectItem
    }

    @ExperimentalCoroutinesApi
    fun richTextAction(): StateFlow<Resource<String?>> {
        return richTextAction
    }

    @ExperimentalCoroutinesApi
    fun richTextDownload(): StateFlow<Resource<ResponseBody?>> {
        return richTextDownload
    }

    @ExperimentalCoroutinesApi
    fun fetchObject(link: Link) {
        Log.i("link", Gson().toJson(link) )
        viewModelScope.launch {
            objectItem.value = Resource.loading(null)
            exploreRepository.getObject("/endpoints${link.url}")
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        objectItem.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        objectItem.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchObjectSchedule(objectId: String) {
        viewModelScope.launch {
            exploreRepository.objectSchedule(objectId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("sche", e.message, e)
                    }
                    .collect {
                        Log.i("sche", Gson().toJson(it))
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun putRichTextAction(richTextButton: Button) {
        viewModelScope.launch {
            richTextAction.value = Resource.loading(null)
            exploreRepository.putRichTextAction("/endpoints${richTextButton.link}")
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        richTextAction.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        richTextAction.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun getFileRichTextResource(fileUrl: String) {
        viewModelScope.launch {
            richTextDownload.value = Resource.loading(null)
            exploreRepositoryNotAuth.getFileRichTextResource(fileUrl)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        richTextDownload.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        richTextDownload.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
     val uniqueLink = MutableLiveData<Resource<UniqueLinkResponse>>()


    @ExperimentalCoroutinesApi
    fun generatePublicLink(uniqueLinkRequest: UniqueLinkRequest) {
        viewModelScope.launch {

            exploreRepository.generatePublicLink(uniqueLinkRequest)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        uniqueLink.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        uniqueLink.value = Resource.success(it)
                    }
        }
    }
}
