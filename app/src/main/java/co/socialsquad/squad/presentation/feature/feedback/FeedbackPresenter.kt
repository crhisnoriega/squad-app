package co.socialsquad.squad.presentation.feature.feedback

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedbackAnswer
import co.socialsquad.squad.data.entity.FeedbackAnswerQuestion
import co.socialsquad.squad.data.entity.FeedbackAnswerSection
import co.socialsquad.squad.data.repository.FeedbackRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_FEEDBACK
import co.socialsquad.squad.presentation.feature.feedback.FeedbackActivity.Companion.EXTRA_FEEDBACK_VIEW_MODEL
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FeedbackPresenter @Inject constructor(
    private val view: FeedbackView,
    private val tagWorker: TagWorker,
    private val feedbackRepository: FeedbackRepository
) {

    private val compositeDisposable = CompositeDisposable()
    private var currentSectionIndex = 0
    private var currentQuestionIndex = 0

    private val currentSection: FeedbackSectionViewModel?
        get() = feedbackViewModel.sections.getOrNull(currentSectionIndex)

    private val currentQuestion: FeedbackQuestionViewModel?
        get() = currentSection?.questions?.getOrNull(currentQuestionIndex)

    private lateinit var feedbackViewModel: FeedbackViewModel

    fun onCreate(intent: Intent) {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_FEEDBACK)
        if (intent.hasExtra(EXTRA_FEEDBACK_VIEW_MODEL)) {
            feedbackViewModel = intent.getSerializableExtra(EXTRA_FEEDBACK_VIEW_MODEL) as FeedbackViewModel

            if (currentSection != null && currentQuestion != null) {
                setSection()
                setQuestion(true)
            }
        }
    }

    fun onNextClicked() {
        currentQuestionIndex++
        view.changeBackButtonIcon()
        if (currentQuestion != null) {
            setQuestion(true)
            showFinishButton()
        } else {
            currentSectionIndex++
            currentQuestionIndex = 0
            if (currentQuestion != null) {
                setSection()
                setQuestion(true)
            } else {
                sendAnswers()
            }
        }

        if (currentQuestion?.items!![0] is FeedbackItemViewModel.FeedbackOptionItemViewModel) {
            val isAnyOptionSelected = currentQuestion?.items?.any { (it as FeedbackItemViewModel.FeedbackOptionItemViewModel).isSelected }
            if (isAnyOptionSelected == true) {
                view.enableNextButton()
            }
        }
    }

    private fun showFinishButton() {
        if (feedbackViewModel.sections.getOrNull(currentSectionIndex + 1) == null &&
            currentSection?.questions?.getOrNull(currentQuestionIndex + 1) == null
        ) {
            view.showFinishButton()
        }
    }

    private fun setSection() {
        view.setSection(currentSection!!)
    }

    private fun setQuestion(shouldDisableButton: Boolean) {
        view.setQuestion(currentQuestion!!, currentQuestionIndex + 1, currentSection!!.questions.size, shouldDisableButton)
    }

    fun onTextChanged(viewModel: FeedbackItemViewModel.FeedbackTextItemViewModel) {
        if (viewModel.text.isNotBlank()) view.enableNextButton() else view.disableNextButton()
    }

    fun onOptionSelected(viewModel: FeedbackItemViewModel.FeedbackOptionItemViewModel) {
        if (viewModel.isMultiSelect) {
            val isAnyOptionSelected = currentQuestion?.items
                ?.any { (it as FeedbackItemViewModel.FeedbackOptionItemViewModel).isSelected }
            if (isAnyOptionSelected == true) view.enableNextButton() else view.disableNextButton()
        } else {
            currentQuestion?.items
                ?.filter { (it as FeedbackItemViewModel.FeedbackOptionItemViewModel).isSelected && it != viewModel }
                ?.forEach {
                    (it as FeedbackItemViewModel.FeedbackOptionItemViewModel).isSelected = false
                    view.updateItem(it)
                }
            if (viewModel.isSelected) view.enableNextButton() else view.disableNextButton()
        }
    }

    private fun sendAnswers() {
        val feedbackAnswer = FeedbackAnswer(
            feedbackViewModel.pk,
            feedbackViewModel.sections.map {
                FeedbackAnswerSection(
                    it.pk,
                    it.questions.map {
                        FeedbackAnswerQuestion(
                            it.pk,
                            it.items.mapNotNull {
                                when {
                                    it is FeedbackItemViewModel.FeedbackTextItemViewModel -> it.text
                                    it is FeedbackItemViewModel.FeedbackOptionItemViewModel && it.isSelected -> it.pk.toString()
                                    else -> null
                                }
                            }
                        )
                    }
                )
            }
        )

        compositeDisposable.add(
            feedbackRepository.sendAnswer(feedbackAnswer)
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    { view.showMessage(R.string.feedback_message_finish) { view.close() } },
                    { it.printStackTrace() }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onCloseClicked() {
        if (currentQuestionIndex > 0) {
            currentQuestionIndex--
            if (currentQuestionIndex == 0) {
                view.changeCloseButtonIcon()
            }
            setQuestion(false)
        } else {
            view.finishView()
        }
    }
}
