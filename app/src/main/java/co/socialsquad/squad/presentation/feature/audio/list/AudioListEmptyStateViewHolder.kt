package co.socialsquad.squad.presentation.feature.audio.list

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder

class AudioListEmptyStateViewHolder(itemView: View) : ViewHolder<AudioEmptyStateViewModel>(itemView) {
    override fun bind(viewModel: AudioEmptyStateViewModel) {}
    override fun recycle() {}
}
