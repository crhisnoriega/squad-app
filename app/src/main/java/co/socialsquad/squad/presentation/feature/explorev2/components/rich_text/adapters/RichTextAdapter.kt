package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters

import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.DivisorDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.LinkDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.LoadingDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.MediaDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.ObjectDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.ResourceDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.SubtitleDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.TextDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.TitleDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextDataType
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLoading

class RichTextAdapter(listener: RichTextListener, lifecycleOwner: LifecycleOwner) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<ViewType> = arrayListOf()
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

    init {
        delegateAdapters.addAll(
            RichTextDataType.LOADING.value to LoadingDelegateAdapter(),
            RichTextDataType.TITLE.value to TitleDelegateAdapter(),
            RichTextDataType.SUBTITLE.value to SubtitleDelegateAdapter(),
            RichTextDataType.TEXT.value to TextDelegateAdapter(),
            RichTextDataType.DIVISOR.value to DivisorDelegateAdapter(),
            RichTextDataType.MEDIA.value to MediaDelegateAdapter(lifecycleOwner),
            RichTextDataType.LINK.value to LinkDelegateAdapter(listener),
            RichTextDataType.OBJECT.value to ObjectDelegateAdapter(listener),
            RichTextDataType.RESOURCE.value to ResourceDelegateAdapter(listener)
        )
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType)?.onCreateViewHolder(parent)
            ?: run { throw IllegalStateException("ViewType não conhecido, valor em questão: $viewType") }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return delegateAdapters.get(getItemViewType(position))
            ?.onBindViewHolder(holder, this.items[position])
            ?: run { throw IllegalStateException("ViewType não reconhecido") }
    }

    fun addRichText(list: List<ViewType>) {
        items.clear()
        items.addAll(list)
    }

    fun showLoading() {
        items.clear()
        items.add(RichTextLoading())
    }
}
