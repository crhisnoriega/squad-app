package co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote

import co.socialsquad.squad.domain.model.squad.SquadResponse
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleDate
import co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary.ScheduleSummary
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val PATH_TIMETABLE_ID = "timetable_id"
private const val PATH_OBJECT_ID = "object_id"
private const val QUERY_POSITION = "position"
private const val QUERY_DATE = "date"

interface ScheduleSummaryApi {
    @GET("/endpoints/timetable/{timetable_id}/object/{object_id}/reservation")
    suspend fun fetchScheduleSummary(
        @Path(PATH_TIMETABLE_ID) timetableId: Long,
        @Path(PATH_OBJECT_ID) objectId: Long
    ): SquadResponse<ScheduleSummary>

    @GET("/endpoints/timetable/{timetable_id}/object/{object_id}/date")
    suspend fun fetchScaleDate(
        @Path(PATH_TIMETABLE_ID) timetableId: Long,
        @Path(PATH_OBJECT_ID) objectId: Long,
        @Query(QUERY_POSITION) position: Long?,
        @Query(QUERY_DATE) date: String?
    ): SquadResponse<ScaleDate>
}
