package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.STORE_CATEGORY_LIST_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListItemViewModel
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_category_list.view.*

class CategoryListViewHolder(itemView: View, val listener: Listener<StoreCategoryListItemViewModel>) : ViewHolder<StoreCategoryListViewModel>(itemView) {
    private val rvCategories: RecyclerView = itemView.store_category_list_rv_categories
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreCategoryListViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreCategoryListItemViewModel -> STORE_CATEGORY_LIST_ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                STORE_CATEGORY_LIST_ITEM_VIEW_MODEL_ID -> CategoryViewHolder(view, glide, listener)
                else -> throw IllegalArgumentException()
            }
        }

        val categoriesAdapter = RecyclerViewAdapter(viewHolderFactory)
        categoriesAdapter.setItems(viewModel.categories)

        with(rvCategories) {
            adapter = categoriesAdapter
            isFocusable = false
        }
    }

    override fun recycle() {}
}
