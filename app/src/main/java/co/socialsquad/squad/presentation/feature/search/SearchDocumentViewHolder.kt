package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.net.Uri
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_documents_item.view.*

class SearchDocumentViewHolder(
    itemView: View,
    private val onDocumentClickedListener: ViewHolder.Listener<DocumentViewModel>
) : ViewHolder<DocumentViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: DocumentViewModel) {
        with(viewModel) {
            glide.load(cover)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.ivImage)

            itemView.tvTitle.text = title

            itemView.tvAuthor.apply {
                text = author
                authorPk?.let { pk -> setOnClickListener { openUser(pk) } }
            }

            val viewCountStringResId =
                if (viewCount == 1) R.string.view_count_singular
                else R.string.view_count_plural
            val views = itemView.context.getString(viewCountStringResId, TextUtils.toUnitSuffix(viewCount.toLong()))
            itemView.tvViewCountAndTimestamp.text = listOfNotNull(views, timestamp?.elapsedTime(itemView.context)).joinToString(" • ")

            fileUrl?.let { url ->
                itemView.setOnClickListener {
                    openExternalBrowser(url)
                    onDocumentClickedListener.onClick(viewModel)
                }
            }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }

    private fun openExternalBrowser(fileUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(fileUrl))
        itemView.context.startActivity(intent)
    }
}
