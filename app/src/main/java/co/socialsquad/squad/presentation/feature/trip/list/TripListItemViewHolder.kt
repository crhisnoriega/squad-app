package co.socialsquad.squad.presentation.feature.trip.list

import android.content.Intent
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.trip.TripViewModel
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_trip_list_item.view.*

class TripListItemViewHolder(itemView: View) : ViewHolder<TripViewModel>(itemView) {
    override fun bind(viewModel: TripViewModel) {
        itemView.apply {
            if (viewModel.imageUrl != null) {
                ivPlay.visibility = View.GONE
                Glide.with(context)
                    .load(viewModel.imageUrl)
                    .roundedCorners(context)
                    .crossFade()
                    .into(ivCover)
                ivCover.setOnClickListener(null)
            } else if (viewModel.videoUrl != null) {
                ivPlay.visibility = View.VISIBLE
                Glide.with(context)
                    .load(viewModel.videoThumbnailUrl)
                    .roundedCorners(context)
                    .crossFade()
                    .into(ivCover)
                ivCover.setOnClickListener { openVideo(viewModel.videoUrl) }
            }

            tvPromotionText.text = viewModel.promotionText
            viewModel.color?.let { tvPromotionText.setTextColor(ColorUtils.parse(it)) }

            tvTitle.text = viewModel.title

            tvDescription.text = viewModel.description
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivCover)
    }

    private fun openVideo(url: String) {
        val intent = Intent(itemView.context, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        itemView.context.startActivity(intent)
    }
}
