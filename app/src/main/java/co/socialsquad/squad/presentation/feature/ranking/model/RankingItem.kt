package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RankingItem(
        @SerializedName("id") val id: Int?,
        @SerializedName("ranking_header") val header: RankHeader?,
        @SerializedName("title") val title: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("media_cover") val media_cover: String?,
        @SerializedName("recurrency_type") val recurrency_type: String?,
        @SerializedName("anonymous") val anonymous: Boolean?,
        @SerializedName("ranking_positions") val positions: PositionList?,
        @SerializedName("criteria") val criteria: CriteriaList?,
        @SerializedName("benefits") val benefits: BenefitList?,
        @SerializedName("prizes") val prizes: PrizeList?,
        @SerializedName("terms") val terms: RankingTerms?,
        @SerializedName("user_in_ranking") val user_in_rank: Boolean,

        var first: Boolean = true
) : Parcelable{
}