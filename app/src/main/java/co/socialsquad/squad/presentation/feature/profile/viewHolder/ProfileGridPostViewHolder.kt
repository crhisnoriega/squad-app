package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_card_grid_post_profile.view.*


const val PROFILE_GRID_POST_VIEW_HOLDER_ID = R.layout.item_card_grid_post_profile

class ProfileGridPostViewHolder(itemView: View) : ViewHolder<ProfileGridPostViewModel>(itemView) {

    override fun recycle() {
    }

    override fun bind(viewModel: ProfileGridPostViewModel) {
        Glide.with(itemView.context).load(viewModel.url).centerCrop().into(itemView.imagePost)
    }
}
