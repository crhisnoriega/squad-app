package co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.ConfirmResetPasswordRequest
import co.socialsquad.squad.data.entity.RecipientsRequest
import co.socialsquad.squad.data.entity.SigninRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.util.Analytics
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ConfirmResetPasswordPresenter @Inject constructor(
    private val view: ConfirmResetPasswordView,
    private val loginRepository: LoginRepository,
    private val analytics: Analytics
) {

    val compositeDisposable = CompositeDisposable()

    fun resetPassword(password: String, confirmPassword: String, subdomain: String, token: String) {

        analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_TAP_CHANGE)

        if (password.equals(confirmPassword)) {
            compositeDisposable.add(
                loginRepository.confirmResetPassword(ConfirmResetPasswordRequest(token, password), subdomain)
                    .doOnSubscribe { view.showLoading() }
                    .doOnTerminate { view.hideLoading() }
                    .subscribe(
                        {
                            if (it.status == "OK") {
                                view.showSuccessDialog()
                            }
                        },
                        {
                            it.printStackTrace()
                            view.showMessage(R.string.reset_password_same_pass_error)
                            analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_ACTION_RECENT_PASSWORD)
                        }
                    )
            )
        } else {
            analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_ACTION_NO_MATCH)
            view.showMessage(R.string.reset_password_error)
        }
    }

    fun onCreate() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_SCREEN_VIEW)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun doLogin(email: String, password: String, subdomain: String) {

        loginRepository.subdomain = subdomain

        val request = SigninRequest(email.toLowerCase(), password)

        compositeDisposable.add(
            loginRepository.signin(request)
                .flatMap<String?> {
                    loginRepository.squadColor = it.company.primaryColor
                    loginRepository.userCompany = it
                    // loginRepository.token = it.token
                    loginRepository.getDeviceToken()
                }
                .flatMapCompletable {
                    loginRepository.registerDevice(RecipientsRequest(it, RecipientsRequest.PLATFORM))
                }
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        view.hideLoading()

                        if (loginRepository.userCompany != null && loginRepository.userCompany!!.user != null && !loginRepository.userCompany!!.user.isPerfilComplete) {
                            view.openProfileScreen()
                            return@subscribe
                        }

                        view.openNavigation()
                    },
                    {
                        it.printStackTrace()

                        loginRepository.subdomain = null
                        loginRepository.token = null
                    }
                )
        )
    }

    fun onShowSuccessDialog() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_SCREEN_VIEW)
    }

    fun onEnterAutomatic() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_TAP_ENTER)
    }

    fun onBackToHome() {
        analytics.sendEvent(Analytics.RESET_PASSWORD_CONFIRM_NEW_PASS_SUCCESS_TAP_MAIN)
    }
}
