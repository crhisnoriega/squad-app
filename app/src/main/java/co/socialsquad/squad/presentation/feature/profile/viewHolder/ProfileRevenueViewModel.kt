package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileRevenueViewModel(val title: String, val subtitle:String) : ViewModel
