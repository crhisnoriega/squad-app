package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ObjectTemplateDataType(val value: Int) : Parcelable {
    @SerializedName("default")
    DEFAULT(0), // default == rectangle_horizontal

    @SerializedName("rectangle_horizontal")
    RECTANGLE_HORIZONTAL(0),

    @SerializedName("rectangle_vertical")
    RECTANGLE_VERTICAL(1),

    @SerializedName("square")
    SQUARE(2),

    @SerializedName("circle")
    CIRCLE(3)
}
