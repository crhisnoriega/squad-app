package co.socialsquad.squad.presentation.feature.points.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_points_area.view.*

class ItemAreaPointsViewHolder(itemView: View, var onClickAction: ((item: ItemAreaPointsViewModel) -> Unit)) : ViewHolder<ItemAreaPointsViewModel>(itemView) {
    override fun bind(viewModel: ItemAreaPointsViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.areaData.title
        itemView.txtSubtitle.text = viewModel.areaData.description

        Glide.with(itemView.context).load(viewModel.areaData.icon).into(itemView.imgIcon)
    }

    override fun recycle() {

    }

}