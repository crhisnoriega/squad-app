package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichTextVO(
    var type: RichTextDataType? = null,
    var title: String? = null,
    var subtitle: String? = null,
    var text: String? = null,
    var link: RichTextLinks? = null,
    var resource: RichTextResources? = null,
    @SerializedName("object")
    var objectType: RichTextObjects? = null,
    var media: List<Media>? = null
) : Parcelable
