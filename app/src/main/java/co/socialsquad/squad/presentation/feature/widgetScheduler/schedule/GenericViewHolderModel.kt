package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel

class GenericViewHolder(itemView: View) : ViewHolder<GenericViewHolderModel>(itemView) {
    override fun bind(viewModel: GenericViewHolderModel) {
    }

    override fun recycle() {
    }
}

class GenericViewHolderModel() : ViewModel
