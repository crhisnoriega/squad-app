package co.socialsquad.squad.presentation.feature.searchv2.results.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder.SearchEmptyViewHolder

class SearchEmptyAdapter(
    private val emptyState: EmptyState?
) :
    RecyclerView.Adapter<SearchEmptyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchEmptyViewHolder {
        return SearchEmptyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_searchv2_empty_state, parent, false),
            parent.context
        )
    }

    override fun onBindViewHolder(holder: SearchEmptyViewHolder, position: Int) =
        holder.bind(emptyState)

    override fun getItemCount() = 1
}
