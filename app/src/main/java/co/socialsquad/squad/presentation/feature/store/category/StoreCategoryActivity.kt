package co.socialsquad.squad.presentation.feature.store.category

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.EXTRA_CATEGORY
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ProductListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListItemViewModel
import co.socialsquad.squad.presentation.feature.store.products.EXTRA_PRODUCTS_TYPE
import co.socialsquad.squad.presentation.feature.store.products.EXTRA_PRODUCTS_VIEW_MODEL
import co.socialsquad.squad.presentation.feature.store.products.ProductsActivity
import co.socialsquad.squad.presentation.feature.store.products.ProductsPresenter
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ItemViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_store_category.*
import javax.inject.Inject

class StoreCategoryActivity : BaseDaggerActivity(), StoreCategoryView {

    @Inject
    lateinit var storeCategoryPresenter: StoreCategoryPresenter

    private lateinit var ivCover: ImageView
    private lateinit var srlRefresh: SwipeRefreshLayout
    private lateinit var rvSubcategories: RecyclerView
    private lateinit var llLoading: LinearLayout
    private lateinit var adapter: RecyclerViewAdapter

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        storeCategoryPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_category)

        ivCover = store_category_iv_cover
        rvSubcategories = store_category_rv_subcategories
        srlRefresh = store_category_srl_refresh
        llLoading = store_category_ll_loading as LinearLayout

        storeCategoryPresenter.onCreate(intent)
    }

    override fun setupToolbar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = title
    }

    override fun setupCover(image: String) {
        ivCover.visibility = View.VISIBLE
        Glide.with(this)
            .load(image)
            .centerCrop()
            .crossFade()
            .into(ivCover)
    }

    override fun setupList() {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreCategoryListItemViewModel -> ITEM_VIEW_MODEL_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ITEM_VIEW_MODEL_ID -> ItemViewHolder(view, onCategoryClicked)
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        }

        adapter = RecyclerViewAdapter(viewHolderFactory)
        with(rvSubcategories) {
            adapter = this@StoreCategoryActivity.adapter
            addItemDecoration(ListDividerItemDecoration(context))
            setItemViewCacheSize(3)
            setHasFixedSize(false)
            isFocusable = false
        }
    }

    val onCategoryClicked = object : ViewHolder.Listener<StoreCategoryListItemViewModel> {
        override fun onClick(viewModel: StoreCategoryListItemViewModel) {
            storeCategoryPresenter.onSubcategoryClicked(viewModel)
        }
    }

    override fun setupSwipeRefresh() {
        srlRefresh.isEnabled = false
        srlRefresh.setOnRefreshListener { storeCategoryPresenter.onRefresh() }
    }

    override fun setItems(categories: List<ViewModel>) {
        adapter.setItems(categories)
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(this, getString(resId), null)
    }

    override fun showLoading() {
        llLoading.visibility = View.VISIBLE
        srlRefresh.visibility = View.GONE
    }

    override fun hideLoading() {
        llLoading.visibility = View.GONE
        srlRefresh.visibility = View.VISIBLE
    }

    override fun openCategory(category: StoreCategoryListItemViewModel) {
        val intent = Intent(this, StoreCategoryActivity::class.java)
        intent.putExtra(EXTRA_CATEGORY, category)
        startActivity(intent)
    }

    override fun openProducts(productListViewModel: ProductListViewModel) {
        val intent = Intent(this, ProductsActivity::class.java)
        intent.putExtra(EXTRA_PRODUCTS_TYPE, ProductsPresenter.Type.CATEGORY)
        intent.putExtra(EXTRA_PRODUCTS_VIEW_MODEL, productListViewModel)
        startActivity(intent)
    }
}
