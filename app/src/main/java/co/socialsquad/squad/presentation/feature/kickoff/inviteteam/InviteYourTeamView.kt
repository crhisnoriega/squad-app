package co.socialsquad.squad.presentation.feature.kickoff.inviteteam

interface InviteYourTeamView {

    fun showLoading()
    fun hideLoading()
    fun showErrorMessage()
    fun moveToNextScreen()
}
