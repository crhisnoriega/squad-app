package co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote

import co.socialsquad.squad.domain.model.squad.SquadResponse
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreationResponse
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

private const val PATH_TIMETABLE_ID = "timetable_id"
private const val PATH_OBJECT_ID = "object_id"

interface TimetableValidationAPI {

    @POST("endpoints/timetable/{timetable_id}/object/{object_id}/reservation")
    suspend fun createTimetableSubmission(
        @Path(PATH_TIMETABLE_ID) timetableId: Long,
        @Path(PATH_OBJECT_ID) objectId: Long,
        @Body timetableCreation: TimetableCreation
    ): SquadResponse<TimetableCreationResponse>
}