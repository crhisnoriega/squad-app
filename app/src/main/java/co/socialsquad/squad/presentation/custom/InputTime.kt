package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.app.TimePickerDialog
import android.content.Context
import android.text.format.DateFormat
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.hideKeyboard
import kotlinx.android.synthetic.main.view_input_time.view.*
import java.util.Calendar
import java.util.Date

class InputTime(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    private var onInputListener: InputView.OnInputListener? = null
    private val labelAnimator = ObjectAnimator
        .ofFloat(12f, 11f)
        .apply {
            duration = 200L
            addUpdateListener { tvLabel.textSize = it.animatedValue as Float }
        }
    var calendar: Calendar? = null
        set(value) {
            field = value?.apply {
                showTime(this)
                date = this.time
            }
        }

    var date: Date? = null

    init {
        View.inflate(context, R.layout.view_input_time, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputTime)) {
            tvLabel.text = getString(R.styleable.InputTime_hint)
            recycle()
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener {
            if (calendar == null) {
                calendar = Calendar.getInstance()
            }
            timePickerDialog?.apply {
                show()
                hideKeyboard()
            }
        }
    }

    private val timePickerListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        calendar?.let {
            it.set(Calendar.HOUR_OF_DAY, hourOfDay)
            it.set(Calendar.MINUTE, minute)
            date = it.time
            showTime(it)
            onInputListener?.onInput()
        }
    }
    private var timePickerDialog: TimePickerDialog? = null
        get() {
            val calendar = calendar ?: Calendar.getInstance()
            return TimePickerDialog(
                context,
                timePickerListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            )
        }

    private fun showTime(calendar: Calendar) {
        tvTime.text = DateFormat.getTimeFormat(context).format(calendar.time.time)
        if (tvTime.visibility == View.GONE) {
            labelAnimator.start()
            tvTime.visibility = View.VISIBLE
        }
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    override fun isValid(valid: Boolean) {}
}
