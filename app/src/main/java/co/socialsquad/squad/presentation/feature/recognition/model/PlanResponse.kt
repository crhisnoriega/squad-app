package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.ranking.model.RankingTerms
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PlanResponse(
        @SerializedName("recognition_identification") val recognition_identification: String?,
        @SerializedName("data") val plans: List<PlanData>?,
        @SerializedName("terms") val terms:RankingTerms,
        var first: Boolean = false
) : Parcelable