package co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter

import co.socialsquad.squad.domain.model.Term

class TermVM(
    val term: Term,
    var checked: Boolean = false
)