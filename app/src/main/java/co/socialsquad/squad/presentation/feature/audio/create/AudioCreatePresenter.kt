package co.socialsquad.squad.presentation.feature.audio.create

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.ScheduledRequest
import co.socialsquad.squad.data.entity.SegmentationQueryRequest
import co.socialsquad.squad.data.entity.SegmentationRequest
import co.socialsquad.squad.data.entity.request.AudioRequest
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.ProgressRequestBody
import co.socialsquad.squad.data.repository.AudioRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_API_RESPONSE_DROP_UPLOAD
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.getDuration
import co.socialsquad.squad.presentation.util.toUTC
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.util.Date
import javax.inject.Inject

class AudioCreatePresenter @Inject constructor(
    private val context: Context,
    loginRepository: LoginRepository,
    private val audioCreateView: AudioCreateView,
    private val tagWorker: TagWorker,
    private val audioRepository: AudioRepository,
    private val contentResolver: ContentResolver
) {
    private val compositeDisposable = CompositeDisposable()

    private var fileUri: Uri? = null
    private var file: File? = null
    private var originalFileUri: Uri? = null
    private var userName = with(loginRepository.userCompany?.user) { listOfNotNull(this?.firstName, this?.lastName).joinToString(" ") }
    private var userAvatar = loginRepository.userCompany?.user?.avatar
    private var color = loginRepository.squadColor
    private val subdomain = loginRepository.subdomain

    fun onCreate(intent: Intent?) {
        intent?.let { int ->
            when {
                int.hasExtra(AudioCreateActivity.EXTRA_AUDIO_GALLERY_FILE_URI) -> {
                    originalFileUri = Uri.parse(int.getStringExtra(AudioCreateActivity.EXTRA_AUDIO_GALLERY_FILE_URI))
                        ?: return
                    val mimeType = contentResolver.getType(originalFileUri!!) ?: return
                    file = audioRepository.createGalleryFile(mimeType)
                    fileUri = Uri.fromFile(file)
                    audioCreateView.showPreparingFile()
                    Completable
                        .fromAction {
                            val inputStream = contentResolver.openInputStream(originalFileUri!!)
                            FileUtils.createFromInputStream(file!!, inputStream!!)
                        }
                        .onDefaultSchedulers()
                        .andThen(Observable.just(createViewModel()))
                        .subscribe(
                            {
                                it.duration = MediaMetadataRetriever().getDuration(context, fileUri!!)
                                audioCreateView.setupPlayer(it)
                                audioCreateView.hideProgress()
                            },
                            {
                                fileUri = null
                                audioCreateView.showError(R.string.audio_create_fail_to_prepare_audio)
                            }
                        )
                }
                int.hasExtra(AudioCreateActivity.EXTRA_AUDIO_RECORDED_FILE_URI) -> {
                    fileUri = Uri.parse(int.getStringExtra(AudioCreateActivity.EXTRA_AUDIO_RECORDED_FILE_URI))
                        ?: return
                    val audioViewModel = createViewModel()
                    audioViewModel.duration = MediaMetadataRetriever().getDuration(context, fileUri!!)
                    audioCreateView.setupPlayer(audioViewModel)
                }
                else -> {
                    audioCreateView.showOpeningError(R.string.audio_create_open_file_error)
                }
            }
        }
        audioCreateView.setupAudienceColor(color)
    }

    private fun createViewModel() = AudioViewModel(
        0,
        "",
        "",
        fileUri.toString(),
        userName,
        -1,
        "",
        userAvatar,
        "",
        "",
        "",
        color,
        0,
        false,
        null,
        0,
        userAvatar,
        0,
        true,
        false,
        false,
        false,
        false,
        subdomain
    )

    fun onInput(title: String?) {
        val conditionsMet = !title.isNullOrBlank() && fileUri != null
        audioCreateView.setSaveButtonEnabled(conditionsMet)
    }

    fun onSaveClicked(
        title: String?,
        description: String?,
        metrics: MetricsViewModel?,
        members: IntArray?,
        downline: Boolean,
        date: Long?
    ) {
        if (fileUri == null) {
            return
        }

        val segmentationRequest = when {
            metrics != null && metrics.selected.isNotEmpty() -> {
                val queries = metrics.selected.map { SegmentationQueryRequest(it.field, it.values.map { it.value }) }
                SegmentationRequest(queries = queries)
            }
            members != null && members.isNotEmpty() -> SegmentationRequest(members = members.toList())
            downline -> SegmentationRequest(downline = true)
            else -> null
        }

        val schedule = date?.let { ScheduledRequest(Date(it).toUTC()) }

        val audioRequest = AudioRequest(title, description, segmentationRequest, schedule)

        compositeDisposable.add(
            audioRepository.createAudio(audioRequest)
                .doOnSubscribe { audioCreateView.showProgress() }
                .flatMapCompletable { audioRepository.sendAudioFile(it.pk, fileUri!!, percentageListener) }
                .subscribe(
                    {
                        val params = Bundle().apply {
                            putBoolean("success", true)
                        }
                        tagWorker.tagEvent(TAG_API_RESPONSE_DROP_UPLOAD, params)
                        audioRepository.deleteGalleryFile()
                        audioCreateView.hideProgress()
                        audioCreateView.uploadSuccess()
                    },
                    {
                        val params = Bundle().apply {
                            putBoolean("success", false)
                        }
                        tagWorker.tagEvent(TAG_API_RESPONSE_DROP_UPLOAD, params)
                        it.printStackTrace()
                        audioCreateView.showError(R.string.audio_create_fail_to_upload)
                        audioCreateView.hideProgress()
                    }
                )
        )
    }

    private val percentageListener = object : ProgressRequestBody.UploadCallbacks {
        override fun onProgressUpdate(percentage: Int) {
            audioCreateView.updateUploadStatus(percentage)
        }
    }
}
