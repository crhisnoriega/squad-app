package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_action_button.view.*

class WhiteActionButton : LinearLayout {

    var onClickListener: () -> Unit = {}

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        View.inflate(
            context,
            R.layout.view_action_button_white, this
        )
        context?.obtainStyledAttributes(
            attrs,
            R.styleable.SquadButton
        )?.apply {
            try {
                text = getString(R.styleable.SquadButton_android_text)
                isActive = getBoolean(R.styleable.SquadButton_android_enabled, true)
                icon = getDrawable(R.styleable.SquadButton_android_drawable)
                textColor = getResourceId(
                    R.styleable.SquadButton_android_textColor,
                    R.color.white
                )
                backgroundActionColor = getResourceId(
                    R.styleable.SquadButton_android_background,
                    R.color.white
                )
                loadingColor = getResourceId(
                    R.styleable.SquadButton_loadingColor,
                    R.color.white
                )

                nestedScroll.setOnClickListener {
                    if (isActive) onClickListener.invoke()
                }
            } finally {
                recycle()
            }
        }
    }

    var text: String? = null
        set(value) {
            field = value
            tvText.text = value
        }

    var isActive: Boolean = true
        set(value) {
            field = value
            nestedScroll.isClickable = value
            nestedScroll.isEnabled = value
        }

    var icon: Drawable? = null
        set(value) {
            field = value
            icon?.let {
                ivIcon.setImageDrawable(value)
                ivIcon.visibility = View.VISIBLE
            }
        }

    var textColor: Int? = null
        set(value) {
            field = value?.let {
                tvText.setTextColor(ContextCompat.getColor(context, it))
                it
            }
        }

    var backgroundActionColor: Int = R.drawable.selector_action_button
        set(value) {
            field = value.let {
                nestedScroll.background = ContextCompat.getDrawable(context, it)
                it
            }
        }

    var loadingColor: Int = android.R.color.white
        set(value) {
            field = value?.let { color ->
                ivLoading.imageTintList = ColorStateList.valueOf(color)
                color
            }
        }

    fun enable(enable: Boolean) {
        nestedScroll.isClickable = enable
        nestedScroll.isEnabled = enable
        isActive = enable
    }

    fun setDrawable(drawable: Drawable) {
        val bitmapDrawable = (drawable as BitmapDrawable).bitmap

        val heightWidth =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25F, resources.displayMetrics)
                .toInt()
        val resultDrawable = BitmapDrawable(
            resources,
            Bitmap.createScaledBitmap(bitmapDrawable, heightWidth, heightWidth, true)
        )

        ivIcon.setImageDrawable(resultDrawable)
        invalidate()
    }

    fun loading(isLoading: Boolean) {
        ivLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
        tvText.visibility = if (isLoading) View.GONE else View.VISIBLE
        ivIcon.visibility = if (isLoading) View.GONE else View.VISIBLE
        if (isLoading) {
            val rotation = RotateAnimation(
                0.0f,
                360.0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            rotation.startOffset = 0
            rotation.duration = 1000
            rotation.repeatCount = Animation.INFINITE
            rotation.setInterpolator(context, android.R.anim.linear_interpolator)
            ivLoading.startAnimation(rotation)
        } else {
            ivLoading.clearAnimation()
        }
    }
}
