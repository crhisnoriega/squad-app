package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.explore.Contact
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.toDips

class ContactSingleViewHolder(
    itemView: View,
    private val largeIcon: Boolean = false,
    var callbackOptions: (contact: Contact) -> Unit
) : ViewHolder<ContactSingleViewHolderModel>
    (itemView) {

    private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
    private val tvSubtitle: TextView = itemView.findViewById(R.id.tvSubtitle)
    private val tvSubSubtitle: TextView = itemView.findViewById(R.id.tvSubSubtitle)
    private val ivListIcon: ImageView = itemView.findViewById(R.id.ivListIcon)
    private val ivInformationIconOptions: ImageButton = itemView.findViewById(
        R.id
            .ivInformationIconOptions
    )
    private val ivInformationIconDetail: ImageButton =
        itemView.findViewById(R.id.ivInformationIconDetail)

    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_contact_single
    }

    override fun bind(viewModel: ContactSingleViewHolderModel) {

        if(largeIcon){
            val layoutParams: ConstraintLayout.LayoutParams = ivListIcon.layoutParams as ConstraintLayout.LayoutParams
            layoutParams.height = 44f.toDips(itemView.context.resources.displayMetrics).toInt()
            layoutParams.width = 44f.toDips(itemView.context.resources.displayMetrics).toInt()
            layoutParams.setMargins(
                16f.toDips(itemView.context.resources.displayMetrics).toInt(),
                0,
                0,
                16f.toDips(itemView.context.resources.displayMetrics).toInt())
            ivListIcon.layoutParams = layoutParams

            val layoutParamsTitle: LinearLayout.LayoutParams = tvTitle.layoutParams as LinearLayout.LayoutParams
            layoutParamsTitle.setMargins(
                12f.toDips(itemView.context.resources.displayMetrics).toInt(),
                16f.toDips(itemView.context.resources.displayMetrics).toInt(),
                0,
                0)
            tvTitle.layoutParams = layoutParamsTitle
        }

        tvSubSubtitle.isVisible = false
        ivInformationIconOptions.isVisible = false
        ivInformationIconDetail.isVisible = false
        ivInformationIconOptions.setOnClickListener {
            callbackOptions.invoke(viewModel.contact)
        }
        ivInformationIconDetail.setOnClickListener {
            ContactListActivity.start(itemView.context, viewModel.contact)
        }

        tvTitle.text = viewModel.contact.title

        if (viewModel.contact.items?.email != null && viewModel.contact.items?.phone != null) {
            tvSubtitle.text = viewModel.contact.subtitle
            ivListIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_complements_overview_contacts))
            ivInformationIconDetail.isVisible = true
        } else {
            if (viewModel.contact.phone != null && viewModel.contact.email != null) {
                tvSubtitle.text = viewModel.contact.phone
                tvSubSubtitle.text = viewModel.contact.email
                ivListIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_complements_overview_contacts))
                tvSubSubtitle.isVisible = true
                ivInformationIconOptions.isVisible = true
            } else {
                if (viewModel.contact.phone != null) {
                    ivListIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_complements_overview_contacts_phone_number))
                    tvSubtitle.text = viewModel.contact.phone
                    ivInformationIconOptions.isVisible = true
                }

                if (viewModel.contact.email != null) {
                    ivListIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_complements_overview_contacts_e_mail_address))
                    tvSubtitle.text = viewModel.contact.email
                    ivInformationIconOptions.isVisible = true
                }
            }
        }
    }

    override fun recycle() {
    }
}