package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder

class ListHeader(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        View.inflate(context, R.layout.view_list_header, this)
        val viewHolder = HeaderViewHolder(this)
        with(context.obtainStyledAttributes(attrs, R.styleable.ListHeader)) {
            val title = getString(R.styleable.ListHeader_title)
            val subtitle = getString(R.styleable.ListHeader_subtitle)
            recycle()

            val viewModel = HeaderViewModel(title, subtitle)
            viewHolder.bind(viewModel)
        }
    }
}
