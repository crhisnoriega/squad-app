package co.socialsquad.squad.presentation.util

import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.install.InstallState
import java.lang.Exception

interface InAppUpdateListener {
	fun onUpdateState(state: InstallState)
	fun onError(exception: Exception)
	fun onAppUpdateInfo(appUpdateInfo: AppUpdateInfo)
	fun nothingToUpdate(updateAvailability: Int)

}