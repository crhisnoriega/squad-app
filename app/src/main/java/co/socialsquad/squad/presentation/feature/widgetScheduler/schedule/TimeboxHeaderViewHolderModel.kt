package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.item_widget_scheduler_timebox_header.view.*
import java.text.SimpleDateFormat
import java.util.Date

class TimeboxHeaderViewHolder(itemView: View) : ViewHolder<TimeboxHeaderViewHolderModel>(itemView) {
    private val formatter = SimpleDateFormat("HH:mm")

    override fun bind(viewModel: TimeboxHeaderViewHolderModel) {
        itemView.time.text = formatter.format(viewModel.startDate)
    }

    override fun recycle() {
    }
}

class TimeboxHeaderViewHolderModel(internal val startDate: Date) : ViewModel
