package co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Information
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.DividerViewHolder
import co.socialsquad.squad.presentation.custom.ITEM_DIVIDER
import co.socialsquad.squad.presentation.custom.ItemDivider
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.OuterDivider
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.opportunity.detail.OpportunitySubmissionDetailActivity
import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityInfoActivity
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListMapper
import co.socialsquad.squad.presentation.util.extra
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_show_recommendations.*
import javax.inject.Inject

class ShowAllOpportunitySubmissionsActivity :
    AppCompatActivity(),
    ShowAllOpportunitySubmissionsView {

    private var colors: OpportunityInitialsColors? = null

    @Inject
    lateinit var presenter: ShowAllOpportunitySubmissionsPresenter

    @Inject
    lateinit var loginRepository: LoginRepository

    private val opportunity: Opportunity by extra(EXTRA_OPPORTUNITY)
    private val objectType: OpportunityObjectType by extra(EXTRA_OBJECT_TYPE)

    companion object {
        private const val EXTRA_OPPORTUNITY = "opportunity"
        private const val EXTRA_OBJECT_TYPE = "object_type"
        fun newIntent(
            context: Context,
            opportunity: Opportunity,
            objectType: OpportunityObjectType
        ): Intent {
            return Intent(
                context,
                ShowAllOpportunitySubmissionsActivity::class.java
            ).apply {
                putExtra(EXTRA_OPPORTUNITY, opportunity)
                putExtra(EXTRA_OBJECT_TYPE, objectType)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_recommendations)
        back.setOnClickListener { finish() }
        setupHeader()
        setupList()
    }

    private fun setupHeader() {
        tvTitle.setText(opportunity.opportunityList.header.title)
        // TODO dynamic subtitle

        // Pegar list_count

        tvMessage.setText(
            getString(
                R.string.show_recommendations_subheader,
                opportunity.opportunityList.opportunityObjects.size
            )
        )
    }

    override fun showLoading(show: Boolean) {
        progressOverlay.show(show)
    }

    override fun onOpportunityDetailsClick(submissionDetails: SubmissionDetails) {
        this.startActivity(
            OpportunitySubmissionDetailActivity.newIntent(
                this,
                submissionDetails,
                colors,
                opportunity.title
            )
        )
    }

    override fun onOpportunityInfoClick(info: Information) {
        this.startActivity(OpportunityInfoActivity.newIntent(this, info, colors!!))
    }

    private fun setupList() {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ItemOpportunityViewModel -> ITEM_OPPORTUNITY_VIEW_MODEL_ID
                is ItemDivider -> ITEM_DIVIDER
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ITEM_OPPORTUNITY_VIEW_MODEL_ID -> OpportunitySubmissionViewHolder(view)
                ITEM_DIVIDER -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        }

        val company = loginRepository.userCompany?.company
        colors = OpportunityInitialsColors(company?.primaryColor, company?.secondaryColor)

        with(rvRecommendations) {
            layoutManager = LinearLayoutManager(context)
            adapter = RecyclerViewAdapter(viewHolderFactory).apply {
                val items = opportunity.opportunityList.opportunityObjects
                    .map {
                        OpportunityListMapper(
                            context,
                            presenter,
                            opportunity.id,
                            opportunity.parentObjectId,
                            objectType,
                            colors!!
                        ).mapOpportunityObject(it)
                    } + listOf(OuterDivider())
                addItems(items)
            }
            setHasFixedSize(true)
            addItemDecoration(ListDividerItemDecoration(context))
        }
    }
}
