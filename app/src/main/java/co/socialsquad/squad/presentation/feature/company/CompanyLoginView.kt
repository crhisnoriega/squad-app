package co.socialsquad.squad.presentation.feature.company

interface CompanyLoginView {

    fun onInput()
    fun setSaveButtonEnabled(enabled: Boolean)
    fun showLoading()
    fun hideLoading()
    fun closeScreen()
    fun showErrorMessageFailedToRegisterUser()
    fun fillIdField(id: String)
    fun setResultOK()
}
