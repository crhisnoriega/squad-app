package co.socialsquad.squad.presentation.feature.live.creator

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Comment
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.Participant
import co.socialsquad.squad.data.entity.StartLiveRequest
import co.socialsquad.squad.data.entity.Stats
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.presentation.util.toDate
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LiveCreatorPresenter @Inject constructor(
    private val liveCreatorView: LiveCreatorView,
    private val liveRepository: LiveRepository,
    private val mixpanel: MixpanelAPI
) {
    private val compositeDisposable = CompositeDisposable()

    private var liveCreateRequest: LiveCreateRequest? = null

    private var pk: Int? = null
    private var rtmpUrl: String? = null
    private var commentQueue: String? = null
    private var startedAt: Long? = null
    private var startScheduled: Boolean? = null

    private var isLiveCreated = false
    private var isStreaming = false

    fun onCreate(intent: Intent) {
        liveCreatorView.keepScreenOn(true)

        when {
            intent.hasExtra(LiveCreatorActivity.EXTRA_LIVE_CREATE_REQUEST) -> {
                liveCreateRequest = intent.getSerializableExtra(LiveCreatorActivity.EXTRA_LIVE_CREATE_REQUEST) as LiveCreateRequest
            }
            intent.hasExtra(LiveCreatorActivity.EXTRA_LIVE_START_SCHEDULED) -> {
                startScheduled = intent.getBooleanExtra(LiveCreatorActivity.EXTRA_LIVE_START_SCHEDULED, false)
                pk = intent.getIntExtra(LiveCreatorActivity.EXTRA_PK, -1)
                commentQueue = intent.getStringExtra(LiveCreatorActivity.EXTRA_COMMENT_QUEUE)
                liveRepository.livePk = pk
                isLiveCreated = true
            }
            intent.hasExtra(LiveCreatorActivity.EXTRA_PK) -> {
                pk = intent.getIntExtra(LiveCreatorActivity.EXTRA_PK, -1)
                rtmpUrl = intent.getStringExtra(LiveCreatorActivity.EXTRA_RTMP_URL)
                commentQueue = intent.getStringExtra(LiveCreatorActivity.EXTRA_COMMENT_QUEUE)
                startedAt = intent.getStringExtra(LiveCreatorActivity.EXTRA_STARTED_AT)?.toDate()?.time
            }
        }
    }

    fun onConnected() {
        isStreaming = true
        startedAt = startedAt ?: System.currentTimeMillis()
        startedAt?.let { liveCreatorView.showStreamingState(it) }
    }

    fun onConnectionFailed() {
        if (isStreaming) with(liveCreatorView) {
            showConnectingState()
            rtmpUrl?.let { liveCreatorView.startStream(it) }
        }
    }

    fun onCameraPrepared(width: Int, height: Int) {
        if (!isLiveCreated) {
            isLiveCreated = true
            liveCreateRequest?.apply {
                aspectRatioWidth = width
                aspectRatioHeight = height
                createLive(this)
            }
        }
        startScheduled?.let {
            if (it && pk != null) {
                startScheduledLive(StartLiveRequest(pk!!, width, height))
            }
        }
    }

    fun onResume() {
        pk?.let { _ ->
            if (startScheduled != true) {
                isLiveCreated = true
                with(liveCreatorView) {
                    showConnectingState()
                    rtmpUrl?.let { startStream(it) }
                    commentQueue?.let { connectQueue(it) }
                }
            }
        }
    }

    private fun createLive(liveCreateRequest: LiveCreateRequest) {
        compositeDisposable.add(
            liveRepository.createLive(liveCreateRequest)
                .subscribe(
                    {
                        mixpanel.track("Social / Create / Live / Create / Success")
                        with(it) {
                            liveRepository.livePk = pk
                            this@LiveCreatorPresenter.pk = pk
                            this@LiveCreatorPresenter.rtmpUrl = rtmpUrl
                            this@LiveCreatorPresenter.commentQueue = commentQueue
                            liveCreatorView.startStream(rtmpUrl)
                            connectQueue(commentQueue ?: "")
                        }
                    },
                    { throwable ->
                        mixpanel.track("Social / Create / Live / Create / Failure")
                        isLiveCreated = false
                        throwable.printStackTrace()
                        liveCreatorView.showMessage(R.string.live_creator_error_failed_to_create_live, DialogInterface.OnDismissListener { liveCreatorView.close() })
                    }
                )
        )
    }

    private fun startScheduledLive(request: StartLiveRequest) {
        compositeDisposable.add(
            liveRepository.startLive(request)
                .subscribe(
                    {
                        mixpanel.track("Social / Create / Live / Create / Success")
                        with(it) {
                            this@LiveCreatorPresenter.pk = pk
                            this@LiveCreatorPresenter.rtmpUrl = rtmpUrl
                            if (!commentQueue.isNullOrEmpty()) {
                                this@LiveCreatorPresenter.commentQueue = commentQueue
                            }
                            liveCreatorView.startStream(rtmpUrl ?: "")
                            connectQueue(this@LiveCreatorPresenter.commentQueue ?: "")
                        }
                    },
                    { throwable ->
                        mixpanel.track("Social / Create / Live / Create / Failure")
                        isLiveCreated = false
                        throwable.printStackTrace()
                        liveCreatorView.showMessage(R.string.live_creator_error_failed_to_create_live, DialogInterface.OnDismissListener { liveCreatorView.close() })
                    }
                )
        )
    }

    private fun connectQueue(commentQueue: String) {
        compositeDisposable.add(
            liveRepository.connectQueue(commentQueue)
                .subscribe(
                    {
                        when (it) {
                            is Comment, is Participant -> liveCreatorView.addQueueItem(it)
                            is Stats -> if (isStreaming) liveCreatorView.setParticipantCount(it.participants)
                        }
                    },
                    {
                        it.printStackTrace()
                        liveCreatorView.showMessage(R.string.live_error_queue_connection)
                    }
                )
        )
    }

    fun onCloseClicked() {
        if (isStreaming) {
            liveCreatorView.keepScreenOn(false)
            stopStreaming()
        } else {
            liveCreatorView.showCloseConfirmationDialog()
        }
    }

    fun onCloseConfirmationClicked() {
        liveCreatorView.keepScreenOn(false)
        if (isStreaming) {
            stopStreaming()
        } else {
            liveCreatorView.close()
        }
    }

    private fun stopStreaming() {
        isStreaming = false
        liveRepository.disconnectQueue()
        liveCreatorView.showFinishedState()
        pk?.let { compositeDisposable.add(liveRepository.endLive(it).subscribe({}, Throwable::printStackTrace)) }
    }

    fun onPostClicked() {
        pk?.let {
            compositeDisposable.add(
                liveRepository.postLive(it)
                    .doOnSubscribe { _ -> liveCreatorView.showLoading() }
                    .doOnTerminate { liveCreatorView.hideLoading() }
                    .subscribe(
                        { liveCreatorView.close() },
                        { throwable ->
                            throwable.printStackTrace()
                            liveCreatorView.showMessage(R.string.live_creator_error_failed_to_post_live)
                        }
                    )
            )
        }
    }

    fun onDeleteClicked() {
        pk?.let {
            compositeDisposable.add(
                liveRepository.deleteLive(it)
                    .doOnSubscribe { _ -> liveCreatorView.showLoading() }
                    .doOnTerminate { liveCreatorView.hideLoading() }
                    .doOnError { throwable -> throwable.printStackTrace() }
                    .onErrorComplete()
                    .subscribe { liveCreatorView.close() }
            )
        }
    }

    fun onDestroy() {
        if (!isStreaming) liveRepository.livePk = null
        liveCreatorView.keepScreenOn(false)
        liveRepository.disconnectQueue()
        compositeDisposable.dispose()
    }
}
