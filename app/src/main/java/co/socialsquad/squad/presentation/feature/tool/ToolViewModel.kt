package co.socialsquad.squad.presentation.feature.tool

import android.util.Log
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class ToolViewModel(
    private val repository: ToolsRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseViewModel() {

    @ExperimentalCoroutinesApi
    val tools: StateFlow<Resource<Opportunity<Submission>>>
        get() = _tools

    @ExperimentalCoroutinesApi
    private val _tools = MutableStateFlow<Resource<Opportunity<Submission>>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    fun fetchOpportunities(link: String) {
        Log.i("op", link)
        viewModelScope.launch {
            repository.fetchOpportunitiesSubmission("/endpoints$link")
                .catch { e ->
                    _tools.value = Resource.error(e.toString(), null)
                }
                .flowOn(dispatcher)
                .collect {
                    _tools.value = Resource.success(it)
                }
            _tools.value = Resource.loading(null)
        }

        _tools.value = Resource.loading(null)
    }
}
