package co.socialsquad.squad.presentation.feature.explorev2.data

import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.domain.model.squad.SquadResponse
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetailsModel
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2SectionItems
import retrofit2.Response
import retrofit2.http.*

interface ExploreAPI {

    @GET("/endpoints/explore/")
    suspend fun getSection(): SectionList

    @GET("endpoints/explore/search")
    suspend fun search(@Query("search_term") searchTerm: String): ExploreSearchV2

    @GET("endpoints/explore/{sectionId}")
    suspend fun searchBySection(
            @Path("sectionId") sectionId: Int,
            @Query("search_term") searchTerm: String
    ): ExploreSearchV2SectionItems

    @GET
    suspend fun getExplore(@Url path: String): Explore

    @GET
    suspend fun getObject(@Url path: String): ObjectItem

    @PUT
    suspend fun putRichTextAction(@Url link: String): Response<String?>

    @GET("endpoints/timetable/{timetable_id}/object/{object_id}/submission")
    suspend fun fetchScheduleSubmissions(@Path("timetable_id") timetableId: String,
                                         @Path("object_id") ObjectId: String): Submissions


    @GET("endpoints/opportunity/submission/{submission_id}")
    suspend fun fetchOpportunityDetails(@Path("submission_id") submission_id: String): OpportunityDetailsModel

    @DELETE("endpoints/opportunity/submission/{submission_id}")
    suspend fun deleteOpportunityDetails(@Path("submission_id") submission_id: String): Response<String?>

    @GET("/endpoints/timetable/submission/{submission_id}")
    suspend fun fetchDetailsTimetable(@Path("submission_id") submission_id: String): SquadResponse<Timetable>



    @POST("/endpoints/publiclink/")
    suspend fun generateLink(@Body uniqueLinkRequest: UniqueLinkRequest) : UniqueLinkResponse
}
