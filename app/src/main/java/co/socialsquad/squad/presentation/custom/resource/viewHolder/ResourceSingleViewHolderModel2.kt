package co.socialsquad.squad.presentation.custom.resource.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Resource

data class ResourceSingleViewHolderModel2(val resource: Resource, val isLast: Boolean = true) : ViewModel