package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileRankingViewModel(

        val title: String?,
        val subtitle: String?,
        val number_of_ranks: Int? = 3,
        val icon: String? = "",
        val participants: String? = "",
        val points: String? = "",
        val position: String? = "",
        val user_in_rank: Boolean = false,
        val rank_id: String?
) : ViewModel
