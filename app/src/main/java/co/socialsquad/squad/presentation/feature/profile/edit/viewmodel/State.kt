package co.socialsquad.squad.presentation.feature.profile.edit.viewmodel

import co.socialsquad.squad.presentation.custom.InputSpinner

class State(var code: Long, var name: String?, var sigla: String?) : InputSpinner.Item {
    override val title: String? = name
}
