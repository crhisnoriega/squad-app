package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Boundaries(
    @SerializedName("top_left")
    val topLeft:Location,
    @SerializedName("bottom_right")
    val bottomRight:Location,
):Parcelable