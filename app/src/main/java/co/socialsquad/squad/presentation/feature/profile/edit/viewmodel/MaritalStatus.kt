package co.socialsquad.squad.presentation.feature.profile.edit.viewmodel

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.InputSpinner

enum class MaritalStatus(override val titleResId: Int) : InputSpinner.ItemEnum {

    C(R.string.marital_status_married),
    S(R.string.marital_status_single),
    D(R.string.marital_status_divorced);

    override lateinit var title: String
}
