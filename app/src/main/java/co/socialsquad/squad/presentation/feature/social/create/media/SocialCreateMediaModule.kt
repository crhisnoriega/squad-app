package co.socialsquad.squad.presentation.feature.social.create.media

import dagger.Binds
import dagger.Module

@Module
abstract class SocialCreateMediaModule {
    @Binds
    abstract fun socialCreateMediaView(socialCreateMediaActivity: SocialCreateMediaActivity): SocialCreateMediaView
}
