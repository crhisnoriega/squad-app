package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RankingData(
        @SerializedName("id") val id: Int?,
        @SerializedName("name") val name: String?,
        @SerializedName("icon") val icon: String?,

        @SerializedName("user_position") val user_position: String?,
        @SerializedName("user_score") val user_score: String?,
        @SerializedName("user_in_rank") val user_in_rank: Boolean?,
        @SerializedName("users_in_rank") val users_in_rank: String?,
        var first: Boolean = false
) : Parcelable