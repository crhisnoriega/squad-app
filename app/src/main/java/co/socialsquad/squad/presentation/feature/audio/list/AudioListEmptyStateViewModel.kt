package co.socialsquad.squad.presentation.feature.audio.list

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel

class AudioEmptyStateViewModel : ViewModel

const val AUDIO_EMPTY_STATE_VIEW_MODEL_ID = R.layout.view_audio_empty_state
