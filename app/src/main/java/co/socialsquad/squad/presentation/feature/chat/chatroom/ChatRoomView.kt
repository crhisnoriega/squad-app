package co.socialsquad.squad.presentation.feature.chat.chatroom

import androidx.paging.PagedList
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.presentation.feature.chat.MessageViewModel

interface ChatRoomView {
    fun setItems(messages: PagedList<MessageViewModel>)
    fun clearTextBox()
    fun finishChatRoom(resultRoom: ChatRoomModel?)
    fun setupToolbar(roomModel: ChatRoomViewModel)
    fun showLoading()
    fun hideLoading()
    fun updateRoomService(chatRoomModel: ChatRoomModel)
    fun sendMessageToService(channelUrl: String?, message: String)
}
