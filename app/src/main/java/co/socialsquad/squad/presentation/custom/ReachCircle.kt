package co.socialsquad.squad.presentation.custom

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.AccelerateInterpolator
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R

class ReachCircle(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    private var mPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mPaintActive: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var reach: Int = 0
    private var maxReach: Int = 5

    private var strokeWidth = convertDpToPixel(4F, context)

    init {
        mPaint.style = Paint.Style.STROKE
        mPaint.color = ContextCompat.getColor(context, R.color._c1c2c3)
        mPaintActive.style = Paint.Style.STROKE
        mPaintActive.color = ContextCompat.getColor(context, R.color.green)
        val attributes = context.theme.obtainStyledAttributes(attributeSet, R.styleable.ReachCircle, 0, 0)
        try {
            maxReach = attributes.getInteger(R.styleable.ReachCircle_maxPotential, 5)
        } finally {
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        mPaint.strokeWidth = (height / 20).toFloat()
        mPaintActive.strokeWidth = (height / 20).toFloat()
        strokeWidth = (height / 20).toFloat()
        for (i in 0 until maxReach) {
            canvas?.drawCircle(width / 2F, height / 2F, (i * 2 * strokeWidth) + (strokeWidth * 1.5F), if (i < reach) { mPaintActive } else { mPaint })
        }
    }

    private fun convertDpToPixel(dp: Float, context: Context): Float {
        val r = context.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.displayMetrics)
    }

    fun setStrokeColor(color: String) {
        mPaintActive.color = Color.parseColor(color)
        invalidate()
    }

    fun setReach(reach: Int) {
        val animator = ValueAnimator.ofInt(this.reach, reach)
        animator.duration = 600
        animator.interpolator = AccelerateInterpolator()
        animator.addUpdateListener { animation ->
            val value = animation.animatedValue as Int
            this.reach = value
            invalidate()
        }
        animator.start()
    }
}
