package co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_profile_edit_hinode_confirmation.*
import javax.inject.Inject

class ProfileEditHinodeConfirmationActivity : BaseDaggerActivity(), ProfileEditHinodeConfirmationView, InputView.OnInputListener {

    @Inject
    lateinit var profileEditHinodeConfirmationPresenter: ProfileEditHinodeConfirmationPresenter

    private var loading: AlertDialog? = null

    override fun onDestroy() {
        profileEditHinodeConfirmationPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit_hinode_confirmation)
        title = getString(R.string.profile_edit_hinode_confirmation_title)
        setupInput()
        profileEditHinodeConfirmationPresenter.onCreate()
    }

    private fun setupInput() {
        setInputViews(listOf(letId))
        letId.setOnClickListener {
            onConfirmClicked()
        }
    }

    private fun onConfirmClicked() {
        val id = letId.text
        profileEditHinodeConfirmationPresenter.onConfirmClicked(id)
    }

    override fun setConfirmButtonEnabled(enabled: Boolean) {
        bConfirm.isEnabled = enabled
    }

    override fun showNavigation() {
        startActivity(Intent(this, NavigationActivity::class.java))
        ActivityCompat.finishAfterTransition(this)
    }

    override fun showErrorMessageFailedToVerifyId() {
        ViewUtils.showDialog(this, getString(R.string.profile_edit_hinode_confirmation_error_failed_to_verify_id), null)
    }

    override fun showLoading() {
        if (loading == null) loading = ViewUtils.createLoadingOverlay(this)
        loading!!.show()
        hideKeyboard(this, letId)
    }

    private fun hideKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }

    override fun onInput() {
        profileEditHinodeConfirmationPresenter.onInput(letId.text)
    }
}
