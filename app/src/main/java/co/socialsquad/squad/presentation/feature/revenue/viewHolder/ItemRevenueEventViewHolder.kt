package co.socialsquad.squad.presentation.feature.revenue.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_revenue_events.view.*

class ItemRevenueEventViewHolder(
    val itemView: View,
    val callback: (viewModel: ItemRevenueEventViewModel) -> Unit
) :
    ViewHolder<ItemRevenueEventViewModel>(itemView) {

    override fun bind(viewModel: ItemRevenueEventViewModel) {
        if (viewModel.isFirst!!) {
            itemView.lineFirst.isVisible = true
        } else if (viewModel.isLast!!) {
            itemView.bottomDivider.isVisible = false
            itemView.lineLast.isVisible = true
        } else {
            itemView.lineComplete.isVisible = true
        }
    }

    override fun recycle() {

    }
}