package co.socialsquad.squad.presentation.feature.store.list

import co.socialsquad.squad.presentation.custom.ViewModel

interface StoreListView {
    fun setupList(companyColor: String?)
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun addItems(items: List<ViewModel>, shouldClearList: Boolean)
    fun showMessage(textResId: Int, onDismissListener: () -> Unit? = {})
    fun checkPermissions(permissions: Array<String>): Boolean
    fun requestPermissions(requestCode: Int, permissions: Array<String>)
    fun createLocationRequest()
    fun showPermissionsDenied()
    fun showLocationDisabled()
    fun verifyLocationServiceAvailability(): Unit?
    fun showList()
}
