package co.socialsquad.squad.presentation.feature.explorev2.items

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.components.banner.SessionBannerWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.category.SessionCategoryWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.collectionhorizontal.SessionCollectionHorizontalWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.collectionvertical.SessionCollectionVerticalWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.list.SessionObjectListVerticalWidget
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.explore_item_banner.view.*
import kotlinx.android.synthetic.main.explore_item_category.view.*
import kotlinx.android.synthetic.main.explore_item_collection_horizontal.view.*
import kotlinx.android.synthetic.main.explore_item_collection_list.view.*
import kotlinx.android.synthetic.main.explore_item_collection_vertical.view.*
import kotlin.time.ExperimentalTime

private const val TYPE_BANNER = 0
private const val TYPE_CATEGORY = 1
private const val TYPE_COLLECTION_HORIZONTAL = 2
private const val TYPE_COLLECTION_VERTICAL = 3
private const val TYPE_LIST = 4
private const val EMPTY_SPACE = 8
private const val TYPE_LOADING = 5

private const val FIRST_POSITION = 0

class ExploreItemsAdapter(
        val context: Context,
        private val layoutInflater: LayoutInflater,
        private val emptyState: EmptyState? = null,
        private val indicatorColor: String
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<ExploreData>()

    private val loadDataType =
            ExploreData(type = SectionType.getDefault(), template = SectionTemplate.getDefault())

    private fun isLoadData(exploreData: ExploreData?) =
            exploreData?.content == null &&
                    exploreData?.list == null &&
                    exploreData?.type == SectionType.getDefault() &&
                    exploreData?.template == SectionTemplate.getDefault()

    fun exploreMediaDataType(mediaUrl: String?): ExploreData {
        return ExploreData(
                type = SectionType.Banner,
                template = SectionTemplate.getDefault(),
                content = ExploreContent(
                        id = 0,
                        template = SectionTemplate.getDefault(),
                        autoScroll = false,
                        items = listOf(ContentItem(id = 0, title = "", media = SimpleMedia(mediaUrl),
                                pinLocation = null)),
                        link = null,
                        button = null,
                        type = SectionType.Banner,
                        scrollTime = 0,
                        title = ""
                )
        )
    }

    fun addLoading() {
        data.clear()
        data.add(loadDataType)
        notifyItemInserted(FIRST_POSITION)
    }

    fun addData(explore: Explore) {
        if (data.size == 1 && isLoadData(data.firstOrNull())) {
            data.clear()
            notifyItemRemoved(FIRST_POSITION)
        }

        if (explore.list.previous.isNullOrEmpty()) {
            val previousSize = data.size
            data.clear()
            notifyItemRangeRemoved(FIRST_POSITION, previousSize)
        }

        if (explore.media?.url != null && explore.media.url.isNotEmpty()) {
            data.add(exploreMediaDataType(explore.media.file))
            notifyItemInserted(data.size - 1)
        }

        if (explore.contents.isNotEmpty()) {
            val listToInsert = explore.contents.map {
                ExploreData(
                        type = it.type,
                        template = it.template,
                        content = it
                )
            }.toMutableList()


            data.addAll(listToInsert)
            notifyItemRangeInserted(data.size - 1, listToInsert.size)
        }
        if (explore.list.count > 0) {
            data.add(
                    ExploreData(
                            type = SectionType.List,
                            template = explore.list.template,
                            list = explore.list
                    )
            )

            if (data.size > 1) {
                data.add(ExploreData(type = SectionType.getByValue("empty"), template = null, content = null))
            }

            notifyItemInserted(data.size - 1)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val type = if (isLoadData(data[position])) {
            TYPE_LOADING
        } else {
            when (data[position].type) {
                SectionType.Banner -> TYPE_BANNER
                SectionType.Category -> TYPE_CATEGORY
                SectionType.VerticalCollection -> TYPE_COLLECTION_VERTICAL
                SectionType.HorizontalCollection -> TYPE_COLLECTION_HORIZONTAL
                SectionType.List -> TYPE_LIST
                SectionType.Empty -> EMPTY_SPACE
            }
        }
        return type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LOADING -> {
                val view =
                        layoutInflater.inflate(R.layout.explore_item_loading_adapter, parent, false)
                object : RecyclerView.ViewHolder(view) {}
            }
            TYPE_BANNER -> {
                val view = layoutInflater.inflate(R.layout.explore_item_banner, parent, false)
                BannerViewHolder(view)
            }
            TYPE_CATEGORY -> {
                val view = layoutInflater.inflate(R.layout.explore_item_category, parent, false)
                CategoryViewHolder(view)
            }
            TYPE_COLLECTION_HORIZONTAL -> {
                val view = layoutInflater.inflate(
                        R.layout.explore_item_collection_horizontal,
                        parent,
                        false
                )
                HorizontalCollectionViewHolder(view)
            }
            TYPE_COLLECTION_VERTICAL -> {
                val view =
                        layoutInflater.inflate(R.layout.explore_item_collection_vertical, parent, false)
                VerticalCollectionViewHolder(view)
            }

            EMPTY_SPACE -> {
                val view =
                        layoutInflater.inflate(R.layout.ranking_separator, parent, false)
                EmptySeparator(view)
            }
            else -> {
                val view =
                        layoutInflater.inflate(R.layout.explore_item_collection_list, parent, false)
                ListViewHolder(view)
            }
        }
    }

    private inner class BannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val banner: SessionBannerWidget = itemView.widgetBanner
    }

    private inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val category: SessionCategoryWidget = itemView.widgetCategory
    }

    private inner class HorizontalCollectionViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
        val collectionHorizontal: SessionCollectionHorizontalWidget =
                itemView.widgetCollectionHorizontal
    }

    private inner class VerticalCollectionViewHolder(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
        val collectionCollectionVertical: SessionCollectionVerticalWidget =
                itemView.widgetCollectionVertical
    }

    private inner class EmptySeparator(itemView: View) :
            RecyclerView.ViewHolder(itemView) {

    }

    private inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val list: SessionObjectListVerticalWidget = itemView.widgetList
        val topLineDividerList: View = itemView.topLineDividerList
    }

    @ExperimentalTime
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dataItem = data[position]

        when (data[position].type) {
            SectionType.Banner -> {
                val viewHolder = holder as ExploreItemsAdapter.BannerViewHolder
                viewHolder.banner.bind(dataItem.content, dataItem.template, emptyState)
            }
            SectionType.Category -> {
                val viewHolder = holder as ExploreItemsAdapter.CategoryViewHolder
                viewHolder.category.bind(dataItem.content, dataItem.template, emptyState)
            }
            SectionType.VerticalCollection -> {
                val viewHolder = holder as ExploreItemsAdapter.VerticalCollectionViewHolder
                viewHolder.collectionCollectionVertical.bind(
                        dataItem.content,
                        dataItem.template,
                        emptyState,
                        indicatorColor
                )
            }
            SectionType.HorizontalCollection -> {
                val viewHolder = holder as ExploreItemsAdapter.HorizontalCollectionViewHolder
                viewHolder.collectionHorizontal.bind(
                        dataItem.content,
                        dataItem.template,
                        emptyState,
                        indicatorColor
                )
            }
            SectionType.List -> {
                val viewHolder = holder as ExploreItemsAdapter.ListViewHolder
                viewHolder.list.bind(dataItem.list, dataItem.template, indicatorColor)
                viewHolder.topLineDividerList.isVisible = position != 0
            }
        }
    }

    override fun getItemCount(): Int = data.size

    fun clearList() {
        data.clear()
    }
}