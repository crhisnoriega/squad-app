package co.socialsquad.squad.presentation.feature.opportunityv2.customview


import android.app.Dialog
import android.graphics.Outline
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.ColorUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class OpportunityDetailsActionsBottomSheetDialog(var title: String, var subtitle: String, var click: () -> Unit) : BottomSheetDialogFragment() {

    var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null

    override fun getTheme(): Int = R.style.FarmBlocks_BottomSheetDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_opportunity_details_actions, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        }
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTopBordersRadius()
        super.onViewCreated(view, savedInstanceState)
        expandDialog()

        var layout: LinearLayout? = this.view?.findViewById(R.id.layoutDelete)
        layout?.setOnClickListener {
            dismiss()
            click.invoke()
        }

        var tvTitle: TextView? = this.view?.findViewById(R.id.title)
        tvTitle?.text = title

        var tvSubTitle: TextView? = this.view?.findViewById(R.id.subTitle)
        tvSubTitle?.text = subtitle

        var shareIcon: ImageView? = this.view?.findViewById(R.id.shareIcon)
        shareIcon?.setColorFilter(ColorUtils.parse(ColorUtils.getCompanyColor(view.context)!!), PorterDuff.Mode.ADD)
    }


    override fun onPause() {
        isShowing = false
        super.onPause()
    }

    override fun onResume() {
        isShowing = true
        super.onResume()
    }


    override fun dismiss() {
        isShowing = false
        super.dismiss()
    }

    override fun onDestroy() {
        isShowing = false
        super.onDestroy()
    }

    private fun setTopBordersRadius() {
        val radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)
        val container = view as ViewGroup

        container.apply {
            clipToOutline = true
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    if (view != null) outline?.setRoundRect(0, 0, view.width, (view.height + 300).toInt(), radius)
                }
            }
        }
    }

    fun expandDialog() {
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(300)
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            super.show(manager, tag)
        }
    }

    companion object {
        var isShowing = false
    }

}