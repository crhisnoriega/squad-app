package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OpportunityDetailsModel(
        @SerializedName("success") val success: Boolean,
        @SerializedName("error") val error: String,
        @SerializedName("data") val data: OpportunityData?
) : Parcelable