package co.socialsquad.squad.presentation.feature.social.create.schedule

import dagger.Binds
import dagger.Module

@Module
abstract class SocialCreateScheduledLiveModule {
    @Binds
    abstract fun view(socialCreateScheduledLiveActivity: SocialCreateScheduledLiveActivity): SocialCreateScheduledLiveView
}
