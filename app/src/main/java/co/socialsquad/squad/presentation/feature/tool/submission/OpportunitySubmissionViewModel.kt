package co.socialsquad.squad.presentation.feature.tool.submission

import android.util.Log
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class OpportunitySubmissionViewModel(
    private val repository: ToolsRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseViewModel() {

    private var nextPage: String? = null

    @ExperimentalCoroutinesApi
    val opportunity: StateFlow<Resource<Opportunity<Submission>>>
        get() = _opportunity

    @ExperimentalCoroutinesApi
    private val _opportunity = MutableStateFlow<Resource<Opportunity<Submission>>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    fun fetchOpportunities(link: String) {
        Log.i("ops", link)
        viewModelScope.launch {
            _opportunity.value = Resource.loading(null)
            repository.fetchOpportunitiesSubmission("/endpoints$link")
                .flowOn(dispatcher)
                .catch { e ->
                    _opportunity.value = Resource.error(e.toString(), null)
                }
                .collect {
                    it?.let { result ->
                        if (result.items.isNullOrEmpty()) {
                            _opportunity.value = Resource.empty()
                        } else {
                            nextPage =
                                if (result.next.isNullOrBlank()) null else "/endpoints${result.next}"
                            _opportunity.value = Resource.success(it)
                        }
                    }
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun getNextPage() {
        nextPage?.let { page ->
            viewModelScope.launch {
                fetchOpportunities(page)
            }
        }
    }
}
