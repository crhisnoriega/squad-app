package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.ColorUtils

class PageIndicator(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    sealed class Type {
        class Image : Type()
        class Video : Type()
    }

    var types: List<Type>? = null
    var viewPager: ViewPager? = null
        set(value) {
            field = value
            value?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
                override fun onPageSelected(position: Int) = select(position)
                override fun onPageScrollStateChanged(state: Int) {}
            })
        }
    var backgroundColor: Int? = null
    var selectedColor = ColorUtils.parse(context, R.color.orange)

    fun init() {
        if (types == null) throw Exception("Variable types must be set.")
        if (viewPager == null) throw Exception("Variable viewPager must be set.")

        types?.forEach {
            val resId = getLayoutResId(it)
            inflate(context, resId, this)

            val child = getChildAt(childCount - 1) as? ViewGroup
                ?: throw Exception("Layout must extend ViewGroup.")
            val background = child.getChildAt(0) as? ImageView
                ?: throw Exception("First child in layout must be an ImageView.")

            backgroundColor?.let { background.imageTintList = ColorUtils.stateListOf(it) }
        }

        select(0)
    }

    private fun select(position: Int) {
        (0 until childCount).forEach {
            val child = getChildAt(it) as ViewGroup
            val background = child.getChildAt(0) as ImageView
            background.imageTintList = when {
                position == it -> ColorUtils.stateListOf(selectedColor)
                backgroundColor != null -> ColorUtils.stateListOf(backgroundColor!!)
                else -> null
            }
        }
    }

    private fun getLayoutResId(type: Type) = when (type) {
        is Type.Video -> R.layout.view_pageindicator_item_play
        else -> R.layout.view_pageindicator_item
    }
}
