package co.socialsquad.squad.presentation.feature.search

import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.SearchRequest.Type
import co.socialsquad.squad.presentation.feature.search.results.SearchResultsFragment
import co.socialsquad.squad.presentation.feature.search.results.SearchResultsFragment.Companion.EXTRA_TYPE

enum class SearchItem(val titleResId: Int, val fragment: Fragment) {
    All(R.string.search_item_all, SearchResultsFragment().type(Type.All)),
    Products(R.string.search_item_products, SearchResultsFragment().type(Type.Products)),
    Immobiles(R.string.immobiles, SearchResultsFragment().type(Type.Immobiles)),
    Events(R.string.search_item_events, SearchResultsFragment().type(Type.Events)),
    Lives(R.string.search_item_lives, SearchResultsFragment().type(Type.Lives)),
    Audios(R.string.search_item_audios, SearchResultsFragment().type(Type.Audios)),
    Videos(R.string.search_item_videos, SearchResultsFragment().type(Type.Videos)),
    Stores(R.string.search_item_stores, SearchResultsFragment().type(Type.Stores)),
    Documents(R.string.search_item_documents, SearchResultsFragment().type(Type.Documents)),
    Trips(R.string.search_item_trips, SearchResultsFragment().type(Type.Trips));

    companion object {
        fun fragments(): Array<Fragment> {
            val fragments = SearchItem.values().map { it.fragment }
            return fragments.toTypedArray()
        }
    }
}

private fun SearchResultsFragment.type(type: Type) =
    apply { arguments = android.os.Bundle().apply { putSerializable(EXTRA_TYPE, type) } }
