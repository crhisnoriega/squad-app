package co.socialsquad.squad.presentation.feature.settings

interface SharingAppView {
    fun showLoadingDialog()
    fun hideLoadingDialog()
}
