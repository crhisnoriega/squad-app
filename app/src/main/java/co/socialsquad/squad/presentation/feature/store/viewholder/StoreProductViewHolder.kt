package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.MediaType
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_product.view.*

class StoreProductViewHolder(itemView: View, val listener: Listener<StoreProductViewModel>) : ViewHolder<StoreProductViewModel>(itemView) {
    private val ivImage: ImageView = itemView.store_product_iv_image
    private val tvName: TextView = itemView.store_product_tv_name
    private val tvPrice: TextView = itemView.store_product_tv_price
    private val tvPoints: TextView = itemView.store_product_tv_points
    private val tvCode: TextView = itemView.store_product_tv_code
    private val glide = Glide.with(itemView)
    private val firstSeparator: TextView = itemView.firstSeparator
    private val secondSeparator: TextView = itemView.secondSeparator

    override fun bind(viewModel: StoreProductViewModel) {
        with(viewModel) {
            medias?.firstOrNull { it.type is MediaType.Image }?.let {
                glide.load(it.url)
                    .crossFade()
                    .into(ivImage)
            }

            tvName.text = name

            itemView.context?.let { context ->
                price?.let {
                    firstSeparator.visibility = View.VISIBLE
                    tvPrice.text = context.getString(R.string.products_item_price, it)
                }
                points?.let {
                    secondSeparator.visibility = View.VISIBLE
                    tvPoints.text = context.getString(R.string.products_item_points, it)
                }
                tvCode.text = context.getString(R.string.product_overview_code, code)
            }

            itemView.setOnClickListener { listener.onClick(viewModel) }
        }
    }

    override fun recycle() {
        glide.clear(ivImage)
    }
}
