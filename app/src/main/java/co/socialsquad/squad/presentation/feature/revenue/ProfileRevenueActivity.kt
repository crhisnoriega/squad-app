package co.socialsquad.squad.presentation.feature.revenue

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.viewHolder.ItemSeparatorViewHolder
import co.socialsquad.squad.presentation.custom.viewHolder.ItemSeparatorViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemHeaderListingViewModel
import co.socialsquad.squad.presentation.feature.points.viewHolder.ItemHeaderPointsViewHolder
import co.socialsquad.squad.presentation.feature.revenue.listing.REVENUE_LIST_TYPE
import co.socialsquad.squad.presentation.feature.revenue.listing.RevenueElementsListingActivity
import co.socialsquad.squad.presentation.feature.revenue.model.EventData
import co.socialsquad.squad.presentation.feature.revenue.model.SectionItem
import co.socialsquad.squad.presentation.feature.revenue.model.StreamData
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueEventViewHolder
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueEventViewModel
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourceViewModel
import co.socialsquad.squad.presentation.feature.revenue.viewHolder.ItemRevenueSourcesViewHolder
import co.socialsquad.squad.presentation.feature.revenue.viewModel.RevenueViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_profile_revenue.*
import kotlinx.android.synthetic.main.activity_profile_wallet.ibBack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import java.text.SimpleDateFormat
import java.util.*


private const val RESOURCES_LIST = "resources_list"
private const val RESOURCES_TITLE = "resources_title"
private const val RESOURCES_SUBTITLE = "resources_subtitle"

class ProfileRevenueActivity : BaseActivity() {

    companion object {
        fun start(context: Context, title: String, subtitle: String) {
            context.startActivity(
                Intent(context, ProfileRevenueActivity::class.java).apply {
                    putExtra(RESOURCES_TITLE, title)
                    putExtra(RESOURCES_SUBTITLE, subtitle)
                }
            )
        }
    }


    override val modules: List<Module> = listOf(module {
    })
    override val contentView = R.layout.activity_profile_revenue

    private val sdfMes = SimpleDateFormat("MMMM")

    private val sdfDate = SimpleDateFormat("dd/MM/yy")

    private val viewModel by viewModel<RevenueViewModel>()

    val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel): Int {
            return when (viewModel) {
                is ItemRevenueEventViewModel -> R.layout.item_revenue_events
                is ItemHeaderListingViewModel -> R.layout.item_listing_header
                is ItemRevenueSourceViewModel -> R.layout.item_revenue_sources
                is ItemSeparatorViewModel -> R.layout.item_separator_top_bottom
                else -> throw IllegalArgumentException()
            }
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            R.layout.item_revenue_events -> ItemRevenueEventViewHolder(view) {

            }

            R.layout.item_listing_header -> ItemHeaderPointsViewHolder(view)

            R.layout.item_revenue_sources -> ItemRevenueSourcesViewHolder(view) {
                RevenueElementsListingActivity.start(
                    this@ProfileRevenueActivity,
                    REVENUE_LIST_TYPE.CAMPAINS,
                    0,
                    "Campanhas"
                )
            }

            R.layout.item_separator_top_bottom -> ItemSeparatorViewHolder(view)

            else -> throw IllegalArgumentException()
        }
    })


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // setFlagLayoutFullscreen()


        ibBack.setOnClickListener {
            finish()
        }

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = Color.WHITE

        var currentMonth = sdfMes.format(Calendar.getInstance().time)
        tvMes.text = currentMonth.substring(0, 1).toUpperCase() + currentMonth.substring(
            1,
            currentMonth.length
        )

        var last = Calendar.getInstance()
        last.time = Date()
        last.set(Calendar.DAY_OF_MONTH, last.getActualMaximum(Calendar.DAY_OF_MONTH))

        var first = Calendar.getInstance()
        first.time = Date()
        first.set(Calendar.DAY_OF_MONTH, 1)

        tvDateRange.text = "${sdfDate.format(first.time)} - ${sdfDate.format(last.time)} "



        configureObservables()

        viewModel.fetchStreams()
        // viewModel.fetchEvents()
    }

    private fun populateData(streamData: StreamData) {
        toolbar_title.text = streamData.headerData?.title
        amount.text = streamData.headerData?.amount
        tvMes.text = streamData.headerData?.month
        tvDateRange.text = streamData.headerData?.period

        configureSourcesList(
            streamData.events?.data,
            streamData.earning_sources_explore!!
        )

    }

    private fun configureObservables() {
        lifecycleScope.launch {
            val value = viewModel.fetchStreams
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        if (it.data?.results?.isEmpty()?.not()!!) {
                            populateData(it.data?.results?.get(0)!!)
                        }

                        revenueShimmer.visibility = View.GONE
                    }
                    Status.ERROR -> {

                    }
                }

            }
        }
    }

    private fun configureSourcesList(events: List<EventData>?, sectionItens: List<SectionItem>?) {
        rvSources.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            adapter = factoryAdapter

            var elements = mutableListOf(
                ItemSeparatorViewModel(top = false),
                ItemHeaderListingViewModel(title = "Eventos", showDivider = true)
            )

            events?.forEachIndexed { index, event ->
                elements.add(
                    ItemRevenueEventViewModel(
                        event,
                        isFirst = index == 0,
                        isLast = index == events.size - 1
                    )
                )
            }

            sectionItens?.forEach {
                elements.add(ItemSeparatorViewModel())
                elements.add(ItemHeaderListingViewModel(it.title!!))
                it.data?.forEach { section ->
                    elements.add(ItemRevenueSourceViewModel(section))
                }

            }

            elements.add(ItemSeparatorViewModel())


            factoryAdapter.addItems(elements)
        }

    }


    private fun getCompanyColor(): String? {
        var userComnpanyJson =
            PreferenceManager.getDefaultSharedPreferences(this).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

}
