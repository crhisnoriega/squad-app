package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleShiftVO
import kotlinx.android.synthetic.main.item_widget_scheduler_timebox.view.*

class TimeboxViewHolder(
        itemView: View,
        private val onSelected: (scaleSfhitVO: ScaleShiftVO, index: Int) -> Unit = { scaleShiftVO: ScaleShiftVO, i: Int -> }
) : ViewHolder<TimeboxViewHolderModel>(itemView) {
    override fun bind(viewModel: TimeboxViewHolderModel) {
        itemView.txtShift.text = viewModel.shift.scaleShift.title
        itemView.txtAvailablePositions.text = viewModel.shift.scaleShift.subtitle

        itemView.setOnClickListener(null)
        itemView.rbShift.setOnCheckedChangeListener(null)

        itemView.rbShift.isChecked = viewModel.shift.isSelected
        itemView.rbShift.setOnCheckedChangeListener { _, _ ->
            onSelected(viewModel.shift, viewModel.index)
        }
        itemView.setOnClickListener {
            onSelected(viewModel.shift, viewModel.index)
        }
    }

    override fun recycle() {
    }
}

class TimeboxViewHolderModel(internal val shift: ScaleShiftVO, internal val index: Int) : ViewModel
