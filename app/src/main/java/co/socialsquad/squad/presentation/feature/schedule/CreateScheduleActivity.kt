package co.socialsquad.squad.presentation.feature.schedule

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.util.DateTimeUtils
import kotlinx.android.synthetic.main.activity_audio_create_schedule.*
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

class CreateScheduleActivity : BaseDaggerActivity(), CreateScheduleView, InputView.OnInputListener {

    @Inject
    lateinit var presenter: CreateSchedulePresenter

    private var miSave: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_create_schedule)

        setupToolbar()
        setInputViews(listOf(itDate, itTime))

        presenter.onCreate(intent)
    }

    override fun setupViews(date: Date?) {
        itDate.setMinDate(System.currentTimeMillis() - 1000)
        date?.let {
            val start = Calendar.getInstance().apply { time = date }
            itDate.calendar = start
            itTime.calendar = start
        }
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.audio_create_schedule_title)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_audio_create_schedule, menu)
        miSave = menu.findItem(R.id.audio_create_schedule_mi_save)

        miSave?.setOnMenuItemClickListener {
            if (itDate.calendar != null && itTime.date != null) {
                presenter.onSaveClicked(itDate.calendar!!, itTime.date!!)
            }
            true
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun setFinishWithResult(resultCode: Int, intent: Intent) {
        setResult(resultCode, intent)
        finish()
    }

    override fun enableSaveButton(conditionsMet: Boolean) {
        miSave?.isEnabled = conditionsMet
    }

    override fun onInput() {
        itDate.calendar?.let {
            itDate.isValid(!DateTimeUtils.beforeToday(it))
        }
        presenter.onInput(itDate.calendar, itTime.date)
    }
}
