package co.socialsquad.squad.presentation.feature.feedback

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_feedback_option_item.view.*

class FeedbackOptionItemViewHolder(
    itemView: View,
    private val onOptionSelected: (viewModel: FeedbackItemViewModel.FeedbackOptionItemViewModel) -> Unit?
) : ViewHolder<FeedbackItemViewModel.FeedbackOptionItemViewModel>(itemView) {
    override fun bind(viewModel: FeedbackItemViewModel.FeedbackOptionItemViewModel) {
        itemView.tvText.text = viewModel.title

        if (viewModel.imageUrl != null) {
            itemView.ivImage.visibility = View.VISIBLE
            Glide.with(itemView)
                .load(viewModel.imageUrl)
                .circleCrop()
                .crossFade()
                .into(itemView.ivImage)
        } else {
            itemView.ivImage.visibility = View.GONE
        }

        setChecked(viewModel)

        itemView.setOnClickListener {
            if (viewModel.isMultiSelect || (!viewModel.isMultiSelect && !viewModel.isSelected)) {
                viewModel.isSelected = !viewModel.isSelected
                setChecked(viewModel)
            }
            onOptionSelected(viewModel)
        }
    }

    override fun recycle() {
        Glide.with(itemView).clear(itemView.ivImage)
    }

    private fun setChecked(viewModel: FeedbackItemViewModel.FeedbackOptionItemViewModel) {
        val drawableResId =
            if (viewModel.isMultiSelect) getRoundedDrawable(viewModel.isSelected)
            else getCircleDrawable(viewModel.isSelected)
        itemView.ivCheck.setImageResource(drawableResId)
    }

    private fun getCircleDrawable(isChecked: Boolean) =
        if (isChecked) R.drawable.shape_radiobutton_checked else R.drawable.shape_radiobutton

    private fun getRoundedDrawable(isChecked: Boolean) =
        if (isChecked) R.drawable.shape_checkbox_large_checked else R.drawable.shape_checkbox_large
}
