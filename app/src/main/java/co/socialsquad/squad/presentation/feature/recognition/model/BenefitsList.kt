package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class BenefitsList(
        @SerializedName("title") val title: String?,
        @SerializedName("show_all") val show_all: Boolean?,
        @SerializedName("data") val data: List<BenefitData>?,
        var first: Boolean = false
) : Parcelable