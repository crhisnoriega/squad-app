package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.hardware.Camera
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.profile.DoubleClickListener
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.utils.ImageRotator
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.bitmapCrossFade
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.fragment_rg_picture.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.*

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3

enum class CAMERA_STATUS {
    OPEN_CAMERA, CLOSE_CAMERA, REQUEST_PERMISSION, WITH_OUT_PERMISSION, PERMISSION, PICTURE_TAKEN
}

class ProfileRGPictureFragment(
    val title: String,
    val buttonName: String,
    val position: Int,
    var fragments: List<Fragment>? = null,
    var isInCompleteProfile: Boolean? = false,
    var imageUrl: String? = null
) : Fragment() {
    private var imageCapture: ImageCapture? = null
    private var mCamera: Camera? = null

    private var status = CAMERA_STATUS.WITH_OUT_PERMISSION

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    private var isFront = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnConfirmar.setOnClickListener {
            btnConfirmar.isEnabled = false
            showLoading()

            val photoFileRotated = File(
                activity?.cacheDir,
                "avatar-${UUID.randomUUID().toString()}.jpg"
            )

            var os = BufferedOutputStream(FileOutputStream(photoFileRotated))

            var rotated =
                ImageRotator.rotateImage90(viewModel.currentPhoto)
            rotated.compress(Bitmap.CompressFormat.JPEG, 100, os)


            viewModel.uploadMedia(position, title, Uri.fromFile(photoFileRotated))
        }


        camera_layout.setOnClickListener(object : DoubleClickListener() {
            override fun onDoubleClick() {
                isFront = if (isFront) {
                    startCamera(CameraSelector.DEFAULT_BACK_CAMERA)
                    false
                } else {
                    startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                    true
                }
            }
        })

        ibBack.setOnClickListener {
            if (isInCompleteProfile!!) {
                (activity as? CompleteProfileActivity)?.previous()
            } else {
                viewModel.state.postValue(StateNav("previous", true))
                (activity as? CompleteProfileActivity)?.previous()
            }
        }


        btn_take_picture.setOnClickListener {

            Log.i("camera", "status: ${status}")
            when (status) {
                CAMERA_STATUS.PERMISSION, CAMERA_STATUS.WITH_OUT_PERMISSION -> {
                    if (checkPermission().not()) {

                        status = CAMERA_STATUS.WITH_OUT_PERMISSION
                        requestPermissions(
                            arrayOf(Manifest.permission.CAMERA),
                            REQUEST_PERMISSION_PICTURE
                        )
                    } else {
                        status = CAMERA_STATUS.PERMISSION
                        startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                    }
                }

                CAMERA_STATUS.OPEN_CAMERA -> takePhoto()

                CAMERA_STATUS.PICTURE_TAKEN -> {
                    doButtonTakePhoto()
                    startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
                }
            }
        }

        txtHelpText.text = title
        btnConfirmar.text = buttonName

        viewModel.saveStatus.observe(requireActivity(), Observer {
            Log.i("rg", "save state picture: $it")
            when (it) {
                title -> {
                    if (position == 1) {
                        viewModel.saveRGImages(title)
                    } else {
                        viewModel.state.value = StateNav("next", true)
                        (activity as? CompleteProfileActivity)?.next()
                        hideLoading()
                    }
                }

                "ok_images_$title" -> {
                    viewModel.state.value = StateNav("next", true)
                    (activity as? CompleteProfileActivity)?.next()
                    hideLoading()
                }

                "upload_error" -> {
                    hideLoading()
                    btnConfirmar.isEnabled = true
                }

            }

        })

        configureFragmentByPosition()

        imageUrl?.let {
            loadPreImage()
        }
    }

    suspend fun download(link: String, file: File) {
        URL(link).openStream().use { input ->
            FileOutputStream(file).use { output ->
                input.copyTo(output)
                viewModel.currentPhoto = file
            }
        }
    }

    fun loadPreImage() {
        viewFinder.visibility = View.GONE
        image_loaded.visibility = View.VISIBLE

        Glide.with(requireActivity())
            .asBitmap()
            .load(imageUrl)
            .bitmapCrossFade()
            .into(image_loaded)

        camera_place_holder.isVisible = false

        btnConfirmar.isEnabled = true

        doButtonAnotherPhoto()

        status = CAMERA_STATUS.PICTURE_TAKEN

        val file = File(
            activity?.cacheDir,
            "avatar-${UUID.randomUUID().toString()}.jpg"
        )

        GlobalScope.async(Dispatchers.IO) {
            download(imageUrl!!, file)
        }
    }


    fun configureFragmentByPosition() {
        if (isInCompleteProfile!!) {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.light_mode_identity_profile_complete_your_profile_details))
            fragments?.forEachIndexed { index, fragment ->
                if (fragment == this) {
                    txtPostion.text = "${index + 1} de ${fragments?.size}"
                    btnConfirmar.isEnabled = true
                }
            }
            txtPostion.isVisible = true
            txtPostion.visibility = View.VISIBLE
            txtTitle.text = "Perfil"
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_document))
            txtTitle.text = "RG"
        }
    }


    private fun showLoading() {
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }


    private fun hideLoading() {
        btnConfirmar.text = buttonName
        buttonIcon.visibility = View.GONE
        buttonIcon.clearAnimation()

        btnConfirmar.isEnabled = true
    }

    private fun doButtonTakePhoto() {
        btn_take_picture.text = "Tirar Foto"
        btn_take_picture.background =
            (context?.getDrawable(R.drawable.shape_button_kickoff_take_photo))
        btn_take_picture.setTextColor(Color.WHITE)
    }

    private fun doButtonTOpenCamera() {
        btn_take_picture.text = "Abrir Câmera"
        btn_take_picture.setBackgroundDrawable(context?.getDrawable(R.drawable.shape_button_kickoff))
        btn_take_picture.setTextColor(ColorUtils.parse("#757575"))
    }

    private fun doButtonAnotherPhoto() {
        btn_take_picture.text = "Tirar Outra Foto"
        btn_take_picture.setBackgroundDrawable(context?.getDrawable(R.drawable.shape_button_kickoff))
        btn_take_picture.setTextColor(ColorUtils.parse("#757575"))
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        startCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
    }

    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_rg_picture, container, false)

    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_PERMISSION_PICTURE -> startCamera(
                CameraSelector.DEFAULT_FRONT_CAMERA
            )

            REQUEST_TAKE_PICTURE -> {
                val imageBitmap = data?.extras?.get("data") as Bitmap
//                Glide.with(this)
//                    .asBitmap()
//                    .load(imageBitmap)
//                    .bitmapCrossFade()
//                    .circleCrop()
//                    .into(img_picture)
//                btnConfirmar.isEnabled = true
            }

            REQUEST_CODE_PICK_IMAGE -> if (resultCode == AppCompatActivity.RESULT_OK) {
                data?.data?.let {
                    Glide.with(this)
                        .asBitmap()
                        .load(it)
                        .bitmapCrossFade()
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                isFirstResource: Boolean
                            ) = false

                            override fun onResourceReady(
                                resource: Bitmap,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                dataSource: DataSource,
                                isFirstResource: Boolean
                            ): Boolean {
                                val directory = activity?.cacheDir
                                val file = File(
                                    activity?.cacheDir,
                                    "avatar-${UUID.randomUUID().toString()}.jpg"
                                )

                                val inputStream = activity?.contentResolver?.openInputStream(it)!!
                                FileUtils.createFromInputStream(file, inputStream)
                                val uri = Uri.fromFile(file)

                                viewModel.currentPhoto = file
                                Log.i("camera", "set camera on result")
                                // profileEditPresenter.onResourceReady(uri)
                                // setResult(Activity.RESULT_OK, Intent())


                                Handler().postDelayed({ doButtonTOpenCamera() }, 600)

                                return false
                            }
                        })
                        .into(image_loaded)

                    viewFinder.visibility = View.GONE
                    image_loaded.visibility = View.VISIBLE

                    status = CAMERA_STATUS.PICTURE_TAKEN

                }
            }
        }
    }

    private fun startCamera(cameraSelector: CameraSelector) {

        camera_layout_aaa.visibility = View.VISIBLE
        camera_place_holder.visibility = View.GONE
        viewFinder.visibility = View.VISIBLE
        image_loaded.visibility = View.GONE

        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .setTargetResolution(Size(480, 400))
                .build()

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this, CameraSelector.DEFAULT_BACK_CAMERA, preview, imageCapture
                )

                doButtonTakePhoto()

                status = CAMERA_STATUS.OPEN_CAMERA
            } catch (exc: Exception) {
                Log.e("camera", "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    override fun onResume() {
        super.onResume()

        doButtonTOpenCamera()
        status = CAMERA_STATUS.WITH_OUT_PERMISSION

        Log.i("camera", "onResume")
    }

    private fun takePhoto() {
        Log.i("camera", "takePhoto")
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // Create time-stamped output file to hold the image
        val photoFile = File(
            activity?.cacheDir,
            "avatar-${UUID.randomUUID().toString()}.jpg"
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile)
            .setMetadata(ImageCapture.Metadata().apply {
            }).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e("camera", "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    Log.i("camera", "take photo 0")
                    val savedUri = Uri.fromFile(photoFile)

                    viewFinder.visibility = View.GONE
                    image_loaded.visibility = View.VISIBLE

                    Glide.with(requireActivity())
                        .asBitmap()
                        .load(savedUri)
                        .bitmapCrossFade()
                        .into(image_loaded)

                    btnConfirmar.isEnabled = true

                    doButtonAnotherPhoto()

                    status = CAMERA_STATUS.PICTURE_TAKEN

                    viewModel.currentPhoto = photoFile

                    Log.i("camera", "take photo 1")
                }
            })
    }


}
