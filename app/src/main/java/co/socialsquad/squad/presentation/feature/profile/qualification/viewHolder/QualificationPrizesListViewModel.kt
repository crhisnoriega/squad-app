package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class QualificationPrizesListViewModel(
    var prizes: List<PrizeViewModel>
) : ViewModel
