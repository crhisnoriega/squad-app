package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemScoreRecognitionViewModel(val title: String, val subtitle: String, val position: String = "") : ViewModel {
}