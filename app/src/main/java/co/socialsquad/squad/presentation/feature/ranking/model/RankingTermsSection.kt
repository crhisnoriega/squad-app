package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RankingTermsSection(
        @SerializedName("title") val title: String?,
        @SerializedName("content") val content: String?,
        @SerializedName("order") val order: Int?,


        ) : Parcelable/**/