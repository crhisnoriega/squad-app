package co.socialsquad.squad.presentation.feature.users

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.users.viewHolder.USER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewHolder
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel
import co.socialsquad.squad.presentation.util.ViewUtils
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_user_list.*
import kotlinx.android.synthetic.main.toolbar_user_list.*
import javax.inject.Inject

open class UserListActivity : BaseDaggerActivity(), UserListView {

    companion object {
        const val QUERY_ITEM_PK = "QUERY_ITEM_PK"
        const val USER_LIST_TYPE = "USER_LIST_TYPE"
        const val EXTRA_USER_LIST = "EXTRA_USER_LIST"
    }

    @Inject
    lateinit var userListPresenter: UserListPresenter

    private var userAdapter: RecyclerViewAdapter? = null
    private val linearLayoutManager by lazy { PrefetchingLinearLayoutManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)
        setupSwipeRefresh()
        setupList()
        userListPresenter.onCreate(intent)
    }

    private fun setupSwipeRefresh() {
        srlUserList.setOnRefreshListener {
            srlUserList.isEnabled = false
            userListPresenter.onRefresh()
        }
    }

    private fun setupList() {
        userAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is UserViewModel -> USER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                USER_VIEW_MODEL_ID -> UserViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvUsers.apply {
            adapter = userAdapter
            layoutManager = linearLayoutManager
            addOnScrollListener(
                EndlessScrollListener(
                    8,
                    rvUsers?.layoutManager as LinearLayoutManager,
                    userListPresenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
            addItemDecoration(ListDividerItemDecoration(context))
        }
    }

    override fun setupTabs(titleSeen: Int, titleNotSeen: Int, squadColor: String?) {
        setTotalSeen(titleSeen, -1)
        setTotalNotSeen(titleNotSeen, -1)
        tlUserList.setSelectedTabIndicatorColor(Color.parseColor(squadColor))

        tlUserList.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                userListPresenter.onTabSelected(tab.position)
            }

            override fun onTabReselected(p0: TabLayout.Tab?) {}

            override fun onTabUnselected(p0: TabLayout.Tab?) {}
        })
    }

    override fun setupToolbar(titleResId: Int, subTitleResId: Int?) {
        setSupportActionBar(tToolbar)
        ivBackButton.setOnClickListener {
            onBackPressed()
        }
        tvListTitle.text = getString(titleResId)
        if (subTitleResId == null) {
            tvListSubtitle.visibility = View.GONE
        } else {
            tvListSubtitle.text = getString(subTitleResId)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun setItems(users: List<UserViewModel>) {
        userAdapter?.setItems(users)
        if (users.isNotEmpty()) {
            userAdapter?.addItems(arrayListOf(DividerViewModel))
        }
    }

    override fun addItems(users: List<UserViewModel>) {
        userAdapter?.remove(DividerViewModel)
        userAdapter?.addItems(users)
        userAdapter?.addItems(arrayListOf(DividerViewModel))
    }

    override fun showMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(textResId), onDismissListener)
    }

    override fun stopRefreshing() {
        srlUserList.apply {
            isRefreshing = false
            isEnabled = true
        }
    }

    override fun startLoading() {
        userAdapter?.startLoading()
    }

    override fun stopLoading() {
        userAdapter?.stopLoading()
    }

    override fun showTabLayout() {
        rlTabs.visibility = View.VISIBLE
    }

    override fun hideTabLayout() {
        rlTabs.visibility = View.GONE
    }

    override fun setTotalSeen(title: Int, count: Int) {
        var tabText = getString(title, count)
        if (count <0) {
            tabText = tabText.replace("-1", "").trim()
        }
        tlUserList.getTabAt(UserListPresenter.SEEN_USERS_TAB_POSITION)?.text = tabText
    }

    override fun setTotalNotSeen(title: Int, count: Int) {
        var tabText = getString(title, count)
        if (count <0) {
            tabText = tabText.replace("-1", "").trim()
        }
        tlUserList.getTabAt(UserListPresenter.NOT_SEEN_USERS_TAB_POSITION)?.text = tabText
    }
}
