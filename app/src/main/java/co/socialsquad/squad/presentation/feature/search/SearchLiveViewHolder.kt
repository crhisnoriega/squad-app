package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.live.list.LiveListItemViewModel
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.toDate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_lives_item.view.*

class SearchLiveViewHolder(itemView: View) : ViewHolder<LiveListItemViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: LiveListItemViewModel) {
        with(viewModel) {
            glide.load(imageUrl)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.ivImage)

            itemView.short_title_lead.text = title

            itemView.tvAuthor.apply {
                text = userName
                setOnClickListener { openUser(userPk.toInt()) }
            }

            val viewCountStringResId =
                if (viewCount == 1) R.string.view_count_singular
                else R.string.view_count_plural
            val views = itemView.context.getString(viewCountStringResId, TextUtils.toUnitSuffix(viewCount.toLong()))
            val timestamp = date?.toDate()?.elapsedTime(itemView.context)
            itemView.tvViewCountAndTimestamp.text = listOfNotNull(views, timestamp).joinToString(" • ")

            itemView.setOnClickListener { openVideo(viewModel.videoUrl) }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }

    private fun openVideo(url: String) {
        val intent = Intent(itemView.context, VideoActivity::class.java).putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        itemView.context.startActivity(intent)
    }
}
