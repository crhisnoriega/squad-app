package co.socialsquad.squad.presentation.feature.schedule

import android.content.Intent
import java.util.Date

interface CreateScheduleView {
    fun setupViews(date: Date?)
    fun enableSaveButton(conditionsMet: Boolean)
    fun setFinishWithResult(resultCode: Int, intent: Intent)
}
