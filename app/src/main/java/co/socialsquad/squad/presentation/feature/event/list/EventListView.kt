package co.socialsquad.squad.presentation.feature.event.list

import co.socialsquad.squad.presentation.feature.event.EventViewModel

interface EventListView {

    fun setupList()
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun addItems(events: List<EventViewModel>, shouldClearList: Boolean)
    fun showMessage(textResId: Int, onDismissListener: () -> Unit? = {})
    fun showShimmer()
    fun hideShimmer()
}
