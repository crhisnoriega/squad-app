package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextTitle
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_rich_text_title.view.*

class TitleDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TitleRichTextViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as TitleRichTextViewHolder
        holder.bind(item as RichTextTitle)
    }

    private inner class TitleRichTextViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_rich_text_title)
    ) {
        fun bind(richTextObj: RichTextTitle) = with(itemView) {
            tv_item_rich_text_title.text = richTextObj.title
        }
    }
}
