package co.socialsquad.squad.presentation.feature.signup.credentials

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import kotlinx.android.synthetic.main.fragment_signup_credentials_email.*

class SignupCredentialsEmailFragment : Fragment() {

    private var signupCredentialsFragmentListener: SignupCredentialsFragmentListener? = null

    val emailTyped
        get() = tietField?.text?.toString() ?: ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        signupCredentialsFragmentListener = activity as SignupCredentialsFragmentListener?
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_signup_credentials_email, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupKeyboardAction()
        setupNextButtonBehavior()
    }

    private fun setupKeyboardAction() {
        tietField.setOnKeyboardActionListener { signupCredentialsFragmentListener?.onKeyboardAction() }
    }

    private fun setupNextButtonBehavior() {
        tietField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                signupCredentialsFragmentListener?.onTextChanged(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }
}
