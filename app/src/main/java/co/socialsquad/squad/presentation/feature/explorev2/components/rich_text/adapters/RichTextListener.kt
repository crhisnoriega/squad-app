package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinkDetails
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextResourcesDetails

interface RichTextListener {
    fun onRichTextLinkClicked(link: RichTextLinkDetails)
    fun onRichTextObjectClicked(link: Link)
    fun onRichTextResourceClicked(resource: RichTextResourcesDetails)
}
