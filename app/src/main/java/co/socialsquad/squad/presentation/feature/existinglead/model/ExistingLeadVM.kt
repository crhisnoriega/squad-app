package co.socialsquad.squad.presentation.feature.existinglead.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.leadsearch.ExistingLead
import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadSeparator
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.LeadResultType
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
class ExistingLeadVM(
    val existingLeadSeparator: ExistingLeadSeparator? = null,
    val existingLead: ExistingLead? = null,
    var checked: Boolean
) : Parcelable, ViewType {

    override fun getViewType(): Int {
        existingLead?.let {
            return LeadResultType.Item.value
        }
        existingLeadSeparator?.let {
            return LeadResultType.Separator.value
        }
        return LeadResultType.Invalid.value
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
