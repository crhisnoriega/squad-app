package co.socialsquad.squad.presentation.feature.social

import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.presentation.util.FileUtils

class SocialViewModelMapper {

    fun mapItems(feed: Feed, audioEnabled: Boolean, userAvatar: String?, color: String?, subdomain: String?): List<SocialViewModel> {
        val items = mutableListOf<SocialViewModel>()

//        val lives = feed.posts.filter { it.type == Post.TYPE_LIVES && !it.lives.isNullOrEmpty() }
//            .flatMap { post -> post.lives!!.map { LiveViewModel(it, post, userAvatar, subdomain) } }
//            .filter { it.isLive || it.isScheduledLive || it.isVideo }
//        if (lives.isNotEmpty()) items.add(LiveListViewModel(lives, color))

//        val posts = feed.posts.filter { it.type != Post.TYPE_LIVES }
        feed.posts.forEach { mapPost(it, audioEnabled, userAvatar, subdomain)?.let { items.add(it) } }

        return items
    }

    fun mapPost(post: Post, audioEnabled: Boolean, userAvatar: String?, subdomain: String?): SocialViewModel? {
        for (media in post.medias.orEmpty()) {
            when {
                FileUtils.isImageMedia(media) -> return PostImageViewModel(post, userAvatar, subdomain = subdomain)
                FileUtils.isVideoMedia(media) -> when {
                    post.type.equals(Post.TYPE_MEDIA, true) || post.type.equals(Post.TYPE_SUCCESS_STORY, true) ->
                        return PostVideoViewModel(post, audioEnabled, userAvatar ?: "", subdomain)
                    post.type.equals(Post.TYPE_POST_LIVE, true) ->
                        return PostLiveViewModel(post, audioEnabled, userAvatar, subdomain)
                }
            }
        }
        return null
    }
}
