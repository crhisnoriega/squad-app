package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_action_recognition.view.*
import kotlinx.android.synthetic.main.item_recognition_benefit.view.*
import kotlinx.android.synthetic.main.item_recognition_benefit.view.dividerItemTop
import kotlinx.android.synthetic.main.item_recognition_benefit.view.mainLayout
import kotlinx.android.synthetic.main.item_recognition_benefit.view.txtTitle


class ItemBenefitRecognitionViewHolder(itemView: View, var onClickAction: (benefit: ItemBenefitRecognitionViewModel) -> Unit) : ViewHolder<ItemBenefitRecognitionViewModel>(itemView) {
    override fun bind(viewModel: ItemBenefitRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.benefitData.highlight
        itemView.txtTitle.isVisible = viewModel.benefitData.highlight.isNullOrBlank().not()

        itemView.txtTitle.setTextColor(ColorUtils.parse(getCompanyColor()!!))
        itemView.txtSubTitle.text = viewModel.benefitData.title
        itemView.txtSubtitle2.text = viewModel.benefitData.description?.trim()
        itemView.dividerItemTop.isVisible = viewModel.benefitData.first.not()


        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.benefitData.icon), itemView.cover)
        //Glide.with(itemView.context).load(viewModel.benefitData.media_cover).into(itemView.cover)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}