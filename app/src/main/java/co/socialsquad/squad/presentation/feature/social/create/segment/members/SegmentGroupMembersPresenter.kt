package co.socialsquad.squad.presentation.feature.social.create.segment.members

import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.repository.SegmentRepository
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SegmentGroupMembersPresenter
@Inject internal constructor(
    private val segmentGroupMetricMembersView: SegmentGroupMembersView,
    private val segmentGroupMembersRepository: SegmentRepository
) {

    private val compositeDisposable = CompositeDisposable()
    private var membersPage = 1
    private var complete = false
    private val selectedMember: ArrayList<UserViewModel> = arrayListOf()

    fun onCreate(bundleSelected: List<UserViewModel>) {
        selectedMember.addAll(bundleSelected)
        getMembers(false)
    }

    private fun getMembers(isToAdd: Boolean) {
        if (complete) return
        compositeDisposable.add(
            segmentGroupMembersRepository.getSegmentationMembers(membersPage)
                .doOnSubscribe { segmentGroupMetricMembersView.startLoading() }
                .doOnTerminate { segmentGroupMetricMembersView.stopLoading() }
                .doOnNext { complete = it.next == null }
                .map { members ->
                    members.results?.filter {
                        it.user?.firstName.isNullOrEmpty() || !it.user?.lastName.isNullOrEmpty()
                    }
                }
                .map { mapToViewModel(it) }
                .subscribe(
                    { members ->
                        if (isToAdd) {
                            segmentGroupMetricMembersView.addMembers(members)
                        } else {
                            segmentGroupMetricMembersView.setMembers(members)
                        }
                        segmentGroupMetricMembersView.setAddButtonEnabled(selectedMember.isNotEmpty())
                        membersPage++
                    },
                    { it.printStackTrace() }
                )
        )
    }

    private fun mapToViewModel(users: List<UserCompany>) =
        users.map { user -> UserViewModel(user.pk, user.user.firstName ?: "", user.user.lastName ?: "", user.user.avatar ?: "", selectedMember.any { selected -> selected.pk == user.pk }) }

    fun onScrolledBeyondVisibleThreshold() {
        getMembers(true)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun selectMember(viewModel: UserViewModel) {
        val selected = selectedMember.filter { selected -> selected.pk == viewModel.pk }
        if (selected.isNotEmpty()) {
            selectedMember.remove(selected[0])
        } else {
            selectedMember.add(viewModel)
        }
        segmentGroupMetricMembersView.setAddButtonEnabled(selectedMember.isNotEmpty())
    }

    fun selectedMembers() {
        segmentGroupMetricMembersView.finishActivity(selectedMember)
    }
}
