package co.socialsquad.squad.presentation.custom.tool

import android.util.Log
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.*
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Tool
import co.socialsquad.squad.presentation.feature.tool.repository.ToolsRepository
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ToolWidgetViewModel(
        private val repository: ToolsRepository,
        private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : BaseViewModel() {

    private var selectedTool: Resource<Tool> = Resource.empty()

    @ExperimentalCoroutinesApi
    val tools: StateFlow<Resource<Opportunity<Submission>>>
        get() = _tools

    @ExperimentalCoroutinesApi
    private val _tools = MutableStateFlow<Resource<Opportunity<Submission>>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val timetable: StateFlow<Resource<Opportunity<Timetable>>>
        get() = _timetable

    @ExperimentalCoroutinesApi
    private val _timetable = MutableStateFlow<Resource<Opportunity<Timetable>>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    private val opportunityWidgetCreation = MutableStateFlow<Resource<OpportunityWidget?>>(Resource.empty())

    @ExperimentalCoroutinesApi
    fun opportunityWidgetCreation(): StateFlow<Resource<OpportunityWidget?>> {
        return opportunityWidgetCreation
    }

    @ExperimentalCoroutinesApi
    fun fetchOpportunities(link2: String) {
        viewModelScope.launch {
            _tools.value = Resource.loading(null)
            repository.fetchOpportunitiesSubmission("/endpoints$link2")
                    .flowOn(dispatcher)
                    .catch { e ->
                        Log.i("oppor", e.message, e)
                        _tools.value = Resource.error(e.toString(), null)
                    }
                    .collect {

                        Log.i("sche2", Gson().toJson(it))
                        if (it?.items.isNullOrEmpty()) {
                            _tools.value = Resource.emptyWithData(it!!)
                        } else {
                            _tools.value = Resource.success(it)
                        }
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchTimetables(link: String) {
        viewModelScope.launch {
            Log.i("timetable", "calling: $link")
            _timetable.value = Resource.loading(null)
            repository.fetchOpportunitiesTimetable("/endpoints$link")
                    .flowOn(dispatcher)
                    .catch { e ->
                        Log.i("timetable", e.message, e)
                        _timetable.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        if (it?.items.isNullOrEmpty()) {
                            _timetable.value = Resource.empty()
                        } else {
                            _timetable.value = Resource.success(it)
                        }
                    }
        }
    }


    fun setSelectedTool(tool: Tool) {
        selectedTool = Resource.success(tool)
    }

    @ExperimentalCoroutinesApi
    fun createNewOpportunity(objectId: Long, tool: Tool) {
        viewModelScope.launch {
            setSelectedTool(tool)
            opportunityWidgetCreation.value = Resource.loading(null)
            repository.createNewOpportunity(objectId, selectedTool.data!!.id)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        opportunityWidgetCreation.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        getNextWidget(it.data)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun createNewOpportunityJustIds(objectId: Long, toolId: Long) {
        viewModelScope.launch {
            opportunityWidgetCreation.value = Resource.loading(null)
            repository.createNewOpportunity(objectId, toolId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        opportunityWidgetCreation.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        getNextWidget(it.data)
                    }
        }
    }

    private fun getNextWidget(submissionCreation: SubmissionCreation) {
        viewModelScope.launch {
            repository.getNextWidget(submissionCreation.id)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        opportunityWidgetCreation.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        opportunityWidgetCreation.value = Resource.success(it.data)
                    }
        }
    }

    fun selectedTool(): Resource<Tool> {
        return selectedTool
    }
}
