package co.socialsquad.squad.presentation.util

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import co.socialsquad.squad.R

object ViewUtils {

    fun showDialog(context: Context?, message: String, onDismissListener: DialogInterface.OnDismissListener?) {
        context?.let {
            showDialog(it, message, context.getString(R.string.dialog_ok), onDismissListener)
        }
    }

    fun showDialog(context: Context, message: String, positiveButton: String, onDismissListener: DialogInterface.OnDismissListener?) {
        AlertDialog.Builder(context, R.style.Dialog)
            .setMessage(message)
            .setPositiveButton(positiveButton) { dialog, which -> dialog.dismiss() }
            .setOnDismissListener(onDismissListener)
            .create()
            .show()
    }

    fun showDialog(
        context: Context,
        message: String,
        positiveButton: String,
        onPositiveClickListener: DialogInterface.OnClickListener?,
        negativeButton: String
    ) {
        AlertDialog.Builder(context, R.style.Dialog)
            .setMessage(message)
            .setNegativeButton(negativeButton, null)
            .setPositiveButton(positiveButton, onPositiveClickListener)
            .create()
            .show()
    }

    fun showNotCancelableDialog(
        context: Context,
        title: String,
        message: String,
        positiveButton: String,
        onPositiveClickListener: DialogInterface.OnClickListener?
    ) {
        AlertDialog.Builder(context, R.style.Dialog)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButton, onPositiveClickListener)
            .setCancelable(false)
            .create()
            .show()
    }

    fun createLoadingOverlay(context: Context?): AlertDialog? {
        context?.let {
            return AlertDialog.Builder(it, R.style.Loading)
                .setView(R.layout.progressbar_loading_overlay)
                .setCancelable(false)
                .create()
        }
        return null
    }
}
