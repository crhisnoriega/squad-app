package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemBenefitRankingViewModel(
        val id: String,
        val title: String,
        val positions: String,
        val description: String,
        val media_cover: String = "",
        val isFirst:Boolean = false) : ViewModel {
}