package co.socialsquad.squad.presentation.feature.opportunity.info

import co.socialsquad.squad.data.repository.OpportunityRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OpportunityInfoPresenter @Inject constructor(
    private val opportunityRepository: OpportunityRepository,
    private val view: OpportunityInfoView
) {

    private val compositeDisposable = CompositeDisposable()

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun getStageInfo(stageId: Int) {
        view.showLoading(true)
        compositeDisposable.add(
            opportunityRepository.opportunityStageDetails(stageId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityStageClicked(it.info)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }
}
