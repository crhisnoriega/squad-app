package co.socialsquad.squad.presentation.feature.searchv2.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreObject
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreSearchV2Section(
    @SerializedName("count")
    val count: Int,
    @SerializedName("items")
    val items: List<ExploreObject>,
    @SerializedName("template")
    val template: String
) : Parcelable
