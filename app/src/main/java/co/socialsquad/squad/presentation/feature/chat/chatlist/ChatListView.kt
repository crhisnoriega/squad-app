package co.socialsquad.squad.presentation.feature.chat.chatlist

import co.socialsquad.squad.presentation.custom.ViewModel

interface ChatListView {
    fun setItems(items: List<ViewModel>)
    fun showLoading()
    fun hideLoading()
    fun updateItem(section: ViewModel)
    fun showNoResults(query: String)
    fun showSearchEmpty()
    fun hideSearch()
}
