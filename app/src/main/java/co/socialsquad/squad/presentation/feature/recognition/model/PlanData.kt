package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PlanData(
        @SerializedName("id") val id: Int?,
        @SerializedName("user_in_plan") val user_in_plan: Boolean?,
        @SerializedName("title") val title: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("media_cover") val media_cover: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("anonymous") val anonymous: Boolean?,

        @SerializedName("levels") val levels: LevelList?,
        var first: Boolean = false
) : Parcelable