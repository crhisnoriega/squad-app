package co.socialsquad.squad.presentation.feature.kickoff.terms

import dagger.Binds
import dagger.Module

@Module
abstract class KickoffTermsModule {

    @Binds
    abstract fun kickoffTermsView(kickoffTermsFragment: KickoffTermsFragment): KickoffTermsView
}
