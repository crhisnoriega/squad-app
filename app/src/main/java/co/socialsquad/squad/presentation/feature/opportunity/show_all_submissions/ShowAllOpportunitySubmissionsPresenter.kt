package co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions

import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.repository.OpportunityRepository
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ShowAllOpportunitySubmissionsPresenter @Inject constructor(
    private val opportunityRepository: OpportunityRepository,
    private val view: ShowAllOpportunitySubmissionsView
) : OpportunityListPresenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getOpportunityAction(opportunity: Opportunity) {
        // not necessary
    }

    override fun getSubmissionDetails(
        opportunityId: Int,
        objectId: Int,
        submissionId: Int,
        objectType: OpportunityObjectType
    ) {
        view.showLoading(true)

        compositeDisposable.add(
            opportunityRepository.opportunitySubmissionDetails(
                opportunityId,
                objectId,
                submissionId,
                objectType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityDetailsClick(it.submissionDetails)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }

    override fun getOpportunityInfo(opportunity: Opportunity) {
        view.showLoading(true)

        compositeDisposable.add(
            opportunityRepository.opportunityDetails(opportunity.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityInfoClick(it.info)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
