package co.socialsquad.squad.presentation.feature.live.list

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.presentation.custom.ViewModel
import java.util.concurrent.TimeUnit

const val LIVE_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_live_list_item

class LiveListItemViewModel(
    val pk: Int,
    val postLivePk: Int,
    val imageUrl: String?,
    val title: String,
    val description: String,
    val userName: String,
    val userPk: Long,
    val date: String?,
    val viewCount: Int,
    val videoPk: Long,
    val videoUrl: String,
    val duration: Long,
    val canShare: Boolean,
    var visualized: Boolean = false,
    var canRecommend: Boolean = false
) : ViewModel {
    constructor(live: Live) : this(
        live.pk,
        live.postLivePk,
        live.schedule?.photo?.url ?: live.owner.user.avatar,
        live.name,
        live.content ?: "",
        live.owner.user.fullName,
        live.owner.pk.toLong(),
        live.startedAt ?: live.createdAt,
        live.medias?.firstOrNull()?.viewsCount?.toInt() ?: 0,
        live.medias?.firstOrNull()?.pk?.toLong() ?: 0,
        live.medias?.firstOrNull()?.streamings?.firstOrNull()?.playlistUrl ?: "",
        live.medias?.firstOrNull()?.timeLength?.let { TimeUnit.SECONDS.toMillis(it) } ?: 0,
        live.canShare,
        live.visualized,
        live.canRecommend
    )
}
