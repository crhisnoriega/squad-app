package co.socialsquad.squad.presentation.feature.schedule

import android.content.Intent
import co.socialsquad.squad.presentation.util.DateTimeUtils
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

class CreateSchedulePresenter @Inject constructor(private val view: CreateScheduleView) {

    companion object {
        const val RESULT_CODE_SCHEDULE = 7
        const val EXTRA_DATE = "EXTRA_DATE"
    }

    fun onCreate(intent: Intent) {
        var date: Date? = null
        if (intent.hasExtra(EXTRA_DATE)) { date = intent.extras?.getLong(EXTRA_DATE)?.let { Date(it) } }
        view.setupViews(date)
    }

    fun onInput(date: Calendar?, time: Date?) {
        val conditionsMet = date != null && time != null && DateTimeUtils.afterNow(date, time)
        view.enableSaveButton(conditionsMet)
    }

    fun onSaveClicked(date: Calendar, time: Date) {
        val dateMillis = Calendar.getInstance().apply {
            this.time = time
            set(Calendar.YEAR, date.get(Calendar.YEAR))
            set(Calendar.MONTH, date.get(Calendar.MONTH))
            set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH))
        }.time.time

        val intent = Intent().apply {
            putExtra(EXTRA_DATE, dateMillis)
        }

        view.setFinishWithResult(RESULT_CODE_SCHEDULE, intent)
    }
}
