package co.socialsquad.squad.presentation.feature.navigation

import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.SectionsFragment
import co.socialsquad.squad.presentation.feature.hub.HubFragment
import co.socialsquad.squad.presentation.feature.notification.NotificationFragment
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.social.SocialFragment

enum class NavigationItem(
        val titleResId: Int
) {
    SOCIAL(R.string.navigation_item_social),
    EXPLORE(R.string.navigation_item_explore),
    PROFILE(R.string.navigation_item_profile),
    NOTIFICATION(R.string.navigation_item_notification),
    HUB(R.string.navigation_item_hub);
}

fun NavigationItem.getFragment(): Fragment {

    var fragment = NavigationActivity.fragments[this]


    if (fragment == null) {
        fragment = when (this) {
            NavigationItem.SOCIAL -> SocialFragment()
            NavigationItem.EXPLORE -> SectionsFragment()
            NavigationItem.PROFILE -> ProfileFragment()
            NavigationItem.NOTIFICATION -> NotificationFragment()
            NavigationItem.HUB -> HubFragment()
        }
    }
   // NavigationActivity.fragments.put(this, fragment)

    return fragment
}
