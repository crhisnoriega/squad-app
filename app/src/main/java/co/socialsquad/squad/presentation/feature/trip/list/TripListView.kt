package co.socialsquad.squad.presentation.feature.trip.list

import co.socialsquad.squad.presentation.feature.trip.TripViewModel

interface TripListView {
    fun setupList(companyColor: String?)
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun addItems(items: List<TripViewModel>, shouldClearList: Boolean)
}
