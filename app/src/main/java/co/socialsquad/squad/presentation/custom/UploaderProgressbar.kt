package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_uploader_progress.view.*

class UploaderProgressbar(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private val view: View = View.inflate(context, R.layout.view_uploader_progress, this)
    private var numberOfUploads: Int = 1
    private var currentUpload: Int = 1

    init {
        pbUploading.max = 100
        pbUploading.progress = 0
    }

    fun setOnClickListeners(onTryAgainOnClickListener: OnClickListener, onCancelOnClickListener: OnClickListener) {
        setTryAgainOnClickListener(onTryAgainOnClickListener)
        setCancelOnClickListener(onCancelOnClickListener)
    }

    fun setText(message: String) {
        tvText.text = message
    }

    fun setThumbnail(thumbnailMedia: Bitmap, showPlay: Boolean) {
        ivThumbnail.setImageBitmap(thumbnailMedia)
        ivPlay.visibility = if (showPlay) View.VISIBLE else View.GONE
    }

    fun setNumberOfUploads(numberOfUploads: Int) {
        this.numberOfUploads = numberOfUploads
        if (numberOfUploads > 1) {
            tvTotalProgress.visibility = View.VISIBLE
            tvTotalProgress.text = "$currentUpload/$numberOfUploads"
        }
    }

    fun startUpload(message: String) {
        tvText.text = message
        pbUploading.isIndeterminate = false
        pbUploading.progress = 0
        pbUploading.visibility = View.VISIBLE
        ibTryAgain.visibility = View.GONE
    }

    fun incrementProgress(increment: Int) {
        pbUploading.incrementBy(increment)
    }

    fun incrementUploadNumber() {
        currentUpload++
        tvTotalProgress.text = "$currentUpload/$numberOfUploads"
        pbUploading.progress = 0
    }

    fun startIndeteminate(message: String) {
        tvText.text = message
        pbUploading.isIndeterminate = true
        pbUploading.visibility = View.VISIBLE
        ibTryAgain.visibility = View.GONE
    }

    fun setError(message: String) {
        tvText.text = message
        ibTryAgain.visibility = View.VISIBLE
        ibCancel.visibility = View.GONE
        pbUploading.visibility = View.GONE
    }

    fun finish() {
        tvText.text = ""
        ibTryAgain.visibility = View.GONE
        ibCancel.visibility = View.GONE
        pbUploading.visibility = View.GONE
        view.visibility = View.GONE
        tvTotalProgress.visibility = View.GONE
        numberOfUploads = 1
        currentUpload = 1
    }

    private fun setTryAgainOnClickListener(onClickListener: OnClickListener) {
        ibTryAgain.setOnClickListener {
            ibTryAgain.visibility = View.GONE
            ibCancel.visibility = View.VISIBLE
            pbUploading.visibility = View.VISIBLE
            onClickListener.onClick(it)
        }
    }

    private fun setCancelOnClickListener(onClickListener: OnClickListener) {
        ibCancel.setOnClickListener {
            view.visibility = View.GONE
            onClickListener.onClick(it)
        }
    }
}
