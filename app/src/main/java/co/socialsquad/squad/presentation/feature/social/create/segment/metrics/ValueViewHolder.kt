package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.linearlayout_segment_values_list_item.view.*

class ValueViewHolder(
    itemView: View,
    private val glide: RequestManager,
    private val multipick: Boolean,
    private val listener: Listener<ValueViewModel>
) : ViewHolder<ValueViewModel>(itemView) {
    private val ivImage: ImageView = itemView.segment_values_list_item_iv_image
    private val tvFirst: TextView = itemView.segment_values_list_item_tv_first
    private val tvSecond: TextView = itemView.segment_values_list_item_tv_second
    private val rbIndicator: RadioButton = itemView.segment_values_list_item_rb_indicator
    private val cbIndicator: CheckBox = itemView.segment_values_list_item_cb_indicator
    private val divider: View = itemView.segment_values_top_divider

    override fun bind(viewModel: ValueViewModel) {
        if (adapterPosition == 0) {
            divider.visibility = View.VISIBLE
        }
        tvFirst.text = viewModel.name
        tvSecond.text =
            if (viewModel.count > 1) itemView.context.getString(R.string.segment_values_members, viewModel.count)
            else itemView.context.getString(R.string.segment_values_member)

        if (viewModel.imageUrl != null) {
            ivImage.visibility = View.VISIBLE
            glide.load(viewModel.imageUrl).into(ivImage)
        } else {
            ivImage.visibility = View.GONE
            glide.clear(ivImage)
        }

        rbIndicator.visibility = if (multipick) View.GONE else View.VISIBLE
        cbIndicator.visibility = if (multipick) View.VISIBLE else View.GONE

        if (viewModel.selected) {
            rbIndicator.isChecked = true
            cbIndicator.isChecked = true
        } else {
            rbIndicator.isChecked = false
            cbIndicator.isChecked = false
        }

        itemView.setOnClickListener {
            if (multipick) {
                if (cbIndicator.isChecked) {
                    cbIndicator.isChecked = false
                    listener.onClick(viewModel)
                } else {
                    cbIndicator.isChecked = true
                    listener.onClick(viewModel)
                }
            } else {
                rbIndicator.isChecked = !viewModel.selected
                listener.onClick(viewModel)
            }
        }
    }

    override fun recycle() {
        glide.clear(ivImage)
    }
}
