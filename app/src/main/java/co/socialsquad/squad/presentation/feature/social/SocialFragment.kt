package co.socialsquad.squad.presentation.feature.social

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.audio.create.AudioCreateActivity
import co.socialsquad.squad.presentation.feature.audio.record.AudioRecordActivity
import co.socialsquad.squad.presentation.feature.chat.chatlist.ChatListActivity
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.navigation.ReselectedListener
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.social.viewholder.LivesListViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.feature.squad.social.EXTRA_CIRCULAR_REVEAL_X
import co.socialsquad.squad.presentation.feature.squad.social.EXTRA_CIRCULAR_REVEAL_Y
import co.socialsquad.squad.presentation.feature.squad.social.SocialSquadActivity
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.feature.video.VideoManager
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.RecyclerViewUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.isVisible
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_social.*
import java.io.File
import javax.inject.Inject

class SocialFragment : Fragment(), SocialView, ReselectedListener {

    companion object {
        const val REQUEST_CODE_PERMISSIONS_FOR_PHOTO = 1
        const val REQUEST_CODE_PERMISSIONS_FOR_VIDEO = 2
        const val REQUEST_CODE_PERMISSIONS_FOR_LIVE = 3
        const val REQUEST_CODE_PERMISSIONS_FOR_AUDIO_RECORD = 4
        const val REQUEST_CODE_EDIT_MEDIA = 61
        const val REQUEST_CODE_EDIT_LIVE = 62
        const val REQUEST_CODE_LIVE_SCHEDULE_UPLOAD = 15
        const val EXTRA_UPLOAD_DESCRIPTION = "EXTRA_UPLOAD_DESCRIPTION"
        const val EXTRA_UPLOAD_DOWNLINE = "EXTRA_UPLOAD_DOWNLINE"
        const val EXTRA_UPLOAD_METRIC_VALUE = "EXTRA_UPLOAD_METRIC_VALUE"
        const val EXTRA_UPLOAD_MEMBERS = "EXTRA_UPLOAD_MEMBERS"
        const val EXTRA_UPLOAD_SCHEDULE = "EXTRA_UPLOAD_SCHEDULE"
    }

    @Inject
    lateinit var socialPresenter: SocialPresenter

    private var loading: AlertDialog? = null
    private var socialAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: PrefetchingLinearLayoutManager? = null
    private var isLoading: Boolean = false
    private var currentMostVisibleItemPosition = -1

    private var socialDeleteDialog: SocialDeleteDialog? = null

    //region: fragment methods

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_social, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (BuildConfig.DEBUG) {
            clChat.visibility = View.VISIBLE
        } else {
            clChat.visibility = View.GONE
        }
        setupToolbar()
        setupSwipeRefresh()
        upbLoader.setOnClickListeners(onTryAgainClicked, onCancelUpload)
        socialPresenter.onViewCreated(this)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        socialAdapter?.apply {
            if (!isVisibleToUser) {
                setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
                rvPosts?.stopScroll()
                view?.visibility = View.INVISIBLE
            } else {
                setMostVisibleItemPosition(currentMostVisibleItemPosition, -1)
                view?.visibility = View.VISIBLE
                socialPresenter.onVisibleToUser()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        socialPresenter.onResume()
        setMostVisibleItemPosition(currentMostVisibleItemPosition, -1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activity?.let { socialPresenter.onActivityResult(it, requestCode, resultCode, data) }
    }

    override fun onPause() {
        super.onPause()
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
    }

    override fun onDestroyView() {
        socialPresenter.onDestroyView()
        VideoManager.release()
        super.onDestroyView()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        socialPresenter.onRequestPermissionsResult(requestCode, grantResults)
    }

    //endregion

    private fun setupToolbar() {
        ivLogo.setOnClickListener {
            val locationOnScreen = IntArray(2).apply { it.getLocationOnScreen(this) }
            val intent = Intent(activity, SocialSquadActivity::class.java)
                .putExtra(EXTRA_CIRCULAR_REVEAL_X, locationOnScreen[0] + (it.width / 2))
                .putExtra(EXTRA_CIRCULAR_REVEAL_Y, locationOnScreen[1] + (it.height / 2))
            startActivity(intent)
        }
        ivChat.setOnClickListener { openChatList() }
        ivAdd.setOnClickListener { socialPresenter.onCreateClicked() }
    }

    private fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener {
            hideEmptyState()
            socialPresenter.onRefresh()
            isLoading = true
        }
    }

    override fun showCreateActionButton(isVisible: Boolean) {
        ivAdd.isVisible = isVisible
    }

    override fun updateFeed() {
        srlLoading.isRefreshing = true
        socialPresenter.onRefresh()
        isLoading = true
    }

    private val onShareListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            socialPresenter.onShareClicked()
        }
    }

    override fun setupList(audioEnabled: Boolean) {
        activity?.let {
            socialAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is PostImageViewModel -> SOCIAL_POST_IMAGE_VIEW_MODEL_ID
                    is PostVideoViewModel, is PostLiveViewModel -> SOCIAL_POST_VIDEO_VIEW_MODEL_ID
                    is LiveViewModel -> SOCIAL_LIVE_VIEW_MODEL_ID
                    is LiveListViewModel -> SOCIAL_LIVE_LIST_VIEW_MODEL_ID
                    is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                    return when (viewType) {
                        SOCIAL_POST_IMAGE_VIEW_MODEL_ID -> PostImageViewHolder(
                            view,
                            onDeleteListener,
                            onEditListener,
                            onLikeListener,
                            onLikeCountListener,
                            onCommentListener,
                            onCommentCountListener,
                            onShareListener
                        )
                        SOCIAL_POST_VIDEO_VIEW_MODEL_ID -> PostVideoViewHolder(
                            view,
                            onVideoClickedListener,
                            onAudioClickedListener,
                            onDeleteListener,
                            onEditListener,
                            onLikeListener,
                            onLikeCountListener,
                            onCommentListener,
                            onCommentCountListener,
                            onShareListener
                        )
                        SOCIAL_LIVE_LIST_VIEW_MODEL_ID -> LivesListViewHolder(
                            view,
                            onLiveClickedListener
                        )
                        LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                        else -> throw IllegalArgumentException()
                    }
                }
            })
            linearLayoutManager = PrefetchingLinearLayoutManager(it)
            rvPosts?.apply {
                adapter = socialAdapter
                layoutManager = linearLayoutManager
                setItemViewCacheSize(0)
                addOnScrollListener(EndlessScrollListener())
                setHasFixedSize(true)
            }
        }
    }

    private val onCommentCountListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL,
                        viewModel
                    )
                    is PostVideoViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL,
                        viewModel
                    )
                    is PostImageViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL,
                        viewModel
                    )
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
            }
            startActivity(intent)
        }
    }

    private val onCommentListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL,
                        viewModel
                    )
                    is PostVideoViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL,
                        viewModel
                    )
                    is PostImageViewModel -> putExtra(
                        PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL,
                        viewModel
                    )
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
                putExtra(PostDetailsActivity.IS_COMMENTING, true)
            }
            startActivity(intent)
        }
    }

    private val onLikeListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            socialPresenter.likePost(viewModel)
        }
    }

    private val onLikeCountListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(activity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.POST_LIKE)
            }
            startActivity(intent)
        }
    }

    val onLiveClickedListener = object : ViewHolder.Listener<LiveViewModel> {
        override fun onClick(viewModel: LiveViewModel) {
            onLiveClicked(viewModel)
        }
    }

    private val onDeleteListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onDeleteSelected(viewModel.pk, viewModel)
        }
    }

    private val onEditListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onEditSelected(viewModel)
        }
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<PostVideoViewModel> {
        override fun onClick(viewModel: PostVideoViewModel) {
            onVideoClicked(viewModel.hlsUrl)
        }
    }

    private val onAudioClickedListener = object : ViewHolder.Listener<AudioToggledViewModel> {
        override fun onClick(viewModel: AudioToggledViewModel) {
            onAudioToggled(viewModel.audioEnabled)
            val items = socialAdapter?.getItemFromType(PostVideoViewModel::class.java)
            items?.forEach {
                it.audioEnabled = viewModel.audioEnabled
                socialAdapter?.update(it, false)
            }
        }
    }

    override fun requestPermissions(requestCode: Int, permissions: Array<String>) {
        requestPermissions(permissions, requestCode)
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(activity, getString(resId), onDismissListener)
    }

    fun onEditSelected(viewModel: PostViewModel) {
        socialPresenter.onEditSelected(viewModel)
    }

    fun onDeleteSelected(pk: Int, viewModel: ViewModel) {
        activity?.let {
            socialDeleteDialog = SocialDeleteDialog(
                it,
                object : SocialDeleteDialog.Listener {
                    override fun onNoClicked() {
                        hideDeleteDialog()
                    }

                    override fun onYesClicked() {
                        socialPresenter.onDeleteClicked(pk, viewModel)
                        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
                    }
                }
            )

            if (!it.isFinishing) socialDeleteDialog?.show()
        }
    }

    override fun hideDeleteDialog() {
        socialDeleteDialog?.dismiss()
    }

    override fun setItems(posts: List<SocialViewModel>) {
        socialAdapter?.setItems(posts)
    }

    override fun addItems(posts: List<SocialViewModel>) {
        socialAdapter?.addItems(posts)
    }

    override fun addPost(post: SocialViewModel) {
        socialAdapter?.addItems(arrayListOf(post))
    }

    override fun addItems(position: Int, socialViewModel: SocialViewModel) {
        socialAdapter?.addItem(socialViewModel, position)
    }

    override fun enableRefreshing() {
        srlLoading.isEnabled = true
    }

    override fun startRefreshing() {
        srlLoading.isRefreshing = true
        isLoading = true
    }

    override fun stopRefreshing() {
        srlLoading.isRefreshing = false
        isLoading = false
    }

    override fun startLoading() {
        isLoading = true
        socialAdapter?.startLoading()
    }

    override fun stopLoading() {
        isLoading = false
        socialAdapter?.stopLoading()
    }

    override fun removePost(viewModel: ViewModel) {
        socialAdapter?.remove(viewModel)
    }

    fun onLiveClicked(liveViewModel: LiveViewModel) {
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)

        when {
            liveViewModel.isLive ->
                Intent(activity, LiveViewerActivity::class.java)
                    .putExtra(LiveViewerActivity.LIVE_VIEW_MODEL, liveViewModel)
                    .putExtra(LiveViewerActivity.PK, liveViewModel.pk)
            liveViewModel.isScheduledLive ->
                Intent(activity, ScheduledLiveDetailsActivity::class.java)
                    .putExtra(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_VIEW_MODEL, liveViewModel)
            liveViewModel.isVideo -> {
                Intent(activity, VideoActivity::class.java)
                    .putExtra(VideoActivity.EXTRA_VIDEO_URL, liveViewModel.videoUrl)
            }
            else -> null
        }?.let { startActivityForResult(it, REQUEST_UPDATE_FEED) }
    }

    fun onVideoClicked(url: String) {
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
        val intent = Intent(activity, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    override fun onReturnVideo(playbackPosition: Long) {
        continueVideo(currentMostVisibleItemPosition, playbackPosition)
    }

    fun onAudioToggled(audioEnabled: Boolean) {
        socialPresenter.onAudioToggled(audioEnabled)
    }

    override fun showPhotoChooser(file: File, requestCode: Int) {
        activity?.let {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(it.packageManager) != null) {
                val uri =
                    FileProvider.getUriForFile(
                        it,
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        file
                    )
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                startActivityForResult(intent, requestCode)
            }
        }
    }

    override fun showVideoChooser(file: File, requestCode: Int) {
        activity?.let {
            val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            if (intent.resolveActivity(it.packageManager) != null) {
                val uri =
                    FileProvider.getUriForFile(
                        it,
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        file
                    )
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                startActivityForResult(intent, requestCode)
            }
        }
    }

    override fun showGalleryChooser(requestCode: Int) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*", "video/*"))
        startActivityForResult(Intent.createChooser(intent, null), requestCode)
    }

    override fun showAudioChooser(requestCode: Int) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "audio/*"
        startActivityForResult(intent, requestCode)
    }

    override fun showCreateLive(intent: Intent) {
        startActivityForResult(intent, REQUEST_CODE_LIVE_SCHEDULE_UPLOAD)
    }

    override fun showCreateFromPhoto(intent: Intent, requestCode: Int) {
        startActivityForResult(intent, requestCode)
    }

    override fun showCreateFromVideo(intent: Intent, requestCode: Int) {
        startActivityForResult(intent, requestCode)
    }

    override fun showCreateFromGallery(intent: Intent, requestCode: Int) {
        startActivityForResult(intent, requestCode)
    }

    override fun showEditMedia(post: PostViewModel) {
        val intent = Intent(activity, SocialCreateMediaActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, REQUEST_CODE_EDIT_MEDIA)
    }

    override fun showEditLive(post: PostLiveViewModel) {
        val intent = Intent(activity, SocialCreateLiveActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, REQUEST_CODE_EDIT_LIVE)
    }

    override fun showLoading() {
        loading = ViewUtils.createLoadingOverlay(activity)
        if (isVisible) loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun showUploadContainer(
        thumbnailMedia: Bitmap,
        isVideoMedia: Boolean,
        currentUpload: Int,
        totalUpload: Int
    ) {
        upbLoader.visibility = View.VISIBLE
        upbLoader.setThumbnail(thumbnailMedia, isVideoMedia)
        if (currentUpload > 1) {
            upbLoader.incrementUploadNumber()
        }
        upbLoader.setNumberOfUploads(totalUpload)
    }

    override fun hideUploadContainer() {
        upbLoader.finish()
    }

    override fun startUpload(text: String) {
        upbLoader.startUpload(text)
    }

    override fun startUploadIndeterminate(string: String) {
        upbLoader.startIndeteminate(string)
    }

    override fun setUploadError(text: String) {
        upbLoader.setError(text)
    }

    private val onTryAgainClicked = View.OnClickListener { socialPresenter.onTryAgainClicked() }

    private val onCancelUpload = View.OnClickListener { socialPresenter.onCancelUploadClicked() }

    override fun showErrorMessageWaitCurrentUpload() {
        ViewUtils.showDialog(
            activity, getString(R.string.social_error_upload_already_in_progress),
            null
        )
    }

    override fun incrementUploadProgressBy(increment: Int) {
        upbLoader.incrementProgress(increment)
    }

    override fun showCreateDialog(
        canCreateFromCamera: Boolean,
        canCreateFromGallery: Boolean,
        canCreateLive: Boolean,
        canCreateAudio: Boolean
    ) {
        activity?.let {
            if (!it.isFinishing)
                SocialCreateDialog(
                    it,
                    canCreateFromCamera,
                    canCreateFromGallery,
                    canCreateLive,
                    canCreateAudio,
                    socialPresenter as SocialCreateDialog.Listener
                ).show()
        }
    }

    private inner class EndlessScrollListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 8

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            linearLayoutManager?.apply {
                if (!isLoading && findLastVisibleItemPosition() > itemCount - visibleThreshold) {
                    socialPresenter.onScrolledBeyondVisibleThreshold()
                }

                mostVisibleView(recyclerView, this)
            }
        }
    }

    private fun mostVisibleView(
        recyclerView: RecyclerView,
        linearLayoutManager: LinearLayoutManager
    ) {
        val rect = Rect()
        recyclerView.getGlobalVisibleRect(rect)
        val containerHeight = rect.height()
        val referencePoint = containerHeight / 2 + rect.top

        val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
        val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()

        val previousMostVisibleItemPosition = currentMostVisibleItemPosition

        var mostVisibleItemPosition = firstVisibleItemPosition
        var leastDifferenceFromReferencePoint = containerHeight
        for (i in firstVisibleItemPosition..lastVisibleItemPosition) {
            val viewHolder = recyclerView.findViewHolderForAdapterPosition(i)
            if (viewHolder != null) {
                val r = Rect()
                viewHolder.itemView.getGlobalVisibleRect(r)
                val difference = Math.abs(r.centerY() - referencePoint)
                if (difference < leastDifferenceFromReferencePoint) {
                    leastDifferenceFromReferencePoint = difference
                    mostVisibleItemPosition = i
                }
            }
        }

        if (previousMostVisibleItemPosition != mostVisibleItemPosition) {
            currentMostVisibleItemPosition = mostVisibleItemPosition
            setMostVisibleItemPosition(
                mostVisibleItemPosition,
                previousMostVisibleItemPosition
            )
        }
    }

    private fun setMostVisibleItemPosition(
        mostVisibleItemPosition: Int,
        previousMostVisibleItemPosition: Int
    ) {
        if (!userVisibleHint) {
            stopVideo(previousMostVisibleItemPosition)
            return
        }
        stopVideo(previousMostVisibleItemPosition)
        playVideo(mostVisibleItemPosition)
    }

    private fun stopVideo(position: Int) {
        if (position == -1) {
            return
        }
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.stop()
        }
    }

    private fun playVideo(position: Int) {
        if (position == -1) {
            return
        }
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play()
        }
    }

    private fun continueVideo(position: Int, playbackPosition: Long) {
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play(playbackPosition)
        }
    }

    override fun updatePost(post: PostViewModel) {}

    override fun onReselected() {
        linearLayoutManager?.let {
            activity?.let { activity ->
                RecyclerViewUtils.scrollToTop(
                    activity,
                    it
                )
            }
        }
    }

    override fun scrollToTop() {
        linearLayoutManager?.let {
            activity?.let { activity ->
                RecyclerViewUtils.scrollToTop(
                    activity,
                    it
                )
            }
        }
    }

    override fun openAudioCreate(uri: Uri) {
        val intent = Intent(activity, AudioCreateActivity::class.java).apply {
            putExtra(AudioCreateActivity.EXTRA_AUDIO_GALLERY_FILE_URI, uri.toString())
        }
        startActivity(intent)
    }

    override fun openAudioRecord() {
        val intent = Intent(activity, AudioRecordActivity::class.java)
        startActivity(intent)
    }

    override fun showChat() {
        ivChat.visibility = View.VISIBLE
    }

    override fun hideChat() {
        ivChat.visibility = View.GONE
    }

    private fun openChatList() {
        startActivity(Intent(activity, ChatListActivity::class.java))
    }

    override fun setMessagesBadgeColor(color: String) {
        if (color.isEmpty()) return
        vMessageBadge.backgroundTintList = ColorUtils.stateListOf(color)
    }

    override fun showMessagesBadge(messagesNumeber: Int) {
        vMessageBadge.visibility = View.VISIBLE
        vMessageBadge.text = minOf(messagesNumeber, 99).toString()
    }

    override fun hideMessagesBadge() {
        vMessageBadge?.visibility = View.GONE
    }

    override fun showEmptyState() {
        empty_container.visibility = View.VISIBLE
        createPublication.setOnClickListener { socialPresenter.onCreateClicked() }
    }

    override fun hideEmptyState() {
        empty_container.visibility = View.GONE
    }
}
