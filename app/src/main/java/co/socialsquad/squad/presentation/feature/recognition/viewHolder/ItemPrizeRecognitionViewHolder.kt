package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_recognition_prize.view.*
import kotlinx.android.synthetic.main.item_recognition_prize.view.mainLayout
import kotlinx.android.synthetic.main.item_recognition_prize.view.txtSubTitle
import kotlinx.android.synthetic.main.item_recognition_prize.view.txtSubtitle2
import kotlinx.android.synthetic.main.item_recognition_prize.view.txtTitle


class ItemPrizeRecognitionViewHolder(itemView: View, var onClickAction: (prize: ItemPrizeRecognitionViewModel) -> Unit) : ViewHolder<ItemPrizeRecognitionViewModel>(itemView) {

    override fun bind(viewModel: ItemPrizeRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }


        itemView.txtTitle.setTextColor(ColorUtils.parse(getCompanyColor()!!))
        itemView.txtSubTitle.text = viewModel.prizeData.title
        itemView.txtSubtitle2.text = viewModel.prizeData.description
        // itemView.dividerItemTop.isVisible = viewModel.isFirst.not()



        viewModel.prizeData.highlight?.let {
            itemView.txtTitle.isVisible = true
            itemView.txtTitle.text = it
        }

        itemView.txtTitle.isVisible =  viewModel.prizeData.highlight.isNullOrBlank().not()

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.prizeData.icon), itemView.cover)


        // Glide.with(itemView.context).load(viewModel.prizeData.media_cover).into(itemView.prizeCover)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}