package co.socialsquad.squad.presentation.feature.hub

interface HubView {
    fun setupToolbar(logo: String?, backgroundColor: String?)
    fun setupTabs(itemsPermitted: MutableList<HubItem>, indicatorColor: String?, textColor: String?)
}
