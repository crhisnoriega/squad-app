package co.socialsquad.squad.presentation.feature.video.list

import android.app.Activity
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.entity.Recommendation
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.repository.VideoRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_VIDEO_LIST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_PLAY_VIDEO
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class VideoListPresenter @Inject constructor(
    private val view: VideoListView,
    private val videoRepository: VideoRepository,
    private val profileRepository: ProfileRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var qualifications: List<Qualification> = listOf()
    private var currentRecommending: VideoListItemViewModel? = null
    private var page = 1
    private var isLoading = true
    private var isComplete = false

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_VIDEO_LIST)
        subscribe()
        getQualifications()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
        getQualifications()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            videoRepository.getVideoList(page)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }.subscribe(
                    {
                        it.takeIf { it.results.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            val list = it.results.orEmpty()
                                .filter { it.type == Post.TYPE_VIDEO && it.medias.orEmpty().any { FileUtils.isVideoMedia(it) } }
                                .map { VideoListItemViewModel(it) }
                            if (page == 1) view.setItems(list) else view.addItems(list)
                            if (it.next == null) isComplete = true
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.video_list_error_get_list)
                    }
                )
        )
    }

    private fun getQualifications() {
        compositeDisposable.add(
            profileRepository.getQualifications()
                .subscribe(
                    { qualifications = it },
                    { it.printStackTrace() }
                )
        )
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED || data == null) return

        if (requestCode == SegmentValuesPresenter.REQUEST_CODE_VALUES) {
            (data.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                recommendVideo(it.values.map { it.value })
            }
        }
    }

    fun onRecommending(viewModel: VideoListItemViewModel) {
        currentRecommending = viewModel
    }

    private fun recommendVideo(values: List<String>) {
        val pks = mutableListOf<Int>()
        values.forEach { name ->
            qualifications.findLast { it.name == name }?.let {
                pks.add(it.pk)
            }
        }
        currentRecommending?.let {
            val recommendation = RecommendationRequest(it.pk, Recommendation.FEED.name.toLowerCase(), pks)
            recommend(recommendation)
        }
    }

    private fun recommend(recommendation: RecommendationRequest) {
        compositeDisposable.add(
            profileRepository.postRecommendation(recommendation)
                .subscribe(
                    { view.showMessage(R.string.profile_recommendation_success) },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.profile_recommendation_error)
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }

    fun onVideoClicked(viewModel: VideoListItemViewModel) {
        tagWorker.tagEvent(TAG_TAP_PLAY_VIDEO)
        compositeDisposable.add(
            videoRepository.markAsViewed(viewModel.videoPk.toLong())
                .subscribe({}, { it.printStackTrace() })
        )
    }
}
