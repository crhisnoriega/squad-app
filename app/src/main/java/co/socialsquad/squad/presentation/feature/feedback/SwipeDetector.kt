package co.socialsquad.squad.presentation.feature.feedback

import android.util.Log
import android.view.MotionEvent
import android.view.View

class SwipeDetector(private val v: View) : View.OnTouchListener {

    private var minDistance = 100
    private var downX: Float = 0.toFloat()
    private var downY: Float = 0.toFloat()
    private var upX: Float = 0.toFloat()
    private var upY: Float = 0.toFloat()

    private var swipeEventListener: OnSwipeEvent? = null

    init {
        v.setOnTouchListener(this)
    }

    fun setOnSwipeListener(listener: OnSwipeEvent) {
        try {
            swipeEventListener = listener
        } catch (e: ClassCastException) {
            Log.e("ClassCastException", "please pass SwipeDetector.onSwipeEvent Interface instance", e)
        }
    }

    private fun onRightToLeftSwipe() {
        if (swipeEventListener != null)
            swipeEventListener!!.swipeEventDetected(v, SwipeTypeEnum.RIGHT_TO_LEFT)
        else
            Log.e("SwipeDetector error", "please pass SwipeDetector.onSwipeEvent Interface instance")
    }

    private fun onLeftToRightSwipe() {
        if (swipeEventListener != null)
            swipeEventListener!!.swipeEventDetected(v, SwipeTypeEnum.LEFT_TO_RIGHT)
        else
            Log.e("SwipeDetector error", "please pass SwipeDetector.onSwipeEvent Interface instance")
    }

    private fun onTopToBottomSwipe() {
        if (swipeEventListener != null)
            swipeEventListener!!.swipeEventDetected(v, SwipeTypeEnum.TOP_TO_BOTTOM)
        else
            Log.e("SwipeDetector error", "please pass SwipeDetector.onSwipeEvent Interface instance")
    }

    private fun onBottomToTopSwipe() {
        if (swipeEventListener != null)
            swipeEventListener!!.swipeEventDetected(v, SwipeTypeEnum.BOTTOM_TO_TOP)
        else
            Log.e("SwipeDetector error", "please pass SwipeDetector.onSwipeEvent Interface instance")
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                downX = event.x
                downY = event.y
                return true
            }
            MotionEvent.ACTION_UP -> {
                upX = event.x
                upY = event.y

                val deltaX = downX - upX
                val deltaY = downY - upY

                if (Math.abs(deltaX) > Math.abs(deltaY)) {
                    if (Math.abs(deltaX) > minDistance) {
                        if (deltaX < 0) {
                            this.onLeftToRightSwipe()
                            return true
                        }
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe()
                            return true
                        }
                    } else {
                        return false
                    }
                } else {
                    if (Math.abs(deltaY) > minDistance) {
                        if (deltaY < 0) {
                            this.onTopToBottomSwipe()
                            return true
                        }
                        if (deltaY > 0) {
                            this.onBottomToTopSwipe()
                            return true
                        }
                    } else {
                        return false
                    }
                }
                return true
            }
        }
        return false
    }

    interface OnSwipeEvent {
        fun swipeEventDetected(v: View, SwipeType: SwipeTypeEnum)
    }

    enum class SwipeTypeEnum {
        RIGHT_TO_LEFT, LEFT_TO_RIGHT, TOP_TO_BOTTOM, BOTTOM_TO_TOP
    }
}
