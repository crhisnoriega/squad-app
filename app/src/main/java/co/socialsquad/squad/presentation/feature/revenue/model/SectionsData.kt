package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SectionsData(
    @SerializedName("section_id") val section_id: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("subtitle") val subtitle: String?,
    @SerializedName("estimated_earnings") val estimated_earnings: String?,
    @SerializedName("estimated_receipt") val estimated_receipt: String?,

    @SerializedName("icon") val icon: String?,
    var first: Boolean = false


    ) : Parcelable