package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ProductLocationDisabledViewModel

class ProductLocationDisabledViewHolder(itemView: View, val listener: Listener<ProductLocationDisabledViewModel>) : ViewHolder<ProductLocationDisabledViewModel>(itemView) {
    override fun bind(viewModel: ProductLocationDisabledViewModel) {
        itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    override fun recycle() {}
}
