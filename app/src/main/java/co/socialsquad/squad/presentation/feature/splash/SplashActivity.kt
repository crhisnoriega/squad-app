package co.socialsquad.squad.presentation.feature.splash

import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.core.app.ActivityCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.service.CALL_SERVICE_ACTION_START
import co.socialsquad.squad.data.service.SignallingService
import co.socialsquad.squad.presentation.feature.about.AboutActivity
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.event.detail.EventDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.SectionsViewModel
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.login.forgotpassword.ResetPasswordActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation.ProfileEditHinodeConfirmationActivity
import co.socialsquad.squad.presentation.feature.signup.accesssquad.AccessSquadActivity
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.store.product.ProductActivity
import co.socialsquad.squad.presentation.feature.store.product.ProductPresenter
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import org.koin.android.viewmodel.ext.android.viewModel
import javax.inject.Inject

class SplashActivity : BaseDaggerActivity(), SplashView {

    companion object {
        const val LIVE_PK_FROM_NOTIFICATION = "LIVE_PK_FROM_NOTIFICATION"
        const val SCHEDULED_LIVE_PK_FROM_NOTIFICATION = "SCHEDULED_LIVE_PK_FROM_NOTIFICATION"
        const val POST_PK_FROM_NOTIFICATION = "POST_PK_FROM_NOTIFICATION"
        const val AUDIO_PK_FROM_NOTIFICATION = "AUDIO_PK_FROM_NOTIFICATION"
    }

    private val viewModel: SectionsViewModel by viewModel()

    @Inject
    lateinit var splashPresenter: SplashPresenter

    private var nextActivityIntent: Intent? = null

    override fun onPause() {
        super.onPause()
        splashPresenter.onPause()
    }

    override fun onResume() {
        super.onResume()
        splashPresenter.onResume(nextActivityIntent)
    }

    override fun onDestroy() {
        splashPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFlagLayoutFullscreen()
        splashPresenter.onCreate(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        this.intent = intent

        splashPresenter.onNewIntent(this)
    }

    override fun setNextAsNavigation() {
        viewModel.fetchAndSaveSections()
        nextActivityIntent = Intent(this, NavigationActivity::class.java)
    }

    override fun setNextAsWalkthrough() {
        nextActivityIntent = Intent(this, OnboardingActivity::class.java)
    }

    override fun setNextAsSignupHinodeConfirmation() {
        nextActivityIntent = Intent(this, ProfileEditHinodeConfirmationActivity::class.java)
    }

    override fun setNextAsLiveViewer(pk: Int) {
        nextActivityIntent = Intent(this, LiveViewerActivity::class.java)
        nextActivityIntent?.putExtra(LiveViewerActivity.PK, pk)
    }

    override fun setNextAsPostViewer(pk: Int) {
        nextActivityIntent = Intent(this, PostDetailsActivity::class.java)
        nextActivityIntent?.putExtra(PostDetailsActivity.POST_DETAIL_PK, pk)
    }

    override fun setNextAsAudioDetail(pk: Int) {
        nextActivityIntent = Intent(this, AudioDetailActivity::class.java)
        nextActivityIntent?.putExtra(AudioDetailActivity.EXTRA_AUDIO_PK, pk)
    }

    override fun setNextAsProduct(pk: Int) {
        nextActivityIntent = Intent(this, ProductActivity::class.java)
        nextActivityIntent?.putExtra(ProductPresenter.EXTRA_PK, pk)
    }

    override fun setNextAsEvent(pk: Int) {
        nextActivityIntent = Intent(this, EventDetailActivity::class.java)
        nextActivityIntent?.putExtra(EventDetailActivity.EXTRA_EVENT_PK, pk)
    }

    override fun showNextActivity() {
        startActivity(nextActivityIntent)
        ActivityCompat.finishAfterTransition(this)
    }

    override fun setNextAsLiveDetail(pk: Int) {
        nextActivityIntent = Intent(this, ScheduledLiveDetailsActivity::class.java)
        nextActivityIntent?.putExtra(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_PK, pk)
    }

    override fun showErrorMessageFailedToConnect() {
        ViewUtils.showDialog(
            this,
            getString(R.string.splash_error_failed_to_connect),
            getString(R.string.splash_error_failed_to_connect_button).toUpperCase()
        ) { splashPresenter.onDismissDialog() }
    }

    override fun showUpdateDialog() {
        val listener = DialogInterface.OnClickListener { dialog, _ ->
            dialog.dismiss()
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$packageName")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            }
        }
        ViewUtils.showNotCancelableDialog(
            this,
            getString(R.string.splash_update_available),
            getString(R.string.splash_update_available_message),
            getString(R.string.splash_update_available_action),
            listener
        )
    }

    override fun onStart() {
        super.onStart()
        splashPresenter.onStart(this, intent)
    }

    override fun preloadImages(url: String?) {
        Glide.with(this.applicationContext)
            .asDrawable()
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    showNextActivity()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    showNextActivity()
                    return true
                }
            })
            .submit()
    }

    override fun startCallService() {
        val intent = Intent(this, SignallingService::class.java).apply {
            action = CALL_SERVICE_ACTION_START
        }
        startService(intent)
    }

    override fun openNavigation() {
        viewModel.fetchAndSaveSections()

        val intent = Intent(this, NavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    override fun openCompleteRegistration() {
        viewModel.fetchAndSaveSections()

        startActivity(Intent(this, KickoffActivity::class.java))
        finish()
    }

    override fun moveToPassword(register: String, subdomain: String) {
        val intent = Intent(this, AboutActivity::class.java)
        intent.putExtra(AboutActivity.extraRegister, register)
        intent.putExtra(AboutActivity.extraSubdomain, subdomain)
        intent.putExtra(AboutActivity.extraHasBack, false)
        startActivity(intent)
        finish()
    }

    override fun moveToAccessSquad(token: String, email: String) {
        val intent = Intent(baseContext, AccessSquadActivity::class.java)
        intent.putExtra(AccessSquadActivity.extraToken, token)
        intent.putExtra(AccessSquadActivity.extraEmail, email)
        startActivity(intent)
        finish()
    }

    override fun moveToResetPassword(token: String, subdomain: String, email: String) {
        val intent = Intent(baseContext, ResetPasswordActivity::class.java)
        intent.putExtra(ResetPasswordActivity.extraToken, token)
        intent.putExtra(ResetPasswordActivity.extraSubdomain, subdomain)
        intent.putExtra(ResetPasswordActivity.extraEmail, email)
        startActivity(intent)
        finish()
    }

    override fun openSubdomainsScreen(companies: List<Company>, email: String) {
        val gson = Gson()
        val json = gson.toJson(companies)

        val intent = Intent(baseContext, AccessSquadActivity::class.java)
        intent.putExtra(AccessSquadActivity.extraEmail, email)
        intent.putExtra(AccessSquadActivity.extraCompanies, json)
        intent.putExtra(AccessSquadActivity.extraIsLogin, true)
        intent.putExtra(AccessSquadActivity.extraIsMagicLink, true)

        startActivity(intent)
    }
}
