package co.socialsquad.squad.presentation.feature.audio.create

import dagger.Binds
import dagger.Module

@Module
abstract class AudioCreateModule {
    @Binds
    abstract fun view(audioCreateActivity: AudioCreateActivity): AudioCreateView
}
