package co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetailsModel
import co.socialsquad.squad.presentation.feature.opportunityv2.repository.OpportunityDetailsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class OpportunityDetailsViewModel(
        var opportunityRepository: OpportunityDetailsRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    private val opportunityDetails = MutableStateFlow<Resource<OpportunityDetailsModel>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    private val timetableDetails = MutableStateFlow<Resource<Timetable>>(Resource.loading(null))


    @ExperimentalCoroutinesApi
    private val deleteOpportunity = MutableStateFlow<Resource<String>>(Resource.empty())

    @ExperimentalCoroutinesApi
    fun opportunityDetails(): StateFlow<Resource<OpportunityDetailsModel>> {
        return opportunityDetails
    }

    @ExperimentalCoroutinesApi
    fun timeTableDetails(): StateFlow<Resource<Timetable>> {
        return timetableDetails
    }

    @ExperimentalCoroutinesApi
    fun deleteOpportunity(): StateFlow<Resource<String>> {
        return deleteOpportunity
    }

    @ExperimentalCoroutinesApi
    fun deleteOpportunityDetails(submission_id: String) {
        viewModelScope.launch {
            opportunityRepository.deleteOpportunityDetails(submission_id)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        opportunityDetails.value = Resource.error("", null)
                    }
                    .collect {
                        Log.i("tag10", it.isSuccessful.toString())
                        opportunityDetails.value = Resource.delete()

                    }
        }
    }


    @ExperimentalCoroutinesApi
    fun fetchDetailsTimetable(submission_id: String) {
        viewModelScope.launch {
            opportunityDetails.value = Resource.loading(null)
            opportunityRepository.fetchDetailsTimetable(submission_id)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        timetableDetails.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        timetableDetails.value = Resource.success(it)
                    }
        }
    }


    @ExperimentalCoroutinesApi
    fun fetchOpportunityDetails(submission_id: String) {
        viewModelScope.launch {
            opportunityDetails.value = Resource.loading(null)
            opportunityRepository.fetchOpportunityDetails(submission_id)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        opportunityDetails.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        opportunityDetails.value = Resource.success(it)
                    }
        }
    }

}
