package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichTextBodyText(var text: String? = null) : ViewType, Parcelable {
    constructor(richTextVo: RichTextVO) : this() {
        this.text = richTextVo.text
    }

    override fun getViewType(): Int {
        return RichTextDataType.TEXT.value
    }
}
