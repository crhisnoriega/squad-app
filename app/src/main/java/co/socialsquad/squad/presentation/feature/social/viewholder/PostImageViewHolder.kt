package co.socialsquad.squad.presentation.feature.social.viewholder

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import androidx.constraintlayout.widget.ConstraintLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_social_post_content.view.*
import kotlinx.android.synthetic.main.view_social_post_header.view.*
import kotlinx.android.synthetic.main.view_social_post_image.view.*
import kotlinx.android.synthetic.main.view_social_post_interactions.view.*
import java.util.*

class PostImageViewHolder(
        itemView: View,
        private val onDeleteListener: Listener<PostViewModel>? = null,
        private val onEditListener: Listener<PostViewModel>? = null,
        private val onLikeListener: Listener<PostViewModel>? = null,
        private val onLikeCountListener: Listener<PostViewModel>? = null,
        private val onCommentListener: Listener<PostViewModel>? = null,
        private val onCommentCountListener: Listener<PostViewModel>? = null,
        private val onShareListener: Listener<PostViewModel>? = null
) : ViewHolder<PostImageViewModel>(itemView) {

    private val glide = Glide.with(itemView)
    private var likesViewHolder: SocialLikesViewHolder<PostViewModel>? = null

    override fun bind(viewModel: PostImageViewModel) {
        with(viewModel) {
            setupHeader(viewModel)
            setupDescription(viewModel)
            setupImage(viewModel)
            val showInteractionsLogic = { itemView.llInteractionsText.visibility = if (commentsCount > 0 || likeCount > 0) View.VISIBLE else View.GONE }
            showInteractionsLogic()
            val socialLikesViewModel = SocialLikesViewModel(currentUserAvatar, liked, likeAvatars, likeCount)

            val holderOnLikeListener = object : ViewHolder.Listener<PostViewModel> {
                override fun onClick(viewModel: PostViewModel) {
                    viewModel.liked = socialLikesViewModel.liked
                    viewModel.likeCount = socialLikesViewModel.likeCount
                    viewModel.likeAvatars = socialLikesViewModel.likeAvatars
                    showInteractionsLogic()
                    onLikeListener?.onClick(viewModel)
                }
            }

            likesViewHolder = SocialLikesViewHolder(itemView, this, holderOnLikeListener, onLikeCountListener!!).apply {
                bind(socialLikesViewModel)
            }
            SocialShareViewHolder(itemView, this, onShareListener).bind(shareViewModel)
            SocialCommentsViewHolder(itemView, this, onCommentListener!!, onCommentCountListener!!)
                    .bind(SocialCommentsViewModel(commentsCount))
        }
    }

    private fun setupHeader(viewModel: PostImageViewModel) {
        with(viewModel) {
            itemView.apply {
                glide.load(creatorAvatar)
                        .circleCrop()
                        .crossFade()
                        .into(ivAvatar)
                ivAvatar.setOnClickListener {
                    openUserActivity(creatorPK)
                }
                tvAuthor.setOnClickListener {
                    openUserActivity(creatorPK)
                }
                tvAuthor.text = creatorName
                tvElapsedTime.text = createdAt.toDate()?.elapsedTime(context)?.capitalize() ?: ""

                tvAudience.text = context.getString(AudienceViewModel.getStringResId(audience.type))
                ivAudience.setImageResource(AudienceViewModel.getDrawableResId(audience.type))

                setupOptions(viewModel)
            }
        }
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, creatorPK)
        }
        itemView.context.startActivity(intent)
    }

    private fun setupOptions(viewModel: PostViewModel) {
        with(viewModel) {
            itemView.apply {
                val options = object : ArrayList<Int>() {
                    init {
                        if (canEdit) add(R.string.social_post_option_edit)
                        if (canDelete) add(R.string.social_post_option_delete)
                    }
                }

                sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
                sOptions.adapter = SocialSpinnerAdapter(
                        context,
                        R.layout.textview_spinner_dropdown_options_item,
                        R.layout.textview_spinner_dropdown_options,
                        options
                )

                sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                        val item = parent.getItemAtPosition(position) ?: return

                        val itemResId = item as Int
                        when (itemResId) {
                            R.string.social_post_option_delete -> onDeleteListener?.onClick(viewModel)
                            R.string.social_post_option_edit -> onEditListener?.onClick(viewModel)
                        }
                        sOptions.setSelection(0)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {}
                }
            }
        }
    }

    private fun setupDescription(viewModel: PostImageViewModel) {
        with(viewModel) {
            itemView.apply {
                if (description.isNullOrBlank()) {
                    (tvDescription.parent as View).visibility = View.GONE
                } else {
                    tvTitle.text = viewModel.description?.trim()

                    with(tvDescription) {
                        text = description
                        (parent as View).visibility = android.view.View.VISIBLE
                        post {
                            layout?.let {
                                for (line in 0 until it.lineCount) {
                                    if (it.getEllipsisCount(line) > 0) {
                                        setOnClickListener {
                                            androidx.appcompat.app.AlertDialog.Builder(context)
                                                    .setMessage(description)
                                                    .create()
                                                    .show()
                                        }
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setupImage(viewModel: PostImageViewModel) {
        itemView.apply {
            setDimensionRatio(ivImage, viewModel.width, viewModel.height)
            glide.load(viewModel.imageUrl).into(ivImage)
        }
    }

    private fun setDimensionRatio(view: View, width: Int, height: Int) {
        val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.dimensionRatio = "w,$height:$width"
        view.layoutParams = layoutParams
    }

    override fun recycle() {
        itemView.apply {
            glide.clear(ivImage)
            glide.clear(ivAvatar)
            setOnClickListener {}
        }
        likesViewHolder?.recycle()
    }
}
