package co.socialsquad.squad.presentation.feature.signup.credentials

interface SignupCredentialsFragmentListener {
    fun onKeyboardAction()
    fun onTextChanged(text: String)
}
