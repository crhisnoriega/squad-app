package co.socialsquad.squad.presentation.custom.resource.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Address

data class AddressSingleViewHolderModel(val address: Address) : ViewModel