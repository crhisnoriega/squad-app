package co.socialsquad.squad.presentation.feature.store.viewholder

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.EXTRA_PRODUCT
import co.socialsquad.squad.presentation.feature.store.MediaType
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import co.socialsquad.squad.presentation.feature.store.product.ProductActivity
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_product.view.*

class ProductViewHolder(itemView: View) : ViewHolder<StoreProductViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreProductViewModel) {
        with(viewModel) {
            medias?.firstOrNull { it.type is MediaType.Image }?.let {
                glide.load(it.url)
                    .crossFade()
                    .into(itemView.ivImage)
            }

            itemView.short_title_lead.text = name

            if (price != null) {
                price?.let { itemView.tvPrice.text = itemView.context.getString(R.string.products_item_price, it) }
            }

            points?.let { itemView.tvPoints.text = itemView.context.getString(R.string.products_item_points, it) }
            itemView.tvCode.text = itemView.context.getString(R.string.product_overview_code, code)

            itemView.setOnClickListener { openProduct(viewModel) }
        }
    }

    private fun openProduct(viewModel: StoreProductViewModel) {
        val intent = Intent(itemView.context, ProductActivity::class.java)
            .putExtra(EXTRA_PRODUCT, viewModel)
        itemView.context.startActivity(intent)
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }
}
