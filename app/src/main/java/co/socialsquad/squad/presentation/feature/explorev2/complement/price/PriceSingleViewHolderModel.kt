package co.socialsquad.squad.presentation.feature.explorev2.complement.price

import co.socialsquad.squad.domain.model.explore.Price
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement

data class PriceSingleViewHolderModel(val price: Price, val complement: Complement) : ViewModel