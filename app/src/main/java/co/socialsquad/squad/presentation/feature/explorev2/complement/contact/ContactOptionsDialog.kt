package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.os.bundleOf
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.explore.Contact
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.argument
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_complement_email_phone.*
import kotlinx.android.synthetic.main.item_complement_options_dialog.tvTitle

private const val REQUEST_CALL_PERMISSION = 6543
class ContactOptionsDialog: BottomSheetDialogFragment() {

    companion object {
        const val ARG_CONTACT = "ARG_CONTACT"

        fun newInstance(contact: Contact) = ContactOptionsDialog().apply {
            arguments = bundleOf(
                ARG_CONTACT to contact
            )
        }
    }

    private var phoneCall:String = ""
    private val contact by argument<Contact>(ARG_CONTACT)
    private val viewAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is ContactViewHolderModel -> ContactViewHolder
                .VIEW_ITEM_POSITION_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View) = when (viewType) {
            ContactViewHolder.VIEW_ITEM_POSITION_ID -> ContactViewHolder(view) {
                if (it.email != null) {
                    val email = Intent(Intent.ACTION_SEND)
                    email.putExtra(Intent.EXTRA_EMAIL, arrayOf(it.email))
                    email.type = "message/rfc822"
                    startActivity(Intent.createChooser(email, ""))
                }

                if (it.phone != null) {
                    if (checkSelfPermission(requireContext(), Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(Manifest.permission.CALL_PHONE),
                            REQUEST_CALL_PERMISSION
                        )
                    } else {
                        phoneCall = it.phone
                        doCall()
                    }
                }
            }
            else -> throw IllegalArgumentException()
        }
    })

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == REQUEST_CALL_PERMISSION){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doCall()
            }
        }
    }

    private fun doCall(){
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${phoneCall}"))
        startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_complement_email_phone, container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tvTitle.text = contact.title

        var actions = mutableListOf<ViewModel>()

        if(contact.phone != null){
            actions.add(ContactViewHolderModel(contact.phone, null))
        }

        if(contact.email != null){
            actions.add(ContactViewHolderModel(null, contact.email))
        }

        rvActions.adapter = viewAdapter
        viewAdapter.addItems(actions)
    }
}