package co.socialsquad.squad.presentation.custom.tool

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.util.inflate

class LoadingAdapter(@LayoutRes private val layoutRes: Int, private val count: Int) :
    RecyclerView.Adapter<LoadingAdapter.PlaceholderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceholderViewHolder {
        return PlaceholderViewHolder(parent.inflate(layoutRes))
    }

    override fun getItemCount() = count
    override fun onBindViewHolder(holder: PlaceholderViewHolder, position: Int) {}

    class PlaceholderViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
