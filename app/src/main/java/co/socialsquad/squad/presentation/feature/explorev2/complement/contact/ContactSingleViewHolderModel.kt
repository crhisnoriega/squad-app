package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import co.socialsquad.squad.domain.model.explore.Contact
import co.socialsquad.squad.presentation.custom.ViewModel

data class ContactSingleViewHolderModel(val contact: Contact) : ViewModel