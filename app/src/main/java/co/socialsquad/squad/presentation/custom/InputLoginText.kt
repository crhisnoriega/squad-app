package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.addTextChangedListener
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import com.google.android.material.textfield.TextInputLayout
import io.github.inflationx.calligraphy3.TypefaceUtils
import kotlinx.android.synthetic.main.view_login_input_text.view.*

class InputLoginText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private var helpText = ""
    private var haveError = false
    private var onInputListener: InputListener? = null
    private var isPasswordVisible = false
    private var textColors: String? = null

    val editText: EditText?
        get() = tietField

    init {
        View.inflate(context, R.layout.view_login_input_text, this)
        setupStyledAttributes(attrs)
        setupTextWatcher()
    }

    private fun setupTextWatcher() {
        tietField.addTextChangedListener { text ->
            tvHidden.setText(text)
            if (haveError) {
                haveError = false
                setHelpText(helpText)
            }
            onInputListener?.onInput(text)
        }
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputLoginText)) {
            tilHint.hint = getString(R.styleable.InputLoginText_hintText)
            tietField.setText(getString(R.styleable.InputLoginText_fieldText))
            val inputType = getInt(R.styleable.InputLoginText_android_inputType, EditorInfo.TYPE_TEXT_VARIATION_NORMAL)
            tietField.inputType = inputType
            tvHidden.inputType = inputType
            tietField.imeOptions = getInt(R.styleable.InputLoginText_android_imeOptions, 0)
            val helpText = getString(R.styleable.InputLoginText_helpText) ?: ""
            tvLoginHelp.text = helpText
            this@InputLoginText.helpText = helpText
        }
        setCollapsedTextFont()

        if (tietField.inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD || tietField.inputType == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD || tietField.inputType == 0x00000081) {
            ibToggleVisibility.visibility = View.VISIBLE
            setupVisibilityToggle()
        }
    }

    private fun setupVisibilityToggle() {
        ibToggleVisibility.setOnClickListener {
            listOf(tietField, tvHidden).forEach {
                val currentTypeface = it.typeface
                val caretPosition = it.selectionStart
                it.inputType = if (isPasswordVisible) {
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                } else {
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                }
                it.setSelection(caretPosition)
                it.typeface = currentTypeface
            }
            isPasswordVisible = !isPasswordVisible
            updateToggleDrawable()
        }
        updateToggleDrawable()
    }

    private fun updateToggleDrawable() {
        ibToggleVisibility.setImageDrawable(if (isPasswordVisible) context.getDrawable(R.drawable.ic_eyes_open) else context.getDrawable(R.drawable.ic_eyes_close))
    }

    private fun setCollapsedTextFont() {
        try {
            val cthField = tilHint.javaClass.getDeclaredField("collapsingTextHelper")
            cthField.isAccessible = true
            val collapsingTextHelper = cthField.get(tilHint)

            val setCollapsedTypefaceMethod = collapsingTextHelper.javaClass.getDeclaredMethod("setCollapsedTypeface", Typeface::class.java)
            setCollapsedTypefaceMethod.isAccessible = true

            val setExpandedTypefaceMethod = collapsingTextHelper.javaClass.getDeclaredMethod("setExpandedTypeface", Typeface::class.java)
            setExpandedTypefaceMethod.isAccessible = true

            val typefaceCollapsed = TypefaceUtils.load(context.resources.assets, "fonts/gotham_bold.ttf")
            val typefaceExpanded = TypefaceUtils.load(context.resources.assets, "fonts/gotham_light.ttf")

            setCollapsedTypefaceMethod.invoke(collapsingTextHelper, typefaceCollapsed)
            setExpandedTypefaceMethod.invoke(collapsingTextHelper, typefaceExpanded)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    fun setHintText(text: String) {
        tietField.hint = text
    }

    fun setHintInput(text: String) {
        tilHint.hint = text
    }

    fun setFieldText(text: String) {
        tietField.setText(text)
    }

    fun getFieldText() = tietField.text ?: ""

    fun setHelpText(text: String) {
        helpText = text
        tvLoginHelp.text = text
    }

    fun setErrorText(text: String) {
        haveError = true
        tvLoginHelp.text = text
        tvLoginHelp.setTextColor(ContextCompat.getColor(context, R.color.error_red))
    }

    fun setOnInputListener(onInputListener: InputListener) {
        this.onInputListener = onInputListener
    }

    fun isValid(valid: Boolean) {
        if (textColors == null) {
            tvLoginHelp.setTextColor(ContextCompat.getColor(context, if (valid) R.color.taupegray else R.color.error_red))
        } else {
            if (valid) {
                tvLoginHelp.setTextColor(ColorUtils.parse(textColors!!))
            } else {
                tvLoginHelp.setTextColor(ContextCompat.getColor(context, R.color.error_red))
            }
        }
    }

    fun loading(isLoading: Boolean) {
        ivLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            val rotation = AnimationUtils.loadAnimation(context, R.anim.clockwise_rotation)
            rotation.repeatCount = Animation.INFINITE
            ivLoading.startAnimation(rotation)
        } else {
            ivLoading.clearAnimation()
        }
    }

    interface InputListener {
        fun onInput(onTextChanged: String)
    }

    fun setOnKeyboardActionListener(callback: () -> Unit) {
        tietField.setOnKeyboardActionListener { callback() }
    }

    fun addTextChangedListener(onTextChanged: (text: String) -> Unit) {
        tietField.addTextChangedListener { text -> onTextChanged(text) }
    }

    fun setColors(color: String) {
        textColors = color

        val textColor = ColorUtils.parse(color)
        tietField.setTextColor(textColor)
        tvLoginHelp.setTextColor(textColor)
        ibToggleVisibility.setColorFilter(textColor)
        ivLoading.setColorFilter(textColor)
        tietField.setHintTextColor(textColor)
        tvHidden.setHintTextColor(textColor)
        setInputTextLayoutColor(textColor, tilHint)
        tilHint.defaultHintTextColor = ColorStateList.valueOf(textColor)

//        setCursorColor(tietField, textColor)
    }

    private fun setCursorColor(view: EditText, color: Int) {
        try {
            var field = TextView::class.java.getDeclaredField("mCursorDrawableRes")
            field.isAccessible = true
            val drawableResId = field.getInt(view)

            field = TextView::class.java.getDeclaredField("mEditor")
            field.isAccessible = true
            val editor = field.get(view)

            val drawable = ContextCompat.getDrawable(view.context, drawableResId)
            drawable!!.setColorFilter(color, PorterDuff.Mode.SRC_IN)
            val drawables = arrayOf(drawable, drawable)

            field = editor.javaClass.getDeclaredField("mCursorDrawable")
            field.isAccessible = true
            field.set(editor, drawables)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setInputTextLayoutColor(color: Int, textInputLayout: TextInputLayout) {
        try {
            val field = textInputLayout.javaClass.getDeclaredField("focusedTextColor")
            field.isAccessible = true
            val states = arrayOf(intArrayOf())
            val colors = intArrayOf(color)
            val myList = ColorStateList(states, colors)
            field.set(textInputLayout, myList)

            val fDefaultTextColor = TextInputLayout::class.java.getDeclaredField("defaultHintTextColor")
            fDefaultTextColor.isAccessible = true
            fDefaultTextColor.set(textInputLayout, myList)

            val method = textInputLayout.javaClass.getDeclaredMethod("updateLabelState", Boolean::class.javaPrimitiveType!!)
            method.isAccessible = true
            method.invoke(textInputLayout, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
