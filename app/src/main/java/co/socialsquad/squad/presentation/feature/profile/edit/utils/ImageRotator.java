package co.socialsquad.squad.presentation.feature.profile.edit.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.File;
import java.io.IOException;

public class ImageRotator {

    public static Bitmap rotateImage(File file) throws IOException {
        String path = file.getAbsolutePath();
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return rotateImage(bitmap, path);
    }

    public static Bitmap rotateImage(Bitmap bitmap, String path) throws IOException {
        int rotate = 270;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        matrix.postScale(-1, 1, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
    }

    public static Bitmap rotateImage90(File file) throws IOException {
        String path = file.getAbsolutePath();
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return rotateImage90(bitmap, path);
    }

    public static Bitmap rotateImage90(Bitmap bitmap, String path) throws IOException {
        int rotate = 90;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
    }
}