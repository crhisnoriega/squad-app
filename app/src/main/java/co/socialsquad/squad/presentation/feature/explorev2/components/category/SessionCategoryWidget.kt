package co.socialsquad.squad.presentation.feature.explorev2.components.category

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.detail.ExploreItemDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreContent
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity

class SessionCategoryWidget(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private val view: View
    private var loading: Boolean = false
    private val rvItems: RecyclerView
    private val title: TextView
    private val subtitle: TextView

    private var emptyState: EmptyState? = null
    private var template: SectionTemplate = SectionTemplate.getDefault()

    init {
        setupStyledAttributes(attrs)
        view = changeTemplate(context)
        rvItems = view.findViewById(R.id.rvItems)
        title = view.findViewById(R.id.object_type_lead)
        subtitle = view.findViewById(R.id.levelDescription)
    }


    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.SessionCategoryWidget)) {
            loading = getBoolean(
                    R.styleable.SessionCategoryWidget_loadingCategory,
                    false
            )
        }
    }


    private fun changeTemplate(context: Context): View {
        return if (loading) {
            View.inflate(
                    context,
                    R.layout.view_session_category_loading_state,
                    this
            )
        } else {
            View.inflate(
                    context,
                    R.layout.view_session_category_widget,
                    this
            )
        }
    }

    fun bind(content: ExploreContent?, template: SectionTemplate?, emptyState: EmptyState?) {
        template?.let {
            this.template = it
        }
        this.emptyState = emptyState
        if (loading) {
            rvItems.adapter = CategoryItemsLoadingAdapter(this.template)
        } else {
            title.text = content?.title.orEmpty()
            if (content != null) {
                rvItems.adapter = CategoryItemsAdapter(
                        context,
                        this.template,
                        content.items,
                        this@SessionCategoryWidget::onCategorySelected
                )
            } else {
                rvItems.adapter = null
            }
        }
    }

    private fun onCategorySelected(contentItem: ContentItem) {
        if (contentItem.isObject()) {
            contentItem.link?.let {
                context.startActivity(ExploreItemObjectActivity.newInstance(context, it))
            }
        } else {
            ExploreItemDetailActivity.start(
                    context = context,
                    item = contentItem,
                    template = template,
                    emptyState = emptyState
            )
        }
    }
}
