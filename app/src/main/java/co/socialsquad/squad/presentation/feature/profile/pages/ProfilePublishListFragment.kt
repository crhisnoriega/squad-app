package co.socialsquad.squad.presentation.feature.profile.pages

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R

class ProfilePublishListFragment : Fragment() {

    companion object {
        fun newInstance() = ProfilePublishListFragment()
    }

    private lateinit var viewModel: ProfilePublishViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_publish, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfilePublishViewModel::class.java)
    }

}