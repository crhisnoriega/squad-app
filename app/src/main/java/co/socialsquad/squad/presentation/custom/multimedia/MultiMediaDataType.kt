package co.socialsquad.squad.presentation.custom.multimedia

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class MultiMediaDataType(val value: Int) : Parcelable {

    NOT_SUPORTED(0),

    VIDEO(1),

    IMAGE(2),

    AUDIO(3),
}
