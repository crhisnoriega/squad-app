package co.socialsquad.squad.presentation.feature.explorev2.listing

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.utils.DownloadUtils
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.LinkSingleViewHolder
import co.socialsquad.squad.presentation.custom.resource.viewHolder.LinkSingleViewHolderModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.ResourceSingleViewHolder2
import co.socialsquad.squad.presentation.custom.resource.viewHolder.ResourceSingleViewHolderModel2
import co.socialsquad.squad.presentation.feature.explorev2.browser.LinkBrowserActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkV2
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Resource
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingTopBottomViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingTopBottomViewModel
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewModel
import co.socialsquad.squad.presentation.feature.resources.ResourceActionsBottomSheetDialog
import co.socialsquad.squad.presentation.feature.resources.ResourceDownloadDialog
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.gson.Gson
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_elements_listing.*
import org.koin.core.module.Module
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


enum class COMPLEMENT_LIST_TYPE(val title: String) {
    RESOURCES("Resources"), LINKS("Links")
}

class ComplementsListingActivity : BaseActivity() {

    companion object {

        const val RESOURCES_LIST = "resources_list"
        const val COMPLEMENT_TYPE = "COMPLEMENT_TYPE"
        const val RESOURCES_TITLE = "resources_title"
        const val RESOURCES_SUBTITLE = "resources_subtitle"

        fun start(context: Context,
                  elementType: COMPLEMENT_LIST_TYPE,
                  elements: List<*>,
                  title: String,
                  subTitle: String
        ) {
            context.startActivity(Intent(context, ComplementsListingActivity::class.java).apply {
                putExtra(RESOURCES_LIST, Gson().toJson(elements))
                putExtra(RESOURCES_TITLE, title)
                putExtra(RESOURCES_SUBTITLE, subTitle)
                putExtra(COMPLEMENT_TYPE, elementType)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_complements_listing

    private val elementType: COMPLEMENT_LIST_TYPE by extra(COMPLEMENT_TYPE)
    private val jsonElements: String by extra(RESOURCES_LIST)
    private val title: String by extra(RESOURCES_TITLE)
    private val subTitle: String by extra(RESOURCES_SUBTITLE)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

        configureElementList()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = title
    }

    private fun configureObservers() {

    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                    Shimmer.AlphaHighlightBuilder()
                            .setBaseAlpha(1f)
                            .setIntensity(0f)
                            .build())
            it.stopShimmer()
            it.clearAnimation()
        }
    }

    private fun configureAsLoading(itens: Int) {
        rvElements.apply {
            layoutManager = LinearLayoutManager(this@ComplementsListingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                        is ItemShowAllRankingTopBottomViewModel -> R.layout.item_ranking_info_shimmer

                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {

                    R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {

                    }
                    R.layout.item_ranking_info_shimmer -> ItemSeparatorRankingTopBottomViewHolder(view) {

                    }

                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            factoryAdapter.setItems((0 until itens).map {
                ItemShowAllRankingTopBottomViewModel("", "") as ViewModel
            }.toMutableList().apply {
                add(0, ItemShowAllRankingViewModel("", ""))
            })
        }
    }


    private fun configureElementList() {
        stopShimmer(listShimmer)

        rvElements.apply {
            layoutManager = LinearLayoutManager(this@ComplementsListingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is LinkSingleViewHolderModel -> LinkSingleViewHolder.ITEM_VIEW_MODEL_ID
                        is ResourceSingleViewHolderModel2 -> ResourceSingleViewHolder2.ITEM_VIEW_MODEL_ID
                        is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                        is ItemShowAllRankingTopBottomViewModel -> R.layout.ranking_separator_top_bottom
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {

                    LinkSingleViewHolder.ITEM_VIEW_MODEL_ID -> LinkSingleViewHolder(view) {
//                        startActivity(Intent(Intent.ACTION_VIEW).apply {
//                            data = Uri.parse(it.link)
//                        })
                        LinkBrowserActivity.start(this@ComplementsListingActivity, it.title!!, it.link!!)
                    }

                    ResourceSingleViewHolder2.ITEM_VIEW_MODEL_ID -> ResourceSingleViewHolder2(view, {
                        when (it.type) {

                            "folder" -> {
                                start(
                                        context = context,
                                        elementType = COMPLEMENT_LIST_TYPE.RESOURCES,
                                        elements = ArrayList(it.resources),
                                        title = it.title!!,
                                        subTitle = it.description!!
                                )
                            }

                            "file" -> {
                                // download file
                                var optionsDialog = ResourceActionsBottomSheetDialog()
                                optionsDialog?.show(this@ComplementsListingActivity.supportFragmentManager, "")
                            }
                        }
                    }, {
                        val uri = Uri.parse(it.link!!)

                        val targetFile = File(context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS)[0], uri.lastPathSegment)

                        Log.i("down", "link: " + it.link!!)
                        Log.i("down", "file: " + targetFile.absoluteFile)

                        if (targetFile.exists()) {

                            var type = targetFile.absolutePath.split(".").last()

                            type = when (type) {
                                "pdf" -> "application/$type"
                                "mp4" -> "video/$type"
                                "jpeg" -> "image/$type"
                                "jpg" -> "image/$type"
                                else -> "*/*"
                            }

                            val intent = Intent(Intent.ACTION_VIEW)
                            val data: Uri = FileProvider.getUriForFile(context, context.applicationContext.packageName + ".provider", targetFile);

                            intent.setDataAndType(data, type)
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(intent)

                        } else {

                            var dialog = ResourceDownloadDialog()
                            dialog.resourceName = it.title!!
                            dialog.show(this@ComplementsListingActivity.supportFragmentManager, "")


                            var utils = DownloadUtils()
                            utils.download(it.link!!, targetFile)
                                    .throttleFirst(5, TimeUnit.MINUTES)
                                    .toFlowable(BackpressureStrategy.LATEST)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        Log.i("download", "$it% Downloaded")
                                    }, {
                                        Log.i("download", it.message, it)
                                    }, {
                                        Log.i("download", "completed")
                                        dialog.dismiss()

                                        var type = it.link!!.split(".").last()

                                        type = when (type) {
                                            "pdf" -> "application/$type"
                                            "mp4" -> "video/$type"
                                            "jpeg" -> "image/$type"
                                            "jpg" -> "image/$type"
                                            else -> "*/*"
                                        }

                                        Log.i("download", "type: $type")

                                        val intent = Intent(Intent.ACTION_VIEW)
                                        val data: Uri = FileProvider.getUriForFile(context, context.applicationContext.packageName + ".provider", targetFile);

                                        intent.setDataAndType(data, type)
                                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                        startActivity(intent)
                                    })

                        }
                    }
                    )

                    R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {

                    }

                    R.layout.ranking_separator_top_bottom -> ItemSeparatorRankingTopBottomViewHolder(view) {

                    }


                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter


            var elements = mutableListOf<ViewModel>()

            elements.add(ItemShowAllRankingViewModel("", ""))

            when (elementType) {
                COMPLEMENT_LIST_TYPE.RESOURCES -> {
                    var isFirst = true
                    var resources: List<Resource> = Gson().fromJson(jsonElements, Array<Resource>::class.java).toList()
                    resources.forEachIndexed { position, resource ->
                        elements.add(ResourceSingleViewHolderModel2(resource, position == resources.size - 1) as ViewModel)
                        isFirst = false
                    }
                }

                COMPLEMENT_LIST_TYPE.LINKS -> {
                    var links: List<LinkV2> = Gson().fromJson(jsonElements, Array<LinkV2>::class.java).toList()
                    links.forEachIndexed { index, linkV2 ->
                        elements.add(LinkSingleViewHolderModel(linkV2, index == links.size - 1))
                    }
                }
            }

            factoryAdapter.setItems(elements)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}