package co.socialsquad.squad.presentation.feature.opportunityv2.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class OpportunityPageAdapter(activity: AppCompatActivity, var fragments: MutableList<Fragment>) :
        FragmentStateAdapter(activity) {

    override fun getItemCount(): Int {
       return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

}