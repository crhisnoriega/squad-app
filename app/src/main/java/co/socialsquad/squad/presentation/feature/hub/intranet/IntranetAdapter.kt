package co.socialsquad.squad.presentation.feature.hub.intranet

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.VirtualOfficeResponse.VirtualOffice.Tool
import kotlin.reflect.KFunction3

class IntranetAdapter(
    val tools: MutableList<Tool>,
    val onClick: KFunction3<String, String, String?, Unit>,
    val context: Context,
    private val secondaryColor: String?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val typeHeader = 10
    private val typeContent = 11

    override fun getItemViewType(position: Int): Int {
        return if (tools[position].url == null) {
            typeHeader
        } else {
            typeContent
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == typeHeader) {
            HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.virtual_office_header, parent, false))
        } else {
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.virtual_office_item, parent, false))
        }
    }

    override fun getItemCount(): Int = tools.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == typeHeader) {
            val viewHolder = (holder as HeaderViewHolder)
            viewHolder.title.text = tools[position].title
        } else {
            val viewHolder = (holder as ViewHolder)
            viewHolder.title.text = tools[position].title
            viewHolder.container.setOnClickListener {
                tools[position].url?.apply { onClick(this, tools[position].title, secondaryColor) }
            }
            if (position == tools.size - 1 || tools[position + 1].url == null) {
                viewHolder.bottomView.setBackgroundColor(ContextCompat.getColor(context, R.color.list_boundary_outer))
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.object_type_lead)
        val container = itemView.findViewById<CardView>(R.id.container)
        val bottomView = itemView.findViewById<View>(R.id.bottomView)
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.object_type_lead)
    }
}
