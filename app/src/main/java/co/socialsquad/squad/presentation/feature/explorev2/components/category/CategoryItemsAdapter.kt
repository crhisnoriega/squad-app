package co.socialsquad.squad.presentation.feature.explorev2.components.category

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide

class CategoryItemsAdapter(
    val context: Context,
    val template: SectionTemplate,
    val items: List<ContentItem>,
    val onCategorySelected: (ContentItem) -> Unit
) : RecyclerView.Adapter<CategoryItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (template) {
            SectionTemplate.Square -> ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_session_category_square_item, parent, false)
            )
            SectionTemplate.Circle -> ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_session_category_circle_item, parent, false)
            )
            SectionTemplate.RectangleHorizontal -> ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.view_session_category_rectangular_horizontal_item,
                    parent,
                    false
                )
            )
            SectionTemplate.RectangleVertical -> ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.view_session_category_rectangular_vertical_item,
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.view_session_category_rectangular_horizontal_item,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contentItem = items[position]

        contentItem.media?.let { Glide.with(context).load(it.url).crossFade().into(holder.image) }
        holder.shortName.text = contentItem.title

        if (contentItem.link != null) {
            holder.container.setOnClickListener { onCategorySelected(contentItem) }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.image)
        val shortName: TextView = itemView.findViewById(R.id.shortName)
        val container: ViewGroup = itemView.findViewById(R.id.container)
    }
}
