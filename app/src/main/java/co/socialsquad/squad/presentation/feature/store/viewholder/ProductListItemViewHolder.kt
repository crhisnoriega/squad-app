package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.MediaType
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_store_product_list_item.view.*

class ProductListItemViewHolder(itemView: View, val glide: RequestManager, val listener: Listener<StoreProductViewModel>) : ViewHolder<StoreProductViewModel>(itemView) {
    private val ivImage: ImageView = itemView.store_product_list_item_iv_image
    private val tvName: TextView = itemView.store_product_list_item_tv_name
    private val tvPrice: TextView = itemView.store_product_list_item_tv_price
    private val tvPoints: TextView = itemView.store_product_list_item_tv_points
    private val bottomContent: LinearLayout = itemView.bottomContent

    override fun bind(viewModel: StoreProductViewModel) {
        with(viewModel) {
            medias?.firstOrNull { it.type is MediaType.Image }?.let {
                glide.load(it.url)
                    .crossFade()
                    .into(ivImage)
            }

            tvName.text = shortName

            itemView.context?.let { context ->

                if (price != null) {
                    price?.let { tvPrice.text = context.getString(R.string.store_list_list_item_price, it) }
                } else {
                    tvPrice.visibility = View.GONE
                }

                if (points != null) {
                    points?.let { tvPoints.text = context.getString(R.string.products_item_points, it) }
                } else {
                    tvPoints.visibility = View.GONE
                }

                if (points == null && price == null) {
                    bottomContent.visibility = View.GONE
                }
            }

            itemView.setOnClickListener { listener.onClick(viewModel) }
        }
    }

    override fun recycle() {
        glide.clear(ivImage)
    }
}
