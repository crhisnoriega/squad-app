package co.socialsquad.squad.presentation.feature.explorev2.complement.price

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide

class PriceListViewHolder(
    itemView: View
) : ViewHolder<PriceListViewModel>
    (itemView) {

    private val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
    private val txtSubtitle: TextView = itemView.findViewById(R.id.txtSubtitle)
    private val imgIcon: ImageView = itemView.findViewById(R.id.imgIcon)

    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_price_list
    }

    override fun bind(viewModel: PriceListViewModel) {
        txtTitle.text = viewModel.priceList.title
        txtSubtitle.text = viewModel.priceList.formatted_price

        viewModel.priceList.icon?.url?.let{
            Glide.with(itemView.context).load(it).into(imgIcon)
        }
    }

    override fun recycle() {
    }
}