package co.socialsquad.squad.presentation.feature.immobiles.details.moredetails

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.presentation.util.ColorUtils

class MoreDetailsAdapter(val details: List<ImmobileDetail.Detail>, val primaryColor: String) : RecyclerView.Adapter<MoreDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.more_details_item_list, parent, false))

    override fun getItemCount(): Int = details.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = details[position].title
        holder.details.text = details[position].description
        val counter = position + 1
        holder.counter.text = counter.toString()

        val bgShape = holder.counter.background as GradientDrawable
        bgShape.setColor(ColorUtils.parse(primaryColor))

        if (position == details.size - 1) {
            holder.separator.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.list_boundary_outer))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val details: TextView = itemView.findViewById(R.id.details)
        val counter: TextView = itemView.findViewById(R.id.counter)
        val separator: View = itemView.findViewById(R.id.separator)
    }
}
