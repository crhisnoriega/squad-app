package co.socialsquad.squad.presentation.feature.terms

import dagger.Binds
import dagger.Module

@Module
interface TermsModule {
    @Binds
    fun view(view: TermsActivity): TermsView
}
