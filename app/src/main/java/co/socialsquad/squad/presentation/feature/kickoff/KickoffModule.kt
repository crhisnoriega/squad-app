package co.socialsquad.squad.presentation.feature.kickoff

import co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode.KickoffHinodeFragment
import co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode.KickoffHinodeModule
import co.socialsquad.squad.presentation.feature.kickoff.inviteteam.InviteYourTeamFragment
import co.socialsquad.squad.presentation.feature.kickoff.inviteteam.InviteYourTeamModule
import co.socialsquad.squad.presentation.feature.kickoff.profile.KickoffProfileFragment
import co.socialsquad.squad.presentation.feature.kickoff.profile.KickoffProfileModule
import co.socialsquad.squad.presentation.feature.kickoff.terms.KickoffTermsFragment
import co.socialsquad.squad.presentation.feature.kickoff.terms.KickoffTermsModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class KickoffModule {

    @ContributesAndroidInjector(modules = arrayOf(KickoffHinodeModule::class))
    abstract fun kickoffHinodeFragment(): KickoffHinodeFragment

    @ContributesAndroidInjector(modules = arrayOf(KickoffTermsModule::class))
    abstract fun kickoffTermsFragment(): KickoffTermsFragment

    @ContributesAndroidInjector(modules = arrayOf(KickoffProfileModule::class))
    abstract fun kickoffProfileFragment(): KickoffProfileFragment

    @ContributesAndroidInjector(modules = arrayOf(InviteYourTeamModule::class))
    abstract fun inviteMembersFragment(): InviteYourTeamFragment

}
