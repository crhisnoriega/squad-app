package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.app.DatePickerDialog
import android.content.Context
import android.text.format.DateUtils
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.hideKeyboard
import kotlinx.android.synthetic.main.view_input_date.view.*
import java.util.Calendar

class InputDate(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    private var onInputListener: InputView.OnInputListener? = null
    private var longDate: Boolean = false
    private var minTimeInMillis: Long? = null

    private val labelAnimator = ObjectAnimator
        .ofFloat(12f, 11f)
        .apply {
            duration = 200L
            addUpdateListener { tvLabel.textSize = it.animatedValue as Float }
        }

    var calendar: Calendar? = null
        set(value) {
            field = value?.apply {
                showDate(this)
                datePickerDialog?.updateDate(get(Calendar.YEAR), get(Calendar.MONTH), get(Calendar.DAY_OF_MONTH))
            }
        }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { _, y, m, d ->
        if (calendar == null) {
            calendar = Calendar.getInstance()
        }
        calendar?.let {
            it.set(y, m, d)
            showDate(it)
            onInputListener?.onInput()
        }
    }

    private var datePickerDialog: DatePickerDialog? = null
        get() {
            val calendar = calendar ?: Calendar.getInstance()
            return DatePickerDialog(
                context,
                dateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
        }

    private fun showDate(calendar: Calendar) {
        val format = if (longDate) DateUtils.FORMAT_SHOW_YEAR else (DateUtils.FORMAT_NUMERIC_DATE or DateUtils.FORMAT_SHOW_YEAR)
        tvDate.text = DateUtils.formatDateTime(context, calendar.time.time, format)
        if (tvDate.visibility == View.GONE) {
            labelAnimator.start()
            tvDate.visibility = View.VISIBLE
        }
    }

    init {
        View.inflate(context, R.layout.view_input_date, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputDate)) {
            tvLabel.text = getString(R.styleable.InputDate_hint)
            longDate = getBoolean(R.styleable.InputDate_longDate, false)
            recycle()
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener {
            datePickerDialog?.apply {
                minTimeInMillis?.let { datePicker.minDate = it }
                show()
                hideKeyboard()
            }
        }
    }

    // This function should be public for other classes setting the minimum available date/hour
    // in the date picker. If not set by another classe, it will be 0 by default
    fun setMinDate(timeInMillis: Long) {
        minTimeInMillis = timeInMillis
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    override fun isValid(valid: Boolean) {}
}
