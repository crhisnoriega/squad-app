package co.socialsquad.squad.presentation.feature.social.create.segment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.AudienceType
import co.socialsquad.squad.presentation.custom.InputOption
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity.Companion.EXTRA_MEMBERS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_POTENTIAL_REACH
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsPresenter
import kotlinx.android.synthetic.main.activity_social_create_segment.*
import javax.inject.Inject

private const val REQUEST_CODE_METRIC = 40
private const val REQUEST_CODE_MEMBERS = 50

class SocialCreateSegmentActivity : BaseDaggerActivity(), SocialCreateSegmentView {

    companion object {
        const val EXTRA_DOWNLINE = "EXTRA_DOWNLINE"
        const val RESULT_CODE_PUBLIC = 11
        const val RESULT_CODE_DOWNLINE = 21
        const val RESULT_CODE_METRIC = 41
        const val RESULT_CODE_MEMBERS = 51
    }

    @Inject
    lateinit var socialCreateSegmentPresenter: SocialCreateSegmentPresenter

    private var metrics: MetricsViewModel? = null
    private var members: List<UserViewModel> = arrayListOf()
    private var downline: Boolean = false

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED || data == null) return

        when (requestCode to resultCode) {
            REQUEST_CODE_METRIC to SegmentMetricsPresenter.RESULT_CODE_METRIC -> {
                setResult(RESULT_CODE_METRIC, data)
                finish()
            }
            REQUEST_CODE_MEMBERS to SegmentGroupMembersActivity.RESULT_CODE_MEMBERS -> {
                setResult(RESULT_CODE_MEMBERS, data)
                finish()
            }
        }
    }

    override fun onDestroy() {
        socialCreateSegmentPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_create_segment)

        socialCreateSegmentPresenter.onCreate()

        setupToolbar()
        setupOnClickListeners()
        metrics = intent.getSerializableExtra(EXTRA_METRICS) as? MetricsViewModel
        members = (intent.extras?.getSerializable(EXTRA_MEMBERS) as? ArrayList<UserViewModel>?).orEmpty()
        downline = intent.getBooleanExtra(EXTRA_DOWNLINE, false)
        setSelectedItem()
    }

    override fun showOptions(types: List<AudienceType>) {
        if (types.any { it.type() == AudienceType.Type.Public }) llPublic.visibility = View.VISIBLE
        if (types.any { it.type() == AudienceType.Type.Downline }) llDownline.visibility = View.VISIBLE
        if (types.any { it.type() == AudienceType.Type.Metrics }) llMetrics.visibility = View.VISIBLE
        if (types.any { it.type() == AudienceType.Type.Members }) llMembers.visibility = View.VISIBLE
        llOptions.visibility = View.VISIBLE
    }

    override fun showLoading() {
        vLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        vLoading.visibility = View.GONE
    }

    private fun deselectAllInputOptions() {
        metrics = null
        members = listOf()
        downline = false

        ioPublic.setCheck(false)
        ioDownline.setCheck(false)
        ioMembers.setCheck(false)
        ioMetrics.setCheck(false)
    }

    private fun selectInputOption(inputOption: InputOption) {
        deselectAllInputOptions()
        inputOption.setCheck(true)
    }

    private fun setSelectedItem() {
        when {
            metrics == null && members.isEmpty() && !downline -> selectInputOption(ioPublic)
            metrics != null -> selectInputOption(ioMetrics)
            !members.isEmpty() -> selectInputOption(ioMembers)
            downline -> selectInputOption(ioDownline)
        }
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.social_create_segment_title)
    }

    private fun setupOnClickListeners() {
        ioPublic.setOnClickListener {
            socialCreateSegmentPresenter.onPublicClicked()
            setResult(RESULT_CODE_PUBLIC)
            finish()
        }

        ioDownline.setOnClickListener {
            socialCreateSegmentPresenter.onDownlineClicked()
        }

        ioMetrics.setOnClickListener {
            socialCreateSegmentPresenter.onMetricsClicked()
            val intent = Intent(this, SegmentMetricsActivity::class.java)
            metrics?.let { intent.putExtra(EXTRA_METRICS, it) }
            startActivityForResult(intent, REQUEST_CODE_METRIC)
        }

        ioMembers.setOnClickListener {
            socialCreateSegmentPresenter.onMembersClicked()
            val intent = Intent(this, SegmentGroupMembersActivity::class.java)
            intent.putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, members.toTypedArray())
            startActivityForResult(intent, REQUEST_CODE_MEMBERS)
        }
    }

    override fun finishAfterDownlineClicked(networkCount: Int) {
        val potentialReach = PotentialReachViewModel(5, 0, networkCount)
        val intent = Intent().putExtra(EXTRA_POTENTIAL_REACH, potentialReach)
        setResult(RESULT_CODE_DOWNLINE, intent)
        finish()
    }
}
