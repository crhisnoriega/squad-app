package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
class RichTextLoading : ViewType, Parcelable {
    override fun getViewType(): Int {
        return RichTextDataType.LOADING.value
    }
}
