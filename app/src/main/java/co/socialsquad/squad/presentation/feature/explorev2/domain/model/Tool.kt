package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.ToolTypeAdapter
import co.socialsquad.squad.domain.model.Button
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tool(
        @SerializedName("id") val id: Long,
        @JsonAdapter(ToolTypeAdapter::class)
        @SerializedName("type") val type: RankingType,
        @SerializedName("button") val button: Button?,
        @SerializedName("content") val content: ToolContent
) : Parcelable
