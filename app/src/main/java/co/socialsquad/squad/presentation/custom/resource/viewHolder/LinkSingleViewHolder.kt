package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkV2
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_link_list.view.*

class LinkSingleViewHolder(itemView: View, var callback: (link: LinkV2) -> Unit) : ViewHolder<LinkSingleViewHolderModel>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_link_list
    }

    override fun bind(viewModel: LinkSingleViewHolderModel) {
        itemView.tvTitle.text = viewModel.link.title

        var urlEllipsed = if (viewModel.link.link?.length!! > 100) {
            viewModel.link.link?.substring(0, 99)
        } else {
            viewModel.link.link!!
        }

        itemView.tvModified.text = viewModel.link.description
        itemView.setOnClickListener {
            callback.invoke(viewModel.link)
        }
    }

    override fun recycle() {

    }
}