package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.ranking.model.RankingData
import co.socialsquad.squad.presentation.feature.ranking.model.RankingItem
import co.socialsquad.squad.presentation.feature.recognition.model.BonusData
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData

class ItemRecognitionBonusViewModel(
        val bonusData: BonusData)
    : ViewModel {
}