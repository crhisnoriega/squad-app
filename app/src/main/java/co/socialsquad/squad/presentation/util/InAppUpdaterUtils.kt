package co.socialsquad.squad.presentation.util

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability

class InAppUpdaterUtils(val context: Context) : InstallStateUpdatedListener {

	private var appUpdateManager: AppUpdateManager = AppUpdateManagerFactory.create(context)
	var listener: InAppUpdateListener? = null

	init {
		appUpdateManager.registerListener(this)
	}

	fun doComplete(){
		appUpdateManager.completeUpdate()
	}

	fun checkAvailableUpdate() {

		val appUpdateInfoTask = appUpdateManager.appUpdateInfo

		appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
			if ((appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
							||
							appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
							)
					&& appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
			) {
				listener?.let {
					it.onAppUpdateInfo(appUpdateInfo)
				}
			} else {
				listener?.nothingToUpdate(appUpdateInfo.updateAvailability())
			}
		}

		appUpdateInfoTask.addOnFailureListener { exception ->
			listener?.let {
				it.onError(exception)
			}
		}
	}

	fun startDownloadUpdate(appUpdateInfo: AppUpdateInfo) {
		appUpdateManager.startUpdateFlowForResult(
				appUpdateInfo,
				AppUpdateType.FLEXIBLE,
				context as AppCompatActivity,
				CODE_UPDATE_REQUEST)
	}

	override fun onStateUpdate(state: InstallState) {
		listener?.let { it.onUpdateState(state) }
	}

	companion object {
		var CODE_UPDATE_REQUEST = 10003
	}

}