package co.socialsquad.squad.presentation.custom.multimedia.audio

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.custom.multimedia.ViewTypeDelegateAdapter

class AudioDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?): MediaViewHolder =
        AudioViewHolder(parent, lifecycleOwner)
}
