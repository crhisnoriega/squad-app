package co.socialsquad.squad.presentation.feature.kickoff.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import kotlinx.android.synthetic.main.fragment_kickoff_welcome.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3

class KickoffWelcomeFragment(
) : Fragment() {
    private val viewModel by sharedViewModel<ProfileEditViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_kickoff_welcome, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_start.setOnClickListener {
            viewModel.state.value = StateNav("picture", false)
        }
    }

}
