package co.socialsquad.squad.presentation.feature.ranking.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.RankingListViewModel
import co.socialsquad.squad.presentation.feature.ranking.details.viewholder.ItemBenefitIntervalPositionRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.details.viewholder.ItemBenefitIntervalPositionRankingViewModel
import co.socialsquad.squad.presentation.feature.ranking.details.viewholder.ItemBenefitPositionRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.details.viewholder.ItemBenefitPositionRankingViewModel
import co.socialsquad.squad.presentation.feature.ranking.model.BenefitData
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewModel
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.activity_ranking_details_benefit.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class RankingBenefitDetailsActivity : BaseActivity() {

    companion object {
        const val BENEFIT_ID = "BENEFIT_ID"
        const val RANK_ID = "RANK_ID"
        const val LEVEL_ID = "LEVEL_ID"

        fun start(context: Context, benefitId: String, rankingId: String? = "", levelId: String? = "") {
            context.startActivity(Intent(context, RankingBenefitDetailsActivity::class.java).apply {
                putExtra(BENEFIT_ID, benefitId)
                putExtra(RANK_ID, rankingId)
                putExtra(LEVEL_ID, levelId)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_ranking_details_benefit

    private val viewModel by viewModel<RankingListViewModel>()
    private val benefitId: String by extra(BENEFIT_ID)
    private val rankingId: String by extra(RANK_ID)
    private val levelId: String by extra(LEVEL_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvTitle.text = "Benefício"

        setupToolbar()
        configureObservers()

        viewModel.fetchBenefitDetails(benefitId, rankingId, levelId)
    }

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.benefitDetails
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        populateScreen(it.data!!)
                        //configurePositionList()
                        // rvPositions.isVisible = false
                    }
                }

            }
        }

    }

    private fun populateScreen(benefitData: BenefitData) {
        app_bar.isVisible = true
        nestedScroll.isVisible = true
        shimmerLayout.isVisible = false

        levelName.text = benefitData.title
        levelDescription.text = benefitData.description
        description.text = benefitData.positions
        Glide.with(this).load(benefitData.mediaCover).into(imgCover)
    }

    private fun configurePositionList() {
        rvPositions.apply {
            layoutManager = LinearLayoutManager(this@RankingBenefitDetailsActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemBenefitPositionRankingViewModel -> R.layout.item_ranking_benefit_single_position
                        is ItemBenefitIntervalPositionRankingViewModel -> R.layout.item_ranking_benefit_interval_position
                        is ItemShowAllRankingViewModel -> R.layout.item_ranking_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_ranking_benefit_single_position -> ItemBenefitPositionRankingViewHolder(view) {

                    }

                    R.layout.item_ranking_benefit_interval_position -> ItemBenefitIntervalPositionRankingViewHolder(view) {

                    }
                    R.layout.item_ranking_show_all -> ItemShowAllRankingViewHolder(view) {
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter
            factoryAdapter.setItems(listOf(
                    ItemBenefitPositionRankingViewModel("", ""),
                    ItemBenefitPositionRankingViewModel("", ""),
                    ItemBenefitIntervalPositionRankingViewModel("", ""),
            ))
        }
    }


    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}