package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CriteriaData(
        @SerializedName("id") val id: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("show_details") val show_details: Boolean?,
        @SerializedName("company") val company: Int?,
        @SerializedName("user_obtained_points") val user_obtained_points:String?,
        var first: Boolean = false
) : Parcelable