package co.socialsquad.squad.presentation.feature.widgetScheduler.repository

import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote.ScheduleSummaryApi
import kotlinx.coroutines.flow.flow

class ScheduleSummaryRepositoryImpl(private val api: ScheduleSummaryApi) :
    ScheduleSummaryRepository {

    override fun fetchScheduleSummary(timetableId: Long, objectId: Long) = flow {
        emit(
            api.fetchScheduleSummary(timetableId = timetableId, objectId = objectId).data
        )
    }

    override fun fetchScaleDate(
        timetableId: Long,
        objectId: Long,
        position: Long?,
        date: String?
    ) = flow {
        emit(
            api.fetchScaleDate(
                timetableId = timetableId,
                objectId = objectId,
                position = position,
                date = date
            ).data
        )
    }
}
