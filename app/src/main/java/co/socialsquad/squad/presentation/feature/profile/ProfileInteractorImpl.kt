package co.socialsquad.squad.presentation.feature.profile

import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import io.reactivex.Observable
import javax.inject.Inject

class ProfileInteractorImpl @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val loginRepository: LoginRepository
) : ProfileInteractor {
    override fun getProfile(): Observable<ProfileViewModel> =
        profileRepository.getProfile().map {
            with(it) {
                loginRepository.userCompany = this
                loginRepository.squadColor = company.primaryColor
                return@map ProfileViewModel(this)
            }
        }

    override fun getProfile(pk: Int): Observable<ProfileViewModel> =
        profileRepository.getProfile(pk).map {
            with(it) { return@map ProfileViewModel(this) }
        }

    override fun getPosts(page: Int): Observable<Feed> {
        return profileRepository.getPosts(page)
    }

    override fun getPosts(pk: Int, page: Int): Observable<Feed> {
        return profileRepository.getPosts(pk, page)
    }
}
