package co.socialsquad.squad.presentation.feature.company

import dagger.Binds
import dagger.Module

@Module
abstract class SystemSelectModule {
    @Binds
    abstract fun view(systemSelectActivity: SystemSelectActivity): SystemSelectView
}
