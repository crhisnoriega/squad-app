package co.socialsquad.squad.presentation.feature.chat

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.LiveDataViewModel

const val CHAT_MY_MESSAGE_VIEW_MODEL_ID = R.layout.view_chat_message_right
const val CHAT_OTHER_PERSON_MESSAGE_VIEW_MODEL_ID = R.layout.view_chat_message_left

abstract class MessageViewModel(
    override val id: Long,
    open val date: Long,
    open val message: String,
    open val read: Boolean
) : LiveDataViewModel

open class MyMessageViewModel(
    override val id: Long,
    override val date: Long,
    override val message: String,
    override val read: Boolean
) : MessageViewModel(id, date, message, read)

open class OtherPersonMessageViewModel(
    override val id: Long,
    override val date: Long,
    override val message: String,
    override val read: Boolean
) : MessageViewModel(id, date, message, read)
