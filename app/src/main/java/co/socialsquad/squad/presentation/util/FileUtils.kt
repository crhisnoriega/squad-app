package co.socialsquad.squad.presentation.util

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.webkit.MimeTypeMap
import androidx.core.content.MimeTypeFilter
import co.socialsquad.squad.data.entity.Media
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

object
FileUtils {

    fun createFromInputStream(file: File, inputStream: InputStream) {
        try {
            val outputStream = FileOutputStream(file, false)
            val buffer = ByteArray(8 * 1024)
            var bytesRead = inputStream.read(buffer)
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bytesRead)
                bytesRead = inputStream.read(buffer)
            }

            inputStream.close()
            outputStream.flush()
            outputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun createFromBitmap(file: File, bitmap: Bitmap) {
        try {
            val stream = FileOutputStream(file, false)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun getMediaType(uri: Uri, contentResolver: ContentResolver): MediaType? {
        return if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            val type = contentResolver.getType(uri)
            type?.let { it.toMediaTypeOrNull() }
        } else {
            val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            mimeType?.let { it.toMediaTypeOrNull() }
        }
    }

    fun isVideoMedia(media: Media): Boolean {
        return MimeTypeFilter.matches(media.mimeType, "video/*")
    }

    fun isImageMedia(media: Media): Boolean {
        media.file?.let {
            return media != null && (
                    it.contains("image") ||
                            it.contains("jpg") ||
                            it.contains("png") ||
                            MimeTypeFilter.matches(media.mimeType, "image/*")
                    )
        }

        return false
    }
}

fun MediaMetadataRetriever.getDuration(context: Context, uri: Uri): Long? {
    setDataSource(context, uri)
    val durationString = extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    return durationString.toLong()
}
