package co.socialsquad.squad.presentation.feature.explorev2.items

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import dagger.android.support.AndroidSupportInjection

class ExploreItemMapPermissionFragment(private val onaction:()->Unit) : Fragment() {

    companion object {

        @JvmStatic
        fun newInstance(onaction:()->Unit) = ExploreItemMapPermissionFragment(onaction)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.explore_item_map_permission_fragment, container, false)
        view.findViewById<Button>(R.id.btnAction).setOnClickListener {
            onaction.invoke()
        }
        view.findViewById<TextView>(R.id.tvDescription).text = Html.fromHtml(getString(R.string.permission_location_description))
        return view
    }
}