package co.socialsquad.squad.presentation.feature.social

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialCreateItemViewHolder
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_social_create.*

class SocialCreateDialog(
    context: Context,
    private val canCreateFromCamera: Boolean,
    private val canCreateFromGallery: Boolean,
    private val canCreateLive: Boolean,
    private val canCreateAudio: Boolean,
    private val listener: Listener
) : BottomSheetDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_social_create)

        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        with(rvActions) {
            adapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is SocialCreateItemViewModel -> SOCIAL_CREATE_ITEM_VIEW_MODEL_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                    return when (viewType) {
                        SOCIAL_CREATE_ITEM_VIEW_MODEL_ID -> SocialCreateItemViewHolder(view, onItemClicked)
                        else -> throw IllegalArgumentException()
                    }
                }
            }).apply {
                addItems(
                    listOf(
                        SocialCreateItemViewModel.Photo(canCreateFromCamera),
                        SocialCreateItemViewModel.Video(canCreateFromCamera),
                        SocialCreateItemViewModel.Gallery(canCreateFromGallery),
                        SocialCreateItemViewModel.Live(canCreateLive) // ,
//                        SocialCreateItemViewModel.AudioRecord(canCreateAudio),
//                        SocialCreateItemViewModel.AudioFile(canCreateAudio)
                    )
                )
            }
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    val onItemClicked = object : ViewHolder.Listener<SocialCreateItemViewModel> {
        override fun onClick(viewModel: SocialCreateItemViewModel) {
            when (viewModel) {
                is SocialCreateItemViewModel.Photo -> listener.onPhotoClicked()
                is SocialCreateItemViewModel.Video -> listener.onVideoClicked()
                is SocialCreateItemViewModel.Gallery -> listener.onGalleryClicked()
                is SocialCreateItemViewModel.Live -> listener.onLiveClicked()
//                is SocialCreateItemViewModel.AudioRecord -> listener.onAudioRecordClicked()
//                is SocialCreateItemViewModel.AudioFile -> listener.onAudioFileClicked()
            }
            dismiss()
        }
    }

    interface Listener {
        fun onPhotoClicked()
        fun onVideoClicked()
        fun onGalleryClicked()
        fun onLiveClicked()
        fun onAudioRecordClicked()
        fun onAudioFileClicked()
    }
}
