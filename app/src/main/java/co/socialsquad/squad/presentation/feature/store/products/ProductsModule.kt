package co.socialsquad.squad.presentation.feature.store.products

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ProductsModule {
    @Binds
    abstract fun view(productsActivity: ProductsActivity): ProductsView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
