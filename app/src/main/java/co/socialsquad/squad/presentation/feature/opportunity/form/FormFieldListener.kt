package co.socialsquad.squad.presentation.feature.opportunity.form

interface FormFieldListener {
    fun onEnableNextButton(enabled: Boolean)
}
