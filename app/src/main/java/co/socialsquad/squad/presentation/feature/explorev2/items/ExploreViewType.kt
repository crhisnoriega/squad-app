package co.socialsquad.squad.presentation.feature.explorev2.items

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val LIST = 1
private const val MAP = 2
private const val MAP_PERMISSION = 3
sealed class ExploreViewType(val value: Int) : Parcelable {

    @Parcelize
    object List : ExploreViewType(LIST)

    @Parcelize
    object Map : ExploreViewType(MAP)

    @Parcelize
    object MapPermission : ExploreViewType(MAP_PERMISSION)
}
