package co.socialsquad.squad.presentation.util

fun String.trimIfSingleWord(): String {
    val words = split(" ")
    val filteredWords = words.filter { it.isNotEmpty() }
    return if (filteredWords.count() == 1) this.trim() else this
}
