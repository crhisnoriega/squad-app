package co.socialsquad.squad.presentation.feature.newlead

import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.SubmissionCreation
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.form.FormUpdate
import co.socialsquad.squad.domain.model.widget.Widget
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.newlead.repository.NewLeadRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class NewLeadSharedViewModel(
        private val repository: NewLeadRepository
) : BaseViewModel() {

    @ExperimentalCoroutinesApi
    val fieldData: StateFlow<Resource<Field>>
        get() = _fieldData

    @ExperimentalCoroutinesApi
    private val _fieldData = MutableStateFlow(Resource.empty<Field>())

    @ExperimentalCoroutinesApi
    val formUpdateResponse: StateFlow<Resource<String>>
        get() = _formUpdateResponse

    @ExperimentalCoroutinesApi
    private val _formUpdateResponse = MutableStateFlow(Resource.empty<String>())

    private var submissionCreation: SubmissionCreation? = null
    private var widget: Widget? = null
    private var form: ArrayList<Field>? = null
    private var formPosition: Int = 0
    private var formUpdate: FormUpdate = FormUpdate(hashMapOf())
    private var submitText: String? = null

    fun loadForm(submissionCreation: SubmissionCreation, widget: Widget, form: ArrayList<Field>) {
        this.submissionCreation = submissionCreation
        this.widget = widget
        this.form = form
        this.formPosition = 0
        loadNextFieldOrFormUpdate()
    }

    fun loadBackFieldOrFinish(): Boolean {
        var finish = true
        if (formPosition - 1 >= 0) {
            form?.also {
                submitText = null
                formPosition--
                _fieldData.value = Resource.success(it[formPosition])
                return false
            }
        }
        return finish
    }

    fun canShowLoading(): Boolean {
        var tmpformPosition = formPosition + 1
        form?.also {
            return !(tmpformPosition >= 0 && tmpformPosition < it.size)
        }
        return false
    }

    private fun loadNextFieldOrFormUpdate() {
        form?.also {
            if (formPosition >= 0 && formPosition < it.size) {

                if (formPosition + 1 == it.size) {
                    widget?.submitButton?.text?.let { text ->
                        submitText = text
                    }
                }

                _fieldData.value = Resource.success(it[formPosition])
            } else {
                createLead()
            }
        }
    }

    fun saveValue(key: String, value: String) {
        formUpdate.data?.let { map ->
            map[key] = value
            formUpdate = FormUpdate(map)
            formPosition++
            loadNextFieldOrFormUpdate()
        }
    }

    fun getValue(key: String): String {
        return formUpdate.data?.let { map ->
            if (map.containsKey(key)) {
                map.getValue(key) as String
            } else {
                ""
            }
        } ?: run {
            ""
        }
    }

    private fun createLead() {
        viewModelScope.launch {
            submissionCreation?.id?.let { submission ->
                widget?.id?.let { widget ->
                    _formUpdateResponse.value = Resource.loading(null)
                    repository.formUpdate(submission, widget, formUpdate)
                            .catch { e ->
                                e.printStackTrace()
                                _formUpdateResponse.value = Resource.error(e.toString(), null)
                            }
                            .flowOn(Dispatchers.IO)
                            .collect {
                                _formUpdateResponse.value = Resource.success(it)
                            }
                }
            }
        }
    }

    fun getSubmitText(): String? = submitText
}
