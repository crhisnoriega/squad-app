package co.socialsquad.squad.presentation.feature.explorev2.items

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.components.banner.SessionBannerWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.category.SessionCategoryWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.collectionhorizontal.SessionCollectionHorizontalWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.collectionvertical.SessionCollectionVerticalWidget
import co.socialsquad.squad.presentation.feature.explorev2.components.list.SessionObjectListVerticalWidget
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionLoadingState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import co.socialsquad.squad.presentation.util.inflate
import com.google.gson.Gson
import kotlinx.android.synthetic.main.explore_item_banner.view.*
import kotlinx.android.synthetic.main.explore_item_category.view.*
import kotlinx.android.synthetic.main.explore_item_collection_horizontal.view.*
import kotlinx.android.synthetic.main.explore_item_collection_list.view.*
import kotlinx.android.synthetic.main.explore_item_collection_vertical.view.*
import kotlin.time.ExperimentalTime

private const val TYPE_BANNER = 0
private const val TYPE_CATEGORY = 1
private const val TYPE_COLLECTION_HORIZONTAL = 2
private const val TYPE_COLLECTION_VERTICAL = 3
private const val TYPE_LIST = 4
private const val EMPTY_SPACE = 5

class ExploreItemsLoadingAdapter(
    private val template: SectionTemplate,
    private val items: List<SectionLoadingState>
) : RecyclerView.Adapter<ExploreItemsLoadingAdapter.LoadingViewHolder>() {

    init {
        Log.i("loading", Gson().toJson(items))
        Log.i("loading", "template $template")
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].type) {
            SectionType.Banner -> TYPE_BANNER
            SectionType.Category -> TYPE_CATEGORY
            SectionType.VerticalCollection -> TYPE_COLLECTION_VERTICAL
            SectionType.HorizontalCollection -> TYPE_COLLECTION_HORIZONTAL
            SectionType.List -> TYPE_LIST
            SectionType.Empty -> EMPTY_SPACE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoadingViewHolder {
        return when (viewType) {
            TYPE_BANNER -> BannerViewHolder(parent.inflate(R.layout.explore_item_banner_loading))
            TYPE_CATEGORY -> CategoryViewHolder(parent.inflate(R.layout.explore_item_category_loading))
            TYPE_COLLECTION_HORIZONTAL -> HorizontalCollectionViewHolder(parent.inflate(R.layout.explore_item_collection_horizontal_loading))
            TYPE_COLLECTION_VERTICAL -> VerticalCollectionViewHolder(parent.inflate(R.layout.explore_item_collection_vertical_loading))
            else -> ListViewHolder(parent.inflate(R.layout.explore_item_collection_list_loading))
        }
    }

    abstract inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(template: SectionTemplate, emptyState: EmptyState)
    }

    private inner class BannerViewHolder(itemView: View) : LoadingViewHolder(itemView) {
        val banner: SessionBannerWidget = itemView.widgetBanner

        @ExperimentalTime
        override fun bind(template: SectionTemplate, emptyState: EmptyState) {
            banner.bind(content = null, template = template, emptyState = emptyState)
        }
    }

    private inner class CategoryViewHolder(itemView: View) : LoadingViewHolder(itemView) {
        val category: SessionCategoryWidget = itemView.widgetCategory
        override fun bind(template: SectionTemplate, emptyState: EmptyState) {
            category.bind(content = null, template = template, emptyState = emptyState)
        }
    }

    private inner class HorizontalCollectionViewHolder(itemView: View) :
        LoadingViewHolder(itemView) {
        val collectionHorizontal: SessionCollectionHorizontalWidget =
            itemView.widgetCollectionHorizontal

        override fun bind(template: SectionTemplate, emptyState: EmptyState) {
            collectionHorizontal.bind(
                content = null,
                template = template,
                emptyState = emptyState,
                indicatorColor = ""
            )
        }
    }

    private inner class VerticalCollectionViewHolder(itemView: View) :
        LoadingViewHolder(itemView) {
        val collectionCollectionVertical: SessionCollectionVerticalWidget =
            itemView.widgetCollectionVertical

        override fun bind(template: SectionTemplate, emptyState: EmptyState) {
            collectionCollectionVertical.bind(
                content = null,
                template = template,
                emptyState = emptyState,
                indicatorColor = ""
            )
        }
    }

    private inner class ListViewHolder(itemView: View) : LoadingViewHolder(itemView) {
        val list: SessionObjectListVerticalWidget = itemView.widgetList
        override fun bind(template: SectionTemplate, emptyState: EmptyState) {
            list.bind(content = null, template = template, indicatorColor = "")
        }
    }

    override fun onBindViewHolder(holder: LoadingViewHolder, position: Int) {
        holder.bind(template, EmptyState(SimpleMedia(""), "", ""))
    }

    override fun getItemCount(): Int = items.size
}
