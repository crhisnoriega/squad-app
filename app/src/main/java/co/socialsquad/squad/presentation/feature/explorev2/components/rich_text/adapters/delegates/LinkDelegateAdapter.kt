package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates

import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.nested_adapters.NestedLinkAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinks
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_rich_text_card_views.view.*

class LinkDelegateAdapter(private val listener: RichTextListener) : ViewTypeDelegateAdapter {

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return LinkViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as LinkViewHolder
        holder.bind(item as RichTextLinks)
    }

    private inner class LinkViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_rich_text_card_views)) {
        val recyclerView: RecyclerView = itemView.rv_item_rich_text_nested_list
        val textViewTitle: TextView = itemView.tv_item_rich_text_title
        fun bind(linkObject: RichTextLinks) = with(itemView) {
            textViewTitle.text = linkObject.title
            val childLayoutManager = LinearLayoutManager(recyclerView.context)
            childLayoutManager.initialPrefetchItemCount = linkObject.content?.size ?: 0
            recyclerView.apply {
                layoutManager = childLayoutManager
                adapter = NestedLinkAdapter(listener, linkObject.content)
                setRecycledViewPool(viewPool)
            }
        }
    }
}
