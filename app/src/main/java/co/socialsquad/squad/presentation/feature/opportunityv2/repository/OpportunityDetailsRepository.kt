package co.socialsquad.squad.presentation.feature.opportunityv2.repository

import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.feature.explorev2.data.ExploreAPI
import co.socialsquad.squad.presentation.feature.opportunityv2.model.OpportunityDetailsModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response

interface OpportunityDetailsRepository {
    fun fetchOpportunityDetails(submission_id: String): Flow<OpportunityDetailsModel>
    fun deleteOpportunityDetails(submission_id: String): Flow<Response<String?>>
    fun fetchDetailsTimetable(submissionId: String): Flow<Timetable?>
}

class OpportunityDetailsRepositoryImpl(private val exploreAPI: ExploreAPI) : OpportunityDetailsRepository {

    override fun fetchOpportunityDetails(submission_id: String): Flow<OpportunityDetailsModel> {
        return flow {
            val apiObjectList = exploreAPI.fetchOpportunityDetails(submission_id)
            emit(apiObjectList)
        }

    }

    override fun fetchDetailsTimetable(submissionId: String): Flow<Timetable?> =
            flow {
                emit(exploreAPI.fetchDetailsTimetable(submissionId).data)
            }

    override fun deleteOpportunityDetails(submission_id: String): Flow<Response<String?>> {
        return flow {
            val apiObjectList = exploreAPI.deleteOpportunityDetails(submission_id)
            emit(apiObjectList)
        }

    }
}