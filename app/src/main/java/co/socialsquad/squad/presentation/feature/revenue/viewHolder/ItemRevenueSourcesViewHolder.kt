package co.socialsquad.squad.presentation.feature.revenue.viewHolder

import android.net.Uri
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import kotlinx.android.synthetic.main.item_revenue_sources.view.*
import kotlinx.android.synthetic.main.item_revenue_sources.view.mainLayout
import kotlinx.android.synthetic.main.item_revenue_sources.view.txtTitle

class ItemRevenueSourcesViewHolder(
    val itemView: View,
    val callback: (viewModel: ItemRevenueSourceViewModel) -> Unit
) :
    ViewHolder<ItemRevenueSourceViewModel>(itemView) {
    override fun bind(viewModel: ItemRevenueSourceViewModel) {
        itemView.mainLayout.setOnClickListener {
            callback.invoke(viewModel)
        }
        itemView.txtTitle.text = viewModel.sectionsData.title
        itemView.txtSubtitle.text = viewModel.sectionsData.subtitle

        viewModel.sectionsData.estimated_earnings?.let {
            itemView.txtRevenue.text = it
        }

        viewModel.sectionsData.estimated_receipt?.let {
            itemView.txtRevenue.text = it
        }

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.sectionsData?.icon), itemView.imgIcon)
    }

    override fun recycle() {

    }
}