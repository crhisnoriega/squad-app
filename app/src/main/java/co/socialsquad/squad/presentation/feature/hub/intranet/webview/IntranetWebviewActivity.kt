package co.socialsquad.squad.presentation.feature.hub.intranet.webview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_intranet_webview.*

class IntranetWebviewActivity : BaseDaggerActivity() {

    private val url by extra<String>(EXTRA_URL)
    private val name by extra<String?>(EXTRA_NAME)
    private val color by extra<String?>(EXTRA_COLOR)

    companion object {
        const val EXTRA_URL = "extra_url"
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_COLOR = "extra_color"

        fun newInstance(context: Context, url: String, name: String?, color: String?): Intent {
            return Intent(context, IntranetWebviewActivity::class.java).apply {
                putExtra(EXTRA_URL, url)
                putExtra(EXTRA_NAME, name)
                color?.let {
                    putExtra(EXTRA_COLOR, color)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intranet_webview)

        setupWebView(url)
        name.let {
            toolbarTitle.text = name
        }
        color?.apply {
            val backgroundColor = Color.parseColor(this)
            tToolbar.setBackgroundColor(backgroundColor)

            window?.apply {
                backgroundColor.let {
                    statusBarColor = it
                }
                decorView.systemUiVisibility = 0
            }
        }

        ibBack.setOnClickListener { finish() }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView(url: String) {
        webView.run {
            clearCache(true)
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    pbLoading.visibility = View.GONE
                    webView.visibility = View.VISIBLE
                }
            }
            clearHistory()
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            loadUrl(url)
            pbLoading.visibility = View.VISIBLE
        }
    }
}
