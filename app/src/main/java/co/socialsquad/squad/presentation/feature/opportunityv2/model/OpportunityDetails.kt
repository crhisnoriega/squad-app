package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class OpportunityDetails(
        @SerializedName("object") val object1: OpportunityObject?,
        @SerializedName("lead") val lead: List<OpportunityLead>?
) : Parcelable