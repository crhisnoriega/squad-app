package co.socialsquad.squad.presentation.feature.immobiles.details

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Immobile
import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.entity.opportunity.Information
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityAction
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.PageIndicator
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.immobiles.details.moredetails.MoreDetailsActivity
import co.socialsquad.squad.presentation.feature.opportunity.OpportunityActivity
import co.socialsquad.squad.presentation.feature.opportunity.detail.OpportunitySubmissionDetailActivity
import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityInfoActivity
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.ITEM_OPPORTUNITY_LIST_ITEM
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListViewHolder
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.shareItem
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import io.branch.indexing.BranchUniversalObject
import kotlinx.android.synthetic.main.activity_immobile_detail.*
import javax.inject.Inject

class ImmobilesDetailsActivity : BaseDaggerActivity(), ImmobilesDetailsView {

    companion object {
        const val extraImmobilePk = "extra_immobile_pk"
        const val extraImmobile = "extra_immobile"
        const val extraPrimaryColor = "extra_primary_color"
    }

    private var sheetBehavior: BottomSheetBehavior<*>? = null
    private var colors: OpportunityInitialsColors? = null
    private var opportunity: Opportunity? = null
    private val immobileJson by extra<String>(extraImmobile)
    private val immobile: Immobile by lazy {
        val gson = Gson()
        gson.fromJson(immobileJson, Immobile::class.java)
    }
    private lateinit var primaryColor: String

    @Inject
    lateinit var presenter: ImmobilesDetailsPresenter

    @Inject
    lateinit var loginRepository: LoginRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_immobile_detail)

        immobile.apply {
            setupViews(this)
            setupImages(this)
        }
        setupBottomSheetActions()
        setupButtons()
        immobile.apply { presenter.immobileData(this.pk, immobile.opportunities ?: emptyList()) }
        if (!immobile.opportunities.isNullOrEmpty()) {
            opportunity = immobile.opportunities!![0]
        }
        presenter.onStart()
    }

    private fun setupBottomSheetActions() {
        sheetBehavior = BottomSheetBehavior.from(llBottonSheetActions)
        nsvContent.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_MOVE -> showActions(false)
                MotionEvent.ACTION_UP -> showActions(true)
            }
            false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OpportunityActivity.OPPORTUNITY_RC_CODE && resultCode == Activity.RESULT_OK) {
            presenter.immobileData(immobile.pk, immobile.opportunities ?: emptyList())
        }
    }

    override fun showLoading(show: Boolean) {
        progressOverlay.show(show)
    }

    private fun setupButtons() {
        locationContainer.setOnClickListener {
            val uri = "geo:" + immobile.lat.toString() + "," + immobile.lng.toString()
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
        }
        back.setOnClickListener { finish() }
        btRecommend.setOnButtonClickListener {
            btRecommend.loading(true)
            opportunity?.let {
                presenter.getOpportunityAction(it)
            }
        }
    }

    override fun setupDetails(details: MutableList<ImmobileDetail>) {
        if (details.isNotEmpty()) {
            detailsList.run {
                layoutManager = LinearLayoutManager(baseContext)
                adapter =
                    ImmobileDetailsAdapter(details, this@ImmobilesDetailsActivity::onDetailSelected)
                visibility = View.VISIBLE
            }

            detailsSeparator.visibility = View.VISIBLE
            finalDetailsSeparator.visibility = View.VISIBLE
            detailsLabel.visibility = View.VISIBLE
        }
    }

    override fun setupShare(subdomain: String?) {
        share.setOnClickListener {
            immobile.apply {
                val viewModel =
                    ShareViewModel(
                        this.pk,
                        this.name,
                        this.detail,
                        this.medias[0].url,
                        Share.Immobile,
                        this.can_share,
                        subdomain
                    )
                BranchUniversalObject().shareItem(viewModel, Share.Immobile, baseContext)
            }
        }
    }

    private fun setupImages(immobile: Immobile) {
        vpItems.adapter = PagerAdapter(supportFragmentManager, immobile.medias)
        vpItems.clipToPadding = false

        if (immobile.medias.size > 1) {
            with(piIndicator) {
                viewPager = vpItems
                types = immobile.medias.map {
                    when (it.mime_type) {
                        "video/mp4" -> PageIndicator.Type.Video()
                        else -> PageIndicator.Type.Image()
                    }
                }
                intent.hasExtra(extraPrimaryColor).takeIf { it }?.apply {
                    primaryColor = intent.extras?.getString(extraPrimaryColor)!!
                    selectedColor = ColorUtils.parse(intent.extras?.getString(extraPrimaryColor)!!)
                }
                init()
            }
        } else {
            piIndicator.visibility = View.GONE
        }
    }

    private fun setupViews(immobile: Immobile) {
        value.text = immobile.start_price

        name.text = immobile.name_long
        status.text = immobile.detail

        byWhom.text = immobile.owner.user.first_name
        audience.text = immobile.audience

        var complement = ""

        if (immobile.address_complement != null && immobile.address_complement.isNotEmpty()) {
            complement = immobile.address_complement.plus(" - ")
        }

        address.text = immobile.address_street.plus(", ")
            .plus(immobile.address_number).plus(" - ")
            .plus(complement)
            .plus(immobile.address_district).plus(", ")
            .plus(immobile.address_city).plus(" - ")
            .plus(immobile.address_state).plus(", ")
            .plus(immobile.address_cep)

        immobile.audience_type?.let {
            audienceImage.setImageResource(
                presenter.getDrawableResId(
                    it
                )
            )
        }
    }

    override fun onOpportunityActionClick(
        opportunity: Opportunity,
        opportunityAction: OpportunityAction
    ) {
        btRecommend.loading(false)
        val intent =
            OpportunityActivity.newIntent(
                this,
                opportunity,
                opportunityAction,
                OpportunityObjectType.REAL_STATE,
                immobile.pk,
                colors
            )
        startActivityForResult(intent, OpportunityActivity.OPPORTUNITY_RC_CODE)
    }

    override fun setupOpportunitiesSubmissions(opportunities: List<Opportunity>) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is OpportunityListViewModel -> ITEM_OPPORTUNITY_LIST_ITEM
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ITEM_OPPORTUNITY_LIST_ITEM -> OpportunityListViewHolder(
                    view,
                    presenter,
                    OpportunityObjectType.REAL_STATE
                )
                else -> throw IllegalArgumentException()
            }
        }

        with(opportunitiesList) {
            layoutManager = LinearLayoutManager(context)
            adapter = RecyclerViewAdapter(viewHolderFactory).apply {

                val company = loginRepository.userCompany?.company
                colors = OpportunityInitialsColors(company?.primaryColor, company?.secondaryColor)

                val items =
                    opportunities
                        .map { OpportunityListViewModel(it, colors!!) }
                addItems(items)
            }
            setHasFixedSize(true)
        }
    }

    override fun showActions(show: Boolean) {
        Handler().post {
            sheetBehavior?.state = if (show) BottomSheetBehavior.STATE_EXPANDED else BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun onOpportunityDetailsClick(submissionDetails: SubmissionDetails) {
        this.startActivity(
            OpportunitySubmissionDetailActivity.newIntent(
                this,
                submissionDetails,
                colors,
                opportunity?.title
            )
        )
    }

    override fun onOpportunityInfoClick(info: Information) {
        this.startActivity(OpportunityInfoActivity.newIntent(this, info, colors!!))
    }

    private fun onDetailSelected(detail: ImmobileDetail) {
        val gson = Gson()
        val json = gson.toJson(detail)

        val intent = Intent(this, MoreDetailsActivity::class.java)
        intent.putExtra(MoreDetailsActivity.extraDetails, json)
        intent.putExtra(MoreDetailsActivity.extraPrimaryColor, primaryColor)
        startActivity(intent)
    }

    class PagerAdapter(fm: FragmentManager, val medias: List<Immobile.Media>) :
        FragmentPagerAdapter(fm) {

        override fun getCount(): Int = medias.size

        override fun getItem(position: Int): Fragment =
            ImmobilesImagesFragment.newInstance(medias[position])
    }
}
