package co.socialsquad.squad.presentation.feature.login.subdomain

import android.content.Intent
import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativeActivity
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailActivity
import kotlinx.android.synthetic.main.activity_login_subdomain.*
import javax.inject.Inject

class LoginSubdomainActivity : BaseDaggerActivity(), LoginSubdomainView {

    companion object {
        const val EXTRA_LOGIN_SUBDOMAIN = "EXTRA_LOGIN_SUBDOMAIN"
    }

    @Inject
    lateinit var presenter: LoginSubdomainPresenter

    public override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_subdomain)

        presenter.onCreate()

        setupToolbar()
        setupKeyboard()
        setupKeyboardAction()
        setupNextButtonBehavior()
        setupOnClickListeners()
    }

    private fun setupToolbar() {
        setSupportActionBar(tToolbar)
    }

    private fun setupKeyboard() {
        iltInput.requestFocus()
    }

    private fun setupKeyboardAction() {
        iltInput.setOnKeyboardActionListener { bNext.performClick() }
    }

    private fun setupNextButtonBehavior() {
        bNext.isEnabled = false
        iltInput.addTextChangedListener { text -> presenter.onInputChanged(text) }
    }

    private fun setupOnClickListeners() {
        ibBack.setOnClickListener { finish() }
        bNext.setOnClickListener { presenter.onNextClicked() }
        tvAlternative.setOnClickListener { presenter.onAlternativeClicked() }
    }

    override fun enableNextButton(enabled: Boolean) {
        bNext.isEnabled = enabled
    }

    override fun showMessage(resId: Int) {
        iltInput.setErrorText(getString(resId))
    }

    override fun showLoading() {
        iltInput.loading(true)
    }

    override fun hideLoading() {
        iltInput.loading(false)
    }

    override fun openLoginEmail(subdomain: String) {
        val intent = Intent(this, LoginEmailActivity::class.java)
            .putExtra(EXTRA_LOGIN_SUBDOMAIN, subdomain)
        startActivity(intent)
    }

    override fun openLoginAlternative(subdomain: String) {
        val intent = Intent(this, LoginAlternativeActivity::class.java)
            .putExtra(EXTRA_LOGIN_SUBDOMAIN, subdomain)
        startActivity(intent)
    }
}
