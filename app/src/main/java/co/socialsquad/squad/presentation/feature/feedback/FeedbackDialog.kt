package co.socialsquad.squad.presentation.feature.feedback

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.feedback.FeedbackActivity.Companion.EXTRA_FEEDBACK_VIEW_MODEL
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_feedback.*

class FeedbackDialog : BottomSheetDialogFragment() {

    companion object {
        const val ARG_FEEDBACK_VIEW_MODEL = "ARG_FEEDBACK_VIEW_MODEL"

        fun newInstance(feedbackViewModel: FeedbackViewModel) = FeedbackDialog().apply {
            arguments = Bundle().apply { putSerializable(ARG_FEEDBACK_VIEW_MODEL, feedbackViewModel) }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dialog_feedback, container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        card_view.setBackgroundResource(R.drawable.cardview_top_radius)
        val feedbackViewModel = arguments?.getSerializable(ARG_FEEDBACK_VIEW_MODEL) as FeedbackViewModel?
        feedbackViewModel?.let {
            tvTitle.text = it.title
            tvDescription.text = it.description
            setupAvatar(it.icon)
        }

        bAccept.setOnClickListener {
            startActivity(Intent(context, FeedbackActivity::class.java).putExtra(EXTRA_FEEDBACK_VIEW_MODEL, feedbackViewModel))
            dismiss()
        }

        SwipeDetector(nestedScroll).setOnSwipeListener(object : SwipeDetector.OnSwipeEvent {
            override fun swipeEventDetected(v: View, swipeType: SwipeDetector.SwipeTypeEnum) {
                if (swipeType == SwipeDetector.SwipeTypeEnum.TOP_TO_BOTTOM) {
                    activity?.apply {
                        this.supportFragmentManager.beginTransaction()
                            .remove(this@FeedbackDialog)
                            .commitAllowingStateLoss()
                    }
                }
            }
        })
    }

    private fun setupAvatar(imageUrl: String) {
        Glide.with(this).load(imageUrl).into(ivIcon)
    }
}
