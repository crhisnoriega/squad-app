package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Resource(
        @SerializedName("type") val type: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("link") val link: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("resources") val resources: List<Resource> = listOf(),
):Parcelable