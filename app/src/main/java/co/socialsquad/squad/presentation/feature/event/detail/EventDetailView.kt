package co.socialsquad.squad.presentation.feature.event.detail

import co.socialsquad.squad.presentation.feature.event.EventViewModel

interface EventDetailView {
    fun showEvent(eventViewModel: EventViewModel)
    fun showMessage(textResId: Int, onDismissListener: (() -> Unit) = {})
    fun close()
    fun checkPermissions(permissions: Array<String>): Boolean
    fun requestPermissions(requestCode: Int, permissions: Array<String>)
    fun createLocationRequest()
    fun setupDistance(latitude: Double, longitude: Double, eventViewModel: EventViewModel)
    fun showLoading()
    fun hideLoading()
    fun showActions(show: Boolean)
    fun updateAttendance(eventViewModel: EventViewModel, currentUserPk: Int)
    fun showCalendarApp(eventViewModel: EventViewModel)
    fun showSuccessDialog(eventViewModel: EventViewModel)
    fun setButtonConfirmText(text: Int)
    fun setButtonEnabled(enable: Boolean)
}
