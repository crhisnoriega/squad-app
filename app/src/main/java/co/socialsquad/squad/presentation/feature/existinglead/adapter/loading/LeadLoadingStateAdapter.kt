package co.socialsquad.squad.presentation.feature.existinglead.adapter.loading

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

private const val ITEM_LOADING_COUNT = 10

class LeadLoadingStateAdapter : RecyclerView.Adapter<LeadLoadingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeadLoadingViewHolder {
        return LeadLoadingViewHolder(parent)
    }

    override fun onBindViewHolder(holder: LeadLoadingViewHolder, position: Int) = holder.bind()

    override fun getItemCount(): Int = ITEM_LOADING_COUNT
}
