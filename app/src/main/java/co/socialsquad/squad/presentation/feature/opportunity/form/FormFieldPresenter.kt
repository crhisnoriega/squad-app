package co.socialsquad.squad.presentation.feature.opportunity.form

import android.text.InputType
import co.socialsquad.squad.data.entity.opportunity.Field
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.TextUtils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FormFieldPresenter @Inject constructor(
    private val viewForm: FormFieldView
) {
    private lateinit var field: Field
    private val compositeDisposable = CompositeDisposable()
    private var value: String = ""

    fun start(field: Field) {
        this.field = field
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onInputChanged(text: String) {
        this.value = text.trim()
        val isValid = isInputValid(text)
        viewForm.enableNextButton(isValid)
    }

    fun isInputValid(text: String): Boolean {

        if (field.isMandatory && text.isBlank()) return false

        return if (text.isNotEmpty()) {
            when (field.inputType) {
                "email" -> TextUtils.isValidEmail(text)
                "phone" -> text.isBlank() || text.length == 15
                else -> true
            }
        } else {
            !field.isMandatory
        }
    }

    fun setupField() {
        viewForm.setInputType(inputType())
        viewForm.setInputMask(inputMask())
        viewForm.setHintText(field.label)
        viewForm.setAdditionalText(field.additionalText, field.additionalTextEmphasized)
    }

    private fun inputMask(): String? {
        return field.fieldMask?.let {
            if (it.equals("phone")) {
                MaskUtils.Mask.MOBILE
            } else {
                null
            }
        }
    }

    private fun inputType(): Int? {
        return when (field.inputType) {
            "email" -> InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            "phone" -> InputType.TYPE_CLASS_PHONE
            else -> {
                if (field.capitalLetter) InputType.TYPE_TEXT_FLAG_CAP_WORDS else InputType.TYPE_CLASS_TEXT
            }
        }
    }
}
