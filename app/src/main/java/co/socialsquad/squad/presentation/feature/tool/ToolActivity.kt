package co.socialsquad.squad.presentation.feature.tool

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.company.SelectionItemViewModel
import co.socialsquad.squad.presentation.feature.company.SelectionViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ToolContent
import co.socialsquad.squad.presentation.feature.tool.di.ToolModule
import co.socialsquad.squad.presentation.feature.tool.lead.TOOL_LEAD_VIEW_HOLDER_MODEL_ID
import co.socialsquad.squad.presentation.feature.tool.lead.ToolLeadHolderModel
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_tool.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject

private const val EXTRA_CONTENT = "EXTRA_CONTENT"

class ToolActivity : BaseActivity() {

    companion object {
        fun start(context: Context, content: ToolContent) {
            val intent = Intent(context, ToolActivity::class.java).apply {
                putExtra(EXTRA_CONTENT, content)
            }
            context.startActivity(intent)
        }
    }

    private lateinit var adapter: RecyclerViewAdapter
    private val items: List<SelectionItemViewModel> = listOf()
    private var itemSelected: SelectionItemViewModel? = null
    private val content by extra<ToolContent>(EXTRA_CONTENT)
    private val viewModel by inject<ToolViewModel>()
    override val modules = listOf(ToolModule.instance)
    override val contentView = R.layout.activity_tool

    private val onSelectedListener = object : ViewHolder.Listener<SelectionItemViewModel> {
        override fun onClick(viewModel: SelectionItemViewModel) {
            // presenter.selectItem(viewModel)
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupList()
        setupClose()
        setupTitle(content.title)
        setupSubtitle(content.subtitle)
        viewModel.fetchOpportunities(content.link)
        setupSectionObservable()
    }

    private fun setupClose() {
        ib_close.setOnClickListener {
            finish()
        }
    }

    private fun setupTitle(title: String) {
        lbl_title.text = title
    }

    private fun setupSubtitle(subtitle: String) {
        lbl_subtitle.text = subtitle
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ToolLeadHolderModel -> TOOL_LEAD_VIEW_HOLDER_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    TOOL_LEAD_VIEW_HOLDER_MODEL_ID -> SelectionViewHolder(view, onSelectedListener)
                    else -> throw IllegalArgumentException()
                }
            }
        })
        rvLeads.layoutManager = LinearLayoutManager(this)
        rvLeads.adapter = adapter
        rvLeads.isNestedScrollingEnabled = false
    }

    @ExperimentalCoroutinesApi
    private fun setupSectionObservable() {
        createFlow(viewModel.tools)
            .success {
                it?.items?.let { submissions ->
                    submissions.map { submission ->
                        // ToolLeadHolderModel(submission.title, submission.timestamp, )
                    }
                }
                adapter.setItems(items)
            }
            .launch()
    }
}
