package co.socialsquad.squad.presentation.feature.explorev2.components.tools

import android.util.Log
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.OpportunityWidget
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.tool.ToolsView
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Tool

class ToolsViewHolder(
        itemView: View,
        private val objectType: String,
        private val objectId: Long,
        private val companyColor: CompanyColor,
        private val onNewSubmissionCallback: (opportunityWidget: OpportunityWidget) -> Unit,
        private val onNewEscalaCallback: (tool: Tool) -> Unit,
        private val onNewOpportunity: (tool: Tool) -> Unit,
) :
        ViewHolder<ToolsViewHolderModel>(itemView) {

    private val toolView: ToolsView = itemView.findViewById(R.id.tool)

    override fun bind(viewModel: ToolsViewHolderModel) {
        toolView.onNewEscalaCallback = {}
        toolView.onNewSubmissionCallback = {}
        toolView.onEmptyClickListener = {
            Log.i("empty", "called: ${it.content.title}")
            when (it.content.title) {
                "Reservas" -> {
                    onNewEscalaCallback(it)
                }

                "Indicações" -> {
                    onNewOpportunity(it)
                }
            }
        }
        viewModel.tool.let { tool ->
            when (tool.type) {
                RankingType.Oportunidade -> {
                    toolView.onNewSubmissionCallback = onNewSubmissionCallback
                    toolView.bind(objectType, objectId, tool, companyColor)
                }
                RankingType.Escala -> {
                    toolView.onNewEscalaCallback = onNewEscalaCallback
                    toolView.bind(objectType, objectId, tool, companyColor)
                }
                RankingType.Invalid -> {
                }
                else -> {
                }
            }
        }
    }

    override fun recycle() {
        toolView.update()
    }
}

class ToolsViewHolderModel(internal val tool: Tool) : ViewModel
