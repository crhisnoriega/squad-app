package co.socialsquad.squad.presentation.feature.explore

import co.socialsquad.squad.presentation.feature.audio.list.AudioListFragment
import co.socialsquad.squad.presentation.feature.audio.list.AudioListModule
import co.socialsquad.squad.presentation.feature.document.list.DocumentListFragment
import co.socialsquad.squad.presentation.feature.document.list.DocumentListModule
import co.socialsquad.squad.presentation.feature.event.list.EventListFragment
import co.socialsquad.squad.presentation.feature.event.list.EventListModule
import co.socialsquad.squad.presentation.feature.immobiles.ImmobilesFragment
import co.socialsquad.squad.presentation.feature.immobiles.ImmobilesModule
import co.socialsquad.squad.presentation.feature.live.list.LiveListFragment
import co.socialsquad.squad.presentation.feature.live.list.LiveListModule
import co.socialsquad.squad.presentation.feature.store.StoreFragment
import co.socialsquad.squad.presentation.feature.store.StoreModule
import co.socialsquad.squad.presentation.feature.store.list.StoreListFragment
import co.socialsquad.squad.presentation.feature.store.list.StoreListModule
import co.socialsquad.squad.presentation.feature.trip.list.TripListFragment
import co.socialsquad.squad.presentation.feature.trip.list.TripListModule
import co.socialsquad.squad.presentation.feature.video.list.VideoListFragment
import co.socialsquad.squad.presentation.feature.video.list.VideoListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ExploreModule {
    @ContributesAndroidInjector(modules = [LiveListModule::class])
    fun liveListFragment(): LiveListFragment

    @ContributesAndroidInjector(modules = [StoreModule::class])
    fun storeFragment(): StoreFragment

    @ContributesAndroidInjector(modules = [ImmobilesModule::class])
    fun immobilesFragment(): ImmobilesFragment

    @ContributesAndroidInjector(modules = [AudioListModule::class])
    fun audioListFragment(): AudioListFragment

    @ContributesAndroidInjector(modules = [VideoListModule::class])
    fun videoListFragment(): VideoListFragment

    @ContributesAndroidInjector(modules = [EventListModule::class])
    fun eventListFragment(): EventListFragment

    @ContributesAndroidInjector(modules = [StoreListModule::class])
    fun storeListFragment(): StoreListFragment

    @ContributesAndroidInjector(modules = [DocumentListModule::class])
    fun documentListFragment(): DocumentListFragment

    @ContributesAndroidInjector(modules = [TripListModule::class])
    fun tripListFragment(): TripListFragment
}
