package co.socialsquad.squad.presentation.feature.video

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory

private const val AUDIO_ENABLED = 1f
private const val AUDIO_DISABLED = 0f

class VideoViewer(
    private val context: Context,
    private val simpleExoPlayerView: PlayerView,
    private val url: String,
    playbackPosition: Long,
    private val listener: Listener?
) : Player.EventListener {
    override fun onSeekProcessed() {}
    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}

    private var player: SimpleExoPlayer? = null
    private var audioEnabled: Boolean = false

    var playbackPosition: Long = 0
        set(value) {
            field = value
            player?.seekTo(value)
        }
        get() = if (field > 0) field else player?.currentPosition ?: 0

    init {
        this.playbackPosition = playbackPosition
    }

    fun prepare(audioEnabled: Boolean, playWhenReady: Boolean = true) {
        this.audioEnabled = audioEnabled

        if (player == null) {
            val defaultTrackSelector = DefaultTrackSelector(context)
            val loadControl = DefaultLoadControl()
            player = SimpleExoPlayer
                .Builder(context)
                .setTrackSelector(defaultTrackSelector)
                .setLoadControl(loadControl)
                .build()
            simpleExoPlayerView.player = player
            player?.addListener(this)
        }

        player?.prepare(getMediaSource(Uri.parse(url)), true, false)
        player?.seekTo(playbackPosition)
        player?.volume = if (audioEnabled) AUDIO_ENABLED else AUDIO_DISABLED
        if (playWhenReady) player?.playWhenReady = true
    }

    fun prepare(audioEnabled: Boolean, playbackPosition: Long) {
        this.playbackPosition = playbackPosition
        prepare(audioEnabled)
    }

    fun playWhenReady() {
        player?.playWhenReady = true
    }

    fun isPlaying() = player?.playWhenReady ?: false

    fun toggleAudioEnabled(): Boolean {
        player?.apply {
            volume = if (audioEnabled) AUDIO_DISABLED else AUDIO_ENABLED
        }
        audioEnabled = !audioEnabled
        return audioEnabled
    }

    private fun getMediaSource(uri: Uri): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSourceFactory("ua")
        val mediaItem = MediaItem.Builder().setUri(uri).build()
        val defaultDataSourceFactory =
            DefaultDataSourceFactory(context, null, defaultHttpDataSourceFactory)
        return HlsMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(mediaItem)
    }

    fun stop() {
        player?.apply {
            playbackPosition = currentPosition
            stop()
        }
    }

    fun pause() {
        player?.playWhenReady = false
    }

    fun release() {
        player?.apply {
            playbackPosition = 0
            stop()
            removeListener(this@VideoViewer)
            release()
        }
        player = null
    }

    override fun onTracksChanged(
        trackGroups: TrackGroupArray,
        trackSelections: TrackSelectionArray
    ) {
    }

    override fun onLoadingChanged(isLoading: Boolean) {}

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            Player.STATE_READY -> listener?.onPlayerReady(this@VideoViewer)
            Player.STATE_ENDED -> {
                listener?.onPlayerEnded(this@VideoViewer)
                player?.stop()
                player?.seekTo(0)
            }
            Player.STATE_IDLE, Player.STATE_BUFFERING -> {
            }
            else -> {
            }
        }
    }

    override fun onRepeatModeChanged(repeatMode: Int) {}

    override fun onPlayerError(error: ExoPlaybackException) {}

    override fun onPositionDiscontinuity(reason: Int) {}

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}

    interface Listener {
        fun onPlayerReady(videoViewer: VideoViewer)
        fun onPlayerEnded(videoViewer: VideoViewer)
    }
}
