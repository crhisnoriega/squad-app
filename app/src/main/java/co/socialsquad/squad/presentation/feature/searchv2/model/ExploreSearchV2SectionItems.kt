package co.socialsquad.squad.presentation.feature.searchv2.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreSearchV2SectionItems(
    @SerializedName("id")
    val id: String,
    @SerializedName("list")
    val list: ExploreSearchV2Section
) : Parcelable
