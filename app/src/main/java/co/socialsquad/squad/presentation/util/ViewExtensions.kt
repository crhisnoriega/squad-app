package co.socialsquad.squad.presentation.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun View.showKeyboard() {
    showKeyboard(context, this)
}

fun View.hideKeyboard() {
    hideKeyboard(context, this)
}

fun View.setGradientBackground(startColor: String, endColor: String) {
    val colors = intArrayOf(Color.parseColor(startColor.replace(" ", "")), Color.parseColor(endColor.replace(" ", "")))
    val gradientDrawable = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors)
    gradientDrawable.cornerRadius = 0f
    background = gradientDrawable
}

private fun getInputMethodManager(context: Context) =
    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?

private fun showKeyboard(context: Context, view: View) {
    getInputMethodManager(context)?.showSoftInput(view, 0)
}

private fun hideKeyboard(context: Context, view: View) {
    getInputMethodManager(context)?.hideSoftInputFromWindow(view.windowToken, 0)
}

fun TextView.setTextColorFromRes(context: Context, colorRes: Int) {
    setTextColor(ContextCompat.getColor(context, colorRes))
}

/**
 * Retorna ou define como GONE o estado de exibição de uma View.
 */
inline var View.isGone: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

/**
 * Retorna ou define como INVISIBLE o estado de exibição de uma View.
 */
inline var View.isInvisibile: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

/**
 * Retorna ou define como VISIBLE o estado de exibição de uma View.
 */
inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

fun AppBarLayout.elevationByScroll(recyclerView: RecyclerView) {

    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        var scrollDy = 0

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            scrollDy += dy
            var ratio = scrollDy.toFloat() / context.convertDpToPixels(100f).toFloat()
            if (ratio > 1)
                ratio = 1f
            elevation = (context.convertDpToPixels(4f) * ratio)
        }
    })
}

fun CardView.elevationByScroll(recyclerView: RecyclerView) {

    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        var scrollDy = 0

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            scrollDy += dy
            var ratio = scrollDy.toFloat() / context.convertDpToPixels(100f).toFloat()
            if (ratio > 1)
                ratio = 1f
            elevation = (context.convertDpToPixels(4f) * ratio)
        }
    })
}

fun View.fadeOutGone() {
    animate()
        .alpha(0.0f)
        .setDuration(1000)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                visibility = View.GONE
                alpha = 1.0f
            }
        })
}
