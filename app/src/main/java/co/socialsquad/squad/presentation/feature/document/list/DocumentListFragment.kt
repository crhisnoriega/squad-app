package co.socialsquad.squad.presentation.feature.document.list

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_document_list.*
import javax.inject.Inject

class DocumentListFragment : Fragment(), DocumentListView {

    @Inject
    lateinit var presenter: DocumentListPresenter

    private lateinit var adapter: RecyclerViewAdapter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_document_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSwipeRefresh()
        presenter.onViewCreated()
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener {
            srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            presenter.onRefresh()
        }
    }

    override fun setupList(companyColor: String?) {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is DocumentViewModel -> R.layout.view_document_list_item
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                R.layout.view_document_list_item -> DocumentListItemViewHolder(view, onDocumentClickedListener)
                else -> throw IllegalArgumentException()
            }
        })
        rv.apply {
            adapter = this@DocumentListFragment.adapter
            layoutManager = PrefetchingLinearLayoutManager(context)
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    private val onDocumentClickedListener = object : ViewHolder.Listener<DocumentViewModel> {
        override fun onClick(viewModel: DocumentViewModel) {
            presenter.onDocumentClicked(viewModel)
        }
    }

    override fun startLoading() {
        adapter.startLoading()
        if (!srl.isRefreshing) srl.isEnabled = false
    }

    override fun stopLoading() {
        adapter.stopLoading()
        srl.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun addItems(documents: List<DocumentViewModel>, shouldClearList: Boolean) {
        with(adapter) {
            if (shouldClearList) {
                setItems(documents)
            } else {
                addItems(documents)
            }
        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit?) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun showEmptyState() {
        srl.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.VISIBLE
    }
}
