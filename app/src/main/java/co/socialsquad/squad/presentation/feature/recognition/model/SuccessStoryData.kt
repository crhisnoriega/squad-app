package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SuccessStoryData(

        @SerializedName("thumbnail_avatar") val thumbnail_avatar: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("user") val user: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("timestamp") val timestamp: String?,
        @SerializedName("details") val details: Boolean?,


        var first: Boolean = false
) : Parcelable