package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreList(
    @SerializedName("count") val count: Int,
    @SerializedName("template") @JsonAdapter(SectionTemplateTypeAdapter::class) val template: SectionTemplate,
    @SerializedName("previous") val previous: String,
    @SerializedName("next") val next: String,
    @SerializedName("items") var items: List<ListItem>
) : Parcelable
