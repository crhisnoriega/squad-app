package co.socialsquad.squad.presentation.feature.immobiles.viewModel

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Immobile
import co.socialsquad.squad.presentation.custom.ViewModel

const val IMMOBILE_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_immobile_list_item

class ImmobileViewModel(
    val active: String,
    val pk: Int,
    val active_display: String,
    val address_cep: String,
    val address_district: String,
    val address_number: String,
    val address_state: String,
    val address_street: String,
    val all_squad: String,
    val audience: String,
    val audience_type: String?,
    val name: String,
    val owner: Immobile.Owner,
    val lat: Double,
    val lng: Double,
    val detail: String,
    val room_number: String?,
    val start_price: String,
    val square_meters: String? = null,
    val garage_number: String?,
    val suite_number: String?,
    val can_share: Boolean,
    val medias: List<Immobile.Media>
) : ViewModel {
    constructor(immobile: Immobile) : this(
        immobile.active,
        immobile.pk,
        immobile.active_display,
        immobile.address_cep,
        immobile.address_district,
        immobile.address_number,
        immobile.address_state,
        immobile.address_street,
        immobile.all_squad,
        immobile.audience,
        immobile.audience_type,
        immobile.name,
        immobile.owner,
        immobile.lat,
        immobile.lng,
        immobile.detail,
        immobile.room_number,
        immobile.start_price,
        immobile.square_meters,
        immobile.garage_number,
        immobile.suite_number,
        immobile.can_share,
        immobile.medias
    )
}
