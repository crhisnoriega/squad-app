package co.socialsquad.squad.presentation.feature.profile.others

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.presentation.feature.profile.ProfileInteractor
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.LiveListViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModelMapper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserPresenter @Inject constructor(
    private var profileInteractor: ProfileInteractor,
    private var userView: UserView,
    private val socialRepository: SocialRepository,
    loginRepository: LoginRepository
) {

    private val color = loginRepository.squadColor
    private val currentUserAvatar = loginRepository.userCompany?.user?.avatar
    private var userPk: Int = -1
    private var page = 1
    private var isLoading = false
    private var isComplete = false
    private val subdomain = loginRepository.subdomain
    private val mapper = SocialViewModelMapper()
    private val compositeDisposable = CompositeDisposable()

    fun onCreate(intent: Intent?) {
        userPk = intent?.getIntExtra(USER_PK_EXTRA, -1) ?: -1
        getProfileAndPosts()
    }

    private fun getProfileAndPosts() {
        compositeDisposable.add(
            profileInteractor.getProfile(userPk)
                .flatMap {
                    userView.showProfile(it)
                    profileInteractor.getPosts(userPk, page)
                }
                .doOnSubscribe {
                    isLoading = true
                    userView.showLoading()
                }
                .doOnTerminate {
                    isLoading = false
                    userView.hideLoading()
                }
                .doOnNext { isComplete = it.next == null }
                .map { mapper.mapItems(it, socialRepository.audioEnabled, currentUserAvatar, color, subdomain) }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        userView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        userView.showMessage(R.string.profile_error_get_posts)
                    }
                )
        )
    }

    fun onRefresh() {
        compositeDisposable.clear()
        page = 1
        getProfileAndPosts()
    }

    fun onAudioToggled(audioEnabled: Boolean) {
        socialRepository.audioEnabled = audioEnabled
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isComplete || isLoading) return

        compositeDisposable.add(
            profileInteractor.getPosts(userPk, page)
                .doOnSubscribe {
                    isLoading = true
                    userView.showLoadingMore()
                }
                .doOnTerminate {
                    isLoading = false
                }
                .doOnNext { isComplete = it.next == null }
                .map { mapper.mapItems(it, socialRepository.audioEnabled, currentUserAvatar, color, subdomain) }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        userView.hideLoadingMore()
                        userView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        userView.showMessage(
                            R.string.profile_error_get_posts,
                            DialogInterface.OnDismissListener { _ ->
                                userView.hideLoadingMore()
                            }
                        )
                    }
                )
        )
    }

    fun onEditSelected(postViewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getSegmentation(postViewModel.pk)
                .doOnSubscribe { userView.showLoading() }
                .doOnTerminate { userView.hideLoading() }
                .subscribe(
                    {
                        postViewModel.audience = AudienceViewModel(postViewModel.audience.type, it)
                        when (postViewModel) {
                            is PostImageViewModel, is PostVideoViewModel -> userView.showEditMedia(postViewModel)
                            is PostLiveViewModel -> userView.showEditLive(postViewModel)
                        }
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        userView.showMessage(
                            R.string.social_error_failed_to_edit_post,
                            DialogInterface.OnDismissListener {
                                userView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun onDeleteClicked(viewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.deleteFeed(viewModel.pk)
                .doOnSubscribe { userView.showLoading() }
                .doOnTerminate {
                    userView.hideLoading()
                    userView.hideDeleteDialog()
                }.subscribe(
                    {
                        userView.removePost(viewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        userView.showMessage(
                            R.string.social_error_failed_to_delete_post,
                            DialogInterface.OnDismissListener {
                                userView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun likePost(post: PostViewModel) {
        compositeDisposable.add(
            socialRepository.likePost(post.pk).subscribe(
                {
                    post.liked = it.liked
                    post.likeAvatars = it.likedUsersAvatars
                    post.likeCount = it.likesCount
                    userView.updatePost(post)
                },
                {
                    it.printStackTrace()
                    post.liked = !post.liked
                    userView.updatePost(post)
                },
                {}
            )
        )
    }

    fun onDestroy() = compositeDisposable.dispose()
}
