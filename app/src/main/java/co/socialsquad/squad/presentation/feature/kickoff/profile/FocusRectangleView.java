package co.socialsquad.squad.presentation.feature.kickoff.profile;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class FocusRectangleView extends View {
    private Paint mPaint;
    private Paint mStrokePaint;
    private Path mPath = new Path();

    public FocusRectangleView(Context context) {
        super(context);
        initPaints();
    }

    public FocusRectangleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaints();
    }

    public FocusRectangleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaints();
    }

    private void initPaints() {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);

        mStrokePaint = new Paint();
        mStrokePaint.setColor(Color.WHITE);
        mStrokePaint.setStrokeWidth(0);
        mStrokePaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPath.reset();

        float strokeWidth = 10f;

        mPaint.setStrokeWidth(strokeWidth);

        // mPath.addCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius, Path.Direction.CW);

        mPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);

        int x0 = canvas.getWidth()/2;
        int y0 = canvas.getHeight()/2;
        int dx = canvas.getHeight()/3;
        int dy = canvas.getHeight()/3;
        //draw guide box
        canvas.drawRect(x0-dx, y0-dy, x0+dx, y0+dy, mStrokePaint);

        canvas.drawPath(mPath, mPaint);
    }
}