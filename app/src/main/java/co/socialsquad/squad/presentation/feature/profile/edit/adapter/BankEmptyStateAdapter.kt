package co.socialsquad.squad.presentation.feature.profile.edit.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

private const val ITEM_LOADING_COUNT = 1

class BankEmptyStateAdapter(val term:String) : RecyclerView.Adapter<BankEmptyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankEmptyViewHolder {
        return BankEmptyViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BankEmptyViewHolder, position: Int) = holder.bind(term)

    override fun getItemCount(): Int = ITEM_LOADING_COUNT
}
