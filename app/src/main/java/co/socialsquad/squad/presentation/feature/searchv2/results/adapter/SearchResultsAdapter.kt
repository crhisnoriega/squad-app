package co.socialsquad.squad.presentation.feature.searchv2.results.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreObject
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Link
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.PinLocation
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.searchv2.SearchListener
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder.SearchResultsViewHolder

class SearchResultsAdapter(
        itemsList: List<Section>,
        hasHeaderAndFooter: Boolean,
        listener: SearchListener?,
        color: String
) : RecyclerView.Adapter<SearchResultsViewHolder>() {

    private val exploreObjects = mutableListOf<ExploreObject>()
    private val primaryColor: String = color
    private var searchListener: SearchListener? = null

    init {
        this.searchListener = listener
        if (hasHeaderAndFooter) {

            itemsList.forEach { section ->
                exploreObjects.add(
                        createExploreObject(
                                template = SectionTemplate.Header,
                                sectionTitle = section.title
                        )
                )
                exploreObjects[0].first = true

                section.items?.forEach { exploreObj ->
                    exploreObj.template = section.template
                    exploreObjects.add(exploreObj)
                }

                exploreObjects[exploreObjects.size - 1].last = true

                if (section.items?.size == 3) {
                    exploreObjects.add(
                            createExploreObject(
                                    template = SectionTemplate.Footer,
                                    sectionId = section.id
                            )
                    )
                }
            }

            exploreObjects.add(createExploreObject(template = SectionTemplate.EmptySpaceEnd))
        } else {
            exploreObjects.add(createExploreObject(template = SectionTemplate.EmptySpace))
            exploreObjects[0].first = true
            itemsList.forEach { section ->
                section.items?.forEach { exploreObj ->
                    exploreObj.template = section.template
                    exploreObjects.add(exploreObj)
                }
                exploreObjects[exploreObjects.size - 1].last = true
            }
            exploreObjects.add(createExploreObject(template = SectionTemplate.EmptySpaceEnd))
        }
    }

    override fun getItemViewType(position: Int): Int {
        var layoutResource = R.layout.view_searchv2_retangular_horizontal
        if (!exploreObjects.isNullOrEmpty()) {
            exploreObjects[position].template.let {
                layoutResource = checkItemLayout(it)
            }
        }
        return layoutResource
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultsViewHolder {
        return SearchResultsViewHolder(
                LayoutInflater.from(parent.context).inflate(viewType, parent, false),
                parent.context
        )
    }

    override fun onBindViewHolder(holder: SearchResultsViewHolder, position: Int) {
        holder.bind(exploreObjects[position], searchListener, primaryColor)
    }

    override fun getItemCount(): Int = exploreObjects.size

    private fun checkItemLayout(template: SectionTemplate?): Int {
        return when (template) {
            SectionTemplate.RectangleVertical -> {
                R.layout.view_searchv2_retangular_vertical
            }
            SectionTemplate.Square -> {
                R.layout.view_searchv2_square
            }
            SectionTemplate.Circle -> {
                R.layout.view_searchv2_circular
            }
            SectionTemplate.Header -> {
                R.layout.view_searchv2_header
            }
            SectionTemplate.Footer -> {
                R.layout.view_searchv2_footer
            }
            SectionTemplate.EmptySpace -> {
                R.layout.view_searchv2_empty_space
            }
            SectionTemplate.EmptySpaceEnd -> {
                R.layout.view_searchv2_empty_space_end
            }
            else -> {
                R.layout.view_searchv2_retangular_horizontal
            }
        }
    }

    private fun createExploreObject(
            template: SectionTemplate,
            sectionTitle: String? = "",
            sectionId: Int = 909192
    ): ExploreObject {
        return ExploreObject(
                id = sectionId,
                shortTitle = sectionTitle.orEmpty(),
                shortDescription = "",
                snippet = "",
                customFields = "",
                media = SimpleMedia(""),
                link = Link(0, LinkType.Invalid, ""),
                location = PinLocation(0.0, 0.0, ""),
                template = template,
                last = false,
                first = false
        )
    }
}
