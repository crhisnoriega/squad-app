package co.socialsquad.squad.presentation.feature.immobiles.details

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Immobile
import co.socialsquad.squad.presentation.util.argument
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_immobiles_images.*

class ImmobilesImagesFragment : Fragment() {

    private val media: Immobile.Media by argument(ARGUMENT_MEDIA)

    companion object {

        private const val ARGUMENT_MEDIA = "media"

        @JvmStatic
        fun newInstance(media: Immobile.Media): ImmobilesImagesFragment {
            return ImmobilesImagesFragment().apply {
                val args = Bundle().apply {
                    putParcelable(ARGUMENT_MEDIA, media)
                }
                this.arguments = args
            }
        }
    }

    private var videoUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_immobiles_images, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (media.mime_type == "video/mp4" && media.streamings.isNotEmpty()) {
            videoView.apply {
                visibility = View.VISIBLE

                loadThumbnail(media.streamings[0].thumbnailUrl)

                videoUri = Uri.parse(media.url)
                prepare(videoUri!!)
            }
        } else {
            Glide.with(requireActivity())
                .load(media.url)
                .into(image)
        }
    }

    override fun onResume() {
        super.onResume()
        if (media.mime_type == "video/mp4") {
            videoUri?.let { videoView.prepare(it) }
        }
    }

    override fun onPause() {
        if (media.mime_type == "video/mp4") {
            videoUri?.let { videoView.pause() }
        }
        super.onPause()
    }
}
