package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.LiveDataViewModel
import co.socialsquad.squad.presentation.custom.PagedViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel

class GenericLoadingViewHolder<T : ViewModel>(itemView: View) : ViewHolder<T>(itemView) {
    override fun bind(viewModel: T) {}
    override fun recycle() {}
}

class LoadingViewHolder(itemView: View) : ViewHolder<LoadingViewModel>(itemView) {

    private val searchContent = itemView.resources.getString(R.string.search_query_loading)
    private val search = itemView.findViewById<TextView?>(R.id.lbl_searching_text)

    override fun bind(viewModel: LoadingViewModel) {
        search?.text = "$searchContent\n\"${viewModel.search}\""
    }

    override fun recycle() {}
}

class PagedLoadingViewHolder<T : LiveDataViewModel>(itemView: View) : PagedViewHolder<T>(itemView) {
    override fun bind(viewModel: T) {}
    override fun recycle() {}
}
