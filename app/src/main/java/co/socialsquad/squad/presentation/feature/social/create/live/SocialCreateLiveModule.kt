package co.socialsquad.squad.presentation.feature.social.create.live

import dagger.Binds
import dagger.Module

@Module
abstract class SocialCreateLiveModule {
    @Binds
    abstract fun socialCreateLiveView(socialCreateLiveActivity: SocialCreateLiveActivity): SocialCreateLiveView
}
