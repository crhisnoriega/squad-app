package co.socialsquad.squad.presentation.feature.explorev2.domain

import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2SectionItems
import kotlinx.coroutines.flow.Flow

interface ExploreRepository {
    fun getSections(): Flow<SectionList>
    fun search(searchTerm: String): Flow<ExploreSearchV2>
    fun searchBySection(sectionId: Int, searchTerm: String): Flow<ExploreSearchV2SectionItems>
    fun getExplore(path: String): Flow<Explore>

    fun getObject(path: String): Flow<ObjectItem>

    fun putRichTextAction(path: String): Flow<String>

    fun objectSchedule(objectId: String): Flow<Submissions>

    fun generatePublicLink(uniqueLinkRequest: UniqueLinkRequest): Flow<UniqueLinkResponse>

}
