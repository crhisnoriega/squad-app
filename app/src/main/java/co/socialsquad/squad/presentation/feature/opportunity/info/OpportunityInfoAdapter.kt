package co.socialsquad.squad.presentation.feature.opportunity.info

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.InformationStage
import co.socialsquad.squad.data.entity.opportunity.InformationType
import co.socialsquad.squad.presentation.util.getColorCompat
import co.socialsquad.squad.presentation.util.isGone
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.opportunity_stage_info_item.view.*

class OpportunityInfoAdapter(
    val context: Context,
    private val stages: List<InformationStage>,
    private val type: InformationType,
    private val circleColor: String? = null,
    private val showArrow: Boolean = true,
    private val onClick: (InformationStage) -> Unit
) :
    RecyclerView.Adapter<OpportunityInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.opportunity_stage_info_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = stages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = with(holder.containerView) {

        val stage = stages[position]
        this.setOnClickListener { onClick(stage) }
        vLineTop.isVisible = position != 0 && type != InformationType.BULLET_POINTS
        vLineBottom.isVisible = position != itemCount - 1 && type != InformationType.BULLET_POINTS
        vSeparator.isInvisible = position == itemCount - 1

        tvTitle.text = stage.title
        tvDescription.text = stage.description

        tvStageIndex.text = stage.order.toString()

        stage.comission?.let { commission ->
            tvCommission.setTextColor(Color.parseColor(commission.color))
            tvCommission.text = commission.text
        } ?: run {
            tvCommission.isGone = true
        }

        val circleBackground = context.getDrawable(R.drawable.ic_circle)?.mutate()?.apply {
            val color =
                circleColor?.let { Color.parseColor(it) } ?: context.getColorCompat(R.color.grass)
            (this as GradientDrawable).setColor(color)
        }

        ivIconStatus.setImageDrawable(circleBackground)

        ibArrowAccess.isVisible = showArrow
    }

    class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer
}
