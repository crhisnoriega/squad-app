package co.socialsquad.squad.presentation.feature.event.detail

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.provider.CalendarContract
import android.text.format.DateUtils
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.CustomDialog
import co.socialsquad.squad.presentation.custom.CustomDialogButtonConfig
import co.socialsquad.squad.presentation.custom.CustomDialogConfig
import co.socialsquad.squad.presentation.custom.CustomDialogListener
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialShareViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.createLocationRequest
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.intentMaps
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.shareItem
import co.socialsquad.squad.presentation.util.trimIfSingleWord
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import io.branch.indexing.BranchUniversalObject
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.view_social_share.*
import javax.inject.Inject

class EventDetailActivity : BaseDaggerActivity(), EventDetailView {

    companion object {
        const val EXTRA_EVENT_PK = "EXTRA_EVENT_PK"
        const val EXTRA_EVENT_VIEW_MODEL = "EXTRA_EVENT_VIEW_MODEL"
    }

    @Inject
    lateinit var presenter: EventDetailPresenter
    private var sheetBehavior: BottomSheetBehavior<*>? = null

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter.onRequestPermissionsResult(grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
        setupToolbar()
        setupConfirmButton()
        setupBottomSheetActions()
        intent?.let { presenter.onCreate(it) }
    }

    override fun showCalendarApp(eventViewModel: EventViewModel) {
        with(eventViewModel) {
            val intent = Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startDate?.time)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate?.time)
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.DESCRIPTION, description)
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
            startActivity(intent)
        }
    }

    override fun setButtonEnabled(enable: Boolean) {
        confirmButton.isActive = enable
    }

    override fun setButtonConfirmText(text: Int) {
        confirmButton.text = getString(text)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupBottomSheetActions() {
        sheetBehavior = BottomSheetBehavior.from(buttonActionsContainer)
        svContent.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_MOVE -> showActions(false)
                MotionEvent.ACTION_UP -> showActions(true)
            }
            false
        }
    }

    override fun showSuccessDialog(eventViewModel: EventViewModel) {
        val dialog = CustomDialog.newInstance(
            CustomDialogConfig(
                iconRes = R.drawable.ic_recommendation_success,
                titleRes = R.string.recommendation_success_title,
                descriptionRes = R.string.event_detail_dialog_description,
                dismissOnClickOutside = false,
                buttonOneConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.event_detail_add_to_calendar,
                    buttonTextColor = R.color.orange_yellow
                ),
                buttonTwoConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.social_post_share,
                    buttonTextColor = R.color.orange_yellow
                ),
                buttonThreeConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.event_detail_dialog_close,
                    buttonTextColor = R.color.oldlavender
                )
            )
        ).apply {
            this.listener = CustomDialogListener(
                buttonOneClick = {
                    dismiss()
                    presenter.openCalendarClick()
                },
                buttonTwoClick = {
                    dismiss()
                    BranchUniversalObject().shareItem(
                        eventViewModel.shareViewModel,
                        eventViewModel.shareViewModel.model,
                        this@EventDetailActivity,
                        { showLoading() },
                        { hideLoading() }
                    )
                    presenter.onShareClicked()
                },
                buttonThreeClick = { dismiss() }
            )
        }

        if (!isFinishing) {
            dialog.show(supportFragmentManager, "success_dialog")
        }
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.event_detail_title)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun showActions(show: Boolean) {
        Handler().post {
            sheetBehavior?.state = if (show) BottomSheetBehavior.STATE_EXPANDED else BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun showEvent(eventViewModel: EventViewModel) {
        svContent.visibility = View.VISIBLE

        with(eventViewModel) {
            Glide.with(this@EventDetailActivity)
                .load(cover)
                .roundedCorners(this@EventDetailActivity)
                .crossFade()
                .into(ivCover)

            if (price != null) {
                tvPrice.text = title
            } else {
                tvPrice.visibility = View.GONE
                singlePrice.visibility = View.GONE
            }

            if (description != null) tvDescription.text = description
            else tvDescription.visibility = View.GONE

            tvCreator.text = creator?.trimIfSingleWord()
            tvCreator.setOnClickListener { creatorPk?.let { openUser(it) } }
            confirmButton.setOnButtonClickListener { presenter.onConfirmAttendanceClicked() }

            if (audienceType != null) audienceType.let {
                ivAudience.setImageResource(AudienceViewModel.getDrawableResId(it))
                tvAudience.text = getString(AudienceViewModel.getStringResId(it))
            } else {
                ivAudience.visibility = View.GONE
                tvAudience.visibility = View.GONE
            }

            if (startDate != null) {
                var dateString = ""
                var timeString = ""
                startDate.let {
                    dateString += DateUtils.formatDateTime(this@EventDetailActivity, it.time, DateUtils.FORMAT_NO_YEAR)
                    timeString += DateUtils.formatDateTime(this@EventDetailActivity, it.time, DateUtils.FORMAT_SHOW_TIME)
                }
                endDate?.let {
                    dateString += " - " + DateUtils.formatDateTime(this@EventDetailActivity, it.time, DateUtils.FORMAT_NO_YEAR)
                    timeString += " - " + DateUtils.formatDateTime(this@EventDetailActivity, it.time, DateUtils.FORMAT_SHOW_TIME)
                }
                tvDate.text = dateString
                tvTime.text = timeString
                bAddToCalendar.setOnClickListener { showCalendarApp(this) }
            } else {
                clDateContainer.visibility = View.GONE
            }

            if (address != null || location != null) {
                if (location != null) tvLocation.text = location
                else tvLocation.visibility = View.GONE

                if (address != null) tvAddress.text = address
                else tvAddress.visibility = View.GONE

                if (latitude != null && longitude != null) {
                    bOpenMap.setOnClickListener { intentMaps(latitude, longitude, location) }
                } else {
                    bOpenMap.visibility = View.GONE
                }
            } else {
                clLocationContainer.visibility = View.GONE
            }

            listOfNotNull(
                speakers.takeIf { !it.isNullOrEmpty() } to llSpeakersContent,
                vip.takeIf { !it.isNullOrEmpty() } to llVipContent
            ).forEach {
                val list = it.first ?: return@forEach
                val container = it.second
                val maxVisibleItems = 3
                val size = if (list.size > maxVisibleItems) maxVisibleItems else list.size

                list.subList(0, size).forEach { user ->
                    layoutInflater.inflate(R.layout.view_event_detail_user, container, false).apply {
                        findViewById<TextView>(R.id.short_title_lead).text = user.name
                        findViewById<TextView>(R.id.tvQualification).text = user.qualification
                        Glide.with(this@EventDetailActivity)
                            .load(user.avatarUrl)
                            .circleCrop()
                            .into(findViewById(R.id.ivAvatar))
                        setOnClickListener { openUser(user.pk) }
                        container.addView(this)
                    }
                }

                if (list.size > maxVisibleItems) {
                    layoutInflater.inflate(R.layout.view_event_detail_see_all, container, false).apply {
                        val type = when (container) {
                            llSpeakersContent -> UserListPresenter.Type.SPEAKERS
                            llVipContent -> UserListPresenter.Type.VIP
                            else -> return@forEach
                        }
                        setOnClickListener { openUserList(list, type) }
                        container.addView(this)
                    }
                }

                (container.parent as View).visibility = View.VISIBLE
            }
        }

        with(eventViewModel) {
            SocialShareViewHolder(llShareContainer, this, onShareListener).bind(shareViewModel)
        }
    }

    override fun updateAttendance(eventViewModel: EventViewModel, currentUserPk: Int) {
        when {
            eventViewModel.creatorPk == currentUserPk -> setupAttendButtonForCreator(eventViewModel)
            eventViewModel.isAttending -> setupCancelAttendanceButton(eventViewModel)
            else -> setupAttendButton(eventViewModel)
        }

        urslAvatars.setUserImages(eventViewModel.attendeesPictures)
        urslAvatars.setOnClickListener {
            if (eventViewModel.numberOfAttendants > 0) {
                val intent = Intent(this, UserListActivity::class.java).apply {
                    putExtra(UserListActivity.QUERY_ITEM_PK, eventViewModel.pk)
                    putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.EVENTS_CONFIRMED)
                }
                startActivity(intent)
            }
        }
    }

    private fun setupAttendButton(eventViewModel: EventViewModel) {
        urslAvatars.setText(getAttendingText(eventViewModel.numberOfAttendants))
    }

    private fun setupCancelAttendanceButton(eventViewModel: EventViewModel) {
        urslAvatars.setText(getAttendingText(eventViewModel.numberOfAttendants))
    }

    private fun setupAttendButtonForCreator(eventViewModel: EventViewModel) {
        if (eventViewModel.numberOfAttendants < 1) {
            urslAvatars.setText(getString(R.string.event_detail_attenders_none_self))
        } else {
            urslAvatars.setText(getAttendingText(eventViewModel.numberOfAttendants))
        }
    }

    private fun getAttendingText(numberOfAttendants: Int) = when {
        numberOfAttendants <= 0 -> getString(R.string.event_detail_attenders_none)
        numberOfAttendants == 1 -> getString(R.string.event_detail_attenders_single, numberOfAttendants)
        else -> getString(R.string.event_detail_attenders_multiple, numberOfAttendants)
    }

    private val onShareListener = object : ViewHolder.Listener<EventViewModel> {
        override fun onClick(viewModel: EventViewModel) {
            presenter.onShareClicked()
        }
    }

    override fun setupDistance(latitude: Double, longitude: Double, eventViewModel: EventViewModel) {
        val distance = TextUtils.calculateDistance(latitude, longitude, eventViewModel.latitude!!, eventViewModel.longitude!!)
        tvAddress.text = getString(R.string.event_detail_distance, tvAddress.text, distance)
    }

    private fun openUserList(list: List<UserViewModel>, type: UserListPresenter.Type) {
        val intent = Intent(this@EventDetailActivity, UserListActivity::class.java)
            .putExtra(UserListActivity.USER_LIST_TYPE, type)
            .putExtra(UserListActivity.EXTRA_USER_LIST, list.toTypedArray())
        startActivity(intent)
    }

    override fun close() {
        finish()
    }

    override fun showMessage(textResId: Int, onDismissListener: (() -> Unit)) {
        ViewUtils.showDialog(this, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    private fun openUser(pk: Int) {
        val intent = Intent(this, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, pk)
        }
        startActivity(intent)
    }

    private fun setupConfirmButton() {
        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_recommendation_success)
        (drawable as? BitmapDrawable)?.let {
            confirmButton.setDrawable(drawable)
        }
    }

    override fun checkPermissions(permissions: Array<String>) =
        permissions.all { ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED }

    override fun requestPermissions(requestCode: Int, permissions: Array<String>) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }

    override fun createLocationRequest() {
        createLocationRequest { request, intent ->
            presenter.onLocationRequestCreated(request, intent)
        }
    }

    override fun showLoading() {
        confirmButton.loading(true)
        vLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        confirmButton.loading(false)
        vLoading.visibility = View.GONE
    }
}
