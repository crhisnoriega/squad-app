package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem
import kotlinx.android.parcel.Parcelize

@Parcelize
class RichTextObjects(
    var title: String? = null,
    var content: List<ListItem>? = null,
    var template: ObjectTemplateDataType? = null
) : ViewType, Parcelable {
    constructor(richText: RichTextVO) : this() {
        this.title = richText.objectType?.title
        this.content = richText.objectType?.content
        this.template = richText.objectType?.template
    }

    override fun getViewType(): Int {
        return RichTextDataType.OBJECT.value
    }
}
