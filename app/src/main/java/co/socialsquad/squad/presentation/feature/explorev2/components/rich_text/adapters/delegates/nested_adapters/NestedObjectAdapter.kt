package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates.nested_adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.RichTextListener
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.ObjectTemplateDataType
import co.socialsquad.squad.presentation.util.inflate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_rich_text_nested_object.view.*

class NestedObjectAdapter(
    private val listener: RichTextListener,
    private val list: List<ListItem>?,
    private val template: ObjectTemplateDataType?
) :
    RecyclerView.Adapter<NestedObjectAdapter.NestedLinkViewHolder>() {

    inner class NestedLinkViewHolder(private val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_rich_text_nested_object)) {

        private val tvLinkTitle: TextView = itemView.tv_item_rich_text_nested_title
        private val tvLinkDescription: TextView = itemView.tv_item_rich_text_nested_description
        private val tvSnippet: TextView = itemView.tv_item_rich_text_nested_snippet
        val container: ViewGroup = itemView.cl_item_rich_text_nested_object_container

        fun bind(richTextLinkDetails: ListItem) {
            if (richTextLinkDetails.shortTitle.isNullOrBlank()) {
                tvLinkTitle.visibility = View.GONE
            } else {
                tvLinkTitle.visibility = View.VISIBLE
                tvLinkTitle.text = richTextLinkDetails.shortTitle
            }
            if (richTextLinkDetails.customFields.isNullOrBlank()) {
                tvLinkDescription.visibility = View.GONE
            } else {
                tvLinkDescription.visibility = View.VISIBLE
                tvLinkDescription.text = richTextLinkDetails.customFields
            }

            if (richTextLinkDetails.snippet.isNullOrBlank()) {
                tvSnippet.visibility = View.GONE
            } else {
                tvSnippet.visibility = View.VISIBLE
                tvSnippet.text = richTextLinkDetails.snippet
            }
            setTemplate(container, template, itemView, richTextLinkDetails.media?.url)
            container.setOnClickListener {
                richTextLinkDetails.link?.url?.let {
                    listener.onRichTextObjectClicked(richTextLinkDetails.link)
                }
            }
        }

        private fun setTemplate(
            container: ViewGroup,
            template: ObjectTemplateDataType?,
            itemView: View,
            imageUrl: String?
        ) {
            template?.let {
                // hide all cardviews
                for (views in container.children) {
                    if (views is CardView) {
                        views.visibility = View.GONE
                    }
                }
                // set the desired card view visible and set the imageview id
                val imageView: ImageView = when (it) {
                    ObjectTemplateDataType.DEFAULT, ObjectTemplateDataType.RECTANGLE_HORIZONTAL -> {
                        itemView.cv_item_rich_text_nested_media_rectangular_horizontal
                            .visibility = View.VISIBLE
                        itemView.iv_item_rich_text_nested_media_rectangular_horizontal
                    }
                    ObjectTemplateDataType.RECTANGLE_VERTICAL -> {
                        itemView.cv_item_rich_text_nested_media_rectangular_vertical
                            .visibility = View.VISIBLE
                        itemView.iv_item_rich_text_nested_media_rectangular_vertical
                    }
                    ObjectTemplateDataType.SQUARE -> {
                        itemView.cv_item_rich_text_nested_media_square
                            .visibility = View.VISIBLE
                        itemView.iv_item_rich_text_nested_media_square
                    }
                    ObjectTemplateDataType.CIRCLE -> {
                        itemView.cv_item_rich_text_nested_media_circular
                            .visibility = View.VISIBLE
                        itemView.iv_item_rich_text_nested_media_circular
                    }
                }
                Glide.with(parent.context).load(imageUrl).centerInside().into(imageView)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NestedLinkViewHolder {
        return NestedLinkViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: NestedLinkViewHolder, position: Int) {
        list?.get(position)?.let { holder.bind(it) }
    }
}
