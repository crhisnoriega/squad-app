package co.socialsquad.squad.presentation.feature.immobiles.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.ImmobileDetail
import kotlin.reflect.KFunction1

class ImmobileDetailsAdapter(
    val details: List<ImmobileDetail>,
    val onDetailSelected: KFunction1<ImmobileDetail, Unit>
) :
    RecyclerView.Adapter<ImmobileDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.immobile_detail_list_item, parent, false))

    override fun getItemCount(): Int = details.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.container.setOnClickListener { onDetailSelected(details[position]) }
        holder.name.text = details[position].name
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
        val name: TextView = itemView.findViewById(R.id.name)
    }
}
