package co.socialsquad.squad.presentation.feature.ranking.viewholder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_ranking_prize.view.*


class ItemPrizeRankingViewHolder(itemView: View, var onClickAction: (prize: ItemPrizeRankingViewModel) -> Unit) : ViewHolder<ItemPrizeRankingViewModel>(itemView) {

    override fun bind(viewModel: ItemPrizeRankingViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.data.title
        itemView.txtTitle.setTextColor(ColorUtils.parse(getCompanyColor()!!))
        itemView.txtSubTitle.text = viewModel.data.title
        itemView.txtSubtitle2.text = viewModel.data.description
//        itemView.dividerItemTop.isVisible = viewModel.isFirst.not()

       // Glide.with(itemView.context).load(viewModel.data.mediaCover).into(itemView.cover)

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.data.icon), itemView.cover)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}