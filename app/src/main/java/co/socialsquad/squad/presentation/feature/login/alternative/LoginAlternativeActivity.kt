package co.socialsquad.squad.presentation.feature.login.alternative

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.login.alternative.success.LoginAlternativeSuccessActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.setGradientBackground
import kotlinx.android.synthetic.main.activity_login_alternative.*
import kotlinx.android.synthetic.main.view_login_input_text.*
import javax.inject.Inject

const val EXTRA_LOGIN_ALTERNATIVE_TYPE = "EXTRA_LOGIN_ALTERNATIVE_TYPE"

class LoginAlternativeActivity : BaseDaggerActivity(), LoginAlternativeView {

    @Inject
    lateinit var presenter: LoginAlternativePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_alternative)
        setupToolBar()
        setupTextField()
        presenter.onCreate(intent)

        iltEmailField.setOnKeyboardActionListener {
            if (TextUtils.isValidEmail(iltEmailField.getFieldText().toString())) {
                bNext.performClick()
            }
        }
    }

    private fun setupToolBar() {
        bNext.isEnabled = false
        bNext?.setOnClickListener {
            presenter.onNextClicked(iltEmailField.getFieldText().toString())
        }
        back.setOnClickListener { onBackPressed() }
    }

    private fun setupTextField() {
        iltEmailField.addTextChangedListener { text ->
            bNext?.isEnabled = TextUtils.isValidEmail(text)
        }
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.showSoftInput(tietField, InputMethodManager.SHOW_IMPLICIT)
        tietField.requestFocus()
    }

    override fun setFieldText(text: String) {
        iltEmailField.setFieldText(text)
    }

    override fun setHintText(messageStringId: Int) {
        iltEmailField.setHintInput(getString(messageStringId))
    }

    override fun setHelpText(messageStringId: Int, parameter: String?) {

        intent.extras?.apply {
            if (this.getSerializable(EXTRA_LOGIN_ALTERNATIVE_TYPE) == LoginAlternativePresenter.Type.RESET_PASSWORD) {
                tvAlternative.text = getString(R.string.login_alternative_forgott_password, parameter)
                return
            }
        }

        if (parameter.isNullOrBlank()) {
            iltEmailField.setHelpText(getString(messageStringId))
        } else {
            iltEmailField.setHelpText(getString(messageStringId, parameter))
        }
    }

    override fun setErrorMessage(messageStringId: Int) {
        iltEmailField.setErrorText(getString(messageStringId))
    }

    override fun setErrorMessage(text: String) {
        iltEmailField.setErrorText(text)
    }

    override fun showSuccessScreen(type: LoginAlternativePresenter.Type) {
        val email = iltEmailField.getFieldText().toString()
        val intent = Intent(this, LoginAlternativeSuccessActivity::class.java)
        intent.putExtra(LoginAlternativeSuccessActivity.EXTRA_EMAIL, email)
        intent.putExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE, type)
        startActivity(intent)
    }

    override fun startLoading() {
        iltEmailField.loading(true)
        bNext?.isEnabled = false
    }

    override fun stopLoading() {
        iltEmailField.loading(false)
        bNext?.isEnabled = TextUtils.isValidEmail(iltEmailField.getFieldText().toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setupHeader(company: Company?) {
        company?.let {
            if (it.isPublic != null && it.isPublic!!) {
                it.aboutGradient?.apply {
                    if (this.isNotEmpty()) {
                        setColorsConfiguration(this[0], this[1], it.aboutFooterBackground!!, it.textColor!!)
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }
            }
            squadHeader.visibility = View.VISIBLE
            squadHeader.bindCompany(it)
        }
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        container.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        tvAlternative.setTextColor(color)
        back.setColorFilter(color)

        iltEmailField.setColors(textColor)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
