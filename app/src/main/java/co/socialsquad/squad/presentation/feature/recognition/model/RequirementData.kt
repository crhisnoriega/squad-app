package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RequirementData(
        @SerializedName("icon") val icon: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("show_details") val show_details: Boolean?,
        @SerializedName("completed") var completed: Boolean?,

        @SerializedName("progress") var progress: ProgressData?,

//        @SerializedName("actual_value") val actual_value: Double?,
//        @SerializedName("target_value_min") val target_value_min: Double?,
//        @SerializedName("target_value_max") val target_value_max: Double?,
//
//        @SerializedName("score_progress_subtitle") val score_progress_subtitle: String?,
//
//        @SerializedName("score_progress_value") val score_progress_value: Int?,
//
//        @SerializedName("completion_percent") val completion_percent: Double?,

        var first: Boolean = false
) : Parcelable