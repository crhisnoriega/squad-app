package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class OpportunityLead(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("email") val email: String?,
        @SerializedName("phone") val phone: String?
) : Parcelable