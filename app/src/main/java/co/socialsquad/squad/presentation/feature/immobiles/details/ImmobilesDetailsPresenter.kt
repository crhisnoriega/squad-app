package co.socialsquad.squad.presentation.feature.immobiles.details

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.repository.ImmobilesRepositoryImpl
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.OpportunityRepository
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListPresenter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ImmobilesDetailsPresenter @Inject constructor(
    private val repository: ImmobilesRepositoryImpl,
    private val loginRepository: LoginRepository,
    private val opportunityRepository: OpportunityRepository,
    private val view: ImmobilesDetailsView
) : OpportunityListPresenter {

    private val AUDIENCE_TYPE_PUBLIC = "p"
    private val AUDIENCE_TYPE_DOWNLINE = "d"
    private val AUDIENCE_TYPE_METRICS = "f"
    private val AUDIENCE_TYPE_MEMBERS = "m"

    private val compositeDisposable = CompositeDisposable()

    fun onStart() {
        view.setupShare(loginRepository.subdomain)
    }

    fun immobileData(pk: Int, opportunities: List<Opportunity>) {
        immobilesDetails(pk)
        opportunity(pk, opportunities)
    }

    override fun getOpportunityAction(opportunity: Opportunity) {

        view.showLoading(true)
        compositeDisposable.add(
            opportunityRepository.opportunityAction(opportunity.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityActionClick(opportunity, it.opportunityAction)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }

    private fun immobilesDetails(pk: Int) {
        compositeDisposable.add(
            repository.immobileDetail(pk)
                .subscribe(
                    {
                        view.setupDetails(it.toMutableList())
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    private fun opportunity(immobileId: Int, opportunities: List<Opportunity>) {

        view.showLoading(true)

        compositeDisposable.add(
            getSubmissions(immobileId, opportunities)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.showActions(true)
                        view.setupOpportunitiesSubmissions(it)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }

    private fun getSubmissions(
        immobileId: Int,
        opportunities: List<Opportunity>
    ): Single<List<Opportunity>> {
        return Observable
            .fromIterable(opportunities)
            .flatMapSingle { opportunity ->
                opportunityRepository
                    .opportunitySubmissions(
                        opportunity.id,
                        immobileId,
                        OpportunityObjectType.REAL_STATE
                    )
                    .map {
                        opportunity.copy(opportunityList = it.opportunityList).apply {
                            parentObjectId = immobileId
                        }
                    }
            }
            .toList()
    }

    fun getDrawableResId(audienceType: String) = when (audienceType) {
        AUDIENCE_TYPE_DOWNLINE -> R.drawable.ic_audience_post_downline
        AUDIENCE_TYPE_METRICS -> R.drawable.ic_audience_post_metrics
        AUDIENCE_TYPE_MEMBERS -> R.drawable.ic_audience_post_members
        else -> R.drawable.ic_audience_post_public
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    override fun getSubmissionDetails(
        opportunityId: Int,
        objectId: Int,
        submissionId: Int,
        objectType: OpportunityObjectType
    ) {

        view.showLoading(true)

        compositeDisposable.add(
            opportunityRepository.opportunitySubmissionDetails(
                opportunityId,
                objectId,
                submissionId,
                objectType
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityDetailsClick(it.submissionDetails)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }

    override fun getOpportunityInfo(opportunity: Opportunity) {
        view.showLoading(true)

        compositeDisposable.add(
            opportunityRepository.opportunityDetails(opportunity.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        view.showLoading(false)
                        view.onOpportunityInfoClick(it.info)
                    },
                    { error ->
                        view.showLoading(false)
                    }
                )
        )
    }
}
