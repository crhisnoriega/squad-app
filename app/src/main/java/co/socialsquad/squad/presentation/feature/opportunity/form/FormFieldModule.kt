package co.socialsquad.squad.presentation.feature.opportunity.form

import dagger.Binds
import dagger.Module

@Module
interface FormFieldModule {

    @Binds
    fun view(fieldFragment: FormFieldFragment): FormFieldView
}
