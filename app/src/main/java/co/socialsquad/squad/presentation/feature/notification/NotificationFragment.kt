package co.socialsquad.squad.presentation.feature.notification

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.navigation.ReselectedListener
import co.socialsquad.squad.presentation.feature.notification.mapper.NotificationLoadingMapper
import co.socialsquad.squad.presentation.feature.notification.viewHolder.*
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.util.RecyclerViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.view_toolbar.tvTitle
import javax.inject.Inject

class NotificationFragment : Fragment(), NotificationView, ReselectedListener {

    @Inject
    lateinit var notificationPresenter: NotificationPresenter

    private var notificationFragmentListener: NotificationFragmentListener? = null

    private var notificationAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isLoading: Boolean = false

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        notificationFragmentListener = activity as NotificationFragmentListener?
        super.onAttach(context)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupRecyclerView()
        setupSwipeRefresh()
        notificationPresenter.onViewCreated()
    }

    private fun setupToolbar() {
        tvTitle.text = getString(R.string.navigation_item_notification)
    }

    private fun setupSwipeRefresh() {
        srlNotifications.setOnRefreshListener {
            notificationPresenter.onRefresh()
        }
    }

    private fun startActiveLive(targetPk: Int) {
        val intent = Intent(activity, LiveViewerActivity::class.java).apply {
            putExtra(LiveViewerActivity.PK, targetPk)
        }
        startActivity(intent)
    }

    private fun startScheduledLive(targetPk: Int) {
        val intent = Intent(activity, ScheduledLiveDetailsActivity::class.java).apply {
            putExtra(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_PK, targetPk)
        }
        startActivity(intent)
    }

    private fun startPostDetail(targetPk: Int) {
        val intent = Intent(activity, PostDetailsActivity::class.java).apply {
            putExtra(PostDetailsActivity.POST_DETAIL_PK, targetPk)
        }
        startActivity(intent)
    }

    private val onNotificationClickedListener =
            object : ViewHolder.Listener<NotificationViewModel> {
                override fun onClick(viewModel: NotificationViewModel) {
                    when {
                        viewModel.type == Share.Live.name -> {
                            startActiveLive(viewModel.targetPk)
                        }
                        viewModel.type == Share.ScheduledLive.name -> {
                            startScheduledLive(viewModel.targetPk)
                        }
                        viewModel.type == Share.Feed.name -> {
                            startPostDetail(viewModel.targetPk)
                        }
                        viewModel.type == Share.Audio.name -> {
                            val intent = Intent(activity, AudioDetailActivity::class.java).apply {
                                putExtra(AudioDetailActivity.EXTRA_AUDIO_PK, viewModel.targetPk)
                            }
                            startActivity(intent)
                        }
                    }
                    viewModel.clicked = true
                    notificationPresenter.notificationClicked(viewModel.pk)
                    notificationAdapter?.notifyDataSetChanged()
                }
            }

    private fun setupRecyclerView() {
        notificationAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is NotificationViewModel -> NOTIFICATION_ITEM_VIEW_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is ItemDivider -> ITEM_DIVIDER
                is NotificationLoadingHeaderViewModel -> ITEM_HEADER_LOADING
                is NotificationLoadingContentViewModel -> ITEM_NOTIFICATION_CONTENT_LOADING
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                NOTIFICATION_ITEM_VIEW_ID -> NotificationViewHolder(
                        view,
                        onNotificationClickedListener
                )
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                ITEM_DIVIDER -> DividerViewHolder(view)
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                ITEM_HEADER_LOADING -> NotificationLoadingHeaderViewHolder(view)
                ITEM_NOTIFICATION_CONTENT_LOADING -> NotificationLoadingContentViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvNotifications?.apply {
            linearLayoutManager = PrefetchingLinearLayoutManager(context)
            adapter = notificationAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(0)
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(EndlessScrollListener())
            setHasFixedSize(true)
        }
    }

    override fun setItems(items: List<ViewModel>) {
        groupEmptyState.isVisible = items.isEmpty()
        notificationAdapter?.setItems(items)
    }

    override fun addItens(items: List<ViewModel>) {
        val allItems = (notificationAdapter?.items ?: arrayListOf()) + items
        var outerCount = allItems.count { it is OuterDivider }
        val headerCount = allItems.count { it is HeaderViewModel }
        while (outerCount > headerCount) {
            notificationAdapter?.removeLast(OuterDivider(-1))
            outerCount--
        }
        notificationAdapter?.addItems(items)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        notificationAdapter?.apply {
//            if (!isVisibleToUser) {
//                rvNotifications?.stopScroll()
//                view?.visibility = View.INVISIBLE
//            } else {
//                view?.visibility = View.VISIBLE
//            }
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.statusBarColor = Color.WHITE
        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    override fun onReselected() {
        linearLayoutManager?.let {
            activity?.let { activity ->
                RecyclerViewUtils.scrollToTop(
                        activity,
                        it
                )
            }
        }
    }

    private inner class EndlessScrollListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 8
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            linearLayoutManager?.apply {
                if (!isLoading && findLastVisibleItemPosition() > itemCount - visibleThreshold) {
                    notificationPresenter.onScrolledBeyondVisibleThreshold()
                }
            }
        }
    }

    fun refresh() {
        notificationPresenter.onRefresh()
    }

    override fun showRefreshing() {
        srlNotifications.isRefreshing = true
    }

    override fun hideRefreshing() {
        srlNotifications.isRefreshing = false
    }

    override fun showLoadingMore() {
        if (notificationAdapter?.itemCount == 0) {
            setItems(NotificationLoadingMapper.mapLoadingContent())
            // disabling swipe to refresh on loading state
            srlNotifications.isEnabled = false
        } else {
            notificationAdapter?.startLoading()
        }
    }

    override fun hideLoadingMore() {
        notificationAdapter?.stopLoading()
        if (srlNotifications != null) {
            srlNotifications.isEnabled = true
        }
    }

    override fun updateNotificationBadge() {
        // this method should clear the badge, but the badge is in parent activity, not here
        // so we clear it through an interface implemented in parent activity
        notificationFragmentListener?.onUnreadNotificationCleared()
    }

    interface NotificationFragmentListener {
        fun onUnreadNotificationCleared()
    }
}
