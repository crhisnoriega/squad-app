package co.socialsquad.squad.presentation.feature.about

import co.socialsquad.squad.data.entity.AboutResponse

interface AboutView {
    fun showLoading()
    fun hideLoading()
    fun showAbout(about: AboutResponse)
    fun showTryAgain()
    fun hideTryAgain()
}
