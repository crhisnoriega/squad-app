package co.socialsquad.squad.presentation.feature.opportunity.detail

import android.content.Context
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.getColorCompat
import co.socialsquad.squad.presentation.util.getScreenWidth
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_opportunity_submission_detail.*

class OpportunitySubmissionDetailActivity : BaseDaggerActivity() {

    private val submissionDetails: SubmissionDetails by extra(EXTRA_SUBMISSION_DETAIL)
    private val submissionColors: OpportunityInitialsColors by extra(EXTRA_COLORS)
    private val title: String by extra(EXTRA_TITLE)

    private val fragments =
        listOf(
            SubmissionDetailProgressFragment(),
            SubmissionDetailDetailsFragment()
        ).toTypedArray()

    private val tabsTitles =
        listOf(
            R.string.recommendations_details_progress_tab,
            R.string.recommendations_details_detail_tab
        )

    private val mapper by lazy { OpportunitySubmissionDetailsMapper(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opportunity_submission_detail)
        ibBack.setOnClickListener { finish() }
        setupTabs()
        setupHeader()
    }

    private fun setupHeader() {
        tvTitle.text = submissionDetails.title
        tvNameIcon.text = submissionDetails.primaryInitials
        short_title_lead.text = submissionDetails.primaryField
        tvRecommendationStatus.text = mapper.mapStatusText(submissionDetails)
        tvRecommendationStatus.setTextColor(mapper.mapStatusColor(submissionDetails))

        ivNameIcon.apply {
            val drawable = this.drawable as LayerDrawable
            val gradientDrawable = drawable
                .findDrawableByLayerId(R.id.gradient)
                .mutate() as GradientDrawable
            val defaultColor = context.getColorCompat(R.color.darkjunglegreen_28)
            val startColor = submissionColors.primaryColor?.let { ColorUtils.parse(it) } ?: defaultColor
            val endColor = submissionColors.secondaryColor?.let { ColorUtils.parse(it) } ?: startColor

            gradientDrawable.colors = intArrayOf(startColor, endColor)
        }
    }

    private fun setupTabs() {
        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position

                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(ContextCompat.getColor(context, R.color.darkjunglegreen_28))
                            TextUtils.applyTypeface(context, "fonts/gotham_bold.ttf", text.toString(), text.toString())
                        } else {
                            setTextColor(ContextCompat.getColor(context, R.color.oldlavender))
                            TextUtils.applyTypeface(context, "fonts/gotham_medium.ttf", text.toString(), text.toString())
                        }
                    }
                }
            }
        })

        vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                tlTabs.getTabAt(position)?.select()
            }
        })

        tabsTitles.forEach {
            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_explore_tab, tlTabs, false).apply {
                    findViewById<ImageView>(R.id.ivIcon)?.visibility = View.GONE
                    findViewById<TextView>(R.id.tvTitle)?.apply {
                        text = getString(it)
                    }
                }
                view.measure(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                tlTabs.layoutParams.height = view.measuredHeight
                customView = view
            }
            tlTabs.addTab(tab)
        }

        tlTabs.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if (tlTabs.measuredWidth < getScreenWidth()) {
            tlTabs.setPadding(0, 0, 0, 0)
        }

        vpContent.adapter = ExplorePagerAdapter(supportFragmentManager, fragments)
        vpContent.offscreenPageLimit = fragments.size
    }

    companion object {
        const val EXTRA_SUBMISSION_DETAIL = "submission_detail"
        const val EXTRA_COLORS = "submission_colors"
        const val EXTRA_TITLE = "title"
        fun newIntent(
            context: Context,
            submissionDetails: SubmissionDetails,
            colors: OpportunityInitialsColors?,
            title: String?
        ): Intent {
            return Intent(context, OpportunitySubmissionDetailActivity::class.java).apply {
                putExtra(EXTRA_SUBMISSION_DETAIL, submissionDetails)
                putExtra(EXTRA_COLORS, colors)
                putExtra(EXTRA_TITLE, title)
            }
        }
    }
}
