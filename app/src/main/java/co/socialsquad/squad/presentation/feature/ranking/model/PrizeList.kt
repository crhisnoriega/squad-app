package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PrizeList(
        @SerializedName("data") val list: List<PrizeData>?,
        @SerializedName("show_all") val show_all: Boolean?,
        @SerializedName("title") val title: String?,
) : Parcelable