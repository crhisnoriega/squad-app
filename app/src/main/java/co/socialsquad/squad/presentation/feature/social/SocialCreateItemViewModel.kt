package co.socialsquad.squad.presentation.feature.social

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel

const val SOCIAL_CREATE_ITEM_VIEW_MODEL_ID = R.layout.view_social_create_item

sealed class SocialCreateItemViewModel(val textResId: Int, val errorTextResId: Int, val iconResId: Int, open val enabled: Boolean) : ViewModel {
    class Photo(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_photo, R.string.social_create_dialog_unavailable, R.drawable.ic_new_action_photo, enabled)
    class Video(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_video, R.string.social_create_dialog_unavailable, R.drawable.ic_new_action_video, enabled)
    class Gallery(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_gallery, R.string.social_create_dialog_unavailable, R.drawable.ic_new_action_gallery, enabled)
    class Live(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_live, R.string.social_create_dialog_live_unavailable, R.drawable.ic_new_action_live, enabled)
//    class AudioFile(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_audio_gallery, R.string.social_create_dialog_unavailable, R.drawable.ic_new_action_upload_audio, enabled)
//    class AudioRecord(override val enabled: Boolean) : SocialCreateItemViewModel(R.string.social_create_dialog_button_audio_record, R.string.social_create_dialog_unavailable, R.drawable.ic_new_action_record_audio, enabled)
}
