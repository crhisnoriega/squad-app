package co.socialsquad.squad.presentation.feature.profile.pages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.viewHolder.PROFILE_GRID_POST_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileGridPostViewHolder
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileGridPostViewModel
import co.socialsquad.squad.presentation.feature.social.*
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.util.argument
import kotlinx.android.synthetic.main.fragment_profile_publish_grid.*


const val ARG_PROFILE_POST_GRID_TYPE = "ARG_PROFILE_POST_GRID_TYPE"

class ProfilePublishGridFragment : Fragment() {

    companion object {
        fun newInstance(postGridType: String): ProfilePublishGridFragment {
            var fragment = ProfilePublishGridFragment()
            fragment.arguments = Bundle().apply {
                putString(ARG_PROFILE_POST_GRID_TYPE, postGridType)
            }
            return fragment
        }
    }

    private val postGridType: String by argument(ARG_PROFILE_POST_GRID_TYPE)
    lateinit var posts: List<SocialViewModel>
    private lateinit var viewModel: ProfilePublishViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_publish_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRecyclerView()
    }

    fun configureRecyclerView() {
        var adapter1 = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is PostVideoViewModel -> SOCIAL_POST_VIDEO_VIEW_MODEL_ID
                is PostImageViewModel -> SOCIAL_POST_IMAGE_VIEW_MODEL_ID
                is ProfileGridPostViewModel -> PROFILE_GRID_POST_VIEW_HOLDER_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    PROFILE_GRID_POST_VIEW_HOLDER_ID -> ProfileGridPostViewHolder(view)

                    SOCIAL_POST_VIDEO_VIEW_MODEL_ID -> PostVideoViewHolder(
                            view,
                            onLikeCountListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {
                                }
                            },
                            onCommentCountListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {

                                }
                            },
                            onCommentListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {

                                }
                            }
                    )
                    SOCIAL_POST_IMAGE_VIEW_MODEL_ID -> PostImageViewHolder(
                            view,
                            onLikeCountListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {
                                }
                            },
                            onCommentCountListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {

                                }
                            },
                            onCommentListener = object : ViewHolder.Listener<PostViewModel> {
                                override fun onClick(viewModel: PostViewModel) {

                                }
                            })
                    else -> throw IllegalArgumentException()
                }
            }
        })

        when (postGridType) {
            "grid" -> {
                rvPosts.apply {
                    layoutManager = GridLayoutManager(requireContext(), 3)
                    adapter = adapter1
                    adapter1.setItems(posts.map { post ->
                        var post1 = post as? co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
                        if (post1 != null && post1.imageUrl != null) {
                            ProfileGridPostViewModel(post1?.imageUrl!!)
                        } else {
                            ProfileGridPostViewModel(null)
                        }
                    }.toList())
                }
                nestedScroll.setBackgroundColor(resources.getColor(R.color.white))
            }

            "list" -> {
                rvPosts.apply {
                    layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                    adapter = adapter1
                    adapter1.setItems(posts)
                }
                nestedScroll.setBackgroundColor(resources.getColor(R.color._eeeff4))
            }
        }

    }

}