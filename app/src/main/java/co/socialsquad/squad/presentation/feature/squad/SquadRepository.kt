package co.socialsquad.squad.presentation.feature.squad

import co.socialsquad.squad.data.entity.Feed
import io.reactivex.Observable

interface SquadRepository {
    fun getSquadFeed(page: Int): Observable<Feed>
}
