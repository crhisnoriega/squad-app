package co.socialsquad.squad.presentation.feature.hub.dashboard

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.hub.dashboard.charts.CHART_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.hub.dashboard.charts.ChartViewHolder
import co.socialsquad.squad.presentation.feature.hub.dashboard.charts.ChartViewModel
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject

class DashboardFragment : Fragment(), DashboardView {

    @Inject
    lateinit var dashboardPresenter: DashboardPresenter

    private var chartsAdapter: RecyclerViewAdapter? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
        setupSwipeRefresh()
        dashboardPresenter.onViewCreated()
    }

    private fun setupSwipeRefresh() {
        srlDashboard.setOnRefreshListener {
            srlDashboard.isRefreshing = false
            dashboardPresenter.onRefresh()
        }
    }

    private fun changeToLastSeven(viewModel: ChartViewModel) {
        viewModel.lastSeven = true
        chartsAdapter?.update(viewModel)
    }

    private fun changeToLastThirty(viewModel: ChartViewModel) {
        viewModel.lastSeven = false
        chartsAdapter?.update(viewModel)
    }

    private val onLastSevenListener = object : ViewHolder.Listener<ChartViewModel> {
        override fun onClick(viewModel: ChartViewModel) {
            changeToLastSeven(viewModel)
        }
    }

    private val onLastThirtyListener = object : ViewHolder.Listener<ChartViewModel> {
        override fun onClick(viewModel: ChartViewModel) {
            changeToLastThirty(viewModel)
        }
    }

    private fun setupList() {
        chartsAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ChartViewModel -> CHART_VIEW_MODEL_ID
                is LoadingViewModel -> R.layout.view_list_loading
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    CHART_VIEW_MODEL_ID -> ChartViewHolder(view, onLastSevenListener, onLastThirtyListener)
                    R.layout.view_list_loading -> GenericLoadingViewHolder<Nothing>(view)
                    else -> throw IllegalArgumentException()
                }
            }
        })
        rvDashboard?.apply {
            adapter = chartsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun showLoading() {
        chartsAdapter?.startLoading()
    }

    override fun hideLoading() {
        chartsAdapter?.stopLoading()
    }

    override fun setItems(list: List<ChartViewModel>) {
        chartsAdapter?.setItems(list)
    }

    override fun addItems(list: List<ChartViewModel>) {
        chartsAdapter?.addItems(list)
        chartsAdapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        dashboardPresenter.onDestroy()
        super.onDestroy()
    }
}
