package co.socialsquad.squad.presentation.feature.company

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.activity_system_select.*
import javax.inject.Inject

class SystemSelectActivity : BaseDaggerActivity(), SystemSelectView {

    companion object {
        const val EXTRA_IS_SYSTEM_EDITION = "EXTRA_IS_SYSTEM_EDITION"
    }

    private var miSave: MenuItem? = null
    private var adapter: RecyclerViewAdapter? = null

    @Inject
    lateinit var systemSelectPresenter: SystemSelectPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_system_select)
        setupToolbar()
        setupList()
        systemSelectPresenter.onCreate()
    }

    private fun setupToolbar() {
        title = ""
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_close_black)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        menuInflater.inflate(R.menu.menu_profile_edit, menu)
        val isSystemEdition = intent.getBooleanExtra(EXTRA_IS_SYSTEM_EDITION, false)
        miSave = menu.findItem(R.id.profile_edit_mi_save)
        miSave?.title =
            if (isSystemEdition) getString(R.string.system_select_save)
            else getString(R.string.system_select_confirm)
        systemSelectPresenter.onCreateOptionsMenu()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }

        R.id.profile_edit_mi_save -> {
            systemSelectPresenter.onSaveClicked()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private val onSelectedListener = object : ViewHolder.Listener<SelectionItemViewModel> {
        override fun onClick(viewModel: SelectionItemViewModel) {
            systemSelectPresenter.selectItem(viewModel)
        }
    }

    fun setupList() {

        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is SelectionItemViewModel -> SELECTION_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    SELECTION_VIEW_MODEL_ID -> SelectionViewHolder(view, onSelectedListener)
                    else -> throw IllegalArgumentException()
                }
            }
        })
        rvSystems.layoutManager = LinearLayoutManager(this)
        rvSystems.adapter = adapter
        rvSystems.isNestedScrollingEnabled = false
    }

    private fun showListBoundaries() {
        vListTopBoundary.visibility = View.VISIBLE
        vListBottomBoundary.visibility = View.VISIBLE
    }

    private fun hideListBoundaries() {
        vListTopBoundary.visibility = View.GONE
        vListBottomBoundary.visibility = View.GONE
    }

    override fun setListItems(items: List<SelectionItemViewModel>) {
        adapter?.setItems(items)
        if (items.isEmpty()) hideListBoundaries() else showListBoundaries()
    }

    override fun setSaveButtonEnabled(enabled: Boolean) {
        miSave?.isEnabled = enabled
    }

    override fun hideLoading() {
        pbLoading.visibility = View.GONE
    }

    override fun showLoading() {
        pbLoading.visibility = View.VISIBLE
    }

    override fun closeScreen() {
        finish()
    }

    override fun onDestroy() {
        systemSelectPresenter.onDestroy()
        super.onDestroy()
    }
}
