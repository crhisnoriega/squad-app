package co.socialsquad.squad.presentation.feature.hub.intranet

import dagger.Binds
import dagger.Module

@Module
abstract class IntranetModule {
    @Binds
    abstract fun view(intranetFragment: IntranetFragment): IntranetView
}
