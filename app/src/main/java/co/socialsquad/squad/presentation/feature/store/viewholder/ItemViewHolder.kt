package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.TextView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ItemViewModel
import kotlinx.android.synthetic.main.view_list_item.view.*

class ItemViewHolder<T : ItemViewModel>(itemView: View, val listener: Listener<T>) : ViewHolder<T>(itemView) {
    private val tvTitle: TextView = itemView.list_item_tv_title

    override fun bind(viewModel: T) {
        tvTitle.text = viewModel.title
        itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    override fun recycle() {}
}
