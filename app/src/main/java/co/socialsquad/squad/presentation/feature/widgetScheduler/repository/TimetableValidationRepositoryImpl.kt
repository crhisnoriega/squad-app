package co.socialsquad.squad.presentation.feature.widgetScheduler.repository

import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreationResponse
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote.TimetableValidationAPI
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TimetableValidationRepositoryImpl(private val api: TimetableValidationAPI) :
    TimetableValidationRepository {

    override fun createSubmission(
        timetableId: Long,
        objectId: Long,
        timetableCreation: TimetableCreation
    ): Flow<TimetableCreationResponse?> {
        return flow {
            emit(api.createTimetableSubmission(timetableId, objectId, timetableCreation).data)
        }
    }
}
