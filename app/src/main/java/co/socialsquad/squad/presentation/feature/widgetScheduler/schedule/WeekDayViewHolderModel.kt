package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.view.View
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.widgetScheduler.component.WidgetScheduleWeekView
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleTimeVO

class WeekDayViewHolder(
    itemView: View,
    private val onClick: (scaleTime: ScaleTimeVO) -> Unit = {}
) : ViewHolder<WeekDayViewHolderModel>(itemView) {
    override fun bind(viewModel: WeekDayViewHolderModel) {
        (itemView as WidgetScheduleWeekView).onClick = onClick
        itemView.bind(viewModel.times, viewModel.companyColor)
    }

    override fun recycle() {
    }
}

class WeekDayViewHolderModel(
    internal val times: List<ScaleTimeVO>,
    internal val companyColor: CompanyColor
) : ViewModel
