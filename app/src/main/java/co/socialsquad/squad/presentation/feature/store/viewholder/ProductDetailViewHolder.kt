package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.TextView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ProductDetailViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import kotlinx.android.synthetic.main.view_product_detail.view.*

class ProductDetailViewHolder(itemView: View, val color: String?) : ViewHolder<ProductDetailViewModel>(itemView) {
    private val tvNumber: TextView = itemView.product_detail_tv_number
    private val tvTitle: TextView = itemView.product_detail_tv_title
    private val tvDescription: TextView = itemView.product_detail_tv_description

    override fun bind(viewModel: ProductDetailViewModel) {
        with(viewModel) {
            tvNumber.text = (adapterPosition + 1).toString()
            tvTitle.text = title
            tvDescription.text = description

            color?.let { tvNumber.backgroundTintList = ColorUtils.stateListOf(it) }
        }
    }

    override fun recycle() {}
}
