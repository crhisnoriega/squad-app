package co.socialsquad.squad.presentation.feature.social

import androidx.core.content.MimeTypeFilter
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Live
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.Query
import co.socialsquad.squad.data.entity.Segmentation
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.Value
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.create.live.LiveCategoryViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.Shareable
import co.socialsquad.squad.presentation.util.toDate
import co.socialsquad.squad.presentation.util.toLocal
import java.io.Serializable
import java.util.Date
import java.util.concurrent.TimeUnit

const val SOCIAL_POST_IMAGE_VIEW_MODEL_ID = R.layout.view_social_post_image
const val SOCIAL_POST_VIDEO_VIEW_MODEL_ID = R.layout.view_social_post_video
const val SOCIAL_LIVE_LIST_VIEW_MODEL_ID = R.layout.view_social_live_list
const val SOCIAL_LIVE_VIEW_MODEL_ID = R.layout.view_social_live

sealed class SocialViewModel : ViewModel, Serializable

abstract class PostViewModel(
    open val pk: Int,
    open val creatorPK: Int,
    open val creatorName: String,
    open val elapsedTime: String,
    open var audience: AudienceViewModel,
    open val creatorAvatar: String,
    open val createdAt: String,
    open val type: String,
    open var liked: Boolean,
    open var likeAvatars: List<String>? = null,
    open var likeCount: Int = 0,
    open var currentUserAvatar: String? = null,
    open var commentsCount: Int,
    open var canEdit: Boolean,
    open var canDelete: Boolean,
    open var canShare: Boolean
) : SocialViewModel(), Serializable

class PostImageViewModel(
    override val pk: Int,
    override val creatorPK: Int,
    override val creatorName: String,
    override val elapsedTime: String,
    override var audience: AudienceViewModel,
    override val creatorAvatar: String,
    override val createdAt: String,
    override val type: String,
    val description: String?,
    val imageUrl: String,
    val width: Int,
    val height: Int,
    override var liked: Boolean,
    override var likeAvatars: List<String>? = null,
    override var likeCount: Int = 0,
    override var currentUserAvatar: String? = null,
    override var commentsCount: Int,
    override var canEdit: Boolean,
    override var canDelete: Boolean,
    override var canShare: Boolean,
    val subdomain: String?
) : PostViewModel(
    pk,
    creatorPK,
    creatorName,
    elapsedTime,
    audience,
    creatorAvatar,
    createdAt,
    type,
    liked,
    likeAvatars,
    likeCount,
    currentUserAvatar,
    commentsCount,
    canEdit,
    canDelete,
    canShare
),
    Serializable,
    Shareable {
    override val shareViewModel get() = ShareViewModel(pk, creatorName, description, imageUrl, Share.Feed, canShare, subdomain)

    constructor(post: Post, userAvatar: String?, subdomain: String?) : this(
        post.pk,
        post.author.pk,
        post.author.user.firstName + " " + post.author.user.lastName,
        post.createdAt,
        AudienceViewModel(post.audienceType, post.segmentation),
        post.author.user.avatar ?: "",
        post.createdAt,
        post.type,
        post.content,
        post.medias?.find {
            FileUtils.isImageMedia(it) && !MimeTypeFilter.matches(it.mimeType, "image/gif")
        }?.let { it.resizedUrl ?: it.url } ?: "",
        post.medias?.find {
            FileUtils.isImageMedia(it) && !MimeTypeFilter.matches(it.mimeType, "image/gif")
        }?.width ?: 0,
        post.medias?.find {
            FileUtils.isImageMedia(it) && !MimeTypeFilter.matches(it.mimeType, "image/gif")
        }?.height ?: 0,
        post.liked,
        post.likedUsersAvatars,
        post.likesCount,
        userAvatar,
        post.commentsCount,
        post.canEdit,
        post.canDelete,
        post.canShare,
        subdomain
    )
}

open class PostVideoViewModel(
    override val pk: Int,
    override val creatorPK: Int,
    override val creatorName: String,
    override val elapsedTime: String,
    override var audience: AudienceViewModel,
    override val creatorAvatar: String,
    override val createdAt: String,
    override val type: String,
    open val hlsUrl: String,
    open val imageUrl: String,
    open val coverUrl: String,
    open val width: Int,
    open val height: Int,
    open var audioEnabled: Boolean,
    open val title: String?,
    open val description: String?,
    open val viewCount: Long,
    override var liked: Boolean,
    override var likeAvatars: List<String>? = null,
    override var likeCount: Int = 0,
    override var currentUserAvatar: String? = null,
    override var commentsCount: Int,
    override var canEdit: Boolean,
    override var canDelete: Boolean,
    override var canShare: Boolean,
    open val subdomain: String?
) : PostViewModel(
    pk,
    creatorPK,
    creatorName,
    elapsedTime,
    audience,
    creatorAvatar,
    createdAt,
    type,
    liked,
    likeAvatars,
    likeCount,
    currentUserAvatar,
    commentsCount,
    canEdit,
    canDelete,
    canShare
),
    Serializable,
    Shareable {
    override val shareViewModel get() = ShareViewModel(pk, creatorName, description, imageUrl, Share.Feed, canShare, subdomain)

    constructor(post: Post, audioEnabled: Boolean = false, userAvatar: String? = "", subdomain: String?) : this(
        post.pk,
        post.author.pk,
        post.author.user.firstName + " " + post.author.user.lastName,
        post.createdAt,
        AudienceViewModel(post.audienceType, post.segmentation),
        post.author.user.avatar ?: "",
        post.createdAt,
        post.type,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.streamings?.firstOrNull()?.playlistUrl
            ?: "",
        post.medias?.find { FileUtils.isVideoMedia(it) }?.let {
            it.resizedUrl ?: it.streamings?.firstOrNull()?.thumbnailUrl
        } ?: "",
        post.cover?.let { it.resizedUrl ?: it.streamings?.firstOrNull()?.thumbnailUrl } ?: "",
        post.medias?.find { FileUtils.isVideoMedia(it) }?.width ?: 0,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.height ?: 0,
        audioEnabled,
        post.title,
        post.content,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.viewsCount ?: 0,
        post.liked,
        post.likedUsersAvatars,
        post.likesCount,
        userAvatar,
        post.commentsCount,
        post.canEdit,
        post.canDelete,
        post.canShare,
        subdomain
    )
}

class PostLiveViewModel(
    override val pk: Int,
    override val creatorPK: Int,
    override val creatorName: String,
    override val elapsedTime: String,
    override var audience: AudienceViewModel,
    override val creatorAvatar: String,
    override val createdAt: String,
    override val type: String,
    override val hlsUrl: String,
    override val imageUrl: String,
    override val coverUrl: String,
    override val width: Int,
    override val height: Int,
    override var audioEnabled: Boolean,
    override val description: String,
    override val viewCount: Long,
    override val title: String,
    val category: LiveCategoryViewModel,
    override var liked: Boolean,
    override var likeAvatars: List<String>? = null,
    override var likeCount: Int = 0,
    override var currentUserAvatar: String? = null,
    override var commentsCount: Int,
    override var canEdit: Boolean,
    override var canDelete: Boolean,
    override var canShare: Boolean,
    override val subdomain: String?
) : PostVideoViewModel(
    pk,
    creatorPK,
    creatorName,
    elapsedTime,
    audience,
    creatorAvatar,
    createdAt,
    type,
    hlsUrl,
    imageUrl,
    coverUrl,
    width,
    height,
    audioEnabled,
    description,
    title,
    viewCount,
    liked,
    likeAvatars,
    likeCount,
    currentUserAvatar,
    commentsCount,
    canEdit,
    canDelete,
    canShare,
    subdomain
),
    Serializable {
    override val shareViewModel get() = ShareViewModel(pk, creatorName, description, imageUrl, Share.Feed, canShare, subdomain)

    constructor(post: Post, audioEnabled: Boolean, userAvatar: String?, subdomain: String?) : this(
        post.pk,
        post.author.pk,
        post.author.user.fullName,
        post.createdAt,
        AudienceViewModel(post.audienceType, post.segmentation),
        post.author.user.avatar ?: "",
        post.live?.startedAt ?: post.createdAt,
        post.type,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.streamings?.firstOrNull()?.playlistUrl
            ?: "",
        post.medias?.find { FileUtils.isVideoMedia(it) }?.let {
            it.resizedUrl ?: it.streamings?.firstOrNull()?.thumbnailUrl
        } ?: "",
        post.cover?.let { it.resizedUrl ?: it.streamings?.firstOrNull()?.thumbnailUrl } ?: "",
        post.medias?.find { FileUtils.isVideoMedia(it) }?.width ?: 0,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.height ?: 0,
        audioEnabled,
        post.live?.content ?: "",
        post.medias?.find { FileUtils.isVideoMedia(it) }?.viewsCount ?: 0,
        post.title ?: "",
        LiveCategoryViewModel(post.live!!.category),
        post.liked,
        post.likedUsersAvatars,
        post.likesCount,
        userAvatar,
        post.commentsCount,
        post.canEdit,
        post.canDelete,
        post.canShare,
        subdomain
    )
}

class LiveListViewModel(val lives: List<ViewModel>, val color: String?) : SocialViewModel()

class LiveViewModel(
    val pk: Int,
    val creatorFullName: String,
    val creatorPK: Int,
    val featuredImageURL: String,
    val url: String,
    val queue: String,
    val startDate: Date? = null,
    val scheduledStartDate: Date? = null,
    val category: String,
    val title: String,
    val content: String,
    val duration: Long,
    val scheduledFeaturedVideo: String? = null,
    val scheduledFeaturedVideoWidth: Int? = null,
    val scheduledFeaturedVideoHeight: Int? = null,
    val scheduledFeaturedThumbnail: String? = null,
    val isAttendingLive: Boolean = false,
    val numberOfAttendants: Int = 0,
    val attendeesPictures: List<String>? = null,
    val rtmpUrl: String,
    val videoUrl: String?,
    val active: Boolean = false,
    var audienceType: String,
    var liked: Boolean,
    var likeAvatars: List<String>? = null,
    var likeCount: Int = 0,
    var currentUserAvatar: String? = null,
    var commentsCount: Int,
    var canEdit: Boolean,
    var canDelete: Boolean,
    var canShare: Boolean,
    val subdomain: String?
) : SocialViewModel(), Serializable, Shareable {
    override val shareViewModel get() = ShareViewModel(pk, creatorFullName, title, featuredImageURL, Share.Live, canShare, subdomain)

    private val isActive get() = active
    private val hasVideoUrl get() = videoUrl != null
    private val hasStartTime get() = startDate != null
    private val hasScheduledStartTime get() = scheduledStartDate != null

    val isLive get() = isActive
    val isScheduledLive get() = !isActive && hasScheduledStartTime && !hasStartTime && !hasVideoUrl
    val isVideo get() = !isActive && hasStartTime && hasVideoUrl

    constructor(live: Live, post: Post? = null, userAvatar: String? = null, subdomain: String?) : this(
        live.pk,
        live.owner.user.fullName,
        live.owner.pk,
        live.schedule?.photo?.url ?: live.owner.user.avatar ?: "",
        live.hlsUrl,
        live.commentQueue ?: "",
        live.startedAt?.toDate(),
        live.schedule?.startTime?.toLocal(),
        live.category.name,
        live.name,
        live.content ?: "",
        live.medias?.firstOrNull()?.timeLength?.let { TimeUnit.SECONDS.toMillis(it) } ?: 0,
        live.schedule?.video?.url,
        live.schedule?.video?.width,
        live.schedule?.video?.height,
        live.schedule?.video?.streamings?.firstOrNull()?.thumbnailUrl,
        live.isAttending ?: false,
        live.totalAttendingUsers ?: 0,
        live.attendingUsersAvatars,
        live.rtmpUrl,
        live.medias?.firstOrNull()?.streamings?.firstOrNull()?.playlistUrl,
        (live.active && live.streaming),
        live.audienceType,
        post?.liked ?: false,
        post?.likedUsersAvatars,
        post?.likesCount ?: 0,
        userAvatar,
        live.questionsCount,
        live.canEdit,
        live.canDelete,
        live.canShare,
        subdomain
    )
}

class AudioToggledViewModel(val audioEnabled: Boolean) : ViewModel

const val SEGMENT_VIEW_MODEL_ID = R.layout.linearlayout_segment_metrics_list_item
const val VALUE_VIEW_MODEL_ID = R.layout.linearlayout_segment_values_list_item
const val DIVIDER_VIEW_MODEL_ID = R.layout.view_segment_divider_list_item
const val MEMBER_VIEW_ID = R.layout.linearlayout_segment_group_members_item

class AudienceViewModel(
    val type: String,
    val members: List<UserViewModel>? = null,
    val metric: MetricsViewModel? = null,
    val downline: Boolean
) : Serializable {
    constructor(type: String, segmentation: Segmentation?) : this(
        type,
        segmentation?.members?.map { UserViewModel(it, true) },
        segmentation?.let { MetricsViewModel(it) },
        segmentation?.downline ?: false
    )

    companion object {
        private const val AUDIENCE_TYPE_PUBLIC = "p"
        private const val AUDIENCE_TYPE_DOWNLINE = "d"
        private const val AUDIENCE_TYPE_METRICS = "f"
        private const val AUDIENCE_TYPE_MEMBERS = "m"

        fun getStringResId(audienceType: String) = when (audienceType) {
            AUDIENCE_TYPE_DOWNLINE -> R.string.social_audience_downline
            AUDIENCE_TYPE_METRICS -> R.string.social_audience_metrics
            AUDIENCE_TYPE_MEMBERS -> R.string.social_audience_members
            else -> R.string.social_audience_public
        }

        fun getDrawableResId(audienceType: String) = when (audienceType) {
            AUDIENCE_TYPE_DOWNLINE -> R.drawable.ic_audience_post_downline
            AUDIENCE_TYPE_METRICS -> R.drawable.ic_audience_post_metrics
            AUDIENCE_TYPE_MEMBERS -> R.drawable.ic_audience_post_members
            else -> R.drawable.ic_audience_post_public
        }
    }
}

class UserViewModel(
    val pk: Int,
    val firstName: String,
    val lastName: String,
    val imageUrl: String,
    var selected: Boolean
) : ViewModel, Serializable {
    constructor(user: UserCompany, selected: Boolean = false) : this(
        user.pk,
        user.user.firstName ?: "",
        user.user.lastName ?: "",
        user.user.avatar ?: "",
        selected
    )
}

data class MetricViewModel(
    val field: String,
    val name: String,
    var values: List<ValueViewModel>,
    val multipick: Boolean,
    val color: String?
) : ViewModel, Serializable {
    constructor(query: Query, multipick: Boolean = false, color: String = "") : this(
        query.field,
        query.displayName,
        query.values.map { ValueViewModel(it, false) },
        multipick,
        color
    )
}

class ValueViewModel(
    val value: String,
    val count: Int,
    var imageUrl: String? = null,
    var name: String,
    var selected: Boolean
) : ViewModel, Serializable {
    constructor(value: Value, selected: Boolean = false) : this(
        value.value,
        value.members,
        value.icon,
        value.displayValue,
        selected
    )
}

data class MetricsViewModel(val selected: List<MetricViewModel>) : Serializable {
    constructor(segmentation: Segmentation, multipick: Boolean = false, color: String = "") : this(
        segmentation.queries.orEmpty().map { MetricViewModel(it, multipick, color) }
    )
}

object DividerViewModel : ViewModel
