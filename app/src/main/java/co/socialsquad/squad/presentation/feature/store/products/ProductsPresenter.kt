package co.socialsquad.squad.presentation.feature.store.products

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.store.ProductListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import javax.inject.Inject

const val EXTRA_PRODUCTS_TYPE = "EXTRA_PRODUCTS_TYPE"
const val EXTRA_PRODUCTS_VIEW_MODEL = "EXTRA_PRODUCTS_VIEW_MODEL"

class ProductsPresenter @Inject constructor(
    private val productsView: ProductsView,
    private val storeInteractor: StoreInteractor
) {
    enum class Type : Serializable { CATEGORY, LIST }

    private val compositeDisposable = CompositeDisposable()
    private var type: Type? = null
    private var pk: Int? = null
    private var page = 1
    private var isLoading = false
    private var isComplete = false
    private val color = storeInteractor.getColor()

    private val observable
        get() = pk?.let {
            when (type) {
                Type.CATEGORY -> storeInteractor.getProductsByCategory(it, page)
                Type.LIST -> storeInteractor.getProductsByList(it, page)
                else -> throw IllegalArgumentException()
            }
        }

    fun onCreate(intent: Intent) {
        if (intent.hasExtra(EXTRA_PRODUCTS_TYPE)) {
            type = intent.getSerializableExtra(EXTRA_PRODUCTS_TYPE) as Type
            val productsViewModel = intent.getSerializableExtra(EXTRA_PRODUCTS_VIEW_MODEL) as ProductListViewModel
            pk = productsViewModel.pk
            productsView.setupToolbar(productsViewModel.title)
        }

        with(productsView) {
            setupList(color)
            setupSwipeRefresh()
        }

        subscribe()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        observable?.let { productCategoryObservable ->
            compositeDisposable.add(
                productCategoryObservable
                    .doOnSubscribe {
                        isLoading = true
                        productsView.startLoading()
                    }.doOnTerminate {
                        productsView.stopLoading()
                        isLoading = false
                    }.doOnNext {
                        isComplete = it.completed
                    }
                    .map { it.storeList }
                    .subscribe(
                        {
                            productsView.addItems(it)
                            page++
                        },
                        {
                            it.printStackTrace()
                            isComplete = true
                            productsView.showMessage(R.string.products_error_get_products, null)
                        }
                    )
            )
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onSearchSelected() {
        productsView.showSearch()
    }
}
