package co.socialsquad.squad.presentation.feature.tool.submission

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunitySubmissionViewHolder
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunitySubmissionViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.opportunityv2.OpportunityDetailsActivity
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewModel
import co.socialsquad.squad.presentation.feature.tool.di.ToolModule
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.activity_opportunity_submission_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

private const val EXTRA_OPPORTUNITY = "extra_opportunity"
private const val OBJECT_TYPE = "object_type"

class OpportunitySubmissionListActivity : BaseActivity() {

    companion object {
        fun start(objectType: String, context: Context, opportunity: Opportunity<Submission>) {
            context.startActivity(
                    Intent(context, OpportunitySubmissionListActivity::class.java).apply {
                        putExtra(EXTRA_OPPORTUNITY, opportunity)
                        putExtra(OBJECT_TYPE, objectType)
                    }
            )
        }
    }

    private val opportunity by extra<Opportunity<Submission>>(EXTRA_OPPORTUNITY)
    private val objectType: String by extra(OpportunityDetailsActivity.OBJECT_TYPE)
    private val viewModel by viewModel<OpportunitySubmissionViewModel>()
    private val companyColor by inject<CompanyColor>()
    private val defaultEmptyState by lazy {
        EmptyState(
                SimpleMedia(""),
                getString(R.string.opportunity_empty_state_msg),
                getString(R.string.opportunity_empty_state_button)
        )
    }

    override val modules: List<Module> = listOf(ToolModule.instance)
    override val contentView = R.layout.activity_opportunity_submission_list

    private val adapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is OpportunitySubmissionViewHolderModel -> OpportunitySubmissionViewHolder.ITEM_VIEW_MODEL_ID
            is ItemShowAllRankingViewModel -> R.layout.ranking_separator
            is EmptySpaceListModel -> R.layout.ranking_separator_top_bottom
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            OpportunitySubmissionViewHolder.ITEM_VIEW_MODEL_ID -> OpportunitySubmissionViewHolder.newInstance(
                    view as ViewGroup, companyColor
            ).apply {
                onSubmissionClickListener = {

                    when (opportunity.header.title) {
                        "Indicações" -> OpportunityDetailsActivity.start(
                                RankingType.Oportunidade,
                                objectType,
                                it.id.toString(),
                                this@OpportunitySubmissionListActivity,
                                initials = it.initials
                        )

                        "Agendamentos" -> OpportunityDetailsActivity.start(RankingType.Schedule, objectType, it.id.toString(), this@OpportunitySubmissionListActivity)
                    }

                }
            }

            R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {

            }

            R.layout.ranking_separator_top_bottom -> EmptySpaceListViewModel(view)
            else -> throw IllegalArgumentException()
        }
    })
    // private val adapterLoading =
    //     LoadingAdapter(R.layout.item_tool_lead_status_loading, ITEM_LOADING_COUNT)

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ibBack.setOnClickListener {
            finish()
        }

        tvTitle.text = opportunity.header.title
        tvSubtitle.text = opportunity.header.subtitle

        setupViewModelObservers()
        setupRecyclerView()
        setupAdapterInitialData()
        viewModel.fetchOpportunities(opportunity.button?.link!!)
    }

    @ExperimentalCoroutinesApi
    private fun setupViewModelObservers() {
        createFlow(viewModel.opportunity)
                .loading { adapter.startLoading() }
                .error {
                    rvSubmissions.isVisible = false
                    showEmptyState(opportunity.emptyState ?: defaultEmptyState)
                    showErrorMessage(it.orEmpty())
                }
                .success { opportunity ->
                    adapter.stopLoading()

                    adapter.addItem(ItemShowAllRankingViewModel("", ""), 0)

                    opportunity?.items?.let { submissions ->
                        adapter.addItems(
                                submissions.filter { it.progress?.status != null }.mapIndexed { index, submission ->
                                    OpportunitySubmissionViewHolderModel(submission, companyColor, index == submissions.size - 1) as ViewModel
                                }.toMutableList().apply {
                                    add(EmptySpaceListModel())
                                }
                        )
                    }

                    rvSubmissions.smoothScrollToPosition(0)

                }
                .launch()
    }

    private fun showEmptyState(emptyState: EmptyState) {
//        Glide.with(this)
//                .load(emptyState.icon.url)
//                .crossFade()
//                .into(img_lead_empty)
//
//        lbl_empty_lead_message.text = emptyState.title
//        btn_empty_action.text = emptyState.subtitle
//        group_empty.isVisible = true
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG
        ).show()
    }

    private fun setupAdapterInitialData() {
        val submissions = opportunity.items?.map { submission ->
            OpportunitySubmissionViewHolderModel(submission, companyColor, false)
        }?.toList()
        submissions?.let {
            adapter.setItems(it)
            rvSubmissions.isVisible = true
        } ?: run {
            showEmptyState(opportunity.emptyState ?: defaultEmptyState)
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvSubmissions.apply {
            this.layoutManager = layoutManager
            this.adapter = this@OpportunitySubmissionListActivity.adapter
//            addOnScrollListener(
//                    EndlessScrollListener(
//                            2,
//                            layoutManager,
//                            viewModel::getNextPage
//                    )
//            )
        }
    }
}
