package co.socialsquad.squad.presentation.feature.audio.list

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.audio.AUDIO_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_audio_list.*
import javax.inject.Inject

class AudioListFragment : Fragment(), AudioListView {

    @Inject
    lateinit var presenter: AudioListPresenter

    private var adapter: RecyclerViewAdapter? = null

    private val emptyState = AudioEmptyStateViewModel()

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_audio_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()
        setupSwipeRefresh()
        setupList()
        presenter.onCreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupToolbar() {
        setHasOptionsMenu(true)
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener { presenter.onRefresh() }
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is AudioViewModel -> AUDIO_VIEW_MODEL_ID
                is AudioEmptyStateViewModel -> AUDIO_EMPTY_STATE_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                AUDIO_VIEW_MODEL_ID -> AudioListItemViewHolder(view, onRecommendListener)
                AUDIO_EMPTY_STATE_VIEW_MODEL_ID -> AudioListEmptyStateViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        })

        rv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@AudioListFragment.adapter
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    private val onRecommendListener = object : ViewHolder.Listener<AudioViewModel> {
        override fun onClick(viewModel: AudioViewModel) {
            onRecommendSelected(viewModel)
        }
    }

    fun onRecommendSelected(viewModel: AudioViewModel) {
        val segmentName = getString(R.string.qualification_title)
        val metric =
            MetricViewModel("qualification", segmentName, listOf<ValueViewModel>(), true, null)
        val intent = Intent(activity, SegmentValuesActivity::class.java)
        intent.putExtra(EXTRA_METRICS, metric)
        presenter.onRecommending(viewModel)
        startActivityForResult(intent, SegmentValuesPresenter.REQUEST_CODE_VALUES)
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit) {
        ViewUtils.showDialog(
            context,
            getString(textResId),
            DialogInterface.OnDismissListener { onDismissListener() }
        )
    }

    override fun setItems(items: List<AudioViewModel>) {
        adapter?.setItems(items)
    }

    override fun addItems(items: List<AudioViewModel>) {
        adapter?.addItems(items)
    }

    override fun removeItem(audioViewModel: AudioViewModel) {
        adapter?.remove(audioViewModel)
    }

    override fun startLoading() {
        adapter?.startLoading()
        if (!srl.isRefreshing) srl.isEnabled = false
    }

    override fun stopLoading() {
        adapter?.stopLoading()
        srl.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun showEmptyState() {
        adapter?.setItems(listOf(emptyState))
        rv.isLayoutFrozen = true
    }

    override fun hideEmptyState() {
        adapter?.remove(emptyState)
        rv.isLayoutFrozen = false
    }

    override fun startRefreshing() {
        srl.isRefreshing = true
    }

    override fun stopRefreshing() {
        srl.isRefreshing = false
    }
}
