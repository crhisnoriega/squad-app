package co.socialsquad.squad.presentation.feature.navigation

interface ReselectedListener {
    fun onReselected()
}
