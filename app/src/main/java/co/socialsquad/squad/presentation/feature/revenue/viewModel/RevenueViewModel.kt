package co.socialsquad.squad.presentation.feature.revenue.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.revenue.model.EventsResponse
import co.socialsquad.squad.presentation.feature.revenue.model.StreamsResponse
import co.socialsquad.squad.presentation.feature.revenue.repository.RevenueRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class RevenueViewModel(val revenueRepository: RevenueRepository) : ViewModel() {

    @ExperimentalCoroutinesApi
    val fetchStreams = MutableStateFlow<Resource<StreamsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val fetchEvents = MutableStateFlow<Resource<EventsResponse>>(Resource.loading(null))


    fun fetchStreams() {
        viewModelScope.launch {
            revenueRepository.fetchStreams()
                .flowOn(Dispatchers.IO)
                .catch { e ->
                    fetchStreams.value = Resource.error("", null)
                }
                .collect {
                    fetchStreams.value = Resource.success(it)
                }
        }
    }

    fun fetchEvents() {
        viewModelScope.launch {
            revenueRepository.fetchEvents()
                .flowOn(Dispatchers.IO)
                .catch { e ->
                    fetchEvents.value = Resource.error(e.message!!, null)
                }
                .collect {
                    fetchEvents.value = Resource.success(it)
                }
        }
    }
}