package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.recommendations.QualificationRecommendationsActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_qualification_requirement_item.view.*
import java.text.NumberFormat
import java.util.Locale

const val QUALIFICATION_RECOMMENDATION_VIEW_MODEL_ID = R.layout.view_qualification_recommendation_item
const val QUALIFICATION_RECOMMENDATION_PROGRESS_COLOR = "#4b8e27"

class QualificationRecommendationViewHolder(itemView: View) : ViewHolder<RecommendationViewModel>(itemView) {
    override fun bind(viewModel: RecommendationViewModel) {

        itemView.short_title_lead.text = viewModel.name
        when (viewModel.type.toLowerCase()) {
            "events" -> itemView.ivRequirement.setImageResource(R.drawable.ic_recommended_events)
            "audio" -> itemView.ivRequirement.setImageResource(R.drawable.ic_recommended_drops)
            "feed" -> itemView.ivRequirement.setImageResource(R.drawable.ic_recommended_videos)
            "live" -> itemView.ivRequirement.setImageResource(R.drawable.ic_recommended_lives)
            else -> itemView.ivRequirement.setImageDrawable(null)
        }
        itemView.tvDescription.text = viewModel.description
        val currentValue = NumberFormat.getNumberInstance(Locale.getDefault()).format(viewModel.value).toString()
        val promotionValue = NumberFormat.getNumberInstance(Locale.getDefault()).format(viewModel.targetValue).toString()
        itemView.tvProgress.text = itemView.context.getString(R.string.qualification_requirement_progress, currentValue, promotionValue)
        itemView.pbProgress.progressTintList = ColorUtils.stateListOf(QUALIFICATION_RECOMMENDATION_PROGRESS_COLOR)

        itemView.pbProgress.progress = viewModel.percentage

        itemView.setOnClickListener {
            val intent = Intent(itemView.context, QualificationRecommendationsActivity::class.java).apply {
                putExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_PK, viewModel.pk)
                putExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_TYPE, viewModel.type)
                putExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_PROGRESS, viewModel.value)
                putExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_TOTAL, viewModel.targetValue)
            }
            it.context.startActivity(intent)
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivRequirement)
        itemView.vlbiDivider.visibility = View.GONE
    }
}
