package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class RichTextDataType(val value: Int) : Parcelable {
    EMPTY_STATE(0),

    LOADING(1),

    @SerializedName("title")
    TITLE(2),

    @SerializedName("subtitle")
    SUBTITLE(3),

    @SerializedName("text")
    TEXT(4),

    @SerializedName("separator")
    DIVISOR(5),

    @SerializedName("media")
    MEDIA(6),

    @SerializedName("link")
    LINK(7),

    @SerializedName("object")
    OBJECT(8),

    @SerializedName("resource")
    RESOURCE(9)
}
