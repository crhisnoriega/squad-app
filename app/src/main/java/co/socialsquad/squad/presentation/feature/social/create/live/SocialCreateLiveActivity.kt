package co.socialsquad.squad.presentation.feature.social.create.live

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputFilter
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.LiveCategory
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.presentation.custom.InputAudience
import co.socialsquad.squad.presentation.custom.InputSchedule
import co.socialsquad.squad.presentation.custom.InputSpinner
import co.socialsquad.squad.presentation.custom.InputText
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.live.creator.LiveCreatorActivity
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.feature.social.create.schedule.SocialCreateScheduledLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.SocialCreateSegmentActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_POTENTIAL_REACH
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsActivity
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_social_create_live.*
import java.io.Serializable
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.TimeZone
import javax.inject.Inject

class LiveCategoryViewModel(var pk: Long, var name: String) : InputSpinner.Item, Serializable {
    constructor(liveCategory: LiveCategory) : this(liveCategory.pk, liveCategory.name)

    override val title: String = name
}

private const val REQUEST_CODE_AUDIENCE = 5
private const val REQUEST_CODE_SCHEDULE = 6

class SocialCreateLiveActivity : BaseDaggerActivity(), SocialCreateLiveView, InputView.OnInputListener {

    @Inject
    lateinit var socialCreateLivePresenter: SocialCreateLivePresenter

    private lateinit var isCategory: InputSpinner<LiveCategoryViewModel>
    private lateinit var itTitle: InputText
    private lateinit var itDescription: InputText
    private lateinit var miStart: MenuItem

    private var metrics: MetricsViewModel? = null
    private var members: IntArray? = null
    private var downline = false
    private var scheduleStartDate: Date? = null
    private var scheduleEndDate: Date? = null
    private var scheduleFeaturedImageUrl: String? = null
    private var schedulePromotionalVideoUrl: String? = null
    private var segmentResultRequestCode: Int? = null
    private var segmentResultResultCode: Int? = null
    private var segmentResultData: Intent? = null
    private var loading: AlertDialog? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) return

        segmentResultRequestCode = requestCode
        segmentResultResultCode = resultCode
        segmentResultData = data

        when (requestCode to resultCode) {
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_PUBLIC -> {
                setAudienceAsPublic()
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_DOWNLINE -> {
                data?.apply {
                    metrics = null
                    members = null
                    downline = true

                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    potentialReach?.let { reach ->
                        val membersText =
                            if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                            else getString(R.string.social_create_segment_item_description_member)
                        iAudience.visibility = View.GONE
                        with(iaAudience) {
                            visibility = View.VISIBLE
                            isEditable = false
                            audience = getString(R.string.audience_downline)
                            audienceImage = R.drawable.ic_audience_detail_downline
                            detail1 = getString(R.string.audience_potential_reach)
                            detail2 = membersText
                            imageResId = R.drawable.ic_audience_detail_metrics_reach
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_METRIC -> {
                data?.apply {
                    members = null
                    downline = false
                    metrics = data.getSerializableExtra(EXTRA_METRICS) as? MetricsViewModel
                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    metrics?.let {
                        potentialReach?.let { reach ->
                            val membersText =
                                if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                                else getString(R.string.social_create_segment_item_description_member)

                            iAudience.visibility = View.GONE
                            with(iaAudience) {
                                visibility = View.VISIBLE
                                isEditable = true
                                audience = getString(R.string.audience_metrics)
                                audienceImage = R.drawable.ic_audience_detail_metrics
                                detail1 = getString(R.string.audience_potential_reach)
                                detail2 = membersText
                                this.potentialReach = reach.circles
                            }
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_MEMBERS -> {
                data?.apply {
                    @Suppress("UNCHECKED_CAST")
                    val selectedMembers = (data.extras?.getSerializable(SegmentGroupMembersActivity.EXTRA_MEMBERS) as? ArrayList<UserViewModel>).orEmpty()
                    members = selectedMembers.map { it.pk }.toIntArray()
                    metrics = null
                    downline = false
                    val membersText = when {
                        selectedMembers.size == 1 -> getString(R.string.segment_members_one_member_selected)
                        selectedMembers.size == 2 -> getString(R.string.segment_members_two_members_selected)
                        selectedMembers.size > 2 -> getString(
                            R.string.segment_members_n_members_selected,
                            selectedMembers.size - 1
                        )
                        else -> ""
                    }

                    val iconUrls = selectedMembers.map { it.imageUrl }

                    iAudience.visibility = View.GONE
                    iaAudience.visibility = View.VISIBLE
                    iaAudience.isEditable = true
                    with(iaAudience) {
                        audience = getString(R.string.audience_members)
                        audienceImage = R.drawable.ic_audience_detail_members
                        detail1 = "${selectedMembers[0].firstName} ${selectedMembers[0].lastName}"
                        detail2 = membersText
                        imageUrl = iconUrls[0]
                    }
                }
            }
            REQUEST_CODE_SCHEDULE to SocialCreateScheduledLiveActivity.RESULT_CODE_SCHEDULE -> {
                data?.apply {
                    miStart.title = getString(R.string.social_create_live_button_schedule)
                    scheduleStartDate = data.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE)?.let { Date(it) }
                    val intentEndDateLong = data.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_END_DATE)
                    if (intentEndDateLong != null && intentEndDateLong > 0L) {
                        scheduleEndDate = Date(intentEndDateLong)
                    }
                    scheduleFeaturedImageUrl = data.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE)
                    schedulePromotionalVideoUrl = data.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_VIDEO)
                    iSchedule.visibility = View.GONE
                    with(iaSchedule) {
                        visibility = View.VISIBLE
                        isEditable = true
                        scheduledDay = DateFormat.getLongDateFormat(context).format(scheduleStartDate)
                        var timeString = DateFormat.getTimeFormat(context).format(scheduleStartDate)
                        scheduleEndDate?.let { timeString += " - " + DateFormat.getTimeFormat(context).format(it) }
                        scheduledTime = timeString + " " + Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.LONG)
                        imageUrl = data.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_VIDEO)
                            ?: data.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE)
                        isVideo = !schedulePromotionalVideoUrl.isNullOrEmpty()
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        socialCreateLivePresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_social_create_live, menu)
        miStart = menu.findItem(R.id.social_create_live_mi_start)
        miStart.setOnMenuItemClickListener { _ ->
            val category = isCategory.selectedItem
            val title = itTitle.text
            val description = itDescription.text
            category?.let {
                socialCreateLivePresenter.onStartClicked(it, title!!, description!!, metrics, members, downline, scheduleStartDate, scheduleEndDate)
            }
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_create_live)

        @Suppress("UNCHECKED_CAST")
        isCategory = social_create_live_si_category as InputSpinner<LiveCategoryViewModel>
        itTitle = social_create_live_ti_title
        itTitle.editText?.let { it.filters = (arrayOf(InputFilter.LengthFilter(40))) }

        itDescription = social_create_live_ti_description

        setupInputListener()
        setupOnClickListeners()

        socialCreateLivePresenter.onCreate(intent)
    }

    private fun setupInputListener() {
        setInputViews(listOf(isCategory, itTitle, itDescription))
    }

    private fun setupOnClickListeners() {
        iAudience.setOnClickListener { openSegment() }
        iaAudience.setOnClickListener { openSegment() }

        iaAudience.listener = object : InputAudience.Listener {
            override fun onRemoveClicked() {
                setAudienceAsPublic()
            }
        }

        iSchedule.setOnClickListener { openSchedule() }
        iaSchedule.setOnClickListener { openSchedule() }
        iaSchedule.listener = object : InputSchedule.Listener {
            override fun onPlayClicked() {
                if (!schedulePromotionalVideoUrl.isNullOrEmpty()) {
                    openVideoActivity(Uri.parse(schedulePromotionalVideoUrl))
                }
            }

            override fun onRemoveClicked() {
                clearSchedule()
            }
        }
    }

    private fun setAudienceAsPublic() {
        iaAudience.visibility = View.GONE
        iAudience.visibility = View.VISIBLE

        segmentResultRequestCode = null
        segmentResultResultCode = null
        segmentResultData = null

        metrics = null
        members = null
        downline = false
    }

    private fun clearSchedule() {
        iSchedule.visibility = View.VISIBLE
        iaSchedule.visibility = View.INVISIBLE

        segmentResultRequestCode = null
        segmentResultResultCode = null
        segmentResultData = null

        scheduleStartDate = null
        scheduleEndDate = null
        scheduleFeaturedImageUrl = null

        miStart.title = getString(R.string.social_create_live_menu_start)
    }

    private fun openSegment() {
        when {
            metrics != null -> {
                val intent = Intent(this, SegmentMetricsActivity::class.java).apply {
                    putExtra(EXTRA_METRICS, metrics)
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            members?.isNotEmpty() ?: false -> {
                val intent = Intent(this, SegmentGroupMembersActivity::class.java).apply {
                    putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, members!!.toTypedArray())
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            else -> {
                val intent = Intent(this, SocialCreateSegmentActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
        }
    }

    private fun openSchedule() {
        val intent = Intent(this, SocialCreateScheduledLiveActivity::class.java)
        scheduleStartDate?.let { intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE, it.time) }
        scheduleEndDate?.let { intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_END_DATE, it.time) }
        scheduleFeaturedImageUrl?.let { intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE, it) }
        schedulePromotionalVideoUrl?.let { intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_VIDEO, it) }
        startActivityForResult(intent, REQUEST_CODE_SCHEDULE)
    }

    override fun setupToolbar(titleResId: Int) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(titleResId)
    }

    override fun showCategories(liveCategoryViewModels: List<LiveCategoryViewModel>) {
        isCategory.items = liveCategoryViewModels
        isCategory.show()
    }

    override fun onInput() {
        socialCreateLivePresenter.onInput(isCategory.selectedItem, itTitle.text, itDescription.text)
    }

    override fun setStartButtonEnabled(enabled: Boolean) {
        miStart.isEnabled = enabled
    }

    override fun setupTitle(title: String) {
        itTitle.text = title
    }

    override fun setupDescription(description: String) {
        itDescription.text = description
    }

    override fun setupCategory(liveCategory: LiveCategoryViewModel) {
        isCategory.items = listOf(liveCategory)
        isCategory.selectedItemPosition = 0
    }

    override fun setupStartButton(textResId: Int) {
        miStart.title = getString(textResId)
    }

    override fun setupAudience(audience: AudienceViewModel) {
        when {
            audience.metric != null -> {
                val intent = Intent().apply { putExtra(EXTRA_METRICS, metrics) }
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_METRIC, intent)
            }
            audience.members != null && audience.members.isNotEmpty() -> {
                val intent = Intent().apply {
                    putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, audience.members as ArrayList<UserViewModel>)
                }
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_MEMBERS, intent)
            }
            audience.downline -> {
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_DOWNLINE, null)
            }
            else -> onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_PUBLIC, null)
        }
    }

    override fun openLiveCreator(liveCreateRequest: LiveCreateRequest) {
        val intent = Intent(this, LiveCreatorActivity::class.java)
        intent.putExtra(LiveCreatorActivity.EXTRA_LIVE_CREATE_REQUEST, liveCreateRequest)
        startActivity(intent)
        ActivityCompat.finishAfterTransition(this)
    }

    override fun showErrorMessage(textResId: Int) {
        ViewUtils.showDialog(this, getString(textResId), null)
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun finishWithResult(intent: Intent?) {
        if (intent != null) {
            setResult(Activity.RESULT_OK, intent)
        } else {
            setResult(Activity.RESULT_OK)
        }
        finish()
    }

    override fun hideSchedule() {
        iaSchedule.visibility = View.GONE
        iSchedule.visibility = View.GONE
    }

    override fun setupAudienceColor(squadColor: String?) {
        squadColor?.let { iaAudience.color = it }
    }

    fun openVideoActivity(videoUri: Uri) {
        val intentVideo = Intent(this, VideoActivity::class.java)
        intentVideo.putExtra(VideoActivity.EXTRA_VIDEO_URL, videoUri.toString())
        startActivity(intentVideo)
    }
}
