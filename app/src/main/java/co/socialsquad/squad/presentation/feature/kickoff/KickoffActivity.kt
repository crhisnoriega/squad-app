package co.socialsquad.squad.presentation.feature.kickoff

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.OpportunityHeader
import co.socialsquad.squad.domain.model.UniqueLink
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.form.FieldType
import co.socialsquad.squad.presentation.feature.kickoff.profile.*
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.voila.VoilaActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

class KickoffActivity : BaseActivity() {

    override val modules: List<Module> = listOf(module {

    })
    override val contentView: Int = R.layout.activity_kickoff

    private val viewModel by viewModel<ProfileEditViewModel>()


    // private val inviteYourTeamFragment = InviteYourTeamFragment(this::openNavigationScreen)
    // private val kickoffProfileFragment = KickoffProfileFragment(this::enableButton)
    private val cpfForm = KickoffCPFFragment(
        "title",
        "subtitle",
        "",
        Field(
            OpportunityHeader(title = "", subtitle = "", icon = null),
            label = "CPF *",
            fieldType = FieldType.CPF,
            capitalLetter = false,
            name = "CPF",
            isMandatory = false,
            validationError = "Número de CPF inválido"
        ), ""
    )

    private val nameForm = KickoffNameFragment(
        "title",
        "subtitle",
        "",
        Field(
            OpportunityHeader(title = "", subtitle = "", icon = null),
            label = "Nome Completo *",
            fieldType = FieldType.ShortText,
            capitalLetter = false,
            name = "Nome Completo",
            isMandatory = false,
            validationError = "Nome inválido"
        ), ""
    )

    private val pictureFragment = KickoffPictureFragment()
    private val uniqueLink = KickoffUniqueLinkFragment().apply {
        arguments = Bundle().apply {
            var link = UniqueLink()
            putParcelable("link", link)
        }
    }
    private val welcome = KickoffWelcomeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupPager()
        setupNextButton()

        viewModel.state.observe(this, Observer {
            when (it.name) {
                "name_form" -> {
                    supportFragmentManager
                        .beginTransaction().apply {
                            if (it.isBack) {
                                setCustomAnimations(
                                    R.anim.anim_slide_right,
                                    R.anim.anim_slide_out_right
                                )
                            } else {
                                setCustomAnimations(
                                    R.anim.anim_slide_left,
                                    R.anim.anim_slide_out_left
                                )
                            }

                        }
                        .replace(
                            R.id.container,
                            nameForm
                        )
                        .commit()
                }

                "cpf_form" -> {
                    supportFragmentManager
                        .beginTransaction().apply {
                            if (it.isBack) {
                                setCustomAnimations(
                                    R.anim.anim_slide_right,
                                    R.anim.anim_slide_out_right
                                )
                            } else {
                                setCustomAnimations(
                                    R.anim.anim_slide_left,
                                    R.anim.anim_slide_out_left
                                )
                            }

                        }
                        .replace(
                            R.id.container,
                            cpfForm
                        )
                        .commit()
                }

                "picture" -> {
                    supportFragmentManager
                        .beginTransaction().apply {
                            if (it.isBack) {
                                setCustomAnimations(
                                    R.anim.anim_slide_right,
                                    R.anim.anim_slide_out_right
                                )
                            } else {
                                setCustomAnimations(
                                    R.anim.anim_slide_left,
                                    R.anim.anim_slide_out_left
                                )
                            }
                        }
                        .replace(
                            R.id.container,
                            pictureFragment
                        )
                        .commit()
                }

                "unique_link" -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(
                            R.id.container,
                            uniqueLink
                        )
                        .commit()
                }

                "welcome" -> {
                    supportFragmentManager
                        .beginTransaction().apply {
                            if (it.isBack) {
                                setCustomAnimations(
                                    R.anim.anim_slide_right,
                                    R.anim.anim_slide_out_right
                                )
                            } else {
                                setCustomAnimations(
                                    R.anim.anim_slide_left,
                                    R.anim.anim_slide_out_left
                                )
                            }
                        }
                        .replace(
                            R.id.container,
                            welcome
                        )
                        .commit()
                }
            }
        })

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.container,
                welcome
            )
            .commit()
    }

    private fun setupNextButton() {
//        bNext.setOnClickListener {
//            when (svpPager.currentItem) {
//                0 -> {
//                    kickoffProfileFragment.onSaveClicked()
//                }
//
//                1 -> {
//                    openNavigationScreen()
//                }
//            }
//        }
    }

    private fun openNavigationScreen() {
        val intent = Intent(this, NavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra(NavigationActivity.extraVoila, "true")

        val voilaIntent = Intent(this, VoilaActivity::class.java)
        startActivities(arrayOf(intent, voilaIntent))
        finish()
    }

//    private fun setupBackButton() {
//        ibBack.setOnClickListener {
//            svpPager.currentItem = 0
//            signOut.visibility = View.VISIBLE
//            bNext.visibility = View.VISIBLE
//            ibBack.visibility = View.GONE
//            bNext.text = getString(R.string.next)
//            currentKickoffStep = 1
//            setupToolbar()
//            window.decorView.rootView.hideKeyboard()
//        }
//    }
//
//    private fun setupToolbar() {
//        tvToolbarTitle.text = getString(R.string.kickoff_title)
//        tvToolbarSubtitle.text = getString(R.string.progress, currentKickoffStep, fragments.size)
//    }

    private fun setupPager() {

    }

    fun forgotPasswordTapped(v: View) {
        val url = "https://escritorio.hinode.com.br/esqueci-senha"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

//    private fun enableButton(enabled: Boolean) {
//        bNext.isEnabled = enabled
//    }
//
//    private fun setupSignOn() {
//        signOut.setOnClickListener { presenter.onLogoutClicked(this) }
//    }
//
//    fun moveToInviteMembers() {
//        svpPager.currentItem = svpPager.currentItem + 1
//        signOut.visibility = View.GONE
//        bNext.text = getString(R.string.skip)
//        ibBack.visibility = View.VISIBLE
//        window.decorView.rootView.hideKeyboard()
//        currentKickoffStep++
//        setupToolbar()
//    }
}
