package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class CEPResponse(
    @SerializedName("cep") var cep: String? = null,
    @SerializedName("logradouro") var logradouro: String? = null,
    @SerializedName("complemento") var complemento: String? = null,
    @SerializedName("bairro") var bairro: String? = null,
    @SerializedName("localidade") var localidade: String? = null,
    @SerializedName("uf") var uf: String? = null,
    @SerializedName("erro") var erro: Boolean? = null,

    ) : Parcelable {
}