package co.socialsquad.squad.presentation.feature.kickoff.profile

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.UniqueLink
import co.socialsquad.squad.presentation.feature.explorev2.sharing.customview.FacebookSharingTypeBottomSheetDialog
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import kotlinx.android.synthetic.main.fragment_kickoff_unique_link.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class KickoffUniqueLinkFragment : Fragment() {

    companion object {
        fun start(context: Context, link: UniqueLink) {
            context.startActivity(Intent(context, KickoffUniqueLinkFragment::class.java).apply {
                putExtra("link", link)
            })
        }
    }

    private val viewModel by sharedViewModel<ProfileEditViewModel>()


    lateinit var link: UniqueLink
    private var isShowingShare = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        link = arguments?.getParcelable("link")!!
        configureToolbar()

        linkShare.text = link?.url?.replace("https://", "")?.replace("http://", "")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_kickoff_unique_link, container, false)


    override fun onResume() {
        super.onResume()
        isShowingShare = false
    }


    private fun configureToolbar() {
        textTitle.text = link.title
        textSubTitle.text = link.description

        btnWhatsapp.setOnClickListener {
            openWhatsApp()
        }

        btnFacebook.setOnClickListener {
            var dialog = FacebookSharingTypeBottomSheetDialog() {
                when (it) {
                    "feed" -> openFacebook()
                    "message" -> openMessenger()
                }
            }
            dialog.show(childFragmentManager, "facebook")
        }

        btnEmail.setOnClickListener {
            sendEmail()
        }



        linkButton.setOnClickListener {
            linkButton.performHapticFeedback(
                HapticFeedbackConstants.VIRTUAL_KEY,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING
            )

            linkShare.text = "Link Copiado"

            copyToClipboard()

            Handler().postDelayed({
                var animation = AnimationUtils.loadAnimation(context, R.anim.fadein_text)
                linkShare.text = link.url?.replace("https://", "")?.replace("http://", "")
                linkShare.startAnimation(animation)
            }, 2500)

            true
        }

    }

    private fun copyToClipboard() {
        val clipboard: ClipboardManager =
            activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", "${link.url}")
        clipboard.setPrimaryClip(clip)
    }

    private fun sendEmail() {
        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "message/rfc822"
        sendIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(""))
        sendIntent.setPackage("com.google.android.gm")
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, link.title)
        sendIntent.putExtra(Intent.EXTRA_TEXT, "${link.description} ${link.url}")
        startActivity(sendIntent)
    }

    private fun openFacebook() {
        var shareDialog = ShareDialog(this)
        val content = ShareLinkContent.Builder()
            .setQuote("${link.description} ${link.url}")
            .build()

        shareDialog.show(content)
    }

    private fun openMessenger() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent
            .putExtra(
                Intent.EXTRA_TEXT,
                link.url
            )
        sendIntent.type = "text/plain"
        sendIntent.setPackage("com.facebook.orca")
        try {
            startActivity(sendIntent)
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun openWhatsApp() {
        val url = "https://api.whatsapp.com/send?text=${link.description} ${link.url}"

        val i = Intent(Intent.ACTION_VIEW)
        i.setPackage("com.whatsapp")

        i.data = Uri.parse(url)
        try {
            startActivity(i)
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun shareText() {
        if (isShowingShare.not()) {

            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "${link.description} ${link.url}")
            startActivity(Intent.createChooser(sharingIntent, "Sharing Option"))

            isShowingShare = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}