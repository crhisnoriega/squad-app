package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue


@Parcelize
data class BenefitsResponse(
        @SerializedName("success") val success: Boolean?,
        @SerializedName("error") var error: @RawValue Any? = null,
        @SerializedName("data") val data: BenefitList?
) : Parcelable