package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.trip.TripViewModel
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_trips_item.view.*

class SearchTripViewHolder(itemView: View) : ViewHolder<TripViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: TripViewModel) {
        with(viewModel) {
            if (viewModel.imageUrl != null) {
                Glide.with(itemView.context)
                    .load(viewModel.imageUrl)
                    .roundedCorners(itemView.context)
                    .crossFade()
                    .into(itemView.ivImage)
                itemView.ivImage.setOnClickListener(null)
            } else if (viewModel.videoUrl != null) {
                Glide.with(itemView.context)
                    .load(viewModel.videoThumbnailUrl)
                    .roundedCorners(itemView.context)
                    .crossFade()
                    .into(itemView.ivImage)
                itemView.ivImage.setOnClickListener { openVideo(viewModel.videoUrl) }
            }

            itemView.tvTitle.text = title

            itemView.tvPromotionText.text = promotionText

            itemView.tvDescription.text = viewModel.description
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }

    private fun openVideo(url: String) {
        val intent = Intent(itemView.context, VideoActivity::class.java).putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        itemView.context.startActivity(intent)
    }
}
