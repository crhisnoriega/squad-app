package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class OpportunityObject(
        @SerializedName("id") val id: String?,
        @SerializedName("short_title") val short_title: String?,
        @SerializedName("short_description") val short_description: String?,
        @SerializedName("custom_fields") val custom_fields: String?,
        @SerializedName("section") val section: Section?

) : Parcelable