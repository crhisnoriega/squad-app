package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
class UserDTO(
    @SerializedName("rg") var rg: String? = null,
    @SerializedName("cpf") var cpf: String? = null,

    @SerializedName("bank_account_savings") var bank_account_savings: Boolean? = false,
    @SerializedName("bank_account_type") var bank_account_type: String? = null,
    @SerializedName("bank_account_document") var bank_account_document: String? = null,
    @SerializedName("bank_account_bank") var bank_account_bank: String? = null,
    @SerializedName("bank_account_name") var bank_account_name: String? = null,
    @SerializedName("bank_account_branch") var bank_account_branch: String? = null,
    @SerializedName("bank_account_number") var bank_account_number: String? = null,
    @SerializedName("bank_account_digit") var bank_account_digit: String? = null,

    @SerializedName("pix") var pix: String? = null,

    @SerializedName("rg_front_image_id") var rg_front_image_id: Int? = null,
    @SerializedName("rg_back_image_id") var rg_back_image_id: Int? = null,

    @SerializedName("birthday") var birthday: String? = null,
    @SerializedName("gender") var gender: String? = null,

    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("last_name") var lastName: String? = null,

    @SerializedName("context_question_1") var context_question_1: String? = null,
    @SerializedName("context_question_2") var context_question_2: String? = null,
    @SerializedName("context_question_3") var context_question_3: String? = null,
    @SerializedName("context_question_4") var context_question_4: String? = null,
    @SerializedName("context_question_5") var context_question_5: String? = null,
    @SerializedName("context_question_6") var context_question_6: String? = null,

    ) : Parcelable {

}