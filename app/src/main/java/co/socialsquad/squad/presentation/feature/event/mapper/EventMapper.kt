package co.socialsquad.squad.presentation.feature.event.mapper

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel

object EventMapper {

    fun mapLoadingContent(): List<ViewModel> {
        return listOf(
            LoadingViewModel(),
            LoadingViewModel(),
            LoadingViewModel(),
            LoadingViewModel(),
            LoadingViewModel(),
            LoadingViewModel(),
            LoadingViewModel()
        )
    }
}
