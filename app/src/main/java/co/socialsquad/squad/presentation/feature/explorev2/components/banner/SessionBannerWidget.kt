package co.socialsquad.squad.presentation.feature.explorev2.components.banner

import android.content.Context
import android.os.Handler
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.feature.explorev2.detail.ExploreItemDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreContent
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.OnPageChangeCallbackWrapper
import com.google.gson.Gson
import com.zhpan.indicator.DrawableIndicator
import com.zhpan.indicator.IndicatorView
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

private const val DEFAULT_SCROLL_TIME = 1

class SessionBannerWidget(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private val view: View
    private var loading: Boolean = true
    private val viewPager: ViewPager2
    private val pageIndicator: DrawableIndicator

    private val viewHolderHandler = Handler()
    private val onPageChangeCallback = OnPageChangeCallbackWrapper()

    private var emptyState: EmptyState? = null
    private var template: SectionTemplate = SectionTemplate.getDefault()

    init {
        setupStyledAttributes(attrs)
        view = changeTemplate(context)
        viewPager = view.findViewById(R.id.pager)
        viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        pageIndicator = view.findViewById(R.id.pageIndicatorView)

        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        pageIndicator
                .setSliderWidth(resources.getDimension(R.dimen.dimen_indicator_small))
                .setSliderHeight(resources.getDimension(R.dimen.dimen_indicator_small))
                .setSliderGap(50f)
                .setSliderColor(ContextCompat.getColor(context, R.color._c1c2c3), ColorUtils.parse(userCompany.company?.primaryColor!!))
                .setSlideMode(IndicatorSlideMode.SCALE)
                .setIndicatorStyle(IndicatorStyle.CIRCLE)

        var draw = if (userCompany.company?.primaryColor!!.equals("#b72c2f")) {
            R.drawable.ic_circle_cyrella
        } else {
            R.drawable.ic_circle_black
        }

        pageIndicator.setIndicatorDrawable(R.drawable.ic_circle_gray_1, draw)
        pageIndicator.setCheckedColor(ColorUtils.parse(userCompany.company?.primaryColor!!))
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.SessionBannerWidget)) {
            loading = getBoolean(R.styleable.SessionBannerWidget_loadingBanner, true)
        }
    }

    private fun changeTemplate(context: Context): View {
        return if (loading) {
            View.inflate(context, R.layout.view_session_big_banner_loading, this)
        } else {
            View.inflate(context, R.layout.view_session_big_banner_widget, this)
        }
    }

    @ExperimentalTime
    fun bind(
            content: ExploreContent?,
            template: SectionTemplate?,
            emptyState: EmptyState? = null
    ) {

        template?.let {
            this.template = it
        }
        this.emptyState = emptyState
        content?.let {
            viewPager.adapter = SessionBannerImageAdapter(
                    context,
                    it.items,
                    this@SessionBannerWidget::onBannerSelected
            )
            pageIndicator.setupWithViewPager(viewPager)

            if (it.autoScroll) {
                var scrollPosition = 0
                onPageChangeCallback.onPageSelected = { position ->
                    scrollPosition = position
                }
                viewPager.registerOnPageChangeCallback(onPageChangeCallback)
                val runnable = object : Runnable {
                    override fun run() {
                        val count = viewPager.adapter?.itemCount ?: 0
                        viewPager.setCurrentItem(scrollPosition++ % count, true)
                        val scrollTime =
                                if (it.scrollTime > 0) it.scrollTime else DEFAULT_SCROLL_TIME
                        viewHolderHandler.postDelayed(
                                this,
                                scrollTime.seconds.inMilliseconds.toLong()
                        )
                    }
                }
                viewHolderHandler.post(runnable)
            } else {
                viewPager.registerOnPageChangeCallback(onPageChangeCallback)
                viewHolderHandler.removeCallbacksAndMessages(null)
            }
        } ?: run {
            viewPager.registerOnPageChangeCallback(onPageChangeCallback)
            viewHolderHandler.removeCallbacksAndMessages(null)
        }
    }

    private fun onBannerSelected(contentItem: ContentItem) {
        if (contentItem.isObject()) {
            contentItem.link?.let {
                // context.startActivity(ExploreItemObjectActivity.newInstance(context, it))
            }
        } else {
            ExploreItemDetailActivity.start(
                    context = context,
                    item = contentItem,
                    template = template,
                    emptyState = emptyState
            )
        }
    }
}
