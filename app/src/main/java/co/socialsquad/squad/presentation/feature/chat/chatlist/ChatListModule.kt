package co.socialsquad.squad.presentation.feature.chat.chatlist

import dagger.Binds
import dagger.Module

@Module
abstract class ChatListModule {
    @Binds
    abstract fun view(chatListActivity: ChatListActivity): ChatListView
}
