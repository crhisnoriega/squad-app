package co.socialsquad.squad.presentation.feature.login.email

import co.socialsquad.squad.data.entity.Company

interface LoginEmailView {
    fun showLoading()
    fun hideLoading()
    fun enableNextButton(enabled: Boolean)
    fun openSubdomainsScreen(companies: List<Company>, email: String)
    fun showEmptySubdomains()
    fun moveToPasswordScreen(company: Company, email: String)
}
