package co.socialsquad.squad.presentation.feature.newlead.repository

import co.socialsquad.squad.domain.model.form.FormUpdate
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.PUT
import retrofit2.http.Path

interface NewLeadAPI {

    @PUT("endpoints/opportunity/submission/{submission_id}/widget/{widget_id}")
    suspend fun formUpdate(
        @Path("submission_id") submissionId: Long,
        @Path("widget_id") widgetId: Long,
        @Body formUpdate: FormUpdate
    ): Response<String?>
}
