package co.socialsquad.squad.presentation.feature.document.list

import co.socialsquad.squad.presentation.feature.document.DocumentViewModel

interface DocumentListView {
    fun setupList(companyColor: String?)
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun addItems(documents: List<DocumentViewModel>, shouldClearList: Boolean)
    fun showMessage(textResId: Int, onDismissListener: () -> Unit? = {})
}
