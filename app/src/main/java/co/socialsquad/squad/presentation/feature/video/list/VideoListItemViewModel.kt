package co.socialsquad.squad.presentation.feature.video.list

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import java.util.concurrent.TimeUnit

const val VIDEO_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_video_list_item

class VideoListItemViewModel(
    val pk: Int,
    val authorPk: Int,
    val authorName: String,
    val thumbnailUrl: String?,
    val title: String?,
    val description: String? = null,
    val timestamp: String,
    val durationMillis: Long,
    val viewCount: Int,
    val videoPk: Int,
    val videoUrl: String,
    var visualized: Boolean = false,
    var canRecommend: Boolean = false
) : ViewModel {
    constructor(post: Post) : this(
        post.pk,
        post.author.pk,
        post.author.user.fullName,
        post.cover?.let { if (FileUtils.isVideoMedia(it)) it.streamings?.firstOrNull()?.thumbnailUrl else it.url }
            ?: post.medias?.find { FileUtils.isVideoMedia(it) }?.streamings?.firstOrNull()?.thumbnailUrl,
        post.title,
        post.content,
        post.createdAt,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.timeLength?.let { TimeUnit.SECONDS.toMillis(it) }
            ?: 0,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.viewsCount?.toInt() ?: 0,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.pk ?: 0,
        post.medias?.find { FileUtils.isVideoMedia(it) }?.streamings?.firstOrNull()?.playlistUrl
            ?: "",
        post.visualized,
        post.canRecommend
    )

    var position = 0L
}
