package co.socialsquad.squad.presentation.feature.signup.accesssquad

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.RegisterUserRequest
import co.socialsquad.squad.presentation.feature.about.AboutActivity
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.feature.signup.accesssquad.adapters.PrivateSquadAdapter
import co.socialsquad.squad.presentation.feature.signup.accesssquad.adapters.PublicSquadAdapter
import co.socialsquad.squad.presentation.feature.signup.credentials.SignupCredentialsActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_access_squad.*
import javax.inject.Inject

class AccessSquadActivity : BaseDaggerActivity(), AccessSquadView {

    companion object {
        const val extraToken = "extra_token"
        const val extraEmail = "extra_email"
        const val extraCompanies = "extra_companies"
        const val extraIsLogin = "extra_is_login"
        const val extraIsMagicLink = "extra_is_magic_link"
    }

    private var isLogin = false
    private var isMagicLink = false
    private val privateSquadAdapter = PrivateSquadAdapter(this::onSquadSelected)
    private val publicSquadAdapter = PublicSquadAdapter(this::onSquadSelected)

    @Inject
    lateinit var presenter: AccessSquadPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_squad)

        if (intent.hasExtra(extraIsLogin)) {
            isLogin = intent.extras?.getBoolean(extraIsLogin)!!
        }

        if (intent.hasExtra(extraIsMagicLink)) {
            isMagicLink = intent.extras?.getBoolean(extraIsMagicLink)!!
        }

        if (isLogin) {
            tvTitleDesc.text = getString(R.string.access_squad_title_description_sign)
            close.setBackgroundResource(R.drawable.ic_back_feedback)
            close.setOnClickListener {
                finish()
            }
        } else {
            close.setBackgroundResource(R.drawable.ic_close_feedback)
            close.setOnClickListener {
                startActivity(Intent(baseContext, OnboardingActivity::class.java))
                finish()
            }
        }

        presenter.onCreate(intent)
        setupDetailsText()
        setupAdapter()

        detailsText.setOnClickListener {
            val intent = Intent(this, SignupCredentialsActivity::class.java)
            startActivity(intent)
            ActivityCompat.finishAfterTransition(this)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.clearData()
    }

    private fun setupDetailsText() {
        val text = "Está procurando um squad diferente? Você pode <font color=\"#FFA601\">tentar um outro e-mail </font> ou pedir para o administrador do seu squad te enviar um convite."

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            detailsText.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            detailsText.text = Html.fromHtml(text)
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        privateSquads.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
        privateSquads.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun addPrivateCompanies(companies: List<Company>) {
        privateSquads.visibility = View.VISIBLE
        privateSquadAdapter.addCompanies(companies)
    }

    override fun addPublicCompanies(companies: List<Company>) {
        publicSquads.visibility = View.VISIBLE
        publicSquadAdapter.addCompanies(companies)
    }

    override fun showListContent() {
        companiesContent.visibility = View.VISIBLE
    }

    override fun showTryAgain() {
        tryAgainContainer.visibility = View.VISIBLE
        privateSquads.visibility = View.GONE
    }

    override fun hideTryAgain() {
        tryAgainContainer.visibility = View.GONE
        privateSquads.visibility = View.VISIBLE
    }

    private fun setupAdapter() {
        privateSquads.run {
            layoutManager = LinearLayoutManager(baseContext)
            isNestedScrollingEnabled = false
            adapter = privateSquadAdapter
            privateSquadAdapter.isLogin(isLogin)
        }

        publicSquads.run {
            layoutManager = LinearLayoutManager(baseContext)
            isNestedScrollingEnabled = false
            adapter = publicSquadAdapter
            publicSquadAdapter.isLogin(isLogin)
        }
    }

    private fun onSquadSelected(subdomain: String, isUniqueValue: Boolean, token: String?, company: Company, type: String) {
        presenter.onSquadSelected(subdomain, type, !company.loginToken.isNullOrEmpty(), isLogin, isMagicLink)
        presenter.setSubdomain(subdomain)

        Log.i("token", "onSquadSelected")
        presenter.authVerifyToken(subdomain, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJlbWFpbCI6ImFkbWluQHNxdWFkLmNvbS5iciIsInVzZXJuYW1lIjoiYWRtaW5Ac3F1YWQuY29tLmJyIiwiZXhwIjoxNjAxMTQ1MjMzLCJvcmlnX2lhdCI6MTU5NTk2MTIzM30.8pMMRIeKjWz_VWMQob64OfwCdvyoOrkTe4MROBueluY")

        if (!company.loginToken.isNullOrEmpty()) {
            company.loginToken?.let { presenter.authVerifyToken(subdomain, it) }
        } else if (intent.hasExtra(extraCompanies)) {
            val gson = Gson()
            val jsonCompany = gson.toJson(company)
            val email = intent.extras?.getString(extraEmail)

            val intent = Intent(this, LoginPasswordActivity::class.java)
            intent.putExtra(LoginPasswordActivity.extraCompany, jsonCompany)
            intent.putExtra(LoginPasswordActivity.EXTRA_LOGIN_EMAIL, email)
            intent.putExtra(LoginPasswordActivity.EXTRA_LOGIN_SUBDOMAIN, subdomain)

            startActivity(intent)
        } else {
            val register = token?.let { RegisterUserRequest(RegisterUserRequest.User(email = intent.extras?.getString(extraEmail)), it, isUniqueValue) }
            val gson = Gson()
            val registerJson = gson.toJson(register)

            val intent = Intent(this, AboutActivity::class.java)
            intent.putExtra(AboutActivity.extraSubdomain, subdomain)
            intent.putExtra(AboutActivity.extraRegister, registerJson)
            intent.putExtra(AboutActivity.extraHasBack, true)
            startActivity(intent)
        }
    }

    override fun openCompleteRegistration() {
        startActivity(Intent(this, KickoffActivity::class.java))
        finish()
    }

    override fun openNavigation() {
        val intent = Intent(this, NavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }
}
