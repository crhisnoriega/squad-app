package co.socialsquad.squad.presentation.feature.chat

import android.view.View
import co.socialsquad.squad.presentation.custom.PagedViewHolder
import kotlinx.android.synthetic.main.view_chat_message_left.view.*

class OtherPersonMessageViewHolder(
    itemView: View
) : PagedViewHolder<OtherPersonMessageViewModel>(itemView) {

    override fun bind(viewModel: OtherPersonMessageViewModel) {
        itemView.tvMessage.text = viewModel.message
    }

    override fun recycle() {}
}
