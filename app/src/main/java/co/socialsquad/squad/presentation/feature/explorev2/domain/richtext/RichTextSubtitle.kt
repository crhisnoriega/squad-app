package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichTextSubtitle(var subTitle: String? = null) : ViewType, Parcelable {
    constructor(richTextVo: RichTextVO) : this() {
        this.subTitle = richTextVo.subtitle
    }

    override fun getViewType(): Int {
        return RichTextDataType.SUBTITLE.value
    }
}
