package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListItemViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_store_categories_list_item.view.*

class CategoryViewHolder(itemView: View, val glide: RequestManager, val listener: Listener<StoreCategoryListItemViewModel>) : ViewHolder<StoreCategoryListItemViewModel>(itemView) {
    private val ivImage: ImageView = itemView.store_categories_list_item_iv_image
    private val tvName: TextView = itemView.store_categories_list_item_tv_name

    override fun bind(viewModel: StoreCategoryListItemViewModel) {
        with(viewModel) {
            tvName.text = title

            ivImage.clipToOutline = true

            glide.load(icon)
                .crossFade()
                .into(ivImage)

            itemView.setOnClickListener { listener.onClick(viewModel) }
        }
    }

    override fun recycle() {
        glide.clear(ivImage)
    }
}
