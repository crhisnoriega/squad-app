package co.socialsquad.squad.presentation.feature.signup.credentials

import dagger.Binds
import dagger.Module

@Module
abstract class SignupCredentialsModule {

    @Binds
    abstract fun signupCredentialsView(signupCredentialsActivity: SignupCredentialsActivity): SignupCredentialsView
}
