package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val OPORTUNIDADE = "opportunity"
private const val ESCALA = "timetable"
private const val LEAD = "lead"
private const val SCHEDULE = "schedule"

sealed class RankingType(val value: String) : Parcelable {

    companion object {
        fun getByValue(value: String) = when (value) {
            OPORTUNIDADE -> Oportunidade
            ESCALA -> Escala
            else -> Invalid
        }
    }

    @Parcelize
    object Invalid : RankingType("")

    @Parcelize
    object Oportunidade : RankingType(OPORTUNIDADE)

    @Parcelize
    object Escala : RankingType(ESCALA)

    @Parcelize
    object Lead : RankingType(LEAD)

    @Parcelize
    object Schedule : RankingType(SCHEDULE)

}
