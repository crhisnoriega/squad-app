package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PlanSummaryResponse(
        @SerializedName("results") val results: List<PlanData>?,
        @SerializedName("count") val count: Int?,
        @SerializedName("next") val next: String?,
        @SerializedName("previous") val previous: String?,
        var first: Boolean = false
) : Parcelable