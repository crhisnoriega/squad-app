package co.socialsquad.squad.presentation.feature.splash

class EnvironmentModel(
    var localEnvironmentApi: String,
    var localEnvironmentApiIntegration: String,
    var localEnvironmentUpload: String,
    var localEnvironmentRabbitUser: String,
    var localEnvironmentRabbitPassword: String,
    var localEnvironmentRabbitHost: String,
    var localEnvironmentRabbitVirtual: String,
    var localEnvironmentRabbitPort: Int
)
