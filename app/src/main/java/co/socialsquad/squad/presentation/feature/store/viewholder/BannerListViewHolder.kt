package co.socialsquad.squad.presentation.feature.store.viewholder

import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.presentation.custom.PageIndicator
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreBannerListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreBannerViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_banner_list.view.*

class BannerListViewHolder(itemView: View, val listener: Listener<StoreBannerViewModel>, val color: String? = null) : ViewHolder<StoreBannerListViewModel>(itemView) {
    private val vpItems: ViewPager = itemView.store_banner_list_vp_items
    private val piIndicator: PageIndicator = itemView.store_banner_list_pi_indicators
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreBannerListViewModel) {
        val numberOfItems = viewModel.banners.size

        val bannerAdapter = BannerAdapter(viewModel.banners)

        vpItems.adapter = bannerAdapter
        vpItems.offscreenPageLimit = numberOfItems

        if (numberOfItems > 1) {
            var isPageChangingAutomatically = false

            val handlerChangePage = Handler()
            val runnableChangePage = {
                val isLastItem = vpItems.currentItem == vpItems.childCount - 1
                val item = if (isLastItem) 0 else vpItems.currentItem + 1
                isPageChangingAutomatically = true
                vpItems.setCurrentItem(item, true)
            }
            val handlerChangePageDelay = 4000L
            handlerChangePage.postDelayed(runnableChangePage, handlerChangePageDelay)

            vpItems.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {
                    handlerChangePage.removeCallbacksAndMessages(null)
                    if (isPageChangingAutomatically) {
                        handlerChangePage.postDelayed(runnableChangePage, handlerChangePageDelay)
                        isPageChangingAutomatically = false
                    } else {
                        handlerChangePage.postDelayed(runnableChangePage, 8000)
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })

            with(piIndicator) {
                viewPager = vpItems
                types = (0 until numberOfItems).map { co.socialsquad.squad.presentation.custom.PageIndicator.Type.Image() }
                backgroundColor = co.socialsquad.squad.presentation.util.ColorUtils.parse(context, co.socialsquad.squad.R.color.black_50)
                color?.let { selectedColor = co.socialsquad.squad.presentation.util.ColorUtils.parse(it) }
                init()
            }
        }
    }

    override fun recycle() {
        piIndicator.removeAllViews()
    }

    private inner class BannerAdapter(private val banners: List<StoreBannerViewModel>) : PagerAdapter() {
        override fun instantiateItem(container: ViewGroup, position: Int): Any =
            ImageView(itemView.context).apply {
                val banner = banners[position]
                glide.load(banner.image)
                    .crossFade()
                    .into(this)
                scaleType = ImageView.ScaleType.CENTER_CROP
                container.addView(this)
                setOnClickListener { listener.onClick(banner) }
            }

        override fun getCount() = banners.size

        override fun isViewFromObject(view: View, `object`: Any) = view === `object`

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            glide.clear(`object` as ImageView)
            container.removeView(`object`)
        }
    }
}
