package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.LinkTypeAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.LinkType
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LinkV2(
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("link") val link: String?
) : Parcelable
