package co.socialsquad.squad.presentation.feature.password

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.terms.TermsActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setGradientBackground
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlinx.android.synthetic.main.activity_post_password.*
import javax.inject.Inject

class PostPasswordActivity : BaseDaggerActivity(), PostPasswordView {

    companion object {
        const val extraRegister = "extra_register"
        const val extraCompany = "extra_company"
        const val extraSubdomain = "extra_subdomain"
    }

    @Inject
    lateinit var presenter: PostPasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_password)

        presenter.onCreate(intent)

        setupKeyboard()

        bNext.isEnabled = false
        bNext.setOnClickListener {
            hideKeyboard()
            presenter.onNextClicked(iltInput.getFieldText().toString(), "")
        }

        iltInput.addTextChangedListener { text -> presenter.onInputChanged(text) }
        iltInput.setHelpText(getString(R.string.set_password_to, intent?.extras?.getString(extraSubdomain)))
        close.setOnClickListener {
            hideKeyboard()
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        showKeyboard()
    }

    private fun setupKeyboard() {
        iltInput.requestFocus()
    }

    override fun setErrorMessage(resId: Int) {
        iltInput.setErrorText(getString(resId))
    }

    override fun showLoading() {
        iltInput.loading(true)
    }

    override fun hideLoading() {
        iltInput.loading(false)
    }

    override fun enableNextButton(boolean: Boolean) {
        bNext.isEnabled = boolean
    }

    override fun setTerms(terms: String) {
        presenter.setSubdomain(intent.extras?.getString(extraSubdomain)!!)
        val termsIntent = Intent(this, TermsActivity::class.java)

        val register = intent.extras?.getString(extraRegister)
        termsIntent.putExtra(TermsActivity.extraRegister, register)
        termsIntent.putExtra(TermsActivity.extraPassword, iltInput.getFieldText().toString())
        termsIntent.putExtra(TermsActivity.extraSubdomain, intent.extras?.getString(extraSubdomain))
        termsIntent.putExtra(TermsActivity.extraTerms, terms)
        termsIntent.putExtra(TermsActivity.extraCompany, intent.extras?.getString(extraCompany))
        startActivity(termsIntent)
    }

    override fun setupHeader(company: Company?) {
        company?.let {

            if (it.isPublic != null && it.isPublic!!) {
                it.aboutGradient?.apply {
                    if (this.isNotEmpty()) {
                        setColorsConfiguration(this[0], this[1], it.aboutFooterBackground!!, it.textColor!!)
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }
            }
            squadHeader.visibility = View.VISIBLE
            squadHeader.bindCompany(it)
        }
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        container.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        close.setColorFilter(color)

        iltInput.setColors(textColor)
        tvAlternative.setTextColor(color)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
