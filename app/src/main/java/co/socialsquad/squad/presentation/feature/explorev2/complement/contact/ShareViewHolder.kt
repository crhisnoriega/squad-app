package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.item_object_share.view.*

class ShareViewHolder(
    itemView: View,
    var onClick: () -> Unit
) : ViewHolder<ShareViewHolderModel>(itemView) {

    companion object{
        const val VIEW_ITEM_POSITION_ID = R.layout.item_object_share
    }

    override fun bind(viewModel: ShareViewHolderModel) {
        itemView.share_icon.setOnClickListener {
            onClick.invoke()
        }
    }

    override fun recycle() {
    }
}