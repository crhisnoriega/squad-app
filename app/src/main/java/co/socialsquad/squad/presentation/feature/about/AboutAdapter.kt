package co.socialsquad.squad.presentation.feature.about

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.AboutResponse
import co.socialsquad.squad.presentation.feature.about.internal.InternalAdapter
import co.socialsquad.squad.presentation.util.ColorUtils

class AboutAdapter : RecyclerView.Adapter<AboutAdapter.ViewHolder>() {

    private val topics = arrayListOf<AboutResponse.Topics>()
    private var textColor: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.about_item, parent, false))

    override fun getItemCount(): Int = topics.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val topic = topics[position]
        holder.title.text = topic.title
        holder.description.text = topic.description
        holder.internalList.run {
            layoutManager = LinearLayoutManager(holder.itemView.context)
            adapter = InternalAdapter(topic.items, textColor)
        }

        textColor?.apply {
            val color = ColorUtils.parse(this)
            holder.title.setTextColor(color)
            holder.description.setTextColor(color)
        }
    }

    fun items(topics: List<AboutResponse.Topics>) {
        this.topics.addAll(topics)
        notifyDataSetChanged()
    }

    fun textColor(textColor: String) {
        this.textColor = textColor
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.object_type_lead)
        val description: TextView = itemView.findViewById(R.id.status)
        val internalList: RecyclerView = itemView.findViewById(R.id.internalDescription)
    }
}
