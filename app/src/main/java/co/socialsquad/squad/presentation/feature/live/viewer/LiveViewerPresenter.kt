package co.socialsquad.squad.presentation.feature.live.viewer

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Comment
import co.socialsquad.squad.data.entity.End
import co.socialsquad.squad.data.entity.LiveCommentRequest
import co.socialsquad.squad.data.entity.Participant
import co.socialsquad.squad.data.entity.Stats
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LIVE_COMMENT
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LIVE_SHARE
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LiveViewerPresenter @Inject constructor(
    private val liveViewerView: LiveViewerView,
    private val liveRepository: LiveRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var pk: Int? = null
    private var hasJoined = false

    fun onCreate(intent: Intent) {
        liveViewerView.keepScreenOn(true)

        val canCreateComment = loginRepository.userCompany?.canCreateLiveComment() ?: false
        if (canCreateComment) liveViewerView.showCommentField()

        if (intent.hasExtra(LiveViewerActivity.PK)) {
            pk = intent.getIntExtra(LiveViewerActivity.PK, -1)
            if (pk == -1) liveViewerView.showMessage(
                R.string.live_viewer_error_get_live,
                DialogInterface.OnDismissListener { liveViewerView.close() }
            )

            pk?.let { showLiveFromNotification(it) }
        } else if (intent.hasExtra(LiveViewerActivity.LIVE_VIEW_MODEL)) {
            val liveViewModel = intent.getSerializableExtra(LiveViewerActivity.LIVE_VIEW_MODEL) as? LiveViewModel
            if (liveViewModel == null) {
                liveViewerView.showMessage(
                    R.string.live_viewer_error_get_live,
                    DialogInterface.OnDismissListener { liveViewerView.close() }
                )
            } else {
                val url = liveViewModel.url
                val queue = liveViewModel.queue

                liveViewerView.setupViewer(url)
                liveViewerView.setupShareButton(liveViewModel)
                connectQueue(queue)
            }
        }
    }

    fun onResume() {
        joinLive()
    }

    fun onPause() {
        leaveLive()
    }

    private fun joinLive() {
        if (!hasJoined) pk?.let {
            compositeDisposable.add(liveRepository.joinLive(it).subscribe({ hasJoined = true }, { it.printStackTrace() }))
        }
    }

    private fun leaveLive() {
        if (hasJoined) pk?.let {
            compositeDisposable.add(liveRepository.leaveLive(it).subscribe({ hasJoined = false }, { it.printStackTrace() }))
        }
    }

    private fun showLiveFromNotification(pk: Int) {
        compositeDisposable.add(
            liveRepository.getLive(pk)
                .doOnSubscribe { liveViewerView.showLoading() }
                .subscribe(
                    { live ->
                        liveViewerView.hideLoading()

                        if (live.active) with(live) {
                            liveViewerView.setupViewer(hlsUrl)
                            connectQueue(commentQueue ?: "")
                        } else {
                            val videoUrl = live.medias?.firstOrNull()?.streamings?.firstOrNull()?.playlistUrl
                            if (videoUrl != null) liveViewerView.openVideo(videoUrl)
                            else liveViewerView.showMessage(
                                R.string.live_viewer_message_ended,
                                DialogInterface.OnDismissListener { liveViewerView.close() }
                            )
                        }
                    },
                    {

                        FirebaseCrashlytics.getInstance().log("showLiveFromNotification error")
                        FirebaseCrashlytics.getInstance().recordException(it)

                        it.printStackTrace()
                        liveViewerView.showMessage(
                            R.string.live_viewer_error_get_live,
                            DialogInterface.OnDismissListener { liveViewerView.close() }
                        )
                    }
                )
        )
    }

    private fun connectQueue(queue: String) {
        compositeDisposable.add(
            liveRepository.connectQueue(queue)
                .subscribe(
                    {
                        when (it) {
                            is Comment, is Participant -> liveViewerView.addQueueItem(it)
                            is Stats -> liveViewerView.setParticipantCount(it.participants)
                            is End -> liveViewerView.setStreamingEnded()
                        }
                    },
                    {
                        it.printStackTrace()
                        liveViewerView.showMessage(R.string.live_error_queue_connection)
                    }
                )
        )
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_LIVE_SHARE)
    }

    fun onPostClicked(message: String) {
        tagWorker.tagEvent(TAG_TAP_LIVE_COMMENT)
        pk?.let {
            val liveCommentRequest = LiveCommentRequest(message)
            compositeDisposable.add(
                liveRepository.comment(it, liveCommentRequest)
                    .subscribe(
                        {
                            hasJoined = true
                        },
                        {
                            it.printStackTrace()
                            liveViewerView.showMessage(R.string.live_viewer_error_post_comment)
                        }
                    )
            )
        }
    }

    fun onDestroy() {
        liveViewerView.keepScreenOn(false)
        liveRepository.disconnectQueue()
        compositeDisposable.dispose()
    }
}
