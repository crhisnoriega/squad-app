package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import com.google.gson.annotations.SerializedName

enum class LinkDataType(val value: Int, val parseString: String) {
    @SerializedName("object")
    OBJECT(0, "object"),

    @SerializedName("category")
    CATEGORY(1, "category"),

    @SerializedName("collection")
    COLLECTION(2, "collection")
}
