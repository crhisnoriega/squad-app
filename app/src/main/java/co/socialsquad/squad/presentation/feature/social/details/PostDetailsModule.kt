package co.socialsquad.squad.presentation.feature.social.details

import dagger.Binds
import dagger.Module

@Module
abstract class PostDetailsModule {
    @Binds
    abstract fun view(postDetailsActivity: PostDetailsActivity): PostDetailsView
}
