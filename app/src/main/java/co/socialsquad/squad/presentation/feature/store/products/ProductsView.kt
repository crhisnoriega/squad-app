package co.socialsquad.squad.presentation.feature.store.products

import android.content.DialogInterface
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel

interface ProductsView {
    fun setupToolbar(title: String)
    fun setupList(color: String?)
    fun setupSwipeRefresh()
    fun showMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?)
    fun setItems(products: List<StoreProductViewModel>)
    fun addItems(products: List<StoreProductViewModel>)
    fun enableRefreshing()
    fun stopRefreshing()
    fun startLoading()
    fun stopLoading()
    fun showSearch()
}
