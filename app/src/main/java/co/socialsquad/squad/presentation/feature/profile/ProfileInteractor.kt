package co.socialsquad.squad.presentation.feature.profile

import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import io.reactivex.Observable

interface ProfileInteractor {
    fun getProfile(): Observable<ProfileViewModel>
    fun getPosts(page: Int): Observable<Feed>
    fun getPosts(pk: Int, page: Int): Observable<Feed>
    fun getProfile(pk: Int): Observable<ProfileViewModel>
}
