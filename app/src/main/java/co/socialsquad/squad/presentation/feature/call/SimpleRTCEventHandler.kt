package co.socialsquad.squad.presentation.feature.call

import android.util.Log
import org.webrtc.DataChannel
import org.webrtc.IceCandidate
import org.webrtc.MediaStream
import org.webrtc.PeerConnection
import org.webrtc.RtpReceiver

class SimpleRTCEventHandler(
    private val onIceCandidateCb: (IceCandidate) -> Unit,
    private val onAddStreamCb: (MediaStream) -> Unit,
    private val onRemoveStreamCb: (MediaStream) -> Unit,
    private val onIceConnectionChangeCb: (PeerConnection.IceConnectionState) -> Unit,
    private val onIceCandidatesRemovedCb: (candidates: Array<out IceCandidate>) -> Unit
) : PeerConnection.Observer {

    val TAG = "RTCEVENT"

    override fun onIceCandidate(candidate: IceCandidate?) {
        if (candidate != null) onIceCandidateCb(candidate)
    }

    override fun onAddStream(stream: MediaStream?) {
        if (stream != null) onAddStreamCb(stream)
    }

    override fun onRemoveStream(stream: MediaStream?) {
        if (stream != null) onRemoveStreamCb(stream)
    }

    override fun onDataChannel(chan: DataChannel?) { Log.w(TAG, "onDataChannel: $chan") }

    override fun onIceConnectionReceivingChange(p0: Boolean) { Log.w(TAG, "onIceConnectionReceivingChange: $p0") }

    override fun onIceConnectionChange(newState: PeerConnection.IceConnectionState?) {
        newState?.let {
            onIceConnectionChangeCb(it)
        }
    }

    override fun onIceGatheringChange(newState: PeerConnection.IceGatheringState?) { Log.w(TAG, "onIceGatheringChange: $newState") }

    override fun onSignalingChange(newState: PeerConnection.SignalingState?) { Log.w(TAG, "onSignalingChange: $newState") }

    override fun onIceCandidatesRemoved(candidates: Array<out IceCandidate>?) {
        candidates?.let { onIceCandidatesRemovedCb(it) }
    }

    override fun onRenegotiationNeeded() { Log.w(TAG, "onRenegotiationNeeded") }

    override fun onAddTrack(receiver: RtpReceiver?, streams: Array<out MediaStream>?) { }
}
