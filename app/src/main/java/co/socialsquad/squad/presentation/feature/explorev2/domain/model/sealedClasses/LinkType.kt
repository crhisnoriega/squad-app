package co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

private const val VALUE_OBJECT = "object"
private const val VALUE_CATEGORY = "category"
private const val VALUE_COLLECTION = "collection"

sealed class LinkType(val value: String) : Parcelable {

    companion object {
        fun getByValue(value: String) = when (value) {
            VALUE_OBJECT -> Object
            VALUE_CATEGORY -> Category
            VALUE_COLLECTION -> Collection
            else -> Invalid
        }
    }

    @Parcelize
    object Invalid : LinkType("")

    @Parcelize
    object Object : LinkType(VALUE_OBJECT)

    @Parcelize
    object Category : LinkType(VALUE_CATEGORY)

    @Parcelize
    object Collection : LinkType(VALUE_COLLECTION)
}
