package co.socialsquad.squad.presentation.feature.explorev2.sharing.customview


import android.app.Dialog
import android.graphics.Outline
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_resource_choose_unique_link_type.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class ChooseUniqueLinkSheetDialog(private var title: String, private val button: Button, private val onSelect: (type: String) -> Unit) : BottomSheetDialogFragment() {

    var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null

    override fun getTheme(): Int = R.style.FarmBlocks_BottomSheetDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_resource_choose_unique_link_type, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        }
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTopBordersRadius()
        super.onViewCreated(view, savedInstanceState)
        expandDialog()

        uniqueLinkLayout.setOnClickListener {
            onSelect.invoke("link")
            dismiss()
        }

        formLayout.setOnClickListener {
            onSelect.invoke("form")
            dismiss()
        }

        Glide.with(requireContext()).load(button.icon?.url).into(rankingImg)

        shareIcon.setColorFilter(ColorUtils.parse(ColorUtils.getCompanyColor(view.context)!!), PorterDuff.Mode.ADD)
        optOutIcon.setColorFilter(ColorUtils.parse(ColorUtils.getCompanyColor(view.context)!!), PorterDuff.Mode.ADD)
    }


    override fun onPause() {
        isShowing = false
        super.onPause()
    }

    override fun onResume() {
        isShowing = true
        super.onResume()
        opportName?.text = title
        try {
            var boldWord = "Indicar"

            button.actions?.link?.text?.let {
                if (it.contains(boldWord)) {
                    linkLabelBold.text = boldWord
                    linkLabelNormal.text = it.replace(boldWord, "").replace("<b>", "").replace("</b>", "")
                } else {
                    linkLabelBold.text = it
                    linkLabelNormal.isVisible = false
                }
            }

            button.actions?.form?.text?.let {
                if (it.contains(boldWord)) {
                    formLabelBold.text = boldWord
                    formLabelNormal.text = it.replace(boldWord, "").replace("<b>", "").replace("</b>", "")
                } else {
                    formLabelBold.text = it
                    formLabelNormal.isVisible = false
                }
            }
        } catch (e: Exception) {
        }
    }


    override fun dismiss() {
        isShowing = false
        super.dismiss()
    }

    override fun onDestroy() {
        isShowing = false
        super.onDestroy()
    }

    private fun setTopBordersRadius() {
        val radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)
        val container = view as ViewGroup

        container.apply {
            clipToOutline = true
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    if (view != null) outline?.setRoundRect(0, 0, view.width, (view.height + 300).toInt(), radius)
                }
            }
        }
    }

    fun expandDialog() {
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(300)
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            super.show(manager, tag)
        }
    }

    companion object {
        var isShowing = false
    }

}