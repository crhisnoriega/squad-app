package co.socialsquad.squad.presentation.feature.resources.di

import co.socialsquad.squad.presentation.feature.resources.viewmodel.ResourcesViewModel
import co.socialsquad.squad.presentation.feature.tool.ToolViewModel
import co.socialsquad.squad.presentation.feature.tool.submission.OpportunitySubmissionListActivity
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ResourceModule {
    val instance = module {
        viewModel { ResourcesViewModel() }
    }
}
