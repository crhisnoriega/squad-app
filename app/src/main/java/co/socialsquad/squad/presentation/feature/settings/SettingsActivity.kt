package co.socialsquad.squad.presentation.feature.settings

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.DividerViewHolder
import co.socialsquad.squad.presentation.custom.ITEM_DIVIDER
import co.socialsquad.squad.presentation.custom.InnerDivider
import co.socialsquad.squad.presentation.custom.ItemDivider
import co.socialsquad.squad.presentation.custom.OuterDivider
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.feature.settings.about.SettingsAboutActivity
import co.socialsquad.squad.presentation.feature.settings.password.SettingsPasswordActivity
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ItemViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ItemViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_settings.*
import javax.inject.Inject

class SettingsActivity : BaseDaggerActivity(), SettingsView {

    @Inject
    lateinit var settingsPresenter: SettingsPresenter

    private var createLoadingOverlay: AlertDialog? = null

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setupToolbar()
        setupList()
        setupOnClickListeners()
    }

    private fun setupToolbar() {
        setSupportActionBar(tSettings)
        title = getString(R.string.settings_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupList() {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is ItemViewModel -> ITEM_VIEW_MODEL_ID
                is ItemDivider -> ITEM_DIVIDER
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                ITEM_VIEW_MODEL_ID -> ItemViewHolder(view, onSettingClicked)
                ITEM_DIVIDER -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        }

        with(rvOptions) {
            layoutManager = LinearLayoutManager(context)
            adapter = RecyclerViewAdapter(viewHolderFactory).apply {
                addItems(createSettingsItems())
            }
            setHasFixedSize(true)
        }
    }

    private fun createSettingsItems(): List<ViewModel> {
        return listOf(
            HeaderViewModel(getString(R.string.settings_invite_header)),
            InnerDivider,
            SettingsViewModel(
                getString(R.string.settings_item_invite),
                {
                    settingsPresenter.onShareAppDialogButtonClicked(this)
                }
            ),
            OuterDivider(),
            HeaderViewModel(getString(R.string.settings_security_header)),
            InnerDivider,
            SettingsViewModel(
                getString(R.string.settings_item_password),
                {
                    startActivity(Intent(this, SettingsPasswordActivity::class.java))
                    settingsPresenter.onSettingClicked()
                }
            ),
            OuterDivider(),
            // TODO uncomment when enable blocked accounts
            /*HeaderViewModel(getString(R.string.settings_privacy_header)),
            InnerDivider,
            SettingsViewModel(
                getString(R.string.settings_item_privacy), {
                Toast.makeText(this, "Em desenvolvimento...", Toast.LENGTH_SHORT).show()
            }),
            OuterDivider,
            */
            HeaderViewModel(getString(R.string.settings_about_header)),
            InnerDivider,
            SettingsViewModel(
                getString(R.string.settings_item_about),
                {
                    startActivity(Intent(this, SettingsAboutActivity::class.java))
                }
            ),
            OuterDivider()
        )
    }

    private val onSettingClicked = object : ViewHolder.Listener<SettingsViewModel> {
        override fun onClick(viewModel: SettingsViewModel) {
            viewModel.onClick()
        }
    }

    private fun setupOnClickListeners() {
        bLogout.setOnClickListener {
            settingsPresenter.onLogoutClicked(this@SettingsActivity)
        }
    }

    override fun showWalkthroughAfterLogout() {
        val intent = Intent(this, OnboardingActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        ActivityCompat.finishAfterTransition(this)
    }

    override fun showLoadingDialog() {
        createLoadingOverlay = ViewUtils.createLoadingOverlay(this)
        createLoadingOverlay?.show()
    }

    override fun hideLoadingDialog() {
        createLoadingOverlay?.dismiss()
        createLoadingOverlay = null
    }
}
