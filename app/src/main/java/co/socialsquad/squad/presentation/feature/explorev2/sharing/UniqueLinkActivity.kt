package co.socialsquad.squad.presentation.feature.explorev2.sharing

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.HapticFeedbackConstants
import android.view.MenuItem
import android.view.animation.AnimationUtils
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.UniqueLink
import co.socialsquad.squad.presentation.feature.explorev2.sharing.customview.FacebookSharingTypeBottomSheetDialog
import co.socialsquad.squad.presentation.util.extra
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import kotlinx.android.synthetic.main.activity_explorer_unique_link.*
import kotlinx.android.synthetic.main.activity_ranking_terms.closeArea
import kotlinx.android.synthetic.main.activity_ranking_terms.toolbar
import org.koin.core.module.Module


class UniqueLinkActivity : BaseActivity() {

    companion object {
        fun start(context: Context, link: UniqueLink) {
            context.startActivity(Intent(context, UniqueLinkActivity::class.java).apply {
                putExtra("link", link)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_explorer_unique_link

    private val link by extra<UniqueLink>("link")
    private var isShowingShare = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()

        linkShare.text = link.url?.replace("https://", "")?.replace("http://", "")
    }

    override fun onResume() {
        super.onResume()
        isShowingShare = false
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""

        textTitle.text = link.title
        textSubTitle.text = link.description

        btnWhatsapp.setOnClickListener {
            openWhatsApp()
        }

        btnFacebook.setOnClickListener {
            var dialog = FacebookSharingTypeBottomSheetDialog() {
                when (it) {
                    "feed" -> openFacebook()
                    "message" -> openMessenger()
                }
            }
            dialog.show(supportFragmentManager, "facebook")
        }

        btnEmail.setOnClickListener {
            sendEmail()
        }

        btnShareAll.setOnClickListener {
            shareText()
        }

        closeArea.setOnClickListener {
            finish()
        }

        linkButton.setOnClickListener {
            linkButton.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING)

            linkShare.text = "Link Copiado"

            copyToClipboard()

            Handler().postDelayed({
                var animation = AnimationUtils.loadAnimation(this, R.anim.fadein_text);
                linkShare.text = link.url?.replace("https://", "")?.replace("http://", "")
                linkShare.startAnimation(animation)
            }, 2500)

            true
        }

    }

    private fun copyToClipboard() {
        val clipboard: ClipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", "${link.url}")
        clipboard.setPrimaryClip(clip)
    }

    private fun sendEmail() {
        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "message/rfc822"
        sendIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(""))
        sendIntent.setPackage("com.google.android.gm")
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, link.title)
        sendIntent.putExtra(Intent.EXTRA_TEXT, "${link.description} ${link.url}")
        startActivity(sendIntent)
    }

    private fun openFacebook() {
        var shareDialog = ShareDialog(this);
        val content = ShareLinkContent.Builder()
                .setQuote("${link.description} ${link.url}")
                .build()

        shareDialog.show(content);
    }

    private fun openMessenger() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent
                .putExtra(Intent.EXTRA_TEXT,
                        link.url)
        sendIntent.type = "text/plain"
        sendIntent.setPackage("com.facebook.orca")
        try {
            startActivity(sendIntent)
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun openWhatsApp() {
        val url = "https://api.whatsapp.com/send?text=${link.description} ${link.url}"

        val i = Intent(Intent.ACTION_VIEW)
        i.setPackage("com.whatsapp")

        i.data = Uri.parse(url)
        try {
            startActivity(i)
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun shareText() {
        if (isShowingShare.not()) {

            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "${link.description} ${link.url}")
            startActivity(Intent.createChooser(sharingIntent, "Sharing Option"))

            isShowingShare = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}