package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.video.VideoManager
import kotlinx.android.synthetic.main.activity_qualification_requirement.*
import javax.inject.Inject

class QualificationRequirementActivity : BaseDaggerActivity(), QualificationRequirementView {
    companion object {
        const val EXTRA_REQUIREMENT_VIEW_MODEL = "EXTRA_REQUIREMENT_VIEW_MODEL"
    }

    private lateinit var adapter: RecyclerViewAdapter

    @Inject
    lateinit var presenter: QualificationRequirementPresenter

    @Inject
    lateinit var loginRepository: LoginRepository

    private val color by lazy { loginRepository.squadColor }

    override fun onPause() {
        VideoManager.hide()
        super.onPause()
    }

    override fun onDestroy() {
        VideoManager.release()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualification_requirement)
        setupList()
        presenter.onCreate(intent)
    }

    override fun setupToolbar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = title
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is QualificationRequirementTipListItemViewModel -> R.layout.view_qualification_requirement_tip_list_item
                is QualificationRequirementHeaderViewModel -> R.layout.view_qualification_requirement_header
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                R.layout.view_qualification_requirement_tip_list_item -> QualificationRequirementTipListItemViewHolder(view, color)
                R.layout.view_qualification_requirement_header -> QualificationRequirementHeaderViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rv.apply {
            adapter = this@QualificationRequirementActivity.adapter
            layoutManager = LinearLayoutManager(this@QualificationRequirementActivity)
            setHasFixedSize(true)
            setItemViewCacheSize(0)
        }
    }

    override fun setItems(items: List<ViewModel>) {
        adapter.setItems(items)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun startLoading() {
        adapter.startLoading()
    }

    override fun stopLoading() {
        adapter.stopLoading()
    }
}
