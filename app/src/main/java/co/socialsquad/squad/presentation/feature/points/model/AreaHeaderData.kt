package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class AreaHeaderData(
        @SerializedName("criteria_quantity") val criteria_quantity: String?,
        @SerializedName("user_points") val user_points: String?,
        var first: Boolean = false
) : Parcelable