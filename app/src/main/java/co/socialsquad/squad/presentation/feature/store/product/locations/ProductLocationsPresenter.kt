package co.socialsquad.squad.presentation.feature.store.product.locations

import android.content.Intent
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_MAP
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_DISTRIBUTOR_PHONE
import co.socialsquad.squad.presentation.feature.store.ProductLocationListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import javax.inject.Inject

const val EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL = "EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL"

class ProductLocationsPresenter @Inject constructor(
    private val productLocationsView: ProductLocationsView,
    storeInteractor: StoreInteractor,
    private val tagWorker: TagWorker
) {
    private val color = storeInteractor.getColor()

    fun onCreate(intent: Intent) {
        if (intent.hasExtra(EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL)) {
            val locationList = intent.getSerializableExtra(EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL) as ProductLocationListViewModel
            productLocationsView.addLocations(locationList.list, color)
        }
    }

    fun onPhoneClicked() {
        tagWorker.tagEvent(TAG_TAP_DISTRIBUTOR_PHONE)
    }

    fun onResume() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_MAP)
    }
}
