package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ProductLocationNetworkErrorViewModel

class ProductLocationNetworkErrorViewHolder(itemView: View, val listener: Listener<ProductLocationNetworkErrorViewModel>) : ViewHolder<ProductLocationNetworkErrorViewModel>(itemView) {
    override fun bind(viewModel: ProductLocationNetworkErrorViewModel) {
        itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    override fun recycle() {}
}
