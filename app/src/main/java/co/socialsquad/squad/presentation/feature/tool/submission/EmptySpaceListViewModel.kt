package co.socialsquad.squad.presentation.feature.tool.submission

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder

class EmptySpaceListViewModel(itemView: View) : ViewHolder<EmptySpaceListModel>(itemView) {
    override fun bind(viewModel: EmptySpaceListModel) {

    }

    override fun recycle() {

    }
}