package co.socialsquad.squad.presentation.feature.live.creator

import android.content.DialogInterface
import android.content.Intent
import android.hardware.Camera
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.presentation.custom.LiveChronometerBadge
import co.socialsquad.squad.presentation.feature.live.LiveAdapter
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.clearFlagKeepScreenOn
import co.socialsquad.squad.presentation.util.setFlagKeepScreenOn
import com.pedro.encoder.input.video.CameraOpenException
import com.pedro.rtplibrary.rtmp.RtmpCamera1
import io.reactivex.annotations.Nullable
import kotlinx.android.synthetic.main.activity_live_creator.*
import net.ossrs.rtmp.ConnectCheckerRtmp
import net.ossrs.rtmp.SrsFlvMuxer
import javax.inject.Inject

class LiveCreatorActivity : BaseDaggerActivity(), LiveCreatorView, ConnectCheckerRtmp, SurfaceHolder.Callback {

    companion object {
        const val EXTRA_LIVE_CREATE_REQUEST = "EXTRA_LIVE_CREATE_REQUEST"
        const val EXTRA_PK = "EXTRA_PK"
        const val EXTRA_RTMP_URL = "EXTRA_RTMP_URL"
        const val EXTRA_COMMENT_QUEUE = "EXTRA_COMMENT_QUEUE"
        const val EXTRA_STARTED_AT = "EXTRA_STARTED_AT"
        const val EXTRA_LIVE_START_SCHEDULED = "EXTRA_LIVE_START_SCHEDULED"
    }

    private inner class RtmpConnectionTest {
        private var srsFlvMuxer: SrsFlvMuxer? = null
        private val handler = Handler()
        private var isConnectionAvailable = false

        fun start(url: String, onSuccess: () -> Unit) {
            srsFlvMuxer = srsFlvMuxer ?: SrsFlvMuxer(object : ConnectCheckerRtmp {
                override fun onAuthSuccessRtmp() {}
                override fun onAuthErrorRtmp() {}
                override fun onConnectionSuccessRtmp() {
                    handler.removeCallbacksAndMessages(null)
                    srsFlvMuxer?.stop()
                    isConnectionAvailable = true
                }

                override fun onConnectionFailedRtmp(reason: String) {
                    handler.apply {
                        removeCallbacksAndMessages(null)
                        postDelayed({ srsFlvMuxer?.start(url) }, 1000)
                    }
                }

                override fun onDisconnectRtmp() {
                    if (isConnectionAvailable) runOnUiThread { onSuccess() }
                }
            })

            handler.postDelayed({ srsFlvMuxer?.start(url) }, 3000)
        }

        fun stop() {
            handler.removeCallbacksAndMessages(null)
            srsFlvMuxer?.stop()
            srsFlvMuxer = null
            isConnectionAvailable = false
        }
    }

    private class RtmpCamera(surfaceView: SurfaceView?, connectChecker: ConnectCheckerRtmp?) : RtmpCamera1(surfaceView, connectChecker) {
        fun switchLantern() = with(cameraManager) { if (isLanternEnable) disableLantern() else enableLantern() }
    }

    @Inject
    lateinit var liveCreatorPresenter: LiveCreatorPresenter

    private var optionsDialog: LiveCreatorOptionsDialog? = null
    private var confirmationDialog: LiveCreatorConfirmationDialog? = null

    private val confirmationDialogListener = object : LiveCreatorConfirmationDialog.Listener {
        override fun onYesClicked() {
            liveCreatorPresenter.onDeleteClicked()
        }
    }

    private val optionsDialogListener = object : LiveCreatorOptionsDialog.Listener {
        override fun onPostClicked() {
            liveCreatorPresenter.onPostClicked()
        }

        override fun onDeleteClicked() {
            confirmationDialog = LiveCreatorConfirmationDialog(this@LiveCreatorActivity, confirmationDialogListener)
            if (!isFinishing) confirmationDialog?.show()
        }
    }

    private lateinit var svVideo: SurfaceView
    private lateinit var llMessage: LinearLayout
    private lateinit var tvMessage: TextView
    private lateinit var llPreview: LinearLayout
    private lateinit var lcbBadge: LiveChronometerBadge
    private lateinit var llParticipants: LinearLayout
    private lateinit var tvParticipantCounter: TextView
    private lateinit var rvQueue: RecyclerView
    private lateinit var bCamera: ImageButton
    private lateinit var bFlash: ImageButton
    private lateinit var bMicrophone: ImageButton
    private lateinit var bComments: ImageButton
    private lateinit var bClose: Button
    private lateinit var llContainerEnd: LinearLayout
    private lateinit var rlControls: RelativeLayout
    private lateinit var llButtons: LinearLayout
    private lateinit var tvEndTitle: TextView
    private lateinit var tvCountdown: TextView
    private lateinit var gestureDetector: GestureDetectorCompat

    private var rtmpCamera: RtmpCamera? = null
    private var liveAdapter: LiveAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var cameraSize: Camera.Size? = null
    private var loading: AlertDialog? = null
    private var isFrontCamera = true
    private var isAudioEnabled = true
    private var hasConnectionFailed = false
    private var isFlashAvailable = false

    private val rtmpConnectionTest = RtmpConnectionTest()

    override fun onResume() {
        super.onResume()
        enableFadingEdges()
        prepareCamera()
        liveCreatorPresenter.onResume()
    }

    override fun onPause() {
        rtmpCamera?.apply { if (isStreaming) stopStream() }
        lcbBadge.stop()
        super.onPause()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) window.decorView.systemUiVisibility = (
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            )
    }

    override fun onDestroy() {
        stopCamera()
        liveCreatorPresenter.onDestroy()
        confirmationDialog?.dismiss()
        optionsDialog?.dismiss()
        rtmpConnectionTest.stop()
        super.onDestroy()
    }

    override fun onBackPressed() {
        showCloseConfirmationDialog()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_creator)

        svVideo = live_creator_sv_video
        llMessage = live_creator_ll_message
        tvMessage = live_creator_tv_message
        llPreview = live_creator_ll_preview
        llParticipants = live_creator_ll_participants
        tvParticipantCounter = live_creator_tv_participant_counter
        lcbBadge = live_creator_lcb_badge
        rvQueue = live_creator_rv_queue
        bCamera = live_creator_b_camera
        bFlash = live_creator_b_flash
        bMicrophone = live_creator_b_mute
        bComments = live_creator_b_comments
        bClose = live_creator_b_close
        llContainerEnd = live_creator_ll_container_end
        rlControls = live_creator_rl_controls
        llButtons = live_creator_ll_buttons
        tvEndTitle = live_creator_tv_end_title
        tvCountdown = live_creator_tv_countdown

        liveCreatorPresenter.onCreate(intent)

        setupList()
        setupOnClickListeners()
        setupCamera()
        setupPreviewBadge()
        setupGestureListeners()
    }

    private fun setupList() {
        liveAdapter = LiveAdapter(this, layoutInflater)
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager?.stackFromEnd = true
        with(rvQueue) {
            adapter = liveAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(5)
            setHasFixedSize(true)
        }
    }

    private fun setupOnClickListeners() {
        bCamera.setOnClickListener {
            switchCamera()
        }

        var isFlashEnabled = false
        bFlash.setOnClickListener {
            rtmpCamera?.switchLantern()
            val drawableResId = if (isFlashEnabled) R.drawable.ic_live_flash_off else R.drawable.ic_live_flash_on
            bFlash.setImageResource(drawableResId)
            isFlashEnabled = !isFlashEnabled
        }

        bMicrophone.setOnClickListener {
            rtmpCamera?.apply {
                if (isAudioMuted) enableAudio() else disableAudio()
                val drawableResId = if (isAudioMuted) R.drawable.ic_live_microphone_off else R.drawable.ic_live_microphone_on
                bMicrophone.setImageResource(drawableResId)
            }
        }

        bComments.setOnClickListener {
            val isVisible = rvQueue.visibility == View.VISIBLE
            rvQueue.visibility = if (isVisible) View.GONE else View.VISIBLE
            val drawableResId = if (isVisible) R.drawable.ic_live_comments_off else R.drawable.ic_live_comments_on
            bComments.setImageResource(drawableResId)
        }

        bClose.setOnClickListener {
            liveCreatorPresenter.onCloseClicked()
        }
    }

    private fun setupGestureListeners() {
        gestureDetector = GestureDetectorCompat(this, object : GestureDetector.SimpleOnGestureListener() {})
        gestureDetector.setOnDoubleTapListener(object : GestureDetector.OnDoubleTapListener {
            override fun onDoubleTap(e: MotionEvent?): Boolean {
                if (e != null) {
                    switchCamera()
                    return true
                }
                return false
            }

            override fun onDoubleTapEvent(e: MotionEvent?) = false

            override fun onSingleTapConfirmed(e: MotionEvent?) = false
        })
    }

    private fun switchCamera() {
        bCamera.isEnabled = false
        if (isFlashAvailable) {
            bFlash.isEnabled = false
            bFlash.visibility = View.GONE
            bFlash.setImageResource(R.drawable.ic_live_flash_off)
        } else {
            bFlash.isEnabled = true
            bFlash.visibility = View.VISIBLE
        }
        isFlashAvailable = !isFlashAvailable

        isFrontCamera = !isFrontCamera

        try {
            rtmpCamera?.switchCamera()
        } catch (e: Throwable) {
            (e as? CameraOpenException)?.printStackTrace()
        }

        Handler().postDelayed({ bCamera.isEnabled = true }, 1000)
    }

    private fun setupCamera() {
        rtmpCamera = RtmpCamera(svVideo, this)
        svVideo.holder.addCallback(this)
    }

    private fun setupPreviewBadge() {
        llPreview.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in_out_infinite))
    }

    private fun showOptionsDialog() {
        optionsDialog = LiveCreatorOptionsDialog(this, optionsDialogListener)
        if (!isFinishing) optionsDialog?.show()
    }

    private fun stopCamera() {
        rtmpCamera?.apply {
            if (isStreaming) stopStream()
            stopPreview()
        }
    }

    override fun showCloseConfirmationDialog() {
        ViewUtils.showDialog(
            this,
            getString(R.string.live_creator_close_dialog_message),
            getString(R.string.live_creator_close_dialog_button_yes),
            DialogInterface.OnClickListener { _, _ -> liveCreatorPresenter.onCloseConfirmationClicked() },
            getString(R.string.live_creator_close_dialog_button_no)
        )
    }

    override fun showStreamingState(time: Long) {
        llMessage.visibility = View.GONE

        llPreview.clearAnimation()
        llPreview.visibility = View.GONE

        lcbBadge.visibility = View.VISIBLE
        lcbBadge.start(time)
    }

    override fun showConnectingState() {
        llMessage.visibility = View.VISIBLE
        tvMessage.text = getString(R.string.live_creator_connecting)

        llPreview.clearAnimation()
        llPreview.visibility = View.GONE

        lcbBadge.visibility = View.VISIBLE
    }

    override fun showFinishedState() {
        rtmpConnectionTest.stop()
        lcbBadge.stop()
        stopCamera()

        showOptionsDialog()

        tvEndTitle.text = getString(R.string.live_creator_end_title, lcbBadge.text)

        rlControls.visibility = View.GONE
        llButtons.visibility = View.GONE
        rvQueue.visibility = View.GONE
        llMessage.visibility = View.GONE
        llContainerEnd.visibility = View.VISIBLE
    }

    override fun startStream(url: String) {
        rtmpConnectionTest.start(url) { showCountdown { rtmpCamera?.startStream(url) } }
    }

    private fun prepareCamera() {
        cameraSize?.let {
            rtmpCamera?.apply {
                val isVideoPrepared = prepareVideo(it.width, it.height, 30, 500 * 1024, false, 2, 90)
                val isAudioPrepared = prepareAudio(128 * 1024, 44100, true, false, false)
                if (isVideoPrepared && isAudioPrepared) {
                    if (!isAudioEnabled) disableAudio()
                    liveCreatorPresenter.onCameraPrepared(it.height, it.width)
                } else {
                    ViewUtils.showDialog(this@LiveCreatorActivity, getString(R.string.live_creator_error_failed_to_prepare_camera), DialogInterface.OnDismissListener { finish() })
                }
            }
        }
    }

    override fun addQueueItem(queueItem: QueueItem) {
        liveAdapter?.addQueueItem(queueItem)
        keepFixedToBottom()
        enableFadingEdges()
    }

    private fun keepFixedToBottom() {
        linearLayoutManager?.let { linearLayoutManager ->
            liveAdapter?.let { adapter ->
                val recentlyAddedItemPosition = linearLayoutManager.findLastVisibleItemPosition() + 1
                val lastItemPosition = adapter.itemCount - 1
                val isFirstItemVisible = recentlyAddedItemPosition == lastItemPosition
                if (isFirstItemVisible) rvQueue.scrollToPosition(lastItemPosition)
            }
        }
    }

    private fun enableFadingEdges() {
        if (rvQueue.childCount == 0) return

        val itemHeight = resources.getDimensionPixelSize(R.dimen.live_queue_item_font_size) +
            2 * resources.getDimensionPixelSize(R.dimen.live_queue_item_vertical_padding)
        val isScrollingPossible = rvQueue.computeVerticalScrollRange() > rvQueue.height - itemHeight
        if (isScrollingPossible) rvQueue.isVerticalFadingEdgeEnabled = true

        val fadingEdgeLength = resources.getDimensionPixelSize(R.dimen.live_queue_fading_edge_length)
        rvQueue.setFadingEdgeLength(fadingEdgeLength)
    }

    override fun close() {
        if (isTaskRoot) startActivity(Intent(this, NavigationActivity::class.java))
        finish()
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(resId), onDismissListener)
    }

    override fun keepScreenOn(enabled: Boolean) {
        if (enabled) setFlagKeepScreenOn()
        else clearFlagKeepScreenOn()
    }

    override fun showLoading() {
        loading = ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun setParticipantCount(count: Int) {
        if (count > 0) {
            llParticipants.visibility = View.VISIBLE
            tvParticipantCounter.text = count.toString()
        } else if (count == 0) {
            llParticipants.visibility = View.GONE
        }
    }

    private fun showCountdown(onFinished: () -> Unit) {
        llMessage.visibility = View.GONE

        val handler = Handler()
        fun countFrom(number: Int) {
            handler.removeCallbacksAndMessages(null)
            if (number > 0) {
                tvCountdown.apply {
                    clearAnimation()
                    text = "$number"
                    startAnimation(AnimationUtils.loadAnimation(this@LiveCreatorActivity, R.anim.fade_out_shrink))
                }
                handler.postDelayed({ countFrom(number - 1) }, 1000)
            } else {
                tvCountdown.visibility = View.GONE
                onFinished()
            }
        }

        countFrom(3)
    }

    override fun onConnectionSuccessRtmp() {
        Log.d("RtmpConnection", "Connection success")
        hasConnectionFailed = false
        runOnUiThread { liveCreatorPresenter.onConnected() }
    }

    override fun onConnectionFailedRtmp(reason: String) {
        Log.d("RtmpConnection", "Connection failed - $reason")
        rtmpCamera?.apply {
            if (isStreaming && !hasConnectionFailed) {
                hasConnectionFailed = true
                lcbBadge.stop()
                stopStream()
                prepareCamera()
            }
        }
        runOnUiThread { liveCreatorPresenter.onConnectionFailed() }
    }

    override fun onDisconnectRtmp() {
        Log.d("RtmpConnection", "Disconnected")
    }

    override fun onAuthErrorRtmp() {
        Log.d("RtmpConnection", "Auth error")
    }

    override fun onAuthSuccessRtmp() {
        Log.d("RtmpConnection", "Auth success")
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        rtmpCamera?.apply {
            cameraSize = resolutionsFront.filter { it.width < 800 }.sortedByDescending { it.width }.first()
            cameraSize?.let {
                val widthAdjustment = it.height * window.decorView.height / it.width
                svVideo.layoutParams.width = widthAdjustment
                prepareCamera()
            }
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        cameraSize?.let {
            val cameraFacing =
                if (isFrontCamera) Camera.CameraInfo.CAMERA_FACING_FRONT
                else Camera.CameraInfo.CAMERA_FACING_BACK
            rtmpCamera?.startPreview(cameraFacing, it.width, it.height)
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        stopCamera()
    }
}
