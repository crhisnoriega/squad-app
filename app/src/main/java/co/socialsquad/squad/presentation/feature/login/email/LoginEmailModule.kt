package co.socialsquad.squad.presentation.feature.login.email

import dagger.Binds
import dagger.Module

@Module
abstract class LoginEmailModule {

    @Binds
    abstract fun view(view: LoginEmailActivity): LoginEmailView
}
