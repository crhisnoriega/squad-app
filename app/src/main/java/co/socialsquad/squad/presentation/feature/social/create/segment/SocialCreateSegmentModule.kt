package co.socialsquad.squad.presentation.feature.social.create.segment

import dagger.Binds
import dagger.Module

@Module
abstract class SocialCreateSegmentModule {
    @Binds
    abstract fun view(view: SocialCreateSegmentActivity): SocialCreateSegmentView
}
