package co.socialsquad.squad.presentation.util

import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.widget.EditText

object MaskUtils {

    private val PLACEHOLDER_CHAR = '#'

    @Retention(AnnotationRetention.RUNTIME)
    annotation class Mask {
        companion object {
            val CNPJ = "##.###.###/####-##"
            val CPF = "###.###.###-##"
            val RG = "##.###.###-#"
            val PHONE = "(##) ####-####"
            val MOBILE = "(##) #####-####"
            val CEP = "#####-###"
        }
    }

    @Retention(AnnotationRetention.RUNTIME)
    annotation class Filter {
        companion object {
            val LETTERS = "[^A-Źa-ź]"
            val NUMBERS_DASH = "[^0-9-]"
            val NUMBERS_DASH_DOT = "[^0-9.-]"
            val NUMBERS_PARENTHESIS_DASH_SPACE = "[^0-9() -]"
            val NUMBERS_UPPERCASE_LETTERS_NO_ACCENTS = "[^A-Z0-9]"
        }
    }

    fun applyMask(editText: EditText, @Mask mask: String) {
        editText.addTextChangedListener(object : TextWatcher {

            private var isRunning = false
            private var isDeleting = false

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                isDeleting = count > after
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(editable: Editable) {
                if (isRunning || isDeleting) return

                val editableLength = editable.length
                val maskLength = mask.length
                if (editableLength > maskLength) {
                    editable.delete(editableLength - 1, editableLength)
                }

                isRunning = true

                val filters = editable.filters
                editable.filters = arrayOfNulls(0)

                var i = 0
                var notSymbolIndex = 0
                val sb = StringBuilder()
                while (i < maskLength && notSymbolIndex < editableLength) {
                    if (mask[i] == editable[notSymbolIndex] || mask[i] == '#') {
                        sb.append(editable[notSymbolIndex])
                        notSymbolIndex++
                    } else {
                        sb.append(mask[i])
                    }
                    i++
                }

                editable.clear()
                editable.append(sb)

                editable.filters = filters

                isRunning = false
            }
        })
    }

    fun mask(unmaskedText: String, @Mask mask: String): String {
        val maskedText = StringBuilder()
        var maskIndex = 0
        var unmaskedTextIndex = 0
        while (maskIndex < mask.length && unmaskedTextIndex != unmaskedText.length) {
            maskedText.append(if (mask[maskIndex] == PLACEHOLDER_CHAR) unmaskedText[unmaskedTextIndex++] else mask[maskIndex])
            maskIndex++
        }
        return maskedText.toString()
    }

    fun unmask(maskedText: String?, @Mask mask: String): String {
        val invalidChars = mask.replace("#".toRegex(), "")
        return maskedText?.replace("[$invalidChars]".toRegex(), "") ?: ""
    }

    fun applyFilter(editText: EditText, @Filter filter: String) {
        editText.filters = arrayOf(object : InputFilter {
            override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence {
                source?.let { src ->
                    if (src == "" || !src.toString().matches(filter.toRegex())) {
                        return src
                    }
                }
                return ""
            }
        })
    }
}
