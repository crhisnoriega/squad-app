package co.socialsquad.squad.presentation.feature.chat.chatlist

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.ChatSearchRequest
import co.socialsquad.squad.data.entity.ChatSection
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.entity.database.ChatRoomModel
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.repository.ChatRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_CHAT_LIST
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomViewModel
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val CHAT_LIST_MESSAGES_HANDLER = "chat_list_handler"

class ChatListPresenter @Inject constructor(
    loginRepository: LoginRepository,
    private val chatRepository: ChatRepository,
    private val chatListView: ChatListView,
    private val tagWorker: TagWorker
) {

    private lateinit var lifecycleOwner: LifecycleOwner
    private var isInSearch: Boolean = false
    private val compositeDisposable = CompositeDisposable()

    private val companyColor = loginRepository.userCompany?.company?.primaryColor
    private var searchDisposable: Disposable? = null

    private lateinit var roomLiveData: LiveData<List<ChatRoomModel>>
    private val liveDataObserver = Observer<List<ChatRoomModel>> { chatList ->
        val sections = chatList?.sortedBy { it.sectionOrder }?.groupBy { it.section }
        val sectionViewModels = mutableListOf<ViewModel>()
        sections?.forEach { section ->
            if (section.value.isNotEmpty()) {
                if (section.key == "contacts") {
                    sectionViewModels.add(HeaderViewModel(titleResId = R.string.chat_list_header_network))
                } else {
                    sectionViewModels.add(HeaderViewModel(section.key))
                }
                val rooms = section.value
                    .map { chatRoomModel -> ChatRoomViewModel(chatRoomModel, companyColor) }
                sectionViewModels.addAll(rooms)
            }
        }
        if (!isInSearch) {
            chatListView.setItems(sectionViewModels)
        }
    }

    fun onCreate(owner: LifecycleOwner) {
        lifecycleOwner = owner
        getChatList()
    }

    fun onResume() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_CHAT_LIST)
    }

    private fun getChatList() {
        roomLiveData = chatRepository.getLiveDataRooms()
        roomLiveData.observe(lifecycleOwner, liveDataObserver)
    }

    private fun mapResults(sections: List<ChatSection>?, contacts: List<UserCompany>?): Pair<MutableList<ViewModel>, MutableList<ViewModel>> {
        val sectionViewModels = mutableListOf<ViewModel>()
        val contactViewModels = mutableListOf<ViewModel>()

        sections?.forEach {
            if (it.chats.isNotEmpty()) {
                sectionViewModels.add(HeaderViewModel(it.title))
                it.chats.forEach {
                    sectionViewModels.add(
                        ChatRoomViewModel(
                            it.chatWith.pk,
                            companyColor,
                            it.channelUrl,
                            it.chatWith.user.avatar,
                            it.chatWith.fullName,
                            it.chatWith.qualification,
                            it.chatWith.qualificationBadge,
                            it.chatWith.user.chatId,
                            it.chatWith.canCall(),
                            it.unreadCount
                        )
                    )
                }
                sectionViewModels.add(DividerViewModel)
            }
        }

        contacts?.forEach {
            if (!sectionViewModels.filterIsInstance<ChatRoomViewModel>()
                .any { chatRoom -> chatRoom.pk == it.pk }
            ) {
                contactViewModels.add(
                    ChatRoomViewModel(
                        it.pk,
                        companyColor,
                        "",
                        it.user.avatar,
                        it.fullName,
                        it.qualification,
                        it.qualificationBadge,
                        it.user.chatId
                    )
                )
            }
        }
        return Pair(sectionViewModels, contactViewModels)
    }

    fun onQueryChanged(query: String) {
        searchDisposable?.apply { if (!isDisposed) dispose() }
        if (!query.isBlank()) {
            searchDisposable = Completable.complete()
                .delay(600, TimeUnit.MILLISECONDS)
                .onDefaultSchedulers()
                .subscribe { search(query) }
        } else {
            chatListView.showSearchEmpty()
        }
    }

    private fun search(query: String) {
        roomLiveData.removeObserver(liveDataObserver)
        compositeDisposable.add(
            chatRepository.search(ChatSearchRequest(query))
                .doOnSubscribe {
                    chatListView.setItems(emptyList())
                    chatListView.showLoading()
                }
                .doOnTerminate { chatListView.hideLoading() }
                .map { mapResults(it.sections, it.contacts) }
                .subscribe(
                    {
                        if (it.first.isEmpty() && it.second.isEmpty()) {
                            chatListView.showNoResults(query)
                        } else {
                            updateList(it.first, it.second)
                        }
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    private fun updateList(sections: List<ViewModel>, contacts: List<ViewModel>) {
        val list = mutableListOf<ViewModel>()
        list.addAll(sections)
        if (contacts.isNotEmpty()) {
            list.addAll(listOf(HeaderViewModel(titleResId = R.string.chat_list_header_network)))
            list.addAll(contacts)
        }
        list.add(DividerViewModel)
        chatListView.setItems(list)
    }

    fun onCancelClicked() {
        searchDisposable?.apply { if (!isDisposed) dispose() }
        restoreInitialState()
    }

    private fun restoreInitialState() {
        chatListView.hideSearch()
        roomLiveData.observe(lifecycleOwner, liveDataObserver)
    }

    fun onDestroy() {
        searchDisposable?.dispose()
        compositeDisposable.dispose()
    }
}
