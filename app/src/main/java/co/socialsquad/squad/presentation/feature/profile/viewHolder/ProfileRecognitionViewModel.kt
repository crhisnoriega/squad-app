package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.data.entity.RecognitionData
import co.socialsquad.squad.data.entity.RecognitionProfileModel
import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileRecognitionViewModel(val recognitionProfileModel: RecognitionData?) : ViewModel
