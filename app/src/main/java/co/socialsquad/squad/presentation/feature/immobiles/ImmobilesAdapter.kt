package co.socialsquad.squad.presentation.feature.immobiles

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Immobile
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlin.reflect.KFunction1

class ImmobilesAdapter(val onImmobileClicked: KFunction1<String, Unit>, val context: Context) :
    RecyclerView.Adapter<ImmobilesAdapter.ViewHolder>() {

    private val immobiles = mutableListOf<Immobile>()
    private val glide = Glide.with(context)
    private var coverColor = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.immobiles_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = immobiles.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position == 0) {
            holder.firstSeparator.visibility = View.VISIBLE
        }
        val immobile = immobiles[position]

        holder.value.text = immobile.start_price
        holder.value.setTextColor(Color.parseColor(coverColor))
        holder.settings.text = setupSettings(immobile)
        holder.name.text = immobile.name
        holder.address.text = immobile.address_street.plus(", ").plus(immobile.address_district)
        holder.container.setOnClickListener {
            val gson = Gson()
            val json = gson.toJson(immobile)
            onImmobileClicked(json)
        }

        if (immobile.medias.isNotEmpty()) {
            val media = immobile.medias.find { !it.mime_type.contains("mp4") }
            glide.load(media?.url)
                .crossFade()
                .into(holder.image)
        }
    }

    private fun setupSettings(immobile: Immobile): String {
        var value = ""

        if (!immobile.room_number.isNullOrEmpty()) {
            value = immobile.room_number
        }
        if (!immobile.suite_number.isNullOrEmpty()) {
            value = if (value.isEmpty()) {
                immobile.room_number!!
            } else {
                value + " • " + immobile.suite_number
            }
        }

        if (!immobile.garage_number.isNullOrEmpty()) {
            value = if (value.isEmpty()) {
                immobile.garage_number
            } else {
                value + " • " + immobile.garage_number
            }
        }
        if (!immobile.square_meters.isNullOrEmpty()) {
            value = if (value.isEmpty()) {
                immobile.square_meters
            } else {
                value + " • " + immobile.square_meters
            }
        }

        return value
    }

    fun immobiles(immobiles: List<Immobile>, secondaryColor: String?) {
        secondaryColor?.apply {
            coverColor = this
        }
        this.immobiles.addAll(immobiles)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.imageView)
        val value: TextView = itemView.findViewById(R.id.valueLabel)
        val name: TextView = itemView.findViewById(R.id.name)
        val settings: TextView = itemView.findViewById(R.id.settings)
        val address: TextView = itemView.findViewById(R.id.address)
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
        val firstSeparator: View = itemView.findViewById(R.id.firstSeparator)
    }
}
