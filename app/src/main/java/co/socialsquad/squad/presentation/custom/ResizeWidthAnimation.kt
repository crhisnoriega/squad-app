package co.socialsquad.squad.presentation.custom

import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

class ResizeWidthAnimation(private val view: View, private val targetWidth: Int) : Animation() {
    private val startWidth = view.width

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
        val newWidth = (startWidth + (targetWidth - startWidth) * interpolatedTime).toInt()
        view.layoutParams.width = newWidth
        view.requestLayout()
    }

    override fun willChangeBounds() = true
}
