package co.socialsquad.squad.presentation.feature.revenue.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_revenue_source_listing.view.*

class ItemRevenueSourceDetailViewHolder(
    val itemView: View,
    val callback: (ItemRevenueSourceDetailViewModel) -> Unit
) :
    ViewHolder<ItemRevenueSourceDetailViewModel>(itemView) {
    override fun bind(viewModel: ItemRevenueSourceDetailViewModel) {
        itemView.dividerItemTop.isVisible = viewModel.isFirst.not()
        itemView.mainLayout.setOnClickListener {
            callback.invoke(viewModel)
        }
    }

    override fun recycle() {

    }
}