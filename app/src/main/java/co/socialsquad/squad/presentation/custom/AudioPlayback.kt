package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.audio.AudioManager
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_audio_playback.view.*

private const val PLAY_BUTTON_DRAWABLE_RES_ID = R.drawable.ic_playback_play
private const val PAUSE_BUTTON_DRAWABLE_RES_ID = R.drawable.ic_playback_pause
private const val REPLAY_BUTTON_DRAWABLE_RES_ID = R.drawable.ic_playback_replay
private const val DEFAULT_SKIP_INTERVAL_MS = 30000L

class AudioPlayback(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var audioViewModel: AudioViewModel? = null
    private var onAudioPlayedListener: OnAudioPlayedListener? = null

    private val listener = object : AudioManager.Listener {
        override fun onReady(isPlaying: Boolean) {
            val drawableResId = if (isPlaying) PAUSE_BUTTON_DRAWABLE_RES_ID else PLAY_BUTTON_DRAWABLE_RES_ID
            updatePlayButtonDrawable(drawableResId)
            updateSkipButtonsVisibility(true)
        }

        override fun onBuffering() {
            ivPlayback.setImageResource(0)
            updateSkipButtonsVisibility(true)
        }

        override fun onEnded() {
            audioViewModel?.isFinished = true
            setupPositionAndDuration(0)
            updatePlayButtonDrawable(REPLAY_BUTTON_DRAWABLE_RES_ID)
            updateSkipButtonsVisibility(false)
            AudioManager.removeListener(this)
        }

        override fun onReleased() {
            if (audioViewModel?.isFinished == false) updatePlayButtonDrawable(PLAY_BUTTON_DRAWABLE_RES_ID)
            updateSkipButtonsVisibility(false)
            AudioManager.removeListener(this)
        }

        override fun onPositionChanged(position: Long) {
            if (position > 0) setupPositionAndDuration(position)
        }
    }

    init {
        View.inflate(context, R.layout.view_audio_playback, this)
    }

    fun init(audioViewModel: AudioViewModel, autoPlay: Boolean = false, onAudioPlayedListener: OnAudioPlayedListener? = null) {
        this.audioViewModel = audioViewModel
        this.onAudioPlayedListener = onAudioPlayedListener

        if (audioViewModel.pk == AudioManager.audioViewModel?.pk) {
            this.audioViewModel = AudioManager.audioViewModel
            AudioManager.addListener(listener)
        }

        setupPlayButton(autoPlay)
        setupSkipButtons()
        setupAvatar()
        setupCircleColor()
        setupPositionAndDuration(audioViewModel.position)
    }

    fun recycle() {
        Glide.with(this).clear(ivAvatar)
        AudioManager.removeListener(listener)
    }

    private fun setupPlayButton(autoPlay: Boolean) {
        audioViewModel?.let { model ->
            when {
                model.isFinished -> REPLAY_BUTTON_DRAWABLE_RES_ID
                model.isPlaying -> PAUSE_BUTTON_DRAWABLE_RES_ID
                else -> PLAY_BUTTON_DRAWABLE_RES_ID
            }.apply { updatePlayButtonDrawable(this) }

            ivAvatar.setOnClickListener {
                AudioManager.apply {
                    if (!model.isPrepared) {
                        prepare(context, Uri.parse(model.url), model)
                        addListener(listener)
                        addListener(model.listener)
                        model.isPrepared = true
                        model.listened = true
                        onAudioPlayedListener?.onPlayed(model.pk)
                        setupCircleColor()
                    }

                    toggle()
                }
            }
            if (autoPlay) ivAvatar.performClick()
        }
    }

    private fun setupSkipButtons() {
        audioViewModel?.let {
            updateSkipButtonsVisibility(!it.isFinished)
        }

        ivSkipBack.setOnClickListener { AudioManager.skipBack(DEFAULT_SKIP_INTERVAL_MS) }
        ivSkipForward.setOnClickListener { AudioManager.skipForward(DEFAULT_SKIP_INTERVAL_MS) }

        val interval = (DEFAULT_SKIP_INTERVAL_MS / 1000).toString()
        listOf(tvSkipBack, tvSkipForward).forEach { it.text = interval }
    }

    private fun setupAvatar() {
        audioViewModel?.let {
            Glide.with(this).load(it.avatar)
                .circleCrop()
                .crossFade()
                .into(ivAvatar)
        }
    }

    private fun setupCircleColor() {
        audioViewModel?.let {
            ivCircle.imageTintList = if (!it.listened) it.color?.let { ColorUtils.stateListOf(it) } else null
        }
    }

    private fun setupPositionAndDuration(position: Long) {
        val duration = audioViewModel?.duration?.toDurationString() ?: "00:00"
        tvDuration.text =
            if (audioViewModel?.position == 0L) duration
            else "${position.toDurationString()} / $duration"
    }

    private fun updatePlayButtonDrawable(resId: Int) {
        ivPlayback.setImageResource(resId)
    }

    private fun updateSkipButtonsVisibility(isVisible: Boolean) {
        listOf(ivSkipBack, tvSkipBack, ivSkipForward, tvSkipForward).forEach { it.visibility = if (isVisible) View.VISIBLE else View.GONE }
    }
}

interface OnAudioPlayedListener {
    fun onPlayed(pk: Int)
}
