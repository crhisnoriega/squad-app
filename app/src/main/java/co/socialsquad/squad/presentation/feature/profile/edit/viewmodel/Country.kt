package co.socialsquad.squad.presentation.feature.profile.edit.viewmodel

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.InputSpinner

enum class Country(val code: String, override val titleResId: Int) : InputSpinner.ItemEnum {

    BR("BR", R.string.country_brazil);

    override lateinit var title: String
}
