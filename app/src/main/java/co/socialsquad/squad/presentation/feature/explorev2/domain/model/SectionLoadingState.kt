package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.SectionTemplateTypeAdapter
import co.socialsquad.squad.data.typeAdapter.SectionTypeAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionType
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SectionLoadingState(
    @SerializedName("type") @JsonAdapter(SectionTypeAdapter::class) val type: SectionType,
    @SerializedName("template") @JsonAdapter(SectionTemplateTypeAdapter::class) val template: SectionTemplate
) : Parcelable {
    companion object {
        fun getDefault() = listOf(
            SectionLoadingState(SectionType.Banner, SectionTemplate.getDefault()),
            SectionLoadingState(SectionType.Category, SectionTemplate.getDefault()),
            SectionLoadingState(SectionType.HorizontalCollection, SectionTemplate.getDefault()),
            SectionLoadingState(SectionType.VerticalCollection, SectionTemplate.getDefault())
        )
    }
}
