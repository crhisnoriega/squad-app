package co.socialsquad.squad.presentation.feature.ranking.data

import co.socialsquad.squad.presentation.feature.ranking.model.*
import retrofit2.Response
import retrofit2.http.*

interface RankingAPIV2 {

    @GET("/endpoints/identity/ranking")
    suspend fun fetchRankingList(): RankListResponse

    @GET("/endpoints/identity/ranking/{ranking_id}")
    // @Headers("Accept: application/json", "Content-type:application/json")
    suspend fun fetchRankingDetails(@Path("ranking_id") rankingId: String): RankingItem

    @POST("/endpoints/identity/ranking/{ranking_id}/accept-terms/")
    suspend fun agreeRankingTerms(@Path("ranking_id") rankingId: String): Response<Void>

    @POST("/endpoints/identity/ranking/{ranking_id}/refuse-terms/")
    suspend fun refuseRankingTerms(@Path("ranking_id") rankingId: String): Response<Void>


    @GET("/endpoints/identity/ranking-positions/")
    suspend fun fetchPositions(@Query("ranking") rankId: String): PositionsResponse

    @GET
    suspend fun fetchPositionsUrl(@Url nextUrl: String): PositionsResponse

    @GET("/endpoints/identity/criteria/")
    suspend fun fetchCriterias(@Query("ranking") rank_id: String): CriteriasResponse

    @GET
    suspend fun fetchCriteriasUrl(@Url nextUrl: String): CriteriasResponse

    @GET(" /endpoints/identity/benefits/{benefit_id}")
    suspend fun fetchBenefitaDetails(
        @Path("benefit_id") benefitId: String,
        @Query("ranking") ranking: String,
        @Query("level") level: String
    ): BenefitData


    @GET("/endpoints/ranking/benefit")
    suspend fun fetchBenefits(): BenefitsResponse


    @GET(" /endpoints/identity/criteria/{criteria_id}/")
    suspend fun fetchCriteriaDetails(@Path("criteria_id") criteriaId: String): CriteriaData


    @GET(" /endpoints/identity/criteria/{criteria_id}/")
    suspend fun fetchCriteriaDetailsByArea(
        @Path("criteria_id") criteriaId: String,
        @Query("area") areaId: String
    ): CriteriaData

}
