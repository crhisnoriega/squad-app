package co.socialsquad.squad.presentation.feature.store

import co.socialsquad.squad.R
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import javax.inject.Inject

const val EXTRA_CATEGORY = "EXTRA_CATEGORY"
const val EXTRA_PRODUCT = "EXTRA_PRODUCT"
const val EXTRA_DETAIL_GROUP = "EXTRA_DETAIL_GROUP"

sealed class MediaType : Serializable {
    object Video : MediaType()
    object Image : MediaType()
}

class Media(
    val url: String,
    val thumbnail: String?,
    val type: MediaType
) : Serializable {
    constructor(media: co.socialsquad.squad.data.entity.Media) : this(
        when {
            FileUtils.isImageMedia(media) -> media.url
            FileUtils.isVideoMedia(media) -> media.streamings?.firstOrNull()?.playlistUrl ?: ""
            else -> ""
        },
        when {
            FileUtils.isImageMedia(media) -> null
            FileUtils.isVideoMedia(media) -> media.streamings?.firstOrNull()?.thumbnailUrl
            else -> null
        },
        when {
            FileUtils.isVideoMedia(media) -> MediaType.Video
            else -> MediaType.Image
        }
    )
}

class StorePresenter @Inject constructor(
    private val storeView: StoreView,
    private val storeInteractor: StoreInteractor,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private val color = storeInteractor.getColor()

    fun onViewCreated() {
        tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_PRODUCT_LIST)
        with(storeView) {
            setupToolbar()
            setupList(color)
            setupSwipeRefresh()
        }
        showStore()
    }

    private fun showStore() {
        subscribe()
    }

    fun onRefresh() {
        compositeDisposable.clear()
        subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            storeInteractor.getStore()
                .doOnSubscribe { storeView.showLoading() }
                .doOnTerminate { storeView.hideLoading() }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() }?.apply {
                            storeView.showEmptyState()
                        } ?: kotlin.run {
                            storeView.showStore(it)
                        }
                    },
                    {
                        storeView.showMessage(R.string.store_error_get_store)
                        it.printStackTrace()
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }
}
