package co.socialsquad.squad.presentation.feature.hub.dashboard.charts

import android.graphics.Color
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import com.bumptech.glide.Glide
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.view_chart_list_item.view.*
import java.text.NumberFormat
import java.util.ArrayList
import java.util.Locale

class ChartViewHolder(
    itemView: View,
    private val onLastSevenListener: Listener<ChartViewModel>,
    private val onLastThirtyListener: Listener<ChartViewModel>
) : ViewHolder<ChartViewModel>(itemView) {

    override fun bind(viewModel: ChartViewModel) {
        val description = Description()
        description.text = ""

        var barEntries: List<BarEntry>
        with(viewModel.dataArray) {
            if (viewModel.lastSeven) {
                (this.size > 7).let { barEntries = this.subList(this.lastIndex - 6, this.lastIndex + 1) }
            } else barEntries = this
        }

        val dataSet = BarDataSet(barEntries, viewModel.title)
        try { dataSet.color = Color.parseColor(viewModel.dataColor) } catch (e: Exception) { dataSet.color = Color.BLUE }
        dataSet.setDrawValues(false)

        with(itemView) {
            itemView.bcChart.let {
                it.data = BarData(dataSet)

                it.axisLeft.setStartAtZero(true)
                it.xAxis.isEnabled = false
                it.axisLeft.isEnabled = false
                it.axisRight.isEnabled = false
                it.setTouchEnabled(false)
                it.description = description
                it.legend.isEnabled = false
                it.setDrawBorders(false)
                it.setDrawBarShadow(false)
            }

            tvTitle.text = viewModel.title
            tvTotalValue.text = NumberFormat.getNumberInstance(Locale.getDefault()).format(viewModel.totalPoints).toString()
            tvDescription.text = viewModel.description
            tvSource.text = viewModel.source
            Glide.with(itemView)
                .load(viewModel.sourceImageUrl)
                .into(itemView.ivSource)
        }

        setupOptions(viewModel)
    }

    private fun setupOptions(viewModel: ChartViewModel) {
        val options = object : ArrayList<Int>() {
            init {
                add(R.string.hub_dashboard_last_30)
                add(R.string.hub_dashboard_last_7)
            }
        }

        val socialSpinnerAdapter = SocialSpinnerAdapter(
            itemView.context,
            R.layout.textview_spinner_dropdown_options_item,
            R.layout.textview_spinner_dropdown_options,
            options
        )

        itemView.sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
        itemView.sOptions.adapter = socialSpinnerAdapter

        itemView.sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position) ?: return

                val itemResId = item as Int
                when (itemResId) {
                    R.string.hub_dashboard_last_7 -> onLastSevenListener.onClick(viewModel)
                    R.string.hub_dashboard_last_30 -> onLastThirtyListener.onClick(viewModel)
                }
                itemView.sOptions.setSelection(0)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun recycle() {}
}
