package co.socialsquad.squad.presentation.custom.multimedia.audio

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.util.AttributeSet
import android.view.TextureView
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.media.AudioAttributesCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.audio.AudioFocusWrapper
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import kotlinx.android.synthetic.main.view_multi_media_pager_audio_view.view.*

class AudioView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private var rlVideo: RelativeLayout
    private val sepvVideo: PlayerView
    private val ivThumbnail: ImageView
    // private val ivPlay: ImageView

    // private val ibMute: ImageButton
    private var player: SimpleExoPlayer? = null
    private var audioFocusWrapper: AudioFocusWrapper? = null
    private val mediaCounter: TextView
    private val cardMediaCounter: View

    val video get() = sepvVideo

    var audioEnabled = false

    private var uri: Uri? = null
    private var position: Long = 0L

    companion object {
        private const val AUDIO_ENABLED = 1f
        private const val AUDIO_DISABLED = 0f
    }

    init {
        inflate(context, R.layout.view_multi_media_pager_audio_view, this)

        rlVideo = video_rl_video
        sepvVideo = video_sepv_video
        ivThumbnail = video_iv_thumbnail
        //  ivPlay = video_iv_play
        //ibMute = findViewById(R.id.video_ib_mute_2)
        mediaCounter = findViewById(R.id.mediaCounter)
        cardMediaCounter = findViewById(R.id.cardMediaCounter)

        rlVideo.setOnClickListener {
            play(player?.currentPosition)
        }


        //ibMute.setImageResource(R.drawable.ic_audio_off)

        //ibMute.setOnClickListener {
        //    toogleMute()
        //}

        // ivThumbnail.visibility = View.VISIBLE
        //ivPlay.visibility = View.GONE
        // ibMute.visibility = View.GONE
    }


    fun buildPlayer() {
        val trackSelector = DefaultTrackSelector(context)
        val loadControl = DefaultLoadControl()
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as android.media.AudioManager
        val audioAttributes = AudioAttributesCompat.Builder()
                .setContentType(AudioAttributesCompat.CONTENT_TYPE_MOVIE)
                .setUsage(AudioAttributesCompat.USAGE_MEDIA)
                .build()
        val exoPlayer = SimpleExoPlayer
                .Builder(context)
                .setTrackSelector(trackSelector)
                .setLoadControl(loadControl)
                .build()
        audioFocusWrapper = AudioFocusWrapper(audioAttributes, audioManager, exoPlayer)
        player = audioFocusWrapper!!.player


    }

    fun prepare(uri: Uri) {
        this.uri = uri
        videoPrepare()
        //ibMute.setImageResource(if (audioEnabled) R.drawable.ic_audio_on else R.drawable.ic_audio_off)
    }

    fun pause() {
        videoPause()
        //ivPlay.visibility = View.VISIBLE
        //ibMute.visibility = View.GONE
    }

    fun release() {
        videoRelease()
    }

    fun loadThumbnail(url: String?) {
        url?.let {
            Glide.with(context).load(url).into(ivThumbnail)
        }
    }

    fun toogleMute() {
        if (audioEnabled) {
            videoMute()
        } else {
            videoUnmute()
        }
    }

    fun play(position: Long? = null, muted: Boolean? = null) {
        position?.let {
            this.position = position
        }
        if (!audioFocusWrapper?.playWhenReady!!) {
            videoPlay()
            muted?.let {
                if (it)
                    videoMute()
                else
                    videoUnmute()
            } ?: run {
                videoUnmute()
            }

            //ivPlay.visibility = View.GONE
            //ibMute.visibility = View.VISIBLE
        }
    }

    fun setLabel(label: String?) {

        cardMediaCounter.visibility = View.VISIBLE

        label?.let {
            mediaCounter.text = it
        }

        Handler().postDelayed({
            cardMediaCounter.visibility = View.GONE
        }, 2000)

    }

    private val videoEventListener = object : Player.EventListener {
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_ENDED -> {
                    player?.seekTo(0)
                    player?.playWhenReady = false
                    // ivThumbnail.visibility = View.VISIBLE
                    //ivPlay.visibility = View.VISIBLE
                    //ibMute.visibility = View.GONE
                    if (!muted()) {
                        toogleMute()
                    }
                }
                Player.STATE_READY -> {
                }
                Player.STATE_BUFFERING -> {
                }
            }
        }
    }

    fun videoPrepare() {
        buildPlayer()

        uri?.let {
            videoPreparePlayer(it, context)
            player?.clearVideoSurface()
            player?.setVideoTextureView(sepvVideo.videoSurfaceView as TextureView)
            audioFocusWrapper?.currentPosition?.let { it1 -> player?.seekTo(it1) }

            player?.volume = if (audioEnabled) AUDIO_ENABLED else AUDIO_DISABLED
            sepvVideo.player = player
        }
    }

    private fun videoPreparePlayer(uri: Uri, context: Context) {
        this.uri = uri

        val httpDataSourceFactory = DefaultHttpDataSourceFactory("ua")
        val dataSourceFactory = DefaultDataSourceFactory(context, null, httpDataSourceFactory)

        val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        val mediaItem = MediaItem.Builder().setMimeType(mimeType).setUri(uri).build()
        val mediaSource = if (mimeType?.contains("video") == true) {
            ProgressiveMediaSource
                    .Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
        } else {
            HlsMediaSource.Factory(dataSourceFactory)
                    .setAllowChunklessPreparation(true)
                    .createMediaSource(mediaItem)
        }

        player?.setMediaSource(mediaSource)
        player?.addListener(videoEventListener)
        player?.prepare()
    }

    fun videoRelease() {
        audioFocusWrapper?.release()
    }

    fun videoHide() {
        audioFocusWrapper?.apply {
            playWhenReady = false
        }
    }

    fun videoPlay() {
        audioFocusWrapper?.playWhenReady = true
    }

    fun videoPause() {
        audioFocusWrapper?.playWhenReady = false
    }

    fun videoMute() {
        player?.volume = AUDIO_DISABLED
        //ibMute.setImageResource(R.drawable.ic_audio_off)
        audioEnabled = false
    }

    fun muted(): Boolean {
        return player?.volume == AUDIO_DISABLED
    }

    fun videoUnmute() {
        player?.volume = AUDIO_ENABLED
        // ibMute.setImageResource(R.drawable.ic_audio_on)
        audioEnabled = true
    }
}
