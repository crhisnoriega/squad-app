package co.socialsquad.squad.presentation.feature.kickoff.profile

import co.socialsquad.squad.data.entity.User

interface KickoffProfileView {
    fun setupInfo(user: User)
    fun onInput()
    fun setSaveButtonEnabled(enabled: Boolean)

    fun showLoading()
    fun hideLoading()

    fun showInviteMembrers()
    fun showErrorMessageFailedToRegisterUser()

    fun email(email: String)
}
