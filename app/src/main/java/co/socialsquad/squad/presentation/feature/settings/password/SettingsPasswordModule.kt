package co.socialsquad.squad.presentation.feature.settings.password

import dagger.Binds
import dagger.Module

@Module
abstract class SettingsPasswordModule {
    @Binds
    abstract fun view(settingsPasswordActivity: SettingsPasswordActivity): SettingsPasswordView
}
