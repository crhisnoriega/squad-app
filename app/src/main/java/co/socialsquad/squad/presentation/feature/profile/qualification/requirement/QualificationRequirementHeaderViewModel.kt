package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import co.socialsquad.squad.presentation.custom.ViewModel

class QualificationRequirementHeaderViewModel(
    val title: String,
    val description: String,
    val videoUrl: String? = null,
    val videoThumbnailUrl: String? = null
) : ViewModel
