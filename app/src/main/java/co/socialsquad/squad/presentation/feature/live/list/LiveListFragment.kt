package co.socialsquad.squad.presentation.feature.live.list

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter.Companion.REQUEST_CODE_VALUES
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_live_list.*
import javax.inject.Inject

class LiveListFragment : Fragment(), LiveListView {

    @Inject
    lateinit var presenter: LiveListPresenter

    private lateinit var adapter: RecyclerViewAdapter

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_live_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSwipeRefresh()
        setupList()
        presenter.onViewCreated()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is LiveListItemViewModel -> LIVE_LIST_ITEM_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                LIVE_LIST_ITEM_VIEW_MODEL_ID -> LiveListItemViewHolder(view, onLiveClickedListener, onRecommendListener)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvLives.apply {
            adapter = this@LiveListFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    fun onRecommendSelected(viewModel: LiveListItemViewModel) {
        val segmentName = getString(R.string.qualification_title)
        val metric = MetricViewModel("qualification", segmentName, listOf<ValueViewModel>(), true, null)
        val intent = Intent(activity, SegmentValuesActivity::class.java)
        intent.putExtra(EXTRA_METRICS, metric)
        presenter.onRecommending(viewModel)
        startActivityForResult(intent, REQUEST_CODE_VALUES)
    }

    private val onRecommendListener = object : ViewHolder.Listener<LiveListItemViewModel> {
        override fun onClick(viewModel: LiveListItemViewModel) {
            onRecommendSelected(viewModel)
        }
    }

    private val onLiveClickedListener = object : ViewHolder.Listener<LiveListItemViewModel> {
        override fun onClick(viewModel: LiveListItemViewModel) {
            presenter.onLiveClicked(viewModel)
        }
    }

    override fun openVideo(url: String) {
        val intent = Intent(activity, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    private fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener {
            srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            presenter.onRefresh()
        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit?) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun addItems(lives: List<LiveListItemViewModel>, clear: Boolean) {
        with(adapter) {
            if (clear) setItems(lives)
            else addItems(lives)
        }
    }

    override fun startLoading() {
        adapter.startLoading()
        if (!srlLoading.isRefreshing) srlLoading.isEnabled = false
    }

    override fun stopLoading() {
        adapter.stopLoading()
        srlLoading.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun showEmptyState() {
        srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.VISIBLE
    }
}
