package co.socialsquad.squad.presentation.feature.opportunityv2.customview

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.dialog_confirm_opportunity_delete.*


class ConfirmOpportunityDeleteDialog(var callback: (state:String) -> Unit) : DialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_confirm_opportunity_delete, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.let { window ->
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            window.decorView.setBackgroundResource(android.R.color.transparent)
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window.attributes.windowAnimations = R.style.DialogAnimationFade
        }
        dialog?.setCanceledOnTouchOutside(false)

        confirm_button.setOnClickListener {
            dismiss()
            callback.invoke("confirm")
        }
        cancel_button.setOnClickListener {
            dismiss()
            callback.invoke("cancel")
        }
    }


}