package co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation

interface ProfileEditHinodeConfirmationView {
    fun setConfirmButtonEnabled(enabled: Boolean)

    fun showNavigation()

    fun showErrorMessageFailedToVerifyId()

    fun showLoading()

    fun hideLoading()
}
