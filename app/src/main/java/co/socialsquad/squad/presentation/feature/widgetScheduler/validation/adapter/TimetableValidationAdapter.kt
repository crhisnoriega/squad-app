package co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter

import android.os.Handler
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.domain.model.Term

class TimetableValidationAdapter(
    private val terms: List<TermVM>?,
    private val timetableValidationEnableButtonCallback: TimetableValidationEnableButtonCallback
) :
    RecyclerView.Adapter<TimetableValidationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimetableValidationViewHolder {
        return TimetableValidationViewHolder(parent, object : TimetableValidationCheckedCallback {
            override fun onChecked(checked: Boolean, term: Term) {
                var allChecked = true

                terms?.let {

                    it.forEach {termIt->
                        if(termIt.term == term){
                            termIt.checked = checked
                        }
                        if (!termIt.checked) {
                            allChecked = false
                        }
                    }

                    Handler().postDelayed(
                        {
                            notifyDataSetChanged()
                        },
                        1
                    )
                } ?: run {
                    allChecked = false
                }

                timetableValidationEnableButtonCallback.changeButtonState(allChecked)
            }
        })
    }

    override fun onBindViewHolder(holder: TimetableValidationViewHolder, position: Int) {
        terms?.let {
            holder.bind(it[position])
        }
    }

    override fun getItemCount(): Int = terms?.size.let { it } ?: run { 0 }
}