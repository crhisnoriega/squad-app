package co.socialsquad.squad.presentation.feature.squad

import co.socialsquad.squad.data.entity.Feed
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import io.reactivex.Observable
import javax.inject.Inject

class SquadRepositoryImpl @Inject constructor(
    private val api: Api
) : SquadRepository {
    override fun getSquadFeed(page: Int): Observable<Feed> = api.getCompanyFeed(page).onDefaultSchedulers()
}
