package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_recognition_bonus.view.dividerItemTop
import kotlinx.android.synthetic.main.item_recognition_bonus.view.mainLayout
import kotlinx.android.synthetic.main.item_recognition_bonus.view.txtSubtitle
import kotlinx.android.synthetic.main.item_recognition_bonus.view.txtTitle
import kotlinx.android.synthetic.main.item_recognition_info.view.*

class ItemRecognitionInfoViewHolder(
        itemView: View,
        var onClickAction: ((item: ItemRecognitionInfoViewModel) -> Unit))
    : ViewHolder<ItemRecognitionInfoViewModel>(itemView) {

    override fun bind(viewModel: ItemRecognitionInfoViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.planData.title
        itemView.txtSubtitle.text = viewModel.planData.description


        if (viewModel.planData?.levels != null) {
            itemView.txtDescription.text = viewModel.planData.levels.title
        } else {
            // itemView.txtDescription.text = viewModel.rankingData.users_in_rank
        }

        itemView.icOptions.visibility = View.VISIBLE

        itemView.dividerItemTop.isVisible = viewModel.planData.first.not()
        // Glide.with(itemView.context).load(viewModel.rankingData.icon).into(itemView.imgIcon)

        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.planData.icon), itemView.imgIcon)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}