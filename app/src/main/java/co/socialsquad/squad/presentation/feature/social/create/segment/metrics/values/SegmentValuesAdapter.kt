package co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values

// class SegmentValuesAdapter(
//        private val context: Context,
//        private val layoutInflater: LayoutInflater,
//        private val metric: MetricViewModel,
// ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//
//    private lateinit var recyclerView: RecyclerView
//
//    private val glide = Glide.with(context)
//    private val values = mutableListOf<ValueViewModel>()
//
//    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        super.onAttachedToRecyclerView(recyclerView)
//        this.recyclerView = recyclerView
//    }
//
//    override fun getItemCount() = values.size
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        val view = layoutInflater.inflate(R.layout.linearlayout_segment_values_list_item, parent, false)
//        return ValueViewModel(view)
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        val valueViewHolder = holder as ValueViewHolder
//        val value = values[position]
//
//        valueViewHolder.tvFirst.text = value.name
//        valueViewHolder.tvSecond.text =
//                if (value.members > 1) context.getString(R.string.segment_values_members, value.members)
//                else context.getString(R.string.segment_values_member)
//
//        if (value.icon != null) {
//            valueViewHolder.ivImage.visibility = View.VISIBLE
//            glide.load(value.icon).into(valueViewHolder.ivImage)
//        } else {
//            valueViewHolder.ivImage.visibility = View.GONE
//            glide.clear(valueViewHolder.ivImage)
//        }
//
//        valueViewHolder.rbIndicator.visibility = if (metric.multipick) View.GONE else View.VISIBLE
//        valueViewHolder.cbIndicator.visibility = if (metric.multipick) View.VISIBLE else View.GONE
//
//        if (metric.values.contains(value)) {
//            valueViewHolder.rbIndicator.isChecked = true
//            valueViewHolder.cbIndicator.isChecked = true
//            listener.onSelected(value)
//        }
//
//        valueViewHolder.itemView.setOnClickListener {
//            if (metric.multipick) {
//                if (valueViewHolder.cbIndicator.isChecked) {
//                    valueViewHolder.cbIndicator.isChecked = false
//                    listener.onDeselected(value)
//                } else {
//                    valueViewHolder.cbIndicator.isChecked = true
//                    listener.onSelected(value)
//                }
//            } else {
//                valueViewHolder.rbIndicator.isChecked = true
//                listener.onSelected(value)
//                for (i in values.indices) {
//                    if (i != valueViewHolder.adapterPosition) {
//                        val viewHolder = recyclerView.findViewHolderForAdapterPosition(i) as ValueViewHolder?
//                        viewHolder?.rbIndicator?.isChecked = false
//                        listener.onDeselected(values[i])
//                    }
//                }
//            }
//        }
//    }
//
//    private fun setupBackground(position: Int, valueViewHolder: ValueViewHolder) {
//        var backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item)
//        var backgroundOuter = context.getDrawable(R.drawable.shape_card_list_item_outer)
//        valueViewHolder.llOuter.setPadding(
//                context.resources.getDimensionPixelSize(R.dimen.default_margin), 0,
//                context.resources.getDimensionPixelSize(R.dimen.default_margin), 0)
//
//        val lastItemPosition = values.size - 1
//        when {
//            values.size == 1 -> {
//                backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item_rounded)
//                backgroundOuter = context.getDrawable(R.drawable.shape_card_list_item_outer_rounded)
//                valueViewHolder.llOuter.setPadding(
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin))
//            }
//            position == 0 -> {
//                backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item_rounded_top)
//                backgroundOuter = context.getDrawable(R.drawable.shape_card_list_item_outer_rounded_top)
//                valueViewHolder.llOuter.setPadding(
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin), 0)
//            }
//            position == lastItemPosition -> {
//                backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item_rounded_bottom)
//                backgroundOuter = context.getDrawable(R.drawable.layerlist_card_list_item_outer_rounded_bottom)
//                valueViewHolder.llOuter.setPadding(
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin), 0,
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin),
//                        context.resources.getDimensionPixelSize(R.dimen.default_margin)
//                                + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2f, context.resources.displayMetrics).toInt())
//            }
//        }
//        valueViewHolder.llOuter.background = backgroundOuter
//        valueViewHolder.llInner.background = backgroundInner
//    }
//
//    fun setItems(values: List<Value>) {
//        this.values.apply {
//            clear()
//            addAll(values)
//        }
//        notifyDataSetChanged()
//    }
//
//    fun addItems(values: List<Value>) {
//        this.values.apply {
//            if (size > 1) notifyItemChanged(size - 2)
//            addAll(values)
//            notifyItemRangeInserted(size - 1, values.size)
//        }
//    }
//
//    private inner class ValueViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        val llOuter: LinearLayout = itemView.segment_values_list_item_ll_outer
//        val llInner: LinearLayout = itemView.segment_values_list_item_ll_inner
//        val ivImage: ImageView = itemView.segment_values_list_item_iv_image
//        val tvFirst: TextView = itemView.segment_values_list_item_tv_first
//        val tvSecond: TextView = itemView.segment_values_list_item_tv_second
//        val rbIndicator: RadioButton = itemView.segment_values_list_item_rb_indicator
//        val cbIndicator: CheckBox = itemView.segment_values_list_item_cb_indicator
//    }
//
// }
