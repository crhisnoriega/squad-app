package co.socialsquad.squad.presentation.custom

import android.view.View
import android.view.ViewGroup
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.dpToPixels
import co.socialsquad.squad.presentation.util.getColorCompat
import kotlinx.android.synthetic.main.view_list_boundary_outer.view.*

const val ITEM_DIVIDER = R.layout.view_list_boundary_outer

sealed class ItemDivider(val backgroundColorRes: Int) : ViewModel
data class OuterDivider(val marginTopInDp: Int = 0) : ItemDivider(R.color.list_boundary_outer)
object InnerDivider : ItemDivider(R.color.list_boundary_inner)

class DividerViewHolder(itemView: View) : ViewHolder<ItemDivider>(itemView) {
    override fun bind(viewModel: ItemDivider) {
        itemView.divider.setBackgroundColor(itemView.context.getColorCompat(viewModel.backgroundColorRes))
        val marginTop = if (viewModel is OuterDivider) viewModel.marginTopInDp.dpToPixels() else 0f
        val lp = itemView.layoutParams as ViewGroup.MarginLayoutParams
        lp.setMargins(lp.leftMargin, marginTop.toInt(), lp.rightMargin, lp.bottomMargin)
        itemView.layoutParams = lp
    }

    override fun recycle() {
    }
}
