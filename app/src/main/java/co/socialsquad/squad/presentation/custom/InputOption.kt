package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_input_option.view.*

class InputOption(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    interface Listener {
        fun onClicked()
    }

    var listener: Listener? = null

    init {
        View.inflate(context, R.layout.view_input_option, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputOption)) {
            tvTitle.text = getString(R.styleable.InputOption_title)
            tvDescription.text = getString(R.styleable.InputOption_description)
            getResourceId(R.styleable.InputOption_image, 0)
                .takeUnless { it == 0 }
                ?.apply { ivImage.setImageResource(this) }
            rbCheck.isChecked = getBoolean(R.styleable.InputOption_checked, false)
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener { listener?.onClicked() }
    }

    fun setCheck(checked: Boolean) {
        rbCheck.isChecked = checked
    }
}
