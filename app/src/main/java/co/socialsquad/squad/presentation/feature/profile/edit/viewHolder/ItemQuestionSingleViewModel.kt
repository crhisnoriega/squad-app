package co.socialsquad.squad.presentation.feature.profile.edit.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemQuestionSingleViewModel(
    val question: String,
    var isSelected: Boolean,
    var isFirst: Boolean
) : ViewModel