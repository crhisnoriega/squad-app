package co.socialsquad.squad.presentation.feature.users

import co.socialsquad.squad.data.entity.PostLikesResponse
import co.socialsquad.squad.data.entity.UserListViewModel
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.Api
import co.socialsquad.squad.data.repository.AudioRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel
import io.reactivex.Observable
import javax.inject.Inject

class UserInteractor @Inject constructor(
    private val api: Api,
    private val socialRepository: SocialRepository,
    private val audioRepository: AudioRepository
) {

    fun getAttendingUsers(livePk: Int, page: Int, type: String): Observable<UserListViewModel> =
        getDownlineViewOrNot("live", livePk, type, page)

    fun getLikedUsers(postPk: Int, page: Int): Observable<UserListViewModel> =
        socialRepository.getFeedUsersLiked(postPk, page).map { mapLikesToViewModel(it) }

    fun getAudioLikedUsers(audioPk: Int, page: Int): Observable<UserListViewModel> =
        audioRepository.getAudioLikes(audioPk, page).map { mapLikesToViewModel(it) }

    fun getAudioPlaybackUsersCount(audioPk: Int, type: String): Observable<Int> =
        api.getDownlineViewOrNor("audio", audioPk, type, 1).onDefaultSchedulers()
            .map { it.count }

    fun getAttendingUsersCount(livePk: Int, type: String): Observable<Int> =
        api.getDownlineViewOrNor("live", livePk, type, 1).onDefaultSchedulers()
            .map { it.count }

    fun getAudioPlaybackDownlineUsers(audioPk: Int, page: Int, type: String): Observable<UserListViewModel> =
        getDownlineViewOrNot("audio", audioPk, type, page)

    fun getAttendingEventUsers(eventPk: Int, page: Int, type: String): Observable<UserListViewModel> =
        getDownlineViewOrNot("event", eventPk, type, page)

    fun getAttendingEventUsersCount(livePk: Int, type: String): Observable<Int> =
        api.getDownlineViewOrNor("event", livePk, type, 1).onDefaultSchedulers()
            .map { it.count }

    fun getDocumentViewedUsers(documentPk: Int, page: Int, type: String): Observable<UserListViewModel> =
        getDownlineViewOrNot("document", documentPk, type, page)

    fun getDocumentViewedUsersCount(livePk: Int, type: String): Observable<Int> =
        api.getDownlineViewOrNor("document", livePk, type, 1).onDefaultSchedulers()
            .map { it.count }

    fun getFeedVisualizationUsers(documentPk: Int, page: Int, type: String): Observable<UserListViewModel> =
        getDownlineViewOrNot("feed", documentPk, type, page)

    fun getFeedVisualizationUsersCount(livePk: Int, type: String): Observable<Int> =
        api.getDownlineViewOrNor("feed", livePk, type, 1).onDefaultSchedulers()
            .map { it.count }

    private fun getDownlineViewOrNot(model: String, itemPk: Int, type: String, page: Int): Observable<UserListViewModel> {
        return api.getDownlineViewOrNor(model, itemPk, type, page).onDefaultSchedulers()
            .map {
                UserListViewModel(
                    it.next == null,
                    it.count ?: 0,
                    it.results.orEmpty().map {
                        UserViewModel(
                            it.pk, it.user.firstName + " " + it.user.lastName,
                            it.qualification, it.user.avatar
                        )
                    }
                )
            }
    }

    private fun mapLikesToViewModel(postLikes: PostLikesResponse): UserListViewModel {
        val viewModelList = arrayListOf<UserViewModel>()
        postLikes.likes?.forEach { like ->
            like.userCompany?.let {
                viewModelList.add(
                    UserViewModel(
                        it.pk,
                        it.user.firstName + " " + it.user.lastName,
                        it.qualification, it.user.avatar
                    )
                )
            }
        }
        return UserListViewModel(postLikes.next == null, postLikes.count ?: 0, viewModelList)
    }
}
