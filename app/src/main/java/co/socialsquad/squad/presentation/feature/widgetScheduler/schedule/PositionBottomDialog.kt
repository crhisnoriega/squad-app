package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.argument
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.dialog_scala_position.*
import kotlinx.android.synthetic.main.item_scala_position.view.*

private const val VIEW_ITEM_POSITION_ID = R.layout.item_scala_position

class PositionBottomDialog : BottomSheetDialogFragment() {

    companion object {
        const val ARGS_POSITIONS = "ARGS_POSITIONS"

        fun newInstance(items: List<PositionViewHolderModel>) = PositionBottomDialog().apply {
            arguments = bundleOf(
                ARGS_POSITIONS to items
            )
        }
    }

    private val items by argument<List<PositionViewHolderModel>>(ARGS_POSITIONS)
    var onClick: (model: PositionViewHolderModel) -> Unit = {}

    private val positionAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is PositionViewHolderModel -> VIEW_ITEM_POSITION_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View) = when (viewType) {
            VIEW_ITEM_POSITION_ID -> PositionViewHolder(view, onClick)
            else -> throw IllegalArgumentException()
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_scala_position, container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvPositions.adapter = positionAdapter
        positionAdapter.setItems(items)
    }
}

class PositionViewHolder(
    itemView: View,
    private var onClick: (model: PositionViewHolderModel) -> Unit = {}
) : ViewHolder<PositionViewHolderModel>(itemView) {

    override fun bind(viewModel: PositionViewHolderModel) {
        viewModel.icon?.let {
            Glide.with(itemView)
                .load(it.url)
                .crossFade()
                .into(itemView.imgPosition)
        }

        itemView.titlePosition.text = viewModel.title

        viewModel.description?.isNullOrBlank()?.let {
            itemView.subtitlePosition.isVisible = false
        } ?: run{
            itemView.subtitlePosition.text = viewModel.description
            itemView.subtitlePosition.isVisible = true
        }
        itemView.radioPosition.isSelected = viewModel.isSelected
        itemView.setOnClickListener {
            itemView.radioPosition.isSelected = true
            onClick(viewModel)
        }
    }

    override fun recycle() {
    }
}

@Parcelize
data class PositionViewHolderModel(
    val id: Long,
    val icon: SimpleMedia?,
    val title: String,
    val description: String?,
    val isSelected: Boolean
) : ViewModel, Parcelable
