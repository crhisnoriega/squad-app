package co.socialsquad.squad.presentation.feature.search

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.view_search_no_results.view.*

class SearchNoResultsViewHolder(itemView: View) : ViewHolder<SearchNoResultsViewModel>(itemView) {
    override fun bind(viewModel: SearchNoResultsViewModel) {
        itemView.tvMessage.text = itemView.context.getString(R.string.search_no_results_description, viewModel.query)
    }

    override fun recycle() {}
}
