package co.socialsquad.squad.presentation.feature.settings

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.webkit.CookieManager
import co.socialsquad.squad.data.database.SquadDataBase
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_STOP
import co.socialsquad.squad.data.service.ChatService
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CHANGE_PASSWORD
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_LOGOUT
import io.branch.referral.Branch
import okhttp3.Cache
import javax.inject.Inject

class SettingsPresenter @Inject constructor(
    private val settingsView: SettingsView,
    private val loginRepository: LoginRepository,
    private val branch: Branch,
    private val tagWorker: TagWorker,
    private val encrypt: SimpleWordEncrypt,
    private val preferences: Preferences,
    private val cache: Cache,
    private val dataBase: SquadDataBase
) : SharingAppPresenter(settingsView, tagWorker) {

    fun onLogoutClicked(context: Context) {
        tagWorker.tagEvent(TAG_TAP_LOGOUT)
        logoutUser(context)
    }

    private fun logoutUser(context: Context) {
        sendLogoutRequest()
        loginRepository.userCompany?.company = null
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.clearSharingAppDialogTimer()
        encrypt.removeAndroidKeyStoreKey()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
        preferences.cleanSections()
        val cookieManager = CookieManager.getInstance()
        cookieManager.removeAllCookies(null)
        cookieManager.flush()
        branch.logout()
        cache.evictAll()
        val intent = Intent(context, ChatService::class.java).apply {
            action = CHAT_SERVICE_ACTION_STOP
        }
        context.startService(intent)
        settingsView.showWalkthroughAfterLogout()
        Thread(
            Runnable {
                dataBase.clearAllTables()
            }
        ).start()
    }

    @SuppressLint("CheckResult")
    private fun sendLogoutRequest() {
        loginRepository.logout().subscribe({}, {})
    }

    fun onSettingClicked() {
        tagWorker.tagEvent(TAG_TAP_CHANGE_PASSWORD)
    }
}
