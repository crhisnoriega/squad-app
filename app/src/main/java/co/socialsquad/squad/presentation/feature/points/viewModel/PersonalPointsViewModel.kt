package co.socialsquad.squad.presentation.feature.points.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.points.model.SystemData
import co.socialsquad.squad.presentation.feature.points.model.SystemPointsResponse
import co.socialsquad.squad.presentation.feature.points.repository.PersonalPointsRepository
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class PersonalPointsViewModel(
    var personalPointsRepository: PersonalPointsRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    val systemsPoints = MutableStateFlow<Resource<SystemPointsResponse>>(Resource.loading(null))

    val systemsData = MutableStateFlow<Resource<SystemData>>(Resource.loading(null))


    @ExperimentalCoroutinesApi
    fun fetchSystemPoints() {
        viewModelScope.launch {
            personalPointsRepository.fetchSystemsPoints()
                .flowOn(Dispatchers.IO)
                .catch { e ->
                    Log.i("rek", e.message, e)
                    systemsPoints.value = Resource.error("", null)
                }
                .collect {
                    Log.i("rek", Gson().toJson(it))
                    systemsPoints.value = Resource.success(it)
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchSystemPointsById(points_system_id: String) {
        viewModelScope.launch {
            personalPointsRepository.fetchSystemsPointsById(points_system_id)
                .flowOn(Dispatchers.IO)
                .catch { e ->
                    systemsData.value = Resource.error("", null)
                }
                .collect {
                    systemsData.value = Resource.success(it)
                }
        }
    }


}
