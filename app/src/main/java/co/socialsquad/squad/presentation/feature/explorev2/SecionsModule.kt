package co.socialsquad.squad.presentation.feature.explorev2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.socialsquad.squad.presentation.ViewModelFactory
import co.socialsquad.squad.presentation.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SecionsModule {

    @Binds
    @IntoMap
    @ViewModelKey(SectionsViewModel::class) // PROVIDE YOUR OWN MODELS HERE
    fun bindSectionsViewModel(sectionsViewModel: SectionsViewModel): ViewModel

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
