package co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter

import co.socialsquad.squad.domain.model.Term

interface TimetableValidationCheckedCallback {
    fun onChecked(checked: Boolean, term: Term)
}