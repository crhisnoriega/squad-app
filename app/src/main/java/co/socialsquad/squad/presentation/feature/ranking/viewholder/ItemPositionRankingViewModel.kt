package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemPositionRankingViewModel(
        val user: String,
        val position: String,
        val score: String,
        val thumbnail_avatar: String?,
        val isFirst: Boolean = false
) : ViewModel {
}