package co.socialsquad.squad.presentation

import android.app.NotificationManager
import android.content.Context
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.service.ChatService
import co.socialsquad.squad.data.service.SignallingService
import co.socialsquad.squad.presentation.feature.about.AboutActivity
import co.socialsquad.squad.presentation.feature.about.AboutModule
import co.socialsquad.squad.presentation.feature.audio.AudioService
import co.socialsquad.squad.presentation.feature.audio.create.AudioCreateActivity
import co.socialsquad.squad.presentation.feature.audio.create.AudioCreateModule
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailModule
import co.socialsquad.squad.presentation.feature.audio.record.AudioRecordActivity
import co.socialsquad.squad.presentation.feature.audio.record.AudioRecordModule
import co.socialsquad.squad.presentation.feature.call.CallRoomActivity
import co.socialsquad.squad.presentation.feature.call.CallRoomModule
import co.socialsquad.squad.presentation.feature.chat.chatlist.ChatListActivity
import co.socialsquad.squad.presentation.feature.chat.chatlist.ChatListModule
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomActivity
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomModule
import co.socialsquad.squad.presentation.feature.company.CompanyLoginActivity
import co.socialsquad.squad.presentation.feature.company.CompanyLoginModule
import co.socialsquad.squad.presentation.feature.company.SystemSelectActivity
import co.socialsquad.squad.presentation.feature.company.SystemSelectModule
import co.socialsquad.squad.presentation.feature.event.detail.EventDetailActivity
import co.socialsquad.squad.presentation.feature.event.detail.EventDetailModule
import co.socialsquad.squad.presentation.feature.explorev2.SectionsFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemMapFragment
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemMapPermissionFragment
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.feature.feedback.FeedbackActivity
import co.socialsquad.squad.presentation.feature.feedback.FeedbackModule
import co.socialsquad.squad.presentation.feature.hub.intranet.webview.IntranetWebviewActivity
import co.socialsquad.squad.presentation.feature.immobiles.details.ImmobileDetailModule
import co.socialsquad.squad.presentation.feature.immobiles.details.ImmobilesDetailsActivity
import co.socialsquad.squad.presentation.feature.immobiles.details.moredetails.MoreDetailsActivity
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.kickoff.KickoffModule
import co.socialsquad.squad.presentation.feature.kickoff.voila.VoilaActivity
import co.socialsquad.squad.presentation.feature.live.creator.LiveCreatorActivity
import co.socialsquad.squad.presentation.feature.live.creator.LiveCreatorModule
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsActivity
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsModule
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerModule
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativeActivity
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativeModule
import co.socialsquad.squad.presentation.feature.login.alternative.success.LoginAlternativeSuccessActivity
import co.socialsquad.squad.presentation.feature.login.alternative.success.LoginAlternativeSuccessModule
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailActivity
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailModule
import co.socialsquad.squad.presentation.feature.login.forgotpassword.ResetPasswordActivity
import co.socialsquad.squad.presentation.feature.login.forgotpassword.ResetPasswordModule
import co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm.ConfirmResetPasswordActivity
import co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm.ConfirmResetPasswordModule
import co.socialsquad.squad.presentation.feature.login.options.LoginOptionsActivity
import co.socialsquad.squad.presentation.feature.login.options.LoginOptionsModule
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordActivity
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordModule
import co.socialsquad.squad.presentation.feature.login.subdomain.LoginSubdomainActivity
import co.socialsquad.squad.presentation.feature.login.subdomain.LoginSubdomainModule
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationModule
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.feature.opportunity.OpportunityActivity
import co.socialsquad.squad.presentation.feature.opportunity.OpportunityModule
import co.socialsquad.squad.presentation.feature.opportunity.detail.OpportunitySubmissionDetailActivity
import co.socialsquad.squad.presentation.feature.opportunity.form.FormFieldFragment
import co.socialsquad.squad.presentation.feature.opportunity.form.FormFieldModule
import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityInfoActivity
import co.socialsquad.squad.presentation.feature.opportunity.info.OpportunityStageInfoActivity
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ShowAllOpportunitySubmissionsActivity
import co.socialsquad.squad.presentation.feature.opportunityv2.OpportunityDetailsActivity
import co.socialsquad.squad.presentation.feature.password.PostPasswordActivity
import co.socialsquad.squad.presentation.feature.password.PostPasswordModule
import co.socialsquad.squad.presentation.feature.profile.edit.ProfileEditActivity
import co.socialsquad.squad.presentation.feature.profile.edit.ProfileEditModule
import co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation.ProfileEditHinodeConfirmationActivity
import co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation.ProfileEditHinodeConfirmationModule
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.profile.others.UserModule
import co.socialsquad.squad.presentation.feature.profile.qualification.ProfileQualificationActivity
import co.socialsquad.squad.presentation.feature.profile.qualification.ProfileQualificationModule
import co.socialsquad.squad.presentation.feature.profile.qualification.recommendations.QualificationRecommendationsActivity
import co.socialsquad.squad.presentation.feature.profile.qualification.recommendations.QualificationRecommendationsModule
import co.socialsquad.squad.presentation.feature.profile.qualification.requirement.QualificationRequirementActivity
import co.socialsquad.squad.presentation.feature.profile.qualification.requirement.QualificationRequirementModule
import co.socialsquad.squad.presentation.feature.schedule.CreateScheduleActivity
import co.socialsquad.squad.presentation.feature.schedule.CreateScheduleModule
import co.socialsquad.squad.presentation.feature.search.SearchActivity
import co.socialsquad.squad.presentation.feature.search.SearchModule
import co.socialsquad.squad.presentation.feature.settings.SettingsActivity
import co.socialsquad.squad.presentation.feature.settings.SettingsModule
import co.socialsquad.squad.presentation.feature.settings.password.SettingsPasswordActivity
import co.socialsquad.squad.presentation.feature.settings.password.SettingsPasswordModule
import co.socialsquad.squad.presentation.feature.signup.accesssquad.AccessSquadActivity
import co.socialsquad.squad.presentation.feature.signup.accesssquad.AccessSquadModule
import co.socialsquad.squad.presentation.feature.signup.acesscode.AccessCodeActivity
import co.socialsquad.squad.presentation.feature.signup.credentials.SignupCredentialsActivity
import co.socialsquad.squad.presentation.feature.signup.credentials.SignupCredentialsModule
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveModule
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaImageActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaModule
import co.socialsquad.squad.presentation.feature.social.create.schedule.SocialCreateScheduledLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.schedule.SocialCreateScheduledLiveModule
import co.socialsquad.squad.presentation.feature.social.create.segment.SocialCreateSegmentActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.SocialCreateSegmentModule
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersModule
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsModule
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesModule
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsModule
import co.socialsquad.squad.presentation.feature.splash.SplashActivity
import co.socialsquad.squad.presentation.feature.splash.SplashModule
import co.socialsquad.squad.presentation.feature.squad.social.SocialSquadActivity
import co.socialsquad.squad.presentation.feature.squad.social.SocialSquadModule
import co.socialsquad.squad.presentation.feature.store.category.StoreCategoryActivity
import co.socialsquad.squad.presentation.feature.store.category.StoreCategoryModule
import co.socialsquad.squad.presentation.feature.store.product.ProductActivity
import co.socialsquad.squad.presentation.feature.store.product.ProductModule
import co.socialsquad.squad.presentation.feature.store.product.details.ProductDetailGroupActivity
import co.socialsquad.squad.presentation.feature.store.product.details.ProductDetailGroupModule
import co.socialsquad.squad.presentation.feature.store.product.locations.ProductLocationsActivity
import co.socialsquad.squad.presentation.feature.store.product.locations.ProductLocationsModule
import co.socialsquad.squad.presentation.feature.store.products.ProductsActivity
import co.socialsquad.squad.presentation.feature.store.products.ProductsModule
import co.socialsquad.squad.presentation.feature.terms.TermsActivity
import co.socialsquad.squad.presentation.feature.terms.TermsModule
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListModule
import co.socialsquad.squad.presentation.feature.walkthrough.WalkthroughActivity
import co.socialsquad.squad.presentation.feature.webview.WebViewActivity
import co.socialsquad.squad.presentation.util.Analytics
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.mixpanel.android.mpmetrics.MixpanelAPI
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.branch.referral.Branch
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class, AppModule.Providers::class])
@Suppress("unused")
interface AppModule {

    @ContributesAndroidInjector
    fun audioService(): AudioService

    @ContributesAndroidInjector
    fun callService(): SignallingService

    @ContributesAndroidInjector
    fun chatService(): ChatService

    @ContributesAndroidInjector(modules = [SplashModule::class])
    fun splashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [LoginAlternativeModule::class])
    fun loginAlternativeActivity(): LoginAlternativeActivity

    @ContributesAndroidInjector(modules = [LoginSubdomainModule::class])
    fun loginSubdomainActivity(): LoginSubdomainActivity

    @ContributesAndroidInjector(modules = [LoginEmailModule::class])
    fun loginEmailActivity(): LoginEmailActivity

    @ContributesAndroidInjector(modules = [LoginPasswordModule::class])
    fun loginPasswordActivity(): LoginPasswordActivity

    @ContributesAndroidInjector(modules = [ConfirmResetPasswordModule::class])
    fun confirmResetPasswordActivity(): ConfirmResetPasswordActivity

    @ContributesAndroidInjector(modules = [ResetPasswordModule::class])
    fun resetPasswordActivity(): ResetPasswordActivity

    @ContributesAndroidInjector(modules = [NavigationModule::class])
    fun navigationActivity(): NavigationActivity

    @ContributesAndroidInjector(modules = [LiveViewerModule::class])
    fun liveViewerActivity(): LiveViewerActivity

    @ContributesAndroidInjector(modules = [LiveCreatorModule::class])
    fun liveCreatorActivity(): LiveCreatorActivity

    @ContributesAndroidInjector(modules = [SettingsModule::class])
    fun settingsActivity(): SettingsActivity

    @ContributesAndroidInjector(modules = [ProductsModule::class])
    fun productsActivity(): ProductsActivity

    @ContributesAndroidInjector(modules = [ProductModule::class])
    fun productActivity(): ProductActivity

    @ContributesAndroidInjector(modules = [ProductDetailGroupModule::class])
    fun productDetailGroupActivity(): ProductDetailGroupActivity

    @ContributesAndroidInjector(modules = [ProductLocationsModule::class])
    fun productLocationsActivity(): ProductLocationsActivity

    @ContributesAndroidInjector(modules = [StoreCategoryModule::class])
    fun storeCategoryActivity(): StoreCategoryActivity

    @ContributesAndroidInjector(modules = [SignupCredentialsModule::class])
    fun signupCredentialsActivity(): SignupCredentialsActivity

    @ContributesAndroidInjector(modules = [ImmobileDetailModule::class])
    fun immobilesDetailsActivity(): ImmobilesDetailsActivity

    @ContributesAndroidInjector(modules = [OpportunityModule::class])
    fun recommendationActivity(): OpportunityActivity

    @ContributesAndroidInjector(modules = [OpportunityModule::class])
    fun showOpportunitiesActivity(): ShowAllOpportunitySubmissionsActivity

    @ContributesAndroidInjector(modules = [OpportunityModule::class])
    fun opportunityObjectDetailActivity(): OpportunitySubmissionDetailActivity

    @ContributesAndroidInjector(modules = [OpportunityModule::class])
    fun opportunityInfoActivity(): OpportunityInfoActivity

    @ContributesAndroidInjector(modules = [OpportunityModule::class])
    fun opportunityStageInfoActivity(): OpportunityStageInfoActivity

    @ContributesAndroidInjector(modules = [ProfileEditModule::class])
    fun profileEditActivity(): ProfileEditActivity

    @ContributesAndroidInjector(modules = [SettingsPasswordModule::class])
    fun settingsPasswordActivity(): SettingsPasswordActivity

    @ContributesAndroidInjector(modules = [ProfileEditHinodeConfirmationModule::class])
    fun profileEditHinodeConfirmationActivity(): ProfileEditHinodeConfirmationActivity

    @ContributesAndroidInjector(modules = [SocialCreateLiveModule::class])
    fun socialCreateLiveActivity(): SocialCreateLiveActivity

    @ContributesAndroidInjector(modules = [SocialCreateMediaModule::class])
    fun socialCreateMediaActivity(): SocialCreateMediaActivity

    @ContributesAndroidInjector(modules = [SocialCreateSegmentModule::class])
    fun socialCreateSegmentActivity(): SocialCreateSegmentActivity

    @ContributesAndroidInjector(modules = [SegmentGroupMembersModule::class])
    fun segmentGroupMetricMembersActivity(): SegmentGroupMembersActivity

    @ContributesAndroidInjector(modules = [SegmentMetricsModule::class])
    fun segmentMetricActivity(): SegmentMetricsActivity

    @ContributesAndroidInjector(modules = [SegmentValuesModule::class])
    fun segmentValueActivity(): SegmentValuesActivity

    @ContributesAndroidInjector(modules = [SocialCreateScheduledLiveModule::class])
    fun socialCreateScheduledLiveActivity(): SocialCreateScheduledLiveActivity

    @ContributesAndroidInjector(modules = [ScheduledLiveDetailsModule::class])
    fun scheduledLiveDetailsActivity(): ScheduledLiveDetailsActivity

    @ContributesAndroidInjector(modules = [UserListModule::class])
    fun usersListActivity(): UserListActivity

    @ContributesAndroidInjector(modules = [AccessSquadModule::class])
    fun accessSquadActivity(): AccessSquadActivity

    @ContributesAndroidInjector(modules = [PostDetailsModule::class])
    fun postDetailsActivity(): PostDetailsActivity

    @ContributesAndroidInjector(modules = [SocialSquadModule::class])
    fun socialSquadActivity(): SocialSquadActivity

    @ContributesAndroidInjector(modules = [AudioCreateModule::class])
    fun audioCreateActivity(): AudioCreateActivity

    @ContributesAndroidInjector(modules = [CreateScheduleModule::class])
    fun createScheduleActivity(): CreateScheduleActivity

    @ContributesAndroidInjector(modules = [AudioDetailModule::class])
    fun audioDetailActivity(): AudioDetailActivity

    @ContributesAndroidInjector(modules = [AudioRecordModule::class])
    fun audioRecordActivity(): AudioRecordActivity

    @ContributesAndroidInjector(modules = [UserModule::class])
    fun usersActivity(): UserActivity

    @ContributesAndroidInjector(modules = [SystemSelectModule::class])
    fun systemSelectActivity(): SystemSelectActivity

    @ContributesAndroidInjector(modules = [CompanyLoginModule::class])
    fun companyLoginActivity(): CompanyLoginActivity

    @ContributesAndroidInjector(modules = [ChatListModule::class])
    fun chatListActivity(): ChatListActivity

    @ContributesAndroidInjector(modules = [ChatRoomModule::class])
    fun chatRoomActivity(): ChatRoomActivity

    @ContributesAndroidInjector(modules = [CallRoomModule::class])
    fun callRoomActivity(): CallRoomActivity

    @ContributesAndroidInjector(modules = [AboutModule::class])
    fun aboutActivity(): AboutActivity

    @ContributesAndroidInjector(modules = [ProfileQualificationModule::class])
    fun profileQualification(): ProfileQualificationActivity

    @ContributesAndroidInjector(modules = [QualificationRecommendationsModule::class])
    fun qualificationRecommendationsActivity(): QualificationRecommendationsActivity

    @ContributesAndroidInjector(modules = [QualificationRequirementModule::class])
    fun qualificationRequirement(): QualificationRequirementActivity

    @ContributesAndroidInjector(modules = [EventDetailModule::class])
    fun eventDetailActivity(): EventDetailActivity

    @ContributesAndroidInjector(modules = [SearchModule::class])
    fun searchActivity(): SearchActivity

    @ContributesAndroidInjector(modules = [co.socialsquad.squad.presentation.feature.searchv2.SearchModule::class])
    fun searchv2Activity(): co.socialsquad.squad.presentation.feature.searchv2.SearchActivity

    @ContributesAndroidInjector(modules = [FeedbackModule::class])
    fun feedbackActivity(): FeedbackActivity

    @ContributesAndroidInjector(modules = [PostPasswordModule::class])
    fun postPasswordActivity(): PostPasswordActivity

    @ContributesAndroidInjector(modules = [TermsModule::class])
    fun termsActivity(): TermsActivity

    @ContributesAndroidInjector(modules = [LoginAlternativeSuccessModule::class])
    fun loginAlternativeSuccessActivity(): LoginAlternativeSuccessActivity

    @ContributesAndroidInjector(modules = [LoginOptionsModule::class])
    fun loginOptionsActivity(): LoginOptionsActivity

    @ContributesAndroidInjector
    fun walkthroughActivity(): WalkthroughActivity

    @ContributesAndroidInjector
    fun intranetWebviewActivity(): IntranetWebviewActivity

    @ContributesAndroidInjector
    fun moreDetailsActivity(): MoreDetailsActivity

    @ContributesAndroidInjector
    fun voilaActivity(): VoilaActivity

    @ContributesAndroidInjector
    fun onBoardingActivity(): OnboardingActivity

    @ContributesAndroidInjector
    fun accessCodeActivity(): AccessCodeActivity

    @ContributesAndroidInjector
    fun socialCreateMediaImageActivity(): SocialCreateMediaImageActivity

    @ContributesAndroidInjector
    fun webViewActivity(): WebViewActivity

    @ContributesAndroidInjector(modules = [FormFieldModule::class])
    fun fieldFragment(): FormFieldFragment

    @ContributesAndroidInjector
    fun sectionsFragment(): SectionsFragment

    @ContributesAndroidInjector
    fun exploreItemObjectActivity(): ExploreItemObjectActivity

    @ContributesAndroidInjector
    fun opportunityDetailsActivity(): OpportunityDetailsActivity

    @ContributesAndroidInjector
    fun exploreItemFragment(): ExploreItemFragment

    @ContributesAndroidInjector
    fun exploreItemMapFragment(): ExploreItemMapFragment

    @ContributesAndroidInjector
    fun exploreItemMapPermissionFragment(): ExploreItemMapPermissionFragment

    @Module
    class Providers {

        @Provides
        @Singleton
        fun context(app: App): Context = app.applicationContext

        @Provides
        @Singleton
        fun firebaseAnalytics(context: Context) = FirebaseAnalytics.getInstance(context)

        @Provides
        @Singleton
        fun mixpanelAPI(context: Context): MixpanelAPI =
            MixpanelAPI.getInstance(context, "7108751c321eccb18a176da6807fc74d")

        @Provides
        @Singleton
        fun fusedLocationProviderClient(context: Context): FusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)

        @Provides
        @Singleton
        fun branchIo(): Branch = Branch.getInstance()

        @Provides
        @Singleton
        fun analytics(analytics: FirebaseAnalytics): Analytics = Analytics(analytics)

        @Provides
        @Singleton
        fun notificationManager(context: Context) =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        @Provides
        @Singleton
        fun securityGateway(context: Context) = SimpleWordEncrypt(context)
    }
}
