package co.socialsquad.squad.presentation.feature.opportunity.info

import co.socialsquad.squad.data.entity.opportunity.Information
import co.socialsquad.squad.presentation.feature.LoadingView

interface OpportunityInfoView : LoadingView {

    fun onOpportunityStageClicked(info: Information)
}
