package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class BenefitData(
        @SerializedName("id") val id: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("media_cover") val mediaCover: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("positions") val positions: String?,
        @SerializedName("show_details") val show_details: Boolean?,
        @SerializedName("medias") val medias: List<Media>?
) : Parcelable