package co.socialsquad.squad.presentation.feature.recognition.listing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.entity.RecognitionData
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingTopBottomViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemSeparatorRankingViewHolder
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingTopBottomViewModel
import co.socialsquad.squad.presentation.feature.ranking.viewholder.ItemShowAllRankingViewModel
import co.socialsquad.squad.presentation.feature.recognition.RecognitionActivity
import co.socialsquad.squad.presentation.feature.recognition.viewHolder.ItemRecognitionInfoViewHolder
import co.socialsquad.squad.presentation.feature.recognition.viewHolder.ItemRecognitionInfoViewModel
import co.socialsquad.squad.presentation.feature.recognition.viewmodel.RecognitionViewModel
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_elements_listing.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

enum class RECOGNITION_LIST_TYPE(val title: String) {
    POSITION("Posições"), CRITERIA("Critérios"), BENEFIT("Benefícios"), PLANS_INFO("Rankings"), PRIZES("Prêmios")
}

class RecognitionElementsListingActivity : BaseActivity() {

    companion object {

        var doRefresh: Boolean = false

        const val ELEMENT_TYPE = "ELEMENT_TYPE"
        const val PLAN_DATA_EXTRA = "PLAN_DATA_EXTRA"

        fun start(context: Context,
                  elementType: RECOGNITION_LIST_TYPE,
                  planData: RecognitionData
        ) {
            context.startActivity(Intent(context, RecognitionElementsListingActivity::class.java).apply {
                putExtra(ELEMENT_TYPE, elementType)
                putExtra(PLAN_DATA_EXTRA, planData)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_elements_listing

    private val elementType: RECOGNITION_LIST_TYPE by extra(ELEMENT_TYPE)
    private val planData: RecognitionData by extra(PLAN_DATA_EXTRA)

    private val viewModel by viewModel<RecognitionViewModel>()

    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

        configureAsLoading(2)

        when (elementType) {
            RECOGNITION_LIST_TYPE.PLANS_INFO -> viewModel.fetchPlanSummary()
        }
    }


    override fun onResume() {
        Log.i("list", "onResume: $doRefresh")
        super.onResume()
        if (doRefresh) {
            doRefresh = false

            factoryAdapter.items.clear()

            ProfileFragment.doRefresh = true
            when (elementType) {

            }
        }
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = planData.title
    }

    private val handler = Handler()
    private var endPage = false

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.fetchPlanSummary
            value.collect {

                Log.i("rek", Gson().toJson(it))

                when (it.status) {
                    Status.SUCCESS -> {
                        configureElementList()
                    }
                    Status.ERROR -> {
                        // backend send 400 when no has more pages
                        endPage = true

                        handler.removeCallbacksAndMessages(null)
                        handler.postDelayed({
                            factoryAdapter.addItems(listOf(ItemShowAllRankingTopBottomViewModel("", "")))
                        }, 500)
                    }
                }
            }
        }


    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                    Shimmer.AlphaHighlightBuilder()
                            .setBaseAlpha(1f)
                            .setIntensity(0f)
                            .build())
            it.stopShimmer()
            it.clearAnimation()
        }
    }

    private fun configureAsLoading(itens: Int) {

        rvElements.apply {
            layoutManager = LinearLayoutManager(this@RecognitionElementsListingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                        is ItemShowAllRankingTopBottomViewModel -> R.layout.item_ranking_info_shimmer
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
                    }
                    R.layout.item_ranking_info_shimmer -> ItemSeparatorRankingTopBottomViewHolder(view) {
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            factoryAdapter.setItems((0 until itens).map {
                ItemShowAllRankingTopBottomViewModel("", "") as ViewModel
            }.toMutableList().apply {
                add(0, ItemShowAllRankingViewModel("", ""))
            })
        }
    }


    private fun addToList() {

    }

    val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel): Int {
            return when (viewModel) {
                is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                is ItemRecognitionInfoViewModel -> R.layout.item_recognition_info
                else -> throw IllegalArgumentException()
            }
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
            }
            R.layout.item_recognition_info -> ItemRecognitionInfoViewHolder(view) {
                RecognitionActivity.start(this@RecognitionElementsListingActivity, it.planData.id.toString())
            }
            else -> throw IllegalArgumentException()
        }
    })

    private val _layoutManager = LinearLayoutManager(this@RecognitionElementsListingActivity, LinearLayoutManager.VERTICAL, false)

    private fun configureElementList() {
        stopShimmer(listShimmer)

        rvElements.apply {
            layoutManager = _layoutManager

            if (page == 1) {
                adapter = factoryAdapter
            }

            var elements = mutableListOf<ViewModel>()

            if (page == 1) {
                elements.add(ItemShowAllRankingViewModel("", ""))
            }

            Log.i("list", elementType.name)
            when (elementType) {

                RECOGNITION_LIST_TYPE.PLANS_INFO -> {
                    var isFirst = true
                    viewModel.fetchPlanSummary?.value?.data?.let { list ->
                        list.results?.forEachIndexed { index, planSummaryData ->
                            elements.add(ItemRecognitionInfoViewModel(planSummaryData))
                        }
                    }
                }


                else -> mutableListOf<ViewModel>()
            }

            factoryAdapter.addItems(elements)


            addOnScrollListener(EndlessScrollListener(20, _layoutManager, {
                when (elementType) {

                    RECOGNITION_LIST_TYPE.PLANS_INFO -> {
                        if (endPage.not()) {
                            page++
                            //viewModel.fetchPositions(rankingId, page)
                        }
                    }
                }
            }))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}