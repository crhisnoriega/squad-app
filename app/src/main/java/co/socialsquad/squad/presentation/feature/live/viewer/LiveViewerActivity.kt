package co.socialsquad.squad.presentation.feature.live.viewer

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.TextureView
import android.view.View
import android.webkit.MimeTypeMap
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.view.GestureDetectorCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.feature.live.LiveAdapter
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.social.LiveViewModel
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.SoftInputUtil
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.clearFlagKeepScreenOn
import co.socialsquad.squad.presentation.util.fadeOutGone
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.isKeyboardHidden
import co.socialsquad.squad.presentation.util.setFlagKeepScreenOn
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import co.socialsquad.squad.presentation.util.shareItem
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player.*
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.LoadEventInfo
import com.google.android.exoplayer2.source.MediaLoadData
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MediaSourceEventListener
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.branch.indexing.BranchUniversalObject
import kotlinx.android.synthetic.main.activity_live_viewer.*
import java.io.IOException
import javax.inject.Inject
import kotlin.math.roundToInt

class LiveViewerActivity : BaseDaggerActivity(), LiveViewerView {

    companion object {
        const val LIVE_VIEW_MODEL = "LIVE_VIEW_MODEL"
        const val PK = "PK"
    }

    private var controlsVisible = true
    private var fadeOutControls = false
    lateinit var softkeyboardHelper: SoftInputUtil

    inner class Player(val context: Context, val lifecycleOwner: LifecycleOwner, uri: Uri, private val view: PlayerView) :
        EventListener,
        TransferListener,
        MediaSourceEventListener,
        LifecycleObserver {
        override fun onLoadStarted(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId?, loadEventInfo: LoadEventInfo, mediaLoadData: MediaLoadData) {
        }

        override fun onDownstreamFormatChanged(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId?, mediaLoadData: MediaLoadData) {
        }

        override fun onUpstreamDiscarded(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId, mediaLoadData: MediaLoadData) {
        }

        override fun onLoadCompleted(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId?, loadEventInfo: LoadEventInfo, mediaLoadData: MediaLoadData) {
        }

        override fun onLoadCanceled(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId?, loadEventInfo: LoadEventInfo, mediaLoadData: MediaLoadData) {
        }

        override fun onPositionDiscontinuity(reason: Int) {}

        override fun onSeekProcessed() {
        }

        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
        }

        private val trackSelector = DefaultTrackSelector(context)
        private val loadControl = DefaultLoadControl()
        private val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
        private val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        private val mediaItem = MediaItem.Builder().setMimeType(mimeType).setUri(uri).build()

        private val httpDataSourceFactory = DefaultHttpDataSourceFactory("ua", this, 4000, 4000, false)
        private val dataSourceFactory = DefaultDataSourceFactory(context, this, httpDataSourceFactory)
        private var mediaSource = HlsMediaSource.Factory(dataSourceFactory).createMediaSource(mediaItem)

        private var player: SimpleExoPlayer? = null
        private val unavailableHandler = Handler()
        private val unavailableRunnable = {
            if (player?.playWhenReady == false) {
                player?.stop()
                ViewUtils.showDialog(context, getString(R.string.live_viewer_message_unavailable), DialogInterface.OnDismissListener { finish() })
            }
        }
        private val finishedHandler = Handler()
        private val finishedRunnable = {
            player?.stop()
            showMessage(R.string.live_viewer_message_ended, DialogInterface.OnDismissListener { close() })
        }

        init {
            lifecycleOwner.lifecycle.addObserver(this)
            initialize()
        }

        private fun initialize() {
            player = SimpleExoPlayer.Builder(context)
                .setTrackSelector(trackSelector)
                .setLoadControl(loadControl)
                .build()

            view.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
            view.player = player?.apply {
                addListener(this@Player)
                setMediaSource(mediaSource, false)
                prepare()
                clearVideoSurface()
                setVideoTextureView(view.videoSurfaceView as TextureView)
                playWhenReady = true
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            Log.i(this.javaClass.simpleName, "onResume")
            player?.apply {
                if (playbackState == STATE_READY || playbackState == STATE_BUFFERING) playWhenReady = true
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            Log.i(this.javaClass.simpleName, "onPause")
            unavailableHandler.removeCallbacksAndMessages(null)
            softkeyboardHelper.onPause()
            player?.apply {
                if (playbackState == STATE_READY || playbackState == STATE_BUFFERING) playWhenReady = false
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            Log.i(this.javaClass.simpleName, "onDestroy")
            unavailableHandler.removeCallbacksAndMessages(null)
            finishedHandler.removeCallbacksAndMessages(null)
            player?.release()
            player = null
        }

        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {
            Log.i(this.javaClass.simpleName, "onPlaybackParametersChanged")
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
            Log.i(this.javaClass.simpleName, "onTracksChanged")
        }

        override fun onPlayerError(error: ExoPlaybackException) {
            Log.i(this.javaClass.simpleName, "onPlayerError - error.message ${error?.message}")
            if (player?.playWhenReady == false) with(unavailableHandler) {
                removeCallbacksAndMessages(null)
                postDelayed(unavailableRunnable, 120000)
            }
            initialize()
        }

        override fun onLoadingChanged(isLoading: Boolean) {
            Log.i(this.javaClass.simpleName, "onLoadingChanged - isLoading $isLoading")
        }

        override fun onRepeatModeChanged(repeatMode: Int) {
            Log.i(this.javaClass.simpleName, "onRepeatModeChanged - repeatMode $repeatMode")
        }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            unavailableHandler.removeCallbacksAndMessages(null)
            when (playbackState) {
                STATE_BUFFERING -> {
                    Log.i(this.javaClass.simpleName, "onPlayerStateChanged - playbackState Player.STATE_BUFFERING, playWhenReady $playWhenReady")
                    tvMessage.visibility = View.VISIBLE
                    tvMessage.text = getString(R.string.live_viewer_message_connecting)
                    if (!playWhenReady) unavailableHandler.postDelayed(unavailableRunnable, 120000)
                }
                STATE_READY -> {
                    Log.i(this.javaClass.simpleName, "onPlayerStateChanged - playbackState Player.STATE_READY, playWhenReady $playWhenReady")
                    tvMessage.visibility = View.GONE
                    fadeOutConstrols()
                }
                STATE_IDLE -> {
                    Log.i(this.javaClass.simpleName, "onPlayerStateChanged - playbackState Player.STATE_IDLE, playWhenReady $playWhenReady")
                }
                STATE_ENDED -> {
                    Log.i(this.javaClass.simpleName, "onPlayerStateChanged - playbackState Player.STATE_ENDED, playWhenReady $playWhenReady")
                    tvMessage.visibility = View.VISIBLE
                    tvMessage.text = getString(R.string.live_viewer_message_ended)
                }
            }
        }

        override fun onTransferInitializing(source: DataSource, dataSpec: DataSpec, isNetwork: Boolean) {
            Log.i(this.javaClass.simpleName, "onTransferInitializing")
        }

        override fun onTransferStart(source: DataSource, dataSpec: DataSpec, isNetwork: Boolean) {
            Log.i(this.javaClass.simpleName, "onTransferStart")
        }

        override fun onTransferEnd(source: DataSource, dataSpec: DataSpec, isNetwork: Boolean) {
            Log.i(this.javaClass.simpleName, "onTransferEnd")
        }

        override fun onBytesTransferred(source: DataSource, dataSpec: DataSpec, isNetwork: Boolean, bytesTransferred: Int) {
            // Log.i(this.javaClass.simpleName, "onBytesTransferred - bytesTransferred $bytesTransferred")
        }

        override fun onLoadError(windowIndex: Int, mediaPeriodId: MediaSource.MediaPeriodId?, loadEventInfo: LoadEventInfo, mediaLoadData: MediaLoadData, error: IOException, wasCanceled: Boolean) {
            if (liveEnded) finishedHandler.postDelayed(finishedRunnable, 10000)
        }
    }

    @Inject
    lateinit var liveViewerPresenter: LiveViewerPresenter

    private lateinit var liveAdapter: LiveAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var loading: AlertDialog? = null
    private lateinit var gestureDetector: GestureDetectorCompat

    private var player: Player? = null
    private var liveEnded: Boolean = false

    override fun onResume() {
        super.onResume()
        liveViewerPresenter.onResume()
        softkeyboardHelper.onResume()
        enableFadingEdges()
    }

    override fun onPause() {
        super.onPause()
        liveViewerPresenter.onPause()
    }

    override fun onDestroy() {
        liveViewerPresenter.onDestroy()
        softkeyboardHelper.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_viewer)

        setupList()
        setupOnClickListeners()
        setupGestureListeners()
        setupInputBehavior()
        setupKeyboardAction()
        showVolumeWarningIfNeeded()

        liveViewerPresenter.onCreate(intent)
    }

    private fun changeControlsStatus(clickOnTop: Boolean) {
        if (controlsVisible && clickOnTop) {
            controlsVisible = false
            lcbBadge.visibility = View.GONE
            llParticipants.visibility = View.GONE
            llComment_container.visibility = View.GONE
            bClose.visibility = View.GONE
            rvQueue.visibility = View.GONE
        } else {
            controlsVisible = true
            lcbBadge.visibility = View.VISIBLE
            llParticipants.visibility = View.VISIBLE
            llComment_container.visibility = View.VISIBLE
            bClose.visibility = View.VISIBLE
            rvQueue.visibility = View.VISIBLE
        }
    }

    private fun fadeOutConstrols() {
        if (!fadeOutControls) {
            fadeOutControls = true
            Handler().postDelayed(
                {
                    controlsVisible = false
                    lcbBadge.fadeOutGone()
                    llParticipants.fadeOutGone()
                    llComment_container.fadeOutGone()
                    bClose.fadeOutGone()
                    rvQueue.fadeOutGone()
                },
                5000
            )
        }
    }

    private fun setupList() {
        liveAdapter = LiveAdapter(this, layoutInflater)
        linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.stackFromEnd = true
        with(rvQueue) {
            adapter = liveAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(5)
            setHasFixedSize(true)
        }
    }

    private fun setupOnClickListeners() {
        bClose.setOnClickListener { close() }

        bComment.setOnClickListener {
            val message = etComment.text.toString()
            liveViewerPresenter.onPostClicked(message)
            clearEditTextAndHideKeyboard()
        }

        clickHolder.setOnClickListener {
            clearEditTextAndHideKeyboard()
            changeControlsStatus(true)
        }

        clickHolderTop.setOnClickListener {
            clearEditTextAndHideKeyboard()
            changeControlsStatus(false)
        }

        etComment.setOnFocusChangeListener { v, hasFocus ->
            bShare.visibility = if (hasFocus) View.GONE else View.VISIBLE
        }

        nestedScroll.setOnClickListener {
            changeControlsStatus(false)
        }

        rvQueue.setOnClickListener {
            changeControlsStatus(false)
        }
    }

    private fun clearEditTextAndHideKeyboard() {
        with(etComment) {
            setText("")
            clearFocus()
            hideKeyboard()
        }
    }

    private fun setupGestureListeners() {
        val minSwipeDistanceY = (resources.displayMetrics.heightPixels * 0.3).roundToInt()
        gestureDetector = GestureDetectorCompat(
            this,
            object : GestureDetector.SimpleOnGestureListener() {
                override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
                    val deltaY = e1.y - e2.y
                    val deltaYAbs = Math.abs(deltaY)
                    if (deltaYAbs >= minSwipeDistanceY && deltaY < 0) {
                        finish()
                        overridePendingTransition(0, R.anim.slide_to_bottom)
                        return true
                    }
                    return false
                }
            }
        )
    }

    private fun setupInputBehavior() {
        etComment.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                bComment.visibility = if (!charSequence.isEmpty()) View.VISIBLE else View.GONE
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    private fun setupKeyboardAction() {
        softkeyboardHelper = SoftInputUtil(this)
        etComment.setOnKeyboardActionListener { bComment.performClick() }
    }

    private fun showVolumeWarningIfNeeded() {
        (getSystemService(AUDIO_SERVICE) as? AudioManager)?.let {
            val current = it.getStreamVolume(AudioManager.STREAM_MUSIC)
            val max = it.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            if (current < max * .3) ViewUtils.showDialog(this, getString(R.string.live_viewer_message_volume), null)
        }
    }

    override fun setupShareButton(live: LiveViewModel) {
        if (!live.canShare) {
            bShare.visibility = View.GONE
        } else {

            isKeyboardHidden {
                etComment.text.clear()
                bShare.visibility = if (it) View.VISIBLE else View.GONE
            }

            bShare.visibility = View.VISIBLE
            bShare.setOnClickListener {
                liveViewerPresenter.onShareClicked()
                BranchUniversalObject().shareItem(live.shareViewModel, Share.Live, this, { showLoading() }, { hideLoading() })
            }
        }
    }

    override fun setupViewer(url: String) {
        try {
            player = Player(this, this, Uri.parse(url), sepvVideo)
        } catch (e: Exception) {
            FirebaseCrashlytics.getInstance().log("setupViewer error")
            FirebaseCrashlytics.getInstance().log("live url: $url")
            FirebaseCrashlytics.getInstance().log("message: ${e.message}")
            FirebaseCrashlytics.getInstance().recordException(e)
        }
    }

    override fun showCommentField() {
        llComment.visibility = View.VISIBLE
    }

    override fun addQueueItem(comment: QueueItem) {
        liveAdapter.addQueueItem(comment)
        keepFixedToBottom()
        enableFadingEdges()
    }

    private fun keepFixedToBottom() {
        val recentlyAddedItemPosition = linearLayoutManager.findLastVisibleItemPosition() + 1
        val lastItemPosition = liveAdapter.itemCount - 1
        val isFirstItemVisible = recentlyAddedItemPosition == lastItemPosition
        if (isFirstItemVisible) rvQueue.scrollToPosition(lastItemPosition)
    }

    private fun enableFadingEdges() {
        if (rvQueue.childCount == 0) return

        val itemHeight = resources.getDimensionPixelSize(R.dimen.live_queue_item_font_size) + 2 * resources.getDimensionPixelSize(
            R.dimen.live_queue_item_vertical_padding
        )
        val isScrollingPossible = rvQueue.computeVerticalScrollRange() > rvQueue.height - itemHeight
        if (isScrollingPossible) rvQueue.isVerticalFadingEdgeEnabled = true

        val fadingEdgeLength = resources.getDimensionPixelSize(R.dimen.live_queue_fading_edge_length)
        rvQueue.setFadingEdgeLength(fadingEdgeLength)
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(resId), onDismissListener)
    }

    override fun showLoading() {
        loading = ViewUtils.createLoadingOverlay(this@LiveViewerActivity)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun keepScreenOn(enabled: Boolean) {
        if (enabled) setFlagKeepScreenOn()
        else clearFlagKeepScreenOn()
    }

    override fun onBackPressed() {
        close()
    }

    override fun close() {
        if (isTaskRoot) startActivity(Intent(this, NavigationActivity::class.java))
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun setParticipantCount(count: Int) {
        if (count > 0) {
            if (controlsVisible) {
                llParticipants.visibility = View.VISIBLE
            }
            tvParticipantCounter.text = count.toString()
        } else if (count == 0) {
            llParticipants.visibility = View.GONE
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun setStreamingEnded() {
        liveEnded = true
    }

    override fun openVideo(url: String) {
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
        ActivityCompat.finishAfterTransition(this)
    }
}
