package co.socialsquad.squad.presentation.feature.login.email

import android.content.Intent
import android.os.Bundle
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordActivity
import co.socialsquad.squad.presentation.feature.signup.accesssquad.AccessSquadActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login_email.*
import javax.inject.Inject

class LoginEmailActivity : BaseDaggerActivity(), LoginEmailView {

    companion object {
        const val EXTRA_LOGIN_EMAIL = "EXTRA_LOGIN_EMAIL"
    }

    @Inject
    lateinit var presenter: LoginEmailPresenter

    public override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_email)

        presenter.onCreate(intent)

        bNext.isEnabled = false

        setupToolbar()
        setupKeyboard()
        setupKeyboardAction()
        setupNextButtonBehavior()
        setupOnClickListeners()
        // debugFields()
    }

    private fun debugFields() {
        if (BuildConfig.DEBUG) {
            iltInput.editText?.setText("admin@squad.com.br")
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(tToolbar)
    }

    private fun setupKeyboard() {
        iltInput.requestFocus()
    }

    private fun setupKeyboardAction() {
        iltInput.setOnKeyboardActionListener { bNext.performClick() }
    }

    private fun setupNextButtonBehavior() {
        iltInput.addTextChangedListener { text -> presenter.onInputChanged(text) }
    }

    private fun setupOnClickListeners() {
        ibBack.setOnClickListener { finish() }
        bNext.setOnClickListener { presenter.onNextClicked(iltInput.getFieldText().toString()) }
    }

    override fun enableNextButton(enabled: Boolean) {
        bNext.isEnabled = enabled
    }

    override fun showLoading() {
        iltInput.loading(true)
    }

    override fun hideLoading() {
        iltInput.loading(false)
    }

    override fun showEmptySubdomains() {
        iltInput.setErrorText("Nenhum squad encontrado para essa conta.")
    }

    override fun moveToPasswordScreen(company: Company, email: String) {
        val intent = Intent(this, LoginPasswordActivity::class.java)
                .putExtra(LoginPasswordActivity.extraCompany, Gson().toJson(company))
                .putExtra(LoginPasswordActivity.EXTRA_LOGIN_EMAIL, email)
                .putExtra(LoginPasswordActivity.EXTRA_LOGIN_SUBDOMAIN, company.subdomain)

        startActivity(intent)
    }

    override fun openSubdomainsScreen(companies: List<Company>, email: String) {
        val gson = Gson()
        val json = gson.toJson(companies)

        val intent = Intent(baseContext, AccessSquadActivity::class.java)
        intent.putExtra(AccessSquadActivity.extraEmail, email)
        intent.putExtra(AccessSquadActivity.extraCompanies, json)
        intent.putExtra(AccessSquadActivity.extraIsLogin, true)

        startActivity(intent)
    }
}
