package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemShowAllRankingViewModel(
        val title: String,
        val type: String)
    : ViewModel {
}