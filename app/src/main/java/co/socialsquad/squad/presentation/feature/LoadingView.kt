package co.socialsquad.squad.presentation.feature

interface LoadingView {
    fun showLoading(show: Boolean)
}
