package co.socialsquad.squad.presentation.util

import android.content.Context
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ViewModel
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.util.LinkProperties

interface Shareable {
    val shareViewModel: ShareViewModel
}

class ShareViewModel(
    var pk: Int,
    var title: String,
    var description: String?,
    var imageUrl: String?,
    var model: Share,
    val canShare: Boolean,
    val subdomain: String?
) : ViewModel

fun BranchUniversalObject.shareItem(shareViewModel: ShareViewModel, shareType: Share, context: Context, onStart: () -> Unit = {}, onLoaded: () -> Unit = {}) {
    if (shareViewModel.subdomain == null) return
    onStart()
    canonicalIdentifier = shareType.name + "/" + shareViewModel.pk
    title = shareViewModel.title
    setContentDescription(shareViewModel.description)
    shareViewModel.imageUrl?.let { setContentImageUrl(it) }
    setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    val lp = LinkProperties()
        .setFeature("sharing")
        .addControlParameter("pk", shareViewModel.pk.toString())
        .addControlParameter("model", shareType.name)
        .addControlParameter("subdomain", shareViewModel.subdomain)

    generateShortUrl(context, lp) { url, error ->
        onLoaded()
        if (error != null) return@generateShortUrl
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.share_to)))
    }
}

fun BranchUniversalObject.shareApp(context: Context, onStart: () -> Unit, onLoaded: () -> Unit) {
    onStart()
    setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    val lp = LinkProperties()
        .setFeature("sharing")

    generateShortUrl(context, lp) { url, error ->
        onLoaded()
        if (error != null) return@generateShortUrl
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.share_to)))
    }
}

fun BranchUniversalObject.shareImmobile(shareViewModel: ShareViewModel, shareType: Share, context: Context, onStart: () -> Unit = {}, onLoaded: () -> Unit = {}) {
    if (shareViewModel.subdomain == null) return
    onStart()
    canonicalIdentifier = shareType.name + "/" + shareViewModel.pk
    title = shareViewModel.title
    setContentDescription(shareViewModel.description)
    shareViewModel.imageUrl?.let { setContentImageUrl(it) }
    setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
    val lp = LinkProperties()
        .setFeature("sharing")
        .addControlParameter("pk", shareViewModel.pk.toString())
        .addControlParameter("model", shareType.name)
        .addControlParameter("subdomain", shareViewModel.subdomain)

    generateShortUrl(context, lp) { url, error ->
        onLoaded()
        if (error != null) return@generateShortUrl
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.share_to)))
    }
}
