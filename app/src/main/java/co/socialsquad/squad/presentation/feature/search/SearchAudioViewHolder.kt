package co.socialsquad.squad.presentation.feature.search

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_videos_item.view.*

class SearchAudioViewHolder(itemView: View) : ViewHolder<AudioViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: AudioViewModel) {
        with(viewModel) {

            glide.load(avatar)
                .circleCrop()
                .crossFade()
                .into(itemView.ivImage)

            itemView.short_title_lead.text = title

            itemView.tvAuthor.apply {
                text = author
                setOnClickListener { openUser(authorPk) }
            }

            val viewCountStringResId =
                if (playbackCount == 1L) R.string.view_count_singular
                else R.string.view_count_plural
            val views = itemView.context.getString(viewCountStringResId, TextUtils.toUnitSuffix(playbackCount))
            val timestamp = date.toDate()?.elapsedTime(itemView.context)
            itemView.tvViewCountAndTimestamp.text = listOfNotNull(views, timestamp).joinToString(" • ")

            itemView.setOnClickListener { openAudioDetail(viewModel.pk) }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }

    private fun openAudioDetail(pk: Int) {
        val intent = Intent(itemView.context, AudioDetailActivity::class.java).putExtra(AudioDetailActivity.EXTRA_AUDIO_PK, pk)
        itemView.context.startActivity(intent)
    }
}
