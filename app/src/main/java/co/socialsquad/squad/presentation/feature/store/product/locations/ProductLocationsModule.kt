package co.socialsquad.squad.presentation.feature.store.product.locations

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ProductLocationsModule {
    @Binds
    abstract fun view(productLocationsActivity: ProductLocationsActivity): ProductLocationsView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
