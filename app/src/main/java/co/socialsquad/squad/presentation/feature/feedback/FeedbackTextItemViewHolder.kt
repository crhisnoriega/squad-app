package co.socialsquad.squad.presentation.feature.feedback

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.view_feedback_text_item.view.*

class FeedbackTextItemViewHolder(
    itemView: View,
    private val onTextChanged: (viewModel: FeedbackItemViewModel.FeedbackTextItemViewModel) -> Unit?
) : ViewHolder<FeedbackItemViewModel.FeedbackTextItemViewModel>(itemView) {
    private var textWatcher: TextWatcher? = null

    override fun bind(viewModel: FeedbackItemViewModel.FeedbackTextItemViewModel) {
        itemView.it.apply {
            hint = viewModel.hint
            editText?.setLines(100)
            editText?.maxLines = 5
            editText?.setText(viewModel.text)
            textWatcher = object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    viewModel.text = s.toString()
                    onTextChanged(viewModel)
                }
            }
            editText?.addTextChangedListener(textWatcher)
            onTextChanged(viewModel)
        }
    }

    override fun recycle() {
        itemView.it.editText?.removeTextChangedListener(textWatcher)
        textWatcher = null
    }
}
