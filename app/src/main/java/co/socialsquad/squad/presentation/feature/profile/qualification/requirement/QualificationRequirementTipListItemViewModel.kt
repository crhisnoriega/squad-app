package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import co.socialsquad.squad.presentation.custom.ViewModel

class QualificationRequirementTipListItemViewModel(
    val number: Int,
    val title: String,
    val description: String
) : ViewModel
