package co.socialsquad.squad.presentation.feature.points.data

import co.socialsquad.squad.presentation.feature.points.model.SystemData
import co.socialsquad.squad.presentation.feature.points.model.SystemPointsResponse
import co.socialsquad.squad.presentation.feature.ranking.model.CriteriasResponse
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface PersonalPointAPI {

    @GET("/endpoints/identity/points-systems/")
    suspend fun fetchPointSystems(): SystemPointsResponse

    @GET("/endpoints/identity/points-systems/{points_system_id}")
    suspend fun fetchPointSystemsById(@Path("points_system_id") points_system_id: String): SystemData


    @GET("/endpoints/identity/plan/{plan_id}/")
    suspend fun fetchPlan(@Path("plan_id") planId: String): PlanData


    @POST("/endpoints/identity/plan/{plan_id}/accept-terms/")
    suspend fun agreeRecognitionTerms(@Path("plan_id") planId: String): Response<Void>

    @POST("/endpoints/identity/plan/{plan_id}/refuse-terms/")
    suspend fun refuseRecognitionTerms(@Path("plan_id") planId: String): Response<Void>

    @GET("/endpoints/identity/criteria/{criteria_id}")
    suspend fun fetchCriterias(
        @Path("criteria_id") criteriaId: String,
        @Query("area") area: String
    ): CriteriasResponse


}