package co.socialsquad.squad.presentation.feature.search

import co.socialsquad.squad.presentation.feature.store.FooterViewModel

class SearchSeeAllViewModel(val searchItem: SearchItem) : FooterViewModel()
