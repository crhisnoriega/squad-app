package co.socialsquad.squad.presentation.feature.ranking

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.RankingListViewModel
import co.socialsquad.squad.presentation.feature.ranking.customviews.OptOutBottomSheetDialog
import co.socialsquad.squad.presentation.feature.ranking.details.RankingBenefitDetailsActivity
import co.socialsquad.squad.presentation.feature.ranking.details.RankingCriteriaDetailsActivity
import co.socialsquad.squad.presentation.feature.ranking.di.RankingListModule
import co.socialsquad.squad.presentation.feature.ranking.listing.ElementsListingActivity
import co.socialsquad.squad.presentation.feature.ranking.listing.RANKING_LIST_TYPE
import co.socialsquad.squad.presentation.feature.ranking.model.*
import co.socialsquad.squad.presentation.feature.ranking.viewholder.*
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_ranking.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import kotlin.math.abs


class RankingActivity : BaseActivity() {

    companion object {

        const val RANKING_ID = "RANKING_ID"
        const val RANKING_DATA = "RANKING_DATA"

        fun start(
            context: Context, rankingId: String?
        ) {
            context.startActivity(Intent(context, RankingActivity::class.java).apply {
                putExtra(RANKING_ID, rankingId)
            })
        }
    }

    override val modules: List<Module> = listOf(RankingListModule.instance)
    override val contentView: Int = R.layout.activity_ranking

    private var currentPage: Int = 0;

    private lateinit var rankingItem: RankingItem

    private val viewModel by viewModel<RankingListViewModel>()
    private val rankingId by extra<String>(RANKING_ID)

//    private val viewPagerListener = object : ViewPager2.OnPageChangeCallback() {
//        override fun onPageSelected(position: Int) {
//            currentPage = position
//
//            var rank = viewModel.rankingItem.value?.data
//
//            rank?.let {
//                rankingItem = it
//            }
//
//            rank?.let { item ->
//                populateExpandedHeader(item)
//                populateLists(item)
//            }
//        }
//    }

    private fun startAnimationButton() {
        val appearBottomUp = AnimationUtils.loadAnimation(this, R.anim.animation_in_bottom)
        appearBottomUp.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {

            }

            override fun onAnimationStart(animation: Animation?) {
                containerButton.visibility = View.VISIBLE
            }

        })
        containerButton.startAnimation(appearBottomUp)

    }

    private fun populateExpandedHeader(rankingItem: RankingItem) {

        if (rankingItem.user_in_rank!!) {
            levelName.text = rankingItem?.header?.position_string
            tvTitle.text = rankingItem?.title
            levelDescription.text = rankingItem?.header?.score
        } else {
            levelName.text = rankingItem?.header?.title
            tvTitle.text = rankingItem?.title
            levelDescription.text = rankingItem?.header?.subtitle
        }

        bottomHeaderDivider.visibility = View.VISIBLE
        levelName.visibility = View.VISIBLE
        levelDescription.visibility = View.VISIBLE

        tvSubtitleCollapsed.visibility = View.GONE
        bottomHeaderDividerCollapsed.visibility = View.GONE
    }

    private fun populateCollapsedHeader(rankingItem: RankingItem) {
        tvTitle.text = rankingItem?.title


        if (rankingItem.user_in_rank!!) {
            tvSubtitleCollapsed.text =
                "${rankingItem?.header?.position_string} • ${rankingItem?.header?.score}"
        } else {
            tvSubtitleCollapsed.text = rankingItem?.header?.title
        }

        //tvSubtitleCollapsed.text = "${rankingItem?.header?.position_string} • ${rankingItem?.header?.score}"

        bottomHeaderDivider.visibility = View.GONE
        levelName.visibility = View.INVISIBLE
        levelDescription.visibility = View.INVISIBLE

        tvSubtitleCollapsed.visibility = View.VISIBLE
        bottomHeaderDividerCollapsed.visibility = View.VISIBLE
    }

    private fun populateLists(rankingItem: RankingItem) {
        rankingItem.benefits?.let {
            configureBenefitList(it)
        }

        rankingItem.positions?.let {
            configurePositionList(it)
        }

        rankingItem.criteria?.let {
            configureCriteriaList(it)
        }

        rankingItem.prizes?.let {
            configurePrizeList(it)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ranking)

        configureToolbar()
        //configureCarousel()
        //configurePositionList()
        //configureCriteriaList()
        //configureBenefitList()
        configureObservers()
        configureOptInButton()
        configureOptOutButton()

        Log.i("rank", "ranking id: $rankingId")

        viewModel.fetchRankDetail(rankingId)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.WHITE
        }


    }

    private fun configureOptOutButton() {
        optOutBtn.setOnClickListener {
            var rank = viewModel.rankingItem.value?.data
            var dialog = OptOutBottomSheetDialog(rank!!) {
                when (it) {
                    "share" -> {
                    }
                    "optout" -> {
                        showLoading()

                        Handler().postDelayed({
                            viewModel.refuseRankTerms(rankingId)
                        }, 1000L)

                    }
                }
            }
            dialog.show(supportFragmentManager, "optout")
        }
    }


    private fun showLoading() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.GRAY
        }

        optInLoading.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        iconLoading.startAnimation(rotation)
    }

    private fun configureOptInButton() {
        optInButton.setOnClickListener {
            showLoading()
            Handler().postDelayed({
                viewModel.agreeRankTerms(rankingId)
            }, 1000L)
        }

        btConfirm1.setOnClickListener {
            showLoading()
            Handler().postDelayed({
                viewModel.agreeRankTerms(rankingId)
            }, 1000L)
        }

        termText.setOnClickListener {
            // var ranking = viewModel.rankingItem.value?.data?.data?.get(0)
            // RankingTermsActivity.start(this, ranking?.rank, ranking?.terms)
        }
    }

    private fun configureObservers() {

        viewModel.rankingItem.observe(this, Observer {
            Log.i("rank", "status: $it")
            when (it.status) {
                Status.SUCCESS -> {
                    configureAppLayout()
                    configureCarousel()
                    // viewPagerListener.onPageSelected(0)

                    populateExpandedHeader(viewModel.rankingItem.value?.data!!)
                    populateLists(viewModel.rankingItem.value?.data!!)

                    rankingShimmer.visibility = View.GONE
                    nestedScroll.visibility = View.VISIBLE
                    app_bar.visibility = View.VISIBLE

                    var rank = viewModel.rankingItem.value?.data

                    Log.i("abc", Gson().toJson(viewModel.rankingItem.value))

                    rank?.user_in_rank?.let { toShow ->

                        optOutBtn.isVisible = toShow

                        if (toShow.not())
                            startAnimationButton()

                        if (toShow.not()) {
                            dividerHeaderPositionBBWithButtom.visibility = View.VISIBLE
                            dividerHeaderPositionBB.visibility = View.GONE
                        } else {
                            dividerHeaderPositionBBWithButtom.visibility = View.GONE
                            dividerHeaderPositionBB.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })


        viewModel.agreeRankTerms.observe(this, Observer {
            Log.i("rank2", "agree status: $it")
            when (it.status) {
                Status.SUCCESS -> {
                    ElementsListingActivity.doRefresh = true
                    finish()
                    RankingActivity.start(this@RankingActivity, rankingId)
                }
                Status.ERROR -> {

                }
            }
        })


        viewModel.refuseRankTerms.observe(this, Observer {
            Log.i("rank2", "agree status: $it")
            when (it.status) {
                Status.SUCCESS -> {
                    ElementsListingActivity.doRefresh = true
                    finish()
                    RankingActivity.start(this@RankingActivity, rankingId)
                }
                Status.ERROR -> {

                }
            }
        })
    }

    private fun configureCarousel() {
        tvValue.orientation = ViewPager2.ORIENTATION_HORIZONTAL;
        tvValue.offscreenPageLimit = 3;

        var density = resources.displayMetrics.density

        Log.i("rank", "density: $density")

        tvValue.apply {
            offscreenPageLimit = 1
            val recyclerView = getChildAt(0) as RecyclerView
            recyclerView.apply {
                val padding = 108 * density
                setPadding(padding.toInt(), 0, padding.toInt(), 0)
                clipToPadding = false
            }
        }

        tvValue.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPage = position
                tvValue.post {
                    tvValue.adapter?.notifyDataSetChanged()
                }
            }
        })

        tvValue.setPageTransformer { page, position ->
            if (-0.8f <= position && position <= 0.8f) {
                val scaleFactor = 1 - (0.3f * abs(position))
                page.scaleX = scaleFactor
                page.scaleY = scaleFactor

            } else {
                //val scaleFactor = max(0.7f, 1 - abs(position - 0.14285715f))
                val scaleFactor = 0.76f
                page.scaleX = scaleFactor
                page.scaleY = scaleFactor
            }
        }

        tvValue.adapter = object : RecyclerView.Adapter<CarouselViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselViewHolder {
                val view: View = LayoutInflater.from(this@RankingActivity)
                    .inflate(R.layout.item_ranking_carousel, parent, false)
                return CarouselViewHolder(view)
            }

            override fun getItemCount(): Int {
                return 1
            }

            override fun onBindViewHolder(holder: CarouselViewHolder, position: Int) {
                //Glide.with(holder.itemView.context).load(viewModel.rankingList.value?.data?.data?.get(position)?.rank?.icon!!).circleCrop().into(holder.img)


                GlideToVectorYou.init().with(holder.itemView.context)
                    .load(Uri.parse(viewModel.rankingItem.value?.data?.icon!!), holder.img)
            }

        }

        //carousel.registerOnPageChangeCallback(viewPagerListener)
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.title = ""
        toolbar_layout.isTitleEnabled = false
    }

    private fun configureAppLayout() {
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->

            if (verticalOffset <= -1 * (app_bar.totalScrollRange - 100)) {
                viewModel.rankingItem.value?.let {
                    populateCollapsedHeader(it.data!!)
                }
            } else {
                viewModel.rankingItem.value?.let {
                    Log.i("rank", Gson().toJson(it))
                    populateExpandedHeader(it.data!!)
                }
            }
        })
    }

    private fun configureBenefitList(benefitList: BenefitList) {
        rvBenefits.apply {
            layoutManager =
                LinearLayoutManager(this@RankingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemBenefitRankingViewModel -> R.layout.item_ranking_benefit
                        is ItemShowAllRankingViewModel -> R.layout.item_ranking_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_ranking_benefit -> ItemBenefitRankingViewHolder(view) {
                        RankingBenefitDetailsActivity.start(this@RankingActivity, it.id, rankingId)
                    }
                    R.layout.item_ranking_show_all -> ItemShowAllRankingViewHolder(view) {
                        ElementsListingActivity.start(
                            this@RankingActivity,
                            RANKING_LIST_TYPE.BENEFIT,
                            listOf<ItemPositionRankingViewModel>(),
                            rankingId,
                            viewModel.rankingItem.value?.data?.user_in_rank!!,

                            titleAPI = RANKING_LIST_TYPE.BENEFIT.title
                        )
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter
            benefitList.list?.let { list ->
                var models = list.map {
                    ItemBenefitRankingViewModel(
                        it.id!!,
                        it.title!!,
                        "",
                        it.description ?: "",
                        it.icon ?: ""
                    ) as ViewModel
                }.toMutableList()

                benefitList.show_all?.let {
                    if (it) models.add(ItemShowAllRankingViewModel("", ""))
                }

                factoryAdapter.setItems(models)

                tvBenefitLabel.text = if (list.size == 1) {
                    "Benefício"
                } else {
                    "Benefícios"
                }

                if (list.isEmpty()) {
                    titleBenefit.isVisible = false
                    rvBenefits.isVisible = false
                    bottomHeaderDividerBB.isVisible = false
                    dividerHeaderPositionBB.isVisible = false
                    dividerHeaderPositionBBWithButtom.isVisible = false
                }
            }
        }
    }

    private fun configureCriteriaList(criteriaList: CriteriaList) {
        rvCriterias.apply {
            layoutManager =
                LinearLayoutManager(this@RankingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemCriteriaRankingViewModel -> R.layout.item_ranking_criteria
                        is ItemShowAllRankingViewModel -> R.layout.item_ranking_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_ranking_criteria -> ItemCriteriaRankingViewHolder(view) {
                        RankingCriteriaDetailsActivity.start(
                            this@RankingActivity,
                            it.id,
                            viewModel.rankingItem.value?.data?.user_in_rank!!
                        )
                    }
                    R.layout.item_ranking_show_all -> ItemShowAllRankingViewHolder(view) {
                        ElementsListingActivity.start(
                            this@RankingActivity,
                            RANKING_LIST_TYPE.CRITERIA,
                            listOf<ItemPositionRankingViewModel>(),
                            viewModel.rankingItem.value?.data?.id.toString(),
                            viewModel.rankingItem.value?.data?.user_in_rank!!,
                            titleAPI = RANKING_LIST_TYPE.CRITERIA.title
                        )
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            criteriaList.list?.let { list ->
                var models = list.map {
                    ItemCriteriaRankingViewModel(
                        it.id!!,
                        it.title!!,
                        it.description!!,
                        it.icon!!,
                        it.points!!,
                        viewModel.rankingItem.value?.data?.user_in_rank!!,
                        showDetails = it.show_details
                    ) as ViewModel
                }.toMutableList()

                criteriaList.show_all?.let {
                    if (it) models.add(ItemShowAllRankingViewModel("", ""))
                }

                factoryAdapter.setItems(models)

                if (list.isEmpty()) {
                    rvCriterias.isVisible = false
                    titleCriterias.isVisible = false
                    criteriasDivider001.isVisible = false
                    criteriasDivider002.isVisible = false
                    criteriasDivider003.isVisible = false

                }
            }
        }
    }

    private fun configurePositionList(positionList: PositionList) {
        rvPositions.apply {
            layoutManager =
                LinearLayoutManager(this@RankingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemPositionRankingViewModel -> R.layout.item_ranking_position
                        is ItemShowAllRankingViewModel -> R.layout.item_ranking_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_ranking_position -> ItemPositionRankingViewHolder(view) {

                    }
                    R.layout.item_ranking_show_all -> ItemShowAllRankingViewHolder(view) {
                        ElementsListingActivity.start(
                            this@RankingActivity,
                            RANKING_LIST_TYPE.POSITION,
                            listOf<ItemPositionRankingViewModel>(),
                            viewModel.rankingItem.value?.data?.id.toString()!!,
                            viewModel.rankingItem.value?.data?.user_in_rank!!,
                            titleAPI = RANKING_LIST_TYPE.POSITION.title
                        )
                    }.apply {
                        setText("Mostrar Todas")
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            positionList.list?.let { list ->
                var models = list.map {
                    ItemPositionRankingViewModel(
                        it.user?.full_name!!,
                        it.position!!,
                        it.score!!,
                        it.user?.avatar
                    ) as ViewModel
                }.toMutableList()

                positionList.show_all?.let {
                    if (it) models.add(ItemShowAllRankingViewModel("", ""))
                }

                factoryAdapter.addItems(models)

                Log.i("pos", "update with size: ${models.size} elem: ${Gson().toJson(models)}")
                if (list.isEmpty()) {
                    bottomHeaderDivider2.isVisible = false
                }
            }

        }

        // voltar
        rvPositions.isVisible = viewModel.rankingItem.value?.data?.user_in_rank!!
        titlePositions.isVisible = viewModel.rankingItem.value?.data?.user_in_rank!!
        positionDivider001.isVisible = viewModel.rankingItem.value?.data?.user_in_rank!!
        positionDivider002.isVisible = viewModel.rankingItem.value?.data?.user_in_rank!!
        positionDivider003.isVisible = viewModel.rankingItem.value?.data?.user_in_rank!!

        rvPositions.adapter?.notifyDataSetChanged()
    }


    private fun configurePrizeList(prizeList: PrizeList) {
        rvPrizes.apply {
            layoutManager =
                LinearLayoutManager(this@RankingActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemPrizeRankingViewModel -> R.layout.item_ranking_prize
                        is ItemShowAllRankingViewModel -> R.layout.item_ranking_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_ranking_prize -> ItemPrizeRankingViewHolder(view) {
                        // RankingBenefitDetailsActivity.start(this@RankingActivity, it.id)
                    }
                    R.layout.item_ranking_show_all -> ItemShowAllRankingViewHolder(view) {
                        ElementsListingActivity.start(
                            this@RankingActivity,
                            RANKING_LIST_TYPE.PRIZES,
                            listOf<ItemPositionRankingViewModel>(),
                            "",
                            viewModel.rankingItem.value?.data?.user_in_rank!!,
                            titleAPI = RANKING_LIST_TYPE.PRIZES.title
                        )
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            prizeList.list?.let { list ->
                var models = list.map {
                    ItemPrizeRankingViewModel(it) as ViewModel
                }.toMutableList()

                prizeList.show_all?.let {
                    if (it) models.add(ItemShowAllRankingViewModel("", ""))
                }

                factoryAdapter.setItems(models)

                tvPrizesLabel.text = prizeList.title

                if (list.isEmpty()) {
                    rvPrizes.isVisible = false
                    titlePrizes.isVisible = false
                    benefitsDivider001.isVisible = false
                    benefitsDivider002.isVisible = false
                    benefitsDivider003.isVisible = false
                }
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}


class CarouselViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val img = itemView.findViewById<ImageView>(R.id.img)
}