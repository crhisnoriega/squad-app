package co.socialsquad.squad.presentation.feature.existinglead

import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.SubmissionCreation
import co.socialsquad.squad.domain.model.form.FormUpdate
import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadSeparator
import co.socialsquad.squad.domain.model.widget.Widget
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.existinglead.repository.ExistingLeadRepository
import co.socialsquad.squad.presentation.feature.newlead.repository.NewLeadRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class ExistingLeadViewModel(
        private val repository: ExistingLeadRepository,
        private val repositoryNewLead: NewLeadRepository
) : BaseViewModel() {

    @ExperimentalCoroutinesApi
    val existingLeads: StateFlow<Resource<List<ExistingLeadSeparator>>>
        get() = _existingLeads

    @ExperimentalCoroutinesApi
    val formUpdateResponse: StateFlow<Resource<String>>
        get() = _formUpdateResponse

    @ExperimentalCoroutinesApi
    private val _existingLeads = MutableStateFlow(Resource.empty<List<ExistingLeadSeparator>>())

    @ExperimentalCoroutinesApi
    private val _formUpdateResponse = MutableStateFlow(Resource.empty<String>())

    var searchTerm: String = ""
    var nextLink: String? = ""

    fun searchLeads(next: String? = null, terms: String = "") {
        this.searchTerm = terms
        this.nextLink = next

        val link = nextLink?.let { "endpoints$it" }
                ?: run { "endpoints/lead/search?search_term=${this.searchTerm}" }

        viewModelScope.launch {
            _existingLeads.value = Resource.loading(null)
            repository.searchExistingLead(link)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        _existingLeads.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        nextLink = it.data.next
                        if (it.data == null) {
                            _existingLeads.value = Resource.empty()
                        } else {
                            it.data.items?.let { leads ->
                                if (leads.isNotEmpty()) {
                                    _existingLeads.value = Resource.success(leads)
                                } else {
                                    _existingLeads.value = Resource.empty()
                                }
                            } ?: run {
                                _existingLeads.value = Resource.empty()
                            }
                        }
                    }
        }
    }

    fun createLead(submissionCreation: SubmissionCreation, widget: Widget, leadId: Long) {
        viewModelScope.launch {
            _formUpdateResponse.value = Resource.loading(null)
            var formUpdate = FormUpdate(hashMapOf())
            formUpdate.data?.put("id", leadId)
            repositoryNewLead.formUpdate(submissionCreation.id, widget.id, formUpdate)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        e.printStackTrace()
                        _formUpdateResponse.value = Resource.error(e.toString(), null)
                    }.collect {
                        _formUpdateResponse.value = Resource.success(it)
                    }
        }
    }
}
