package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.recognition.RecognitionActivity
import co.socialsquad.squad.presentation.feature.recognition.listing.RECOGNITION_LIST_TYPE
import co.socialsquad.squad.presentation.feature.recognition.listing.RecognitionElementsListingActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_card_ranking_profile.view.*
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.*
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.info
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.rlContactCircle2
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.short_description
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.short_title

class ProfileRecognitionViewHolder(itemView: View) : ViewHolder<ProfileRecognitionViewModel>(itemView) {

    override fun recycle() {
    }

    override fun bind(viewModel: ProfileRecognitionViewModel) {
        itemView.short_title.text = viewModel.recognitionProfileModel?.title
        itemView.short_description.text = viewModel.recognitionProfileModel?.subtitle

        //Glide.with(itemView.context).load(viewModel.recognitionProfileModel?.icon).circleCrop().into(itemView.imgRecognition)

        itemView.rlContactCircle2.backgroundTintList =
            ColorUtils.getCompanyColor(itemView.context)?.let { ColorUtils.stateListOf(it) }

        itemView.info.setOnClickListener {
            viewModel.recognitionProfileModel?.multiple_plans?.let {
                if (it) {
                    RecognitionElementsListingActivity.start(itemView.context, RECOGNITION_LIST_TYPE.PLANS_INFO, viewModel?.recognitionProfileModel!!)
                } else {
                    RecognitionActivity.start(itemView.context, viewModel.recognitionProfileModel?.plan_id!!)
                }

            }
        }
    }
}
