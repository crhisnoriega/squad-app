package co.socialsquad.squad.presentation.feature.existinglead

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.SubmissionCreation
import co.socialsquad.squad.domain.model.leadsearch.ExistingLead
import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadSeparator
import co.socialsquad.squad.domain.model.widget.Widget
import co.socialsquad.squad.presentation.App
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.existinglead.adapter.loading.LeadEmptyStateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.loading.LeadLoadingStateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.loading.LeadNotFoundStateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.LeadResultsAdapter
import co.socialsquad.squad.presentation.feature.existinglead.di.ExistingLeadModule
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity
import co.socialsquad.squad.presentation.feature.lead.LeadActivity
import co.socialsquad.squad.presentation.feature.opportunityv2.OpportunityDetailsActivity
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.SearchLoadingAdapter
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.hideKeyboard
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_existing_lead.*
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class ExistingLeadActivity : BaseActivity() {


    companion object {

        private const val EXTRA_SUBMISSION = "submission"
        private const val EXTRA_WIDGET = "widget"
        private const val EXTRA_OBJECT_TYPE = "EXTRA_OBJECT_TYPE"

        fun startForResult(activity: Activity, submissionCreation: SubmissionCreation, widget: Widget, code: Int, objectType: String? = "") {
            val intent = Intent(activity, ExistingLeadActivity::class.java).apply {
                putExtra(EXTRA_SUBMISSION, submissionCreation)
                putExtra(EXTRA_WIDGET, widget)
                putExtra(EXTRA_OBJECT_TYPE, objectType)
            }
            activity.startActivityForResult(intent, code)
        }
    }

    private val checkedListener = object : LeadResultsAdapter.CheckedListener {
        override fun onChecked(checked: Boolean, existingLeadVM: ExistingLeadVM) {
            btnConfirmar.isEnabled = true
        }
    }

    override val modules: List<Module> = ExistingLeadModule.instance
    override val contentView: Int = R.layout.activity_existing_lead

    private val submissionCreation: SubmissionCreation by extra(EXTRA_SUBMISSION)
    private val objectType: String by extra(EXTRA_OBJECT_TYPE)
    private val widget: Widget by extra(EXTRA_WIDGET)
    private val viewModel by viewModel<ExistingLeadViewModel>()

    private var firstTime = true;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupBackButton()
        setupNextButton()
        setupLeadsCreateObservable()
        setupLeadsObservable()
        setupSearchBar()

        var linearLayoutManager = LinearLayoutManager(this)
        rvLeadList.layoutManager = linearLayoutManager
        rvLeadList.adapter = LeadLoadingStateAdapter()

        rvLeadList.addOnScrollListener(EndlessScrollListener(10, linearLayoutManager, {
            if (viewModel.existingLeads.value.status != Status.LOADING && viewModel.nextLink != null) {
                viewModel.searchLeads(viewModel.nextLink, viewModel.searchTerm)
            }
        }))

        rvLeadList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                var currentAdapter = rvLeadList.adapter as? LeadResultsAdapter
                currentAdapter?.let {
                    it.leads?.get(linearLayoutManager.findFirstVisibleItemPosition())?.existingLeadSeparator?.let { item ->
                        currentHeaderLayout.visibility = View.VISIBLE
                        currentHeader.text = item.title.toUpperCase()
                    } ?: kotlin.run {

                    }
                }
            }
        })
        viewModel.searchLeads()
    }

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)


    }

    private var buttonText = ""

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE
        buttonIcon.clearAnimation()
    }


    private var existingLead: ExistingLead? = null

    private fun setupNextButton() {
        btnConfirmar.text = widget.submitButton.text

        btnConfirmar.setOnClickListener {
            if (rvLeadList.adapter is LeadResultsAdapter) {
                (rvLeadList.adapter as LeadResultsAdapter).getSelectedLead()?.let {
                    existingLead = it.existingLead
                    it.existingLead?.id?.let { leadId ->
                        Log.i("ll", Gson().toJson(submissionCreation))
                        btnConfirmar.isEnabled = false
                        showLoading()
                        Handler().postDelayed({ viewModel.createLead(submissionCreation, widget, leadId) }, 1000)
                    }
                }
            }
        }
    }

    private var wasCleaned = false
    private var lastSearchTerm = ""
    private fun setupSearchBar() {

        ibClear.setOnClickListener {
            wasCleaned = true
            currentHeaderLayout.visibility = View.GONE
            viewModel.searchLeads()
            etSearch.setText("")
            hideKeyboard()
        }

        etSearch.apply {
            addTextChangedListener(object : TextWatcher {
                private var searchFor = ""

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(query: CharSequence?, start: Int, before: Int, count: Int) {
                    val searchText = query.toString().trim()
                    lastSearchTerm = searchText
                    if (searchText == searchFor)
                        return

                    if (searchText.isNullOrBlank()) ibClear.visibility = View.GONE else ibClear.visibility = View.VISIBLE

                    if (wasCleaned) {
                        wasCleaned = false
                        return
                    }


                    searchFor = searchText
                    setupSearching(searchFor)

                    handler.removeCallbacksAndMessages(null)
                    handler.postDelayed({

                        lifecycleScope.launch {
                            delay(500) // debounce timeOut

                            if (searchText != searchFor)
                                return@launch

                            // setupEmptyResults()
                            query?.let {
                                viewModel.searchLeads(null, it.toString())
                            } ?: run {
                                viewModel.searchLeads(null, "")
                            }
                        }

                    }, 800)

                }

                override fun afterTextChanged(p0: Editable?) {}
            })
        }
    }

    private fun setupEmptyResults() {
        currentHeaderLayout.visibility = View.GONE
        rvLeadList.adapter = LeadEmptyStateAdapter()
    }

    private fun setupNotFoundResults() {
        currentHeaderLayout.visibility = View.GONE
        rvLeadList.adapter = LeadNotFoundStateAdapter(lastSearchTerm)
    }

    private fun setupSearching(term: String) {
        //rvLeadList.adapter = LeadSearchingAdapter(viewModel.searchTerm)
        if (rvLeadList.adapter is SearchLoadingAdapter) {
            (rvLeadList.adapter as SearchLoadingAdapter).updateSearchTerm(term)
        } else {
            rvLeadList.adapter = SearchLoadingAdapter(term)
        }
    }

    private fun setupLoadingState2() {
        rvLeadList.adapter = LeadLoadingStateAdapter()
    }

    private fun setupSearchResults(results: List<ExistingLeadSeparator>) {
        if (rvLeadList.adapter !is LeadResultsAdapter) {
            rvLeadList.adapter = LeadResultsAdapter(App.companyColor?.primaryColor, checkedListener)
        }
        (rvLeadList.adapter as LeadResultsAdapter).addGrouping(results)
    }

    private fun setupLeadsCreateObservable() {
        lifecycleScope.launch {
            viewModel.formUpdateResponse.collect {

                when (it.status) {
                    Status.SUCCESS -> {

                        hideLoading()
                        val dialogBuilder = AlertDialog.Builder(this@ExistingLeadActivity, R.style.TransparentDialog)
                        val dialogView = layoutInflater.inflate(R.layout.dialog_newlead_success, null)
                        dialogBuilder.setView(dialogView)
                        dialogBuilder.setCancelable(false)

                        val alertDialog = dialogBuilder.create()

                        dialogView.findViewById<TextView>(R.id.btnVisualizar).setOnClickListener {
                            alertDialog.dismiss()

                            this@ExistingLeadActivity.setResult(LeadActivity.RESULT_RELOAD_OPPORTUNITY)
                            this@ExistingLeadActivity.finish()

                            ExploreItemObjectActivity.doRefresh = true
                            OpportunityDetailsActivity.start(RankingType.Oportunidade, objectType, submissionCreation.id.toString(), this@ExistingLeadActivity, initials = existingLead?.avatar?.initials)
                        }
                        dialogView.findViewById<TextView>(R.id.btnEnviarOutro).setOnClickListener {
                            alertDialog.dismiss()
                            this@ExistingLeadActivity.setResult(LeadActivity.RESULT_CREATE_NEW_OPPORTUNITY)
                            this@ExistingLeadActivity.finish()
                        }
                        dialogView.findViewById<TextView>(R.id.btnFechar).setOnClickListener {
                            alertDialog.dismiss()
                            this@ExistingLeadActivity.setResult(LeadActivity.RESULT_RELOAD_OPPORTUNITY)
                            this@ExistingLeadActivity.finish()

                            ExploreItemObjectActivity.doRefresh = true
                        }

                        alertDialog.show()
                    }
                    Status.EMPTY -> {
                    }
                    Status.ERROR -> {
                        Toast.makeText(
                                this@ExistingLeadActivity,
                                getString(R.string.new_lead_error),
                                Toast.LENGTH_LONG
                        )
                                .show()
                    }
                    Status.LOADING -> {

                    }
                }
            }
        }
    }

    private fun setupLeadsObservable() {
        lifecycleScope.launch {
            viewModel.existingLeads.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.let { results ->
                            setupSearchResults(results)
                        }
                    }
                    Status.EMPTY -> {
                        if (firstTime) {
                            setupEmptyResults()
                            firstTime = false
                        } else {
                            setupNotFoundResults()
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(
                                this@ExistingLeadActivity,
                                getString(R.string.new_lead_error),
                                Toast.LENGTH_LONG
                        )
                                .show()
                    }
                    Status.LOADING -> {
                        if (viewModel.searchTerm.isEmpty()) {
                            //setupLoadingState()
                        } else {
                            //setupSearching()
                        }
                    }
                }
            }
        }
    }

    private fun setupBackButton() {
        ibClose.setOnClickListener {
            finish()
        }
    }
}
