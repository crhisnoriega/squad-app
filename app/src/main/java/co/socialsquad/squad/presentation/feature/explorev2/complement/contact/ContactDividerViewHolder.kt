package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder

class ContactDividerViewHolder(
    itemView: View,
) : ViewHolder<ContactDividerViewHolderModel>(itemView) {


    companion object{
        const val VIEW_ITEM_POSITION_ID = R.layout.item_contact_divider
    }

    override fun bind(viewModel: ContactDividerViewHolderModel) {

    }

    override fun recycle() {
    }
}