package co.socialsquad.squad.presentation.feature.audio.record

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.Menu
import android.view.MenuItem
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.feature.audio.AudioManager
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.audio.create.AudioCreateActivity
import kotlinx.android.synthetic.main.activity_audio_record.*
import javax.inject.Inject

class AudioRecordActivity : BaseDaggerActivity(), AudioRecordView {

    @Inject
    lateinit var audioRecordPresenter: AudioRecordPresenter

    private var miPost: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_record)
        setupToolbar()
        setupOnClickListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_audio_record, menu)
        miPost = menu.findItem(R.id.menu_audio_record_next)
        miPost?.isEnabled = false
        miPost?.setOnMenuItemClickListener {
            audioRecordPresenter.onPostClicked()
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        title = getString(R.string.audio_create_title)
    }

    private fun setupOnClickListeners() {
        ivRecord.setOnClickListener {
            audioRecordPresenter.onRecordClicked()
            resetChronometer()
            cElapsedTime.start()
        }

        ivStop.setOnClickListener {
            audioRecordPresenter.onStopClicked()
            resetChronometer()
        }
        tvRecordAgain.setOnClickListener {
            audioRecordPresenter.onResetClicked()
        }
    }

    private fun resetChronometer() {
        cElapsedTime.stop()
        cElapsedTime.base = SystemClock.elapsedRealtime()
    }

    override fun showStopButton() {
        ivCircle.visibility = View.VISIBLE
        cElapsedTime.visibility = View.VISIBLE
        ivRecord.visibility = View.GONE
        tvRecord.visibility = View.GONE
        ivStop.visibility = View.VISIBLE
        clRecordedActions.visibility = View.GONE
    }

    override fun showRecordButton() {
        ivCircle.visibility = View.VISIBLE
        cElapsedTime.visibility = View.VISIBLE
        tvRecord.visibility = View.VISIBLE
        ivRecord.visibility = View.VISIBLE
        ivStop.visibility = View.GONE
        clRecordedActions.visibility = View.GONE
    }

    override fun showPlayer() {
        ivCircle.visibility = View.GONE
        cElapsedTime.visibility = View.GONE
        tvRecord.visibility = View.GONE
        ivRecord.visibility = View.GONE
        ivStop.visibility = View.GONE
        clRecordedActions.visibility = View.VISIBLE
    }

    override fun setupPlayer(audioViewModel: AudioViewModel) {
        apAudio.init(audioViewModel)
    }

    override fun enableNextOption() {
        miPost?.isEnabled = true
    }

    override fun disableNextOption() {
        miPost?.isEnabled = false
    }

    override fun openAudioCreateActivity(audioUri: String) {
        val intent = Intent(this, AudioCreateActivity::class.java).apply {
            putExtra(AudioCreateActivity.EXTRA_AUDIO_RECORDED_FILE_URI, audioUri)
        }
        startActivity(intent)
        finish()
    }

    override fun onStop() {
        resetChronometer()
        apAudio.recycle()
        AudioManager.release()
        audioRecordPresenter.onStop()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        audioRecordPresenter.onResume()
    }
}
