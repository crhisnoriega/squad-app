package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class MediaResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("mime_type") var mime_type: String?,
    @SerializedName("url") var url: String?,
    @SerializedName("ext") var ext: String?
) : Parcelable {
}