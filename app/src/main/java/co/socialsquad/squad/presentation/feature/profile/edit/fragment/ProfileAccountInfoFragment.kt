package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.customview.ChooseAccountTypeSheetDialog
import kotlinx.android.synthetic.main.fragment_edit_profile_account_info.*
import kotlinx.android.synthetic.main.fragment_edit_profile_account_info.btnConfirmar
import kotlinx.android.synthetic.main.fragment_edit_profile_account_info.buttonIcon
import kotlinx.android.synthetic.main.fragment_edit_profile_account_info.ibBack
import kotlinx.android.synthetic.main.fragment_edit_profile_account_info.imgLead
import kotlinx.android.synthetic.main.fragment_edit_profile_bank_form.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ProfileAccountInfoFragment(
    var isInCompleteProfile: Boolean? = false
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()


    var listener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(sa: CharSequence?, start: Int, before: Int, count: Int) {
            btnConfirmar.isEnabled = agencyAccount.text.isNullOrBlank().not() &&
                    numberAccount.text.isNullOrBlank().not() &&
                    typeAccount.text.isNullOrBlank().not()
        }

        override fun afterTextChanged(s: Editable?) {

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupContent()
        setupNextButton()
        setupSkipButton()
        setupBack()
        setupHeader()

        agencyAccount.addEditTextChangedListener(listener)
        typeAccount.addEditTextChangedListener(listener)
        numberAccount.addEditTextChangedListener(listener)


        if (isInCompleteProfile!!.not())
            populateData()


        if (isInCompleteProfile!!) {
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_teddoc))
        }

    }

    override fun onResume() {
        super.onResume()
        listener.onTextChanged("", 0, 0, 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_account_info, container, false)

    private fun setupHeader() {

    }

    private fun setupBack() {
        ibBack.setOnClickListener {
            viewModel.state.postValue(StateNav("previous", true))
            (activity as? CompleteProfileActivity)?.previous()
        }
    }

    private fun setupContent() {


        accountTypeLayoutArea.setOnClickListener {
            var dialog = ChooseAccountTypeSheetDialog(typeAccount.text) { value, label ->
                typeAccount.text = label
            }
            dialog.show(childFragmentManager, "")
        }

        agencyAccount.setEditInputType(InputType.TYPE_CLASS_NUMBER)
        agencyAccount.setEditInputMask("####")
        agencyAccount.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                agencyCounter.text = "${s.toString().length}/4"
                if (s.toString().length == 4)
                    numberAccount.onFocusGained()
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        numberAccount.setEditInputType(InputType.TYPE_CLASS_NUMBER)
        numberAccount.setEditInputMask("###########")
        numberAccount.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                numberCounter.text = "${s.toString().length}/11"
                if (s.toString().length == 11)
                    btnConfirmar.isEnabled = true
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }


    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    private fun setupNextButton() {
        btnConfirmar.setOnClickListener {

            btnConfirmar.isEnabled = false
            hideKeyboard(requireActivity())
            viewModel.storeAccountInfo(
                viewModel.selectBankInfo?.code_number,
                typeAccount.text != "Conta Corrente",
                agencyAccount.text,
                numberAccount.text
            )
            viewModel.state.value = StateNav("next", true)
            (activity as? CompleteProfileActivity)?.next()
        }

    }

    private fun populateData() {
        var user = viewModel.userCompany?.user
        user?.let {
            agencyAccount.text = user.bank_account_branch
            numberAccount.text = user.bank_account_number
            typeAccount.text =
                if (user.bank_account_savings!!) "Conta Poupança" else "Conta Corrente"
        }
    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
