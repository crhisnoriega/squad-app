package co.socialsquad.squad.presentation.feature.kickoff.inviteteam

import dagger.Binds
import dagger.Module

@Module
interface InviteYourTeamModule {

    @Binds
    fun view(inviteYourTeamFragment: InviteYourTeamFragment): InviteYourTeamView
}
