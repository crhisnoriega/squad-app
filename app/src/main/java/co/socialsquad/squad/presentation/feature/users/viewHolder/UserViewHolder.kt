package co.socialsquad.squad.presentation.feature.users.viewHolder

import android.content.Intent
import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_user_list_item.view.*

class UserViewHolder(itemView: View) : ViewHolder<UserViewModel>(itemView) {

    private val glide = Glide.with(itemView)

    override fun bind(viewModel: UserViewModel) {
        if (adapterPosition == 0) {
            itemView.outerBound.visibility = View.VISIBLE
        }
        itemView.tvUserQualification.text = viewModel.qualification ?: ""
        itemView.tvUserName.text = viewModel.name
        glide.load(viewModel.avatarUrl)
            .circleCrop()
            .crossFade()
            .into(itemView.ivUserAvatar)
        itemView.setOnClickListener {
            openUserActivity(viewModel.pk)
        }
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, creatorPK)
        }
        itemView.context.startActivity(intent)
    }

    override fun recycle() {
        glide.clear(itemView.ivUserAvatar)
    }
}
