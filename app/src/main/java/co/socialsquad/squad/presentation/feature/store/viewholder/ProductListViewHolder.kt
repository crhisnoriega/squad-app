package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.STORE_PRODUCT_LIST_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.StoreProductListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_product_list.view.*

class ProductListViewHolder(itemView: View, val listener: Listener<StoreProductViewModel>) : ViewHolder<StoreProductListViewModel>(itemView) {
    private val rvProducts: RecyclerView = itemView.store_product_list_rv_products
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreProductListViewModel) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreProductViewModel -> STORE_PRODUCT_LIST_ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                STORE_PRODUCT_LIST_ITEM_VIEW_MODEL_ID -> ProductListItemViewHolder(view, glide, listener)
                else -> throw IllegalArgumentException()
            }
        }

        val categoriesAdapter = RecyclerViewAdapter(viewHolderFactory)
        categoriesAdapter.setItems(viewModel.products)

        with(rvProducts) {
            adapter = categoriesAdapter
            isFocusable = false
        }
    }

    override fun recycle() {}
}
