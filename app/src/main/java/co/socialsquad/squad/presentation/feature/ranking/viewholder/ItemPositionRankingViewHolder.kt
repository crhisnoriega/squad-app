package co.socialsquad.squad.presentation.feature.ranking.viewholder

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.ColorFilter
import android.preference.PreferenceManager
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_ranking_position.view.*

class ItemPositionRankingViewHolder(
        itemView: View, var onClickAction: ((item: ItemPositionRankingViewModel) -> Unit)) : ViewHolder<ItemPositionRankingViewModel>(itemView) {
    override fun bind(viewModel: ItemPositionRankingViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.user
        itemView.txtSubtitle.text = viewModel.score
        itemView.tvPosition.text = viewModel.position
        itemView.dividerItemTop.isVisible = viewModel.isFirst.not()


        if (viewModel.user.startsWith("Voc")) {
            itemView.imgPosition.setColorFilter(ColorUtils.parse(getCompanyColor()!!))
            itemView.icOptions.visibility = View.VISIBLE
        } else {
            itemView.imgPosition.setColorFilter(Color.BLACK)
            itemView.icOptions.visibility = View.GONE
        }


        if (viewModel.thumbnail_avatar == null) {
            Glide.with(itemView.context).load("https://d1gpti4ogccykg.cloudfront.net/default_placeholder.png").circleCrop().into(itemView.imgIcon)
        } else {
            Glide.with(itemView.context).load(viewModel.thumbnail_avatar).circleCrop().into(itemView.imgIcon)
        }
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}