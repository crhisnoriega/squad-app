package co.socialsquad.squad.presentation.feature.trip.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.trip.TripViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_trip_list.*
import javax.inject.Inject

class TripListFragment : Fragment(), TripListView {

    @Inject
    lateinit var presenter: TripListPresenter

    private lateinit var adapter: RecyclerViewAdapter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_trip_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSwipeRefresh()
        presenter.onViewCreated()
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener { presenter.onRefresh() }
    }

    override fun setupList(companyColor: String?) {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is TripViewModel -> R.layout.view_trip_list_item
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                R.layout.view_trip_list_item -> TripListItemViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rv.apply {
            adapter = this@TripListFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(ListDividerItemDecoration(context))
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    override fun startLoading() {
        adapter.startLoading()
        if (!srl.isRefreshing) srl.isEnabled = false
    }

    override fun stopLoading() {
        adapter.stopLoading()
        srl.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun addItems(items: List<TripViewModel>, shouldClearList: Boolean) {
        if (shouldClearList) adapter.setItems(items) else adapter.addItems(items)
    }

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun showEmptyState() {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }
}
