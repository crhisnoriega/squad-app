package co.socialsquad.squad.presentation.feature.settings

import androidx.appcompat.app.AppCompatActivity
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.util.shareApp
import io.branch.indexing.BranchUniversalObject

open class SharingAppPresenter(
    private val sharingAppView: SharingAppView,
    private val tagWorker: TagWorker
) {

    fun onShareAppDialogButtonClicked(context: AppCompatActivity) {
        tagWorker.tagEvent(TagWorker.TAG_TAP_SHARE_APP)
        BranchUniversalObject().shareApp(context, { sharingAppView.showLoadingDialog() }, { sharingAppView.hideLoadingDialog() })
    }
}
