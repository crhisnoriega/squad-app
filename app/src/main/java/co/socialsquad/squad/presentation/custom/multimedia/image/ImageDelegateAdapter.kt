package co.socialsquad.squad.presentation.custom.multimedia.image

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.custom.multimedia.ViewTypeDelegateAdapter

class ImageDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?): MediaViewHolder =
        ImageViewHolder(parent, lifecycleOwner)
}
