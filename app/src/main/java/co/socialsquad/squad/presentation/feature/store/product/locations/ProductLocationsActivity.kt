package co.socialsquad.squad.presentation.feature.store.product.locations

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_product_locations.*
import javax.inject.Inject

class ProductLocationsActivity : BaseDaggerActivity(), ProductLocationsView {

    @Inject
    lateinit var productLocationsPresenter: ProductLocationsPresenter

    private lateinit var rvLocations: RecyclerView
    private lateinit var nsvContainer: NestedScrollView
    private lateinit var supportMapFragment: SupportMapFragment

    private val glide by lazy { Glide.with(this) }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_locations)

        rvLocations = product_locations_rv_locations
        nsvContainer = product_locations_nsv_container
        supportMapFragment = supportFragmentManager.findFragmentById(R.id.product_locations_smf_map) as SupportMapFragment

        setupToolbar()

        productLocationsPresenter.onCreate(intent)
    }

    override fun onResume() {
        super.onResume()
        productLocationsPresenter.onResume()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.product_locations_title)
    }

    override fun addLocations(locations: List<StoreViewModel>, color: String?) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is StoreViewModel -> PRODUCT_LOCATION_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PRODUCT_LOCATION_VIEW_MODEL_ID -> ProductLocationViewHolder(view, glide, color, onPhoneNumberClicked)
                else -> throw IllegalArgumentException()
            }
        }

        with(rvLocations) {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(locations) }
            addItemDecoration(ListDividerItemDecoration(context))
            setHasFixedSize(true)
            isFocusable = false
        }

        setupMap(locations, color)
    }

    private val onPhoneNumberClicked = object : ViewHolder.Listener<StoreViewModel> {
        override fun onClick(viewModel: StoreViewModel) {
            productLocationsPresenter.onPhoneClicked()
        }
    }

    @SuppressLint("MissingPermission")
    private fun setupMap(locations: List<StoreViewModel>, color: String?) {
        supportMapFragment.getMapAsync {
            it.isMyLocationEnabled = true
            it.uiSettings.isMyLocationButtonEnabled = false
            it.setOnMapLoadedCallback { showLocationMarkers(locations, it, color) }
        }

        product_locations_v_map_scroll_fix.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE ->
                    nsvContainer.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL ->
                    nsvContainer.requestDisallowInterceptTouchEvent(false)
            }
            super.onTouchEvent(event)
        }
    }

    @SuppressLint("InflateParams")
    private fun showLocationMarkers(locations: List<StoreViewModel>, googleMap: GoogleMap, color: String?) {
        fun createMarkerBitmap(location: StoreViewModel): Bitmap {
            with(location) {
                with(layoutInflater.inflate(R.layout.view_product_locations_map_tooltip, null, false)) {
                    val tvAmount = findViewById<TextView>(R.id.product_locations_map_tooltip_tv_amount)
                    tvAmount.text = amount.toString()

                    color?.let {
                        val stateList = ColorUtils.stateListOf(it)
                        findViewById<ImageView>(R.id.product_locations_map_tooltip_iv_arrow)
                            .backgroundTintList = stateList
                        findViewById<ImageView>(R.id.product_locations_map_tooltip_iv_box)
                            .backgroundTintList = stateList
                        findViewById<ImageView>(R.id.product_locations_map_tooltip_iv_arrow_overlay)
                            .imageTintList = stateList
                    }

                    measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                    )
                    layout(0, 0, measuredWidth, measuredHeight)

                    isDrawingCacheEnabled = true
                    buildDrawingCache()
                    return drawingCache
                }
            }
        }

        val markers = arrayListOf<MarkerOptions>()
        locations.forEach {
            val latLng = LatLng(it.latitude, it.longitude)

            val bitmap = createMarkerBitmap(it)

            val markerOptions = MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
            markers.add(markerOptions)
            googleMap.addMarker(markerOptions)
        }

        val latLngBoundsBuilder = LatLngBounds.Builder()
        markers.forEach { latLngBoundsBuilder.include(it.position) }
        val latLngBounds = latLngBoundsBuilder.build()

        val paddingTop = resources.getDimensionPixelOffset(R.dimen.padding_product_locations_map_top)
        googleMap.setPadding(0, paddingTop, 0, 0)
        val padding = resources.getDimensionPixelOffset(R.dimen.padding_product_locations_map)
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, padding))
    }
}
