package co.socialsquad.squad.presentation.feature.squad.social.viewHolder

import android.graphics.Color
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.squad.social.SquadHeaderViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_squad_header.view.*

const val SQUAD_HEADER_VIEW_ID = R.layout.view_squad_header

class SquadHeaderViewHolder(itemView: View) : ViewHolder<SquadHeaderViewModel>(itemView) {

    override fun bind(viewModel: SquadHeaderViewModel) {
        itemView.setBackgroundColor(Color.parseColor(viewModel.backgroundColor))

        itemView.short_title_lead.text = viewModel.squadName
        itemView.short_title_lead.setTextColor(Color.parseColor(viewModel.textColor))

        itemView.tvQualification.text = viewModel.squadTitle
        itemView.tvQualification.setTextColor(Color.parseColor(viewModel.textColor))

        Glide.with(itemView)
            .load(viewModel.squadImage)
            .into(itemView.ivSquadAvatar)
    }

    override fun recycle() {
        Glide.with(itemView)
            .clear(itemView.ivSquadAvatar)
    }
}
