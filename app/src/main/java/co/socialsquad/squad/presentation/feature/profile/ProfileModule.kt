package co.socialsquad.squad.presentation.feature.profile

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileModule {
    @Binds
    abstract fun view(profileFragment: ProfileFragment): ProfileView

    @Binds
    abstract fun interactor(profileInteractorImpl: ProfileInteractorImpl): ProfileInteractor
}
