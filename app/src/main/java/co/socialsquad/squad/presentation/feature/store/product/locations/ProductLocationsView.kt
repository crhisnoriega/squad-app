package co.socialsquad.squad.presentation.feature.store.product.locations

import co.socialsquad.squad.presentation.feature.store.StoreViewModel

interface ProductLocationsView {
    fun addLocations(locations: List<StoreViewModel>, color: String?)
}
