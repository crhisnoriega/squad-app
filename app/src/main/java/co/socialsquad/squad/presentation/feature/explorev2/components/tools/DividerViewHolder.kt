package co.socialsquad.squad.presentation.feature.explorev2.components.tools

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_tool_divider.view.*

class DividerViewHolder(itemView: View) : ViewHolder<DividerViewHolderModel>(itemView) {
    override fun bind(viewModel: DividerViewHolderModel) {
        viewModel.isLast?.let {
            itemView.dividerBottomLine.isVisible = it.not()
        }
    }

    override fun recycle() {

    }
}

class DividerViewHolderModel(var isLast: Boolean? = false) : ViewModel
