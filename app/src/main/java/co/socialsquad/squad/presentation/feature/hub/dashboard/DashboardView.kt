package co.socialsquad.squad.presentation.feature.hub.dashboard

import co.socialsquad.squad.presentation.feature.hub.dashboard.charts.ChartViewModel

interface DashboardView {
    fun showMessage(textResId: Int, onDismissListener: () -> Unit = {})
    fun showLoading()
    fun hideLoading()
    fun setItems(list: List<ChartViewModel>)
    fun addItems(list: List<ChartViewModel>)
}
