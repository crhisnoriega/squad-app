package co.socialsquad.squad.presentation.feature.signup.credentials

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import kotlinx.android.synthetic.main.fragment_signup_credentials_password.*

class SignupCredentialsPasswordFragment : Fragment() {

    private var signupCredentialsFragmentListener: SignupCredentialsFragmentListener? = null
    private var isPasswordVisible = false

    val passwordTyped
        get() = tietPassword?.text?.toString() ?: ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        signupCredentialsFragmentListener = activity as SignupCredentialsFragmentListener?
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_signup_credentials_password, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupKeyboardAction()
        setupPasswordField()
        setupVisibilityToggle()
        setupNextButtonBehavior()
    }

    private fun setupKeyboardAction() {
        tietPassword.setOnKeyboardActionListener { signupCredentialsFragmentListener?.onKeyboardAction() }
    }

    private fun setupPasswordField() {
        tietPassword.apply {
            val currentTypeface = typeface
            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            transformationMethod = PasswordTransformationMethod.getInstance()
            typeface = currentTypeface
        }
    }

    private fun setupVisibilityToggle() {
        ibToggleVisibility.setOnClickListener {
            tietPassword.apply {
                val currentTypeface = typeface
                val caretPosition = selectionStart
                inputType =
                    if (isPasswordVisible) InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    else InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                setSelection(caretPosition)
                typeface = currentTypeface
            }
            isPasswordVisible = !isPasswordVisible
        }
    }

    private fun setupNextButtonBehavior() {
        tietPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                signupCredentialsFragmentListener?.onTextChanged(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }
}
