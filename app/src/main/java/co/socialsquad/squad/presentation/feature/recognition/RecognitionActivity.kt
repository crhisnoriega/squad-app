package co.socialsquad.squad.presentation.feature.recognition

import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.ranking.details.RankingBenefitDetailsActivity
import co.socialsquad.squad.presentation.feature.recognition.customviews.RecognitionOptOutInBottomSheetDialog
import co.socialsquad.squad.presentation.feature.recognition.model.*
import co.socialsquad.squad.presentation.feature.recognition.viewHolder.*
import co.socialsquad.squad.presentation.feature.recognition.viewmodel.RecognitionViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_recognition.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import kotlin.math.abs


class RecognitionActivity : BaseActivity() {
    private var currentLevel: LevelData? = null
    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_recognition

    private var currentPage: Int = 0;

    private val viewModel by viewModel<RecognitionViewModel>()
    private val planId by extra<String>(EXTRA_PLAN_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureObservers()
        configureToolbar()
        configureOptInButton()


        viewModel.fetchPlan(planId)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.WHITE
        }
    }

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.fetchPlan
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        populateData(it.data)
                    }
                    Status.ERROR -> {

                    }
                }

            }
        }

        lifecycleScope.launch {
            val value = viewModel.refuseTerms
            value.collect {
                Log.i("rek", "refuse: $it")
                when (it.status) {
                    Status.SUCCESS -> {
                        finish()
                        start(this@RecognitionActivity, planId)
                    }
                    Status.ERROR -> {

                    }
                }
            }
        }

        lifecycleScope.launch {
            val value = viewModel.agreeTerms
            value.collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        finish()
                        start(this@RecognitionActivity, planId)
                    }
                    Status.ERROR -> {

                    }
                }
            }
        }
    }

    private fun populateData(planData: PlanData?) {
        var currentPosition = 1000
        planData?.levels?.data?.forEachIndexed { index, levelData ->
            levelData.user_info?.current_level?.let {
                if (it) {
                    currentPosition = index
                }
            }
            levelData.isBlocked = index > currentPosition

//            if (planData.user_in_plan!!) {
//                levelData.requirements?.data?.forEach {
//                    it.completed = (index < currentPosition)
//                }
//            } else {
//                levelData.requirements?.data?.forEach {
//                    it.completed = false
//                }
//            }
        }



        configureOptOutButton(planData)
        configureCarousel(planData?.title!!, planData?.levels?.data!!)

        planData.levels?.data.forEachIndexed { index, levelData ->

            Log.i("rek1", "user info: ${Gson().toJson(levelData.user_info)}")

            levelData.user_info?.current_level?.let {
                if (it) {
                    currentPosition = index
                    Handler().postDelayed({
                        tvValue.setCurrentItem(index, true)

                    }, 500)

                    return@forEachIndexed
                }
            }
        }

        if (currentPosition == 1000) {
            populateLevelData(planData.levels?.data[0])
        }

        Handler().postDelayed({
            recognitionShimmer.visibility = View.GONE
            nestedScroll.visibility = View.VISIBLE
            app_bar.visibility = View.VISIBLE
        }, 800)

        planData?.user_in_plan?.let {
            if (it.not()) showOptInButton()
        }
    }

    private fun showOptInButton() {
        startAnimationButton()
        dividerHeaderPositionBBWithButtom.isVisible = true
    }


    private fun configureCarousel(title: String, levels: List<LevelData>) {
        tvValue.orientation = ViewPager2.ORIENTATION_HORIZONTAL;
        tvValue.offscreenPageLimit = 3;

        var density = resources.displayMetrics.density

        tvValue.apply {
            offscreenPageLimit = 1
            val recyclerView = getChildAt(0) as RecyclerView
            recyclerView.apply {
                //val padding = 200 + 180

                val padding = 108 * density
                setPadding(padding.toInt(), 0, padding.toInt(), 0)
                clipToPadding = false
            }
        }

        tvValue.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPage = position
                tvValue.adapter?.notifyDataSetChanged()

                levels.forEachIndexed { index, levelData ->
                    levelData.isSelected = index == position
                }

                var level = levels[position]

                currentLevel = level
                updateHeader(title, level)
                populateLevelData(level)
                tvValue.adapter?.notifyDataSetChanged()
            }
        })

        tvValue.setPageTransformer { page, position ->
            if (-0.8f <= position && position <= 0.8f) {
                val scaleFactor = 1 - (0.3f * abs(position))
                page.scaleX = scaleFactor
                page.scaleY = scaleFactor

            } else {
                //val scaleFactor = max(0.7f, 1 - abs(position - 0.14285715f))
                val scaleFactor = 0.76f
                page.scaleX = scaleFactor
                page.scaleY = scaleFactor
            }
        }

        tvValue.adapter = object : RecyclerView.Adapter<CarouselViewHolder2>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselViewHolder2 {
                val view: View =
                    LayoutInflater.from(this@RecognitionActivity).inflate(viewType, parent, false)
                return CarouselViewHolder2(view)
            }

            override fun getItemCount(): Int {
                return levels.size
            }

            override fun getItemViewType(position: Int): Int {

                return if (levels[position].user_info?.current_level!!) {
                    R.layout.item_recognition_carousel
                } else {
                    if (levels[position].isBlocked) {
                        R.layout.item_recognition_carousel_blocked
                    } else {
                        R.layout.item_recognition_carousel_without_progress
                    }
                }
            }

            override fun onBindViewHolder(holder: CarouselViewHolder2, position: Int) {

                var level = levels[position]

                GlideToVectorYou.init().with(holder.itemView.context)
                    .load(Uri.parse(level.icon), holder.levelImage)


//                if (level.isBlocked) {
//                    holder.levelImage.alpha = 0.7f
//                } else {
//                    holder.levelImage.alpha = 1f
//                }

                if (position == currentPage) {
                    // diminuir
                } else {
                    //aumentar
                }

                if (holder.circularProgressbar != null) {
                    //holder.circularProgressbar.isVisible = level.isSelected
                    level.user_info?.progress?.let {
                        //holder.circularProgressbar.progress = (it).toInt()

                        Handler().postDelayed({
                            animateProgress(holder.circularProgressbar, it.toInt())
                        }, 1000)

                    }
                    holder.circularProgressbar.secondaryProgressTintList =
                        ColorStateList.valueOf(resources.getColor(R.color._eeeff4));
                    holder.circularProgressbar.progressTintList =
                        ColorStateList.valueOf(ColorUtils.parse(ColorUtils.getCompanyColor(this@RecognitionActivity)!!));
                }

            }

            private fun animateProgress(pbOption: ProgressBar, percent: Int) {
                val animation = ObjectAnimator.ofInt(pbOption, "progress", percent)
                //animation.duration = 1000
                animation.interpolator = DecelerateInterpolator()
                animation.start()
            }
        }
        // carousel.registerOnPageChangeCallback(viewPagerListener)
    }

    private fun populateLevelData(level: LevelData) {

        configureRequirementsList(level.requirements)
        configureBonusList(level.bonus)
        configurePrizeList(level.prizes)
        configureBenefitsList(level.benefits)
        //configureSuccessHistories(level.success_stories)
    }

    private fun configurePrizeList(prizesList: PrizesList?) {
        prizesLabel?.text = prizesList?.title

        prizesList?.data?.let {
            prizeGroup.isVisible = it.isEmpty().not()
        } ?: kotlin.run {
            prizeGroup.isVisible = false
        }


        rvPrizes.apply {
            layoutManager =
                LinearLayoutManager(this@RecognitionActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemPrizeRecognitionViewModel -> R.layout.item_recognition_prize
                        is ItemShowAllRecognitionViewModel -> R.layout.item_recognition_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_recognition_prize -> ItemPrizeRecognitionViewHolder(view) {

                    }
                    R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(view) {

                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ViewModel>()

            prizesList?.data?.forEachIndexed { index, requirementData ->
                elements.add(ItemPrizeRecognitionViewModel(requirementData))
            }

            prizesList?.show_all?.let {
                if (it) elements.add(ItemShowAllRecognitionViewModel("", ""))
            }

            factoryAdapter.addItems(elements)
        }
    }

    private fun configureBonusList(bonusList: BonusList?) {
        bonusLabel.text = bonusList?.title

        bonusList?.data?.let {
            bonusGroup.isVisible = it.isEmpty().not()
        } ?: kotlin.run {
            bonusGroup.isVisible = false
        }

        rvBonus.apply {
            layoutManager =
                LinearLayoutManager(this@RecognitionActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemRecognitionBonusViewModel -> R.layout.item_recognition_bonus
                        is ItemShowAllRecognitionViewModel -> R.layout.item_recognition_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_recognition_bonus -> ItemRecognitionBonusViewHolder(view) {

                    }
                    R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(view) {

                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ItemRecognitionBonusViewModel>()

            bonusList?.data?.forEachIndexed { index, requirementData ->
                elements.add(ItemRecognitionBonusViewModel(requirementData))
            }

            factoryAdapter.addItems(elements)
        }
    }

    private fun configureSuccessHistories(successStoriesList: SuccessStoriesList?) {
        successHistoryLabel.text = successStoriesList?.title

        successStoriesList?.data?.let {
            successHistoryGroup.isVisible = it.isEmpty().not()
        } ?: kotlin.run {
            successHistoryGroup.isVisible = false
        }

        rvSucessHistory.apply {
            layoutManager =
                LinearLayoutManager(this@RecognitionActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemBenefitRecognitionViewModel -> R.layout.item_recognition_benefit
                        is ItemShowAllRecognitionViewModel -> R.layout.item_recognition_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_recognition_benefit -> ItemBenefitRecognitionViewHolder(view) {

                    }
                    R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(view) {

                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ItemSuccessHistoryRecognitionViewModel>()

            successStoriesList?.data?.forEachIndexed { index, requirementData ->
                elements.add(ItemSuccessHistoryRecognitionViewModel(requirementData))
            }

            factoryAdapter.addItems(elements)
        }
    }

    private fun configureBenefitsList(benefitsList: BenefitsList?) {
        benefitLabel.text = benefitsList?.title

        benefitsList?.data?.let {
            benefitGroup.isVisible = it.isEmpty().not()
        } ?: kotlin.run {
            benefitGroup.isVisible = false
        }

        rvBenefits.apply {
            layoutManager =
                LinearLayoutManager(this@RecognitionActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemBenefitRecognitionViewModel -> R.layout.item_recognition_benefit
                        is ItemShowAllRecognitionViewModel -> R.layout.item_recognition_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.item_recognition_benefit -> ItemBenefitRecognitionViewHolder(view) {
                        RankingBenefitDetailsActivity.start(
                            this@RecognitionActivity,
                            benefitId = it.benefitData.id!!,
                            levelId = currentLevel?.id.toString()!!
                        )
                    }
                    R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(view) {

                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ItemBenefitRecognitionViewModel>()

            benefitsList?.data?.forEachIndexed { index, requirementData ->
                elements.add(ItemBenefitRecognitionViewModel(requirementData))
            }

            factoryAdapter.addItems(elements)
        }
    }

    private fun configureRequirementsList(requirementsList: RequirementsList?) {
        requirementsLabel.text = requirementsList?.title

        requirementsList?.data?.let {
            criteriasGroup.isVisible = it.isEmpty().not()
        } ?: kotlin.run {
            criteriasGroup.isVisible = false
        }

        rvRequeriments.apply {
            layoutManager =
                LinearLayoutManager(this@RecognitionActivity, LinearLayoutManager.VERTICAL, false)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemActionRecognitionViewModel -> ItemActionRecognitionViewHolder.ITEM_ACTION_RECOGNITION_VIEW_HOLDER_ID
                        is ItemProgressRecognitionViewModel -> ItemProgressRecognitionViewHolder.ITEM_PROGRESS_RECOGNITION_VIEW_HOLDER_ID
                        is ItemScoreRecognitionViewModel -> ItemScoreRecognitionViewHolder.ITEM_SCORE_RECOGNITION_VIEW_HOLDER_ID
                        is ItemShowAllRecognitionViewModel -> R.layout.item_recognition_show_all
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    ItemActionRecognitionViewHolder.ITEM_ACTION_RECOGNITION_VIEW_HOLDER_ID -> ItemActionRecognitionViewHolder(
                        view
                    ) {

                    }
                    ItemProgressRecognitionViewHolder.ITEM_PROGRESS_RECOGNITION_VIEW_HOLDER_ID -> ItemProgressRecognitionViewHolder(
                        view
                    ) {

                    }
                    ItemScoreRecognitionViewHolder.ITEM_SCORE_RECOGNITION_VIEW_HOLDER_ID -> ItemScoreRecognitionViewHolder(
                        view
                    ) {

                    }

                    R.layout.item_recognition_show_all -> ItemShowAllRecognitionViewHolder(view) {

                    }

                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            var elements = mutableListOf<ViewModel>()

            requirementsList?.data?.forEachIndexed { index, requirementData ->
                requirementData.progress?.score_progress?.let {
                    elements.add(ItemProgressRecognitionViewModel(requirementData))
                }

                requirementData.progress?.boolean?.let {
                    elements.add(ItemActionRecognitionViewModel(requirementData))
                }
            }

            factoryAdapter.addItems(elements)
        }
    }


    private fun updateHeader(title: String, levelData: LevelData) {
        levelName.text = levelData.title
        levelDescription.text = levelData.user_info?.subtitle

        when (levelData.user_info?.subtitle) {
            "Conquistado" -> levelDescription.setTextColor(ColorUtils.parse("#4dab21"))
            "Bloqueado" -> levelDescription.setTextColor(ColorUtils.parse("#cf2c2c"))
            else -> levelDescription.setTextColor(ColorUtils.parse("#757575"))
        }

        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->

            if (verticalOffset <= -1 * (app_bar.totalScrollRange - 100)) {
                bottomHeaderDivider.visibility = View.GONE
                levelName.visibility = View.INVISIBLE
                levelDescription.visibility = View.INVISIBLE

                tvSubtitleCollapsed.visibility = View.VISIBLE
                bottomHeaderDividerCollapsed.visibility = View.VISIBLE
                tvTitle.text = title
                tvSubtitleCollapsed.text = levelData.title

                //bottomHeaderDivider.visibility = View.INVISIBLE
                //dividerHeaderPosition.visibility = View.INVISIBLE
            } else {
                bottomHeaderDivider.visibility = View.VISIBLE
                levelName.visibility = View.VISIBLE
                levelDescription.visibility = View.VISIBLE
                tvSubtitleCollapsed.visibility = View.GONE

                bottomHeaderDividerCollapsed.visibility = View.GONE
                //tvTittle.text = "Ranking #1"

                tvTitle.text = title
                //bottomHeaderDivider.visibility = View.VISIBLE
                //dividerHeaderPosition.visibility = View.VISIBLE
            }
        })
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.title = ""
        toolbar_layout.isTitleEnabled = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun configureOptInButton() {
        optInButton.setOnClickListener {
            showLoading()
            Handler().postDelayed({
                viewModel.agreeRecognitionTerms(planId)
            }, 1000L)
        }

        btConfirm1.setOnClickListener {
            showLoading()
            Handler().postDelayed({
                viewModel.agreeRecognitionTerms(planId)
            }, 1000L)
        }

        termText.setOnClickListener {

            //RankingTermsActivity.start(this, ranking?.rank, ranking?.terms)
        }
    }

    private fun configureOptOutButton(planData: PlanData?) {

        optOutBtn.isVisible = planData?.user_in_plan!!
        optOutBtn.setOnClickListener {
            var plan = viewModel.planData
            var dialog = RecognitionOptOutInBottomSheetDialog(plan) {
                when (it) {
                    "share" -> {
                    }
                    "optout" -> {
                        showLoading()

                        Handler().postDelayed({
                            viewModel.refuseRecognitionTerms(plan.id.toString())
                        }, 1000L)

                    }
                }
            }
            dialog.show(supportFragmentManager, "optout")
        }
    }

    private fun startAnimationButton() {
        val appearBottomUp = AnimationUtils.loadAnimation(this, R.anim.animation_in_bottom)
        appearBottomUp.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {

            }

            override fun onAnimationStart(animation: Animation?) {
                containerButton.visibility = View.VISIBLE
            }

        })
        containerButton.startAnimation(appearBottomUp)

    }

    private fun showLoading() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.GRAY
        }

        optInRecLoading.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        iconRecLoading.startAnimation(rotation)
    }


    companion object {
        const val EXTRA_PLAN_ID = "EXTRA_PLAN_ID"
        const val OBJECT_TYPE = "object_type"
        const val TOOL_TYPE = "tool_type"
        const val INITIALS_LETTER = "initials_letter"
        const val NOME_LEAD = "nome_lead"

        fun start(
            context: Context, planId: String
        ) {
            context.startActivity(Intent(context, RecognitionActivity::class.java).apply {
                putExtra(EXTRA_PLAN_ID, planId)
            })
        }
    }
}

class CarouselViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val levelImage = itemView.findViewById<ImageView>(R.id.levelImage)
    val circularProgressbar = itemView.findViewById<ProgressBar>(R.id.circularProgressbar)
}