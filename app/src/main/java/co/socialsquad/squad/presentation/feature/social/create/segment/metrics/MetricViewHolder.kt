package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.util.ColorUtils
import kotlinx.android.synthetic.main.linearlayout_segment_metrics_list_item.view.*

class MetricViewHolder(itemView: View, val listener: Listener<MetricViewModel>) : ViewHolder<MetricViewModel>(itemView) {

    override fun bind(viewModel: MetricViewModel) {
        with(itemView) {
            vDivider.visibility = if (adapterPosition == 0) View.VISIBLE else View.GONE
            tvText.text = viewModel.name
            setOnClickListener { listener.onClick(viewModel) }
            viewModel.color?.let { tvCountBadge.backgroundTintList = ColorUtils.stateListOf(it) }
            tvCountBadge.text = viewModel.values.size.toString()
            tvCountBadge.visibility = if (viewModel.values.isNotEmpty()) View.VISIBLE else View.GONE
        }
    }

    override fun recycle() {}
}
