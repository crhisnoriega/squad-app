package co.socialsquad.squad.presentation.feature.splash

import android.app.Activity
import android.content.Intent
import android.util.Log
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.data.entity.*
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.PlatformRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import io.branch.referral.Branch
import io.branch.referral.BranchError
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONObject
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    private val splashView: SplashView,
    private val loginRepository: LoginRepository,
    private val profileRepository: ProfileRepository,
    private val preferences: Preferences,
    private val platformRepository: PlatformRepository,
    private val branch: Branch
) {
    private val compositeDisposable = CompositeDisposable()
    private var isInBackground = false
    private var livePkFromNotification: Int? = null
    private var livePKFromShare: Int? = null
    private var scheduledLivePKFromShare: Int? = null
    private var scheduledLivePKFromNotification: Int? = null
    private var postPk: Int? = null
    private var audioPk: Int? = null
    private var productPk: Int? = null
    private var eventPk: Int? = null

    fun onCreate(intent: Intent) {
        if (intent.hasExtra(SplashActivity.LIVE_PK_FROM_NOTIFICATION)) {
            livePkFromNotification =
                intent.getIntExtra(SplashActivity.LIVE_PK_FROM_NOTIFICATION, -1)
        }

        if (intent.hasExtra(SplashActivity.SCHEDULED_LIVE_PK_FROM_NOTIFICATION)) {
            scheduledLivePKFromNotification =
                intent.getIntExtra(SplashActivity.SCHEDULED_LIVE_PK_FROM_NOTIFICATION, -1)
        }

        if (intent.hasExtra(SplashActivity.POST_PK_FROM_NOTIFICATION)) {
            postPk = intent.getIntExtra(SplashActivity.POST_PK_FROM_NOTIFICATION, -1)
        }

        if (intent.hasExtra(SplashActivity.AUDIO_PK_FROM_NOTIFICATION)) {
            audioPk = intent.getIntExtra(SplashActivity.AUDIO_PK_FROM_NOTIFICATION, -1)
        }
    }

    private fun verifyResetPasswordToken(token: String, subdomain: String, email: String) {
        compositeDisposable.add(
            loginRepository.verifyResetPasswordToken(
                VerifyResetPasswordTokenRequest(token),
                subdomain
            )
                .subscribe(
                    {
                        splashView.moveToResetPassword(token, subdomain, email)
                    },
                    {
                        Log.d("Error", it.message)
                        loginRepository.squadName = null
                        verifyTokenAndContinue()
                    }
                )
        )
    }

    private fun verifyMagicLinkToken(token: String, email: String) {
        compositeDisposable.add(

            loginRepository.verifyMagicLinkToken(MagicLinkResumeRequest(token))
                .subscribe(
                    {
                        if (it.size == 1) {
                            val company = it[0]
                            val subdomain = company.subdomain!!
                            setSubdomain(subdomain)
                            company.loginToken?.let { authVerifyToken(it, subdomain) }
                        } else {
                            splashView.openSubdomainsScreen(it, email)
                        }
                    },
                    {
                        Log.d("Error", it.message)
                        loginRepository.squadName = null
                        verifyTokenAndContinue()
                    }
                )
        )
    }

    private fun clearData() {
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.subdomain = null
        loginRepository.clearSharingAppDialogTimer()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
    }

    private fun callResume(
        token: String,
        subdomain: String,
        email: String,
        firstName: String?,
        lastName: String?,
        phone: String?
    ) {
        compositeDisposable.add(
            loginRepository.callResume(CallResumeTokenRequest(token))
                .subscribe(
                    {
                        Log.i("deeplink", "genericSucess")
                        genericSuccess(it, email, firstName, lastName, phone, token, subdomain)
                    },
                    {
                        try {
                            val error = it as HttpException
                            val errorBody = error.response()?.errorBody()?.string()
                            val gson = Gson()
                            val response =
                                gson.fromJson(errorBody, CallResumeTokenResponse::class.java)
                            Log.i("deeplink", "genericSucess2")
                            genericSuccess(
                                response,
                                email,
                                firstName,
                                lastName,
                                phone,
                                token,
                                subdomain
                            )
                        } catch (e: Exception) {
                            Log.i("deeplink", e.message, e)
                            genericError()

                        }
                    }
                )
        )
    }

    private fun genericError() {
        if (BuildConfig.DEBUG) {
            loginRepository.squadName = null
        }
        verifyTokenAndContinue()
    }

    private fun genericSuccess(
        it: CallResumeTokenResponse,
        email: String,
        firstName: String?,
        lastName: String?,
        phone: String?,
        token: String,
        subdomain: String
    ) {
        if (it.token != null && it.token!!.isNotEmpty()) {
            Log.i("deeplink", "case 1")
            authVerifyToken(it.token, subdomain)
            return
        }
        if (it.detail.toLowerCase().contains("u009")) {
            Log.i("deeplink", "case 2")
            val register = RegisterUserRequest(
                RegisterUserRequest.User(
                    firstName,
                    lastName,
                    listOf(phone),
                    email,
                    ""
                ), token, false
            )
            val gson = Gson()
            splashView.moveToPassword(gson.toJson(register), subdomain)
            return
        }

        if (it.detail.toLowerCase().contains("u007")) {
            Log.i("deeplink", "case 3")

            if (BuildConfig.DEBUG) {
                loginRepository.squadName = null
            }
            verifyTokenAndContinue()
        }
    }

    fun onDismissDialog() {
        loginRepository.token?.let { verifyToken(it) }
    }

    private fun verifyToken(token: String) {

        compositeDisposable.add(
            platformRepository.getVersion()
                .map { hasNewerVersion(it.currentVersion ?: "0") }
                .flatMapCompletable { loginRepository.verifyToken(TokenVerifyRequest(token)) }
                .andThen(loginRepository.authRefreshToken(TokenVerifyRequest(token), null))
                .subscribe(
                    { userCompany ->
                        with(userCompany) {
                            loginRepository.userCompany = this
                            company.primaryColor?.let { loginRepository.squadColor = it }
                            userCompany?.user?.email?.let { branch.setIdentity(it) }

                        }

                        with(splashView) {
                            startCallService()
                            when {
//                                    userCompany?.cid.isNullOrEmpty().and(userCompany.isEmployee.not()) -> setNextAsSignupHinodeConfirmation()
                                livePkFromNotification != null -> setNextAsLiveViewer(
                                    livePkFromNotification!!
                                )
                                livePKFromShare != null -> setNextAsLiveViewer(livePKFromShare!!)
                                scheduledLivePKFromShare != null -> setNextAsLiveDetail(
                                    scheduledLivePKFromShare!!
                                )
                                scheduledLivePKFromNotification != null -> setNextAsLiveDetail(
                                    scheduledLivePKFromNotification!!
                                )
                                postPk != null -> setNextAsPostViewer(postPk!!)
                                audioPk != null -> setNextAsAudioDetail(audioPk!!)
                                productPk != null -> setNextAsProduct(productPk!!)
                                eventPk != null -> setNextAsEvent(eventPk!!)
                                else -> {
                                    Log.i(
                                        "login",
                                        "called: ${loginRepository.userCompany?.user?.isPerfilComplete!!}"
                                    )
                                    if (loginRepository.userCompany?.user?.isPerfilComplete!!) {
                                        setNextAsNavigation()
                                    } else {
                                        openCompleteRegistration()
                                    }
                                }
                            }

                            if (!isInBackground) preloadImages(userCompany.company.logo)
                        }
                    },
                    { throwable ->
                        FirebaseCrashlytics.getInstance()
                            .log("Error to verify token. Is null: ${token.isEmpty()}")
                        throwable.printStackTrace()
                        clearData()
                        when (throwable) {
                            is UnsupportedVersion -> splashView.showUpdateDialog()
                            is UnknownHostException, is SocketTimeoutException -> splashView.showErrorMessageFailedToConnect()
                            else -> showWalkthrough()
                        }
                    }
                )
        )
    }

    private fun authVerifyToken(token: String, subdomain: String) {
        compositeDisposable.add(

            loginRepository.authVerifyToken(TokenVerifyRequest(token), subdomain)
                .subscribe(
                    {
                        loginRepository.squadColor = it.company.primaryColor
                        loginRepository.userCompany = it
                        loginRepository.token = it.token
                        loginRepository.subdomain = subdomain
                        loginRepository.getDeviceToken()

                        Log.i("login", "authVerifyToken")
                        splashView.openNavigation()
                    },
                    {
                        Log.d("Error", it.message)
                        if (BuildConfig.DEBUG) {
                            loginRepository.squadName = null
                        }
                        verifyTokenAndContinue()
                    }
                )

        )
    }

    private fun showWalkthrough() {
        setFeedRedirect()
        splashView.setNextAsWalkthrough()
        if (!isInBackground) splashView.showNextActivity()
    }

    private fun setFeedRedirect() {
        when {
            scheduledLivePKFromShare != null -> setFeedRedirectProperty(
                Share.ScheduledLive.name,
                scheduledLivePKFromShare!!
            )
            scheduledLivePKFromNotification != null -> setFeedRedirectProperty(
                Share.ScheduledLive.name,
                scheduledLivePKFromNotification!!
            )
            livePKFromShare != null -> setFeedRedirectProperty(Share.Live.name, livePKFromShare!!)
            postPk != null -> setFeedRedirectProperty(Share.Feed.name, postPk!!)
            audioPk != null -> setFeedRedirectProperty(Share.Audio.name, audioPk!!)
            productPk != null -> setFeedRedirectProperty(Share.Product.name, productPk!!)
            eventPk != null -> setFeedRedirectProperty(Share.Event.name, eventPk!!)
        }
    }

    private fun setFeedRedirectProperty(model: String, pk: Int) {
        preferences.shareModel = model
        preferences.sharePK = pk
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onPause() {
        isInBackground = true
    }

    fun onResume(intent: Intent?) {
        if (isInBackground && intent != null) {
            splashView.showNextActivity()
        }
        isInBackground = false
    }

    private fun verifyTokenAndContinue() {
        val localToken = loginRepository.token
        if (localToken != null && loginRepository.subdomain != null) {
            verifyToken(localToken)
        } else {
            showWalkthrough()
        }
    }

    private fun hasNewerVersion(newVersion: String): Boolean {
        val newVersion = newVersion.split(".")
        val currentVersion = BuildConfig.VERSION_NAME.replace("-debug", "").split(".")
        (0 until currentVersion.size).map {
            if (newVersion[it].toInt() < currentVersion[it].toInt()) return false
            if (newVersion[it].toInt() > currentVersion[it].toInt()) throw UnsupportedVersion()
        }
        return false
    }

    fun setSubdomain(subdomain: String) {
        loginRepository.subdomain = subdomain
    }

    fun onNewIntent(context: Activity) {
        Log.i("login", "branch io check: onNewIntent")
        // Branch reinit (in case Activity is already in foreground when Branch link is clicked)
        Branch.sessionBuilder(context)
            .withCallback { referringParams: JSONObject?, error: BranchError? ->
                checkDeepLink(
                    error,
                    referringParams
                )
            }.reInit()
    }

    fun onStart(context: Activity, intent: Intent) {
        // Branch init
        Log.i("login", "branch io check: onStart")
        Branch.sessionBuilder(context)
            .withCallback { referringParams: JSONObject?, error: BranchError? ->
                checkDeepLink(
                    error,
                    referringParams
                )
            }.withData(intent.data).init()
    }

    private fun checkDeepLink(error: BranchError?, referringParams: JSONObject?) {
        if (error == null) {

            if (referringParams != null && referringParams.has("+clicked_branch_link") && referringParams.getBoolean(
                    "+clicked_branch_link"
                )
            ) {

                Log.i("BRANCH_SDK_REDIRECT", referringParams.toString())

                val gson = Gson()
                val branchData =
                    gson.fromJson(referringParams.toString(), MagicLinkDeeplink::class.java)

                when (branchData.channel) {
                    "api_registercompanylink" -> {
                        clearData()
                        splashView.moveToAccessSquad(branchData.token, branchData.email)
                    }

                    "api_invitelink" -> {
                        clearData()
                        preferences.subdomain = branchData.subdomain

                        callResume(
                            branchData.token,
                            branchData.subdomain,
                            branchData.email,
                            branchData.firstName,
                            branchData.lastName,
                            branchData.phone
                        )
                    }

                    "api_resetpasswordlink" -> {
                        clearData()
                        verifyResetPasswordToken(
                            branchData.token,
                            branchData.subdomain,
                            branchData.email
                        )
                    }

                    "api_magiclinktoken" -> {
                        clearData()
                        verifyMagicLinkToken(branchData.token, branchData.email)
                    }

                    else -> {
                        if (branchData.token.isNotEmpty() && branchData.subdomain.isNotEmpty()) {
                            clearData()
                            authVerifyToken(branchData.token, branchData.subdomain)
                        }
                    }
                }
            } else {
                openApp()
            }
        } else {
            Log.d("BRANCH_SDK", error.message)
            openApp()
        }
    }

    private fun openApp() {
        if (BuildConfig.DEBUG) {
            loginRepository.squadName = null
        }
        verifyTokenAndContinue()
    }
}
