package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_input.view.*

class Input(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    interface Listener {
        fun onClick()
    }

    private var listener: Listener? = null

    init {
        View.inflate(context, R.layout.view_input, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.Input)) {
            short_title_lead.text = getString(R.styleable.Input_name)
            tvDescription.text = getString(R.styleable.Input_description)
            ivImage.setImageResource(getResourceId(R.styleable.Input_image, 0))
            recycle()
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener { listener?.onClick() }
    }
}
