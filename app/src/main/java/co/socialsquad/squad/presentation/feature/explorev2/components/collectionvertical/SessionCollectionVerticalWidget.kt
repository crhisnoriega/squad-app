package co.socialsquad.squad.presentation.feature.explorev2.components.collectionvertical

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.detail.ExploreItemDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreContent
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity

class SessionCollectionVerticalWidget(context: Context, attrs: AttributeSet?) :
    LinearLayout(context, attrs) {

    private val view: View
    private var loading: Boolean = true
    private val rvItems: RecyclerView
    private val title: TextView
    private val subtitle: TextView
    private val button: RelativeLayout

    private var emptyState: EmptyState? = null
    private var template: SectionTemplate = SectionTemplate.getDefault()

    init {
        setupStyledAttributes(attrs)
        view = changeTemplate(context)
        rvItems = view.findViewById(R.id.rvItems)
        title = view.findViewById(R.id.object_type_lead)
        subtitle = view.findViewById(R.id.levelDescription)
        button = view.findViewById(R.id.button)
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.SessionCollectionVerticalWidget)) {
            loading = getBoolean(
                R.styleable.SessionCollectionVerticalWidget_loadingCollectionVertical,
                true
            )
        }
    }

    private fun changeTemplate(context: Context): View {
        return if (loading) {
            View.inflate(
                context,
                R.layout.view_session_collection_vertical_loading_state,
                this
            )
        } else {
            View.inflate(
                context,
                R.layout.view_session_collection_vertical_widget,
                this
            )
        }
    }

    fun bind(
        content: ExploreContent?,
        template: SectionTemplate?,
        emptyState: EmptyState?,
        indicatorColor: String
    ) {
        template?.let {
            this.template = it
        }
        this.emptyState = emptyState
        if (loading) {
            rvItems.adapter = CollectionVerticalItemsLoadingAdapter(template!!)
        } else {
            title.text = content?.title.orEmpty()
            subtitle.text = content?.subtitle.orEmpty()
            button.findViewById<TextView>(R.id.buttonText).text = content?.button?.text.orEmpty()
            if (content != null) {
                button.isVisible = true
                button.setOnClickListener {
                    onCollectionSelected(
                        ContentItem(
                            id = content.id,
                            title = content.title,
                            link = content.link,
                            pinLocation = null,
                        )
                    )
                }

                rvItems.adapter = CollectionVerticalItemsAdapter(
                    context,
                    content.template,
                    content.items,
                    indicatorColor,
                    this@SessionCollectionVerticalWidget::onCollectionSelected
                )
                rvItems.isVisible = false
            } else {
                button.isVisible = false
                rvItems.adapter = null
            }
        }
    }

    private fun onCollectionSelected(contentItem: ContentItem) {
        if (contentItem.isObject()) {
            contentItem.link?.let {
                context.startActivity(ExploreItemObjectActivity.newInstance(context, it))
            }
        } else {
            ExploreItemDetailActivity.start(
                context = context,
                item = contentItem,
                template = template,
                emptyState = emptyState
            )
        }
    }
}
