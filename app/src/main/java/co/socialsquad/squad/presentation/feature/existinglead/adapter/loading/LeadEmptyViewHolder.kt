package co.socialsquad.squad.presentation.feature.existinglead.adapter.loading

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate

class LeadEmptyViewHolder(val parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_lead_not_found_state)) {

    fun bind() {
    }
}
