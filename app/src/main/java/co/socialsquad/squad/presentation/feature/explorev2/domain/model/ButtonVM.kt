package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.view.View
import co.socialsquad.squad.domain.model.Button

class ButtonVM(
    val button: Button,
    val onClickListener: (view:View) -> Unit
)
