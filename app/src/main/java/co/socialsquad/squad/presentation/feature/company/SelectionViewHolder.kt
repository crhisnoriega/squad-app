package co.socialsquad.squad.presentation.feature.company

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_input_option.view.*

class SelectionViewHolder(
    itemView: View,
    private val onSelectedListener: Listener<SelectionItemViewModel>
) : ViewHolder<SelectionItemViewModel>(itemView) {

    private val glide = Glide.with(itemView)

    override fun bind(viewModel: SelectionItemViewModel) {
        with(viewModel) {
            setupImage(this)
            itemView.setOnClickListener {
                selected = true
                onSelectedListener.onClick(this)
            }
            itemView.rbCheck.isChecked = selected
            itemView.tvTitle.text = title
        }
    }

    private fun setupImage(viewModel: SelectionItemViewModel) {
        if (viewModel.imageUrl.isNullOrEmpty()) {
            itemView.ivImage.visibility = View.GONE
        } else {
            itemView.ivImage.visibility = View.VISIBLE
            itemView.apply {
                glide.load(viewModel.imageUrl).into(ivImage)
            }
        }
    }

    override fun recycle() {
        itemView.apply {
            glide.clear(ivImage)
        }
    }
}
