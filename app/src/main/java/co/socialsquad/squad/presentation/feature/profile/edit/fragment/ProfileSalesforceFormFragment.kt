package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.dialog.ConfirmLeaveNoSaveDialog
import co.socialsquad.squad.presentation.util.MaskUtils
import kotlinx.android.synthetic.main.fragment_edit_profile_salesforce_form.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ProfileSalesforceFormFragment(
    val title: String,
    val subtitle: String,
    val icon: String,
    val field: Field,
    val value: String?
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()
    private var wasChanged = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupContent()
        setupNextButton()
        setupSkipButton()
        setupBack()
        setupHeader()

        viewModel.saveStatus.observe(requireActivity(), Observer {
            when (it) {
                "ok" -> viewModel.state.value = StateNav("next", false)
            }
        })

        btnConfirmar.isEnabled = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_salesforce_form, container, false)

    private fun setupHeader() {
        // txtTitle.text = title
        // txtSubtitle.text = subtitle

    }

    private fun setupBack() {
        ibBack.setOnClickListener {
            if (wasChanged) {
                var confirmDialog = ConfirmLeaveNoSaveDialog() {
                    when (it) {
                        "confirm" -> viewModel.state.postValue(StateNav("previous", true))
                    }
                }
                confirmDialog.show(childFragmentManager, "confirm")

            } else {
                viewModel.state.postValue(StateNav("previous", true))
            }

        }
    }

    private fun setupContent() {
        edtField.setRequiredField(field.isMandatory)
        edtField.requestComponentFocus()

        edtField.setEditOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btnConfirmar.performClick()
            }
            false
        })

        edtField.addEditTextChangedListener(
            listener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    var oldValue = MaskUtils.unmask(value, MaskUtils.Mask.CPF)
                    var currentValue = MaskUtils.unmask(p0.toString(), MaskUtils.Mask.CPF)

                    wasChanged = oldValue != currentValue
                }

                override fun afterTextChanged(fieldValue: Editable?) {
                    if (field.isMinimalInput(fieldValue.toString())) {
                        checkValidField(fieldValue.toString())
                    } else {
                        txtValidationError.text = ""
                        txtHelpText.isVisible = true
                        txtValidationError.isVisible = false
                        btnConfirmar.isEnabled = false
                    }
                }
            }
        )
        field.inputMask()?.let {
            edtField.setEditInputMask(it)
        }
        edtField.setEditInputType(field.inputType())
        // -> edtField.setInnerPadding(16, 5, 16, 5)
        edtField.hint = field.label

        edtField.text = value

//        field.additionalInformation?.let {
//
//            txtSupplementary.text = it.supplementaryText
//
//            it.featuredText?.apply {
//                txtFeaturedText.text = this
//            } ?: run {
//                txtFeaturedText.isVisible = false
//            }
//        } ?: run {
//            llTxtFooter.isVisible = false
//        }


    }

    private fun checkValidField(fieldValue: String) {

        val needValidationMin: Boolean = field.validationMin != null
        val needValidationMax: Boolean = field.validationMax != null

        val validMin = !needValidationMin ||
                (needValidationMin && fieldValue.length >= field.validationMin!!)
        val validMax = !needValidationMax ||
                (needValidationMax && fieldValue.length <= field.validationMax!!)

        var validContentRequired = field.validInputType(fieldValue)

        var validated = (validMin && validMax && validContentRequired)

        field.validationError?.let {
            if (!validated) {
                txtValidationError.text = it
                txtHelpText.isVisible = false
                txtValidationError.isVisible = true
            } else {
                txtValidationError.text = ""
                txtHelpText.isVisible = true
                txtValidationError.isVisible = false
            }
        } ?: run {
            txtHelpText.isVisible = true
            txtValidationError.isVisible = false
        }

        btnConfirmar.isEnabled = validated
    }

    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    private fun setupNextButton() {
        btnConfirmar.setOnClickListener {
            btnConfirmar.isEnabled = false
            hideKeyboard(requireActivity())
            showLoading()

            viewModel.saveSalesForce(edtField.text)
        }

    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
