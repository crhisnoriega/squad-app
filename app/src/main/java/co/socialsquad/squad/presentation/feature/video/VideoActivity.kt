package co.socialsquad.squad.presentation.feature.video

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.socialsquad.squad.R
import com.google.android.exoplayer2.ui.PlayerView
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_VIDEO_URL = "EXTRA_VIDEO_URL"
        const val EXTRA_MUTED = "EXTRA_MUTED"
        const val EXTRA_PLAYBACK_POSITION = "EXTRA_PLAYBACK_POSITION"
    }

    private lateinit var sepvVideo: PlayerView

    private var destroyVideo = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        sepvVideo = video_sepv_video
    }

    override fun onResume() {
        super.onResume()

        val url = intent?.extras?.getString(EXTRA_VIDEO_URL)
        val position = intent?.extras?.getLong(EXTRA_PLAYBACK_POSITION)
        val muted = intent?.extras?.getBoolean(EXTRA_MUTED, false) ?: run { false }
        VideoManager.prepare(this, Uri.parse(url), sepvVideo, !muted, position)
        VideoManager.play()
    }

    override fun onPause() {
        VideoManager.hide()
        super.onPause()
    }

    private fun setPositionResult() {
        val intent = Intent().apply {
            val position = VideoManager.currentPosition()
            putExtra(EXTRA_PLAYBACK_POSITION, position)
            putExtra(EXTRA_MUTED, VideoManager.muted())
        }
        setResult(RESULT_OK, intent)
    }

    override fun onDestroy() {
        if (destroyVideo) VideoManager.release()
        super.onDestroy()
    }

    override fun onBackPressed() {
        destroyVideo = false
        setPositionResult()
        super.onBackPressed()
    }
}
