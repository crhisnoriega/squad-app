package co.socialsquad.squad.presentation.feature.kickoff.voila

import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import kotlinx.android.synthetic.main.fragment_voila.*

class VoilaActivity : BaseDaggerActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_voila)

        accessFeed.setOnClickListener { finish() }
    }
}
