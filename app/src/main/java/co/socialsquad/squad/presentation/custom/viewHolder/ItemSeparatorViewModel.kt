package co.socialsquad.squad.presentation.custom.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemSeparatorViewModel(
    val top: Boolean? = true,
    val bottom: Boolean? = true,
    val middle: Boolean? = true
) : ViewModel {
}