package co.socialsquad.squad.presentation.feature.social

import dagger.Binds
import dagger.Module

@Module
abstract class SocialModule {
    @Binds
    abstract fun view(socialFragment: SocialFragment): SocialView
}
