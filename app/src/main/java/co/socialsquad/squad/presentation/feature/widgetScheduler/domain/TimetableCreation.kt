package co.socialsquad.squad.presentation.feature.widgetScheduler.domain

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TimetableCreation(
    @SerializedName("date")
    val date: String,
    @SerializedName("shift_id")
    val shiftId: Long,
    @SerializedName("position_id")
    val positionId: Long,
    @SerializedName("notes")
    val notes: String,
) : Parcelable