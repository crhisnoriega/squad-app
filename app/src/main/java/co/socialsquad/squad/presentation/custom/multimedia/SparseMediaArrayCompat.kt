package co.socialsquad.squad.presentation.custom.multimedia

import androidx.collection.SparseArrayCompat

fun SparseArrayCompat<ViewTypeDelegateAdapter>.addAll(vararg delegates: Pair<Int, ViewTypeDelegateAdapter>) {
    delegates.forEach {
        this.put(it.first, it.second)
    }
}
