package co.socialsquad.squad.presentation.feature.company

import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.data.utils.TagWorker
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CompanyLoginPresenter @Inject constructor(
    private val companyLoginView: CompanyLoginView,
    private val preferences: Preferences,
    private val loginRepository: LoginRepository,
    private val signupRepository: SignupRepository,
    private val simpleWordEncrypt: SimpleWordEncrypt,
    private val tagWorker: TagWorker
) {

    private val compositeDisposable = CompositeDisposable()

    fun onCreate() {
        val cid = loginRepository.userCompany?.cid ?: ""
        companyLoginView.fillIdField(cid)
    }

    fun onInput(
        id: String?,
        password: String?
    ) {

        val conditionsMet = (
            !id.isNullOrBlank() &&
                !password.isNullOrBlank()
            )

        companyLoginView.setSaveButtonEnabled(conditionsMet)
    }

    private fun storeCredentials(password: String) {
        val encrypted = simpleWordEncrypt.encrypt(password)
        preferences.externalSystemPassword = encrypted?.encryptedMessage
        preferences.externalSystemPasswordIV = encrypted?.iv
    }

    fun onSaveClicked(
        id: String,
        password: String
    ) {
        val email = loginRepository.userCompany?.user?.email ?: ""

        val registerIntegrationRequest = RegisterIntegrationRequest(id.toLong(), password, email)

        compositeDisposable.add(
            signupRepository.registerIntegration(registerIntegrationRequest)
                .doOnNext { storeCredentials(registerIntegrationRequest.password) }
                .doOnSubscribe { companyLoginView.showLoading() }
                .subscribe(
                    { (token, userCompany) ->
                        loginRepository.token = token
                        loginRepository.subdomain = userCompany.company.subdomain
                        loginRepository.userCompany = userCompany
                        loginRepository.squadColor = userCompany.company.primaryColor

                        companyLoginView.hideLoading()
                        companyLoginView.setResultOK()
                        companyLoginView.closeScreen()
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        companyLoginView.hideLoading()
                        companyLoginView.showErrorMessageFailedToRegisterUser()
                    }
                )
        )
    }
}
