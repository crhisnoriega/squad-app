package co.socialsquad.squad.presentation.feature.explorev2.data

import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.*
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextBodyText
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextDataType
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextDivisor
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextLinks
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextMedia
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextObjects
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextResources
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextSubtitle
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextTitle
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2SectionItems
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow

@ExperimentalCoroutinesApi
class ExploreRepositoryImp(private val exploreAPI: ExploreAPI) : ExploreRepository {

    override fun getSections(): Flow<SectionList> {
        return flow {
            emit(exploreAPI.getSection())
        }
    }

    override fun search(searchTerm: String): Flow<ExploreSearchV2> {
        return flow {
            emit(exploreAPI.search(searchTerm))
        }.debounce(500L)
    }

    override fun searchBySection(sectionId: Int, searchTerm: String): Flow<ExploreSearchV2SectionItems> {
        return flow {
            emit(exploreAPI.searchBySection(sectionId, searchTerm))
        }.debounce(500L)
    }

    override fun getExplore(path: String): Flow<Explore> {
        return flow {
            emit(exploreAPI.getExplore(path))
        }.debounce(1000L)
    }

    override fun getObject(path: String): Flow<ObjectItem> {
        return flow {
            val apiObjectList = exploreAPI.getObject(path)
            emit(formatList(apiObjectList))
        }
    }

    override fun objectSchedule(objectId: String): Flow<Submissions> {
        return flow {
            val apiObjectList = exploreAPI.fetchScheduleSubmissions("1", objectId)
            emit(apiObjectList)
        }
    }

    override fun generatePublicLink(uniqueLinkRequest: UniqueLinkRequest): Flow<UniqueLinkResponse> {
        return flow{
            emit(exploreAPI.generateLink(uniqueLinkRequest))
        }
    }

    override fun putRichTextAction(link: String): Flow<String> {
        return flow {
            exploreAPI.putRichTextAction(link)
            emit("success")
        }
    }

    private fun formatList(objectItem: ObjectItem): ObjectItem {
        val formattedList = ArrayList<ViewType>()
        objectItem.richText?.items?.forEach {
            when (it.type) {
                RichTextDataType.TITLE -> formattedList.add(RichTextTitle(it))
                RichTextDataType.SUBTITLE -> formattedList.add(RichTextSubtitle(it))
                RichTextDataType.TEXT -> formattedList.add(RichTextBodyText(it))
                RichTextDataType.DIVISOR -> formattedList.add(RichTextDivisor())
                RichTextDataType.MEDIA -> formattedList.add(RichTextMedia(it))
                RichTextDataType.LINK -> formattedList.add(RichTextLinks(it))
                RichTextDataType.OBJECT -> formattedList.add(RichTextObjects(it))
                RichTextDataType.RESOURCE -> formattedList.add(RichTextResources(it))
                null -> Unit
            }
        }
        objectItem.richText?.formattedItems = formattedList
        return objectItem
    }
}
