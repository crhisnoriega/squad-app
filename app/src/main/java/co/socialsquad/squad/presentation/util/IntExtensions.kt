package co.socialsquad.squad.presentation.util

import android.content.res.Resources
import android.util.DisplayMetrics

fun Int.dpToPixels() = this * Resources.getSystem().displayMetrics.density

fun Int.pixelToDp(): Int {
    return (this / (Resources.getSystem().displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}
