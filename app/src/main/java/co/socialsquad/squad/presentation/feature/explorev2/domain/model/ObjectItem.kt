package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichText
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ObjectItem(
        @SerializedName("id") val id: Long,
        @SerializedName("medias") val medias: List<Media>? = null,
        @SerializedName("media") val media: Media? = null,
        @SerializedName("location") val location: Location,
        @SerializedName("snippet") val snippet: String? = null,
        @SerializedName("long_title") val longTitle: String? = null,
        @SerializedName("short_title") val short_title: String? = null,
        @SerializedName("long_description") val longDescription: String? = null,
        @SerializedName("short_description") val short_description: String? = null,
        @SerializedName("title") val title: String? = null,
        @SerializedName("custom_fields") val customFields: String? = null,
        @SerializedName("rich_text") var richText: RichText? = null,
        @SerializedName("tools") val tools: List<Tool> = listOf(),
        @SerializedName("section") val section: Section? = null,
        @SerializedName("complements") val complements: List<Complement> = listOf()
) : Parcelable
