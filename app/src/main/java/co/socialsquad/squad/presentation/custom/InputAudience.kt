package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_input_audience.view.*

class InputAudience(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    interface Listener {
        fun onRemoveClicked()
    }

    private val glide: RequestManager by lazy { Glide.with(context) }

    var listener: Listener? = null

    var audience = ""
        set(value) {
            tvAudience.text = value
            ivDetail.visibility = View.GONE
        }

    var audienceImage = R.drawable.ic_audience_detail_public
        set(value) {
            ivType.setImageResource(value)
            rcPotentialReach.visibility = View.GONE
            ivDetail.visibility = View.VISIBLE
        }

    var potentialReach = 0
        set(value) {
            if (value > 0) {
                rcPotentialReach.visibility = View.VISIBLE
                rcPotentialReach.setReach(value)
                ivDetail.visibility = View.INVISIBLE
            } else {
                rcPotentialReach.visibility = View.GONE
                ivDetail.visibility = View.VISIBLE
            }
        }

    var color = "#252525"
        set(value) {
            rcPotentialReach.setStrokeColor(value)
        }

    var detail1 = ""
        set(value) {
            tvDetailLine1.text = value
        }

    var detail2 = ""
        set(value) {
            tvDetailLine2.text = value
        }

    var imageResId = 0
        set(value) {
            ivDetail.setImageResource(value)
            ivDetail.visibility = View.VISIBLE
        }

    var imageUrl: String? = null
        set(value) {
            glide.load(value).circleCrop().into(ivDetail)
            ivDetail.visibility = View.VISIBLE
        }

    var isEditable = true
        set(value) {
            tvEdit.visibility = if (value) View.VISIBLE else View.GONE
            isClickable = value
        }

    init {
        View.inflate(context, R.layout.view_input_audience, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputAudience)) {
            short_title_lead.text = getString(R.styleable.InputAudience_name)
        }
    }

    private fun setupViews() {
        ibRemove.setOnClickListener { listener?.onRemoveClicked() }
    }
}
