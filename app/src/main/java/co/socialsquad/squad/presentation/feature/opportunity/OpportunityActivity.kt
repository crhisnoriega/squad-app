package co.socialsquad.squad.presentation.feature.opportunity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.opportunity.Field
import co.socialsquad.squad.data.entity.opportunity.FormFieldValue
import co.socialsquad.squad.data.entity.opportunity.Header
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityAction
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails
import co.socialsquad.squad.presentation.custom.CustomDialog
import co.socialsquad.squad.presentation.custom.CustomDialogButtonConfig
import co.socialsquad.squad.presentation.custom.CustomDialogConfig
import co.socialsquad.squad.presentation.custom.CustomDialogListener
import co.socialsquad.squad.presentation.feature.opportunity.detail.OpportunitySubmissionDetailActivity
import co.socialsquad.squad.presentation.feature.opportunity.form.FormFieldFragment
import co.socialsquad.squad.presentation.feature.opportunity.form.FormFieldListener
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityInitialsColors
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_opportunity.*
import javax.inject.Inject

class OpportunityActivity : BaseDaggerActivity(), OpportunityView, FormFieldListener {

    private val opportunity by extra<Opportunity>(EXTRA_OPPORTUNITY)
    private val opportunityAction by extra<OpportunityAction>(EXTRA_OPPORTUNITY_ACTION)
    private val opportunityType by extra<OpportunityObjectType>(EXTRA_OPPORTUNITY_TYPE)
    private val objectId by extra<Int>(EXTRA_OBJECT_ID)
    private val submissionColors: OpportunityInitialsColors by extra(EXTRA_COLORS)

    @Inject
    lateinit var presenter: OpportunityPresenter

    private var loading: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opportunity)
        setupNextButton()
        setupContinueButton()
        setupCloseButton()
        setupForm()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onRecommendationSuccess(oppotunitySubmission: OpportunitySubmissionResponse) {

        opportunityAction.form.let {
            val dialog = CustomDialog.newInstance(
                CustomDialogConfig(
                    iconRes = R.drawable.ic_recommendation_success,
                    titleRes = R.string.recommendation_success_title,
                    descriptionRes = R.string.recommendation_success_message,
                    dismissOnClickOutside = false,
                    buttonOneConfig = CustomDialogButtonConfig(
                        showButton = true,
                        buttonTextRes = R.string.recommendation_success_btn_recommend_track,
                        buttonTextColor = R.color.orange_yellow
                    ),
                    buttonTwoConfig = CustomDialogButtonConfig(
                        showButton = true,
                        buttonTextRes = R.string.recommendation_success_btn_recommend_other,
                        buttonTextColor = R.color.orange_yellow
                    ),
                    buttonThreeConfig = CustomDialogButtonConfig(
                        showButton = true,
                        buttonTextRes = R.string.recommendation_success_btn_close,
                        buttonTextColor = R.color.oldlavender
                    )
                )
            ).apply {
                this.listener = CustomDialogListener(
                    buttonOneClick = {
                        presenter.getSubmissionDetails(oppotunitySubmission)
                    },
                    buttonTwoClick = {
                        val intent =
                            context?.let { it1 ->
                                newIntent(
                                    it1,
                                    opportunity,
                                    opportunityAction,
                                    OpportunityObjectType.REAL_STATE,
                                    objectId,
                                    submissionColors
                                )
                            }
                        startActivityForResult(intent, OpportunityActivity.OPPORTUNITY_RC_CODE)
                        finish()
                    },
                    buttonThreeClick = {
                        dismiss()
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                )
            }

            Handler().postDelayed({ dialog.show(supportFragmentManager, "success_dialog") }, 1000)
        }
    }

    override fun onOpportunityDetailsClick(submissionDetails: SubmissionDetails) {
        this.startActivity(OpportunitySubmissionDetailActivity.newIntent(this, submissionDetails, submissionColors, opportunity?.title))
        finish()
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(this, getString(resId), null)
    }

    private fun setupForm() {
        opportunityAction.form.apply { presenter.setupForm(this, opportunity.title) }
    }

    override fun clearInputs() {
        setupForm()
    }

    override fun setButtonEnabled(isEnabled: Boolean) {
        btNext.enable(isEnabled)
    }

    override fun setupHeader(header: Header, title: String?) {
        squadHeader.bindFields(title, header.title, header.icon?.url)
    }

    override fun setupFields(fields: List<Field>) {
        val fieldsPagerAdapter = FieldsPagerAdapter(supportFragmentManager)

        fields.sortedBy { it.order }.forEach {
            fieldsPagerAdapter.addFieldFragment(FormFieldFragment.newInstance(it))
        }

        vpFields.adapter = fieldsPagerAdapter
        fieldsPagerAdapter.notifyDataSetChanged()

        checkConstrols()
    }

    override fun setNextButtonText(textId: Int) {
        btNext.text = getString(textId)
    }

    override fun onNextPage(page: Int) {
        vpFields.currentItem = page
        checkConstrols()
    }

    private fun checkConstrols() {

        if (vpFields.currentItem == 0) {
            ibClose.setImageResource(R.drawable.ic_close_feedback)
        } else {
            ibClose.setImageResource(R.drawable.ic_back_feedback)
        }

        vpFields.adapter?.let { adapter ->
            val fieldMandatory = (adapter as FieldsPagerAdapter).isMandatory(vpFields.currentItem)

            fieldMandatory?.let {
                val fieldValid = adapter.isValid(vpFields.currentItem)!!
                val fieldValue = adapter.getValue(vpFields.currentItem)!!

                // Last item
                if (vpFields.currentItem + 1 == adapter.count) {
                    tvSkip.visibility = View.GONE
                } else {
                    tvSkip.visibility = if (!it) View.VISIBLE else View.GONE
                }

                if (it) {
                    btNext.enable(fieldValid)
                } else {
                    btNext.enable(fieldValue.value.isEmpty() || fieldValid)
                }
            }
        }
    }

    override fun onBackPage(page: Int) {
        if (page < 0) {
            finish()
        } else {
            vpFields.currentItem = page
            btNext.enable(true)
            checkConstrols()
        }
    }

    override fun onFormCompleted() {
        presenter.submitOportunityForm(objectId, opportunityType.type)
    }

    override fun onBackPressed() {
        vpFields.adapter?.let { it ->
            presenter.onBackClicked(vpFields.currentItem, it.count)
        }
    }

    private fun setupNextButton() {
        btNext.setOnButtonClickListener {
            vpFields.adapter?.let { it ->
                val value = (it as FieldsPagerAdapter).getValue(vpFields.currentItem)
                value?.let { it1 -> presenter.addOpportunityFieldValue(it1) }
                presenter.onNextClicked(vpFields.currentItem + 1, it.count)
            }
        }
    }

    private fun setupContinueButton() {
        tvSkip.setOnClickListener { view ->
            vpFields.adapter?.let { it ->
                presenter.onNextClicked(vpFields.currentItem + 1, it.count)
            }
        }
    }

    private fun setupCloseButton() {
        ibClose.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onEnableNextButton(isEnabled: Boolean) {
        btNext.enable(isEnabled)
    }

    override fun showLoading() {
        btNext.loading(true)
    }

    override fun hideLoading() {
        hideKeyboard()
        btNext.loading(false)
    }

    class FieldsPagerAdapter(fragmentManager: FragmentManager) :
        FragmentPagerAdapter(fragmentManager) {

        private val fragments: MutableList<Fragment> = ArrayList()

        override fun getCount(): Int {
            return fragments.size
        }

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        fun addFieldFragment(fragment: Fragment) {
            fragments.add(fragment)
        }

        fun getValue(position: Int): FormFieldValue? {
            val fieldFragment = getItem(position) as FormFieldFragment
            return fieldFragment.getFieldValue()
        }

        fun isMandatory(position: Int): Boolean? {
            val fieldFragment = getItem(position) as FormFieldFragment
            return fieldFragment.isFieldMandatory()
        }

        fun isValid(position: Int): Boolean? {
            val fieldFragment = getItem(position) as FormFieldFragment
            return fieldFragment.isFieldValid()
        }
    }

    companion object {
        private const val EXTRA_OPPORTUNITY = "EXTRA_OPPORTUNITY"
        private const val EXTRA_OPPORTUNITY_ACTION = "EXTRA_OPPORTUNITY_ACTION"
        private const val EXTRA_OPPORTUNITY_TYPE = "EXTRA_OPPORTUNITY_TYPE"
        private const val EXTRA_OBJECT_ID = "EXTRA_OBJECT_ID"
        private const val EXTRA_COLORS = "EXTRA_COLORS"
        const val OPPORTUNITY_RC_CODE = 999
        fun newIntent(
            context: Context,
            opportunity: Opportunity,
            opportunityAction: OpportunityAction,
            opportunityType: OpportunityObjectType,
            objectId: Int,
            colors: OpportunityInitialsColors?
        ): Intent {
            return Intent(context, OpportunityActivity::class.java).apply {
                putExtra(EXTRA_OPPORTUNITY, opportunity)
                putExtra(EXTRA_OPPORTUNITY_ACTION, opportunityAction)
                putExtra(EXTRA_OPPORTUNITY_TYPE, opportunityType)
                putExtra(EXTRA_OBJECT_ID, objectId)
                putExtra(EXTRA_COLORS, colors)
            }
        }
    }
}
