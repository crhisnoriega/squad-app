package co.socialsquad.squad.presentation.feature.about

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.AboutResponse
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.feature.password.PostPasswordActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.setGradientBackground
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_about.*
import javax.inject.Inject

class AboutActivity : BaseDaggerActivity(), AboutView {

    private val aboutAdapter = AboutAdapter()
    private var videoUri: Uri? = null
    private var about: AboutResponse? = null

    companion object {
        const val extraRegister = "extra_register"
        const val extraSubdomain = "extra_subdomain"
        const val extraHasBack = "extra_has_back"
    }

    @Inject
    lateinit var presenter: AboutPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        detailsList.run {
            layoutManager = LinearLayoutManager(baseContext)
            adapter = aboutAdapter
        }

        presenter.onCreate(intent.extras!!.getString(extraSubdomain)!!)

        setupNextButton()
        setupScroll()
    }

    private fun setupScroll() {
        container.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                    if (scrollY > oldScrollY) {
                        presenter.onTermsScrollDown()
                    }
                }
        )
    }

    private fun setupNextButton() {
        bNext.setOnClickListener {
            presenter.onNext()
            val subdomain = intent.extras!!.getString(extraSubdomain)
            val registerJson = intent.extras!!.getString(extraRegister)

            val intent = Intent(this, PostPasswordActivity::class.java)
            intent.putExtra(PostPasswordActivity.extraSubdomain, subdomain)
            intent.putExtra(PostPasswordActivity.extraRegister, registerJson)
            intent.putExtra(PostPasswordActivity.extraCompany, Gson().toJson(about?.company))

            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if (about != null && about!!.media != null && about!!.media.mimeType == "video/mp4") {
            videoUri?.let { videoView.prepare(it) }
        }
    }

    override fun onPause() {
        if (about != null && about!!.media != null && about!!.media.mimeType == "video/mp4") {
            videoUri?.let { videoView.pause() }
        }
        super.onPause()
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.GONE
    }

    override fun showAbout(about: AboutResponse) {
        container.visibility = View.VISIBLE
        squadHeader.visibility = View.VISIBLE
        nextContainer.visibility = View.VISIBLE

        aboutAdapter.items(about.topics)
        setupContents(about)
    }

    override fun showTryAgain() {
        tryAgainContainer.visibility = View.VISIBLE
    }

    override fun hideTryAgain() {
        tryAgainContainer.visibility = View.GONE
    }

    private fun setupContents(about: AboutResponse) {
        this.about = about
        if (about.media != null) {
            if (about.media.mimeType == "video/mp4" && about.media.streamings != null) {
                videoView.apply {
                    visibility = View.VISIBLE

                    about.media.streamings?.apply {
                        loadThumbnail(this[0].thumbnailUrl)
                    }

                    videoUri = Uri.parse(about.media.url)
                    prepare(videoUri!!)
                }
            } else {
                image.visibility = View.VISIBLE
                Glide.with(baseContext).load(about.media.url).into(image)
            }
        } else {
            mediaView.visibility = View.GONE
        }

        intent.hasExtra(extraHasBack).apply {
            intent.extras!!.getBoolean(extraHasBack).takeIf { it }?.apply {
                back.setBackgroundResource(R.drawable.ic_back_feedback)
                back.visibility = View.VISIBLE
                back.setOnClickListener { finish() }
            } ?: kotlin.run {
                close.setBackgroundResource(R.drawable.ic_close_feedback)
                close.visibility = View.VISIBLE
                close.setOnClickListener {
                    startActivity(Intent(baseContext, OnboardingActivity::class.java))
                    finish()
                }
            }
        }

        status.text = about.description_text

        if (about.company.isPublic != null && about.company.isPublic!!) {
            about.company.aboutGradient?.apply {
                if (this.isNotEmpty()) {
                    setColorsConfiguration(this[0], this[1], about.company.aboutFooterBackground!!, about.company.textColor!!)
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        squadHeader.bindCompany(about.company)
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        mainContainer.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        finalSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        status.setTextColor(color)
        finalDescription.setTextColor(color)
        close.setColorFilter(color)
        back.setColorFilter(color)
        aboutAdapter.textColor(textColor)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
