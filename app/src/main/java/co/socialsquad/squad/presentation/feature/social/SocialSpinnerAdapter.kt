package co.socialsquad.squad.presentation.feature.social

import android.annotation.SuppressLint
import android.content.Context
import android.database.DataSetObserver
import android.view.View
import android.view.ViewGroup
import android.widget.SpinnerAdapter
import android.widget.TextView

class SocialSpinnerAdapter(
    private val context: Context,
    private val dropdownViewResId: Int,
    private val viewResId: Int,
    private val items: List<Int>
) : SpinnerAdapter {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = View.inflate(context, dropdownViewResId, null) as TextView
        if (position == 0) {
            textView.height = 0
            textView.tag = 0
        } else {
            textView.text = context.getString(items[position - 1])
            textView.tag = items[position - 1]
        }
        return textView
    }

    override fun registerDataSetObserver(observer: DataSetObserver) {}

    override fun unregisterDataSetObserver(observer: DataSetObserver) {}

    override fun getCount() = items.size + 1

    override fun getItem(position: Int): Int? {
        return if (position == 0 || items.isEmpty()) null else items[position - 1]
    }

    override fun getItemId(position: Int): Long {
        return if (position == 0 || items.isEmpty()) 0 else items[position - 1].toLong()
    }

    override fun hasStableIds() = false

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View =
        View.inflate(context, viewResId, null)

    override fun getItemViewType(position: Int) = 0

    override fun getViewTypeCount() = 1

    override fun isEmpty() = items.isEmpty()
}
