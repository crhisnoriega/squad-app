package co.socialsquad.squad.presentation.feature.ranking.repository

import co.socialsquad.squad.presentation.feature.ranking.data.RankingAPIV2
import co.socialsquad.squad.presentation.feature.ranking.model.*
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface RankingRepository {
    suspend fun fetchRankings(): Flow<RankListResponse>

    suspend fun fetchRankDetails(rankId: String): Flow<RankingItem>
    suspend fun agreeRankTerms(rankId: String): Flow<AgreeRefuseTermsResponse>
    suspend fun refuseRankTerms(rankId: String): Flow<AgreeRefuseTermsResponse>
    suspend fun fetchPositions(rankId: String, nextUrl: String): Flow<PositionsResponse>
    suspend fun fetchCriterias(rankId: String, nextUrl: String): Flow<CriteriasResponse>
    suspend fun fetchBenefits(): Flow<BenefitsResponse>
    suspend fun fetchCriteriaDetails(criteriaId: String?, areaId: String?): Flow<CriteriaData>
    suspend fun fetchBenefitDetails(
        benefitId: String,
        rankId: String,
        level: String
    ): Flow<BenefitData>


}


@ExperimentalCoroutinesApi
class RankingRepositoryImpl(private val rankingAPI: RankingAPIV2) : RankingRepository {
    override suspend fun fetchRankings(): Flow<RankListResponse> {
        return flow {
            emit(rankingAPI.fetchRankingList())
        }
    }

    override suspend fun fetchRankDetails(rankId: String): Flow<RankingItem> {
        return flow {
            emit(rankingAPI.fetchRankingDetails(rankId))
        }
    }


    override suspend fun agreeRankTerms(rankId: String): Flow<AgreeRefuseTermsResponse> {
        return flow {
            rankingAPI.agreeRankingTerms(rankId)
            emit(AgreeRefuseTermsResponse(true))
        }
    }

    override suspend fun refuseRankTerms(rankId: String): Flow<AgreeRefuseTermsResponse> {
        return flow {
            rankingAPI.refuseRankingTerms(rankId)
            emit(AgreeRefuseTermsResponse(false))
        }
    }

    override suspend fun fetchPositions(rankId: String, nextUrl: String): Flow<PositionsResponse> {
        return flow {
            if (nextUrl.isNullOrBlank()) {
                emit(rankingAPI.fetchPositions(rankId))
            } else {
                emit(rankingAPI.fetchPositionsUrl(nextUrl))
            }
        }
    }

    override suspend fun fetchCriterias(rankId: String, nextUrl: String): Flow<CriteriasResponse> {
        return flow {
            if (nextUrl.isNullOrBlank()) {
                emit(rankingAPI.fetchCriterias(rankId))
            } else {
                emit(rankingAPI.fetchCriteriasUrl(nextUrl))
            }
        }
    }

    override suspend fun fetchBenefits(): Flow<BenefitsResponse> {
        return flow {
            emit(rankingAPI.fetchBenefits())
        }
    }

    override suspend fun fetchCriteriaDetails(
        criteriaId: String?,
        areaId: String?
    ): Flow<CriteriaData> {
        return if (areaId == null) {
            flow {
                emit(rankingAPI.fetchCriteriaDetails(criteriaId ?: ""))
            }
        } else {
            flow {
                emit(rankingAPI.fetchCriteriaDetailsByArea(criteriaId ?: "", areaId))
            }
        }
    }

    override suspend fun fetchBenefitDetails(
        benefitId: String,
        rankId: String,
        level: String
    ): Flow<BenefitData> {
        return flow {
            emit(rankingAPI.fetchBenefitaDetails(benefitId, rankId, level))
        }
    }


}