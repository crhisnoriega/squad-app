package co.socialsquad.squad.presentation.feature.audio.detail

import dagger.Binds
import dagger.Module

@Module
abstract class AudioDetailModule {
    @Binds
    abstract fun view(audioDetailActivity: AudioDetailActivity): AudioDetailView
}
