package co.socialsquad.squad.presentation.feature.hub.intranet

import android.app.Activity
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import co.socialsquad.squad.R
import co.socialsquad.squad.data.encryption.EncryptedObject
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.entity.VirtualOfficeResponse
import co.socialsquad.squad.data.entity.VirtualOfficeResponse.VirtualOffice.Tool
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.VirtualOfficeRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class IntranetPresenter @Inject constructor(
    val loginRepository: LoginRepository,
    private var virtualOfficeRepository: VirtualOfficeRepository,
    private val intranetView: IntranetView,
    private val preferences: Preferences,
    private val simpleWordEncrypt: SimpleWordEncrypt
) {
    private val webUrl = loginRepository.userCompany?.company?.virtualOfficeUrl ?: ""
    private val cid = loginRepository.userCompany?.cid
    private val compositeDisposable = CompositeDisposable()

    private var isLoading = true
    private var isComplete = false

    fun onViewCreated() {
//        intranetView.setupWebView(webUrl, CustomClient())
        getVirtualOffices()
    }

    fun onVisibilityChanged(isVisible: Boolean) {
        if (isVisible) showCompanyLoginIfNeeded()
    }

    private fun showCompanyLoginIfNeeded() {
        val storedPassword = preferences.externalSystemPassword
        if (storedPassword.isNullOrEmpty()) intranetView.showCompanyLoginScreen()
    }

    private inner class CustomClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            intranetView.showLoading()
            return false
        }

        // For pre N devices
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            intranetView.showLoading()
            return false
        }

        override fun onPageFinished(view: WebView, url: String?) {
            intranetView.hideLoading()
            super.onPageFinished(view, url)
            if (url?.contains("/login") == true) {
                preferences.externalSystemPassword?.let {
                    view.loadUrl(
                        "javascript:(function() {" +
                            "document.getElementById('usuario').value = '$cid';" +
                            "document.getElementById('senha').value = '${simpleWordEncrypt.decrypt(
                                EncryptedObject(
                                    preferences.externalSystemPasswordIV
                                        ?: byteArrayOf(),
                                    it
                                )
                            )}';" +
                            "document.getElementById('btnLogar').click();" +
                            "})();"
                    )
                }
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int) {
        if (requestCode == IntranetFragment.REQUEST_CODE_COMPANY_LOGIN && resultCode == Activity.RESULT_OK) {
            intranetView.setupWebView(webUrl, CustomClient())
        }
    }

    private fun getVirtualOffices() {

        compositeDisposable.add(
            virtualOfficeRepository.getTools()
                .doOnSubscribe {
                    isLoading = true
                    intranetView.showLoading()
                }.doOnTerminate {
                    intranetView.hideLoading()
                    isLoading = false
                }.subscribe(
                    {
                        val list = setupVirtualOfficesList(it)
                        intranetView.setupOffices(list, loginRepository.userCompany?.company?.primaryColor)
                    },
                    {
                        it.printStackTrace()
                        intranetView.showMessage(R.string.erro_search_information)
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }

    private fun setupVirtualOfficesList(virtualOfficeResponse: VirtualOfficeResponse): MutableList<Tool> {

        val list = mutableListOf<Tool>()

        virtualOfficeResponse.response.forEachIndexed { index, virtualOffice ->
            val tool = Tool(virtualOffice.pk, virtualOffice.title, virtualOffice.order, null, 0)
            list.add(tool)
            list.addAll(virtualOffice.tools)
        }

        return list
    }
}
