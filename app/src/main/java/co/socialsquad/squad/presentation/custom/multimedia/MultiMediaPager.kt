package co.socialsquad.squad.presentation.custom.multimedia

import android.content.Context
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.widget.ViewPager2
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.util.ColorUtils
import com.google.gson.Gson
import com.zhpan.indicator.DrawableIndicator
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import kotlinx.android.synthetic.main.view_multi_media_pager.view.*

class MultiMediaPager(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var mPager: ViewPager2
    private val mIndicatorView: DrawableIndicator
    private val mLayoutIndicator: LinearLayout
    private var mLoading: Boolean = true

    init {
        setupStyledAttributes(attrs)
        changeTemplate(context)
        mPager = pager
        mIndicatorView = indicatorView
        mLayoutIndicator = layoutIndicator

        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        mIndicatorView
                .setSliderWidth(resources.getDimension(R.dimen.dimen_indicator_small))
                .setSliderHeight(resources.getDimension(R.dimen.dimen_indicator_small))
                .setSliderGap(50f)
                .setSliderColor(ContextCompat.getColor(context, R.color._c1c2c3), ColorUtils.parse(userCompany.company?.primaryColor!!))
                .setSlideMode(IndicatorSlideMode.SCALE)
                .setIndicatorStyle(IndicatorStyle.CIRCLE)

        var draw = if (userCompany.company?.primaryColor!!.equals("#b72c2f")) {
            R.drawable.ic_circle_cyrella
        } else {
            R.drawable.ic_circle_black
        }

        mIndicatorView.setIndicatorDrawable(R.drawable.ic_circle_gray_1, draw)
        mIndicatorView.setCheckedColor(ColorUtils.parse(userCompany.company?.primaryColor!!))
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.MultiMediaPager)) {
            mLoading = getBoolean(R.styleable.MultiMediaPager_loading, true)
        }
    }

    private fun changeTemplate(context: Context): View {
        return if (mLoading) {
            View.inflate(context, R.layout.view_multi_media_pager, this)
        } else {
            View.inflate(context, R.layout.view_multi_media_pager, this)
        }
    }

    fun bind(medias: List<Media>, lifecycleOwner: LifecycleOwner) {
        if (medias.isEmpty()) {
            visibility = GONE
            return
        }

        var singleMedia = medias.size == 1

        mPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        mPager.isUserInputEnabled = false
        var adapter = MultiMediaPagerAdapter(lifecycleOwner)
        adapter.addMedias(
                medias.filter {
                    it.getViewType() != MultiMediaDataType.NOT_SUPORTED.value
                }
        )

        mPager.adapter = adapter
        mPager.offscreenPageLimit = 1

        if (!singleMedia) {
            mLayoutIndicator.visibility = VISIBLE
            mPager.isUserInputEnabled = true
            mIndicatorView.setupWithViewPager(mPager)
        } else {
            mLayoutIndicator.visibility = GONE
        }

        mPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                //super.onPageSelected(position)
                (mPager.adapter as MultiMediaPagerAdapter).playItem(position)
            }
        })
    }
}