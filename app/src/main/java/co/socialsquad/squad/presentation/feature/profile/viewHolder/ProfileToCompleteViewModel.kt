package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.data.entity.CompleteYourProfile
import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileToCompleteViewModel(
    val completeYourProfile: CompleteYourProfile,
    var animate: Boolean? = false
) : ViewModel {

    fun isComplete(): Boolean {
        var profile_ok =
            if (completeYourProfile?.profile?.visible!!) completeYourProfile?.profile?.completed!! else true

        var context_ok =
            if (completeYourProfile?.context?.visible!!) completeYourProfile?.context?.completed!! else true

        var payment_ok =
            if (completeYourProfile?.payment?.visible!!) completeYourProfile?.payment?.completed!! else true


        return profile_ok && context_ok && payment_ok
    }
}
