package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.view_input_promotional_video.view.*

class InputPromotionalVideo(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    interface Listener {
        fun onRemoveClicked()
        fun onPlayClicked()
    }

    private val glide: RequestManager by lazy { Glide.with(context) }

    var listener: InputPromotionalVideo.Listener? = null

    var imageUrl: String? = null
        set(value) {
            glide.load(value).roundedCorners(context).into(ivDetail)
            ivDetail.visibility = View.VISIBLE
            ivPlay.visibility = View.VISIBLE
        }

    var scheduledDay = ""
        set(value) {
            tvDetailLine1.text = value
        }

    var scheduledTime = ""
        set(value) {
            tvDetailLine2.text = value
        }

    var isEditable = true
        set(value) {
            tvEdit.visibility = if (value) View.VISIBLE else View.GONE
            isClickable = value
        }

    init {
        View.inflate(context, R.layout.view_input_promotional_video, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputPromotionalVideo)) {
            short_title_lead.text = getString(R.styleable.InputPromotionalVideo_name)
        }
    }

    private fun setupViews() {
        ibRemove.setOnClickListener { listener?.onRemoveClicked() }
        ivDetail.setOnClickListener { listener?.onPlayClicked() }
    }
}
