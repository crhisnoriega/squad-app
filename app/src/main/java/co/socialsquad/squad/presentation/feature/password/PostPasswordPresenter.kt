package co.socialsquad.squad.presentation.feature.password

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.TermsRepository
import co.socialsquad.squad.presentation.util.Analytics
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PostPasswordPresenter @Inject constructor(
    private val view: PostPasswordView,
    private val repository: TermsRepository,
    private val loginRepository: LoginRepository,
    private val analytics: Analytics
) {

    private val compositeDisposable = CompositeDisposable()

    fun onNextClicked(password: String, subdomain: String) {
        analytics.sendEvent(Analytics.SIGNUP_CREATE_PASSWORD_TAP_NEXT)
        view.showLoading()
        compositeDisposable.add(
            repository.terms()
                .doOnSubscribe {
                    view.showLoading()
                }.doOnTerminate {
                    view.hideLoading()
                }.subscribe(
                    {
                        val gson = Gson()
                        val textJson = gson.toJson(it.results[0])
                        view.setTerms(textJson)
                    },
                    {
                        view.setErrorMessage(R.string.error_get_terms)
                        view.hideLoading()
                    }
                )
        )
    }

    fun onInputChanged(text: String) {
        text.takeIf { it.length >= 6 }?.apply { view.enableNextButton(true) }
            ?: kotlin.run { view.enableNextButton(false) }
    }

    fun setSubdomain(subdomain: String) {
        loginRepository.subdomain = subdomain
    }

    fun onCreate(intent: Intent) {
        analytics.sendEvent(Analytics.SIGNUP_CREATE_PASSWORD_SCREEN_VIEW)
        if (intent.hasExtra(PostPasswordActivity.extraCompany)) {
            val gson = Gson()
            val company = gson.fromJson(intent.extras!!.getString(PostPasswordActivity.extraCompany), Company::class.java)
            view.setupHeader(company)
        }
    }
}
