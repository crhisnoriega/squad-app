package co.socialsquad.squad.presentation.feature.social.comment

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_comment_list_item.view.*

class CommentViewHolder(itemView: View, var onDeleteListener: ViewHolder.Listener<CommentViewModel>) : ViewHolder<CommentViewModel>(itemView) {

    override fun bind(viewModel: CommentViewModel) {
        if (adapterPosition == 0) {
            itemView.llTopDivider.visibility = View.VISIBLE
        } else {
            itemView.llTopDivider.visibility = View.GONE
        }
        itemView.tvCommenterName.text = viewModel.userName
        itemView.tvCommentTime.text = if (viewModel.time == null) {
            itemView.context.getString(R.string.social_comment_posting)
        } else {
            setupTimestamp(viewModel.time!!)
        }
        itemView.tvCommentText.text = viewModel.text
        Glide.with(itemView).load(viewModel.userAvatar)
            .circleCrop()
            .crossFade()
            .into(itemView.ivCommenterAvatar)
        setupOptions(viewModel)

        itemView.ivCommenterAvatar.setOnClickListener {
            openUserActivity(viewModel.userPk)
        }
        itemView.tvCommenterName.setOnClickListener {
            openUserActivity(viewModel.userPk)
        }
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, creatorPK)
        }
        itemView.context.startActivity(intent)
    }

    private fun setupTimestamp(date: String): String {
        return date.toDate()?.elapsedTime(itemView.context)?.capitalize() ?: ""
    }

    private fun setupOptions(viewModel: CommentViewModel) {
        val options = object : ArrayList<Int>() {
            init {
                if (viewModel.canDelete) add(R.string.social_comment_option_delete)
            }
        }

        val socialSpinnerAdapter = SocialSpinnerAdapter(
            itemView.context,
            R.layout.textview_spinner_dropdown_options_item,
            R.layout.textview_spinner_dropdown_options,
            options
        )

        itemView.sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
        itemView.sOptions.adapter = socialSpinnerAdapter

        itemView.sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position) ?: return

                val itemResId = item as Int
                when (itemResId) {
                    R.string.social_comment_option_delete -> {
                        onDeleteListener.onClick(viewModel)
                    }
                }
                itemView.sOptions.setSelection(0)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun recycle() {
        Glide.with(itemView)
            .clear(itemView.ivCommenterAvatar)
    }
}
