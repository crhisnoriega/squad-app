package co.socialsquad.squad.presentation.feature.lead

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.OpportunityWidget
import co.socialsquad.squad.domain.model.lead.Options
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.existinglead.ExistingLeadActivity
import co.socialsquad.squad.presentation.feature.lead.di.LeadModule
import co.socialsquad.squad.presentation.feature.newlead.NewLeadActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_lead.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class LeadActivity : BaseActivity() {

    companion object {

        const val REQUEST_LEAD_OPTIONS = 1111
        private const val REQUEST_NEW_LEAD = 2222
        private const val REQUEST_EXISTING_LEAD = 3333

        const val RESULT_RELOAD_OPPORTUNITY = 1235
        const val RESULT_CREATE_NEW_OPPORTUNITY = 1234

        private const val EXTRA_OPPORTUNITY_WIDGET = "opportunity_widget"
        private const val EXTRA_OBJECT_TYPE = "EXTRA_OBJECT_TYPE"

        fun startForResult(activity: Activity, opportunityWidget: OpportunityWidget, code: Int, objectType: String? = "") {
            activity.startActivityForResult(createIntent(activity, opportunityWidget, objectType), code)
        }

        fun start(context: Context, opportunityWidget: OpportunityWidget) {
            context.startActivity(createIntent(context, opportunityWidget))
        }

        private fun createIntent(context: Context, opportunityWidget: OpportunityWidget, objectType: String? = ""): Intent {
            return Intent(context, LeadActivity::class.java).apply {
                putExtra(EXTRA_OPPORTUNITY_WIDGET, opportunityWidget)
                putExtra(EXTRA_OBJECT_TYPE, objectType)
            }
        }
    }

    private val opportunityWidget: OpportunityWidget by extra(EXTRA_OPPORTUNITY_WIDGET)
    private val objectType: String by extra(EXTRA_OBJECT_TYPE)
    override val modules = listOf(LeadModule.instance)
    override val contentView = R.layout.activity_lead
    private val viewModel by inject<LeadViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupObservable()
        setupCloseButton()

        if (savedInstanceState == null) {
            opportunityWidget.lead?.let {
                viewModel.loadLead(it)
            }
        }
    }

    private fun setupCloseButton() {
        ibClose.setOnClickListener {
            finish()
        }
    }

    private fun setupNextButton() {
        btnConfirmar.setOnClickListener {
            if (rbNewLead.isChecked) {
                opportunityWidget.lead?.form?.also { it ->
                    NewLeadActivity.startForResult(this, opportunityWidget.submission, opportunityWidget.widget, it, REQUEST_NEW_LEAD)
                    return@setOnClickListener
                }
            }
            if (rbExistingLead.isChecked) {
                ExistingLeadActivity.startForResult(this, opportunityWidget.submission, opportunityWidget.widget, REQUEST_EXISTING_LEAD, objectType)
                return@setOnClickListener
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_NEW_LEAD || requestCode == REQUEST_EXISTING_LEAD) {
            if (resultCode == RESULT_RELOAD_OPPORTUNITY || resultCode == RESULT_CREATE_NEW_OPPORTUNITY) {
                setResult(resultCode)
                finish()
            }
        }
    }

    private fun setupLeadOptions(options: Options) {
        txtTitle.text = options.title
        txtSubtitle.text = options.subtitle
        txtHelpText.text = options.helpText

        Glide.with(this)
                .load(options.icon.url)
                .crossFade()
                .into(imgLead)
    }

    private fun setupContent(options: Options) {
        txtNewLeadTitle.text = options.newLeadTitle
        txtNewLeadSubtitle.text = options.newLeadSubtitle
        txtExistingLeadTitle.text = options.existingLeadTitle
        txtExistingLeadSubtitle.text = options.existingLeadSubtitle
    }

    private fun setupRadios() {
        clExistingLead.setOnClickListener {
            rbExistingLead.performClick()
        }
        clNewLead.setOnClickListener {
            rbNewLead.performClick()
        }
        rbNewLead.setOnCheckedChangeListener { compoundButton, checked ->
            if (checked) {
                selectRadioNewLead()
            }
        }
        rbExistingLead.setOnCheckedChangeListener { compoundButton, checked ->
            if (checked) {
                selectRadioExistingLead()
            }
        }
    }

    private fun selectRadioNewLead() {
        btnConfirmar.isEnabled = true
        rbExistingLead.isChecked = false
    }

    private fun selectRadioExistingLead() {
        btnConfirmar.isEnabled = true
        rbNewLead.isChecked = false
    }

    private fun setupObservable() {
        lifecycleScope.launch {
            viewModel.lead.collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.apply {
                            setupLeadOptions(this.options)
                            setupNextButton()
                            setupRadios()
                            setupContent(this.options)
                        }
                    }
                }
            }
        }
    }
}
