package co.socialsquad.squad.presentation.feature.existinglead.adapter.result.lead

import android.view.ViewGroup
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.LeadResultsAdapter
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ResultsViewHolder
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM

class ExistingLeadDelegateAdapter(private val colorPrimary: String?, private val onCheckedChangeListener: LeadResultsAdapter.CheckedListener) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): ResultsViewHolder =
        ExistingLeadViewHolder(parent, colorPrimary, onCheckedChangeListener)

    override fun onBindViewHolder(holder: ResultsViewHolder, item: ExistingLeadVM) = holder.bind(item)
}
