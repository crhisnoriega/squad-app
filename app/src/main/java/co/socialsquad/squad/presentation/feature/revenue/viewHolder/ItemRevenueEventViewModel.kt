package co.socialsquad.squad.presentation.feature.revenue.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.revenue.model.EventData

class ItemRevenueEventViewModel(
    val eventData: EventData,
    val isFirst: Boolean? = false,
    val isLast: Boolean? = false
) : ViewModel {
}