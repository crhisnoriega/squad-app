package co.socialsquad.squad.presentation.feature.widgetScheduler.repository

import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleDate
import co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary.ScheduleSummary
import kotlinx.coroutines.flow.Flow

interface ScheduleSummaryRepository {
    fun fetchScheduleSummary(timetableId: Long, objectId: Long): Flow<ScheduleSummary?>
    fun fetchScaleDate(
        timetableId: Long,
        objectId: Long,
        position: Long?,
        date: String?
    ): Flow<ScaleDate?>
}
