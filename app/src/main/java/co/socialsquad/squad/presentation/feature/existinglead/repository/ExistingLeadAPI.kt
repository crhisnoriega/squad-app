package co.socialsquad.squad.presentation.feature.existinglead.repository

import co.socialsquad.squad.domain.model.leadsearch.LeadSearchResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface ExistingLeadAPI {

    @GET
    suspend fun searchExistingLead(@Url path: String): LeadSearchResponse
}
