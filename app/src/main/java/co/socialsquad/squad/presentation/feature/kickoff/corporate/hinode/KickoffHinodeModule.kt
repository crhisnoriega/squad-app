package co.socialsquad.squad.presentation.feature.kickoff.corporate.hinode

import dagger.Binds
import dagger.Module

@Module
abstract class KickoffHinodeModule {

    @Binds
    abstract fun view(kickoffHinodeFragment: KickoffHinodeFragment): KickoffHinodeView
}
