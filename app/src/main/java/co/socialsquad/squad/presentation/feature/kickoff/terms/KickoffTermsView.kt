package co.socialsquad.squad.presentation.feature.kickoff.terms

interface KickoffTermsView {
    fun showNavigation()
    fun showErrorMessageFailedToRegisterUser()
    fun showLoading()
    fun hideLoading()
}
