package co.socialsquad.squad.presentation.feature.hub

import android.util.Log
import co.socialsquad.squad.data.repository.LoginRepository
import javax.inject.Inject

class HubPresenter @Inject constructor(
    loginRepository: LoginRepository,
    private val hubView: HubView
) {
    private val logo = loginRepository.userCompany?.company?.logo
    private val backgroundColor = loginRepository.userCompany?.company?.primaryColor
    private val indicatorColor = loginRepository.userCompany?.company?.secondaryColor
    private val textColor = loginRepository.userCompany?.company?.textColor
    private val itemsPermitted = mutableListOf<HubItem>().apply {
        if (loginRepository.userCompany?.company?.featureDashboard == true) add(HubItem.Dashboard)
        if (loginRepository.userCompany?.company?.featureIntranet == true) add(HubItem.Intranet)
    }

    fun onViewCreated() {
        Log.i("color", "indicatorColor: $indicatorColor textColor: $textColor")
        hubView.setupToolbar(logo, backgroundColor)
        if (itemsPermitted.size > 0) hubView.setupTabs(itemsPermitted, indicatorColor, textColor)
    }
}
