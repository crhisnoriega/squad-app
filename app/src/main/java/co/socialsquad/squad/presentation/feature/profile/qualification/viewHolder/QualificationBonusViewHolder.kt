package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.view_qualification_bonus_item.view.*

const val QUALIFICATION_BONUS_VIEW_HOLDER_ID = R.layout.view_qualification_bonus_item

class QualificationBonusViewHolder(itemView: View) : ViewHolder<QualificationBonusViewModel>(itemView) {
    override fun bind(viewModel: QualificationBonusViewModel) {
        itemView.tvBonusValue.text = viewModel.bonus
    }

    override fun recycle() {
    }
}
