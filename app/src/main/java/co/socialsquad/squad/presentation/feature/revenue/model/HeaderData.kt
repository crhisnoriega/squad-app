package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class HeaderData(
    @SerializedName("title") val title: String?,
    @SerializedName("amount") val amount: String?,
    @SerializedName("month") val month: String?,
    @SerializedName("period") val period: String?,
    var first: Boolean = false


    ) : Parcelable