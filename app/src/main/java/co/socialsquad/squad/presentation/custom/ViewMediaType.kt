package co.socialsquad.squad.presentation.custom

import android.os.Parcelable

interface ViewMediaType : Parcelable {
    fun getViewType(): Int
    fun isVideoType(): Boolean
    fun isImageType(): Boolean
    fun isAudioType(): Boolean
}
