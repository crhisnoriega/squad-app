package co.socialsquad.squad.presentation.feature.explorev2.domain

import kotlinx.coroutines.flow.Flow
import okhttp3.ResponseBody

interface ExploreRepositoryNotAuth {
    fun getFileRichTextResource(fileUrl: String): Flow<ResponseBody>
}
