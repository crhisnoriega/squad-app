package co.socialsquad.squad.presentation.feature.explorev2.components.collectionhorizontal

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.BaseItemsLoadingAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate

class CollectionHorizontalItemsLoadingAdapter(template: SectionTemplate) :
    BaseItemsLoadingAdapter(template) {

    override fun getLayoutRes(template: SectionTemplate) = when (template) {
        SectionTemplate.Square -> R.layout.view_session_collection_horizontal_square_item_loading_state
        SectionTemplate.Circle -> R.layout.view_session_collection_horizontal_circle_item_loading_state
        SectionTemplate.RectangleVertical -> R.layout.view_session_collection_horizontal_rectangular_vertical_item_loading_state
        SectionTemplate.RectangleHorizontal -> R.layout.view_session_collection_horizontal_rectangular_horizontal_item_loading_state
        else -> R.layout.view_session_collection_horizontal_rectangular_horizontal_item_loading_state
    }
}
