package co.socialsquad.squad.presentation.feature.login.forgotpassword

import dagger.Binds
import dagger.Module

@Module
abstract class ResetPasswordModule {

    @Binds
    abstract fun view(view: ResetPasswordActivity): ResetPasswordView
}
