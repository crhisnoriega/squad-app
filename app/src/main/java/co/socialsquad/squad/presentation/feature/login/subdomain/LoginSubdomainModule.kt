package co.socialsquad.squad.presentation.feature.login.subdomain

import dagger.Binds
import dagger.Module

@Module
abstract class LoginSubdomainModule {

    @Binds
    abstract fun view(view: LoginSubdomainActivity): LoginSubdomainView
}
