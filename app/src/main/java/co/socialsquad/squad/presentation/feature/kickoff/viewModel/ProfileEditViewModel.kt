package co.socialsquad.squad.presentation.feature.kickoff.viewModel

import android.net.Uri
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.presentation.feature.profile.edit.data.*
import com.google.gson.Gson
import java.io.File

class ProfileEditViewModel(
    var loginRepository: LoginRepository,
    var profileRepository: ProfileRepository
) : ViewModel() {
    fun saveValue(name: String, value: String) {

    }

    var selectBankInfo: BankInfo? = null
    var currentPhoto: File? = null

    var rgImages = mutableListOf<MediaResponse>()

    var banksInfo = MutableLiveData<List<BankInfo>>()
    var state = MutableLiveData<StateNav>()
    var cepResponse = MutableLiveData<CEPResponse>()
    var userCompany = loginRepository.userCompany
    var saveStatus = MutableLiveData<String>()


    fun queryCEP(cep: String) {
        profileRepository.queryCEP(cep).subscribe({
            cepResponse.value = it
        }, {
            cepResponse.value = CEPResponse(erro = true)
        })
    }


    fun saveAvatarKickoff() {
        profileRepository.uploadAvatar(
            loginRepository.userCompany?.user?.pk!!,
            Uri.fromFile(currentPhoto)
        ).subscribe(
            {
                state.postValue(StateNav("name_form", false))
            },
            {
                state.postValue(StateNav("error_upload", false))

            }
        )
    }


    fun uploadMedia(position: Int, type: String, media: Uri) {
        profileRepository.uploadMedia(media).subscribe({
            rgImages.add(it)
            saveStatus.value = type
        }, {
            Log.i("media", it.message, it)
            saveStatus.value = "upload_error"
        })
    }

    fun saveAddress(
        street: String? = null,
        cep: String? = null,
        number: String? = null,
        address_city: String? = null,
        address_state: String? = null,
        address_neighborhood: String? = null,
        complement: String? = null
    ) {
        var userCompany = loginRepository.userCompany
        userCompany?.user?.street = street
        userCompany?.user?.zipcode = cep
        userCompany?.user?.number = number
        userCompany?.user?.address_city = address_city
        userCompany?.user?.address_state = address_state
        userCompany?.user?.address_neighborhood = address_neighborhood
        userCompany?.user?.complement = complement


        saveUser(userCompany, "address")

    }

    fun saveGender(gender: String?) {
        var userCompany = loginRepository.userCompany

        var userCompany2 = UserCompanyDTO().apply {
            user = UserDTO()
            user?.gender = gender
            user?.cpf = userCompany?.user?.cpf
            user?.birthday = userCompany?.user?.birthday
        }
        profileRepository.editUserV2(userCompany?.pk!!, userCompany2).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "gender"
        }, {
            saveStatus.value = "error"
        })
    }

    fun saveBirthday(birthday: String?) {
        var userCompany = loginRepository.userCompany

        var userCompany2 = UserCompanyDTO().apply {
            user = UserDTO()
            user?.birthday = birthday
            user?.cpf = userCompany?.user?.cpf
        }
        profileRepository.editUserV2(userCompany?.pk!!, userCompany2).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "birthday"
        }, {
            saveStatus.value = "error"
        })
    }

    private fun saveUser(userCompany: UserCompany?, eventName: String) {
        profileRepository.editUser(userCompany?.pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = eventName
        }, {
            saveStatus.value = "error"
        })
    }


    fun saveSalesForce(salesforce_id: String?) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.salesforce_id = salesforce_id
            this.user = UserDTO()
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = loginRepository.userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "ok"
        }, {
            saveStatus.value = "error"
        })
    }

    fun saveCPF(cpf: String?) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO()
            this.user?.birthday = userCompany?.user?.birthday
        }
        userCompany?.user?.cpf = cpf



        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "ok"
        }, {
            saveStatus.value = "error"
        })
    }

    fun saveRG(rg: String?) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO()
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = loginRepository.userCompany?.user?.birthday
        }
        userCompany?.user?.rg =
            rg?.replace(".", "")?.replace("-", "")?.replace("/", "")

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "rg"
        }, {
            saveStatus.value = "error"
        })
    }


    fun saveRGImages(title: String) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO()
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = loginRepository.userCompany?.user?.birthday
        }

        Log.i("images", "rr: ${Gson().toJson(rgImages)}")

        userCompany?.user?.rg_front_image_id = (rgImages[0]?.id)
        userCompany?.user?.rg_back_image_id = (rgImages[1]?.id)

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            lastUserCompany = it
            saveStatus.value = "ok_images_$title"
        }, {
            saveStatus.value = "error"
        })
    }

    fun saveCNPJ(cnpj: String?) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                this.bank_account_document = cnpj
            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "ok"
        }, {
            saveStatus.value = "error"
        })
    }

    fun saveAccountType(accountType: String) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                bank_account_type = accountType

            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            lastUserCompany = it
            saveStatus.value = "regime"
        }, {
            saveStatus.value = "error"
        })
    }

    fun savePix(
        pix: String?
    ) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                this.pix = pix
            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "pix"
        }, {
            saveStatus.value = "error"
        })
    }

    var bankCode: String? = ""
    var agency: String? = ""
    var accountNumber: String? = ""
    var accountType: String? = ""
    var isSaving: Boolean? = false

    fun storeAccountInfo(
        bankCode: String?,
        isSaving: Boolean?,
        agency: String?,
        accountNumber: String?
    ) {
        this.bankCode = bankCode
        this.agency = agency
        this.accountNumber = accountNumber
        this.isSaving = isSaving
        this.accountType = "PF"
    }

    fun saveAccountInfo(

    ) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                bank_account_bank = bankCode
                bank_account_branch = agency
                bank_account_number = accountNumber
                bank_account_type = accountType
                bank_account_savings = isSaving
                bank_account_digit = accountNumber?.get(accountNumber?.length!! - 1)?.toString()
                bank_account_name = selectBankInfo?.short_name
                bank_account_document = userCompany?.user?.cpf
            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            saveStatus.value = "ted"
        }, {
            saveStatus.value = "error"
        })
    }

    private var allBanks = mutableListOf<BankInfo>()

    fun fetchBanksInfo() {
        profileRepository.getBanksData().subscribe({
            banksInfo.value = it
            allBanks.addAll(it)
        }, {})
    }

    fun searchBank(search: String) {
        if (search.isNullOrBlank()) {
            banksInfo.value = allBanks
        } else {
            var results = allBanks.filter {
                it.short_name?.toLowerCase()?.contains(search.toLowerCase())!!
                        ||
                        it.code_number?.toLowerCase()?.contains(search.toLowerCase())!!
            }
            banksInfo.value = results
        }
    }

    fun saveName(text: String) {
        var nameParts = text.split(" ")

        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                firstName = nameParts[0]
                lastName = text.replace(nameParts[0], "")
            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday
        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            state.postValue(StateNav("cpf_form", false))
        }, {
        })


    }

    var answers = mutableMapOf<Fragment, String>()

    fun saveQuestions(text: String, fragments: List<Fragment>?) {
        var pk = loginRepository.userCompany?.pk

        var userCompany = UserCompanyDTO().apply {
            this.user = UserDTO().apply {
                context_question_1 = answers[fragments?.get(0)]
                context_question_2 = answers[fragments?.get(1)]
                context_question_3 = answers[fragments?.get(2)]
                context_question_4 = answers[fragments?.get(3)]
                context_question_5 = answers[fragments?.get(4)]
                context_question_6 = answers[fragments?.get(5)]
            }
            this.user?.cpf = userCompany?.user?.cpf
            this.user?.birthday = userCompany?.user?.birthday

        }

        profileRepository.editUserV2(pk!!, userCompany).subscribe({
            loginRepository.userCompany = it
            state.postValue(StateNav(text, false))
        }, {
        })
    }

    companion object {
        var lastUserCompany: UserCompany? = null
    }


}

data class StateNav(
    var name: String,
    var isBack: Boolean,
    var fragment: Fragment? = null
)