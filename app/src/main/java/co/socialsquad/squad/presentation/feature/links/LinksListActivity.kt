package co.socialsquad.squad.presentation.feature.links

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.LinkSingleViewHolder
import co.socialsquad.squad.presentation.custom.resource.viewHolder.LinkSingleViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkGroup
import co.socialsquad.squad.presentation.feature.resources.di.ResourceModule
import co.socialsquad.squad.presentation.feature.resources.viewmodel.ResourcesViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_links_list.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module


private const val RESOURCES_LIST = "links_list"
private const val RESOURCES_TITLE = "links_title"
private const val RESOURCES_SUBTITLE = "links_subtitle"

class LinksListActivity : BaseActivity() {

    companion object {
        fun start(context: Context, title: String, subtitle: String, linkGroup: LinkGroup) {
            context.startActivity(
                    Intent(context, LinksListActivity::class.java).apply {
                        putExtra(RESOURCES_LIST, Gson().toJson(linkGroup))
                        putExtra(RESOURCES_TITLE, title)
                        putExtra(RESOURCES_SUBTITLE, subtitle)
                    }
            )
        }
    }

    private val jsonLinkGroup by extra<String>(RESOURCES_LIST)
    private val title by extra<String>(RESOURCES_TITLE)
    private val subtitle by extra<String>(RESOURCES_SUBTITLE)
    private val viewModel by viewModel<ResourcesViewModel>()
    private val companyColor by inject<CompanyColor>()
    private val defaultEmptyState by lazy {
        EmptyState(
                SimpleMedia(""),
                getString(R.string.opportunity_empty_state_msg),
                getString(R.string.opportunity_empty_state_button)
        )
    }

    override val modules: List<Module> = listOf(ResourceModule.instance)
    override val contentView = R.layout.activity_links_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ibBack.setOnClickListener {
            finish()
        }

        tvTitle.text = title
        tvSubtitle.text = subtitle

        setupViewModelObservers()
        setupRecyclerView()
        setupAdapterInitialData()
    }

    private fun setupViewModelObservers() {
        viewModel.statusMutable.observe(this, Observer {

        })
    }

    private fun hideEmptyState() {
        group_empty.isVisible = false
    }

    private fun showEmptyState(emptyState: EmptyState) {
        Glide.with(this)
                .load(emptyState.icon.url)
                .crossFade()
                .into(img_lead_empty)

        lbl_empty_lead_message.text = emptyState.title
        btn_empty_action.text = emptyState.subtitle
        group_empty.isVisible = true
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG
        ).show()
    }

    private fun setupAdapterInitialData() {

        rvResources.apply {
            layoutManager = LinearLayoutManager(context)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is LinkSingleViewHolderModel -> LinkSingleViewHolder.ITEM_VIEW_MODEL_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    LinkSingleViewHolder.ITEM_VIEW_MODEL_ID -> LinkSingleViewHolder(view) {
                        startActivity(Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(it.link)
                        })
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter.apply {
                var counter = 0
                var linkGroup: LinkGroup = Gson().fromJson(jsonLinkGroup, LinkGroup::class.java)
                linkGroup.links?.forEach {
                    addItem(LinkSingleViewHolderModel(it), counter++)
                }
            }
        }
    }


    private fun setupRecyclerView() {

    }
}
