package co.socialsquad.squad.presentation.feature.profile.edit.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.data.BankInfo

class ItemProfileBankViewModel(var bankInfo: BankInfo, var isFirst: Boolean? = false) : ViewModel