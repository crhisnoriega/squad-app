package co.socialsquad.squad.presentation.feature.points.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.item_points_criteria.view.*

class ItemCriteriaPointsViewHolder(
    itemView: View,
    var onClickAction: ((item: ItemCriteriaPointsViewModel) -> Unit)
) : ViewHolder<ItemCriteriaPointsViewModel>(itemView) {

    override fun bind(viewModel: ItemCriteriaPointsViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }
        itemView.txtTitle.text = viewModel.criteriaData.title
        itemView.txtSubtitle.text = viewModel.criteriaData.description
        itemView.txtSubtitle333.text = viewModel.criteriaData.user_obtained_points
        itemView.detailsArrow.isVisible = viewModel.criteriaData.show_details!!

        Glide.with(itemView.context).load(viewModel.criteriaData.icon).into(itemView.imgIcon)
    }

    override fun recycle() {

    }

}