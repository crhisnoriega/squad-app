package co.socialsquad.squad.presentation.feature.explorev2.complement.price

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.explore.Price
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.resources.di.ResourceModule
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_price_list.*
import org.koin.core.module.Module

private const val RESOURCES_PRICE = "resources_price"

class PriceListActivity : BaseActivity() {

    companion object {
        fun start(context: Context, price:Price) {
            context.startActivity(
                    Intent(context, PriceListActivity::class.java).apply {
                        putExtra(RESOURCES_PRICE, price)
                    }
            )
        }
    }

    private val price by extra<Price>(RESOURCES_PRICE)
    override val modules: List<Module> = listOf(ResourceModule.instance)
    override val contentView = R.layout.activity_price_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        ibBack.setOnClickListener {
            finish()
        }

        setupHeader()
        setupPricesList()
    }

    private fun setupHeader(){
        tvTitle.text = price.title
    }

    private fun setupPricesList(){
        val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is PriceListViewModel -> PriceListViewHolder.ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PriceListViewHolder.ITEM_VIEW_MODEL_ID -> PriceListViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })

        rvResources.adapter = factoryAdapter.apply {
            addItems(price.lists.flatMap { listOf(PriceListViewModel(it)) })
        }
    }
}
