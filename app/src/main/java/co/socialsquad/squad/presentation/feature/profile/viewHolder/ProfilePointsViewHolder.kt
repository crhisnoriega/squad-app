package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.points.PointsActivity
import co.socialsquad.squad.presentation.feature.points.listing.POINTS_LIST_TYPE
import co.socialsquad.squad.presentation.feature.points.listing.PointsElementsListingActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import kotlinx.android.synthetic.main.item_card_points_profile.view.*

const val PROFILE_POINTS_VIEW_HOLDER_ID = R.layout.item_card_points_profile

class ProfilePointsViewHolder(itemView: View) : ViewHolder<ProfilePointsViewModel>(itemView) {


    override fun recycle() {
    }

    override fun bind(viewModel: ProfilePointsViewModel) {
        itemView.info.setOnClickListener {

            if (viewModel.point_system_id != null) {
                PointsActivity.start(itemView.context, null, viewModel.point_system_id )
            } else {
                PointsElementsListingActivity.start(
                    itemView.context,
                    POINTS_LIST_TYPE.SYSTEMS,
                    2,
                    "Sistemas"
                )
            }
        }
        itemView.short_title.text = viewModel.title
        itemView.short_description.text = viewModel.subtitle

        itemView.rlContactCircle2.backgroundTintList =
            ColorUtils.getCompanyColor(itemView.context)?.let { ColorUtils.stateListOf(it) }

//        if (viewModel.icon?.endsWith("png")!!) {
//            Glide.with(itemView.context).load(viewModel.icon)
//                .circleCrop()
//                .into(itemView.imgSection2)
//        } else {
//            GlideToVectorYou.init().with(itemView.context)
//                .load(Uri.parse(viewModel.icon), itemView.imgSection2)
//        }
    }
}
