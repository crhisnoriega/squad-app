package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.FooterViewModel

class FooterViewHolder<T : FooterViewModel>(itemView: View, val listener: Listener<T>) : ViewHolder<T>(itemView) {
    override fun bind(viewModel: T) {
        itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    override fun recycle() {}
}
