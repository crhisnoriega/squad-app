package co.socialsquad.squad.presentation.feature.profile.qualification.recommendations

import android.content.Intent
import android.util.Log
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.entity.Recommendation
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.repository.VideoRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_QUALIFICATION_RECOMMENDATIONS
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.live.list.LiveListItemViewModel
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import javax.inject.Inject

class QualificationRecommendationsPresenter @Inject constructor(
    private val tagWorker: TagWorker,
    private val profileRepository: ProfileRepository,
    loginRepository: LoginRepository,
    private val liveRepository: LiveRepository,
    private val videoRepository: VideoRepository,
    private val view: QualificationRecommendationsView
) {

    enum class Type : Serializable { EVENTS, AUDIO, FEED, LIVE, STORE }

    private var type: Type? = null
    private var pk: Int? = null
    private val compositeDisposable = CompositeDisposable()
    private var qualifications: List<Qualification> = listOf()
    private var currentRecommendingLive: LiveListItemViewModel? = null
    private var currentRecommendingVideo: VideoListItemViewModel? = null
    private var currentRecommendingAudio: AudioViewModel? = null
    private val companyColor = loginRepository.userCompany?.company?.primaryColor
    private val userAvatar = loginRepository.userCompany?.user?.avatar
    private val subdomain = loginRepository.subdomain

    private var isCompleted = false
    private var isLoading = false
    private var page: Int = 1
    private var unvisualizedTitled: Boolean = false
    private var visualizedTitled: Boolean = false
    private var tappedVideo: VideoListItemViewModel? = null

    private val observable
        get() = pk?.let { pk ->
            when (type) {
                Type.AUDIO ->
                    profileRepository.getQualificationRecommendationAudio(pk, page)
                        .doOnNext { isCompleted = it.next == null }
                        .map { it.results.orEmpty().map { AudioViewModel(it, companyColor, userAvatar, subdomain) } }
                        .map { addItemsWithHeaders(it.filter { !it.listened }, it.filter { it.listened }) }
                Type.FEED ->
                    profileRepository.getQualificationRecommendationVideos(pk, page)
                        .doOnNext { isCompleted = it.next == null }
                        .map {
                            it.results.orEmpty().filter {
                                (it.type == Post.TYPE_VIDEO || it.type == Post.TYPE_SUCCESS_STORY) && it.medias.orEmpty().any { FileUtils.isVideoMedia(it) }
                            }.map { VideoListItemViewModel(it) }
                        }
                        .map { addItemsWithHeaders(it.filter { !it.visualized }, it.filter { it.visualized }) }
                Type.LIVE ->
                    profileRepository.getQualificationRecommendationLives(pk, page)
                        .doOnNext { isCompleted = it.next == null }
                        .map { it.results.orEmpty().map { LiveListItemViewModel(it) } }
                        .map { addItemsWithHeaders(it.filter { !it.visualized }, it.filter { it.visualized }) }
                else -> Observable.just(listOf())
            }
        }

    private fun addItemsWithHeaders(notVisualized: List<ViewModel>, visualised: List<ViewModel>): List<ViewModel> {
        val viewModels = arrayListOf<ViewModel>()
        if (notVisualized.isNotEmpty() && !unvisualizedTitled) {
            viewModels.add(HeaderViewModel(null, null, R.string.qualification_recommendation_pending))
            unvisualizedTitled = true
        }
        viewModels.addAll(notVisualized)
        if (visualised.isNotEmpty() && !visualizedTitled) {
            if (viewModels.isNotEmpty()) {
                viewModels.add(DividerViewModel)
            }
            viewModels.add(HeaderViewModel(null, null, if (type == Type.AUDIO) R.string.qualification_recommendation_reproduced else R.string.qualification_recommendation_visualized))
            visualizedTitled = true
        }
        viewModels.addAll(visualised)
        return viewModels
    }

    fun onCreate(intent: Intent) {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_QUALIFICATION_RECOMMENDATIONS)
        pk = intent.getIntExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_PK, -1)
        type = when (intent.getStringExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_TYPE).toLowerCase()) {
            "events" -> Type.EVENTS
            "audio" -> Type.AUDIO
            "feed" -> Type.FEED
            "live" -> Type.LIVE
            "store" -> Type.STORE
            else -> null
        }
        prepareToolbar(intent)
        getRecommendedContent()
        getQualifications()
    }

    private fun prepareToolbar(intent: Intent) {
        val progress = intent.getIntExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_PROGRESS, 0)
        val total = intent.getIntExtra(QualificationRecommendationsActivity.EXTRA_RECOMMENDATION_TOTAL, 0)
        val title = when (type) {
            Type.EVENTS -> R.string.qualification_recommendation_title_events
            Type.AUDIO -> R.string.qualification_recommendation_title_audios
            Type.FEED -> R.string.qualification_recommendation_title_videos
            Type.LIVE -> R.string.qualification_recommendation_title_lives
            else -> R.string.qualification_title
        }
        view.setupToolbar(title, progress, total)
    }

    private fun getRecommendedContent() {
        observable?.let { observable ->
            compositeDisposable.add(
                observable.doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    view.stopRefreshing()
                    isLoading = false
                }.doOnNext {
                    if (type == Type.FEED) {
                        it.filterIsInstance(VideoListItemViewModel::class.java).forEach {
                            videoRepository.getSavedVideo(it.pk)?.let { savedVideo ->
                                it.position = savedVideo.position
                            }
                        }
                    }
                }.subscribe(
                    {
                        view.addItems(it, page == 1)
                    },
                    Throwable::printStackTrace
                )
            )
        }
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isCompleted) {
            page++
            getRecommendedContent()
        }
    }

    fun onVideoReturned(position: Long) {
        Log.i("position: ", "$position")
        tappedVideo?.let {
            it.position = position
            videoRepository.saveVideo(it)
            view.updateItem(it)
        }
    }

    fun onRefresh() {
        isCompleted = false
        page = 1
        unvisualizedTitled = false
        visualizedTitled = false
        compositeDisposable.clear()
        getRecommendedContent()
    }

    fun onLiveClicked(viewModel: LiveListItemViewModel) {
        compositeDisposable.add(
            liveRepository.markMediaViewed(viewModel.videoPk)
                .subscribe({}, Throwable::printStackTrace)
        )
        compositeDisposable.add(
            liveRepository.markLiveRecommendationAsViewed(viewModel.postLivePk.toLong())
                .subscribe({}, Throwable::printStackTrace)
        )
        view.openVideo(viewModel.videoUrl)
    }

    fun onVideoClicked(viewModel: VideoListItemViewModel) {
        tagWorker.tagEvent(TagWorker.TAG_TAP_PLAY_VIDEO)
        tappedVideo = viewModel
        compositeDisposable.add(
            videoRepository.markAsViewed(viewModel.videoPk.toLong())
                .subscribe({}, Throwable::printStackTrace)
        )
        compositeDisposable.add(
            videoRepository.markVideoRecommendationAsViewed(viewModel.pk.toLong())
                .subscribe({}, Throwable::printStackTrace)
        )
    }

    private fun getQualifications() {
        compositeDisposable.add(
            profileRepository.getQualifications()
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }.subscribe(
                    {
                        qualifications = it
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    fun onRecommendingLive(viewModel: LiveListItemViewModel) {
        currentRecommendingLive = viewModel
    }

    fun onRecommendingAudio(viewModel: AudioViewModel) {
        currentRecommendingAudio = viewModel
    }

    fun onRecommendingVideo(viewModel: VideoListItemViewModel) {
        currentRecommendingVideo = viewModel
    }

    fun onRecommend(values: List<String>, type: Recommendation) {
        val pks = mutableListOf<Int>()
        values.forEach { name ->
            qualifications.findLast { it.name == name }?.let {
                pks.add(it.pk)
            }
        }
        currentRecommendingVideo?.let {
            val recommendation = RecommendationRequest(it.pk, type.name.toLowerCase(), pks)
            recommend(recommendation)
        }
    }

    private fun recommend(recommendation: RecommendationRequest) {
        compositeDisposable.add(
            profileRepository.postRecommendation(recommendation)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }.subscribe(
                    {}, { it.printStackTrace() }
                )
        )
    }
}
