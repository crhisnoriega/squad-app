package co.socialsquad.squad.presentation.feature.store.product.details

import co.socialsquad.squad.presentation.custom.ViewModel

interface ProductDetailGroupView {
    fun setupToolbar(title: String)
    fun setupList(items: List<ViewModel>, color: String?)
    fun showImage(imageUrl: String)
    fun showVideo(thumbnailUrl: String, url: String)
}
