package co.socialsquad.squad.presentation.feature.opportunity

import co.socialsquad.squad.data.entity.opportunity.Field
import co.socialsquad.squad.data.entity.opportunity.Header
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.entity.opportunity.SubmissionDetails

interface OpportunityView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(resId: Int)
    fun onRecommendationSuccess(oppotunitySubmission: OpportunitySubmissionResponse)
    fun clearInputs()
    fun setButtonEnabled(isEnabled: Boolean)
    fun setupHeader(header: Header, title: String?)
    fun setupFields(fields: List<Field>)
    fun setNextButtonText(recommendationButtonSend: Int)
    fun onNextPage(page: Int)
    fun onBackPage(page: Int)
    fun onFormCompleted()
    fun onOpportunityDetailsClick(submissionDetails: SubmissionDetails)
}
