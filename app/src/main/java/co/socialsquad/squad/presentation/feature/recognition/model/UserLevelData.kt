package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserLevelData(
    @SerializedName("current_level") val current_level: Boolean?,
    @SerializedName("next_level") val next_level: Boolean?,
    @SerializedName("previous_level") val previous_level: Boolean?,
    @SerializedName("progress") val progress: Int?,
    @SerializedName("target_progress") val target_progress: Int?,
    @SerializedName("subtitle") val subtitle: String?
) : Parcelable