package co.socialsquad.squad.presentation.feature.social.create.schedule

import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULE_PROMOTIONAL_IMAGE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_CLEAR
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_EDIT
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_PLAY
import co.socialsquad.squad.presentation.util.DateTimeUtils
import co.socialsquad.squad.presentation.util.FileUtils
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

class SocialCreateSchedulePresenter
@Inject constructor(
    private val socialCreateScheduledLiveView: SocialCreateScheduledLiveView,
    private val loginRepository: LoginRepository,
    private val liveRepository: LiveRepository,
    private val tagWorker: TagWorker,
    private val contentResolver: ContentResolver
) {
    private val color = loginRepository.squadColor
    private lateinit var userAvatar: Uri
    var promotionalVideoUri: Uri? = null

    fun onCreate(intent: Intent) {
        socialCreateScheduledLiveView.setupToolbar(R.string.social_create_live_schedule_title)
        setupInitialData(intent)
    }

    private fun setupInitialData(intent: Intent) {
        var scheduleStartDate: Date? = null
        var scheduleEndDate: Date? = null
        if (intent.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE) ?: 0 > 0) {
            scheduleStartDate = intent.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE)?.let { Date(it) }
            if (intent.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_END_DATE) ?: 0 > 0) {
                scheduleEndDate = intent.extras?.getLong(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE)?.let { Date(it) }
            }
        }
        userAvatar = Uri.parse(
            if (intent.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE).isNullOrEmpty()) {
                loginRepository.userCompany?.user?.avatar ?: ""
            } else {
                intent.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE)
            }
        )
        socialCreateScheduledLiveView.setupFeaturedImage(userAvatar)
        socialCreateScheduledLiveView.setupViews(color, scheduleStartDate, scheduleEndDate)
        intent.extras?.getString(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_VIDEO)?.let {
            promotionalVideoUri = Uri.parse(it)
            socialCreateScheduledLiveView.setupPromotionalVideo(promotionalVideoUri!!)
        }
    }

    fun onResourceReady(bitmap: Bitmap) {
        val file = liveRepository.createPhotoFile()
        FileUtils.createFromBitmap(file, bitmap)
        with(Uri.fromFile(file)) {
            userAvatar = this
            liveRepository.scheduleImageMedia = this
        }
    }

    fun onPromotionalVideoSelected(videoUri: Uri) {
        val file = liveRepository.createVideoFile()
        val inputStream = contentResolver.openInputStream(videoUri)
        inputStream?.let {
            FileUtils.createFromInputStream(file, it)
            promotionalVideoUri = Uri.fromFile(file)
            liveRepository.scheduleVideoMedia = promotionalVideoUri
        }
    }

    fun onInput(date: Calendar?, startTime: Date?, endTime: Date?) {
        val conditionsMet = date != null && startTime != null && DateTimeUtils.afterNow(date, startTime)
        socialCreateScheduledLiveView.enableButton(conditionsMet)
        socialCreateScheduledLiveView.setupPromotionalVideoValues(date, startTime, endTime)
    }

    fun saveSchedule(date: Calendar, startTime: Date, endTime: Date?) {
        val startCalendar = Calendar.getInstance()
        startCalendar.apply {
            time = startTime
            set(Calendar.YEAR, date.get(Calendar.YEAR))
            set(Calendar.MONTH, date.get(Calendar.MONTH))
            set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH))
        }
        val intent = Intent()
        intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_START_DATE, startCalendar.time.time)
        endTime?.let {
            val endCalendar = Calendar.getInstance()
            endCalendar?.apply {
                time = it
                set(Calendar.YEAR, date.get(Calendar.YEAR))
                set(Calendar.MONTH, date.get(Calendar.MONTH))
                set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH))
                intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_END_DATE, endCalendar.time.time)
            }
        }
        intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_IMAGE, userAvatar.toString())
        intent.putExtra(SocialCreateScheduledLiveActivity.SCHEDULE_FEATURE_VIDEO, promotionalVideoUri?.toString())
        socialCreateScheduledLiveView.setFinishWithResult(intent)
    }

    fun onPromotionalVideoClicked() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO)
    }

    fun onPromotionalImageClicked() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULE_PROMOTIONAL_IMAGE)
    }

    fun onPromotionalVideoEditClicked() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_EDIT)
    }

    fun clearPromotionalVideo() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_CLEAR)
        promotionalVideoUri = null
        liveRepository.scheduleVideoMedia = null
        liveRepository.deleteCreatedVideo()
    }

    fun openPlayer() {
        tagWorker.tagEvent(TAG_TAP_SCHEDULE_PROMOTIONAL_VIDEO_PLAY)
        promotionalVideoUri?.let { socialCreateScheduledLiveView.openVideoActivity(it) }
    }
}
