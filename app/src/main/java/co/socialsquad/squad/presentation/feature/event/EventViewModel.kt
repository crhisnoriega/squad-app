package co.socialsquad.squad.presentation.feature.event

import co.socialsquad.squad.data.entity.Event
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.users.viewHolder.UserViewModel
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.Shareable
import co.socialsquad.squad.presentation.util.toDate
import java.io.Serializable
import java.util.Date

class EventViewModel(
    val pk: Int,
    val cover: String?,
    val startDate: Date?,
    val endDate: Date?,
    val title: String?,
    val shortTitle: String?,
    val location: String?,
    val category: String?,
    val companyColor: String?,
    val address: String?,
    val latitude: Double?,
    val longitude: Double?,
    val creator: String?,
    val audienceType: String?,
    val description: String?,
    val price: String?,
    val creatorPk: Int?,
    val speakers: List<UserViewModel>?,
    val vip: List<UserViewModel>?,
    val canShare: Boolean,
    val isAttending: Boolean,
    val numberOfAttendants: Int,
    val attendeesPictures: List<String>,
    val subdomain: String?
) : ViewModel, Serializable, Shareable {
    override val shareViewModel
        get() = ShareViewModel(
            pk,
            title
                ?: "",
            description, cover, Share.Event, canShare, subdomain
        )

    constructor(event: Event, companyColor: String? = "", subdomain: String?) : this(
        event.pk,
        event.cover?.url,
        event.startDate?.toDate(),
        event.endDate?.toDate(),
        event.title,
        event.shortTitle,
        event.location?.name,
        event.category?.name,
        companyColor,
        event.location?.address,
        event.location?.lat,
        event.location?.lng,
        event.owner?.user?.fullName,
        event.audienceType,
        event.content,
        event.tickets,
        event.owner?.pk,
        event.speakers?.map {
            UserViewModel(it.pk ?: 0, it.fullName ?: "", it.qualification, it.user?.avatar)
        },
        event.vip?.map {
            UserViewModel(it.pk ?: 0, it.fullName ?: "", it.qualification, it.user?.avatar)
        },
        event.canShare ?: false,
        event.confirmed ?: false,
        event.confirmationsCount ?: 0,
        event.confirmedUsersAvatars ?: listOf(),
        subdomain
    )
}
