package co.socialsquad.squad.presentation.feature.video

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.view.TextureView
import android.webkit.MimeTypeMap
import androidx.media.AudioAttributesCompat
import co.socialsquad.squad.presentation.feature.audio.AudioFocusWrapper
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory

object VideoManager {

    interface Listener {
        fun onPlayerEnded()
    }

    private const val AUDIO_ENABLED = 1f
    private const val AUDIO_DISABLED = 0f

    @SuppressLint("StaticFieldLeak")
    private var player: SimpleExoPlayer? = null
    private var audioFocusWrapper: AudioFocusWrapper? = null
    private var uri: Uri? = null
    private var wasPlaying = false

    var playbackPosition = player?.currentPosition
    var listener: Listener? = null

    private val videoEventListener = object : Player.EventListener {
        override fun onLoadingChanged(isLoading: Boolean) {}
        override fun onRepeatModeChanged(repeatMode: Int) {}
        override fun onSeekProcessed() {}
        override fun onPositionDiscontinuity(reason: Int) {}
        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_ENDED -> {
                    player?.seekTo(0)
                    player?.playWhenReady = false
                    wasPlaying = false
                    listener?.onPlayerEnded()
                }
                Player.STATE_BUFFERING -> {
                }
            }
        }
    }

    fun prepare(context: Context, uri: Uri, view: PlayerView, audioEnabled: Boolean, position: Long? = null) {

        if (player == null || uri != this.uri) {
            preparePlayer(uri, context)
        } else if (player != null) {
            player?.release()
            preparePlayer(uri, context)
        }

        player?.clearVideoSurface()
        player?.setVideoTextureView(view.videoSurfaceView as TextureView)
        if (position != null) {
            player?.seekTo(position)
        } else {
            player?.seekTo(currentPosition())
        }

        player?.volume = if (audioEnabled) AUDIO_ENABLED else AUDIO_DISABLED
        view.player = player
    }

    private fun preparePlayer(uri: Uri, context: Context) {
        this.uri = uri

        val trackSelector = DefaultTrackSelector(context)
        val loadControl = DefaultLoadControl()
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as android.media.AudioManager
        val audioAttributes = AudioAttributesCompat.Builder()
            .setContentType(AudioAttributesCompat.CONTENT_TYPE_MOVIE)
            .setUsage(AudioAttributesCompat.USAGE_MEDIA)
            .build()
        val exoPlayer = SimpleExoPlayer
            .Builder(context)
            .setTrackSelector(trackSelector)
            .setLoadControl(loadControl)
            .build()
        audioFocusWrapper = AudioFocusWrapper(audioAttributes, audioManager, exoPlayer)
        player = audioFocusWrapper?.player

        val httpDataSourceFactory = DefaultHttpDataSourceFactory("ua")
        val dataSourceFactory = DefaultDataSourceFactory(context, null, httpDataSourceFactory)

        val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        val mediaItem = MediaItem.Builder().setMimeType(mimeType).setUri(uri).build()
        val mediaSource = if (mimeType?.contains("video") == true) {
            ProgressiveMediaSource
                .Factory(dataSourceFactory)
                .createMediaSource(mediaItem)
        } else {
            HlsMediaSource.Factory(dataSourceFactory)
                .setAllowChunklessPreparation(true)
                .createMediaSource(mediaItem)
        }

        player?.setMediaSource(mediaSource)
        player?.addListener(videoEventListener)
        player?.prepare()
    }

    fun release() {
        audioFocusWrapper?.release()
        audioFocusWrapper = null
        player = null
    }

    fun hide() {
        audioFocusWrapper?.apply {
            wasPlaying = playWhenReady
            playWhenReady = false
        }
    }

    fun play() {
        wasPlaying = true
        audioFocusWrapper?.playWhenReady = wasPlaying
    }

    fun pause() {
        wasPlaying = false
        audioFocusWrapper?.playWhenReady = wasPlaying
    }

    fun mute() {
        player?.volume = AUDIO_DISABLED
    }

    fun muted(): Boolean {
        return player?.volume == AUDIO_DISABLED
    }

    fun unmute() {
        player?.volume = AUDIO_ENABLED
    }

    fun isPlaying() = audioFocusWrapper?.playWhenReady ?: false

    fun currentPosition() = audioFocusWrapper?.currentPosition ?: 0
}
