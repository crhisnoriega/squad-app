package co.socialsquad.squad.presentation.feature.login.password

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.login.alternative.EXTRA_LOGIN_ALTERNATIVE_TYPE
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativeActivity
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativePresenter
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.setGradientBackground
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login_password.*
import javax.inject.Inject

class LoginPasswordActivity : BaseDaggerActivity(), LoginPasswordView {

    companion object {
        const val extraCompany = "extra_company"
        const val EXTRA_LOGIN_SUBDOMAIN = "EXTRA_LOGIN_SUBDOMAIN"
        const val EXTRA_LOGIN_EMAIL = "EXTRA_LOGIN_EMAIL"
    }

    @Inject
    lateinit var presenter: LoginPasswordPresenter

    public override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_password)

        presenter.onCreate(intent)

        setupKeyboard()
        setupKeyboardAction()
        setupNextButtonBehavior()
        setupOnClickListeners()
        // setupDebug()
    }

    private fun setupDebug() {
        if (BuildConfig.DEBUG) {
            iltInput.editText?.setText("Squad@2018!")
        }
    }

    override fun setupHelpText(subdomain: String) {
        iltInput.setHelpText(getString(R.string.login_password_help, subdomain))
    }

    private fun setupKeyboard() {
        iltInput.requestFocus()
    }

    private fun setupKeyboardAction() {
        iltInput.setOnKeyboardActionListener { bNext.performClick() }
    }

    private fun setupNextButtonBehavior() {
        enableNextButton(false)
        iltInput.addTextChangedListener { text -> presenter.onInputChanged(text) }
    }

    private fun setupOnClickListeners() {
        bNext.setOnClickListener { presenter.onNextClicked() }
        tvAlternative.setOnClickListener { presenter.onAlternativeClicked() }
    }

    override fun enableNextButton(enabled: Boolean) {
        bNext.isEnabled = enabled
    }

    override fun showMessage(resId: Int) {
        iltInput.setErrorText(getString(resId))
    }

    override fun showLoading() {
        iltInput.loading(true)
    }

    override fun hideLoading() {
        iltInput.loading(false)
    }

    override fun openNavigation() {
        val intent = Intent(this, NavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun openLoginAlternative(subdomain: String, email: String, company: Company?) {
        val intent = Intent(this, LoginAlternativeActivity::class.java)
            .putExtra(EXTRA_LOGIN_SUBDOMAIN, subdomain)
            .putExtra(EXTRA_LOGIN_EMAIL, email)
            .putExtra(extraCompany, Gson().toJson(company))
            .putExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE, LoginAlternativePresenter.Type.RESET_PASSWORD)
        startActivity(intent)
    }

    override fun openProfileScreen() {
        val intent = Intent(this, KickoffActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun setupHeader(company: Company?) {
        back.setOnClickListener { finish() }

        company?.let {

            if (it.isPublic != null && it.isPublic!!) {
                it.aboutGradient?.apply {
                    if (this.isNotEmpty()) {
                        setColorsConfiguration(this[0], this[1], it.aboutFooterBackground!!, it.textColor!!)
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }
            }
            squadHeader.bindCompany(it)
        }
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        container.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        tvAlternative.setTextColor(color)
        back.setColorFilter(color)
        resetSeparator.setBackgroundColor(color)

        iltInput.setColors(textColor)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
