package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.view_social_post_interactions.view.*

class SocialCommentsViewHolder<T : ViewModel>(
    itemView: View,
    private val viewModel: T,
    private val onCommentListener: Listener<T>,
    private val onCommentCountListener: Listener<T>
) : ViewHolder<SocialCommentsViewModel>(itemView) {

    override fun bind(viewModel: SocialCommentsViewModel) {
        with(viewModel) {
            itemView.apply {
                llCommentContainer.setOnClickListener { onCommentListener.onClick(this@SocialCommentsViewHolder.viewModel) }
                tvCommentCount.setOnClickListener { onCommentCountListener.onClick(this@SocialCommentsViewHolder.viewModel) }
                when {
                    commentsCount > 1 -> {
                        tvCommentCount.visibility = View.VISIBLE
                        tvCommentCount.text = context.getString(R.string.social_comment_count_multiple, commentsCount)
                    }
                    commentsCount == 1 -> {
                        tvCommentCount.visibility = View.VISIBLE
                        tvCommentCount.text = context.getString(R.string.social_comment_count_one, commentsCount)
                    }
                    else -> tvCommentCount.visibility = View.GONE
                }
            }
        }
    }

    override fun recycle() {}
}

class SocialCommentsViewModel(
    var commentsCount: Int
) : ViewModel
