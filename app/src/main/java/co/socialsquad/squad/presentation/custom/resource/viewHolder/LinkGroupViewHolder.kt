package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.LinkGroup
import kotlinx.android.synthetic.main.item_link_list.view.*

class LinkGroupViewHolder(itemView: View, var callback: (linkGroup: LinkGroup) -> Unit) : ViewHolder<LinkGroupViewHolderModel>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_link_list

    }

    override fun bind(viewModel: LinkGroupViewHolderModel) {
        itemView.tvTitle.text = viewModel.linkGroup.title
        itemView.tvModified.text = viewModel.linkGroup.description
        itemView.setOnClickListener {
            callback.invoke(viewModel.linkGroup)
        }
    }

    override fun recycle() {

    }
}