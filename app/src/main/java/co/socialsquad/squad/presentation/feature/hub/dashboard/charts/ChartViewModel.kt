package co.socialsquad.squad.presentation.feature.hub.dashboard.charts

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Chart
import co.socialsquad.squad.presentation.custom.ViewModel
import com.github.mikephil.charting.data.BarEntry
import java.io.Serializable

const val CHART_VIEW_MODEL_ID = R.layout.view_chart_list_item

class ChartViewModel(
    val pk: Long,
    val title: String,
    val description: String?,
    val totalPoints: Long,
    val sourceImageUrl: String?,
    val source: String?,
    val dataArray: List<BarEntry>,
    var lastSeven: Boolean = false,
    val dataColor: String? = "#00FFFF"
) : ViewModel, Serializable {
    constructor(chart: Chart, companyColor: String?) : this(
        chart.pk,
        chart.title,
        chart.titleUnit,
        chart.totalValue,
        chart.sourceLogoUrl,
        chart.source,
        chart.points?.mapIndexed { i, l -> BarEntry(i.toFloat(), l.toFloat()) }.orEmpty(),
        false,
        companyColor
    )
}
