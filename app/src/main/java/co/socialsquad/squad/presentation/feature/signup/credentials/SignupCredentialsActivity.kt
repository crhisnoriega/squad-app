package co.socialsquad.squad.presentation.feature.signup.credentials

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.login.alternative.EXTRA_LOGIN_ALTERNATIVE_TYPE
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativePresenter
import co.socialsquad.squad.presentation.feature.login.alternative.success.LoginAlternativeSuccessActivity
import co.socialsquad.squad.presentation.feature.signup.acesscode.AccessCodeActivity
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlinx.android.synthetic.main.activity_signup_credentials.*
import kotlinx.android.synthetic.main.view_login_input_text.*
import javax.inject.Inject

class SignupCredentialsActivity : BaseDaggerActivity(), SignupCredentialsView, SignupCredentialsFragmentListener {

    companion object {
        private const val PASSWORD_MINIMUM_LENGTH = 6
    }

    @Inject
    lateinit var signupCredentialsPresenter: SignupCredentialsPresenter

    private lateinit var ibClose: ImageButton
    private lateinit var bNext: Button

    private val isEmailValid
        get() = TextUtils.isValidEmail(tietField.text.toString())

    private var loading: AlertDialog? = null

    public override fun onDestroy() {
        signupCredentialsPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_credentials)

        ibClose = signup_credentials_ib_close
        bNext = signup_credentials_b_next
        bNext.isEnabled = false

        setupOnClickListeners()
        setupTexts()

        iltInput.setOnKeyboardActionListener { bNext.performClick() }

        registerAccessCode.setOnClickListener {
            startActivity(Intent(baseContext, AccessCodeActivity::class.java))
        }

        signupCredentialsPresenter.onCreate()
    }

    override fun onResume() {
        super.onResume()
        iltInput.requestFocus()
        iltInput.showKeyboard()
    }

    private fun setupTexts() {
        tietField.addTextChangedListener(textWatcher())
        tietField.setOnKeyboardActionListener { if (isEmailValid) bNext.performClick() }
    }

    private fun setupOnClickListeners() {
        ibClose.setOnClickListener { finish() }

        bNext.setOnClickListener {
            signupCredentialsPresenter.onNextClicked(iltInput.getFieldText().toString())
        }
    }

    override fun openKickoff() {
        startActivity(Intent(this, KickoffActivity::class.java))
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(this, getString(resId), null)
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
        bNext.hideKeyboard()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun onKeyboardAction() {
        if (isEmailValid) bNext.performClick()
    }

    override fun onTextChanged(text: String) {
        bNext.isEnabled = isEmailValid
    }

    private fun textWatcher() = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            bNext.isEnabled = isEmailValid
        }

        override fun afterTextChanged(editable: Editable) {}
    }

    override fun setErrorMessage(res: Int) {
        iltInput.setErrorText(getString(res))
    }

    override fun navigateToSuccessView() {
        val intent = Intent(baseContext, LoginAlternativeSuccessActivity::class.java)
        intent.putExtra(LoginAlternativeSuccessActivity.EXTRA_EMAIL, iltInput.getFieldText().toString())
            .putExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE, LoginAlternativePresenter.Type.CREATE_ACCOUNT)
        startActivity(intent)
    }
}
