package co.socialsquad.squad.presentation.feature.audio.list

import android.app.Activity
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Qualification
import co.socialsquad.squad.data.entity.Recommendation
import co.socialsquad.squad.data.entity.request.RecommendationRequest
import co.socialsquad.squad.data.repository.AudioRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_DROP_LIST
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values.SegmentValuesPresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AudioListPresenter @Inject constructor(
    private val view: AudioListView,
    private val audioRepository: AudioRepository,
    private val profileRepository: ProfileRepository,
    loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()
    private var qualifications: List<Qualification> = listOf()
    private var currentRecommending: AudioViewModel? = null
    private var page = 1
    private var isLoading = true
    private var isComplete = false
    private val color = loginRepository.squadColor
    private val userAvatar = loginRepository.userCompany?.user?.avatar
    private val subdomain = loginRepository.subdomain

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_DROP_LIST)
        subscribe()
        getQualifications()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
        getQualifications()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            audioRepository.getAudioList(page)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }.subscribe(
                    {
                        val list = it.results.map { AudioViewModel(it, color, userAvatar, subdomain) }

                        if (page == 1 && list.isEmpty()) {
                            view.showEmptyState()
                        } else {
                            view.hideEmptyState()
                            if (page == 1) view.setItems(list) else view.addItems(list)
                            if (it.next == null) isComplete = true
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.audio_error_get_list)
                    }
                )
        )
    }

    private fun getQualifications() {
        compositeDisposable.add(
            profileRepository.getQualifications()
                .subscribe(
                    { qualifications = it },
                    { it.printStackTrace() }
                )
        )
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED || data == null) return

        if (requestCode == SegmentValuesPresenter.REQUEST_CODE_VALUES) {
            (data.getSerializableExtra(EXTRA_METRICS) as? MetricViewModel)?.let {
                recommendAudio(it.values.map { it.value })
            }
        }
    }

    fun onRecommending(viewModel: AudioViewModel) {
        currentRecommending = viewModel
    }

    private fun recommendAudio(values: List<String>) {
        val pks = mutableListOf<Int>()
        values.forEach { name ->
            qualifications.findLast { it.name == name }?.let {
                pks.add(it.pk)
            }
        }
        currentRecommending?.let {
            val recommendation = RecommendationRequest(it.pk, Recommendation.AUDIO.name.toLowerCase(), pks)
            recommend(recommendation)
        }
    }

    private fun recommend(recommendation: RecommendationRequest) {
        compositeDisposable.add(
            profileRepository.postRecommendation(recommendation)
                .subscribe(
                    { view.showMessage(R.string.profile_recommendation_success) },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.profile_recommendation_error)
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }
}
