package co.socialsquad.squad.presentation.util

import android.util.DisplayMetrics
import android.util.TypedValue

fun Float.toDips(diplayMetrics: DisplayMetrics) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, diplayMetrics)