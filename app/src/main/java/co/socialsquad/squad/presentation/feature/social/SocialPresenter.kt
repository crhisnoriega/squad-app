package co.socialsquad.squad.presentation.feature.social

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.ContentResolver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.CountDownTimer
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.core.content.MimeTypeFilter
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.LiveCreateRequest
import co.socialsquad.squad.data.entity.Post
import co.socialsquad.squad.data.entity.ScheduledPostRequest
import co.socialsquad.squad.data.entity.SegmentationQueryRequest
import co.socialsquad.squad.data.entity.SegmentationRequest
import co.socialsquad.squad.data.entity.SquadPermissions
import co.socialsquad.squad.data.entity.hasPermission
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.remote.ProgressRequestBody
import co.socialsquad.squad.data.repository.ChatRepository
import co.socialsquad.squad.data.repository.LiveRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CREATE_CAMERA_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CREATE_GALLERY_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CREATE_LIVE_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CREATE_MOVIE_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_CREATE_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SOCIAL_LIKE_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_SOCIAL_SHARE_POST
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.audio.create.REQUEST_CODE_AUDIO_FILE
import co.socialsquad.squad.presentation.feature.social.SocialFragment.Companion.REQUEST_CODE_LIVE_SCHEDULE_UPLOAD
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.create.schedule.SocialCreateScheduledLiveActivity
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.BitmapUtils
import co.socialsquad.squad.presentation.util.CameraUtils
import co.socialsquad.squad.presentation.util.FileUtils
import co.socialsquad.squad.presentation.util.toUTC
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.util.Date
import javax.inject.Inject

private const val REQUEST_CODE_IMAGE_CAPTURE = 1
private const val REQUEST_CODE_VIDEO_CAPTURE = 2
private const val REQUEST_CODE_PICK_IMAGE_OR_VIDEO = 3
private const val REQUEST_CODE_UPLOAD = 4
const val REQUEST_UPDATE_FEED = 5

class SocialPresenter @Inject constructor(
    private val context: Context,
    private val socialView: SocialView,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository,
    private val liveRepository: LiveRepository,
    private val contentResolver: ContentResolver,
    private val mixpanelAPI: MixpanelAPI,
    private val tagWorker: TagWorker,
    private val chatRepository: ChatRepository
) : ProgressRequestBody.UploadCallbacks, SocialCreateDialog.Listener {

    companion object {
        const val REQUEST_CODE_VIDEO_FULLSCREEN = 10
    }

    private var compositeDisposable = CompositeDisposable()

    private var page = 1
    private var complete = false
    private var countDownTimer: CountDownTimer? = null

    private var uri: Uri? = null
    private var currentDownline = false
    private var currentMetrics: MetricsViewModel? = null
    private var currentMembers: IntArray? = null
    private var currentDescription: String? = null
    private var currentScheduleDate: Date? = null
    private var liveRequest: LiveCreateRequest? = null

    private var hasUploadMedia = false
    private var hasUploadScheduleMedia = false
    private var schedulepk: Int? = null
    private var currentUploadProgress = 0
    private val mapper = SocialViewModelMapper()
    private val color = loginRepository.squadColor
    private val subdomain = loginRepository.subdomain

    fun onViewCreated(lifecycleOwner: LifecycleOwner) {
        val isAudioEnabled = socialRepository.audioEnabled
        socialView.setupList(isAudioEnabled)
        if (compositeDisposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

        initChat(lifecycleOwner)

        getFeed()

        val userCompany = loginRepository.userCompany

        userCompany?.company?.primaryColor?.let { socialView.setMessagesBadgeColor(it) }
        socialView.showCreateActionButton(userCompany.hasPermission(SquadPermissions.CREATION_ACTION))
    }

    fun onVisibleToUser() {
    }

    fun onResume() {
        onVisibleToUser()
    }

    private fun initChat(lifecycleOwner: LifecycleOwner) {
        if (loginRepository.userCompany?.company?.featureChat == true) {
            socialView.showChat()
            chatRepository.getChatInfoLiveData()
                .observe(
                    lifecycleOwner,
                    androidx.lifecycle.Observer { chatInfo ->
                        if (chatInfo == null) return@Observer
                        with(chatInfo) {
                            if (unreadCount > 0) socialView.showMessagesBadge(unreadCount)
                            else socialView.hideMessagesBadge()
                        }
                    }
                )
        } else {
            socialView.hideChat()
        }
    }

    private fun getFeed() {
        page = 1
        compositeDisposable.add(
            socialRepository.getFeed(page, 0)
                .doOnSubscribe { socialView.startLoading() }
                .doOnNext { complete = it.next == null }
                .map {
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .subscribe(
                    { posts ->
                        if (page == 1 && posts.isEmpty()) {
                            socialView.showEmptyState()
                        } else {
                            socialView.hideEmptyState()
                            socialView.setItems(posts)
                            page++
                        }

                        socialView.enableRefreshing()
                        socialView.stopLoading()
                    },
                    { throwable ->
                        complete = true
                        throwable.printStackTrace()
                        socialView.showMessage(
                            R.string.social_error_get_first_page,
                            DialogInterface.OnDismissListener {
                                socialView.stopLoading()
                            }
                        )
                    }
                )
        )
    }

    fun onRefresh() {
        complete = false
        socialView.stopLoading()
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getFeed(1, 0)
                .doOnTerminate { socialView.stopRefreshing() }
                .doOnNext { complete = it.next == null }
                .map {
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .subscribe(
                    { posts ->
                        if (posts.isEmpty()) {
                            socialView.showEmptyState()
                        } else {
                            socialView.hideEmptyState()
                            socialView.setItems(posts)
                            page = 2
                        }
                    },
                    { throwable ->
                        complete = true
                        throwable.printStackTrace()
                        socialView.showMessage(R.string.social_error_refresh)
                    }
                )
        )
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (complete) return
        compositeDisposable.add(
            socialRepository.getFeed(page, 0)
                .doOnSubscribe { socialView.startLoading() }
                .doOnNext { complete = it.next == null }
                .map {
                    it.posts =
                        it.posts.filter { it.type != Post.TYPE_LIVES || (it.type == Post.TYPE_LIVES && it.lives != null && it.lives!!.isNotEmpty()) }; it
                }
                .map {
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .subscribe(
                    { posts ->
                        socialView.stopLoading()
                        socialView.addItems(posts)
                        page++
                    },
                    { throwable ->
                        complete = true
                        throwable.printStackTrace()
                        socialView.showMessage(
                            R.string.social_error_get_next_page,
                            DialogInterface.OnDismissListener {
                                socialView.stopLoading()
                            }
                        )
                    }
                )
        )
    }

    private fun uploadMedia(
        description: String?,
        metrics: MetricsViewModel?,
        members: IntArray?,
        downline: Boolean,
        scheduledDate: Date?
    ) {
        currentDescription = description
        currentMetrics = metrics
        currentMembers = members
        currentDownline = downline
        currentScheduleDate = scheduledDate
        uploadMedia()
    }

    private fun uploadMedia() {
        hasUploadMedia = true

        val segmentationRequest = when {
            currentMetrics != null && currentMetrics!!.selected.isNotEmpty() -> {
                val queries =
                    currentMetrics!!.selected.map {
                        SegmentationQueryRequest(
                            it.field,
                            it.values.map { it.value }
                        )
                    }
                SegmentationRequest(queries = queries)
            }
            currentMembers != null && currentMembers!!.isNotEmpty() -> SegmentationRequest(members = currentMembers?.toList())
            currentDownline -> SegmentationRequest(downline = true)
            else -> null
        }

        val scheduleRequest = currentScheduleDate?.let { ScheduledPostRequest(it) }

        val feedRequest =
            FeedRequest(
                content = currentDescription,
                type = Post.TYPE_MEDIA,
                segmentation = segmentationRequest,
                scheduled = scheduleRequest
            )

        mixpanelAPI.track("Social / Create / Media / Post ")

        val uri = socialRepository.createMedia

        var isVideo = false

        socialView.apply {
            startUpload(context.getString(R.string.social_upload_sending))
            uri?.apply {
                isVideo = if (isVideoMedia(this)) {
                    showUploadContainer(getThumbnailMedia(this), true)
                    true
                } else {
                    showUploadContainer(getThumbnailImage(this), false)
                    false
                }
            }
        }

        val mediaUri = socialRepository.createMedia
        var currentPost: Post? = null
        compositeDisposable.add(

            socialRepository.postFeed(feedRequest)
                .flatMapCompletable { post ->
                    currentPost = post
                    mediaUri?.let { socialRepository.postFeedUpload(post.pk, it, this) }
                }.subscribe(
                    {
                        mixpanelAPI.track("Social / Create / Media / Post / Success ")
                        setupHideUploadContainerView()
                        currentPost?.apply {
                            if (!isVideo) mediaUri?.let {
                                addMediaToSocialList(this)
                            }
                        }

                        socialView.hideEmptyState()
                        socialRepository.deleteCreateFiles()
                        hasUploadMedia = false
                    },
                    { throwable ->

                        FirebaseCrashlytics.getInstance().log("uploadMedia error")
                        FirebaseCrashlytics.getInstance().log("media uri: $mediaUri")
                        FirebaseCrashlytics.getInstance().log("message: ${throwable.message}")
                        FirebaseCrashlytics.getInstance().recordException(throwable)

                        mixpanelAPI.track("Social / Create / Media / Post / Failure ")
                        socialView.apply {
                            setUploadError(context.getString(R.string.social_upload_error))
                        }
                        hasUploadMedia = false
                        throwable.printStackTrace()
                    }
                )
        )
    }

    private fun uploadScheduleMedia() {
        if (liveRequest == null) return

        val imageMediaUri = liveRepository.scheduleImageMedia ?: return
        val videoMediaUri = liveRepository.scheduleVideoMedia

        socialView.apply {
            startUpload(context.getString(R.string.social_upload_sending))
            imageMediaUri.apply {
                showUploadContainer(
                    getThumbnailImage(this),
                    false,
                    1,
                    if (videoMediaUri == null) 1 else 2
                )
            }
        }

        val pipeline: Completable = if (schedulepk != null) {
            liveRepository.scheduleMediaUpload(schedulepk!!, imageMediaUri, this, false)
                .concatWith(
                    if (videoMediaUri == null) {
                        Completable.fromAction {}
                    } else {
                        socialView.showUploadContainer(getThumbnailMedia(videoMediaUri), true, 2, 2)
                        liveRepository.scheduleMediaUpload(schedulepk!!, videoMediaUri, this, true)
                    }
                )
        } else {
            liveRepository.createLive(liveRequest!!)
                .flatMapCompletable { live ->
                    live.schedule?.pk?.let {
                        schedulepk = it
                        liveRepository.scheduleMediaUpload(it, imageMediaUri, this, false)
                            .concatWith(
                                if (videoMediaUri == null) {
                                    Completable.fromAction {}
                                } else {
                                    socialView.showUploadContainer(
                                        getThumbnailMedia(videoMediaUri),
                                        true,
                                        2,
                                        2
                                    )
                                    liveRepository.scheduleMediaUpload(
                                        schedulepk!!,
                                        videoMediaUri,
                                        this,
                                        true
                                    )
                                }
                            )
                    }
                }
        }
        compositeDisposable.add(
            pipeline
                .subscribe(
                    {
                        mixpanelAPI.track("Social / Create / Live / Schedule / Success")
                        setupHideUploadContainerView()
                        socialRepository.deleteCreateFiles()
                        liveRepository.scheduleVideoMedia = null
                        liveRepository.scheduleImageMedia = null
                        hasUploadMedia = false
                        hasUploadScheduleMedia = false
                        schedulepk = null
                        onRefresh()
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        mixpanelAPI.track("Social / Create / Live / Schedule / Failure")
                        socialView.apply {
                            setUploadError(context.getString(R.string.social_upload_error))
                        }
                    }
                )
        )
    }

    private fun addMediaToSocialList(post: Post) {
        post.type = Post.TYPE_MEDIA
        post.canEdit = true
        post.canDelete = true
        mapper.mapPost(
            post,
            socialRepository.audioEnabled,
            loginRepository.userCompany?.user?.avatar,
            subdomain
        )?.let {
            socialView.addItems(1, it)
        }
    }

    private fun setupHideUploadContainerView() {
        with(socialView) {
            startUploadIndeterminate(context.getString(R.string.social_upload_complete))
        }
        countDownTimer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                with(socialView) {
                    hideUploadContainer()
                }
            }
        }.start()
    }

    private fun getThumbnailImage(uri: Uri?): Bitmap {
        return MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
    }

    private fun getThumbnailMedia(uri: Uri?): Bitmap {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, uri)
        return retriever.frameAtTime
    }

    fun onAudioToggled(audioEnabled: Boolean) {
        socialRepository.audioEnabled = audioEnabled
    }

    fun onEditSelected(postViewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getSegmentation(postViewModel.pk)
                .doOnSubscribe { socialView.showLoading() }
                .doOnTerminate { socialView.hideLoading() }
                .subscribe(
                    {
                        postViewModel.audience =
                            AudienceViewModel(
                                postViewModel.audience.type,
                                it
                            )
                        when (postViewModel) {
                            is PostImageViewModel, is PostVideoViewModel -> socialView.showEditMedia(
                                postViewModel
                            )
                            is PostLiveViewModel -> socialView.showEditLive(
                                postViewModel
                            )
                        }
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        socialView.showMessage(R.string.social_error_failed_to_edit_post)
                    }
                )
        )
    }

    fun onDeleteClicked(pk: Int, viewModel: ViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.deleteFeed(pk)
                .doOnSubscribe { socialView.startRefreshing() }
                .doOnTerminate {
                    socialView.stopRefreshing()
                    socialView.hideDeleteDialog()
                }.subscribe(
                    {
                        socialView.removePost(viewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        socialView.showMessage(R.string.social_error_failed_to_delete_post)
                    }
                )
        )
    }

    fun onDestroyView() {
        countDownTimer?.apply {
            this.cancel()
        }
        compositeDisposable.dispose()
    }

    override fun onPhotoClicked() {
        tagWorker.tagEvent(TAG_TAP_CREATE_CAMERA_POST)
        if (hasUploadMedia) {
            socialView.showErrorMessageWaitCurrentUpload()
            return
        }

        val permissions = arrayOf(Manifest.permission.CAMERA)
        if (checkPermissions(permissions)) {
            mixpanelAPI.track("Social / Create / Media (Photo)")

            val file = socialRepository.createPhotoFile()
            uri = Uri.fromFile(file)
            socialView.showPhotoChooser(file, REQUEST_CODE_IMAGE_CAPTURE)
        } else {
            socialView.requestPermissions(
                SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_PHOTO,
                permissions
            )
        }
    }

    override fun onVideoClicked() {
        tagWorker.tagEvent(TAG_TAP_CREATE_MOVIE_POST)
        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
        if (checkPermissions(permissions)) {
            mixpanelAPI.track("Social / Create / Media (Video)")

            val file = socialRepository.createVideoFile()
            uri = Uri.fromFile(file)
            socialView.showVideoChooser(file, REQUEST_CODE_VIDEO_CAPTURE)
        } else {
            socialView.requestPermissions(
                SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_VIDEO,
                permissions
            )
        }
    }

    override fun onGalleryClicked() {
        tagWorker.tagEvent(TAG_TAP_CREATE_GALLERY_POST)
        if (hasUploadMedia) {
            socialView.showErrorMessageWaitCurrentUpload()
            return
        }
        mixpanelAPI.track("Social / Create / Media (Gallery)")

        socialView.showGalleryChooser(REQUEST_CODE_PICK_IMAGE_OR_VIDEO)
    }

    override fun onLiveClicked() {
        tagWorker.tagEvent(TAG_TAP_CREATE_LIVE_POST)
        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
        if (checkPermissions(permissions)) {
            if (CameraUtils(context).isCameraAvailable()) {
                mixpanelAPI.track("Social / Create / Live")
                val intent = Intent(context, SocialCreateLiveActivity::class.java)
                socialView.showCreateLive(intent)
            } else {
                socialView.showMessage(R.string.error_camera_not_available)
            }
        } else {
            socialView.requestPermissions(
                SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_LIVE,
                permissions
            )
        }
    }

    override fun onAudioFileClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_CREATE_GALLERY_DROP)
        socialView.showAudioChooser(REQUEST_CODE_AUDIO_FILE)
    }

    override fun onAudioRecordClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_CREATE_RECORD_DROP)
        val permissions = arrayOf(Manifest.permission.RECORD_AUDIO)
        if (checkPermissions(permissions)) {
            socialView.openAudioRecord()
        } else {
            socialView.requestPermissions(
                SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_AUDIO_RECORD,
                permissions
            )
        }
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_SOCIAL_SHARE_POST)
    }

    private fun checkPermissions(permissions: Array<String>) = permissions.all {
        ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
        if (grantResults.isEmpty() || grantResults.any { it == PackageManager.PERMISSION_DENIED }) {
            socialView.showMessage(R.string.error_permissions_not_granted)
        } else when (requestCode) {
            SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_PHOTO -> onPhotoClicked()
            SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_VIDEO -> onVideoClicked()
            SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_LIVE -> onLiveClicked()
            SocialFragment.REQUEST_CODE_PERMISSIONS_FOR_AUDIO_RECORD -> onAudioRecordClicked()
        }
    }

    fun onActivityResult(context: Context, requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != RESULT_OK) return

        when (requestCode) {
            REQUEST_CODE_IMAGE_CAPTURE -> {
                val bitmap = uri?.let { BitmapUtils.normalizeExifRotation(context, it) }
                val file = File(uri?.path)
                FileUtils.createFromBitmap(file, bitmap!!)
                socialRepository.createMedia = uri
                val intent = Intent(context, SocialCreateMediaActivity::class.java)
                intent.putExtra(
                    SocialCreateMediaActivity.EXTRA_TYPE,
                    SocialCreateMediaActivity.TYPE_IMAGE
                )
                socialView.showCreateFromPhoto(intent, REQUEST_CODE_UPLOAD)
            }
            REQUEST_CODE_VIDEO_CAPTURE -> {
                socialRepository.createMedia = uri
                val intent = Intent(context, SocialCreateMediaActivity::class.java)
                intent.putExtra(
                    SocialCreateMediaActivity.EXTRA_TYPE,
                    SocialCreateMediaActivity.TYPE_VIDEO
                )
                socialView.showCreateFromVideo(intent, REQUEST_CODE_UPLOAD)
            }
            REQUEST_CODE_PICK_IMAGE_OR_VIDEO -> {
                val intent = Intent(context, SocialCreateMediaActivity::class.java)

                val dataUri = data?.data ?: return
                val mimeType = contentResolver.getType(dataUri)
                val file = socialRepository.createGalleryFile(mimeType!!)
                uri = Uri.fromFile(file)

                Completable
                    .fromAction {
                        if (MimeTypeFilter.matches(mimeType, "image/*")) {
                            intent.putExtra(
                                SocialCreateMediaActivity.EXTRA_TYPE,
                                SocialCreateMediaActivity.TYPE_IMAGE
                            )
                            val bitmap =
                                BitmapUtils.normalizeExifRotation(
                                    context,
                                    dataUri,
                                    MediaStore.Images.Media.getBitmap(contentResolver, dataUri)
                                )
                            FileUtils.createFromBitmap(file, bitmap)
                        } else if (MimeTypeFilter.matches(mimeType, "video/*")) {
                            intent.putExtra(
                                SocialCreateMediaActivity.EXTRA_TYPE,
                                SocialCreateMediaActivity.TYPE_VIDEO
                            )
                            val inputStream = contentResolver.openInputStream(dataUri)
                            FileUtils.createFromInputStream(file, inputStream!!)
                        }
                    }.onDefaultSchedulers()
                    .doOnSubscribe { socialView.showLoading() }
                    .doOnTerminate { socialView.hideLoading() }
                    .subscribe(
                        {
                            socialRepository.createMedia = uri
                            socialView.showCreateFromGallery(intent, REQUEST_CODE_UPLOAD)
                        },
                        {
                            it.printStackTrace()
                        }
                    )
            }
            REQUEST_CODE_UPLOAD -> {
                val description = data?.getStringExtra(SocialFragment.EXTRA_UPLOAD_DESCRIPTION)
                val metricValue =
                    data?.getSerializableExtra(SocialFragment.EXTRA_UPLOAD_METRIC_VALUE) as? MetricsViewModel
                val members = data?.getIntArrayExtra(SocialFragment.EXTRA_UPLOAD_MEMBERS)
                val downline = data?.getBooleanExtra(SocialFragment.EXTRA_UPLOAD_DOWNLINE, false)
                val scheduledDate =
                    data?.getLongExtra(SocialFragment.EXTRA_UPLOAD_SCHEDULE, 0)?.let {
                        if (it > 0) Date(it).toUTC() else null
                    }
                uploadMedia(description, metricValue, members, downline ?: false, scheduledDate)
            }
            SocialFragment.REQUEST_CODE_EDIT_MEDIA,
            SocialFragment.REQUEST_CODE_EDIT_LIVE -> {
                socialView.scrollToTop()
                socialView.startRefreshing()
                onRefresh()
            }
            REQUEST_CODE_VIDEO_FULLSCREEN -> {
                data?.apply {
                    socialView.onReturnVideo(getLongExtra(VideoActivity.EXTRA_PLAYBACK_POSITION, 0))
                }
            }
            REQUEST_CODE_LIVE_SCHEDULE_UPLOAD -> {
                liveRequest =
                    data?.getSerializableExtra(SocialCreateScheduledLiveActivity.SCHEDULE_LIVE_REQUEST) as? LiveCreateRequest
                    ?: return
                hasUploadMedia = true
                hasUploadScheduleMedia = true
                uploadScheduleMedia()
            }
            REQUEST_UPDATE_FEED -> {
                socialView.updateFeed()
            }
            REQUEST_CODE_AUDIO_FILE -> {
                data?.data?.let { socialView.openAudioCreate(it) }
            }
        }
    }

    fun onTryAgainClicked() {
        if (hasUploadScheduleMedia) {
            uploadScheduleMedia()
        } else {
            uploadMedia()
        }
    }

    private fun isVideoMedia(uri: Uri): Boolean {
        return if (isImageMedia(uri)) {
            false
        } else {
            val retriever = MediaMetadataRetriever()
            retriever.setDataSource(context, uri)
            return "yes" == retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO)
        }
    }

    private fun isImageMedia(uri: Uri): Boolean {
        val filePath = uri.path.toString()
            .toLowerCase()
        val mimeType = contentResolver.getType(uri)
        if (filePath.contains("image") || filePath.contains("jpg") || filePath.contains(
                "png"
            ) || MimeTypeFilter.matches(mimeType, "image/*")
        ) {
            return true
        }
        return false
    }

    override fun onProgressUpdate(percentage: Int) {
        val diff = percentage - currentUploadProgress
        socialView.incrementUploadProgressBy(diff)
        currentUploadProgress += diff
    }

    fun onCancelUploadClicked() {
        hasUploadMedia = false
        hasUploadScheduleMedia = false
    }

    fun onCreateClicked() {
        tagWorker.tagEvent(TAG_TAP_CREATE_POST)
        loginRepository.userCompany?.apply {
            val canCreateFromCamera = canCreateCamera()
            val canCreateFromGallery = canCreatePhoto() && canCreateVideo()
            val canCreateLive = canCreateLive()
            val canCreateAudio = canCreateAudio()
            socialView.showCreateDialog(
                canCreateFromCamera,
                canCreateFromGallery,
                canCreateLive,
                canCreateAudio
            )
        }
    }

    fun likePost(post: PostViewModel) {
        tagWorker.tagEvent(TAG_TAP_SOCIAL_LIKE_POST)
        compositeDisposable.add(
            socialRepository.likePost(post.pk).subscribe(
                {
                    post.liked = it.liked
                    post.likeAvatars = it.likedUsersAvatars
                    post.likeCount = it.likesCount
                    socialView.updatePost(post)
                },
                {
                    it.printStackTrace()
                    post.liked = false
                    socialView.updatePost(post)
                },
                {}
            )
        )
    }
}
