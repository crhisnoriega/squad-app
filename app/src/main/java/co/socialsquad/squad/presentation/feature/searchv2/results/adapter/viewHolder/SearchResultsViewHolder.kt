package co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreObject
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.searchv2.SearchListener
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide

class SearchResultsViewHolder(itemView: View, val context: Context) :
        RecyclerView.ViewHolder(itemView) {

    internal fun bind(item: ExploreObject?, listener: SearchListener?, color: String) {
        if (item != null) {
            when (item.template) {
                SectionTemplate.Header -> {
                    itemView.findViewById<TextView>(R.id.tvSectionTitle).text = item.shortTitle
                    itemView.findViewById<View>(R.id.toRemoveOnFirst)?.isVisible = item.first.not()
                }
                SectionTemplate.Footer -> {
                    itemView.setOnClickListener { listener?.onShowAllClicked(item.id) }
                }
                SectionTemplate.EmptySpace -> {
                    itemView.findViewById<View>(R.id.toRemoveEmptyOnFirst)?.isVisible = item.first.not()
                }
                SectionTemplate.EmptySpaceEnd -> {

                }
                else -> {
                    itemView.setOnClickListener { listener?.onItemClicked(item.link) }
                    itemView.findViewById<TextView>(R.id.tvTitle).text = item.shortTitle
                    if (color.isNotEmpty()) {
                        itemView.findViewById<TextView>(R.id.tvTitle).setTextColor(
                                ColorUtils.parse(color)
                        )
                    }
                    itemView.findViewById<TextView>(R.id.tvShortName).text = item.shortDescription
                    itemView.findViewById<TextView>(R.id.tvDescription).text = item.customFields
                    item.media?.url.let {
                        Glide.with(context).load(item.media?.url)
                                .into(itemView.findViewById(R.id.ivPhoto))
                    }
                    itemView.findViewById<View>(R.id.viewSeparator)?.let {
                        it.isVisible = item.last.not()
                    }


                }
            }
        }
    }
}
