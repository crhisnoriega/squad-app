package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.shareItem
import io.branch.indexing.BranchUniversalObject
import kotlinx.android.synthetic.main.view_social_share.view.*

class SocialShareViewHolder<T : ViewModel>(
    itemView: View,
    private val viewModel: T,
    private val onShareListener: Listener<T>? = null
) : ViewHolder<ShareViewModel>(itemView) {

    override fun bind(viewModel: ShareViewModel) {
        with(viewModel) {
            itemView.apply {
                fun hideLoading() {
                    llShareContainer.isEnabled = true
                    ivShare.visibility = View.VISIBLE
                    pbShareLoading.visibility = View.GONE
                }

                fun showLoading() {
                    llShareContainer.isEnabled = false
                    ivShare.visibility = View.GONE
                    pbShareLoading.visibility = View.VISIBLE
                }

                if (!canShare) {
                    llShareContainer.visibility = View.GONE
                } else {
                    llShareContainer.visibility = View.VISIBLE
                    llShareContainer.setOnClickListener {
                        onShareListener?.onClick(this@SocialShareViewHolder.viewModel)
                        BranchUniversalObject().shareItem(this@with, model, context, { showLoading() }, { hideLoading() })
                    }
                }
            }
        }
    }

    override fun recycle() {}
}
