package co.socialsquad.squad.presentation.feature.signup.credentials

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.UserCreateRequest
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.TextUtils
import io.reactivex.disposables.CompositeDisposable
import java.net.UnknownHostException
import javax.inject.Inject

class SignupCredentialsPresenter @Inject constructor(
    private val signupCredentialsView: SignupCredentialsView,
    private val signupRepository: SignupRepository,
    private val analytics: Analytics
) {
    private val compositeDisposable = CompositeDisposable()

    fun onNextClicked(email: String) {
        analytics.sendEvent(Analytics.SIGNUP_TAP_NEXT)

        if (TextUtils.isValidEmail(email)) {

            val userCreateRequest = UserCreateRequest(email)
            compositeDisposable.add(
                signupRepository.createUser(userCreateRequest)
                    .doOnSubscribe { signupCredentialsView.showLoading() }
                    .doOnTerminate { signupCredentialsView.hideLoading() }
                    .subscribe({
                        it.status?.apply {
                            if (this.toLowerCase() == "ok") {
                                signupCredentialsView.navigateToSuccessView()
                            }
                        }
                    }) {
                        if (it is UnknownHostException) {
                            analytics.sendEvent(Analytics.SIGNUP_ACTION_NO_CONNECTION)
                        }

                        it.printStackTrace()
                        signupCredentialsView.showMessage(R.string.signup_credentials_error_failed_to_create_user)
                    }
            )
        } else {
            analytics.sendEvent(Analytics.SIGNUP_ACTION_INVALID_EMAIL)
            signupCredentialsView.setErrorMessage(R.string.signup_credentials_error_invalid_email)
        }
    }

    fun onCreate() {
        analytics.sendEvent(Analytics.SIGNUP_SCREEN_VIEW)
    }

    fun onDestroy() = compositeDisposable.dispose()
}
