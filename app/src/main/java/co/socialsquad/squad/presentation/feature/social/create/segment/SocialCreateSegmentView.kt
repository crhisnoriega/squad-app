package co.socialsquad.squad.presentation.feature.social.create.segment

import co.socialsquad.squad.data.entity.AudienceType

interface SocialCreateSegmentView {
    fun finishAfterDownlineClicked(networkCount: Int)
    fun showOptions(types: List<AudienceType>)
    fun showLoading()
    fun hideLoading()
}
