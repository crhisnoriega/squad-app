package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EmptyState(
    @SerializedName("icon") val icon: SimpleMedia,
    @SerializedName("title", alternate = ["text"]) val title: String,
    @SerializedName("subtitle", alternate = ["button_text"]) val subtitle: String
) : Parcelable
