package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.animation.DecelerateInterpolator
import android.widget.ProgressBar

private const val ANIMATION_DURATION_MS = 400
private const val MULTIPLIER_FACTOR = 100

class AnimatingProgressBar(context: Context, attrs: AttributeSet) : ProgressBar(context, attrs) {

    private var _progress = 0

    @Synchronized
    override fun setMax(max: Int) {
        super.setMax(max * MULTIPLIER_FACTOR)
    }

    @Synchronized
    fun incrementBy(diff: Int) {
        _progress += diff * MULTIPLIER_FACTOR
        val animation = ObjectAnimator.ofInt(this, "progress", _progress)
        animation.duration = ANIMATION_DURATION_MS.toLong()
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }
}
