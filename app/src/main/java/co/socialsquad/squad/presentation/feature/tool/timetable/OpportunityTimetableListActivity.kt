package co.socialsquad.squad.presentation.feature.tool.timetable

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunityTimetableViewHolder
import co.socialsquad.squad.presentation.custom.tool.viewHolder.OpportunityTimetableViewHolderModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.RankingType
import co.socialsquad.squad.presentation.feature.opportunityv2.OpportunityDetailsActivity
import co.socialsquad.squad.presentation.feature.tool.di.ToolModule
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_opportunity_timetable_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

private const val EXTRA_OPPORTUNITY = "extra_opportunity"

class OpportunityTimetableListActivity : BaseActivity() {

    companion object {
        fun start(context: Context, opportunity: Opportunity<Timetable>) {
            context.startActivity(
                    Intent(context, OpportunityTimetableListActivity::class.java).apply {
                        putExtra(EXTRA_OPPORTUNITY, opportunity)
                    }
            )
        }
    }

    private val opportunity by extra<Opportunity<Timetable>>(EXTRA_OPPORTUNITY)
    private val viewModel by viewModel<OpportunityTimetableViewModel>()
    private val companyColor by inject<CompanyColor>()
    private val defaultEmptyState by lazy {
        EmptyState(
                SimpleMedia(""),
                getString(R.string.opportunity_empty_state_msg),
                getString(R.string.opportunity_empty_state_button)
        )
    }

    override val modules: List<Module> = listOf(ToolModule.instance)
    override val contentView = R.layout.activity_opportunity_timetable_list

    private val adapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is OpportunityTimetableViewHolderModel -> OpportunityTimetableViewHolder.ITEM_VIEW_MODEL_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            OpportunityTimetableViewHolder.ITEM_VIEW_MODEL_ID -> OpportunityTimetableViewHolder.newInstance(
                    view as ViewGroup, companyColor
            ).apply {
                onSubmissionClickListener = {
                    OpportunityDetailsActivity.start(RankingType.Escala, "PDV", it.id.toString(), this@OpportunityTimetableListActivity)
                }
            }
            else -> throw IllegalArgumentException()
        }
    })
    // private val adapterLoading =
    //     LoadingAdapter(R.layout.item_tool_lead_status_loading, ITEM_LOADING_COUNT)

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ibBack.setOnClickListener {
            finish()
        }

        tvTitle.text = opportunity.header.title
        tvSubtitle.text = opportunity.header.subtitle

        setupViewModelObservers()
        setupRecyclerView()
        setupAdapterInitialData()
        var urlLink = opportunity.button?.link!!

        if (urlLink.contains("?")) {
            urlLink = urlLink.split("?")[0]
        }

        viewModel.fetchOpportunities(urlLink)
    }

    @ExperimentalCoroutinesApi
    private fun setupViewModelObservers() {
        createFlow(viewModel.opportunity)
                .loading { adapter.startLoading() }
                .error {
                    rvSubmissions.isVisible = false
                    showEmptyState(opportunity.emptyState ?: defaultEmptyState)
                    showErrorMessage(it.orEmpty())
                }
                .success { opportunity ->
                    adapter.stopLoading()
                    opportunity?.items?.let { submissions ->
                        adapter.setItems(
                                submissions.map { submission ->
                                    OpportunityTimetableViewHolderModel(submission, companyColor)
                                }.toList()
                        )
                    }
                }
                .launch()
    }

    private fun hideEmptyState() {
        group_empty.isVisible = false
    }

    private fun showEmptyState(emptyState: EmptyState) {
        Glide.with(this)
                .load(emptyState.icon.url)
                .crossFade()
                .into(img_lead_empty)

        lbl_empty_lead_message.text = emptyState.title
        btn_empty_action.text = emptyState.subtitle
        group_empty.isVisible = true
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG
        ).show()
    }

    private fun setupAdapterInitialData() {
        val submissions = opportunity.items?.map { submission ->
            OpportunityTimetableViewHolderModel(submission, companyColor)
        }?.toList()
        submissions?.let {
            adapter.setItems(it)
            rvSubmissions.isVisible = true
        } ?: run {
            showEmptyState(opportunity.emptyState ?: defaultEmptyState)
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvSubmissions.apply {
            this.layoutManager = layoutManager
            this.adapter = this@OpportunityTimetableListActivity.adapter
//            addOnScrollListener(
//                    EndlessScrollListener(
//                            2,
//                            layoutManager,
//                            viewModel::getNextPage
//                    )
//            )
        }
    }
}
