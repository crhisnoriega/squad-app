package co.socialsquad.squad.presentation.feature.audio.detail

import android.content.Intent
import android.text.format.DateFormat
import android.text.format.DateUtils
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.OnAudioPlayedListener
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialLikesViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialLikesViewModel
import co.socialsquad.squad.presentation.feature.social.viewholder.SocialShareViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.toDate
import kotlinx.android.synthetic.main.view_audio_detail.view.*
import java.util.ArrayList

class AudioDetailViewHolder(
    itemView: View,
    private val onDeleteListener: Listener<AudioViewModel>,
    private val onLikeListener: Listener<AudioViewModel>,
    private val onLikeCountListener: Listener<AudioViewModel>,
    private val onPlayedListener: OnAudioPlayedListener,
    private val onShareListener: Listener<AudioViewModel>? = null
) : ViewHolder<AudioViewModel>(itemView) {

    private var likesViewHolder: SocialLikesViewHolder<AudioViewModel>? = null

    override fun bind(viewModel: AudioViewModel) {
        with(viewModel) viewModel@{
            itemView.apply {
                tvAuthor.text = author
                tvAuthor.setOnClickListener {
                    openUserActivity(viewModel.authorPk)
                }
                tvQualification.text = qualification
                tvTitle.text = title

                if (!description.isNullOrBlank()) {
                    tvDescription.text = description
                    tvDescription.post {
                        if (tvDescription.layout?.lineCount ?: 0 > 3) {
                            bMore.visibility = View.VISIBLE
                            bMore.setOnClickListener {
                                tvDescription.maxLines = Int.MAX_VALUE
                                bMore.visibility = View.GONE
                            }
                        }
                    }
                } else {
                    tvDescription.visibility = View.GONE
                }

                setupDate(this@viewModel)

                tvAudience.text = context.getString(AudienceViewModel.getStringResId(audienceType))
                ivAudience.setImageResource(AudienceViewModel.getDrawableResId(audienceType))

                setupOptions(this@viewModel)

                val showInteractionLogic = { llInteractionsText.visibility = if (playbackCount > 0 || likeCount > 0) View.VISIBLE else View.GONE }

                val socialLikesViewModel = SocialLikesViewModel(currentUserAvatar, liked, likeAvatars, likeCount)

                val holderOnLikeListener = object : ViewHolder.Listener<AudioViewModel> {
                    override fun onClick(viewModel: AudioViewModel) {
                        viewModel.liked = socialLikesViewModel.liked
                        viewModel.likeCount = socialLikesViewModel.likeCount
                        viewModel.likeAvatars = socialLikesViewModel.likeAvatars
                        showInteractionLogic()
                        onLikeListener.onClick(viewModel)
                    }
                }

                likesViewHolder = SocialLikesViewHolder(itemView, this@viewModel, holderOnLikeListener, onLikeCountListener).apply {
                    bind(socialLikesViewModel)
                }

                SocialShareViewHolder(itemView, this@viewModel, onShareListener)
                    .bind(shareViewModel)

                setupPlaybackCount(playbackCount)

                tvPlaybackCount.setOnClickListener { openUserListActivity(this@viewModel) }
                showInteractionLogic()

                apAudio.init(
                    viewModel, true,
                    object : OnAudioPlayedListener {
                        override fun onPlayed(pk: Int) {
                            playbackCount++
                            setupPlaybackCount(playbackCount)
                            onPlayedListener.onPlayed(pk)
                        }
                    }
                )
            }
        }
    }

    private fun openUserActivity(creatorPK: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, creatorPK)
        }
        itemView.context.startActivity(intent)
    }

    private fun openUserListActivity(viewModel: AudioViewModel) {
        val intent = Intent(itemView.context, UserListActivity::class.java).apply {
            putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
            putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.AUDIO_PLAYBACK)
        }
        itemView.context.startActivity(intent)
    }

    private fun setupPlaybackCount(totalListenings: Long) {
        itemView.tvPlaybackCount.apply {
            if (totalListenings > 0) {
                text = context.getString(R.string.audio_playbacks, TextUtils.toUnitSuffix(totalListenings))
                visibility = View.VISIBLE
            } else {
                visibility = View.GONE
            }
        }
    }

    private fun setupDate(viewModel: AudioViewModel) {
        with(viewModel) {
            itemView.apply {
                val scheduledDate = scheduledDate?.toDate()
                if (scheduledDate != null) {
                    val date = DateUtils.formatDateTime(context, scheduledDate.time, DateUtils.FORMAT_NO_YEAR)
                    val time = DateFormat.getTimeFormat(context).format(scheduledDate)
                    tvDate.text = context.getString(R.string.audio_date_time, date, time)
                } else {
                    tvDate.text = date.toDate()?.elapsedTime(context)?.capitalize() ?: ""
                }
            }
        }
    }

    private fun setupOptions(viewModel: AudioViewModel) {
        val options = object : ArrayList<Int>() {
            init {
//                if (viewModel.canEdit) add(R.string.audio_option_edit)
                if (viewModel.canDelete) add(R.string.audio_option_delete)
            }
        }

        val socialSpinnerAdapter = SocialSpinnerAdapter(
            itemView.context,
            R.layout.textview_spinner_dropdown_options_item,
            R.layout.textview_spinner_dropdown_options,
            options
        )

        itemView.sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
        itemView.sOptions.adapter = socialSpinnerAdapter

        itemView.sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position) ?: return

                val itemResId = item as Int
                when (itemResId) {
//                    R.string.audio_option_edit -> onEditListener.onClick(viewModel)
                    R.string.audio_option_delete -> onDeleteListener.onClick(viewModel)
                }
                itemView.sOptions.setSelection(0)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun recycle() {
        itemView.apAudio.recycle()
        likesViewHolder?.recycle()
    }
}
