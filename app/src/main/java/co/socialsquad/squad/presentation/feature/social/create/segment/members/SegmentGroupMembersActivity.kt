package co.socialsquad.squad.presentation.feature.social.create.segment.members

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.MEMBER_VIEW_ID
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_segment_group_members.*
import javax.inject.Inject

class SegmentGroupMembersActivity : BaseDaggerActivity(), SegmentGroupMembersView {

    companion object {
        const val RESULT_CODE_MEMBERS = 51
        const val EXTRA_MEMBERS = "EXTRA_UPLOAD_MEMBERS"
    }

    @Inject
    lateinit var segmentGroupMembersPresenter: SegmentGroupMembersPresenter

    private lateinit var rvMembers: RecyclerView
    private lateinit var tvDescription: TextView
    private lateinit var llFailedContainer: LinearLayout
    private var membersAdapter: RecyclerViewAdapter? = null
    private val glide by lazy { Glide.with(this) }

    private lateinit var miAdd: MenuItem
    private var loading: Boolean = false

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search_members, menu)
        miAdd = menu.findItem(R.id.segment_member_item_add)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.segment_member_item_add -> {
                segmentGroupMembersPresenter.selectedMembers()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        segmentGroupMembersPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segment_group_members)

        rvMembers = segment_group_members_rv_members
        tvDescription = segment_group_members_tv_description
        llFailedContainer = segment_group_members_ll_container
        setupToolbar()
        setupMemberList()
        var selectedMembers = intent.extras?.getSerializable(SegmentGroupMembersActivity.EXTRA_MEMBERS) as? Array<UserViewModel>
        segmentGroupMembersPresenter.onCreate(selectedMembers.orEmpty().toList())
    }

    fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = getString(R.string.social_create_segment_item_members)
    }

    fun setupMemberList() {
        membersAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is UserViewModel -> MEMBER_VIEW_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View) = when (viewType) {
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                MEMBER_VIEW_ID -> MemberViewHolder(view, glide, onMemberClicked)
                else -> throw IllegalArgumentException()
            }
        })
        rvMembers.apply {
            layoutManager = LinearLayoutManager(this@SegmentGroupMembersActivity)
            adapter = membersAdapter
            addOnScrollListener(
                EndlessScrollListener(
                    linearLayoutManager = rvMembers.layoutManager as LinearLayoutManager,
                    onScrolledBeyondVisibleThreshold = {
                        if (!loading) segmentGroupMembersPresenter.onScrolledBeyondVisibleThreshold()
                    }
                )
            )
        }
    }

    override fun addMembers(members: List<UserViewModel>) {
        membersAdapter?.addItems(members)
    }

    override fun setMembers(members: List<UserViewModel>) {
        membersAdapter?.setItems(members)
    }

    override fun startLoading() {
        if (!loading) {
            loading = true
            membersAdapter?.startLoading()
        }
    }

    override fun stopLoading() {
        if (loading) {
            loading = false
            runOnUiThread { membersAdapter?.stopLoading() }
        }
    }

    val onMemberClicked = object : ViewHolder.Listener<UserViewModel> {
        override fun onClick(viewModel: UserViewModel) {
            segmentGroupMembersPresenter.selectMember(viewModel)
        }
    }

    override fun showEmptyState(text: String) {
        tvDescription.text = getString(R.string.segment_members_failed_state_description, text)
        llFailedContainer.visibility = View.VISIBLE
    }

    override fun hideEmptyState() {
        llFailedContainer.visibility = View.GONE
    }

    override fun finishActivity(members: List<UserViewModel>) {
        val intent = Intent()
        intent.putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, members as ArrayList<UserViewModel>)
        setResult(RESULT_CODE_MEMBERS, intent)
        finish()
    }

    override fun setAddButtonEnabled(enabled: Boolean) {
        miAdd.isEnabled = enabled
    }
}
