package co.socialsquad.squad.presentation.feature.login.alternative

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.MagicLinkRequest
import co.socialsquad.squad.data.entity.PasswordResetRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailActivity.Companion.EXTRA_LOGIN_EMAIL
import co.socialsquad.squad.presentation.feature.login.password.LoginPasswordActivity
import co.socialsquad.squad.presentation.feature.login.subdomain.LoginSubdomainActivity.Companion.EXTRA_LOGIN_SUBDOMAIN
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.TextUtils
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import java.net.UnknownHostException
import javax.inject.Inject

class LoginAlternativePresenter @Inject constructor(
    private val loginRepository: LoginRepository,
    private val view: LoginAlternativeView,
    private val tagWorker: TagWorker,
    private val preferences: Preferences,
    private val analytics: Analytics
) {

    enum class Type : Serializable { RESET_PASSWORD, MAGIC_LINK, CREATE_ACCOUNT }

    private var type: Type? = null
    private var email: String? = null
    private var subdomain: String? = null

    private val compositeDisposable = CompositeDisposable()

    fun onCreate(intent: Intent) {

        email = intent.getStringExtra(EXTRA_LOGIN_EMAIL)
        subdomain = intent.getStringExtra(EXTRA_LOGIN_SUBDOMAIN)
        type = intent.getSerializableExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE) as? Type
        view.setHintText(R.string.login_alternative_hint)

        if (intent.hasExtra(LoginPasswordActivity.extraCompany)) {
            val gson = Gson()
            val company = gson.fromJson(intent.extras!!.getString(LoginPasswordActivity.extraCompany), Company::class.java)
            view.setupHeader(company)
        }

        when (type) {
            Type.RESET_PASSWORD -> {
                analytics.sendEvent(Analytics.RESET_PASSWORD_EMAIL_SCREEN_VIEW)
                view.setHelpText(R.string.login_alternative_help_reset, subdomain)
            }
            Type.MAGIC_LINK -> {
                analytics.sendEvent(Analytics.MAGIC_LINK_EMAIL_SCREEN_VIEW)
                view.setHelpText(R.string.login_alternative_help_link)
            }
        }

        clearData()
        email?.let { view.setFieldText(it) }
    }

    private fun clearData() {
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.subdomain = null
        loginRepository.clearSharingAppDialogTimer()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
        preferences.subdomain = null
    }

    fun onNextClicked(emailField: String) {
        email = emailField

        when (type) {
            Type.MAGIC_LINK -> {

                analytics.sendEvent(Analytics.MAGIC_LINK_EMAIL_TAP_NEXT)

                if (TextUtils.isValidEmail(email)) {
                    compositeDisposable.add(
                        loginRepository.magicLink(MagicLinkRequest(emailField))
                            .doOnSubscribe { view.startLoading() }
                            .doOnTerminate { view.stopLoading() }
                            .subscribe(
                                {
                                    type?.let { view.showSuccessScreen(it) }
                                },
                                {
                                    it.printStackTrace()
                                    showErrorMessage(it)

                                    if (it is UnknownHostException) {
                                        analytics.sendEvent(Analytics.MAGIC_LINK_EMAIL_ACTION_NO_CONNECTION)
                                    } else {
                                        analytics.sendEvent(Analytics.MAGIC_LINK_EMAIL_ACTION_NO_SQUAD)
                                    }
                                }
                            )
                    )
                } else {
                    analytics.sendEvent(Analytics.MAGIC_LINK_EMAIL_ACTION_INVALID_EMAIL)
                    view.setErrorMessage(R.string.signup_credentials_error_invalid_email)
                }
            }

            Type.RESET_PASSWORD -> {

                analytics.sendEvent(Analytics.RESET_PASSWORD_EMAIL_TAP_NEXT)

                if (TextUtils.isValidEmail(email)) {

                    subdomain?.apply {
                        compositeDisposable.add(
                            loginRepository.resetPassword(PasswordResetRequest(emailField), this)
                                .doOnSubscribe { view.startLoading() }
                                .doOnTerminate { view.stopLoading() }
                                .subscribe(
                                    {
                                        type?.let { view.showSuccessScreen(it) }
                                    },
                                    {
                                        it.printStackTrace()
                                        showErrorMessage(it)

                                        if (it is UnknownHostException) {
                                            analytics.sendEvent(Analytics.RESET_PASSWORD_EMAIL_ACTION_NO_CONNECTION)
                                        }
                                    }
                                )
                        )
                    }
                } else {
                    analytics.sendEvent(Analytics.RESET_PASSWORD_EMAIL_ACTION_INVALID_EMAIL)
                    view.setErrorMessage(R.string.signup_credentials_error_invalid_email)
                }
            }
        }
    }

    private fun showErrorMessage(throwable: Throwable) {
        when (type) {
            Type.RESET_PASSWORD -> view.setErrorMessage(R.string.login_alternative_error_reset)
            Type.MAGIC_LINK -> view.setErrorMessage(R.string.login_alternative_error_link)
            else -> view.setErrorMessage(throwable.message ?: "")
        }
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }
}
