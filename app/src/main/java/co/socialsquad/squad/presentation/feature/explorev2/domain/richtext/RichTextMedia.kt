package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichTextMedia(
    var list: List<Media>? = null
) : ViewType, Parcelable {
    constructor(richtext: RichTextVO) : this() {
        this.list = richtext.media
    }

    override fun getViewType(): Int {
        return RichTextDataType.MEDIA.value
    }
}
