package co.socialsquad.squad.presentation.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.TypedValue
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

fun <T> RequestBuilder<T>.roundedCorners(context: Context, radius: Int = 4): RequestBuilder<T> {
    val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, radius.toFloat(), context.resources.displayMetrics)
    return apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(px.toInt())))
}

fun <T> RequestBuilder<T>.circleCrop(): RequestBuilder<T> {
    return apply(RequestOptions().circleCrop())
}

fun <T> RequestBuilder<T>.centerCrop(): RequestBuilder<T> {
    return apply(RequestOptions().centerCrop())
}

fun RequestBuilder<Drawable>.crossFade(): RequestBuilder<Drawable> {
    return transition(DrawableTransitionOptions.withCrossFade())
}

fun RequestBuilder<Bitmap>.bitmapCrossFade(): RequestBuilder<Bitmap> {
    return transition(BitmapTransitionOptions.withCrossFade())
}
