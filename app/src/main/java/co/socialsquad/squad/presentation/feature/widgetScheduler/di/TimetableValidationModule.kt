package co.socialsquad.squad.presentation.feature.widgetScheduler.di

import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.TimetableValidationRepository
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.TimetableValidationRepositoryImpl
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.remote.TimetableValidationAPI
import co.socialsquad.squad.presentation.feature.widgetScheduler.validation.TimetableValidationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object TimetableValidationModule {

    val repository = module {
        single {
            provideAPI(get())
        }

        single<TimetableValidationRepository> {
            TimetableValidationRepositoryImpl(get())
        }
    }

    val instance = listOf(
        module {
            viewModel { TimetableValidationViewModel(get()) }
        },
        repository
    )

    private fun provideAPI(retrofit: Retrofit): TimetableValidationAPI =
        retrofit.create(TimetableValidationAPI::class.java)
}
