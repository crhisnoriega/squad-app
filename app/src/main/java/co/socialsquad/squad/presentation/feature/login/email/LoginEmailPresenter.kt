package co.socialsquad.squad.presentation.feature.login.email

import android.content.Intent
import android.util.Log
import co.socialsquad.squad.data.entity.SubdomainsRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.TextUtils
import io.reactivex.disposables.CompositeDisposable
import java.net.UnknownHostException
import javax.inject.Inject

class LoginEmailPresenter @Inject constructor(
    private val view: LoginEmailView,
    private val tagWorker: TagWorker,
    private val loginRepository: LoginRepository,
    private val analytics: Analytics
) {
    private val compositeDisposable = CompositeDisposable()
    private var email: String = ""

    fun onCreate(intent: Intent) {
        analytics.sendEvent(Analytics.SIGN_EMAIL_SCREEN_VIEW)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onNextClicked(email: String) {
        analytics.sendEvent(Analytics.SIGN_EMAIL_TAP_NEXT)

        compositeDisposable.add(
            loginRepository.getSubdomains(SubdomainsRequest(email))
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            if (it.size == 1) {
                                view.moveToPasswordScreen(it.get(0), email)
                            } else {
                                view.openSubdomainsScreen(it, email)
                            }
                        } else {
                            analytics.sendEvent(Analytics.SIGN_EMAIL_ACTION_NO_SQUAD)
                            view.showEmptySubdomains()
                        }
                    },
                    {
                        Log.d("", it.message)

                        if (it is UnknownHostException) {
                            analytics.sendEvent(Analytics.SIGN_EMAIL_ACTION_NO_CONNECTION)
                        }
                    }
                )
        )
    }

    fun onInputChanged(email: String) {
        this.email = email.trim()

        val isValid = TextUtils.isValidEmail(email)
        view.enableNextButton(isValid)
    }
}
