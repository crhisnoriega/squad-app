package co.socialsquad.squad.presentation.custom.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_separator_top_bottom.view.*

class ItemSeparatorViewHolder(var itemView:View): ViewHolder<ItemSeparatorViewModel>(itemView) {
    override fun bind(viewModel: ItemSeparatorViewModel) {
        itemView.top_line.isVisible = viewModel.top!!
        itemView.bottom_line.isVisible = viewModel.bottom!!
    }

    override fun recycle() {

    }
}