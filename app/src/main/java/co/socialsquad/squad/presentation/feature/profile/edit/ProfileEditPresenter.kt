package co.socialsquad.squad.presentation.feature.profile.edit

import android.net.Uri
import co.socialsquad.squad.data.entity.Phone
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_PROFILE_EDIT
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.*
import co.socialsquad.squad.presentation.util.DateTimeUtils
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.TextUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import java.util.*
import javax.inject.Inject

class ProfileEditPresenter @Inject constructor(
        private val profileEditView: ProfileEditView,
        private val profileRepository: ProfileRepository,
        private val loginRepository: LoginRepository,
        private val tagWorker: TagWorker
) {
    private val compositeDisposable = CompositeDisposable()

    var userCompany = loginRepository.userCompany

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_PROFILE_EDIT)
        loginRepository.userCompany?.let {
            profileEditView.setupInfo(it.user, it.bio)
        }
    }

    fun onResourceReady(avatarUri: Uri) {
        profileRepository.avatar = avatarUri
    }

    fun onInput(
            firstName: String?,
            lastName: String?,
            birthday: Calendar?,
            document: String?,
            cpf: String?,
            gender: Gender?,
            maritalStatus: MaritalStatus?,
            email: String?,
            mobile: String?,
            phone: String?,
            zipcode: String?,
            street: String?,
            number: String?,
            country: Country?,
            city: City?,
            state: State?
    ) {
        val isMobileValid = mobile?.let { it.length >= 14 } ?: false
        val isPhoneValid = phone?.let { it.isBlank() || it.length == 14 } ?: false
        val isCPFValid = cpf?.let { it.isBlank() || TextUtils.isValidCPF(MaskUtils.unmask(it, MaskUtils.Mask.CPF)) }
                ?: false

        val conditionsMet = (
                !firstName.isNullOrBlank() &&
                        !lastName.isNullOrBlank() &&
                        birthday != null && DateTimeUtils.beforeToday(birthday) &&
                        !document.isNullOrBlank() &&
                        isCPFValid &&
                        gender != null &&
                        maritalStatus != null &&
                        TextUtils.isValidEmail(email) &&
                        isMobileValid &&
                        isPhoneValid &&
                        zipcode?.length == 9 &&
                        !street.isNullOrBlank() &&
                        !number.isNullOrBlank() &&
                        country != null &&
                        state != null &&
                        city != null
                )

        profileEditView.setSaveButtonEnabled(conditionsMet)
    }

    fun onStateClicked() {
        compositeDisposable.add(
                profileRepository.getStates()
                        .doOnSubscribe { profileEditView.showLoading() }
                        .doOnTerminate { profileEditView.hideLoading() }
                        .subscribe(
                                { states ->
                                    val spinnerStates = ArrayList<State>()
                                    states.states?.forEach { state ->
                                        val spinnerState = State(state.code, state.name, state.sigla)
                                        spinnerStates.add(spinnerState)
                                    }
                                    profileEditView.showStates(spinnerStates)
                                },
                                {
                                    it.printStackTrace()
                                    profileEditView.showErrorMessageFailedToGetStates()
                                }
                        )
        )
    }

    fun onCityClicked(stateCode: Long) {
        compositeDisposable.add(
                profileRepository.getCities(stateCode)
                        .doOnSubscribe { profileEditView.showLoading() }
                        .doOnTerminate { profileEditView.hideLoading() }
                        .subscribe(
                                { cities ->
                                    val spinnerCities = ArrayList<City>()
                                    cities.cities?.forEach { city ->
                                        val spinnerCity = City(city.code, city.name)
                                        spinnerCities.add(spinnerCity)
                                    }
                                    profileEditView.showCities(spinnerCities)
                                },
                                {
                                    it.printStackTrace()
                                    profileEditView.showErrorMessageFailedToGetCities()
                                }
                        )
        )
    }

    fun onNeighborhoodClicked(cityCode: Long) {
        compositeDisposable.add(
                profileRepository.getNeighborhoods(cityCode)
                        .doOnSubscribe { profileEditView.showLoading() }
                        .doOnTerminate { profileEditView.hideLoading() }
                        .subscribe(
                                { neighborhoods ->
                                    val spinnerNeighborhoods = ArrayList<Neighborhood>()
                                    neighborhoods.neighborhoods?.forEach {
                                        val spinnerNeighborhood = Neighborhood(it.code, it.name)
                                        spinnerNeighborhoods.add(spinnerNeighborhood)
                                    }

                                    profileEditView.showNeighborhoods(spinnerNeighborhoods)
                                },
                                {
                                    it.printStackTrace()
                                    profileEditView.showErrorMessageFailedToGetNeighborhoods()
                                }
                        )
        )
    }

    fun onSaveClicked(
            firstName: String? = null,
            lastName: String? = null,
            birthday: Calendar? = null,
            document: String? = null,
            cpf: String? = null,
            genderObj: Gender? = null,
            maritalStatusObj: MaritalStatus? = null,
            email: String? = null,
            mobile: String? = null,
            phone: String? = null,
            zipcode: String? = null,
            street: String? = null,
            number: String? = null,
            complement: String? = null,
            countryObj: Country? = null,
            cityObj: City? = null,
            stateObj: State? = null,
            neighborhoodObj: Neighborhood? = null,
            bio: String? = null
    ) {
        val userCompany = loginRepository.userCompany
        val user = userCompany?.user
        user?.firstName = firstName
        user?.lastName = lastName
        user?.birthday = birthday?.let { DateTimeUtils.format(it) }
        user?.rg = document
        user?.cpf = cpf?.let { MaskUtils.unmask(it, MaskUtils.Mask.CPF) }

        genderObj?.let {
            user?.gender = it.name
        }

        maritalStatusObj?.let {
            user?.maritalStatus = it.toString()
        }

        user?.contactEmail = email

        val phones = ArrayList<Phone>()
        phones.add(Phone(MaskUtils.unmask(mobile, MaskUtils.Mask.MOBILE), "M"))
        phone?.let { phones.add(Phone(MaskUtils.unmask(it, MaskUtils.Mask.PHONE), "P")) }
        user?.phones = phones

        user?.zipcode = MaskUtils.unmask(zipcode, MaskUtils.Mask.CEP)
        user?.street = street
        user?.number = number
        user?.complement = complement

        countryObj?.let {
            user?.country = it.code
        }

        cityObj?.let {
            user?.city = co.socialsquad.squad.data.entity.City(it.code, it.name)
        }

        stateObj?.let { state ->
            user?.state = co.socialsquad.squad.data.entity.State(state?.code
                    ?: -1, state?.name, state?.sigla)
        }

        neighborhoodObj?.let {
            user?.neighborhood = co.socialsquad.squad.data.entity.Neighborhood(it.code, it.name)
        }

        userCompany?.user = user
        userCompany?.bio = bio
        loginRepository.userCompany = userCompany

        val editUserObservable = profileRepository.editUser(userCompany!!.pk, userCompany)

        val getUserCompanyConsumer = Consumer<UserCompany> {
            loginRepository.userCompany = it
            profileEditView.finishAfterSuccess()
            profileRepository.avatar = null
        }

        val throwableConsumer = Consumer<Throwable> {
            it.printStackTrace()
            profileEditView.showErrorMessageFailedToRegisterUser()
        }

        val avatarUri = profileRepository.avatar

        if (avatarUri != null) {
            compositeDisposable.add(
                    profileRepository.uploadAvatar(userCompany.user.pk, avatarUri)
                            .doOnSubscribe { profileEditView.showLoading() }
                            .doOnTerminate { profileEditView.hideLoading() }
                            .andThen(editUserObservable)
                            .subscribe(getUserCompanyConsumer, throwableConsumer)
            )
        } else {
            compositeDisposable.add(
                    editUserObservable
                            .doOnSubscribe { profileEditView.showLoading() }
                            .doOnTerminate { profileEditView.hideLoading() }
                            .subscribe(getUserCompanyConsumer, throwableConsumer)
            )
        }

    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
