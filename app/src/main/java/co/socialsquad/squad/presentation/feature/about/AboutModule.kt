package co.socialsquad.squad.presentation.feature.about

import dagger.Binds
import dagger.Module

@Module
abstract class AboutModule {
    @Binds
    abstract fun view(callRoomActivity: AboutActivity): AboutView
}
