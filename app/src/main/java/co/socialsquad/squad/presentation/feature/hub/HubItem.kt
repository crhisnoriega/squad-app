package co.socialsquad.squad.presentation.feature.hub

import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.hub.dashboard.DashboardFragment
import co.socialsquad.squad.presentation.feature.hub.intranet.IntranetFragment

enum class HubItem(
    val titleResId: Int,
    val fragment: Fragment
) {
    Dashboard(R.string.hub_item_dashboard, DashboardFragment()),
    Intranet(R.string.hub_item_intranet, IntranetFragment());

    companion object {
        fun fragments() = values().map { it.fragment }.toTypedArray()
    }
}
