package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import android.widget.TextView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import kotlinx.android.synthetic.main.view_list_header.view.*

class HeaderViewHolder(itemView: View) : ViewHolder<HeaderViewModel>(itemView) {
    private val tvTitle: TextView = itemView.list_header_tv_title
    private val tvSubtitle: TextView = itemView.list_header_tv_subtitle

    override fun bind(viewModel: HeaderViewModel) {
        with(viewModel) {
            tvTitle.text = title ?: titleResId?.let { itemView.context.getString(it) }

            if (subtitle != null || subtitleResId != null) {
                tvSubtitle.text = subtitle ?: subtitleResId?.let { itemView.context.getString(it) }
                tvSubtitle.visibility = View.VISIBLE
            }
        }
    }

    override fun recycle() {
        tvSubtitle.visibility = View.GONE
    }
}
