package co.socialsquad.squad.presentation.feature.opportunity

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.Form
import co.socialsquad.squad.data.entity.opportunity.FormFieldValue
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmission
import co.socialsquad.squad.data.entity.opportunity.OpportunitySubmissionResponse
import co.socialsquad.squad.data.repository.OpportunityRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OpportunityPresenter @Inject constructor(
    val view: OpportunityView,
    private val opportunityRepository: OpportunityRepository
) {

    private var formValues: MutableSet<FormFieldValue> = mutableSetOf()
    val compositeDisposable = CompositeDisposable()

    fun submitOportunityForm(opportunityId: Int?, opportunityType: String?) {

        val opportunitySubmission = OpportunitySubmission(
            objectType = opportunityType,
            objectId = opportunityId,
            fieldValues = formValues.toList()
        )
        view.showLoading()

        compositeDisposable.add(
            opportunityRepository.createOpportunity(opportunitySubmission)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view.showLoading() }
                .subscribe({
                    view.hideLoading()
                    view.onRecommendationSuccess(it)
                }) {
                    it.printStackTrace()
                    view.hideLoading()
                    view.showMessage(R.string.recommendation_error_send)
                }
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun setupForm(form: Form, title: String?) {
        view.setupHeader(form.sections[0].header, title)
        view.setupFields(form.sections[0].fields)
    }

    fun onNextClicked(currentItem: Int, count: Int) {

        when {
            count == currentItem -> {
                view.onFormCompleted()
            }
            count - 1 == currentItem -> {
                view.setNextButtonText(R.string.recommendation_button_send)
                view.onNextPage(currentItem)
            }
            else -> {
                view.onNextPage(currentItem)
                view.setNextButtonText(R.string.next)
            }
        }
    }

    fun onBackClicked(currentItem: Int, count: Int) {
        if (count != currentItem) {
            view.setNextButtonText(R.string.next)
        }
        view.onBackPage(currentItem - 1)
    }

    fun addOpportunityFieldValue(value: FormFieldValue) {
        formValues.add(value)
    }

    fun getSubmissionDetails(oppotunitySubmission: OpportunitySubmissionResponse) {
        compositeDisposable.add(
            opportunityRepository.opportunitySubmissionDetails(
                oppotunitySubmission.opportunityId,
                oppotunitySubmission.objectId,
                oppotunitySubmission.submissionId,
                OpportunityObjectType.REAL_STATE
            )
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                    {
                        view.hideLoading()
                        view.onOpportunityDetailsClick(it.submissionDetails)
                    },
                    { error ->
                        error.printStackTrace()
                        view.hideLoading()
                    }
                )
        )
    }
}
