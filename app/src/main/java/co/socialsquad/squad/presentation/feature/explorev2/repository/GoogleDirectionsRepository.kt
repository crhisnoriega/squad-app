package co.socialsquad.squad.presentation.feature.explorev2.repository

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionList
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface GoogleDirectionsRepository {
    fun getRoutes(): Flow<JsonObject>

}

class GoogleDirectionsRepositoryImpl() : GoogleDirectionsRepository {
    override fun getRoutes(): Flow<JsonObject> {
        return flow {

        }
    }

}