package co.socialsquad.squad.presentation.feature.existinglead.adapter.searching

import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate

class LeadSearchingViewHolder(val parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_lead_searching)) {

    private val tvDescription: TextView = itemView.findViewById(R.id.tvDescription)

    fun bind(searchTerm: String) {
        tvDescription.text = searchTerm
    }
}
