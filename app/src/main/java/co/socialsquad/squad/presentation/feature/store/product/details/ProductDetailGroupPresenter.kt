package co.socialsquad.squad.presentation.feature.store.product.details

import android.content.Intent
import co.socialsquad.squad.presentation.feature.store.EXTRA_DETAIL_GROUP
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupViewModel
import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import javax.inject.Inject

class ProductDetailGroupPresenter @Inject constructor(
    private val productDetailGroupView: ProductDetailGroupView,
    storeInteractor: StoreInteractor
) {
    private val color = storeInteractor.getColor()

    fun onCreate(intent: Intent) {
        val detailGroup = intent.getSerializableExtra(EXTRA_DETAIL_GROUP) as ProductDetailGroupViewModel

        productDetailGroupView.setupToolbar(detailGroup.title)

        val media = detailGroup.media
        if (media != null) {
            productDetailGroupView.showVideo(media.thumbnail!!, media.url)
        } else {
            productDetailGroupView.showImage(detailGroup.cover)
        }

        with(productDetailGroupView) {
            detailGroup.media?.let { showVideo(it.thumbnail!!, it.url) }
                ?: showImage(detailGroup.cover)
        }

        productDetailGroupView.setupList(detailGroup.details, color)
    }
}
