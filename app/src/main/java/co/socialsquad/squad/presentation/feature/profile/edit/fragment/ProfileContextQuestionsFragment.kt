package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemQuestionSingleViewHolder
import co.socialsquad.squad.presentation.feature.profile.edit.viewHolder.ItemQuestionSingleViewModel
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.fragment_edit_profile_context_questions.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3


class ProfileContextQuestionsFragment(
    var textHelp: String? = "",
    var questions: MutableList<String> = mutableListOf(),
    var fragments: List<Fragment>? = null,
    var isLast: Boolean? = false,
    var isInCompleteProfile: Boolean? = true,
    var isMultiple: Boolean? = false,
    var isEditable: Boolean? = false,
) : Fragment() {
    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    private var radioComponents = listOf<RadioButton>()

    private var layoutsComponents = listOf<ConstraintLayout>()

    var gender: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_context_questions, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureButtons()
        if (isEditable!!) configureEditText() else configureQuestionList()
        configureFragmentByPosition()

        viewModel.state.observe(requireActivity(), Observer {
            when (it.name) {
                textHelp -> {
                    (activity as? CompleteProfileActivity)?.next()
                }
            }
        })
        btnSalvar.isEnabled = false
    }

    private fun configureEditText() {
        text_answer.visibility = View.VISIBLE
        text_answer.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnSalvar.isEnabled = s?.toString().isNullOrBlank().not()
                s?.toString()?.let {
                    viewModel.answers[this@ProfileContextQuestionsFragment] = it
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
        text_answer.requestFocus()


    }

    override fun onResume() {
        super.onResume()
        if (isEditable!!) {
            showKeyboard(requireContext(), text_answer)
            btnSalvar.isEnabled = true
        }
    }

    fun showKeyboard(context: Context, view: View) {
        val imm: InputMethodManager? =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }


    private fun configureButtons() {
        if (isLast!!) {
            btnSalvar.setOnClickListener {
                btnSalvar.isEnabled = false
                showLoading()
                viewModel.saveQuestions(textHelp!!, fragments)
            }
        } else {
            btnSalvar.setOnClickListener {
                (activity as? CompleteProfileActivity)?.next()
            }
        }


        btnBack.setOnClickListener {
            (activity as? CompleteProfileActivity)?.previous()
            hideKeyboard(requireActivity())
        }

        btnSalvar.isEnabled = false
    }

    private fun configureQuestionList() {

        rvQuestions.visibility = View.VISIBLE
        var layout =
            if (isMultiple!!) R.layout.item_profile_question_context_multiple else R.layout.item_profile_question_context_single

        var elements = questions.mapIndexed { index, s ->
            ItemQuestionSingleViewModel(s, false, index == 0)
        }
        var factory = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ItemQuestionSingleViewModel -> layout
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    layout -> ItemQuestionSingleViewHolder(
                        view
                    ) { selected ->
                        if (isMultiple!!) {
                            selected.isSelected = selected.isSelected.not()
                        } else {
                            elements.forEach {
                                it.isSelected = false
                            }
                            selected.isSelected = true
                        }


                        rvQuestions.adapter?.notifyDataSetChanged()

                        btnSalvar.isEnabled = true

                        var ll = viewModel.answers[textHelp!!]
                        ll = if (ll.isNullOrBlank()) {
                            "${selected.question}"
                        } else {
                            "$ll, ${selected.question}"
                        }
                        viewModel.answers[this@ProfileContextQuestionsFragment] = ll
                    }

                    else -> throw IllegalArgumentException()
                }
            }
        })
        rvQuestions.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = factory
        }

        factory.addItems(elements)
    }

    fun configureFragmentByPosition() {


        if (isInCompleteProfile!!) {
            fragments?.forEachIndexed { index, fragment ->
                if (fragment == this) {
                    txtPostion.text = "${index + 1} de ${fragments?.size}"

                    if (index == (fragments?.size!! - 1)) {
                        btnSalvar.text = "Salvar"
                    } else {
                        btnSalvar.text = "Próximo"
                    }
                }
            }
            txtPostion.isVisible = true
            txtHelpText.text = textHelp
            btnSalvar.isEnabled = true
        }
    }


    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    private fun canSave(canSave: Boolean) {
        btnSalvar.isEnabled = true
    }

    private fun showLoading() {
        btnSalvar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }


    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}
