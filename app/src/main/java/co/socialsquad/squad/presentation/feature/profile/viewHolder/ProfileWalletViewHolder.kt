package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.ProfileWalletActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import kotlinx.android.synthetic.main.item_card_recognition_profile.view.*
import kotlinx.android.synthetic.main.item_card_waller_profile.view.*
import kotlinx.android.synthetic.main.item_card_waller_profile.view.info
import kotlinx.android.synthetic.main.item_card_waller_profile.view.rlContactCircle2
import kotlinx.android.synthetic.main.item_card_waller_profile.view.short_description
import kotlinx.android.synthetic.main.item_card_waller_profile.view.short_title

const val PROFILE_WALLET_VIEW_HOLDER_ID = R.layout.item_card_waller_profile

class ProfileWalletViewHolder(itemView: View) : ViewHolder<ProfileWalletViewModel>(itemView) {


    override fun recycle() {
    }

    override fun bind(viewModel: ProfileWalletViewModel) {
        itemView.short_title.text = viewModel.title
        itemView.short_description.text = viewModel.subtitle
        itemView.info.setOnClickListener {
            ProfileWalletActivity.start(itemView.context, "", "")
        }

        itemView.rlContactCircle2.backgroundTintList =
            ColorUtils.getCompanyColor(itemView.context)?.let { ColorUtils.stateListOf(it) }
    }
}
