package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.view.View
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder

class ContactHeaderViewHolder(
    itemView: View,
) : ViewHolder<ContactHeaderViewHolderModel>(itemView) {

    private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)

    companion object {
        const val VIEW_ITEM_POSITION_ID = R.layout.item_contact_header
    }

    override fun bind(viewModel: ContactHeaderViewHolderModel) {
        tvTitle.text = viewModel.title
    }

    override fun recycle() {
    }
}