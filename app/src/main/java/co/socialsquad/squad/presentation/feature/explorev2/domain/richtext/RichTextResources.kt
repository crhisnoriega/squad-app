package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
class RichTextResources(var title: String? = null, var content: List<RichTextResourcesDetails>? = null) : ViewType, Parcelable {
    constructor(richText: RichTextVO) : this() {
        this.title = richText.resource?.title
        this.content = richText.resource?.content
    }

    override fun getViewType(): Int {
        return RichTextDataType.RESOURCE.value
    }
}
