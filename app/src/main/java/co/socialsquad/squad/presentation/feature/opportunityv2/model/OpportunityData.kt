package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.Media
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class OpportunityData(
        @SerializedName("id") val id: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("details") val details: OpportunityDetails?,
        @SerializedName("stages") val stages: List<OpportunityStage>?,
        @SerializedName("media") val media: Media?,
) : Parcelable