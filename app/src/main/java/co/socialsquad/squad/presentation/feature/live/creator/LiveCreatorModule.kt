package co.socialsquad.squad.presentation.feature.live.creator

import dagger.Binds
import dagger.Module

@Module
abstract class LiveCreatorModule {
    @Binds
    abstract fun view(liveCreatorActivity: LiveCreatorActivity): LiveCreatorView
}
