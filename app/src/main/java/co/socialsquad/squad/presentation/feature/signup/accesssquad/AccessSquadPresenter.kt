package co.socialsquad.squad.presentation.feature.signup.accesssquad

import android.content.Intent
import android.util.Log
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.TokenVerifyRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.util.Analytics
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.disposables.CompositeDisposable
import retrofit2.adapter.rxjava2.HttpException
import javax.inject.Inject

class AccessSquadPresenter @Inject constructor(
        private val view: AccessSquadView,
        private val preferences: Preferences,
        private val loginRepository: LoginRepository,
        private val analytics: Analytics
) {

    private val compositeDisposable = CompositeDisposable()

    fun onCreate(intent: Intent) {
        sendAnalytics(intent)

        if (intent.hasExtra(AccessSquadActivity.extraCompanies)) {
            val gson = Gson()
            val listType = object : TypeToken<List<Company>>() {}.type
            val companies: MutableList<Company> = gson.fromJson(intent.extras!!.getString(AccessSquadActivity.extraCompanies), listType)

            setupCompanies(companies)
            view.hideLoading()
            return
        }

        if (intent.hasExtra(AccessSquadActivity.extraToken)) {
            loginRepository.subdomain = null
            compositeDisposable.add(
                    loginRepository.getRegisteredCompanies(intent.extras?.getString(AccessSquadActivity.extraToken)!!)
                            .doOnSubscribe {
                                view.showLoading()
                            }
                            .doOnTerminate { view.hideLoading() }
                            .subscribe(
                                    {
                                        setupCompanies(it)
                                        view.showLoading()
                                    },
                                    {
                                        Log.d("Error", it.message)
                                        view.showTryAgain()
                                    }
                            )
            )
        }
    }

    private fun sendAnalytics(intent: Intent) {
        when {
            intent.hasExtra(AccessSquadActivity.extraIsMagicLink) -> {
                analytics.sendEvent(Analytics.MAGIC_LINK_SQUADS_SCREEN_VIEW)
            }
            intent.hasExtra(AccessSquadActivity.extraIsLogin) -> {
                analytics.sendEvent(Analytics.SIGN_SQUADS_SCREEN_VIEW)
            }
            else -> {
                analytics.sendEvent(Analytics.SIGNUP_SQUADS_SCREEN_VIEW)
            }
        }
    }

    private fun setupCompanies(companies: List<Company>) {
        if (companies.isNotEmpty()) {
            val privates = companies.filter { it.isPublic == false }.toMutableList()
            if (privates.isNotEmpty()) {
                privates.add(0, Company(name = "Privados"))
                view.addPrivateCompanies(privates.toList())
            }

            val publics = companies.filter { it.isPublic == true }.toMutableList()
            if (publics.isNotEmpty()) {
                publics.add(0, Company(name = "Públicos"))
                view.addPublicCompanies(publics.toList())
            }

            view.showListContent()
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun setSubdomain(subdomain: String) {
        loginRepository.subdomain = subdomain
    }

    fun clearData() {
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.subdomain = null
        loginRepository.clearSharingAppDialogTimer()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
    }

    fun authVerifyToken(subdomain: String, loginToken: String) {
        compositeDisposable.add(

            loginRepository.authVerifyToken(TokenVerifyRequest(loginToken), null)
                .subscribe(
                    {
                        loginRepository.squadColor = it.company.primaryColor
                        loginRepository.userCompany = it
                        loginRepository.token = it.token
                        loginRepository.subdomain = subdomain
                        loginRepository.getDeviceToken()

                        view.openNavigation()
                    },
                    {
                    }
                )
        )
    }

    fun onSquadSelected(name: String, type: String, isLaunch: Boolean, isLogin: Boolean, isMagicLink: Boolean) {
        if (isMagicLink) {
            analytics.sendEvent(Analytics.MAGIC_LINK_SQUADS_TAP_LAUNCH) {
                param("Type", type)
                param("SquadName", name)
            }
        } else if (isLaunch) {
            analytics.sendEvent(Analytics.SIGNUP_SQUADS_TAP_LAUNCH) {
                param("Type", type)
                param("SquadName", name)
            }
        } else {

            if (isLogin) {
                analytics.sendEvent(Analytics.SIGN_SQUADS_TAP_NEXT) {
                    param("Type", type)
                    param("SquadName", name)
                }
            } else {
                analytics.sendEvent(Analytics.SIGNUP_SQUADS_TAP_NEXT) {
                    param("Type", type)
                    param("SquadName", name)
                }
            }
        }
    }
}
