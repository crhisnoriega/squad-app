package co.socialsquad.squad.presentation.feature.kickoff.inviteteam

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_invite_your_team.*
import javax.inject.Inject
import kotlin.reflect.KFunction0

class InviteYourTeamFragment(val openNavigationScreen: KFunction0<Unit>) : Fragment(), InviteYourTeamView {

    private lateinit var adapter: InviteYourTeamAdapter

    @Inject
    lateinit var presenter: InviteYourTeamPresenter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_invite_your_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = InviteYourTeamAdapter(requireActivity(), this::setupSendInvitesButton)

        setupRecyclerView()
        addMember.setOnClickListener {
            view.hideKeyboard()
            addMember()
        }

        sendInvites.setOnClickListener {
            adapter.hasInvalidEmail().takeIf { !it }?.apply {
                presenter.sendInvites(adapter.emails())
            }
        }
    }

    override fun showLoading() {
        sendInvites.isEnabled = false
    }

    override fun hideLoading() {
        sendInvites.isEnabled = true
    }

    override fun showErrorMessage() {
        activity?.apply {
            ViewUtils.showDialog(this, getString(R.string.invite_members_sent_error), DialogInterface.OnDismissListener { })
        }
    }

    private fun setupRecyclerView() {
        activity?.apply {
            recyclerView.isNestedScrollingEnabled = false
            val linearLayoutManager = LinearLayoutManager(this)
            recyclerView.layoutManager = linearLayoutManager
            recyclerView.adapter = adapter
            adapter.init()
        }
    }

    private fun addMember() {
        adapter.add()
        nestedScrollView.post {
            nestedScrollView.fullScroll(View.FOCUS_DOWN)
        }
    }

    private fun setupSendInvitesButton(boolean: Boolean) {
        sendInvites.isEnabled = boolean
    }

    override fun moveToNextScreen() {
        openNavigationScreen()
    }
}
