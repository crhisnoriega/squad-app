package co.socialsquad.squad.presentation.feature.opportunityv2.di

import co.socialsquad.squad.presentation.feature.tool.ToolViewModel
import co.socialsquad.squad.presentation.feature.tool.submission.OpportunitySubmissionListActivity
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object OpportunityV2Module {
    val instance = module {

    }
}
