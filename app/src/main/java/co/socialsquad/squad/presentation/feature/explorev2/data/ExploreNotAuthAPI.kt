package co.socialsquad.squad.presentation.feature.explorev2.data

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url

interface ExploreNotAuthAPI {
    @GET
    suspend fun getFileRichTextResource(@Url fileUrl: String): ResponseBody
}
