package co.socialsquad.squad.presentation.feature.resources

import android.os.Bundle
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.dialog_resource_download.*


class ResourceDownloadDialog : DialogFragment() {

    lateinit var resourceName: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_resource_download, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.let { window ->
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            window.decorView.setBackgroundResource(android.R.color.transparent)
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window.attributes.windowAnimations = R.style.DialogAnimationFade
        }
        dialog?.setCanceledOnTouchOutside(false)
        loading(true)
        cancel_button.setOnClickListener {
            dismiss()
        }

        if (this::resourceName.isInitialized) {
            tvResourceName.text = resourceName
        }
    }

    fun loading(isLoading: Boolean) {
        ivLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            val rotation = AnimationUtils.loadAnimation(context, R.anim.clockwise_rotation)
            rotation.repeatCount = Animation.INFINITE
            ivLoading.startAnimation(rotation)
        } else {
            ivLoading.clearAnimation()
        }
    }


}