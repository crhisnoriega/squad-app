package co.socialsquad.squad.presentation.feature.social.create.segment.members

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.util.TextUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.linearlayout_segment_group_members_item.view.*
import java.util.ArrayList
import kotlin.reflect.KFunction1

private const val VIEW_TYPE_LOADING = 0
private const val VIEW_TYPE_CARD = 1
private const val LIMIT_MEMBERS_ADDED = 50

class SegmentGroupMembersAdapter(
    private val context: Context,
    private val layoutInflater: LayoutInflater,
    private val isSelectableCard: Boolean,
    private val changeVisibilitySearchButton: KFunction1<Int, Unit>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    private val glide = Glide.with(context)
    private var members: MutableList<UserCompany?> = ArrayList()
    private var addedMembers: MutableList<UserCompany> = ArrayList()
    private var isSearchedDisabled = true

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        VIEW_TYPE_LOADING -> {
            val view = layoutInflater.inflate(R.layout.progressbar_loading, parent, false)
            LoadingViewHolder(view)
        }
        VIEW_TYPE_CARD -> {
            val view = layoutInflater.inflate(R.layout.linearlayout_segment_group_members_item, parent, false)
            CardViewHolder(view)
        }
        else -> throw IllegalArgumentException()
    }

    fun add(members: List<UserCompany>) {
        this.members.addAll(members)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return members.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (members[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_CARD
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CardViewHolder) {
            val fullName = "${TextUtils.toCamelcase(
                members[position]?.user?.firstName
            )} ${TextUtils.toCamelcase(
                members[position]?.user?.lastName
            )}"
            holder.tvMember.text = fullName

            val glideOptions = RequestOptions().placeholder(R.drawable.ic_placeholder_members)
            glide.load(members[position]?.user?.avatar)
                .apply(glideOptions)
                .circleCrop()
                .into(holder.ivMember)

            if (isSelectableCard) {
                members[position]?.apply { setupSelected(this, holder) }
                setupBackground(holder.adapterPosition, holder)
                holder.llContainerOuter.setOnClickListener(
                    onContainerClicked(holder.adapterPosition)
                )
            } else {
                holder.rbMember.visibility = View.GONE
                setupBackground(position, holder)
            }
        } else if (holder is LoadingViewHolder) {
            if (members.size == 1) {
                val rect = Rect()
                recyclerView.getGlobalVisibleRect(rect)
                holder.itemView.layoutParams.height = rect.height() - 2 *
                    context.resources.getDimensionPixelSize(R.dimen.default_margin)
            } else {
                holder.itemView.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            }
        }
    }

    private fun setupSelected(member: UserCompany, holder: CardViewHolder) {
        for (addedMember in addedMembers) {
            if (member.user.pk == addedMember.user.pk) {
                holder.rbMember.isChecked = true
                return
            }
        }
        holder.rbMember.isChecked = false
    }

    private fun onContainerClicked(position: Int): View.OnClickListener? {
        return View.OnClickListener {
            if (addedMembers.contains(members[position])) {
                removeMember(position)
                notifyItemChanged(position)
            } else if (addedMembers.size < LIMIT_MEMBERS_ADDED) {
                members[position]?.let {
                    addedMembers.add(it)
                    addedMembers.size.apply { if (this == 1) changeVisibilitySearchButton(this) }
                    orderAddedMembers()
                    notifyItemChanged(position)
                }
            } else {
                showAddedLimitError()
            }
        }
    }

    private fun removeMember(position: Int) {
        addedMembers.remove(members[position])
        addedMembers.size.apply { if (this == 0) changeVisibilitySearchButton(this) }
    }

    private fun orderAddedMembers() {
        addedMembers = addedMembers.sortedWith(compareBy({ it.user.firstName }))
            .toMutableList()
    }

    private fun setupBackground(position: Int, holder: CardViewHolder) {
        var backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item)
        var backgroundOuter = context.getDrawable(R.drawable.shape_card_list_item_outer)
        holder.llContainerOuter.setPadding(
            context.resources.getDimensionPixelSize(R.dimen.default_margin), 0,
            context.resources.getDimensionPixelSize(R.dimen.default_margin), 0
        )

        val lastItemPosition = members.size - 1
        when {
            members.size == 1 -> {
                backgroundOuter = context.getDrawable(R.drawable.shape_card_list_item_outer_rounded)
                holder.llContainerOuter.setPadding(
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin)
                )
            }
            position == 0 -> {
                backgroundInner = context.getDrawable(R.drawable.ripple_card_list_item_rounded_top)
                backgroundOuter =
                    context.getDrawable(R.drawable.shape_card_list_item_outer_rounded_top)
                holder.llContainerOuter.setPadding(
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin), 0
                )
            }
            position == lastItemPosition -> {
                backgroundInner =
                    context.getDrawable(R.drawable.ripple_card_list_item_rounded_bottom)
                backgroundOuter = context.getDrawable(
                    R.drawable.layerlist_card_list_item_outer_rounded_bottom
                )
                holder.llContainerOuter.setPadding(
                    context.resources.getDimensionPixelSize(R.dimen.default_margin), 0,
                    context.resources.getDimensionPixelSize(R.dimen.default_margin),
                    context.resources.getDimensionPixelSize(R.dimen.default_margin)
                )
            }
        }

        setLayoutMargin(holder.llContainerOuter, 0)
        holder.llContainerOuter.background = backgroundOuter
        holder.llContainerInner.background = backgroundInner
    }

    private fun setLayoutMargin(llContainerOuter: LinearLayout, margin: Int) {
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.bottomMargin = margin
        llContainerOuter.layoutParams = layoutParams
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        if (holder is CardViewHolder) glide.clear(holder.ivMember)
    }

    fun showLoading() {
        members.add(null)
        notifyItemRangeInserted(members.size, 1)
    }

    fun hideLoading() {
        val index = members.indexOf(null)

        if (index != -1) {
            members.removeAt(index)
            notifyItemRemoved(members.size)
        }
    }

    fun addedMembers(): MutableList<UserCompany> {
        return addedMembers
    }

    fun clear() {
        members.clear()
        notifyDataSetChanged()
    }

    fun addSearchedMembers(searchedMembers: List<UserCompany>) {
        clear()
        changeMemberList(searchedMembers)
    }

    fun restoreMemberList() {
        clear()
    }

    private fun changeMemberList(members: List<UserCompany>) {
        this.members.addAll(members)
        if (isSearchedDisabled) {
            this.members.addAll(0, addedMembers)
        }
        notifyDataSetChanged()
    }

    fun setSearchDisabled(disabled: Boolean) {
        isSearchedDisabled = disabled
    }

    private fun showAddedLimitError() {
        Toast.makeText(
            context, context.getString(R.string.segment_members_limit_added_members),
            Toast.LENGTH_LONG
        )
            .show()
    }

    internal inner class CardViewHolder(containerView: View) :
        RecyclerView.ViewHolder(containerView) {
        val ivMember: ImageView = itemView.segment_group_members_iv_member
        val tvMember: TextView = itemView.segment_group_members_tv_member
        val rbMember: CheckBox = itemView.segment_group_members_rb
        val llContainerOuter: LinearLayout = itemView.segment_group_members_ll_outer
        val llContainerInner: LinearLayout = itemView.segment_group_members_ll_inner
    }

    private inner class LoadingViewHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView)
}
