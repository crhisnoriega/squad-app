package co.socialsquad.squad.presentation.feature.store

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.navigation.ReselectedListener
import co.socialsquad.squad.presentation.feature.store.category.StoreCategoryActivity
import co.socialsquad.squad.presentation.feature.store.product.ProductActivity
import co.socialsquad.squad.presentation.feature.store.products.EXTRA_PRODUCTS_TYPE
import co.socialsquad.squad.presentation.feature.store.products.EXTRA_PRODUCTS_VIEW_MODEL
import co.socialsquad.squad.presentation.feature.store.products.ProductsActivity
import co.socialsquad.squad.presentation.feature.store.products.ProductsPresenter
import co.socialsquad.squad.presentation.feature.store.viewholder.BannerListViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.CategoryListViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.FooterViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductListViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.StoreProductListBannerViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.StoreProductViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_store.*
import javax.inject.Inject

class StoreFragment : Fragment(), StoreView, ReselectedListener {

    @Inject
    lateinit var storePresenter: StorePresenter

    private lateinit var srlLoading: SwipeRefreshLayout
    private lateinit var llLoading: LinearLayout

    private var rvProducts: RecyclerView? = null
    private var storeAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (!isVisibleToUser) rvProducts?.stopScroll()
        else setupToolbar()
    }

    override fun onDestroyView() {
        storePresenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_store, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvProducts = store_rv_store
        srlLoading = store_srl_loading
        llLoading = store_ll_loading

        storePresenter.onViewCreated()
    }

    override fun setupToolbar() {
        setHasOptionsMenu(true)
    }

    override fun setupList(color: String?) {
        activity?.let {
            storeAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                    is FooterViewModel -> FOOTER_VIEW_MODEL_ID
                    is StoreBannerListViewModel -> STORE_BANNER_LIST_VIEW_MODEL_ID
                    is StoreCategoryListViewModel -> STORE_CATEGORY_LIST_VIEW_MODEL_ID
                    is StoreProductListViewModel -> STORE_PRODUCT_LIST_VIEW_MODEL_ID
                    is StoreProductListBannerViewModel -> STORE_PRODUCT_LIST_BANNER_VIEW_MODEL_ID
                    is StoreProductViewModel -> STORE_PRODUCT_VIEW_MODEL_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                    FOOTER_VIEW_MODEL_ID -> FooterViewHolder(view, onFooterClicked)
                    STORE_BANNER_LIST_VIEW_MODEL_ID -> BannerListViewHolder(view, onBannerClicked, color)
                    STORE_CATEGORY_LIST_VIEW_MODEL_ID -> CategoryListViewHolder(view, onCategoryClicked)
                    STORE_PRODUCT_LIST_VIEW_MODEL_ID -> ProductListViewHolder(view, onProductClicked)
                    STORE_PRODUCT_VIEW_MODEL_ID -> StoreProductViewHolder(view, onProductClicked)
                    STORE_PRODUCT_LIST_BANNER_VIEW_MODEL_ID -> StoreProductListBannerViewHolder(view)
                    else -> throw IllegalArgumentException()
                }
            })

            rvProducts?.apply {
                adapter = storeAdapter
                setItemViewCacheSize(10)
                setHasFixedSize(true)
            }
        }
    }

    val onFooterClicked = object : ViewHolder.Listener<StoreFooterViewModel> {
        override fun onClick(viewModel: StoreFooterViewModel) {
            val intent = Intent(activity, ProductsActivity::class.java)
            intent.putExtra(EXTRA_PRODUCTS_TYPE, ProductsPresenter.Type.LIST)
            intent.putExtra(EXTRA_PRODUCTS_VIEW_MODEL, viewModel.productList)
            startActivity(intent)
        }
    }

    val onCategoryClicked = object : ViewHolder.Listener<StoreCategoryListItemViewModel> {
        override fun onClick(viewModel: StoreCategoryListItemViewModel) {
            if (viewModel.hasSubcategories) {
                val intent = Intent(activity, StoreCategoryActivity::class.java)
                intent.putExtra(EXTRA_CATEGORY, viewModel)
                startActivity(intent)
            } else {
                val productListViewModel = ProductListViewModel(viewModel.pk, viewModel.title)
                val intent = Intent(activity, ProductsActivity::class.java)
                intent.putExtra(EXTRA_PRODUCTS_TYPE, ProductsPresenter.Type.CATEGORY)
                intent.putExtra(EXTRA_PRODUCTS_VIEW_MODEL, productListViewModel)
                startActivity(intent)
            }
        }
    }

    val onProductClicked = object : ViewHolder.Listener<StoreProductViewModel> {
        override fun onClick(viewModel: StoreProductViewModel) {
            val intent = Intent(activity, ProductActivity::class.java)
            intent.putExtra(EXTRA_PRODUCT, viewModel)
            startActivity(intent)
        }
    }

    val onBannerClicked = object : ViewHolder.Listener<StoreBannerViewModel> {
        override fun onClick(viewModel: StoreBannerViewModel) = with(viewModel) {
            when {
                category != null -> onCategoryClicked.onClick(category)
                product != null -> onProductClicked.onClick(product)
                productList != null -> onFooterClicked.onClick(StoreFooterViewModel(productList))
                else -> throw IllegalArgumentException()
            }
        }
    }

    override fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener {
            srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            storePresenter.onRefresh()
        }
    }

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(activity, getString(resId), onDismissListener)
    }

    override fun showStore(viewModels: List<ViewModel>) {
        storeAdapter?.setItems(viewModels)
    }

    override fun showLoading() {
        if (!srlLoading.isRefreshing) {
            llLoading.visibility = View.VISIBLE
            srlLoading.visibility = View.GONE
            srlLoading.isEnabled = false
        }
    }

    override fun hideLoading() {
        if (llLoading.visibility == View.VISIBLE) {
            llLoading.visibility = View.GONE
            srlLoading.visibility = View.VISIBLE
        }

        if (srlLoading.isRefreshing) {
            srlLoading.isRefreshing = false
        }

        srlLoading.isEnabled = true
    }

    override fun onReselected() {
        if (linearLayoutManager != null) {
            val smoothScroller = object : LinearSmoothScroller(activity) {
                override fun getVerticalSnapPreference(): Int {
                    return LinearSmoothScroller.SNAP_TO_START
                }
            }
            smoothScroller.targetPosition = 0
            linearLayoutManager?.startSmoothScroll(smoothScroller)
        }
    }

    override fun showEmptyState() {
        srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.GONE
    }
}
