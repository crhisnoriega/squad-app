package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.linearlayout_card_header_large_view.view.*

class CardHeaderLargeView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        inflate(context, R.layout.linearlayout_card_header_large_view, this)

        val tvTitle = card_header_large_view_tv_title
        val tvDescription = card_header_large_view_tv_description
        val ivImage = card_header_large_view_iv_image

        val styledAttributes =
            getContext().obtainStyledAttributes(attrs, R.styleable.CardHeaderLargeView)

        val imageResId =
            styledAttributes.getResourceId(R.styleable.CardHeaderLargeView_android_icon, 0)
        ivImage.setImageResource(imageResId)

        val title: String? =
            styledAttributes.getString(R.styleable.CardHeaderLargeView_android_title)
        title?.let {
            tvTitle.text = title
            tvTitle.visibility = View.VISIBLE
        }

        tvDescription.text =
            styledAttributes.getString(R.styleable.CardHeaderLargeView_android_description)

        styledAttributes.recycle()
    }
}
