package co.socialsquad.squad.presentation.feature.feedback

import co.socialsquad.squad.data.entity.FeedbackItem
import co.socialsquad.squad.data.entity.FeedbackQuestion
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.Serializable

class FeedbackQuestionViewModel(
    val pk: Int,
    val title: String?,
    val description: String,
    val items: List<FeedbackItemViewModel>
) : ViewModel, Serializable {
    constructor(question: FeedbackQuestion) : this(
        question.pk ?: 0,
        question.title,
        question.content ?: "",
        getViewModels(question)
    )
}

fun getViewModels(question: FeedbackQuestion): List<FeedbackItemViewModel> {
    val list = mutableListOf<FeedbackItemViewModel>()
    if (question.items == null || question.items.isEmpty()) {
        if (question.type() == FeedbackQuestion.Type.OpenText) {
            list.add(FeedbackItemViewModel.FeedbackTextItemViewModel(FeedbackItem(0, "", "")))
        }
    }
    question.items?.forEach {
        it?.apply {
            when (question.type()) {
                FeedbackQuestion.Type.OpenText -> list.add(FeedbackItemViewModel.FeedbackTextItemViewModel(this))
                FeedbackQuestion.Type.SingleChoice, FeedbackQuestion.Type.MultipleChoice ->
                    list.add(FeedbackItemViewModel.FeedbackOptionItemViewModel(this, question.type() == FeedbackQuestion.Type.MultipleChoice))
            }
        }
    }

    return list
}
