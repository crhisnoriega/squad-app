package co.socialsquad.squad.presentation.feature.company

interface SystemSelectView {

    fun setListItems(items: List<SelectionItemViewModel>)
    fun setSaveButtonEnabled(enabled: Boolean)
    fun showLoading()
    fun hideLoading()
    fun closeScreen()
}
