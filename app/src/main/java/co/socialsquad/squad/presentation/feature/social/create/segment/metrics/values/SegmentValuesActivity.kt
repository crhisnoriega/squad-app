package co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.VALUE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.ValueViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_segment_values.*
import javax.inject.Inject

class SegmentValuesActivity : BaseDaggerActivity(), SegmentValuesView {

    @Inject
    lateinit var segmentValuesPresenter: SegmentValuesPresenter

    private lateinit var miAdd: MenuItem
    private lateinit var rvValues: RecyclerView
    private lateinit var srlRefresh: SwipeRefreshLayout
    private var valuesAdapter: RecyclerViewAdapter? = null
    private val glide by lazy { Glide.with(this) }
    private var loading: AlertDialog? = null

    override fun onDestroy() {
        segmentValuesPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_segment_values, menu)
        miAdd = menu.findItem(R.id.segment_values_mi_apply)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.segment_values_mi_apply -> {
                segmentValuesPresenter.onAddClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segment_values)

        rvValues = segment_values_rv_metrics
        srlRefresh = segment_values_srl_refresh

        segmentValuesPresenter.onCreate(intent)
    }

    override fun setupToolbar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.title = title
    }

    private var isMultipick: Boolean? = null

    override fun setupList(metric: MetricViewModel) {
        isMultipick = metric.multipick
        valuesAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ValueViewModel -> VALUE_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View) = when (viewType) {
                VALUE_VIEW_MODEL_ID -> ValueViewHolder(view, glide, metric.multipick, onItemSelected)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        with(rvValues) {
            layoutManager = PrefetchingLinearLayoutManager(this@SegmentValuesActivity)
            adapter = valuesAdapter
            addOnScrollListener(
                EndlessScrollListener(
                    8,
                    linearLayoutManager = layoutManager as LinearLayoutManager,
                    onScrolledBeyondVisibleThreshold = {
                        if (loading?.isShowing == false) segmentValuesPresenter.onScrolledBeyondVisibleThreshold()
                    }
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
            addItemDecoration(ListDividerItemDecoration(context))
        }
        (rvValues.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    val onItemSelected = object : ViewHolder.Listener<ValueViewModel> {
        override fun onClick(viewModel: ValueViewModel) {
            setAddEnabled(true)
            if (viewModel.selected) {
                segmentValuesPresenter.onValueDeselected(viewModel)
            } else {
                segmentValuesPresenter.onValueSelected(viewModel)
            }
        }
    }

    override fun setupSwipeRefresh() {
        srlRefresh.isEnabled = false
        srlRefresh.setOnRefreshListener {
            segmentValuesPresenter.onRefresh()
        }
    }

    override fun showErrorMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(textResId), onDismissListener)
    }

    override fun setItems(values: List<ValueViewModel>) {
        val newList = values.toMutableList<ViewModel>().apply { add(DividerViewModel) }
        valuesAdapter?.setItems(newList)
    }

    override fun addItems(values: List<ValueViewModel>) {
        valuesAdapter?.remove(DividerViewModel)
        val newList = values.toMutableList<ViewModel>().apply { add(DividerViewModel) }
        valuesAdapter?.addItems(newList)
    }

    override fun updateSelected(viewModel: ValueViewModel) {
        valuesAdapter?.update(viewModel)
    }

    override fun enableRefreshing() {
        srlRefresh.isEnabled = true
    }

    override fun disableRefreshing() {
        srlRefresh.isEnabled = false
    }

    override fun stopRefreshing() {
        srlRefresh.isRefreshing = false
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun setAddEnabled(enabled: Boolean) {
        miAdd.isEnabled = enabled
    }

    override fun finishWithResult(resultCode: Int, intent: Intent) {
        setResult(resultCode, intent)
        finish()
    }
}
