package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ToolContent(
    @SerializedName("icon") val icon: SimpleMedia,
    @SerializedName("title") val title: String,
    @SerializedName("subtitle") val subtitle: String,
    @SerializedName("link") val link: String
) : Parcelable
