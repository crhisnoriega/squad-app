package co.socialsquad.squad.presentation.feature.existinglead.adapter.result.separator

import android.view.ViewGroup
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ResultsViewHolder
import co.socialsquad.squad.presentation.feature.existinglead.adapter.result.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM

class SeparatorDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): ResultsViewHolder =
        LeadSeparatorViewHolder(parent)

    override fun onBindViewHolder(holder: ResultsViewHolder, item: ExistingLeadVM) = holder.bind(item)
}
