package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PlanSummaryData(
        @SerializedName("plan_summary") val plan_summary: PlanData?,
        var first: Boolean = false
) : Parcelable