package co.socialsquad.squad.presentation.feature.widgetScheduler.validation

import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreationResponse
import co.socialsquad.squad.presentation.feature.widgetScheduler.repository.TimetableValidationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class TimetableValidationViewModel(
    private val timetableValidationRepository: TimetableValidationRepository
) : BaseViewModel() {

    @ExperimentalCoroutinesApi
    private val timetableCreation =
        MutableStateFlow<Resource<TimetableCreationResponse>>(Resource.empty())

    @ExperimentalCoroutinesApi
    fun timetableCreation(): StateFlow<Resource<TimetableCreationResponse>> {
        return timetableCreation
    }

    @ExperimentalCoroutinesApi
    fun createSubmission(timetableId: Long, objectId: Long, creation: TimetableCreation) {
        viewModelScope.launch {
            timetableCreation.value = Resource.loading(null)
            timetableValidationRepository.createSubmission(timetableId, objectId, creation)
                .flowOn(Dispatchers.IO)
                .catch { e ->
                    e.printStackTrace()
                    timetableCreation.value = Resource.error(e.toString(), null)
                }
                .collect {
                    it?.let {
                        timetableCreation.value = Resource.success(it)
                    } ?: run {
                        timetableCreation.value = Resource.empty()
                    }
                }
        }
    }
}
