package co.socialsquad.squad.presentation.feature.squad.social

import co.socialsquad.squad.presentation.custom.ViewModel

class SquadHeaderViewModel(
    val squadName: String,
    val squadTitle: String,
    val squadImage: String,
    val backgroundColor: String,
    val textColor: String
) : ViewModel
