package co.socialsquad.squad.presentation.feature.store

import android.content.DialogInterface
import co.socialsquad.squad.presentation.custom.ViewModel

interface StoreView {
    fun setupToolbar()
    fun setupList(color: String?)
    fun setupSwipeRefresh()
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun showStore(viewModels: List<ViewModel>)
    fun showLoading()
    fun hideLoading()
    fun showEmptyState()
}
