package co.socialsquad.squad.presentation.feature.recognition.repository

import co.socialsquad.squad.presentation.feature.recognition.data.RecognitionAPI
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import co.socialsquad.squad.presentation.feature.recognition.model.PlanSummaryResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface RecognitionRepository {
    suspend fun fetchPlanSummary(): Flow<PlanSummaryResponse>
    suspend fun fetchPlan(planId: String): Flow<PlanData>
    suspend fun agreeRecongTerms(planId: String): Flow<AgreeRefuseTermsResponse>
    suspend fun refuseRecongTerms(planId: String): Flow<AgreeRefuseTermsResponse>

}

@ExperimentalCoroutinesApi
class RecognitionRepositoryImpl(private val recognitionAPI: RecognitionAPI) : RecognitionRepository {
    override suspend fun fetchPlanSummary(): Flow<PlanSummaryResponse> {
        return flow {
            emit(recognitionAPI.fetchPlanSummary())
        }
    }

    override suspend fun fetchPlan(planId: String): Flow<PlanData> {
        return flow {
            emit(recognitionAPI.fetchPlan(planId))
        }

    }

    override suspend fun agreeRecongTerms(planId: String): Flow<AgreeRefuseTermsResponse> {
        return flow {
            recognitionAPI.agreeRecognitionTerms(planId)
            emit(AgreeRefuseTermsResponse(true))
        }
    }

    override suspend fun refuseRecongTerms(planId: String): Flow<AgreeRefuseTermsResponse> {
        return flow {
            recognitionAPI.refuseRecognitionTerms(planId)
            emit(AgreeRefuseTermsResponse(false))
        }
    }


}