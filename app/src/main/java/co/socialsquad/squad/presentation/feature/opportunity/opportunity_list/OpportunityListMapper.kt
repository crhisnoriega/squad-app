package co.socialsquad.squad.presentation.feature.opportunity.opportunity_list

import android.content.Context
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.ObjectStagesStatus
import co.socialsquad.squad.data.entity.opportunity.OpportunityObject
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectProgress
import co.socialsquad.squad.data.entity.opportunity.OpportunityObjectType
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ItemOpportunityStepViewModel
import co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions.ItemOpportunityViewModel

class OpportunityListMapper(
    private val context: Context,
    private val presenter: OpportunityListPresenter,
    private val opportunityId: Int,
    private val objectId: Int,
    private val objectType: OpportunityObjectType,
    private val colors: OpportunityInitialsColors
) {

    fun mapOpportunityObject(opportunityObject: OpportunityObject): ItemOpportunityViewModel {

        return ItemOpportunityViewModel(
            primaryInitials = opportunityObject.primaryInitials,
            primaryFieldText = opportunityObject.primaryField,
            step = opportunityObject.title,
            timeSpan = opportunityObject.timestamp,
            steps = opportunityObject.progress.toItemOpportunityStepViewModel(),
            colors = colors
        ) {
            presenter.getSubmissionDetails(
                opportunityId,
                objectId,
                opportunityObject.id,
                objectType
            )
        }
    }

    private fun OpportunityObjectProgress.toItemOpportunityStepViewModel(): List<ItemOpportunityStepViewModel> {

        val stages = (1..stagesCount).toList()

        return stages.map { index ->
            val isPrevious = index < this.currentCount
            val showLine = index > 1
            if (index != this.currentCount) {
                val icon =
                    if (isPrevious) R.drawable.ic_opportunity_step_green else R.drawable.ic_opportunity_step_silver
                ItemOpportunityStepViewModel.Normal(
                    iconRes = icon,
                    showLine = showLine
                )
            } else {
                ItemOpportunityStepViewModel.Bigger(
                    iconRes = this.status.mapStepStatusIcon(isPrevious),
                    showLine = showLine
                )
            }
        }
    }

    private fun ObjectStagesStatus.mapStepStatusIcon(isPrevious: Boolean): Int {

        return when (this) {
            ObjectStagesStatus.PENDING -> R.drawable.ic_opportunity_step_silver
            ObjectStagesStatus.COMPLETED -> if (isPrevious) R.drawable.ic_opportunity_step_green else R.drawable.ic_approved
            ObjectStagesStatus.BLOCKED -> if (isPrevious) R.drawable.ic_opportunity_step_orange else R.drawable.ic_blocked
            ObjectStagesStatus.CLOSED -> if (isPrevious) R.drawable.ic_opportunity_step_red else R.drawable.ic_ended
        }
    }
}
