package co.socialsquad.squad.presentation.feature.resources.viewmodel

import androidx.lifecycle.MutableLiveData
import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.presentation.feature.base.Resource

class ResourcesViewModel : BaseViewModel() {

    var statusMutable = MutableLiveData<Resource<Opportunity<*>>>()


}