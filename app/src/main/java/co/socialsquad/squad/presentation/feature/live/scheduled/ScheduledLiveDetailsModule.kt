package co.socialsquad.squad.presentation.feature.live.scheduled

import dagger.Binds
import dagger.Module

@Module
abstract class ScheduledLiveDetailsModule {
    @Binds
    abstract fun view(scheduledLiveDetailsActivity: ScheduledLiveDetailsActivity): ScheduledLiveDetailsView
}
