package co.socialsquad.squad.presentation.feature.terms

import android.content.Intent
import android.util.Log
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.RegisterUserRequest
import co.socialsquad.squad.data.entity.TermsResponse
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.DateTimeUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import java.util.*
import javax.inject.Inject

class TermsPresenter @Inject constructor(
        private val view: TermsView,
        private val loginRepository: LoginRepository,
        private val analytics: Analytics
) {

    val compositeDisposable = CompositeDisposable()

    fun terms(textTerms: String) {
        val gson = Gson()
        val terms: TermsResponse.Result = gson.fromJson(textTerms, TermsResponse.Result::class.java)

        var obj = gson.fromJson(textTerms, JsonObject::class.java)
        Log.i("deeplink", obj.get("updated_at").asString)

        view.setTerms(terms)

        val calendar = DateTimeUtils.parseToCalendar(terms.updated_at)

        view.setUpdatedAt(
                calendar?.get(Calendar.DAY_OF_MONTH).toString(),
                calendar?.get(Calendar.MONTH),
                calendar?.get(Calendar.YEAR).toString()
        )
    }

    fun registerUser(registerJson: String, password: String?, subdomain: String) {
        analytics.sendEvent(Analytics.SIGNUP_TERMS_TAP_AGREE)

        loginRepository.subdomain = subdomain

        val gson = Gson()
        val request: RegisterUserRequest = gson.fromJson(registerJson, RegisterUserRequest::class.java)
        request.user.password = password
        compositeDisposable.add(
                loginRepository.register(request)
                        .doOnSubscribe { view.showLoading() }
                        .doOnTerminate { view.hideLoading() }
                        .doOnError {
                            it.printStackTrace()
                        }
                        .subscribe(
                                {

                                    loginRepository.squadColor = it.company.primaryColor
                                    loginRepository.userCompany = it
                                    loginRepository.token = it.token
                                    loginRepository.getDeviceToken()

                                    view.moveToKickoff()
                                },
                                {
                                    it.printStackTrace()
                                    FirebaseCrashlytics.getInstance().recordException(it)
                                    if (it is HttpException) {
                                        val body = it.response()?.errorBody()
                                        body?.string()?.let {
                                            FirebaseCrashlytics.getInstance().log(it)
                                            when {
                                                // USER_TOKEN_NOT_FOUND
                                                it.contains("U011") -> {
                                                    view.showDialogError(
                                                            R.string.signup_error_token_subject,
                                                            R.string.signup_error_messagem,
                                                            R.string.signup_error_token_dialog,
                                                            "U011"
                                                    )
                                                }
                                                // USER_ALREADY_ACTIVATED
                                                it.contains("U007") -> {
                                                    view.showDialogError(
                                                            R.string.signup_error_activated_subject,
                                                            R.string.signup_error_messagem,
                                                            R.string.signup_error_activated_dialog,
                                                            "U007"
                                                    )
                                                }
                                                // SUBDOMAIN_NOT_FOUND
                                                it.contains("S000") -> {

                                                    view.showDialogError(
                                                            R.string.signup_error_subdomain_subject,
                                                            R.string.signup_error_messagem,
                                                            R.string.signup_error_subdomain_dialog,
                                                            "S000"
                                                    )
                                                }
                                                else -> {
                                                    view.showMessage(R.string.error_get_terms)
                                                }
                                            }
                                        }
                                    } else {
                                        view.showMessage(R.string.error_get_terms)
                                    }
                                }
                        )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onCreate(intent: Intent) {
        analytics.sendEvent(Analytics.SIGNUP_TERMS_SCREEN_VIEW)
        if (intent.hasExtra(TermsActivity.extraCompany)) {
            val gson = Gson()
            val company = gson.fromJson(intent.extras!!.getString(TermsActivity.extraCompany), Company::class.java)
            view.setupHeader(company)
        }
    }

    fun onTermsScrollDown() {
        analytics.sendEvent(Analytics.SIGNUP_TERMS_ACTION_SCROLL_DOWN)
    }
}
