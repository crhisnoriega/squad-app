package co.socialsquad.squad.presentation.feature.lead.di

import co.socialsquad.squad.presentation.feature.lead.LeadViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object LeadModule {
    val instance = module {
        viewModel { LeadViewModel() }
    }
}
