package co.socialsquad.squad.presentation.feature.widgetScheduler.domain

import co.socialsquad.squad.domain.model.ScaleShift

data class ScaleShiftVO(
    val scaleShift: ScaleShift,
    var isSelected: Boolean,
)
