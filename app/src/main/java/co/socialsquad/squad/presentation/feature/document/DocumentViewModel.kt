package co.socialsquad.squad.presentation.feature.document

import co.socialsquad.squad.data.entity.Document
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.toDate
import java.io.Serializable
import java.util.Date

class DocumentViewModel(
    val pk: Int,
    val cover: String?,
    val title: String?,
    val description: String?,
    val author: String?,
    val authorPk: Int?,
    val viewCount: Int,
    val timestamp: Date?,
    val fileUrl: String?
) : ViewModel, Serializable {
    constructor(document: Document) : this(
        document.pk ?: 0,
        document.cover?.url,
        document.title,
        document.content,
        document.owner?.user?.fullName,
        document.owner?.pk,
        document.visualizations ?: 0,
        document.created_at?.toDate(),
        document.file
    )
}
