package co.socialsquad.squad.presentation.feature.about

import android.util.Log
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.util.Analytics
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AboutPresenter @Inject constructor(
        private val view: AboutView,
        private val repository: LoginRepository,
        private val preferences: Preferences,
        private val analytics: Analytics
) {

    val compositeDisposable = CompositeDisposable()

    fun onCreate(subdomain: String) {
        analytics.sendEvent(Analytics.SIGNUP_ABOUT_SCREEN_VIEW)

        preferences.subdomain = subdomain
        compositeDisposable.add(
                repository.aboutNoHeader()
                        .doOnSubscribe {
                            view.showLoading()
                            view.hideTryAgain()
                        }
                        .doOnTerminate { view.hideLoading() }
                        .subscribe(
                                {
                                    view.showAbout(it)
                                },
                                {
                                    Log.i("deeplink", it.message, it)
                                    it.printStackTrace()
                                    view.showTryAgain()
                                }
                        )
        )
    }

    fun onNext() {
        analytics.sendEvent(Analytics.SIGNUP_ABOUT_TAP_NEXT)
    }

    fun onTermsScrollDown() {
        analytics.sendEvent(Analytics.SIGNUP_ABOUT_ACTION_SCROLL_DOWN)
    }
}
