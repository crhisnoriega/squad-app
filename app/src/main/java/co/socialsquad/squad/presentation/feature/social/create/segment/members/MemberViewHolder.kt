package co.socialsquad.squad.presentation.feature.social.create.segment.members

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.RequestManager
import kotlinx.android.synthetic.main.linearlayout_segment_group_members_item.view.*

class MemberViewHolder(itemView: View, val glide: RequestManager, val listener: Listener<UserViewModel>) : ViewHolder<UserViewModel>(itemView) {

    private val ivMember: ImageView = itemView.segment_group_members_iv_member
    private val tvMember: TextView = itemView.segment_group_members_tv_member
    private val cbMember: CheckBox = itemView.segment_group_members_rb

    override fun bind(viewModel: UserViewModel) {
        glide.load(viewModel.imageUrl)
            .circleCrop()
            .crossFade()
            .into(ivMember)
        tvMember.text = "${viewModel.firstName} ${viewModel.lastName}"
        cbMember.isChecked = viewModel.selected
        itemView.setOnClickListener { cbMember.isChecked = !cbMember.isChecked }
        cbMember.setOnCheckedChangeListener { _, isChecked ->
            viewModel.selected = isChecked
            listener.onClick(viewModel)
        }
    }

    override fun recycle() {
        glide.clear(ivMember)
    }
}
