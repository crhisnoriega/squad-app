package co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SummaryTimetable
import co.socialsquad.squad.domain.model.Validation
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleSummary(
    @SerializedName("timetable") val timetable: SummaryTimetable,
    @SerializedName("validation") val validation: Validation
) : Parcelable
