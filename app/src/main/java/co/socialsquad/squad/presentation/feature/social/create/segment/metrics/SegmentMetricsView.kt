package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel

interface SegmentMetricsView {
    fun setupToolbar()
    fun setupList()
    fun setupSwipeRefresh()
    fun showErrorMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun setItems(metrics: List<MetricViewModel>)
    fun enableRefreshing()
    fun stopRefreshing()
    fun showLoading()
    fun hideLoading()
    fun openSegmentMetric(metric: MetricViewModel, requestCode: Int)
    fun finishWithResult(resultCode: Int, intent: Intent)
    fun updateItem(itemViewModel: MetricViewModel)
    fun enableFinishButton(enabled: Boolean)
    fun setPotentialReachValue(reach: PotentialReachViewModel)
    fun setPotentialReachArcColor(color: String)
}
