package co.socialsquad.squad.presentation.feature.existinglead.adapter.loading

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

private const val ITEM_LOADING_COUNT = 10

class LeadNotFoundStateAdapter(val searchTerm: String) : RecyclerView.Adapter<LeadNotFoundViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeadNotFoundViewHolder {
        return LeadNotFoundViewHolder(parent, searchTerm)
    }

    override fun onBindViewHolder(holder: LeadNotFoundViewHolder, position: Int) = holder.bind()

    override fun getItemCount(): Int = ITEM_LOADING_COUNT
}
