package co.socialsquad.squad.presentation.feature.searchv2.results.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder.SearchLoadingViewHolder

class SearchLoadingAdapter(private var searchTerm: String?) :
        RecyclerView.Adapter<SearchLoadingViewHolder>() {

    private lateinit var searchHolder: SearchLoadingViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchLoadingViewHolder {
        searchHolder = SearchLoadingViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.fragment_searchv2_loading_state, parent, false)
        )
        searchHolder.bind("")

        return searchHolder
    }

    override fun onBindViewHolder(holder: SearchLoadingViewHolder, position: Int) {
        Log.i("search", "updating on viewholder $searchTerm")
        holder.bind(searchTerm)
    }

    override fun getItemCount() = 1

    internal fun updateSearchTerm(searchTerm: String?) {
        this.searchTerm = searchTerm
        notifyDataSetChanged()
    }
}
