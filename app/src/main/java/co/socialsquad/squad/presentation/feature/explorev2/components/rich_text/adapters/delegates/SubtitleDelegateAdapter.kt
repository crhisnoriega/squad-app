package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextSubtitle
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_rich_text_subtitle.view.*

class SubtitleDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return SubtitleRichTextViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as SubtitleRichTextViewHolder
        holder.bind(item as RichTextSubtitle)
    }

    private inner class SubtitleRichTextViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_rich_text_subtitle)
    ) {
        fun bind(richTextObj: RichTextSubtitle) = with(itemView) {
            tv_item_rich_text_subtitle.text = richTextObj.subTitle
        }
    }
}
