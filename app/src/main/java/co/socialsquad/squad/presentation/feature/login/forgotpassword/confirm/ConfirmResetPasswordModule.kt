package co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm

import dagger.Binds
import dagger.Module

@Module
abstract class ConfirmResetPasswordModule {

    @Binds
    abstract fun view(view: ConfirmResetPasswordActivity): ConfirmResetPasswordView
}
