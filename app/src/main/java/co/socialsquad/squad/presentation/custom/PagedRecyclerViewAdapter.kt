package co.socialsquad.squad.presentation.custom

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

interface LiveDataViewModel {
    val id: Long
}

abstract class PagedViewHolder<in T : LiveDataViewModel>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    interface Listener<T> {
        fun onClick(viewModel: T)
    }

    abstract fun bind(viewModel: T)
    abstract fun recycle()
}

interface PagedViewHolderFactory {
    fun getType(viewModel: LiveDataViewModel): Int
    fun getHolder(viewType: Int, view: View): PagedViewHolder<*>
}

class PagedRecyclerViewAdapter<T : LiveDataViewModel>(private val viewHolderFactory: PagedViewHolderFactory) : PagedListAdapter<T, PagedViewHolder<T>>(
    object : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(
            oldViewModel: T,
            newViewModel: T
        ) = oldViewModel.id == newViewModel.id

        override fun areContentsTheSame(
            oldViewModel: T,
            newViewModel: T
        ) = oldViewModel.id == newViewModel.id
    }
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagedViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return viewHolderFactory.getHolder(viewType, view) as PagedViewHolder<T>
    }

    override fun onBindViewHolder(holder: PagedViewHolder<T>, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.id!!
    }

    override fun getItemViewType(position: Int): Int {
        getItem(position)?.let {
            return viewHolderFactory.getType(it)
        }
        return 0
    }

    override fun onViewRecycled(holder: PagedViewHolder<T>) {
        holder.recycle()
        super.onViewRecycled(holder)
    }
}
