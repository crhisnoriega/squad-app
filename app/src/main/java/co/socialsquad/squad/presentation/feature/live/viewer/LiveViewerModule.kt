package co.socialsquad.squad.presentation.feature.live.viewer

import dagger.Binds
import dagger.Module

@Module
abstract class LiveViewerModule {
    @Binds
    internal abstract fun view(liveViewerActivity: LiveViewerActivity): LiveViewerView
}
