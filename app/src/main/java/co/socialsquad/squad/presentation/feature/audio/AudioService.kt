package co.socialsquad.squad.presentation.feature.audio

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.AudioRepository
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.exoplayer2.DefaultControlDispatcher
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import dagger.android.AndroidInjection
import javax.inject.Inject

private const val NOTIFICATION_ID = 907

class AudioService : Service() {

    @Inject
    lateinit var audioRepository: AudioRepository

    private var playerNotificationManager: PlayerNotificationManager? = null
    private var notification: Notification? = null
    private var mediaSession: MediaSessionCompat? = null

    override fun onCreate() {
        AndroidInjection.inject(this)

        AudioManager.addListener(object : AudioManager.Listener {
            override fun onReady(isPlaying: Boolean) {
                saveAudio()
                if (isPlaying) startForeground(NOTIFICATION_ID, notification)
                else stopForeground(false)
            }

            override fun onEnded() {
                saveAudio()
                stopForeground(false)
            }

            override fun onReleased() {
                saveAudio()
                playerNotificationManager?.setPlayer(null)
            }
        })

        var bitmap: Bitmap? = null
        Glide.with(this)
            .asBitmap()
            .load(AudioManager.audioViewModel?.avatar)
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    bitmap = resource
                }
            })

        mediaSession = MediaSessionCompat(applicationContext, "MEDIA_SESSION_TAG").apply { isActive = true }

        playerNotificationManager = PlayerNotificationManager(
            this,
            getString(R.string.notification_channel_audio_id),
            NOTIFICATION_ID,
            object : PlayerNotificationManager.MediaDescriptionAdapter {
                override fun getCurrentContentText(player: Player): CharSequence? = AudioManager.audioViewModel?.author
                override fun getCurrentContentTitle(player: Player): CharSequence = AudioManager.audioViewModel?.title.toString()
                override fun createCurrentContentIntent(player: Player): PendingIntent? = null
                override fun getCurrentLargeIcon(player: Player, callback: PlayerNotificationManager.BitmapCallback): Bitmap? = bitmap
            },
            object : PlayerNotificationManager.NotificationListener {
                override fun onNotificationStarted(notificationId: Int, notification: Notification) {
                    this@AudioService.notification = notification
                    startForeground(notificationId, notification)
                }

                override fun onNotificationCancelled(notificationId: Int) {
                    stopSelf()
                }
            }
        ).apply {
            setSmallIcon(R.drawable.ic_notification_squad)
            setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
            setPriority(NotificationCompat.PRIORITY_LOW)
            setUseChronometer(true)
            setUseNavigationActions(false)
            setControlDispatcher(DefaultControlDispatcher(30000, 30000))
            setUseStopAction(false)
            setPlayer(AudioManager.player)
            mediaSession?.sessionToken?.let { setMediaSessionToken(it) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        saveAudio()
        mediaSession?.release()
    }

    private fun saveAudio() {
        AudioManager.audioViewModel?.let { audioRepository.saveAudio(it) }
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int) = Service.START_STICKY
}
