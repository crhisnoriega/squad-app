package co.socialsquad.squad.presentation.feature.social.details

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedComment
import co.socialsquad.squad.data.entity.FeedCommentRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_POST_DETAIL_COMMENT
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_POST_DETAIL_SHARE
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModelMapper
import co.socialsquad.squad.presentation.feature.social.comment.CommentViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PostDetailsPresenter @Inject constructor(
    private val postDetailsView: PostDetailsView,
    private val socialRepository: SocialRepository,
    loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {

    private val socialViewModelMapper = SocialViewModelMapper()
    private val compositeDisposable = CompositeDisposable()
    private val userName = loginRepository.userCompany?.user?.firstName + " " + loginRepository.userCompany?.user?.lastName
    private val userAvatar = loginRepository.userCompany?.user?.avatar
    private val userPk = loginRepository.userCompany?.pk ?: 0
    private val subdomain = loginRepository.subdomain

    private var pk: Int = -1
    private var commentPage: Int = 1
    private var isCompleted = false
    private var isLoading = false

    fun onCreate(intent: Intent?) {
        intent?.let {
            if (it.hasExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL) || it.hasExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL) || it.hasExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL)) {
                loadLocalData(it)
                return
            } else {
                pk = it.getIntExtra(PostDetailsActivity.POST_DETAIL_PK, -1)
            }
        }
        compositeDisposable.add(
            socialRepository.getFeedItem(pk)
                .doOnSubscribe {
                    isLoading = true
                    postDetailsView.showLoading()
                }
                .doOnTerminate {
                    isLoading = false
                    postDetailsView.hideLoading()
                }
                .map { socialViewModelMapper.mapPost(it, socialRepository.audioEnabled, userAvatar, subdomain) }
                .subscribe(
                    { showPost(it) },
                    {
                        it.printStackTrace()
                        postDetailsView.showMessage(R.string.social_post_not_found) { postDetailsView.finish() }
                    },
                    { showComments() }
                )
        )
    }

    private fun loadLocalData(intent: Intent) {
        postDetailsView.hideLoading()
        val viewModel: PostViewModel? = when {
            intent.hasExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL) -> {
                intent.getSerializableExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL) as PostImageViewModel
            }
            intent.hasExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL) -> {
                intent.getSerializableExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL) as PostVideoViewModel
            }
            intent.hasExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL) -> {
                intent.getSerializableExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL) as PostLiveViewModel
            }
            else -> null
        }
        pk = viewModel?.pk ?: -1
        showPost(viewModel)
        showComments()
    }

    fun likePost(post: PostViewModel) {
        compositeDisposable.add(
            socialRepository.likePost(post.pk).subscribe(
                {
                    post.liked = it.liked
                    post.likeAvatars = it.likedUsersAvatars
                    post.likeCount = it.likesCount
                },
                {
                    it.printStackTrace()
                    post.liked = false
                },
                {}
            )
        )
    }

    fun onEditSelected(postViewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getSegmentation(postViewModel.pk)
                .doOnSubscribe { postDetailsView.showLoading() }
                .doOnTerminate { postDetailsView.hideLoading() }
                .subscribe(
                    {
                        postViewModel.audience = AudienceViewModel(postViewModel.audience.type, it)
                        when (postViewModel) {
                            is PostImageViewModel, is PostVideoViewModel -> postDetailsView.showEditMedia(postViewModel)
                            is PostLiveViewModel -> postDetailsView.showEditLive(postViewModel)
                        }
                    },
                    {
                        it.printStackTrace()
                        postDetailsView.showMessage(R.string.social_error_failed_to_edit_post)
                    }
                )
        )
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_POST_DETAIL_SHARE)
    }

    fun onDeleteClicked() {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.deleteFeed(pk)
                .doOnSubscribe {
                    postDetailsView.showLoading()
                    postDetailsView.hideDeleteDialog()
                }.doOnTerminate {
                    postDetailsView.hideLoading()
                }.subscribe(
                    {
                        postDetailsView.finish()
                    },
                    {
                        it.printStackTrace()
                        postDetailsView.showMessage(R.string.social_error_failed_to_delete_post)
                    }
                )
        )
    }

    fun onAudioToggled(audioEnabled: Boolean) {
        socialRepository.audioEnabled = audioEnabled
    }

    fun commentPost(viewModel: PostViewModel, text: String) {
        tagWorker.tagEvent(TAG_TAP_POST_DETAIL_COMMENT)
        val commentViewModel = CommentViewModel(
            -1,
            userPk,
            userAvatar,
            userName,
            text,
            null,
            false,
            false
        )
        postDetailsView.insertNewComment(commentViewModel)
        compositeDisposable.add(
            socialRepository.commentPost(FeedCommentRequest(viewModel.pk, text))
                .subscribe(
                    {
                        commentViewModel.pk = it.pk
                        commentViewModel.time = it.createdAt
                        commentViewModel.text = it.text ?: ""
                        commentViewModel.userAvatar = it.userCompany?.user?.avatar
                        commentViewModel.canDelete = it.canDelete
                        commentViewModel.canEdit = it.canEdit
                        postDetailsView.updateComment(commentViewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        postDetailsView.removeComment(commentViewModel)
                        postDetailsView.showMessage(R.string.social_error_failed_to_comment_post)
                    }
                )
        )
    }

    private fun showComments() {
        commentPage = 1
        compositeDisposable.add(
            socialRepository.getPostComments(pk, commentPage)
                .doOnSubscribe {
                    isLoading = true
                    postDetailsView.showLoadingComments()
                }
                .doOnComplete {
                    commentPage++
                    isLoading = false
                    postDetailsView.hideLoadingComments()
                }
                .doOnNext { if (it.next == null) isCompleted = true }
                .doOnNext { postDetailsView.updateCommentCount(it.count) }
                .map { it.results }
                .map { mapComments(it) }
                .subscribe(
                    { postDetailsView.setComments(it) },
                    { it.printStackTrace() }
                )
        )
    }

    private fun mapComments(comments: List<FeedComment>?) = comments.orEmpty().map { mapCommentsViewModel(it) }

    private fun mapCommentsViewModel(comment: FeedComment): CommentViewModel = CommentViewModel(
        comment.pk,
        comment.userCompany?.pk ?: -1,
        comment.userCompany?.user?.avatar,
        comment.userCompany?.user?.firstName + " " + comment.userCompany?.user?.lastName,
        comment.text ?: "", comment.createdAt, comment.canDelete, comment.canEdit
    )

    private fun showPost(socialViewModel: SocialViewModel?) {
        socialViewModel?.let {
            when (it) {
                is PostImageViewModel -> postDetailsView.showImagePost(it, userAvatar)
                is PostVideoViewModel -> postDetailsView.showVideoPost(it, userAvatar)
            }
        }
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isLoading || isCompleted) return
        compositeDisposable.add(
            socialRepository.getPostComments(pk, commentPage)
                .doOnSubscribe {
                    isLoading = true
                    postDetailsView.showLoadingComments()
                }
                .doOnComplete {
                    commentPage++
                    isLoading = false
                    postDetailsView.hideLoadingComments()
                }
                .doOnNext { if (it.next == null) isCompleted = true }
                .map { it.results }
                .map { mapComments(it) }
                .subscribe(
                    { postDetailsView.addComments(it) },
                    { it.printStackTrace() }
                )
        )
    }

    fun deleteComment(viewModel: CommentViewModel) {
        compositeDisposable.add(
            socialRepository.deleteComment(viewModel.pk)
                .subscribe(
                    { postDetailsView.removeComment(viewModel) },
                    {
                        it.printStackTrace()
                        postDetailsView.showMessage(R.string.social_error_failed_to_remove_comment)
                    }
                )
        )
    }

    fun onRefresh() {
        compositeDisposable.clear()
        showComments()
    }
}
