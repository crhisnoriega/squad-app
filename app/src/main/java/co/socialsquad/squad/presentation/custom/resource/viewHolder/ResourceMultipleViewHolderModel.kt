package co.socialsquad.squad.presentation.custom.resource.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement

data class ResourceMultipleViewHolderModel(val component: Complement) : ViewModel