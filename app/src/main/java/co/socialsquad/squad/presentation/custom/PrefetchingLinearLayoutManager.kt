package co.socialsquad.squad.presentation.custom

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView

private const val NUMBER_OF_PAGES_TO_PREFETCH = 2

class PrefetchingLinearLayoutManager(context: Context) : LinearLayoutManager(context) {

    private var orientationHelper: OrientationHelper? = null

    override fun setOrientation(orientation: Int) {
        super.setOrientation(orientation)
        orientationHelper = null
    }

    override fun getExtraLayoutSpace(state: RecyclerView.State): Int {
        if (orientationHelper == null) {
            orientationHelper = OrientationHelper.createOrientationHelper(this, orientation)
        }
        return orientationHelper!!.totalSpace * NUMBER_OF_PAGES_TO_PREFETCH
    }
}
