package co.socialsquad.squad.presentation.feature.profile.edit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.OpportunityHeader
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.form.FieldType
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.profile.edit.fragment.*
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_profile_picture.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class CompleteProfileActivity : BaseActivity() {
    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_profile_picture

    private val editType: ProfilePictureActivity.Companion.EDIT_TYPE by extra(EDIT_TYPE_FORM)

    private val viewModel by viewModel<ProfileEditViewModel>()

    private var fragments = mutableListOf<Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadFragment()
        configureViewPager()

    }

    override fun onResume() {
        super.onResume()
    }

    fun next() {
        Log.i("nav", "next: ${fragmentContainer.currentItem}")
        if (fragmentContainer.currentItem + 1 >= fragments.size) {
            NavigationActivity.loadFirstTime = false
            ProfileFragment.reloadCompleteProfile = true
            setResult(2001)
            finish()
        } else {
            fragmentContainer.currentItem = fragmentContainer.currentItem + 1
        }
    }

    fun previous() {
        Log.i("nav", "previous: ${fragmentContainer.currentItem}")
        if (fragmentContainer.currentItem - 1 < 0) {
            finish()
        } else {
            fragmentContainer.currentItem = fragmentContainer.currentItem - 1
        }
    }

    private fun configureViewPager() {
        fragmentContainer.adapter =
            ExplorePagerAdapter(supportFragmentManager, fragments.toTypedArray())
        //fragmentContainer.offscreenPageLimit = fragments.size
    }

    private fun loadFragment() {
        when (editType) {
            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PROFILE_ITEM -> {
                fragments.add(
                    ProfileGenderFragment(fragments = fragments, isInCompleteProfile = true)
                )

                fragments.add(
                    ProfileBirthdayFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Aniversário",
                            fieldType = FieldType.CPF,
                            capitalLetter = false,
                            name = "aniversario",
                            isMandatory = false,
                            validationError = "Número de CPF inválido"
                        ),
                        viewModel.userCompany?.user?.birthday,
                        fragments = fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileAddressFragment(
                        fragments = fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileRGFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "RG",
                            fieldType = FieldType.RG,
                            capitalLetter = false,
                            name = "RG",
                            isMandatory = false,
                            validationError = "Número de RG inválido"
                        ), viewModel.userCompany?.user?.rg,
                        fragments = fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte da frente do seu RG:",
                        "Próximo",
                        0,
                        fragments = fragments,
                        isInCompleteProfile = true
                    )
                )
                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte de trás do seu RG:",
                        "Salvar",
                        1,
                        fragments = fragments,
                        isInCompleteProfile = true
                    )
                )


            }


            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_CONTEXT_ITEM -> {

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Como que você ficou sabendo da Squad?",
                        mutableListOf<String>(
                            "Corretor",
                            "Familiar",
                            "Amigo",
                            "Redes Sociais",
                            "Outro"
                        ),
                        fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Quanto tempo você pretende investir por mês\n" +
                                "nessa oportunidade de renda complementar?",
                        mutableListOf<String>(
                            "Menos de 1 hora",
                            "Entre 1 e 3 horas",
                            "Entre 3 e 5 horas",
                            "Entre 5 e 10 horas",
                            "Mais de 10 horas"
                        ),
                        fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Quanto dinheiro você pretende ganhar por mês\n" +
                                "com essa oportunidade de renda complementar?",
                        mutableListOf<String>(
                            "Menos de R\$100",
                            "Entre R\$100 e R\$250",
                            "Entre R\$250 e R\$500",
                            "Entre R\$500 e R\$1.000",
                            "Mais de R\$1.000"
                        ),
                        fragments,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Como que você pretende gerar clientes potenciais?",
                        mutableListOf<String>(
                            "Familiares",
                            "Amigos",
                            "Colegas de Trabalho",
                            "Seguidores nas Redes Sociais",
                            "Mídia Paga",
                            "Outro"
                        ),
                        fragments,
                        isInCompleteProfile = true,
                        isMultiple = true
                    )
                )

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Como que você pretende abordar clientes potenciais?",
                        mutableListOf<String>(
                            "Pessoalmente",
                            "Telefone",
                            "Whatsapp",
                            "Facebook",
                            "Instagram",
                            "Outra"
                        ),
                        fragments,
                        isInCompleteProfile = true,
                        isMultiple = true
                    )
                )

                fragments.add(
                    ProfileContextQuestionsFragment(
                        "Você tem alguma dúvida ou sugestão\n" +
                                "sobre essa oportunidade de renda complementar?",
                        mutableListOf<String>(
                            "Familiares",
                            "Amigos",
                            "Colegas de Trabalho",
                            "Seguidores nas Redes Sociais",
                            "Mídia Paga",
                            "Outro"
                        ),
                        fragments,
                        isLast = true,
                        isInCompleteProfile = true,
                        isEditable = true
                    )
                )
            }

            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_ITEM -> {
                fragments.add(
                    ProfileCompanyTypeFragment(isInCompleteProfile = true, value = "")
                )
            }


            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_PF -> {
                fragments.add(
                    ProfilePixFormFragment(
                        "Pessoa Física",
                        "Completar Perfil",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Pix",
                            fieldType = FieldType.PIX,
                            capitalLetter = false,
                            name = "Pix",
                            isMandatory = false,
                            validationError = "Chave Pix inválida"
                        ), viewModel.userCompany?.user?.pix,
                        fragments = fragments,
                        isInCompleteProfile = true,
                        resId = R.drawable.light_mode_identity_profile_payment_information_bank_account_personal_account
                    )
                )
            }

            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_PJ -> {
                fragments.add(
                    ProfileCNPJFormFragment(
                        "Pessoa Jurídica",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "CNPJ",
                            fieldType = FieldType.CNPJ,
                            capitalLetter = false,
                            name = "CNPJ",
                            isMandatory = false,
                            validationError = "Número de CNPJ inválido"
                        ), null,
                        isInCompleteProfile = true
                    )
                )

                fragments.add(
                    ProfilePixFormFragment(
                        "Pessoa Jurídica",
                        "Completar Perfil",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Pix",
                            fieldType = FieldType.PIX,
                            capitalLetter = false,
                            name = "Pix",
                            isMandatory = false,
                            validationError = "Chave Pix inválida"
                        ), viewModel.userCompany?.user?.pix,
                        fragments = fragments,
                        isInCompleteProfile = true,
                        resId = R.drawable.light_mode_identity_profile_payment_information_bank_account_business_account
                    )
                )
            }

            ProfilePictureActivity.Companion.EDIT_TYPE.BANK_TED -> {
                fragments.add(ProfileBankChooseFragment(isInCompleteProfile = true))
                fragments.add(ProfileAccountInfoFragment(isInCompleteProfile = true))
                fragments.add(ProfileAccountConfirmationFragment(isInCompleteProfile = true))
            }


        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("nav", "requestCode: $requestCode resultCode: $resultCode")
        when (resultCode) {
            2001 -> {
                setResult(2001)
                finish()
            }
        }
    }

    companion object {
        const val EDIT_TYPE_FORM = "EDIT_TYPE_FORM"


        fun showForm(context: Context, editType: ProfilePictureActivity.Companion.EDIT_TYPE) {
            var intent = Intent(context, CompleteProfileActivity::class.java).apply {
                putExtra(EDIT_TYPE_FORM, editType)
            }
            context.startActivity(intent)
        }

    }

}