package co.socialsquad.squad.presentation.util

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

fun EditText.setOnKeyboardActionListener(callback: (view: View) -> Unit) {
    setOnEditorActionListener { _, i, keyEvent ->
        if (i == EditorInfo.IME_ACTION_GO ||
            i == EditorInfo.IME_ACTION_DONE ||
            i == EditorInfo.IME_ACTION_NEXT ||
            i == EditorInfo.IME_ACTION_SEND ||
            i == EditorInfo.IME_ACTION_SEARCH ||
            keyEvent?.action == KeyEvent.KEYCODE_ENTER
        ) {
            callback(this)
            return@setOnEditorActionListener true
        }
        false
    }
}

fun EditText.addTextChangedListener(onTextChanged: (text: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable) {}
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            onTextChanged(charSequence.toString())
        }
    })
}
