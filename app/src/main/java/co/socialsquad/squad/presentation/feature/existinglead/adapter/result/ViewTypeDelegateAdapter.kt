package co.socialsquad.squad.presentation.feature.existinglead.adapter.result

import android.view.ViewGroup
import co.socialsquad.squad.presentation.feature.existinglead.model.ExistingLeadVM

interface ViewTypeDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): ResultsViewHolder
    fun onBindViewHolder(holder: ResultsViewHolder, item: ExistingLeadVM)
}
