package co.socialsquad.squad.presentation.feature.widgetScheduler.validation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.Header
import co.socialsquad.squad.domain.model.Term
import co.socialsquad.squad.domain.model.Validation
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.widgetScheduler.di.TimetableValidationModule
import co.socialsquad.squad.presentation.feature.widgetScheduler.dialog.ConfirmSchedulerDialog
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreation
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.TimetableCreationResponse
import co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter.TermVM
import co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter.TimetableValidationAdapter
import co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter.TimetableValidationEnableButtonCallback
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_timetable_validation.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class TimetableValidationActivity : BaseActivity() {

    companion object {

        const val REQUEST_VALIDATE = 12342
        const val RESULT_CREATE_NEW_RESERVE = 111
        const val RESULT_RELOAD_RESERVE = 222
        private const val EXTRA_TIMETABLE_ID = "timetable_id"
        private const val EXTRA_OBJECT_ID = "object_id"
        private const val EXTRA_VALIDATION = "validation"
        private const val EXTRA_RESERVA = "reserva"

        fun startForResult(
                activity: Activity,
                timetableId: Long,
                objectId: Long,
                validation: Validation,
                timetableCreation: TimetableCreation
        ) {
            activity.startActivityForResult(
                    createIntent(activity, timetableId, objectId, validation, timetableCreation),
                    REQUEST_VALIDATE
            )
        }

        private fun createIntent(
                context: Context,
                timetableId: Long,
                objectId: Long,
                validation: Validation,
                timetableCreation: TimetableCreation
        ): Intent {
            return Intent(context, TimetableValidationActivity::class.java).apply {
                putExtra(EXTRA_VALIDATION, validation)
                putExtra(EXTRA_RESERVA, timetableCreation)
                putExtra(EXTRA_TIMETABLE_ID, timetableId)
                putExtra(EXTRA_OBJECT_ID, objectId)
            }
        }
    }

    override val modules = TimetableValidationModule.instance
    override val contentView = R.layout.activity_timetable_validation
    private val timetableId: Long by extra(EXTRA_TIMETABLE_ID)
    private val objectId: Long by extra(EXTRA_OBJECT_ID)
    private val validation: Validation by extra(EXTRA_VALIDATION)
    private val timetableCreation: TimetableCreation by extra(EXTRA_RESERVA)
    private val viewModel by inject<TimetableValidationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupBackButton()
        setupHeader(validation.header)
        setupTerms(validation.terms)
        setupConfirmButton(validation.button)
        setupObservableCreation()
    }

    private fun setupObservableCreation() {
        lifecycleScope.launch {
            viewModel.timetableCreation().collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        llLoading.isVisible = false
                        it.data?.let { timetableCreation ->
                            showSavedDialog(timetableCreation)
                        }
                    }
                    Status.LOADING -> {
                        llLoading.isVisible = true
                    }
                    Status.ERROR -> {
                        llLoading.isVisible = false
                        var dialog2 = ConfirmSchedulerDialog() {
                            this@TimetableValidationActivity.finish()
                        }
                        dialog2.show(supportFragmentManager, "error")
                    }
                    Status.EMPTY -> {
                        llLoading.isVisible = false
                    }
                }
            }
        }
    }

    private fun setupHeader(header: Header) {

        txtTitle.text = header.title
        txtSubtitle.text = header.subtitle
        txtHelpText.text = header.helpText

        Glide.with(this)
                .asBitmap()
                .load(header.icon.url)
                .into(imgLead)
    }

    private fun setupConfirmButton(button: String) {
        btnConfirmar.text = button
        btnConfirmar.setOnClickListener {
            viewModel.createSubmission(timetableId, objectId, timetableCreation)
        }
    }

    private fun setupTerms(terms: List<Term>) {
        rvTerms.adapter = TimetableValidationAdapter(terms.flatMap { listOf(TermVM(it)) },
                object : TimetableValidationEnableButtonCallback {
                    override fun changeButtonState(enable: Boolean) {
                        btnConfirmar.isEnabled = enable
                    }
                })
    }

    private fun setupBackButton() {
        ibClose.setOnClickListener {
            finish()
        }
    }

    private fun showSavedDialog(timetableCreationResponse: TimetableCreationResponse) {
        val dialogBuilder =
                AlertDialog.Builder(this@TimetableValidationActivity, R.style.TransparentDialog)
        val dialogView = layoutInflater.inflate(R.layout.dialog_timetable_validation_success, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val alertDialog = dialogBuilder.create()

        dialogView.findViewById<TextView>(R.id.object_type_lead).text = timetableCreationResponse.title
        dialogView.findViewById<TextView>(R.id.levelDescription).text =
                timetableCreationResponse.description

        dialogView.findViewById<TextView>(R.id.btnVisualizar).setOnClickListener {
            // alertDialog.dismiss()
        }
        dialogView.findViewById<TextView>(R.id.btnEnviarOutro).setOnClickListener {
            alertDialog.dismiss()
            this@TimetableValidationActivity.setResult(RESULT_CREATE_NEW_RESERVE)
            this@TimetableValidationActivity.finish()
        }
        dialogView.findViewById<TextView>(R.id.btnFechar).setOnClickListener {
            alertDialog.dismiss()
            this@TimetableValidationActivity.setResult(RESULT_RELOAD_RESERVE)
            this@TimetableValidationActivity.finish()
        }

        alertDialog.show()
    }
}
