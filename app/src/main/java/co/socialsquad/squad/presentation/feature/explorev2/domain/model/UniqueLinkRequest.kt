package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UniqueLinkRequest(
        @SerializedName("type") val type: String?,
        @SerializedName("exploreobject_id") val exploreobject_id: String?,
        @SerializedName("opportunity_id") val opportunity_id: String?
):Parcelable