package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters

import android.os.Parcelable

interface ViewType : Parcelable {
    fun getViewType(): Int
}
