package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.CENTER_RESIZE_MIN_SCALE
import co.socialsquad.squad.presentation.custom.CenterResizeLinearAdapter
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.SnapHelperOneByOne
import co.socialsquad.squad.presentation.custom.SpacedItemDecoration
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import kotlinx.android.synthetic.main.view_qualification_header_item.view.*

const val PROFILE_QUALIFICATION_HEADER_VIEW_HOLDER_ID = R.layout.view_qualification_header_item

class ProfileQualificationHeaderViewHolder(
    itemView: View,
    private val windowManager: WindowManager,
    private val updateCurrentQualificationListener: Listener<ProfileQualificationViewModel>
) :
    ViewHolder<ProfileQualificationHeaderViewModel>(itemView) {

    private var snapper: SnapHelperOneByOne? = null
    private var scrollListener = CurrentItemScrollListener()
    private lateinit var qualificationsAdapter: RecyclerViewAdapter
    private var centerResizeLinearAdapter: CenterResizeLinearAdapter? = null
    private var space: Long = 0L
    private var currentItemPosition: Int = 0
    private var recycledItem: Int = 0

    override fun bind(viewModel: ProfileQualificationHeaderViewModel) {
        setupQualificationsRecycler()
        setUpListItems(viewModel.qualifications)
    }

    private fun setupQualificationsRecycler() {
        qualificationsAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is ProfileQualificationViewModel -> QUALIFICATION_PROGRESS_VIEW_HOLDER_ID
                is QualificationEmptyHeaderViewModel -> QUALIFICATION_EMPTY_HEADER_VIEW_HOLDER_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                QUALIFICATION_PROGRESS_VIEW_HOLDER_ID -> ProfileQualificationViewHolder(view)
                QUALIFICATION_EMPTY_HEADER_VIEW_HOLDER_ID -> QualificationProgressEmptyHeaderViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        })
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        space = Math.round((displayMetrics.widthPixels - ((1 + (2 * CENTER_RESIZE_MIN_SCALE * 0.88)) * itemView.context.resources.getDimension(R.dimen.qualification_simple_size))) / 4)
        centerResizeLinearAdapter = CenterResizeLinearAdapter(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        itemView.rvQualifications.apply {
            adapter = qualificationsAdapter
            layoutManager = centerResizeLinearAdapter
            addItemDecoration(SpacedItemDecoration(space.toInt()))
            setItemViewCacheSize(20)
            setHasFixedSize(false)
            addOnScrollListener(scrollListener)
        }
        snapper = SnapHelperOneByOne()
        snapper?.attachToRecyclerView(itemView.rvQualifications)
    }

    private fun setUpListItems(qualifications: List<ProfileQualificationViewModel>) {
        qualificationsAdapter.setItems(listOf(QualificationEmptyHeaderViewModel()))
        val pos = if (currentItemPosition > 0) currentItemPosition - 1 else qualifications.indexOf(qualifications.firstOrNull { it.actual })
        qualifications[pos].focused = true
        qualificationsAdapter.addItems(qualifications)
        qualificationsAdapter.addItems(listOf(QualificationEmptyHeaderViewModel()))
        qualifications[pos].let {
            currentItemPosition = Math.max(qualifications.indexOf(it), 0) + 1
            scrollToActualQualification(Math.max(qualifications.indexOf(it), 0))
            updateQualificationText(it)
        }
    }

    private fun scrollToActualQualification(targetPosition: Int) {
        val offset = (itemView.context.resources.getDimension(R.dimen.qualification_simple_size) * 0.7) + space - (itemView.context.resources.getDimension(R.dimen.qualification_simple_size) * 0.11)
        centerResizeLinearAdapter?.scrollToPositionWithOffset(targetPosition + 1, offset.toInt())
    }

    private fun updateCurrentQualification(model: ProfileQualificationViewModel) {
        updateQualificationText(model)
        updateCurrentQualificationListener.onClick(model)
    }

    private fun updateQualificationText(model: ProfileQualificationViewModel) {
        itemView.tvQualificationName.text = model.name
        itemView.tvQualificationProgress.text = when {
            model.target -> itemView.context.getString(R.string.qualification_porcentage, model.progress)
            model.actual -> itemView.context.getString(R.string.qualification_progress_actual)
            model.progress > 99 -> itemView.context.getString(R.string.qualification_progress_acquired)
            else -> itemView.context.getString(R.string.qualification_progress_locked)
        }
    }

    override fun recycle() {
        itemView.rvQualifications.apply {
            onFlingListener = null
            removeItemDecorationAt(0)
            removeOnScrollListener(scrollListener)
        }
        qualificationsAdapter.removeAllFromPosition(0)
        recycledItem = currentItemPosition
    }

    private inner class CurrentItemScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                centerResizeLinearAdapter?.findFirstCompletelyVisibleItemPosition()?.let {
                    val viewModel = qualificationsAdapter.getItemByPosition(it) as? ProfileQualificationViewModel
                    viewModel?.let { model ->
                        if (currentItemPosition != it) {
                            (qualificationsAdapter.getItemByPosition(currentItemPosition) as? ProfileQualificationViewModel)?.focused = false
                            model.focused = true
                            (itemView.rvQualifications.findViewHolderForAdapterPosition(currentItemPosition) as? ProfileQualificationViewHolder)
                                ?.animate(0)
                            currentItemPosition = it
                            (itemView.rvQualifications.findViewHolderForAdapterPosition(currentItemPosition) as? ProfileQualificationViewHolder)
                                ?.animate(model.progress)
                            updateCurrentQualification(model)
                        }
                    }
                }
            }
        }
    }
}
