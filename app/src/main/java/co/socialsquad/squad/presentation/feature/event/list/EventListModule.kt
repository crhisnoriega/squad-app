package co.socialsquad.squad.presentation.feature.event.list

import dagger.Binds
import dagger.Module

@Module
interface EventListModule {
    @Binds
    fun view(eventListFragment: EventListFragment): EventListView
}
