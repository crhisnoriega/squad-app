package co.socialsquad.squad.presentation.feature.profile.edit

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.InputFilter
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.NestedScrollView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.User
import co.socialsquad.squad.presentation.InputTextTextWatchListener
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.utils.ImageRotator
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.City
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.Gender
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.Neighborhood
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.State
import co.socialsquad.squad.presentation.util.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import kotlinx.android.synthetic.main.activity_profile_edit.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.inject.Inject

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val TEMP_AVATAR_FILENAME = "avatar.jpg"

class ProfileEditActivity : BaseDaggerActivity(), ProfileEditView, InputView.OnInputListener {

    @Inject
    lateinit var profileEditPresenter: ProfileEditPresenter

    private val viewModel by viewModel<ProfileEditViewModel>()

    private var loading: AlertDialog? = null
    private var miSave: MenuItem? = null

    private var shouldReloadStates = true
    private var shouldReloadCities = true
    private var shouldReloadNeighborhoods = true


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menu.clear()
//        menuInflater.inflate(R.menu.menu_profile_edit, menu)
//        miSave = menu.findItem(R.id.profile_edit_mi_save)
//        onInput()
        return true
    }

//    @Suppress("unchecked_cast")
//    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
//        android.R.id.home -> {
//            finish()
//            true
//        }
//        R.id.profile_edit_mi_save -> {
//            itFirstName.hideKeyboard()
//            profileEditPresenter.onSaveClicked(
//                itFirstName.text!!,
//                itLastName.text!!,
//                idBirthday.calendar!!,
//                itDocument.text!!,
//                itCpf.text,
//                isGender.selectedItem as Gender,
//                isMaritalStatus.selectedItem as MaritalStatus,
//                itEmail.text!!,
//                itMobile.text!!,
//                itPhone.text,
//                itZipcode.text!!,
//                itStreet.text!!,
//                itNumber.text!!,
//                itComplement.text,
//                (isCountry as InputSpinner<Country>).selectedItem!!,
//                (isCity as InputSpinner<City>).selectedItem!!,
//                (isState as InputSpinner<State>).selectedItem!!,
//                (isNeighborhood as InputSpinner<Neighborhood>).selectedItem,
//                itBio.text!!
//            )
//            true
//        }
//        else -> super.onOptionsItemSelected(item)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i(
            "camera",
            "requestCode: $requestCode intent: ${data} picture: ${data?.getSerializableExtra("PICTURE")}"
        )
        when (requestCode) {
            1001 -> {
                data?.let {
                    var uri = Uri.fromFile(
                        it.getSerializableExtra("PICTURE") as File
                    )
                    Glide.with(this)
                        .asBitmap()
                        .load(uri)
                        .bitmapCrossFade()
                        .circleCrop()
                        .into(iiPicture.imageView as ImageView)


                    val photoFileRotated = File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        "avatar-${UUID.randomUUID().toString()}.jpg"
                    )

                    var os = BufferedOutputStream(FileOutputStream(photoFileRotated))

                    var rotated =
                        ImageRotator.rotateImage(it.getSerializableExtra("PICTURE") as File)
                    rotated.compress(Bitmap.CompressFormat.JPEG, 100, os)

                    profileEditPresenter.onResourceReady(Uri.fromFile(photoFileRotated))
                    canSave(true)
                }
            }

            REQUEST_CODE_PICK_IMAGE -> if (resultCode == RESULT_OK) {
                data?.data?.let {
                    Glide.with(this)
                        .asBitmap()
                        .load(it)
                        .bitmapCrossFade()
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                isFirstResource: Boolean
                            ) = false

                            override fun onResourceReady(
                                resource: Bitmap,
                                model: Any,
                                target: com.bumptech.glide.request.target.Target<Bitmap>,
                                dataSource: DataSource,
                                isFirstResource: Boolean
                            ): Boolean {
                                val directory = cacheDir
                                val file = File(directory, TEMP_AVATAR_FILENAME)
                                val inputStream = contentResolver.openInputStream(it)!!
                                FileUtils.createFromInputStream(file, inputStream)
                                val uri = Uri.fromFile(file)
                                profileEditPresenter.onResourceReady(uri)
                                setResult(Activity.RESULT_OK, Intent())
                                return false
                            }
                        })
                        .circleCrop()
                        .into(iiPicture.imageView as ImageView)
                }
            }
        }
    }

    override fun onDestroy() {
        profileEditPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        if (reLoad) {
            if (ProfileEditViewModel.lastUserCompany != null) {
                if (ProfileEditViewModel.lastUserCompany?.user?.bank_account_type == "PJ") {
                    company_area.visibility = View.VISIBLE
                } else {
                    company_area.visibility = View.GONE
                }
            }
            reLoad = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)

        setupToolbar()
        setupInputMasks()
        setupInputLists()
        setupKeyboardActions()
        setupInputListener()
        setupOnClickListeners()
        configureActions()

        contentScroll.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->

            Log.i("scroll", "scrollX: $scrollX scrollY: $scrollY")

            val scrollBounds = Rect()
            contentScroll.getHitRect(scrollBounds)

            if (dividerToHide.getLocalVisibleRect(scrollBounds).not()) {
                bottomHeaderDividerCollapsedObject.visibility = View.VISIBLE
                //bottomHeaderToolbar.isVisible = true
            } else {
                bottomHeaderDividerCollapsedObject.visibility = View.GONE
            }
        }

        profileEditPresenter.onCreate()
    }

    private fun configureActions() {
        saveAction.setOnClickListener {
            profileEditPresenter.onSaveClicked(
                firstName = itFirstName.text,
                lastName = itLastName.text,
                genderObj = Gender.M,
                bio = itBio.text,
                email = itEmail.text,
                mobile = itMobile.text
            )
        }

        cancelAction.setOnClickListener {
            finish()
        }

        addressLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.ADDRESS
            )
        }

        birthdayLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.BIRTHDAY
            )
        }

        genderLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.GENDER
            )
        }

        cpfLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.CPF_FORM
            )
        }

        rgLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.RG_FORM
            )
        }

        cnpjLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.CNPJ_FORM
            )
        }

        pixLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.BANK_PIX
            )
        }

        teddocLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.BANK_TED
            )
        }

        salesforceLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.SALESFORCE
            )
        }

        regimeLayout.setOnClickListener {
            ProfilePictureActivity.showForm(
                this,
                ProfilePictureActivity.Companion.EDIT_TYPE.REGIMEN
            )
        }

        if (viewModel.userCompany?.user?.bank_account_type == "PJ") {
            company_area.visibility = View.VISIBLE
        } else {
            company_area.visibility = View.GONE
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        toolbar_title.text = "Editar Perfil    "
    }


    private fun canSave(canSave: Boolean) {
        saveAction.setTextColor(ColorUtils.parse(if (canSave) "#f8a700" else "#88282828"))
    }


    private fun setupInputMasks() {
        itFirstName.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.LETTERS) }

        itLastName.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.LETTERS) }

        itMobile.editText?.let {
            MaskUtils.applyMask(it, MaskUtils.Mask.MOBILE)
            MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_PARENTHESIS_DASH_SPACE)
        }

        itBio.editText?.let {
            val filter = InputFilter.LengthFilter(140)
            it.filters = (arrayOf(filter))
        }


//        itDocument.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_UPPERCASE_LETTERS_NO_ACCENTS) }
//        itCpf.editText?.let { MaskUtils.applyMask(it, MaskUtils.Mask.CPF) }
//        itCpf.editText?.let { MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_DASH_DOT) }
//
//        itCpf.setOnInputListener(this)
//
//        itPhone.editText?.let {
//            MaskUtils.applyMask(it, MaskUtils.Mask.PHONE)
//            MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_PARENTHESIS_DASH_SPACE)
//        }
//        itZipcode.editText?.let {
//            MaskUtils.applyFilter(it, MaskUtils.Filter.NUMBERS_DASH)
//            MaskUtils.applyMask(it, MaskUtils.Mask.CEP)
//        }
    }

    @Suppress("unchecked_cast")
    private fun setupInputLists() {
//        (isGender as InputSpinner<Gender>).items = InputSpinner.ItemEnum.values(this, Gender.values())
//        (isMaritalStatus as InputSpinner<MaritalStatus>).items = InputSpinner.ItemEnum.values(this, MaritalStatus.values())
//
//        (isCountry as InputSpinner<Country>).apply {
//            items = InputSpinner.ItemEnum.values(this@ProfileEditActivity, Country.values())
//            selectedItemPosition = 0
//        }
//
//        isState.addOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                shouldReloadCities = true
//                shouldReloadNeighborhoods = true
//                isCity.clear()
//                isNeighborhood.clear()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {}
//        })
//
//        isCity.addOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                shouldReloadNeighborhoods = true
//                isNeighborhood.clear()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>) {}
//        })
    }

    private fun setupKeyboardActions() {
        itLastName.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
//        itCpf.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
//        itPhone.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
//        itComplement.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
    }

    private fun setupInputListener() {
//        setInputViews(
//            Arrays.asList(
//                iiPicture,
//                itFirstName,
//                itLastName,
//                idBirthday,
//                itDocument,
//                itCpf,
//                isGender,
//                isMaritalStatus,
//                itEmail,
//                itMobile,
//                itPhone,
//                itZipcode,
//                itStreet,
//                itNumber,
//                itComplement,
//                isCountry,
//                isCity,
//                isState,
//                isNeighborhood
//            )
//        )
    }

    @Suppress("unchecked_cast")
    private fun setupOnClickListeners() {
        iiPicture.setOnClickListener {
            val intent = Intent(this, ProfilePictureActivity::class.java).apply {
                putExtra(
                    ProfilePictureActivity.EDIT_TYPE_FORM,
                    ProfilePictureActivity.Companion.EDIT_TYPE.PICTURE
                )
            }
            startActivityForResult(intent, 1001)
        }

//        isState.setOnClickListener {
//            when {
//                shouldReloadStates -> profileEditPresenter.onStateClicked()
//                else -> isState.show()
//            }
//        }
//
//        isCity.setOnClickListener {
//            val state = (isState as InputSpinner<State>).selectedItem
//            when {
//                state == null -> return@setOnClickListener
//                shouldReloadCities -> profileEditPresenter.onCityClicked(state.code)
//                else -> isCity.show()
//            }
//        }
//
//        isNeighborhood.setOnClickListener {
//            val city = (isCity as InputSpinner<City>).selectedItem
//            when {
//                city == null -> return@setOnClickListener
//                shouldReloadNeighborhoods -> profileEditPresenter.onNeighborhoodClicked(city.code)
//                else -> isNeighborhood.show()
//            }
//        }
    }

    @Suppress("unchecked_cast")
    override fun setupInfo(user: User, bio: String?) {
        Glide.with(this)
            .asBitmap()
            .load(user.avatar ?: "")
            .circleCrop()
            .bitmapCrossFade()
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Bitmap>,
                    isFirstResource: Boolean
                ) = false

                override fun onResourceReady(
                    resource: Bitmap,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Bitmap>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    val directory = cacheDir
                    val file = File(directory, TEMP_AVATAR_FILENAME)
                    FileUtils.createFromBitmap(file, resource)

                    val uri = Uri.fromFile(file)
                    // profileEditPresenter.onResourceReady(uri)
                    return false
                }
            })
            .into(iiPicture.imageView as ImageView)

        itFirstName.text = TextUtils.toCamelcase(user.firstName)
        itLastName.text = TextUtils.toCamelcase(user.lastName)
        itBio.text = bio ?: ""
        // DateTimeUtils.parseToCalendar(user.birthday)?.let { idBirthday.calendar = it }

        val email = if (user.contactEmail != null) user.contactEmail else user.email
        itEmail.text = email
        itEmail.doClickable {

        }
        var phoneToShow = ""
        val phones = user.phones
        if (phones != null) {
            for (i in phones.indices) {
                val phone = phones[i]

                if (phone.category == "M") {
                    phoneToShow = MaskUtils.unmask(phone.number, MaskUtils.Mask.MOBILE)
                    itMobile.text = MaskUtils.mask(
                        MaskUtils.unmask(phone.number, MaskUtils.Mask.MOBILE),
                        MaskUtils.Mask.MOBILE
                    )
                } else {
                    // itPhone.text = MaskUtils.mask(MaskUtils.unmask(phone.number, MaskUtils.Mask.PHONE), MaskUtils.Mask.PHONE)
                }
            }
        }

        itFirstName.addEditTextChangedListener(CheckTextViewChanger(user.firstName ?: "") {
            canSave(it)
        })

        itLastName.addEditTextChangedListener(CheckTextViewChanger(user.lastName ?: "") {
            canSave(it)
        })

        itBio.addEditTextChangedListener(CheckTextViewChanger(bio ?: "") {
            canSave(it)
        })

        itEmail.addEditTextChangedListener(CheckTextViewChanger(user.contactEmail ?: "") {
            canSave(it)
        })

        itMobile.addEditTextChangedListener(CheckTextViewChangerUnmask(phoneToShow ?: "") {
            canSave(it)
        })
//        itDocument.text = user.rg
//        user.cpf?.let { itCpf.text = MaskUtils.mask(MaskUtils.unmask(it, MaskUtils.Mask.CPF), MaskUtils.Mask.CPF) }
//        user.gender?.let { (isGender as InputSpinner<Gender>).selectedItemPosition = Gender.values().indexOf(Gender.valueOf(it)) }
//        user.maritalStatus?.let { (isMaritalStatus as InputSpinner<MaritalStatus>).selectedItemPosition = MaritalStatus.values().indexOf(MaritalStatus.valueOf(it)) }
//        itZipcode.text = user.zipcode
//        itStreet.text = user.street
//        itNumber.text = user.number
//        itComplement.text = user.complement
//
//        user.state?.let {
//            with(isState as InputSpinner<State>) {
//                items = listOf(State(it.code, it.name, it.sigla))
//                selectedItemPosition = 0
//            }
//        }
//
//        user.city?.let {
//            with(isCity as InputSpinner<City>) {
//                items = listOf(City(it.code, it.name))
//                selectedItemPosition = 0
//            }
//        }
//
//        user.neighborhood?.let {
//            with(isNeighborhood as InputSpinner<Neighborhood>) {
//                items = listOf(Neighborhood(it.code, it.name))
//                selectedItemPosition = 0
//            }
//        }
    }

    @Suppress("unchecked_cast")
    override fun showStates(states: List<State>) {
        shouldReloadStates = false
        shouldReloadCities = true
        shouldReloadNeighborhoods = true
//        isCity.clear()
//        isNeighborhood.clear()
//        (isState as InputSpinner<State>).apply {
//            clear()
//            items = states
//            post { show() }
//        }
    }

    @Suppress("unchecked_cast")
    override fun showCities(cities: List<City>) {
        shouldReloadCities = false
        shouldReloadNeighborhoods = true
//        isNeighborhood.clear()
//        (isCity as InputSpinner<City>).apply {
//            clear()
//            items = cities
//            post { show() }
//        }
    }

    @Suppress("unchecked_cast")
    override fun showNeighborhoods(neighborhoods: List<Neighborhood>) {
        shouldReloadNeighborhoods = false
//        (isNeighborhood as InputSpinner<Neighborhood>).apply {
//            clear()
//            items = neighborhoods
//            post { show() }
//        }
    }

    override fun setSaveButtonEnabled(enabled: Boolean) {
        miSave?.isEnabled = enabled
    }

    @Suppress("unchecked_cast")
    override fun onInput() {
//        profileEditPresenter.onInput(
//            itFirstName.text,
//            itLastName.text,
//            idBirthday.calendar,
//            itDocument.text,
//            itCpf.text,
//            (isGender as InputSpinner<Gender>).selectedItem,
//            (isMaritalStatus as InputSpinner<MaritalStatus>).selectedItem,
//            itEmail.text,
//            itMobile.text,
//            itPhone.text,
//            itZipcode.text,
//            itStreet.text,
//            itNumber.text,
//            (isCountry as InputSpinner<Country>).selectedItem,
//            (isCity as InputSpinner<City>).selectedItem,
//            (isState as InputSpinner<State>).selectedItem
//        )
//
//        itFirstName.isValid(!itFirstName.isBlank)
//        itLastName.isValid(!itLastName.isBlank)
//        idBirthday.isValid(idBirthday.calendar?.let { DateTimeUtils.beforeToday(it) } == true)
//        itDocument.isValid(!itDocument.isBlank)
//        itCpf.isValid(TextUtils.isValidCPF(MaskUtils.unmask(itCpf.text!!, MaskUtils.Mask.CPF)))
//        isGender.isValid(isGender.selectedItem != null)
//        isMaritalStatus.isValid(isMaritalStatus.selectedItem != null)
//
//        itEmail.isValid(TextUtils.isValidEmail(itEmail.text))
//        itMobile.isValid(itMobile.length == 15)
//
//        itZipcode.isValid(itZipcode.length == 9)
//        itStreet.isValid(!itStreet.isBlank)
//        itNumber.isValid(!itNumber.isBlank)
//        isCountry.isValid((isCountry as InputSpinner<Country>).selectedItem != null)
//        isCity.isValid((isCity as InputSpinner<City>).selectedItem != null)
//        isState.isValid((isState as InputSpinner<State>).selectedItem != null)
    }


    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun showErrorMessageFailedToGetStates() {
        ViewUtils.showDialog(
            this,
            getString(R.string.profile_edit_error_failed_to_get_states),
            null
        )
    }

    override fun showErrorMessageFailedToGetCities() {
        ViewUtils.showDialog(
            this,
            getString(R.string.profile_edit_error_failed_to_get_cities),
            null
        )
    }

    override fun showErrorMessageFailedToGetNeighborhoods() {
        ViewUtils.showDialog(
            this,
            getString(R.string.profile_edit_error_failed_to_get_neighborhoods),
            null
        )
    }

    override fun showErrorMessageFailedToRegisterUser() {
        ViewUtils.showDialog(this, getString(R.string.profile_edit_error_failed_to_save), null)
    }

    override fun finishAfterSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    class CheckTextViewChanger(
        var originalText: String,
        var onChange: (canChange: Boolean) -> Unit
    ) : InputTextTextWatchListener() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onChange.invoke(s.toString() != originalText)

        }
    }

    class CheckTextViewChangerUnmask(
        var originalText: String,
        var onChange: (canChange: Boolean) -> Unit
    ) : InputTextTextWatchListener() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onChange.invoke(MaskUtils.unmask(s.toString(), MaskUtils.Mask.MOBILE) != originalText)

        }
    }

    companion object {
        var reLoad = false
    }
}
