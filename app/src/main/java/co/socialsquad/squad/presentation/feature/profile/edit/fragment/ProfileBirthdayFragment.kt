package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.dialog.ConfirmLeaveNoSaveDialog
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.fragment_edit_profile_birthday.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat


class ProfileBirthdayFragment(
    val title: String,
    val subtitle: String,
    val icon: String,
    val field: Field,
    var value: String?,
    var fragments: List<Fragment>? = null,
    var isInCompleteProfile: Boolean? = false
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    private var formattedValue: String? = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupContent()
        setupNextButton()
        setupSkipButton()
        setupBack()
        setupHeader()

        viewModel.saveStatus.observe(requireActivity(), Observer {
            Log.i("nav", it)
            when (it) {
                "birthday" -> {
                    viewModel.state.value = StateNav("next", true, this)
                    (activity as? CompleteProfileActivity)?.next()
                    hideLoading()
                }
            }
        })

        configureFragmentByPosition()

    }

    fun configureFragmentByPosition() {
        if (isInCompleteProfile!!) {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.light_mode_identity_profile_complete_your_profile_details))
            fragments?.forEachIndexed { index, fragment ->
                if (fragment == this) {
                    txtPostion.text = "${index + 1} de ${fragments?.size}"

                    if (index == (fragments?.size!! - 1)) {
                        btnConfirmar.text = "Salvar"
                    } else {
                        btnConfirmar.text = "Próximo"
                    }
                }
            }
            txtPostion.isVisible = true
            txtTitle.text = "Perfil"
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_birthday))
            txtTitle.text = "Aniversário"
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_birthday, container, false)

    private fun setupHeader() {
        // txtTitle.text = title
        // txtSubtitle.text = subtitle

    }

    private var wasChanged = false
    private fun setupBack() {
        ibBack.setOnClickListener {


            if (isInCompleteProfile!!) {
                (activity as? CompleteProfileActivity)?.previous()
            } else {

                if (wasChanged) {
                    var confirmDialog = ConfirmLeaveNoSaveDialog() {
                        when (it) {
                            "confirm" -> {
                                viewModel.state.postValue(StateNav("previous", true))
                                (activity as? CompleteProfileActivity)?.previous()
                            }
                        }
                    }
                    confirmDialog.show(childFragmentManager, "confirm")

                } else {
                    viewModel.state.postValue(StateNav("previous", true))
                    (activity as? CompleteProfileActivity)?.previous()
                }

            }
        }
    }


    private fun setupContent() {
        edtField.setRequiredField(field.isMandatory)
        edtField.setEditOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btnConfirmar.performClick()
            }
            false
        })

        edtField.addEditTextChangedListener(
            listener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    wasChanged = formattedValue != p0.toString()
                }

                override fun afterTextChanged(fieldValue: Editable?) {
                }
            }
        )


        edtField.onErrorAction = {
            Log.i("cal", "isValid ${it}")
            txtValidationError.text = "Data inválida"
            txtValidationError.isVisible = it.not()
            txtHelpText.isVisible = it
            btnConfirmar.isEnabled = it
        }

        edtField.setEditInputType(InputType.TYPE_NULL)
        // -> edtField.setInnerPadding(16, 5, 16, 5)
        edtField.hint = field.label



        try {
            var formattedValueTmp = value?.split(" ")?.get(0)?.trim()
            formattedValueTmp = sdfRead.format(sdfWrite.parse(formattedValueTmp))
            formattedValue = formattedValueTmp
            edtField.text = formattedValue
        } catch (e: Exception) {

        }


//        field.additionalInformation?.let {
//
//            txtSupplementary.text = it.supplementaryText
//
//            it.featuredText?.apply {
//                txtFeaturedText.text = this
//            } ?: run {
//                txtFeaturedText.isVisible = false
//            }
//        } ?: run {
//            llTxtFooter.isVisible = false
//        }


    }

    private fun checkValidField(fieldValue: String) {

        val needValidationMin: Boolean = field.validationMin != null
        val needValidationMax: Boolean = field.validationMax != null

        val validMin = !needValidationMin ||
                (needValidationMin && fieldValue.length >= field.validationMin!!)
        val validMax = !needValidationMax ||
                (needValidationMax && fieldValue.length <= field.validationMax!!)

        var validContentRequired = field.validInputType(fieldValue)

        var validated = (validMin && validMax && validContentRequired)

        field.validationError?.let {
            if (!validated) {
                txtValidationError.text = it
                txtHelpText.isVisible = false
                txtValidationError.isVisible = true
            } else {
                txtValidationError.text = ""
                txtHelpText.isVisible = true
                txtValidationError.isVisible = false
            }
        } ?: run {
            txtHelpText.isVisible = true
            txtValidationError.isVisible = false
        }

        btnConfirmar.isEnabled = validated
    }

    private fun setupSkipButton() {
    }

    override fun onStop() {
        super.onStop()

    }

    var sdfRead = SimpleDateFormat("dd/MM/yyyy")
    var sdfWrite = SimpleDateFormat("yyyy-MM-dd")

    private fun setupNextButton() {
        btnConfirmar.text = "Salvar"

        btnConfirmar.setOnClickListener {

            btnConfirmar.isEnabled = false

            showLoading()

            try {
                viewModel.saveBirthday(sdfWrite.format(sdfRead.parse(edtField.text)))
            } catch (e: Exception) {

            }
        }

    }

    private var buttonText = ""

    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
        btnConfirmar.isEnabled = true
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
