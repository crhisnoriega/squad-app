package co.socialsquad.squad.presentation.feature.ranking.viewholder

import co.socialsquad.squad.presentation.custom.ViewModel

class ItemCriteriaRankingViewModel(
        val id: String,
        val title: String,
        val description: String,
        val icon: String = "",
        val points: String,
        val isInRank: Boolean = false,
        val isFirst: Boolean = false,
        val showDetails: Boolean?
) : ViewModel {
}