package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class EventList(
    @SerializedName("data") val data: List<EventData>? = listOf(),
    @SerializedName("show_all") val show_all: Boolean?,
    var isFirst: Boolean = false,
    var isLast: Boolean = false
) : Parcelable