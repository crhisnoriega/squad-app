package co.socialsquad.squad.presentation.feature.existinglead.repository

import co.socialsquad.squad.domain.model.leadsearch.LeadSearchResponse
import kotlinx.coroutines.flow.Flow

interface ExistingLeadRepository {
    fun searchExistingLead(path: String): Flow<LeadSearchResponse>
}
