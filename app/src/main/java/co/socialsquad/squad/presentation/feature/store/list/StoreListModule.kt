package co.socialsquad.squad.presentation.feature.store.list

import dagger.Binds
import dagger.Module

@Module
interface StoreListModule {
    @Binds
    fun view(view: StoreListFragment): StoreListView
}
