package co.socialsquad.squad.presentation.feature.kickoff

import android.content.Context
import android.content.Intent
import android.webkit.CookieManager
import co.socialsquad.squad.data.database.SquadDataBase
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_STOP
import co.socialsquad.squad.data.service.ChatService
import io.branch.referral.Branch
import okhttp3.Cache
import javax.inject.Inject

class KickoffPresenter @Inject constructor(
    private val preferences: Preferences,
    private val loginRepository: LoginRepository,
    private val encrypt: SimpleWordEncrypt,
    private val cache: Cache,
    private val dataBase: SquadDataBase,
    private val branch: Branch,
    private val view: KickoffView
) {

    fun onLogoutClicked(context: Context) {
        loginRepository.userCompany = null
        loginRepository.token = null
        loginRepository.clearSharingAppDialogTimer()
        encrypt.removeAndroidKeyStoreKey()
        preferences.externalSystemPassword = null
        preferences.externalSystemPasswordIV = null
        preferences.audioList = null
        preferences.videoList = null
        val cookieManager = CookieManager.getInstance()
        cookieManager.removeAllCookies(null)
        cookieManager.flush()
        branch.logout()
        cache.evictAll()
        val intent = Intent(context, ChatService::class.java).apply {
            action = CHAT_SERVICE_ACTION_STOP
        }
        context.startService(intent)
        view.finishScreen()
        Thread(
            Runnable {
                dataBase.clearAllTables()
            }
        ).start()
    }
}
