package co.socialsquad.squad.presentation.feature.explorev2.complement.price

import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Complement
import kotlinx.android.synthetic.main.item_price_single.view.*

class PriceSingleViewHolder(
        itemView: View, var share: (component: Complement) -> Unit?
) : ViewHolder<PriceSingleViewHolderModel>
(itemView) {

    private val ivInformationIcon: ImageButton = itemView.findViewById(R.id.ivInformationIcon)
    private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
    private val tvModified: TextView = itemView.findViewById(R.id.tvModified)

    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_price_single
    }

    override fun bind(viewModel: PriceSingleViewHolderModel) {

        tvTitle.text = viewModel.price.title
        tvModified.text = viewModel.price.formattedPrice

        ivInformationIcon.isVisible = viewModel.price.lists.isNotEmpty() && viewModel.price.lists
                .size > 1
        ivInformationIcon.setOnClickListener {
            PriceListActivity.start(itemView.context, viewModel.price)
        }

        itemView.share_icon.setOnClickListener {
            share.invoke(viewModel.complement)
        }
    }

    override fun recycle() {

    }
}