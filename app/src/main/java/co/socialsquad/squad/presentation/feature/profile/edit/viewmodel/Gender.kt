package co.socialsquad.squad.presentation.feature.profile.edit.viewmodel

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.InputSpinner

enum class Gender(override val titleResId: Int) : InputSpinner.ItemEnum {
    M(R.string.gender_male), F(R.string.gender_female);

    override lateinit var title: String
}
