package co.socialsquad.squad.presentation.feature.explorev2.browser

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.activity_complements_browser_link.*
import org.koin.core.module.Module


enum class COMPLEMENT_LIST_TYPE(val title: String) {
    RESOURCES("Resources"), LINKS("Links")
}

class LinkBrowserActivity : BaseActivity() {

    companion object {

        const val LINK_URL = "LINK_URL"
        const val LINK_TITLE = "LINK_TITLE"

        fun start(context: Context,
                  title: String,
                  linkUrl: String
        ) {
            context.startActivity(Intent(context, LinkBrowserActivity::class.java).apply {
                putExtra(LINK_URL, linkUrl)
                putExtra(LINK_TITLE, title)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_complements_browser_link

    private val linkUrl: String by extra(LINK_URL)
    private val linkTitle: String by extra(LINK_TITLE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

        configureWebView()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = linkTitle
        tvLink.text = linkUrl
    }

    private fun configureObservers() {

    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                    Shimmer.AlphaHighlightBuilder()
                            .setBaseAlpha(1f)
                            .setIntensity(0f)
                            .build())
            it.stopShimmer()
            it.clearAnimation()
        }
    }


    private fun configureWebView() {
        Log.i("link2", "link $linkUrl")

        webView.settings.javaScriptEnabled = true
        webView.settings.allowContentAccess = true
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView!!.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                Log.i("link2", "newProgress $newProgress")
            }

        }

        webView.loadUrl(linkUrl)
    }

    override fun onStop() {
        super.onStop()
        Handler().postDelayed({ finish() }, 500)
        Log.i("link2", "onStop")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}