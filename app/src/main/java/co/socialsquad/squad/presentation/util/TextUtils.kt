package co.socialsquad.squad.presentation.util

import android.content.Context
import android.location.Location
import android.text.Spannable
import android.text.SpannableString
import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan
import io.github.inflationx.calligraphy3.TypefaceUtils
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

fun Long.toDurationString(): String {
    val hours = TimeUnit.MILLISECONDS.toHours(this)
    val minutes = TimeUnit.MILLISECONDS.toMinutes(this) % TimeUnit.HOURS.toMinutes(1)
    val seconds = TimeUnit.MILLISECONDS.toSeconds(this) % TimeUnit.MINUTES.toSeconds(1)
    return when {
        hours > 0 -> String.format("%d:%02d:%02d", hours, minutes, seconds)
        minutes > 9 -> String.format("%02d:%02d", minutes, seconds)
        else -> String.format("%d:%02d", minutes, seconds)
    }
}

object TextUtils {

    fun isValidEmail(email: String?): Boolean {
        return email != null &&
                email.isNotEmpty() &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isMinimalEmail(email: String?): Boolean {
        return email?.contains(".")!! && email?.contains("@")!!
    }

    fun isMinimalPhone(phone: String?): Boolean {
        val unmasked = MaskUtils.unmask(phone, MaskUtils.Mask.MOBILE)
        return unmasked.length == 11
    }

    fun isMinimalCPF(phone: String?): Boolean {
        val unmasked = MaskUtils.unmask(phone, MaskUtils.Mask.CPF)
        return unmasked.length == 11
    }


    fun isMinimalCNPJ(phone: String?): Boolean {
        val unmasked = MaskUtils.unmask(phone, MaskUtils.Mask.CNPJ)
        return unmasked.length == 14
    }

    fun isMinimalRG(phone: String?): Boolean {
        val unmasked = MaskUtils.unmask(phone, MaskUtils.Mask.RG)
        return unmasked.length == 9
    }

    fun isMinimalPIX(phone: String?): Boolean {
        return true
    }

    fun isValidPhone(phone: String?): Boolean {
        return phone != null &&
                phone.isNotEmpty() &&
                android.util.Patterns.PHONE.matcher(phone).matches() &&
                Pattern.compile("(\\(?\\d{2}\\)?\\s)?(\\d{4,5}\\-\\d{4})")
                    .matcher(phone).find()
    }

    fun isCPF(document: String): Boolean {
        if (document.isEmpty()) return false

        val numbers = document.filter { it.isDigit() }.map {
            it.toString().toInt()
        }

        if (numbers.size != 11) return false

        //repeticao
        if (numbers.all { it == numbers[0] }) return false

        //digito 1
        val dv1 = ((0..8).sumOf { (it + 1) * numbers[it] }).rem(11).let {
            if (it >= 10) 0 else it
        }

        val dv2 = ((0..8).sumOf { it * numbers[it] }.let { (it + (dv1 * 9)).rem(11) }).let {
            if (it >= 10) 0 else it
        }

        return numbers[9] == dv1 && numbers[10] == dv2
    }


    fun checkCnpj(et: String): Boolean{
        var str = et.replace("-", "").replace("/","").replace(".","")
        var calc: Int
        var num = 5
        var sum = 0
        for(x in 0..11) {
            calc = str[x].toString().toInt() * num
            sum += calc
            --num
            if(num == 1) num = 9
        }
        var rest = sum % 11
        var test = 11 - rest
        if(test < 2) test = 0
        if(test != str[12].toString().toInt()) return false
        num = 6
        sum = 0
        for(x in 0..12) {
            calc = str[x].toString().toInt() * num
            sum += calc
            --num
            if(num == 1) num = 9
        }
        rest = sum % 11
        test = 11 - rest
        if(test < 2) test = 0
        if(test != str[13].toString().toInt()) return false
        return true
    }

    fun isCNPJ(document: String): Boolean {
        if (document.isEmpty()) return false

        val numbers = document.filter { it.isDigit() }.map {
            it.toString().toInt()
        }.toMutableList()

        if (numbers.size != 14) return false

        //repeticao
        if (numbers.all { it == numbers[0] }) return false

        //digito 1
        val dv1  =  11 - (arrayOf(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2).mapIndexed { index, i ->
            i * numbers[index]
        }).sum().rem(11)
        numbers.add(dv1)

        //digito 2
        val dv2  =  11 - (arrayOf(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2).mapIndexed { index, i ->
            i * numbers[index]
        }).sum().rem(11)

        return numbers[12] == dv1 && numbers[13] == dv2
    }

    fun applyTypeface(
        context: Context,
        fontPath: String,
        text: String,
        typefacedText: String
    ): SpannableString {
        val calligraphyTypefaceSpan = CalligraphyTypefaceSpan(
            TypefaceUtils.load(context.resources.assets, fontPath)
        )
        val spannableString = SpannableString(text)
        spannableString.setSpan(
            calligraphyTypefaceSpan,
            text.indexOf(typefacedText),
            text.indexOf(typefacedText) + typefacedText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }

    fun isValidCPF(unformattedCpf: String): Boolean {

        if (unformattedCpf.length != 11 ||
            unformattedCpf == "00000000000" ||
            unformattedCpf == "99999999999"
        ) {
            return false
        }

        val digits = unformattedCpf.substring(0, 9)
        val verificationDigits = unformattedCpf.substring(9, 11)

        val verificationDigit1 = generateValidationDigit(digits)
        val verificationDigit2 = generateValidationDigit(digits + verificationDigit1)

        return verificationDigits == verificationDigit1 + verificationDigit2
    }

    private fun generateValidationDigit(digits: String): String {
        var aux = digits.length + 1
        var verificationDigit = 0
        for (i in 0 until digits.length) {
            verificationDigit += Integer.parseInt(digits.substring(i, i + 1)) * aux
            aux--
        }

        verificationDigit = 11 - verificationDigit % 11

        return if (verificationDigit > 9) "0" else verificationDigit.toString()
    }

    fun toCamelcase(word: String?): String {
        return if (word == null || word.isEmpty()) {
            ""
        } else if (word.length == 1) {
            word.toUpperCase()
        } else {
            word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
        }
    }

    fun toUnitSuffix(value: Long): String {
        val suffixes = TreeMap<Long, String>().apply {
            this[1_000L] = "k"
            this[1_000_000L] = "M"
            this[1_000_000_000L] = "G"
            this[1_000_000_000_000L] = "T"
            this[1_000_000_000_000_000L] = "P"
            this[1_000_000_000_000_000_000L] = "E"
        }
        if (value == java.lang.Long.MIN_VALUE) return toUnitSuffix(java.lang.Long.MIN_VALUE + 1)
        if (value < 0) return "-" + toUnitSuffix(-value)
        if (value < 1000) return java.lang.Long.toString(value)

        val e = suffixes.floorEntry(value)
        val divideBy = e.key
        val suffix = e.value

        val truncated = value / (divideBy!! / 100) // the number part of the output times 10
        val hasDecimal = truncated < 100000 && truncated / 100.0 != (truncated / 100).toDouble()
        return if (hasDecimal) (truncated / 100.0).toString() + suffix else (truncated / 100).toString() + suffix
    }

    fun calculateDistance(
        startLatitude: Double,
        startLongitude: Double,
        endLatitude: Double,
        endLongitude: Double
    ): Double {
        val distance = FloatArray(2)
        Location.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, distance)
        return distance[0].toDouble() / 1000
    }

    fun validateTwoWord(text: String): Boolean {
        var parts = text.split(" ")
        if (parts.size >= 1 && parts[0].length > 2 && parts[1].isNullOrBlank().not()) {
            return true
        }
        return false
    }
}
