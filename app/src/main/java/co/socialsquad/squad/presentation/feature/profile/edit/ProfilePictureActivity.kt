package co.socialsquad.squad.presentation.feature.profile.edit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.OpportunityHeader
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.form.FieldType
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.fragment.*
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_profile_picture.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class ProfilePictureActivity : BaseActivity() {
    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_profile_picture

    private val editType: EDIT_TYPE by extra(EDIT_TYPE_FORM)

    private val viewModel by viewModel<ProfileEditViewModel>()

    private var fragments = mutableListOf<Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadFragment()
        configureViewPager()

    }

    override fun onResume() {
        super.onResume()
        configureObservers()
    }

    private fun configureObservers() {
        viewModel.state.observe(this, Observer {
            Log.i("nav", "name: ${it.name} current: ${fragmentContainer.currentItem}")
            when (it.name) {
                "next" -> {
                    if (fragmentContainer.currentItem + 1 >= fragments.size) {
                        setResult(2001)
                        finish()
                    } else {
                        fragmentContainer.currentItem = fragmentContainer.currentItem + 1
                    }
                }

                "previous" -> {
                    if (fragmentContainer.currentItem - 1 < 0) {
                        finish()
                    } else {
                        fragmentContainer.currentItem = fragmentContainer.currentItem - 1
                    }
                }

                "finish" -> finish()
            }
        })
    }

    private fun configureViewPager() {
        fragmentContainer.adapter =
            ExplorePagerAdapter(supportFragmentManager, fragments.toTypedArray())
        fragmentContainer.offscreenPageLimit = fragments.size
    }

    private fun loadFragment() {
        when (editType) {

            EDIT_TYPE.PICTURE ->

                fragments.add(ProfilePictureFragment())

            EDIT_TYPE.ADDRESS ->

                fragments.add(ProfileAddressFragment(viewModel.userCompany?.user))

            EDIT_TYPE.BIRTHDAY ->

                fragments.add(
                    ProfileBirthdayFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Aniversário",
                            fieldType = FieldType.CPF,
                            capitalLetter = false,
                            name = "aniversario",
                            isMandatory = false,
                            validationError = "Número de CPF inválido"
                        ), viewModel.userCompany?.user?.birthday
                    )
                )

            EDIT_TYPE.GENDER ->

                fragments.add(ProfileGenderFragment(viewModel.userCompany?.user?.gender))

            EDIT_TYPE.CPF_FORM -> {

                fragments.add(
                    ProfileCPFFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "CPF",
                            fieldType = FieldType.CPF,
                            capitalLetter = false,
                            name = "CPF",
                            isMandatory = false,
                            validationError = "Número de CPF inválido"
                        ), viewModel.userCompany?.user?.cpf
                    )
                )
            }

            EDIT_TYPE.RG_FORM -> {

                fragments.add(
                    ProfileRGFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "RG",
                            fieldType = FieldType.RG,
                            capitalLetter = false,
                            name = "RG",
                            isMandatory = false,
                            validationError = "Número de RG inválido"
                        ), viewModel.userCompany?.user?.rg
                    )
                )

                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte da frente do seu RG:",
                        "Próximo",
                        0,
                        imageUrl = if (ProfileEditViewModel.lastUserCompany == null)
                            viewModel.userCompany?.user?.rg_front_image?.url
                        else ProfileEditViewModel.lastUserCompany?.user?.rg_front_image?.url

                    )
                )
                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte de trás do seu RG:",
                        "Salvar",
                        1,
                        imageUrl = if (ProfileEditViewModel.lastUserCompany == null)
                            viewModel.userCompany?.user?.rg_back_image?.url
                        else ProfileEditViewModel.lastUserCompany?.user?.rg_back_image?.url
                    )
                )
            }

            EDIT_TYPE.CNPJ_FORM -> {

                fragments.add(
                    ProfileCNPJFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "CNPJ",
                            fieldType = FieldType.CNPJ,
                            capitalLetter = false,
                            name = "CNPJ",
                            isMandatory = false,
                            validationError = "Número de CNPJ inválido"
                        ), viewModel.userCompany?.user?.bank_account_document
                    )
                )
            }

            EDIT_TYPE.SALESFORCE -> {

                fragments.add(
                    ProfileSalesforceFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "ID",
                            fieldType = FieldType.PIX,
                            capitalLetter = false,
                            name = "ID",
                            isMandatory = false,
                            validationError = "ID do Salesforce inválido"
                        ), viewModel.userCompany?.salesforce_id
                    )
                )
            }

            EDIT_TYPE.ACCOUNT_TYPE -> {
                fragments.add(ProfileAccountTypeFragment())
            }

            EDIT_TYPE.BANK_TED -> {
                fragments.add(ProfileAccountConfirmationFragment(isSummary = true))
                fragments.add(ProfileBankChooseFragment())
                fragments.add(ProfileAccountInfoFragment())
                fragments.add(ProfileAccountConfirmationFragment())
            }

            EDIT_TYPE.REGIMEN -> {
                fragments.add(
                    ProfileCompanyTypeFragment(
                        title = "Regime",
                        subtitle = "Qual será o regime de pagamento?",
                        iconRes = R.drawable.identity_recognition_requirements_regimem,
                        buttonName = "Salvar",
                        value = viewModel.userCompany?.user?.bank_account_type ?: ""
                    )
                )
            }

            EDIT_TYPE.BANK_PIX -> {
                fragments.add(
                    ProfilePixFormFragment(
                        "title",
                        "Salvar",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Pix",
                            fieldType = FieldType.PIX,
                            capitalLetter = false,
                            name = "Pix",
                            isMandatory = false,
                            validationError = "Chave Pix inválida"
                        ),
                        viewModel.userCompany?.user?.pix,
                        resId = R.drawable.identity_recognition_requirements_pix
                    )
                )

            }

            EDIT_TYPE.COMPLETE_YOUR_PROFILE_ITEM -> {
                fragments.add(
                    ProfileGenderFragment()
                )

                fragments.add(
                    ProfileBirthdayFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Aniversário",
                            fieldType = FieldType.CPF,
                            capitalLetter = false,
                            name = "aniversario",
                            isMandatory = false,
                            validationError = "Número de CPF inválido"
                        ), viewModel.userCompany?.user?.birthday
                    )
                )

                fragments.add(ProfileAddressFragment())
            }


            EDIT_TYPE.COMPLETE_YOUR_PAYMENT_ITEM -> {

                fragments.add(
                    ProfileRGFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "RG",
                            fieldType = FieldType.RG,
                            capitalLetter = false,
                            name = "RG",
                            isMandatory = false,
                            validationError = "Número de RG inválido"
                        ), viewModel.userCompany?.user?.rg
                    )
                )

                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte da frente do seu RG:",
                        "Próximo",
                        0
                    )
                )
                fragments.add(
                    ProfileRGPictureFragment(
                        "Tire uma foto da parte de trás do seu RG:",
                        "Salvar",
                        1
                    )
                )

                fragments.add(
                    ProfilePixFormFragment(
                        "title",
                        "subtitle",
                        "",
                        Field(
                            OpportunityHeader(title = "", subtitle = "", icon = null),
                            label = "Pix",
                            fieldType = FieldType.PIX,
                            capitalLetter = false,
                            name = "Pix",
                            isMandatory = false,
                            validationError = "Chave Pix inválida"
                        ), viewModel.userCompany?.user?.pix,
                        resId = R.drawable.identity_recognition_requirements_pix
                    )
                )
            }


        }

    }

    companion object {
        const val EDIT_TYPE_FORM = "EDIT_TYPE_FORM"

        enum class EDIT_TYPE {
            ADDRESS, PICTURE, BIRTHDAY,
            GENDER, CPF_PICTURE,
            CPF_FORM, RG_FORM, CNPJ_FORM,
            ACCOUNT_TYPE, BANK_TED, BANK_PIX,
            SALESFORCE,
            COMPLETE_YOUR_PROFILE_ITEM,
            COMPLETE_YOUR_PAYMENT_ITEM,
            COMPLETE_YOUR_CONTEXT_ITEM,
            COMPLETE_YOUR_PAYMENT_PF,
            COMPLETE_YOUR_PAYMENT_PJ,
            REGIMEN

        }

        fun showForm(context: Context, editType: EDIT_TYPE) {
            var intent = Intent(context, ProfilePictureActivity::class.java).apply {
                putExtra(EDIT_TYPE_FORM, editType)
            }
            context.startActivity(intent)
        }


    }

}