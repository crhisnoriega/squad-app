package co.socialsquad.squad.presentation.feature.recognition.data

import co.socialsquad.squad.presentation.feature.ranking.model.RankingResponse
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import co.socialsquad.squad.presentation.feature.recognition.model.PlanSummaryResponse
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface RecognitionAPI {

    @GET("/endpoints/identity/plan/")
    suspend fun fetchPlanSummary(): PlanSummaryResponse


    @GET("/endpoints/identity/plan/{plan_id}/")
    suspend fun fetchPlan(@Path("plan_id") planId: String): PlanData


    @POST("/endpoints/identity/plan/{plan_id}/accept-terms/")
    suspend fun agreeRecognitionTerms(@Path("plan_id") planId: String):  Response<Void>

    @POST("/endpoints/identity/plan/{plan_id}/refuse-terms/")
    suspend fun refuseRecognitionTerms(@Path("plan_id") planId: String): Response<Void>

}