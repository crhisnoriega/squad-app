package co.socialsquad.squad.presentation.feature.tool.di

import co.socialsquad.squad.presentation.feature.tool.ToolViewModel
import co.socialsquad.squad.presentation.feature.tool.submission.OpportunitySubmissionViewModel
import co.socialsquad.squad.presentation.feature.tool.timetable.OpportunityTimetableViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ToolModule {
    val instance = module {
        // single<ToolApi> {
        //     provideToolApi(get())
        // }
        // single<ToolsRepository>(override=true) {
        //     ToolsRepositoryImpl(api = get())
        // }
        factory {
            ToolViewModel(repository = get())
        }
        viewModel { OpportunitySubmissionViewModel(repository = get()) }
        viewModel { OpportunityTimetableViewModel(repository = get()) }
    }

    // private fun provideToolApi(retrofit: Retrofit) = retrofit.create(ToolApi::class.java)
}
