package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.item_ranking_position.view.*


class ItemScoreRecognitionViewHolder(
        itemView: View, var onClickAction: ((item: ItemScoreRecognitionViewModel) -> Unit)) : ViewHolder<ItemScoreRecognitionViewModel>(itemView) {
    override fun bind(viewModel: ItemScoreRecognitionViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }
    }

    override fun recycle() {

    }

    companion object{
        const val ITEM_SCORE_RECOGNITION_VIEW_HOLDER_ID = R.layout.item_score_recognition

    }
}