package co.socialsquad.squad.presentation.feature.search.results

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class SearchResultsModule {
    @Binds
    abstract fun view(view: SearchResultsFragment): SearchResultsView

    @Binds
    abstract fun interactor(interactor: StoreInteractorImpl): StoreInteractor
}
