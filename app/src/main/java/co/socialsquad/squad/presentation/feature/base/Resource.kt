package co.socialsquad.squad.presentation.feature.base

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> empty(): Resource<T> {
            return Resource(Status.EMPTY, null, null)
        }


        fun <T> emptyWithData(data: T): Resource<T> {
            return Resource(Status.EMPTY_WITH_DATA, data, null)
        }

        fun <T> delete(): Resource<T> {
            return Resource(Status.DELETE, null, null)
        }

        fun <T> notFound(): Resource<T> {
            return Resource(Status.NOT_FOUND, null, null)
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    EMPTY,
    EMPTY_WITH_DATA,
    LOADING,
    DELETE,
    NOT_FOUND
}
