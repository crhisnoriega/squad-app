package co.socialsquad.squad.presentation.feature.search

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder

class SearchEmptyStateViewHolder(itemView: View) : ViewHolder<SearchEmptyStateViewModel>(itemView) {
    override fun bind(viewModel: SearchEmptyStateViewModel) {}
    override fun recycle() {}
}
