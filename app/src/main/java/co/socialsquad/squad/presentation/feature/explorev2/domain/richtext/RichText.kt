package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.domain.model.Button
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichText(
    @SerializedName("button") var button: Button? = null,
    @SerializedName("items") var items: List<RichTextVO>? = null,
    var formattedItems: List<ViewType>? = null
) : Parcelable
