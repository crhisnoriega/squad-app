package co.socialsquad.squad.presentation.feature.profile.viewHolder

import android.content.Intent
import android.graphics.Typeface
import android.view.View
import co.socialsquad.squad.data.entity.Profile
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_ACTION_CALL
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_ACTION
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_QUEUE_ID
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_TYPE
import co.socialsquad.squad.presentation.feature.call.CALL_ROOM_EXTRA_USER
import co.socialsquad.squad.presentation.feature.call.CallRoomActivity
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomActivity
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomViewModel
import co.socialsquad.squad.presentation.feature.chat.chatroom.EXTRA_CHAT_ROOM
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_profile_header.view.*

class ProfileHeaderViewHolder(itemView: View, private val onEditListener: Listener<ProfileViewModel>?) : ViewHolder<ProfileViewModel>(itemView) {
    override fun bind(viewModel: ProfileViewModel) {
        with(viewModel) {
            Glide.with(itemView)
                .load(avatar)
                .circleCrop()
                .crossFade()
                .into(itemView.ivAvatar)

            //if (!qualification.isNullOrBlank()) itemView.tvQualification.text = qualification else itemView.tvQualification.visibility = View.GONE

            itemView.bEdit.typeface = Typeface.createFromAsset(itemView.context.assets, "fonts/roboto_bold.ttf")

            itemView.tvQualification.text = viewModel.firstName + " " + viewModel.lastName
            if (!role.isNullOrBlank()) itemView.tvRole.text = role else itemView.tvRole.visibility = View.GONE

            if (!bio.isNullOrBlank()) itemView.tvBio.text = bio else itemView.tvBio.visibility = View.GONE

            if (onEditListener != null) {
                itemView.bEdit.visibility = View.VISIBLE
                itemView.bEdit.setOnClickListener { onEditListener.onClick(this) }
            }

//            if (canCall == true) {
//                itemView.clButtons.visibility = View.VISIBLE
//                itemView.ibMessage.setOnClickListener { openChatRoom(viewModel) }
//                itemView.ibVoiceCall.setOnClickListener { openCall(CALL_TYPE_AUDIO, viewModel) }
//                itemView.ibVideoCall.setOnClickListener { openCall(CALL_TYPE_VIDEO, viewModel) }
//            }
        }
    }

    override fun recycle() {
        Glide.with(itemView).clear(itemView.ivAvatar)
    }

    private fun openCall(mode: String, viewModel: ProfileViewModel) {
        val profile = with(viewModel) { Profile(pk, firstName, lastName, avatar, qualification, qualificationBadge, chatId) }
        Intent(itemView.context, CallRoomActivity::class.java)
            .putExtra(CALL_ROOM_EXTRA_QUEUE_ID, viewModel.chatId)
            .putExtra(CALL_ROOM_EXTRA_ACTION, CALL_ROOM_ACTION_CALL)
            .putExtra(CALL_ROOM_EXTRA_TYPE, mode)
            .putExtra(CALL_ROOM_EXTRA_USER, profile)
            .let { itemView.context.startActivity(it) }
    }

    private fun openChatRoom(viewModel: ProfileViewModel) {
        val chatRoom = ChatRoomViewModel(
            viewModel.pk,
            "",
            "",
            viewModel.avatar,
            "${viewModel.firstName} ${viewModel.lastName}",
            viewModel.qualification,
            viewModel.qualificationBadge,
            viewModel.chatId
        )
        Intent(itemView.context, ChatRoomActivity::class.java)
            .putExtra(EXTRA_CHAT_ROOM, chatRoom)
            .let { itemView.context.startActivity(it) }
    }
}
