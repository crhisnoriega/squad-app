package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


private const val REGULAR = "regular"

sealed class ToolType(val value: String) : Parcelable {

    companion object {
        fun getByValue(value: String) = when (value) {
            else -> Regular
        }
    }

    @Parcelize
    object Regular : ToolType(REGULAR)
}
