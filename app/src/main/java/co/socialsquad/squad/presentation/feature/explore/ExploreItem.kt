package co.socialsquad.squad.presentation.feature.explore

import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.audio.list.AudioListFragment
import co.socialsquad.squad.presentation.feature.document.list.DocumentListFragment
import co.socialsquad.squad.presentation.feature.event.list.EventListFragment
import co.socialsquad.squad.presentation.feature.immobiles.ImmobilesFragment
import co.socialsquad.squad.presentation.feature.live.list.LiveListFragment
import co.socialsquad.squad.presentation.feature.store.StoreFragment
import co.socialsquad.squad.presentation.feature.store.list.StoreListFragment
import co.socialsquad.squad.presentation.feature.trip.list.TripListFragment
import co.socialsquad.squad.presentation.feature.video.list.VideoListFragment

enum class ExploreItem(
    val titleResId: Int,
    val imageResId: Int
) {
    Products(R.string.explore_item_store, R.drawable.ic_explore_products),
    Immobiles(R.string.immobiles, R.drawable.ic_immobiles),
    Events(R.string.explore_item_events, R.drawable.ic_explore_events),
    Lives(R.string.explore_item_lives, R.drawable.ic_explore_lives),
    Audios(R.string.explore_item_audio, R.drawable.ic_explore_drops),
    Videos(R.string.explore_item_videos, R.drawable.ic_explore_videos),
    Stores(R.string.explore_item_stores, R.drawable.ic_explore_stores),
    Documents(R.string.explore_item_documents, R.drawable.ic_explore_documents),
    Trips(R.string.explore_item_trips, R.drawable.ic_explore_trips);

    companion object {
        fun fragments(): Array<Fragment> {
            val fragments = ExploreItem.values().map { it.getFragment() }
            return fragments.toTypedArray()
        }
    }
}

fun ExploreItem.getFragment(): Fragment {
    return when (this) {
        ExploreItem.Products -> StoreFragment()
        ExploreItem.Immobiles -> ImmobilesFragment()
        ExploreItem.Events -> EventListFragment()
        ExploreItem.Lives -> LiveListFragment()
        ExploreItem.Audios -> AudioListFragment()
        ExploreItem.Videos -> VideoListFragment()
        ExploreItem.Stores -> StoreListFragment()
        ExploreItem.Documents -> DocumentListFragment()
        ExploreItem.Trips -> TripListFragment()
    }
}
