package co.socialsquad.squad.presentation.feature.social.details

import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.comment.CommentListView

interface PostDetailsView : CommentListView {
    fun showImagePost(postImageViewModel: PostImageViewModel, currentUserAvatar: String?)
    fun showVideoPost(postVideoViewModel: PostVideoViewModel, currentUserAvatar: String?)
    fun hideLoading()
    fun finish()
    fun showLoading()
    fun showEditMedia(postViewModel: PostViewModel)
    fun showEditLive(postViewModel: PostLiveViewModel)
    fun showMessage(message: Int, onDismissListener: (() -> Unit) = {})
    fun hideDeleteDialog()
}
