package co.socialsquad.squad.presentation.feature.points.repository

import co.socialsquad.squad.presentation.feature.points.data.PersonalPointAPI
import co.socialsquad.squad.presentation.feature.points.model.SystemData
import co.socialsquad.squad.presentation.feature.points.model.SystemPointsResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface PersonalPointsRepository {
    suspend fun fetchSystemsPoints(): Flow<SystemPointsResponse>
    suspend fun fetchSystemsPointsById(points_system_id: String): Flow<SystemData>

}

@ExperimentalCoroutinesApi
class PersonalPointsRepositoryImpl(private val personalPointAPI: PersonalPointAPI) :
    PersonalPointsRepository {
    override suspend fun fetchSystemsPoints(): Flow<SystemPointsResponse> {
        return flow {
            emit(personalPointAPI.fetchPointSystems())
        }
    }

    override suspend fun fetchSystemsPointsById(points_system_id: String): Flow<SystemData> {
        return flow {
            emit(personalPointAPI.fetchPointSystemsById(points_system_id))
        }
    }


}