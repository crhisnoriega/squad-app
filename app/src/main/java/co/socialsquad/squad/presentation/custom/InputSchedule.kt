package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.synthetic.main.view_input_schedule.view.*

class InputSchedule(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    interface Listener {
        fun onRemoveClicked()
        fun onPlayClicked()
    }

    private val glide: RequestManager by lazy { Glide.with(context) }
    var listener: InputSchedule.Listener? = null

    var imageUrl: String? = null
        set(value) {
            glide.load(value)
                .roundedCorners(context)
                .apply(RequestOptions().signature(ObjectKey(System.currentTimeMillis())))
                .into(ivDetail)
            ivDetail.visibility = View.VISIBLE
        }

    var scheduledDay = ""
        set(value) {
            tvDetailLine1.text = value
            field = value
        }

    var scheduledTime = ""
        set(value) {
            tvDetailLine2.text = value
            field = value
        }

    var isEditable = true
        set(value) {
            tvEdit.visibility = if (value) View.VISIBLE else View.GONE
            isClickable = value
            field = value
        }

    var isVideo = false
        set(value) {
            ivPlay.visibility = if (value) {
                View.VISIBLE
            } else {
                View.GONE
            }
            field = value
        }

    init {
        View.inflate(context, R.layout.view_input_schedule, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputSchedule)) {
            short_title_lead.text = getString(R.styleable.InputSchedule_name)
        }
    }

    private fun setupViews() {
        ibRemove.setOnClickListener { listener?.onRemoveClicked() }
        ivDetail.setOnClickListener { listener?.onPlayClicked() }
    }
}
