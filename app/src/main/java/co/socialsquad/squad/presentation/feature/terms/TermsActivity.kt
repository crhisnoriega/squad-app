package co.socialsquad.squad.presentation.feature.terms

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.data.entity.TermsResponse
import co.socialsquad.squad.presentation.custom.CustomDialog
import co.socialsquad.squad.presentation.custom.CustomDialogButtonConfig
import co.socialsquad.squad.presentation.custom.CustomDialogConfig
import co.socialsquad.squad.presentation.custom.CustomDialogListener
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.setGradientBackground
import kotlinx.android.synthetic.main.activity_terms.*
import javax.inject.Inject

class TermsActivity : BaseDaggerActivity(), TermsView {

    companion object {
        const val extraTerms = "extra_terms"
        const val extraCompany = "extra_company"
        const val extraRegister = "extra_register"
        const val extraPassword = "extra_password"
        const val extraSubdomain = "extra_subdomain"
    }

    var textColor: String? = null

    @Inject
    lateinit var presenter: TermsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)


        Log.i("deeplink", "intent: " + intent)

        presenter.onCreate(intent)

        intent?.hasExtra(extraTerms)?.apply {
            presenter.terms(intent.extras?.getString(extraTerms)!!)
        }

        back.setOnClickListener { finish() }

        agree.setOnClickListener {
            if (intent.hasExtra(extraRegister) && intent.hasExtra(extraPassword) && intent.hasExtra(extraSubdomain)) {
                val extras = intent.extras
                presenter.registerUser(extras?.getString(extraRegister)!!, extras.getString(extraPassword), extras?.getString(extraSubdomain)!!)
            }
        }

        setupScroll()
    }

    private fun setupScroll() {
        termsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    presenter.onTermsScrollDown()
                }
            }
        })
    }

    override fun setTerms(terms: TermsResponse.Result) {
        if (terms.items == null) terms.items = arrayListOf()
        terms.items.add(0, TermsResponse.Result.Item(null, terms.description))

        termsList.layoutManager = LinearLayoutManager(baseContext)
        val adapter = TermsAdapter(terms.items)
        termsList.adapter = adapter
        textColor?.apply { adapter.textColor(this) }
    }

    override fun setUpdatedAt(day: String, month: Int?, year: String) {
        val months = resources.getStringArray(R.array.months)
        updatedAt.text = getString(R.string.updated_at, day, months[month!!], year)
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.GONE
    }

    override fun showMessage(resId: Int) {
        ViewUtils.showDialog(this, getString(resId), DialogInterface.OnDismissListener { })
    }

    override fun moveToKickoff() {
        val intent = Intent(this, KickoffActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun setupHeader(company: Company?) {
        company?.let {

            if (it.isPublic != null && it.isPublic!!) {
                it.aboutGradient?.apply {
                    if (this.isNotEmpty()) {
                        setColorsConfiguration(this[0], this[1], it.aboutFooterBackground!!, it.textColor!!)
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }
            }
            squadHeader.visibility = View.VISIBLE
            squadHeader.bindCompany(it)
        }
    }

    override fun showDialogError(subject: Int, message: Int, dialogDescription: Int, code: String) {
        CustomDialog.newInstance(
            CustomDialogConfig(
                titleRes = R.string.product_locations_error_oops,
                descriptionRes = dialogDescription,
                dismissOnClickOutside = false,
                buttonOneConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.signup_error_dialog_button,
                    buttonTextColor = R.color.orange_yellow
                ),
                buttonTwoConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.signup_error_dialog_button_back,
                    buttonTextColor = R.color.oldlavender
                )
            )
        ).apply {
            this.listener = CustomDialogListener(
                buttonOneClick = {
                    val intent = Intent(Intent.ACTION_SENDTO).apply {
                        data = Uri.parse("mailto:")
                        putExtra(Intent.EXTRA_EMAIL, getString(R.string.squad_email))
                        putExtra(Intent.EXTRA_SUBJECT, getString(R.string.report_error))
                        putExtra(Intent.EXTRA_TEXT, getString(message, code))
                    }
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent)
                    }
                },
                buttonTwoClick = {
                    dismiss()
                    val intent = Intent(this@TermsActivity, OnboardingActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            )
        }.show(supportFragmentManager, "error_dialog")
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        this.textColor = textColor

        container.setGradientBackground(startColor, endColor)
        agreeContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        finalSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        termsLabel.setTextColor(color)
        serviceTermsLabel.setTextColor(color)
        updatedAt.setTextColor(color)
        and.setTextColor(color)

        back.setColorFilter(color)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
