package co.socialsquad.squad.presentation.feature.settings.password

import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.CustomDialog
import co.socialsquad.squad.presentation.custom.CustomDialogButtonConfig
import co.socialsquad.squad.presentation.custom.CustomDialogConfig
import co.socialsquad.squad.presentation.custom.CustomDialogListener
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import kotlinx.android.synthetic.main.activity_settings_password.*
import javax.inject.Inject

private const val PASSWORD_MINIMUM_LENGTH = 6

class SettingsPasswordActivity : BaseDaggerActivity(), SettingsPasswordView {

    @Inject
    lateinit var settingsPasswordPresenter: SettingsPasswordPresenter

    private var miDone: MenuItem? = null
    private var loading: AlertDialog? = null

    override fun onDestroy() {
        settingsPasswordPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_settings_password, menu)
        miDone = menu.findItem(R.id.settings_password_mi_done)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.settings_password_mi_done -> {
            itCurrent.hideKeyboard()
            settingsPasswordPresenter.onNextClicked(itCurrent.text!!, itNew.text!!)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_password)
        settingsPasswordPresenter.onCreate()
    }

    override fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.settings_password_title)
    }

    override fun setupKeyboardActions() {
        itConfirm.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
    }

    override fun setupInputListener() {
        object : InputView.OnInputListener {
            override fun onInput() {
                itCurrent.isValid(!itCurrent.isBlank)
                settingsPasswordPresenter.onInput(itCurrent.text, itNew.text, itConfirm.text)
            }
        }.setInputViews(listOf(itCurrent))

        object : InputView.OnInputListener {
            override fun onInput() {
                itNew.isValid(itNew.length >= PASSWORD_MINIMUM_LENGTH)
                settingsPasswordPresenter.onInput(itCurrent.text, itNew.text, itConfirm.text)
            }
        }.setInputViews(listOf(itNew))

        object : InputView.OnInputListener {
            override fun onInput() {
                itConfirm.isValid(itConfirm.length >= PASSWORD_MINIMUM_LENGTH && itNew.text == itConfirm.text)
                settingsPasswordPresenter.onInput(itCurrent.text, itNew.text, itConfirm.text)
            }
        }.setInputViews(listOf(itConfirm))
    }

    override fun setupPasswordFields() {
        listOf(itCurrent, itNew, itConfirm).forEach {
            it.editText?.apply {
                val currentTypeface = typeface
                inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                transformationMethod = PasswordTransformationMethod.getInstance()
                typeface = currentTypeface
            }
        }
    }

    override fun setDoneButtonEnabled(enabled: Boolean) {
        miDone?.isEnabled = enabled
    }

    override fun showMessageNewPasswordSet() {
        val dialog = CustomDialog.newInstance(
            CustomDialogConfig(
                titleRes = R.string.settings_password_success_title,
                descriptionRes = R.string.settings_password_success_description,
                dismissOnClickOutside = false,
                buttonOneConfig = CustomDialogButtonConfig(
                    showButton = true,
                    buttonTextRes = R.string.settings_password_success_button,
                    buttonTextColor = R.color.bright_blue
                )
            )
        ).apply {
            this.listener = CustomDialogListener(
                buttonOneClick = {
                    dismiss()
                },
                onDismiss = {
                    finish()
                }
            )
        }
        dialog.show(supportFragmentManager, "success_dialog")
    }

    override fun showErrorMessageFailedToEditPassword() {
        ViewUtils.showDialog(
            this,
            getString(R.string.settings_password_error_failed_to_edit_password),
            null
        )
    }

    override fun showLoading() {
        loading = ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }
}
