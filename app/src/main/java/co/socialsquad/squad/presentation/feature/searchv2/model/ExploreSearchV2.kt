package co.socialsquad.squad.presentation.feature.searchv2.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionList
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreSearchV2(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("error")
    var error: List<String>,
    @SerializedName("data")
    val data: SectionList
) : Parcelable
