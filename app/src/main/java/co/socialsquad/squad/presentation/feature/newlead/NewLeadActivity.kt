package co.socialsquad.squad.presentation.feature.newlead

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.SubmissionCreation
import co.socialsquad.squad.domain.model.form.Field
import co.socialsquad.squad.domain.model.widget.Widget
import co.socialsquad.squad.presentation.custom.tool.ToolWidgetViewModel
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.lead.LeadActivity
import co.socialsquad.squad.presentation.feature.newlead.di.NewLeadModule
import co.socialsquad.squad.presentation.util.extra
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class NewLeadActivity : BaseActivity() {

    companion object {

        private const val EXTRA_FORM = "form"
        private const val EXTRA_SUBMISSION = "submission"
        private const val EXTRA_WIDGET = "widget"
        private const val EXTRA_OBJECT_TYPE = "EXTRA_OBJECT_TYPE"

        fun startForResult(activity: Activity, submissionCreation: SubmissionCreation, widget: Widget, form: ArrayList<Field>, code: Int, objectType: String? = "") {
            val intent = Intent(activity, NewLeadActivity::class.java).apply {
                putExtra(EXTRA_FORM, form)
                putExtra(EXTRA_SUBMISSION, submissionCreation)
                putExtra(EXTRA_WIDGET, widget)
                putExtra(EXTRA_OBJECT_TYPE, objectType)
            }
            activity.startActivityForResult(intent, code)
        }
    }

    private val form: ArrayList<Field> by extra(EXTRA_FORM)
    private val submissionCreation: SubmissionCreation by extra(EXTRA_SUBMISSION)
    private val widget: Widget by extra(EXTRA_WIDGET)
    private val viewModel by viewModel<NewLeadSharedViewModel>()
    private val viewModelTool: ToolWidgetViewModel by viewModel()

    private val objectType: String by extra(EXTRA_OBJECT_TYPE)

    override val modules = NewLeadModule.instance
    override val contentView = R.layout.activity_new_lead

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (form.isEmpty()) {
            finish()
            return
        }

        setupObservable()
        setupCreateOpportunity()

        if (savedInstanceState == null) {
            viewModel.loadForm(submissionCreation, widget, form)
        }
    }


    private fun setupObservable() {
        lifecycleScope.launch {
            viewModel.fieldData.collect {
                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.wasConfirm = false
                        it.data?.apply {
                            supportFragmentManager
                                    .beginTransaction()
                                    .replace(
                                            R.id.container,
                                            LeadFormFragment(this.header, this, viewModel.getValue(this.name), viewModel.getSubmitText(), objectType, submissionCreation)
                                    )
                                    .commit()
                        }
                    }
                }
            }
        }
    }

    fun callNewCreate() {
        viewModelTool.createNewOpportunityJustIds(submissionCreation.objectId, submissionCreation.opportunityId)
    }

    private fun setupCreateOpportunity() {
        lifecycleScope.launch {
            viewModelTool.opportunityWidgetCreation().collect {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        it.data?.apply {
                            LeadActivity.startForResult(
                                    this@NewLeadActivity,
                                    this,
                                    LeadActivity.REQUEST_LEAD_OPTIONS
                            )
                        }
                    }
                    Status.ERROR -> {

                    }
                    Status.EMPTY -> {
                    }
                }
            }
        }
    }
}
