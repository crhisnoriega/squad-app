package co.socialsquad.squad.presentation.feature.opportunityv2.pages.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_card_opportunity_stage_pending.view.*


class OpportunityStageViewHolder(itemView: View) :
        ViewHolder<OpportunityStageViewModel>(itemView) {

    override fun bind(viewModel: OpportunityStageViewModel) {
        itemView.short_title_lead.text = viewModel.stage.title
        itemView.short_description_lead.text = viewModel.stage.description

        if (viewModel.stage.status == "completed") {
            itemView.timestamp.text = (if (viewModel.stage.timestamp.isNullOrBlank()) "nao definido" else viewModel.stage.timestamp) + " • "
        } else {
            itemView.timestamp.isVisible = false
        }

        itemView.timestamp.setTextColor(ColorUtils.parse("#939393"))
        //itemView.timestamp.text = viewModel.stage.timestamp + " • "

        itemView.commission.text = viewModel.stage.comission_formatted

        if (viewModel.stage.comission_formatted?.contains("Sem")!!.not()) {
            itemView.commission.setTextColor(ColorUtils.parse("#4dab21"))
        } else {
            itemView.commission.setTextColor(ColorUtils.parse("#939393"))
        }

        itemView.lineFirst.isVisible = viewModel.isFirst
        itemView.lineLast.isVisible = viewModel.isLast
        itemView.lineComplete.isVisible = !viewModel.isLast && !viewModel.isFirst

        viewModel.stage.issue?.let {
            itemView.tvName2.text = it.title
            itemView.tvSubTitle33.text = it.description

            if (it.actionText == null) {
                itemView.tvAction.isVisible = false
            } else {
                itemView.tvAction.text = it.actionText
            }
        }

        itemView.finalDivider?.isVisible = viewModel.isLast.not()

    }

    override fun recycle() {
    }

}