package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.os.Handler
import android.os.SystemClock
import android.util.AttributeSet
import android.view.View
import android.widget.Chronometer
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.linearlayout_live_chronometer_badge.view.*

class LiveChronometerBadge(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private val llBadge: LinearLayout
    private val cChronometer: Chronometer
    private val badgeAnimationDuration = 300L
    private val badgeAnimationDelayDuration = 3000L
    private val handlerHideTimer = Handler()
    private val runnableHideTimer = {
        animate(measuredCollapsedWidth)
        expanded = false
    }
    private val delayHideTimer = badgeAnimationDelayDuration + badgeAnimationDuration
    private val measuredCollapsedWidth: Int
        get() {
            val currentVisibility = cChronometer.visibility
            cChronometer.visibility = View.GONE
            llBadge.measure(LayoutParams.WRAP_CONTENT, llBadge.height)
            val collapsedWidth = llBadge.measuredWidth
            cChronometer.visibility = currentVisibility
            return collapsedWidth
        }
    private var animation: ResizeWidthAnimation? = null
    private var expanded = true
    private var expandedWidth = 0
    private var chronometerWidth = 0
    private var previousText = ""

    val text get() = cChronometer.text.toString()

    private fun animate(width: Int) {
        animation?.cancel()
        animation = ResizeWidthAnimation(llBadge, width)
        animation?.duration = badgeAnimationDuration
        llBadge.startAnimation(animation)
    }

    private fun updateExpandedWidth() {
        val currentText = cChronometer.text

        cChronometer.text = when (currentText.length) {
            7 -> "0:00:00"
            8 -> "00:00:00"
            else -> "00:00"
        }

        if (currentText.length > previousText.length) {
            cChronometer.measure(LayoutParams.WRAP_CONTENT, cChronometer.height)
            cChronometer.measuredWidth.let { if (it > chronometerWidth) chronometerWidth = it }
            cChronometer.layoutParams.width = chronometerWidth
        }

        llBadge.measure(LayoutParams.WRAP_CONTENT, llBadge.height)
        llBadge.measuredWidth.let {
            if (it > expandedWidth) {
                expandedWidth = it
                if (expanded) animate(expandedWidth)
            }
        }

        previousText = currentText.toString()
        cChronometer.text = currentText
    }

    init {
        View.inflate(context, R.layout.linearlayout_live_chronometer_badge, this) as LinearLayout

        llBadge = live_chronometer_badge_ll_badge
        cChronometer = live_chronometer_badge_c_chronometer

        val styledAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.LiveChronometerBadge)

        val hideChronometer = styledAttributes.getBoolean(R.styleable.LiveChronometerBadge_hideChronometer, false)
        if (hideChronometer) {
            cChronometer.visibility = View.GONE
        } else {
            cChronometer.visibility = View.VISIBLE
            updateExpandedWidth()
            animate(measuredCollapsedWidth)
        }

        styledAttributes.recycle()
    }

    fun start(time: Long?) {
        with(cChronometer) {
            base = time?.let { it - System.currentTimeMillis() + SystemClock.elapsedRealtime() } ?: SystemClock.elapsedRealtime()
            setOnChronometerTickListener { updateExpandedWidth() }
            animate(expandedWidth)
            start()
        }

        handlerHideTimer.postDelayed(runnableHideTimer, badgeAnimationDelayDuration)

        llBadge.setOnClickListener {
            handlerHideTimer.removeCallbacksAndMessages(null)
            animation?.cancel()
            expanded = if (expanded) {
                animate(measuredCollapsedWidth)
                false
            } else {
                animate(expandedWidth)
                handlerHideTimer.postDelayed(runnableHideTimer, delayHideTimer)
                true
            }
        }
    }

    fun stop() {
        llBadge.setOnClickListener(null)
        cChronometer.stop()
    }
}
