package co.socialsquad.squad.presentation.util

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateUtils
import android.util.Log
import androidx.room.TypeConverter
import co.socialsquad.squad.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Date.elapsedTime(context: Context, asUnit: Boolean = false): String {

    var diff = Date().time - this.time

    val minuteInMillis = DateUtils.MINUTE_IN_MILLIS
    val hourInMillis = DateUtils.HOUR_IN_MILLIS
    val dayInMillis = DateUtils.DAY_IN_MILLIS
    val weekInMillis = DateUtils.WEEK_IN_MILLIS
    val monthInMillis = DateUtils.DAY_IN_MILLIS * 30
    val yearInMillis = DateUtils.YEAR_IN_MILLIS + dayInMillis

    var elapsedYears = diff / yearInMillis
    diff %= yearInMillis

    var elapsedMonths = diff / monthInMillis
    diff %= monthInMillis

    var elapsedWeeks = diff / weekInMillis
    diff %= weekInMillis

    var elapsedDays = diff / dayInMillis
    diff %= dayInMillis

    var elapsedHours = diff / hourInMillis
    diff %= hourInMillis

    var elapsedMinutes = diff / minuteInMillis

    elapsedYears = Math.abs(elapsedYears)
    elapsedMonths = Math.abs(elapsedMonths)
    elapsedWeeks = Math.abs(elapsedWeeks)
    elapsedDays = Math.abs(elapsedDays)
    elapsedHours = Math.abs(elapsedHours)
    elapsedMinutes = Math.abs(elapsedMinutes)

    return when {
        elapsedYears > 0 ->
            if (elapsedYears > 1) context.getString(if (asUnit) R.string.elapsed_time_years_ago_unit else R.string.elapsed_time_years_ago, elapsedYears)
            else context.getString(if (asUnit) R.string.elapsed_time_year_ago_unit else R.string.elapsed_time_year_ago)
        elapsedMonths > 0 ->
            if (elapsedMonths > 1) context.getString(if (asUnit) R.string.elapsed_time_months_ago_unit else R.string.elapsed_time_months_ago, elapsedMonths)
            else context.getString(if (asUnit) R.string.elapsed_time_month_ago_unit else R.string.elapsed_time_month_ago)
        elapsedWeeks > 0 ->
            if (elapsedWeeks > 1) context.getString(if (asUnit) R.string.elapsed_time_weeks_ago_unit else R.string.elapsed_time_weeks_ago, elapsedWeeks)
            else context.getString(if (asUnit) R.string.elapsed_time_week_ago_unit else R.string.elapsed_time_week_ago)
        elapsedDays > 0 ->
            if (elapsedDays > 1) context.getString(if (asUnit) R.string.elapsed_time_days_ago_unit else R.string.elapsed_time_days_ago, elapsedDays)
            else context.getString(if (asUnit) R.string.elapsed_time_day_ago_unit else R.string.elapsed_time_day_ago)
        elapsedHours > 0 ->
            if (elapsedHours > 1) context.getString(if (asUnit) R.string.elapsed_time_hours_ago_unit else R.string.elapsed_time_hours_ago, elapsedHours)
            else context.getString(if (asUnit) R.string.elapsed_time_hour_ago_unit else R.string.elapsed_time_hour_ago)
        elapsedMinutes > 0 ->
            if (elapsedMinutes > 1) context.getString(if (asUnit) R.string.elapsed_time_minutes_ago_unit else R.string.elapsed_time_minutes_ago, elapsedMinutes)
            else context.getString(if (asUnit) R.string.elapsed_time_minute_ago_unit else R.string.elapsed_time_minute_ago)
        else -> context.getString(if (asUnit) R.string.elapsed_time_just_now_unit else R.string.elapsed_time_just_now)
    }
}

@SuppressLint("SimpleDateFormat")
fun String.toDate(): Date? {
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")

    return try {
        simpleDateFormat.parse(this)
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

fun Date.toUTC() = Date(DateTimeUtils.toUTC(time, TimeZone.getDefault()))

fun Date.toLocal() = Date(DateTimeUtils.toLocalTime(time, TimeZone.getDefault()))

fun Long.toDate(): Date = Calendar.getInstance().apply { timeInMillis = this@toDate }.time

object DateTimeUtils {

    fun afterNow(chosenDay: Calendar, chosenTime: Date): Boolean {
        val chosenTimeCalendar = Calendar.getInstance()
        chosenTimeCalendar.time = chosenTime
        chosenDay.set(Calendar.HOUR_OF_DAY, chosenTimeCalendar.get(Calendar.HOUR_OF_DAY))
        chosenDay.set(Calendar.AM_PM, chosenTimeCalendar.get(Calendar.AM_PM))
        chosenDay.set(Calendar.HOUR, chosenTimeCalendar.get(Calendar.HOUR))
        chosenDay.set(Calendar.MINUTE, chosenTimeCalendar.get(Calendar.MINUTE))
        chosenDay.set(Calendar.SECOND, chosenTimeCalendar.get(Calendar.SECOND))
        val now = Calendar.getInstance()
        return chosenDay.after(now)
    }

    fun beforeToday(calendar: Calendar): Boolean {
        return truncateToDay(calendar).before(truncateToDay(Calendar.getInstance()))
    }

    fun afterToday(calendar: Calendar): Boolean {
        return truncateToDay(calendar).after(truncateToDay(Calendar.getInstance()))
    }

    fun beforeYesterday(calendar: Calendar): Boolean {
        val yesterday = Calendar.getInstance()
        yesterday.timeInMillis -= DateUtils.DAY_IN_MILLIS
        return truncateToDay(calendar).before(truncateToDay(yesterday))
    }

    fun isToday(calendar: Calendar): Boolean {
        return DateUtils.isToday(calendar.timeInMillis)
    }

    fun isYesterday(calendar: Calendar): Boolean {
        return DateUtils.isToday(calendar.timeInMillis + DateUtils.DAY_IN_MILLIS)
    }

    private fun truncateToDay(calendar: Calendar): Calendar {
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar
    }

    @SuppressLint("SimpleDateFormat")
    fun format(calendar: Calendar): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        return simpleDateFormat.format(calendar.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun parseToCalendar(date1: String?): Calendar? {
        if (date1 == null) return null

        var date = date1.split("T")[0]

        Log.i("deeplink", "date: " + date)

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        try {
            val d = simpleDateFormat.parse(date)
            return toCalendar(d)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

    private fun toCalendar(date: Date): Calendar {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar
    }

    fun toLocalTime(time: Long, to: TimeZone): Long {
        return convertTime(time, TimeZone.getTimeZone("UTC"), to)
    }

    fun toUTC(time: Long, from: TimeZone): Long {
        return convertTime(time, from, TimeZone.getTimeZone("UTC"))
    }

    fun convertTime(time: Long, from: TimeZone, to: TimeZone): Long {
        return time + getTimeZoneOffset(time, from, to)
    }

    private fun getTimeZoneOffset(time: Long, from: TimeZone, to: TimeZone): Long {
        var fromOffset = from.getOffset(time)
        var toOffset = to.getOffset(time)
        var diff = 0L

        if (fromOffset >= 0) {
            if (toOffset > 0) {
                toOffset *= -1
            } else {
                toOffset = Math.abs(toOffset)
            }
            diff = (fromOffset + toOffset) * -1L
        } else {
            if (toOffset <= 0) {
                toOffset = -1 * Math.abs(toOffset)
            }
            diff = (Math.abs(fromOffset) + toOffset.toLong())
        }
        return diff
    }

    @TypeConverter
    @JvmStatic
    fun toOffsetDateTime(value: Long): Date {
        return Date(value)
    }

    @TypeConverter
    @JvmStatic
    fun fromOffsetDateTime(date: Date): Long {
        return date.time
    }
}
