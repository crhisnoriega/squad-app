package co.socialsquad.squad.presentation.feature.feedback

interface FeedbackView {
    fun showLoading()
    fun hideLoading()
    fun setSection(sectionViewModel: FeedbackSectionViewModel)
    fun setQuestion(questionViewModel: FeedbackQuestionViewModel, questionNumber: Int, numberOfQuestions: Int, shouldDisableNextButton: Boolean)
    fun showMessage(message: Int, onDismissListener: () -> Unit)
    fun close()
    fun enableNextButton()
    fun disableNextButton()
    fun updateItem(viewModel: FeedbackItemViewModel)
    fun showFinishButton()
    fun finishView()
    fun changeBackButtonIcon()
    fun changeCloseButtonIcon()
}
