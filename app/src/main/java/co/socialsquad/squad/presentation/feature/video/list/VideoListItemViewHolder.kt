package co.socialsquad.squad.presentation.feature.video.list

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.profile.qualification.recommendations.QualificationRecommendationsActivity.Companion.REQUEST_CODE_VIDEO_POSITION
import co.socialsquad.squad.presentation.feature.social.SocialSpinnerAdapter
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.toDate
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_video_list_item.view.*

class VideoListItemViewHolder(
    itemView: View,
    private val onVideoClickedListener: Listener<VideoListItemViewModel>,
    private val onRecommendListener: Listener<VideoListItemViewModel>
) : ViewHolder<VideoListItemViewModel>(itemView) {
    override fun bind(viewModel: VideoListItemViewModel) {
        with(viewModel) model@{
            itemView.apply {
                Glide.with(this)
                    .load(thumbnailUrl)
                    .roundedCorners(context)
                    .crossFade()
                    .into(ivThumbnail)

                tvDuration.text = durationMillis.toDurationString()

                val progress = ((viewModel.position.toFloat() / durationMillis.toFloat()) * 100).toInt()
                pbVideo.progress = progress
                tvProgress.text = "$progress%"

                if (progress > 0) {
                    pbVideo.visibility = View.VISIBLE
                    tvProgress.visibility = View.VISIBLE
                } else {
                    pbVideo.visibility = View.GONE
                    tvProgress.visibility = View.GONE
                }

                if (!title.isNullOrBlank()) {
                    tvTitle.visibility = View.VISIBLE
                    tvTitle.text = title
                } else {
                    tvTitle.visibility = View.GONE
                }

                if (!description.isNullOrBlank()) {
                    tvDescription.visibility = View.VISIBLE
                    tvDescription.text = description
                } else {
                    tvDescription.visibility = View.GONE
                }

                tvAuthor.apply {
                    text = authorName
                    setOnClickListener {
                        val intent = Intent(context, UserActivity::class.java).apply {
                            putExtra(USER_PK_EXTRA, authorPk)
                        }
                        context.startActivity(intent)
                    }
                }

                val viewCountStringResId =
                    if (viewCount == 1) R.string.view_count_singular
                    else R.string.view_count_plural
                val viewCount = context.getString(viewCountStringResId, TextUtils.toUnitSuffix(viewCount.toLong()))
                val timestamp = timestamp.toDate()?.elapsedTime(context)?.capitalize() ?: ""
                @SuppressLint("SetTextI18n")
                tvViewCountAndTimestamp.text = "$viewCount • $timestamp"
                tvViewCountAndTimestamp.setOnClickListener {
                    val intent = Intent(itemView.context, UserListActivity::class.java).apply {
                        putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                        putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.FEED_VISUALIZATION)
                    }
                    itemView.context.startActivity(intent)
                }

                setOnClickListener {
                    onVideoClickedListener.onClick(this@model)

                    val intent = Intent(context, VideoActivity::class.java)
                    intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, videoUrl)
                    intent.putExtra(VideoActivity.EXTRA_PLAYBACK_POSITION, position)
                    (context as? Activity)?.startActivityForResult(intent, REQUEST_CODE_VIDEO_POSITION)
                }
                setupOptions(this@model)
            }
        }
    }

    private fun setupOptions(viewModel: VideoListItemViewModel) {
        itemView.apply {
            val options = object : ArrayList<Int>() {
                init {
                    if (viewModel.canRecommend) add(R.string.social_post_option_recommend)
                }
            }

            sOptions.visibility = if (options.isEmpty()) View.GONE else View.VISIBLE
            sOptions.adapter = SocialSpinnerAdapter(
                context,
                R.layout.textview_spinner_dropdown_options_item,
                R.layout.textview_spinner_dropdown_options,
                options
            )

            sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    val item = parent.getItemAtPosition(position) ?: return

                    val itemResId = item as Int
                    when (itemResId) {
                        R.string.social_post_option_recommend -> onRecommendListener.onClick(viewModel)
                    }
                    sOptions.setSelection(0)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
    }

    override fun recycle() {
        itemView.apply { Glide.with(this).clear(ivThumbnail) }
    }
}
