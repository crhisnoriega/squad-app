package co.socialsquad.squad.presentation.feature.profile.edit.corporate.hinode.confirmation

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileEditHinodeConfirmationModule {
    @Binds
    abstract fun view(profileEditHinodeConfirmationActivity: ProfileEditHinodeConfirmationActivity): ProfileEditHinodeConfirmationView
}
