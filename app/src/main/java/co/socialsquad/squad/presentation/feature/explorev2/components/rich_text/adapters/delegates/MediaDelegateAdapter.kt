package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.multimedia.MultiMediaPager
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.feature.explorev2.domain.richtext.RichTextMedia
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_rich_text_media.view.*

class MediaDelegateAdapter(val lifecycleOwner: LifecycleOwner) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MediaDelegateAdapter(parent, lifecycleOwner)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as MediaDelegateAdapter
        holder.bind(item as RichTextMedia)
    }

    private inner class MediaDelegateAdapter(parent: ViewGroup, val lifecycleOwner: LifecycleOwner) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_rich_text_media)
    ) {

        val mPager: MultiMediaPager = itemView.viewPager

        fun bind(richTextObj: RichTextMedia) = richTextObj.list?.let { mPager.bind(it, lifecycleOwner) }
    }
}
