package co.socialsquad.squad.presentation.feature.explorev2.sharing.customview


import android.app.Dialog
import android.graphics.Outline
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import co.socialsquad.squad.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_facebook_share_type.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class FacebookSharingTypeBottomSheetDialog(private val onSelect: (type: String) -> Unit) : BottomSheetDialogFragment() {

    var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null

    override fun getTheme(): Int = R.style.FarmBlocks_BottomSheetDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_facebook_share_type, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        }
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTopBordersRadius()
        super.onViewCreated(view, savedInstanceState)
        expandDialog()

        feedShare.setOnClickListener {
            onSelect.invoke("feed")
            dismiss()
        }

        messageShare.setOnClickListener {
            onSelect.invoke("message")
            dismiss()
        }
    }


    override fun onPause() {
        isShowing = false
        super.onPause()
    }

    override fun onResume() {
        isShowing = true
        super.onResume()
    }


    override fun dismiss() {
        isShowing = false
        super.dismiss()
    }

    override fun onDestroy() {
        isShowing = false
        super.onDestroy()
    }

    private fun setTopBordersRadius() {
        val radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)
        val container = view as ViewGroup

        container.apply {
            clipToOutline = true
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    if (view != null) outline?.setRoundRect(0, 0, view.width, (view.height + 300).toInt(), radius)
                }
            }
        }
    }

    fun expandDialog() {
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(300)
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            super.show(manager, tag)
        }
    }

    companion object {
        var isShowing = false
    }

}