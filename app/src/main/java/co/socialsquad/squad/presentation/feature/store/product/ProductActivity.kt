package co.socialsquad.squad.presentation.feature.store.product

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.searchv2.SearchActivity
import co.socialsquad.squad.presentation.feature.store.EXTRA_DETAIL_GROUP
import co.socialsquad.squad.presentation.feature.store.FOOTER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.FooterViewModel
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.PRODUCT_DETAIL_GROUPS_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_DISABLED_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_LIST_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_NETWORK_ERROR_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_NOT_AVAILABLE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_LOCATION_NO_PERMISSION_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.PRODUCT_OVERVIEW_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupViewModel
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupsViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationDisabledViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationListViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationLoadingViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNetworkErrorViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNoPermissionViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNotAvailableViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationSeeAllViewModel
import co.socialsquad.squad.presentation.feature.store.ProductOverviewViewModel
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.feature.store.product.details.ProductDetailGroupActivity
import co.socialsquad.squad.presentation.feature.store.product.locations.EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL
import co.socialsquad.squad.presentation.feature.store.product.locations.ProductLocationsActivity
import co.socialsquad.squad.presentation.feature.store.viewholder.FooterViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductDetailGroupsViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationDisabledViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationListViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationNetworkErrorViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationNoPermissionViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductLocationNotAvailableViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductOverviewViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.createLocationRequest
import co.socialsquad.squad.presentation.util.intentDial
import co.socialsquad.squad.presentation.util.isLocationServiceEnabled
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_product.*
import javax.inject.Inject

class ProductActivity : BaseDaggerActivity(), ProductView {

    @Inject
    lateinit var productPresenter: ProductPresenter

    private lateinit var rvMedia: RecyclerView

    private val glide by lazy { Glide.with(this) }
    private var adapter: RecyclerViewAdapter? = null

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_product, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.product_mi_search -> {
            startActivity(Intent(this, SearchActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        productPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        productPresenter.onRequestPermissionsResult(grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        rvMedia = product_rv_media

        setupToolbar()
        productPresenter.onViewCreated(intent)
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.product_title)
    }

    override fun setupTitle(title: String) {
        this.title = title
    }

    override fun setupList(color: String?) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ProductOverviewViewModel -> PRODUCT_OVERVIEW_VIEW_MODEL_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is ProductLocationLoadingViewModel -> PRODUCT_LOCATION_LOADING_VIEW_MODEL_ID
                is ProductDetailGroupsViewModel -> PRODUCT_DETAIL_GROUPS_VIEW_MODEL_ID
                is ProductLocationNoPermissionViewModel -> PRODUCT_LOCATION_NO_PERMISSION_VIEW_MODEL_ID
                is ProductLocationDisabledViewModel -> PRODUCT_LOCATION_DISABLED_VIEW_MODEL_ID
                is ProductLocationNotAvailableViewModel -> PRODUCT_LOCATION_NOT_AVAILABLE_VIEW_MODEL_ID
                is ProductLocationNetworkErrorViewModel -> PRODUCT_LOCATION_NETWORK_ERROR_VIEW_MODEL_ID
                is ProductLocationListViewModel -> PRODUCT_LOCATION_LIST_VIEW_MODEL_ID
                is FooterViewModel -> FOOTER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PRODUCT_OVERVIEW_VIEW_MODEL_ID -> {
                    val holder = ProductOverviewViewHolder(view, color)
                    lifecycle.addObserver(holder)
                    holder
                }
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                PRODUCT_LOCATION_LOADING_VIEW_MODEL_ID -> ProductLocationLoadingViewHolder<Nothing>(view)
                PRODUCT_DETAIL_GROUPS_VIEW_MODEL_ID -> ProductDetailGroupsViewHolder(view, onDetailGroupClicked)
                PRODUCT_LOCATION_NO_PERMISSION_VIEW_MODEL_ID -> ProductLocationNoPermissionViewHolder(view, onProductLocationNoPermissionClicked)
                PRODUCT_LOCATION_DISABLED_VIEW_MODEL_ID -> ProductLocationDisabledViewHolder(view, onProductLocationDisabledClicked)
                PRODUCT_LOCATION_NOT_AVAILABLE_VIEW_MODEL_ID -> ProductLocationNotAvailableViewHolder(view)
                PRODUCT_LOCATION_NETWORK_ERROR_VIEW_MODEL_ID -> ProductLocationNetworkErrorViewHolder(view, onProductLocationNetworkErrorClicked)
                PRODUCT_LOCATION_LIST_VIEW_MODEL_ID -> ProductLocationListViewHolder(view, glide, color, onProductLocationListClicked)
                FOOTER_VIEW_MODEL_ID -> FooterViewHolder(view, onFooterClicked)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                else -> throw IllegalArgumentException()
            }
        }

        adapter = RecyclerViewAdapter(viewHolderFactory)
        rvMedia.apply {
            adapter = this@ProductActivity.adapter
            setItemViewCacheSize(10)
            setHasFixedSize(true)
        }
    }

    override fun checkPermissions(permissions: Array<String>) =
        permissions.all { ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED }

    override fun requestPermissions(requestCode: Int, permissions: Array<String>) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }

    private val onDetailGroupClicked = object : ViewHolder.Listener<ProductDetailGroupViewModel> {
        override fun onClick(viewModel: ProductDetailGroupViewModel) {
            val intent = Intent(this@ProductActivity, ProductDetailGroupActivity::class.java)
            intent.putExtra(EXTRA_DETAIL_GROUP, viewModel)
            startActivity(intent)
        }
    }

    private val onProductLocationNoPermissionClicked = object : ViewHolder.Listener<ProductLocationNoPermissionViewModel> {
        override fun onClick(viewModel: ProductLocationNoPermissionViewModel) {
            productPresenter.onRequestPermissionsClicked()
        }
    }

    private val onProductLocationDisabledClicked = object : ViewHolder.Listener<ProductLocationDisabledViewModel> {
        override fun onClick(viewModel: ProductLocationDisabledViewModel) {
            productPresenter.onLocationDisabledClicked()
        }
    }

    private val onProductLocationNetworkErrorClicked = object : ViewHolder.Listener<ProductLocationNetworkErrorViewModel> {
        override fun onClick(viewModel: ProductLocationNetworkErrorViewModel) {
            productPresenter.onNetworkErrorClicked()
        }
    }

    private val onProductLocationListClicked = object : ViewHolder.Listener<StoreViewModel> {
        override fun onClick(viewModel: StoreViewModel) {
            productPresenter.onOpenPhoneClicked()
            viewModel.phone?.let { intentDial(it) }
        }
    }

    private val onFooterClicked = object : ViewHolder.Listener<FooterViewModel> {
        override fun onClick(viewModel: FooterViewModel) {
            when (viewModel) {
                is ProductLocationSeeAllViewModel -> {
                    val intent = Intent(this@ProductActivity, ProductLocationsActivity::class.java)
                    intent.putExtra(EXTRA_PRODUCT_LOCATION_LIST_VIEW_MODEL, viewModel.list)
                    startActivity(intent)
                }
            }
        }
    }

    override fun setItems(items: ArrayList<ViewModel>) {
        adapter?.setItems(items)
    }

    override fun showLoading() {
        adapter?.startLoading()
    }

    override fun hideLoading() {
        adapter?.stopLoading()
    }

    override fun showMessage(resId: Int, onDismissListener: () -> Unit) {
        ViewUtils.showDialog(this, getString(resId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun createLocationRequest() {
        createLocationRequest { request, intent ->
            productPresenter.onLocationRequestCreated(request, intent)
        }
    }

    override fun verifyLocationServiceAvailability() =
        isLocationServiceEnabled { productPresenter.onLocationServiceAvailability(it) }
}
