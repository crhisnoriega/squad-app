package co.socialsquad.squad.presentation.feature.social.create.media

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputAudience
import co.socialsquad.squad.presentation.custom.InputSchedule
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.schedule.CreateScheduleActivity
import co.socialsquad.squad.presentation.feature.schedule.CreateSchedulePresenter
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.SocialCreateSegmentActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_POTENTIAL_REACH
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsActivity
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setOnKeyboardActionListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_social_create_media.*
import java.io.File
import java.util.ArrayList
import java.util.Calendar
import java.util.TimeZone
import javax.inject.Inject

private const val REQUEST_CODE_AUDIENCE = 5
private const val REQUEST_CODE_SCHEDULE = 6

class SocialCreateMediaActivity : BaseDaggerActivity(), SocialCreateMediaView {

    companion object {
        const val EXTRA_TYPE = "EXTRA_TYPE"
        const val EXTRA_POST = "EXTRA_POST"
        const val TYPE_IMAGE = 0
        const val TYPE_VIDEO = 1
    }

    @Inject
    lateinit var socialCreateMediaPresenter: SocialCreateMediaPresenter

    private var miPost: MenuItem? = null
    private var miPostTitle: Int = R.string.social_create_media_button_post
    private var metrics: MetricsViewModel? = null
    private var members: IntArray? = null
    private var downline = false
    private var scheduledDate: Long? = null
    private var segmentResultRequestCode: Int? = null
    private var segmentResultResultCode: Int? = null
    private var segmentResultData: Intent? = null
    private var loading: AlertDialog? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) return

        segmentResultRequestCode = requestCode
        segmentResultResultCode = resultCode
        segmentResultData = data

        members = null
        downline = false
        metrics = null

        when (requestCode to resultCode) {
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_PUBLIC -> {
                setAudienceAsPublic()
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_DOWNLINE -> {
                data?.apply {
                    downline = true
                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    potentialReach?.let { reach ->
                        val membersText =
                            if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                            else getString(R.string.social_create_segment_item_description_member)

                        iAudience.visibility = View.GONE
                        with(iaAudience) {
                            visibility = View.VISIBLE
                            isEditable = false
                            audience = getString(R.string.audience_downline)
                            audienceImage = R.drawable.ic_audience_detail_downline
                            detail1 = getString(R.string.audience_potential_reach)
                            detail2 = membersText
                            imageResId = R.drawable.ic_audience_detail_metrics_reach
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_METRIC -> {
                data?.apply {
                    metrics = data.getSerializableExtra(EXTRA_METRICS) as? MetricsViewModel
                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    metrics?.let {
                        potentialReach?.let { reach ->
                            val membersText =
                                if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                                else getString(R.string.social_create_segment_item_description_member)

                            iAudience.visibility = View.GONE
                            with(iaAudience) {
                                visibility = View.VISIBLE
                                isEditable = true
                                audience = getString(R.string.audience_metrics)
                                audienceImage = R.drawable.ic_audience_detail_metrics
                                detail1 = getString(R.string.audience_potential_reach)
                                detail2 = membersText
                                this.potentialReach = reach.circles
                            }
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_MEMBERS -> {
                data?.apply {
                    @Suppress("UNCHECKED_CAST")
                    val selectedMembers = (data.extras?.getSerializable(SegmentGroupMembersActivity.EXTRA_MEMBERS) as? List<UserViewModel>).orEmpty()
                    members = selectedMembers.map { it.pk }.toIntArray()

                    val membersText = when {
                        selectedMembers.size == 1 -> getString(R.string.segment_members_one_member_selected)
                        selectedMembers.size == 2 -> getString(R.string.segment_members_two_members_selected)
                        selectedMembers.size > 2 -> getString(R.string.segment_members_n_members_selected, selectedMembers.size - 1)
                        else -> ""
                    }

                    val iconUrls = selectedMembers.map { it.imageUrl }

                    iAudience.visibility = View.GONE
                    with(iaAudience) {
                        visibility = View.VISIBLE
                        isEditable = true
                        audience = getString(R.string.audience_members)
                        audienceImage = R.drawable.ic_audience_detail_members
                        detail1 = "${selectedMembers[0].firstName} ${selectedMembers[0].lastName}"
                        detail2 = membersText
                        imageUrl = iconUrls[0]
                    }
                }
            }
            REQUEST_CODE_SCHEDULE to CreateSchedulePresenter.RESULT_CODE_SCHEDULE -> {
                data?.apply {
                    miPost?.title = getString(R.string.social_create_live_button_schedule)

                    scheduledDate = data.extras?.getLong(CreateSchedulePresenter.EXTRA_DATE)

                    iSchedule.visibility = View.GONE
                    with(isSchedule) {
                        visibility = View.VISIBLE
                        isEditable = true
                        scheduledDay = DateFormat.getLongDateFormat(context).format(scheduledDate)
                        val timeString = DateFormat.getTimeFormat(context).format(scheduledDate)
                        scheduledTime = timeString + " " + Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.LONG)
                    }
                }
            }
        }
        miPost?.isEnabled = true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_create_media)
        socialCreateMediaPresenter.onCreate(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_social_create_media, menu)
        miPost = menu.findItem(R.id.menu_social_create_media_post)
        miPost?.title = getString(miPostTitle)
        if (miPostTitle != R.string.social_create_media_button_post) {
            miPost?.isEnabled = false
        }
        miPost?.setOnMenuItemClickListener {
            socialCreateMediaPresenter.onPostClicked(itDescription.text, metrics, members, downline, scheduledDate)
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun setupToolbar(titleResId: Int) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(titleResId)
    }

    override fun setupOnClickListeners() {
        iAudience.setOnClickListener { openSegment() }
        iaAudience.setOnClickListener { openSegment() }
        iaAudience.listener = object : InputAudience.Listener {
            override fun onRemoveClicked() {
                setAudienceAsPublic()
            }
        }

        iSchedule.setOnClickListener { openSchedule() }
        isSchedule.setOnClickListener { openSchedule() }
        isSchedule.listener = object : InputAudience.Listener, InputSchedule.Listener {
            override fun onPlayClicked() {}

            override fun onRemoveClicked() {
                resetSchedule()
            }
        }
    }

    private fun setAudienceAsPublic() {
        iaAudience.visibility = View.GONE
        iAudience.visibility = View.VISIBLE

        segmentResultRequestCode = null
        segmentResultResultCode = null
        segmentResultData = null

        members = null
        downline = false
        metrics = null
    }

    private fun openSegment() {
        when {
            metrics != null -> {
                val intent = Intent(this, SegmentMetricsActivity::class.java).apply {
                    putExtra(EXTRA_METRICS, metrics)
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            members?.isNotEmpty() ?: false -> {
                val intent = Intent(this, SegmentGroupMembersActivity::class.java).apply {
                    putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, members!!.toTypedArray())
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            else -> {
                val intent = Intent(this, SocialCreateSegmentActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
        }
    }

    override fun setupKeyboardActions() {
        itDescription.editText?.setOnKeyboardActionListener { it.hideKeyboard() }
        itDescription.setOnInputListener(object : InputView.OnInputListener {
            override fun onInput() { socialCreateMediaPresenter.onInput(itDescription.editText?.text.toString()) }
        })
    }

    override fun setupDescription(description: String) {
        itDescription.text = description
    }

    override fun setupImageFromUri(uri: Uri, width: Int?, height: Int?) {
        showImage(uri, width, height)
        ivThumbnail.setOnClickListener { openImage(uri) }
    }

    override fun setupVideoFromUri(videoUri: Uri, thumbnailUri: Uri, width: Int?, height: Int?) {
        showImage(thumbnailUri, width, height)
        ivThumbnail.setOnClickListener { openVideo(videoUri) }
    }

    private fun showImage(uri: Uri, width: Int?, height: Int?) {
        ivThumbnail.visibility = View.VISIBLE
        if (width == null || height == null) {
            setImageViewSizeFromUri(uri)
        } else {
            setDimensionRatio(ivThumbnail, width, height)
        }
        Glide.with(this)
            .load(uri)
            .apply(
                RequestOptions
                    .skipMemoryCacheOf(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
            )
            .crossFade()
            .into(ivThumbnail)
    }

    private fun setImageViewSizeFromUri(uri: Uri) {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(File(uri.path).absolutePath, options)
        setDimensionRatio(ivThumbnail, options.outHeight, options.outWidth)
    }

    private fun openImage(uri: Uri) {
        val intent = Intent(this, SocialCreateMediaImageActivity::class.java)
        intent.putExtra(SocialCreateMediaImageActivity.EXTRA_URI, uri)
        startActivity(intent)
    }

    override fun setupVideoFromFile(uri: Uri) {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(this, uri)

        val bitmap = retriever.frameAtTime
        retriever.release()

        setDimensionRatio(ivThumbnail, bitmap.width, bitmap.height)
        ivThumbnail.setImageBitmap(bitmap)
        ivThumbnail.setOnClickListener { openVideo(uri) }
    }

    private fun openVideo(uri: Uri) {
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, uri.toString())
        startActivity(intent)
    }

    override fun setupAudience(audience: AudienceViewModel) {
        when {
            audience.metric != null -> {
                val intent = Intent().apply { putExtra(EXTRA_METRICS, audience.metric) }
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_METRIC, intent)
            }
            audience.members != null && audience.members.isNotEmpty() -> {
                val intent = Intent()
                intent.putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, audience.members as ArrayList<UserViewModel>)
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_MEMBERS, intent)
            }
            audience.downline -> {
                onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_DOWNLINE, null)
            }
            else -> onActivityResult(REQUEST_CODE_AUDIENCE, SocialCreateSegmentActivity.RESULT_CODE_PUBLIC, null)
        }
    }

    override fun showMedia(isVideo: Boolean) {
        ivThumbnail.visibility = View.VISIBLE
        if (isVideo) ivPlayButton.visibility = View.VISIBLE
    }

    override fun finishWithResult(intent: Intent?) {
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun showMessage(textResId: Int) {
        ViewUtils.showDialog(this, getString(textResId), null)
    }

    private fun setDimensionRatio(view: View, width: Int, height: Int) {
        val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.dimensionRatio = if (width > height) {
            "w,$height:$width"
        } else {
            "w,1:1"
        }
        view.layoutParams = layoutParams
    }

    override fun setupActionBarActionTitle(stringId: Int) {
        miPostTitle = stringId
    }

    override fun enableButton() {
        miPost?.isEnabled = true
        itDescription.setOnInputListener(object : InputView.OnInputListener {
            override fun onInput() {}
        })
    }

    override fun setupAudienceColor(color: String) {
        iaAudience.color = color
    }

    private fun openSchedule() {
        val intent = Intent(this, CreateScheduleActivity::class.java)
        scheduledDate?.let { intent.putExtra(CreateSchedulePresenter.EXTRA_DATE, it) }
        startActivityForResult(intent, REQUEST_CODE_SCHEDULE)
    }

    private fun resetSchedule() {
        iSchedule.visibility = View.VISIBLE
        isSchedule.visibility = View.GONE
        miPost?.title = getString(miPostTitle)
        scheduledDate = null
    }

    override fun hideSchedule() {
        iSchedule.visibility = View.GONE
        isSchedule.visibility = View.GONE
    }
}
