package co.socialsquad.squad.presentation.feature.explorev2.objectitem.address

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.feature.explorev2.SectionsFragment
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Address
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.address.customview.ShareAddressSheetDialog
import co.socialsquad.squad.presentation.feature.ranking.di.RankingListModule
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.android.PolyUtil
import com.google.maps.errors.ZeroResultsException
import kotlinx.android.synthetic.main.activity_object_address_map.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.module.Module


class ExploreAddressDetailActivity : BaseActivity() {

    companion object {

        const val EXTRA_ADDRESS = "EXTRA_ADDRESS"
        const val EXTRA_TITLE = "EXTRA_TITLE"
        const val EXTRA_ICON_URL = "EXTRA_ICON_URL"
        const val EXTRA_IMAGE_URL = "EXTRA_IMAGE_URL"

        fun start(
            context: Context, address: Address, title: String, icon: String,
            image: String
        ) {
            context.startActivity(Intent(context, ExploreAddressDetailActivity::class.java).apply {
                putExtra(EXTRA_ADDRESS, address)
                putExtra(EXTRA_TITLE, title)
                putExtra(EXTRA_ICON_URL, icon)
                putExtra(EXTRA_IMAGE_URL, image)
            })
        }
    }

    override val modules: List<Module> = listOf(RankingListModule.instance)
    override val contentView: Int = R.layout.activity_object_address_map

    private lateinit var map: MapView
    private lateinit var googleMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var bitmapSectionIcon: Bitmap

    private val address by extra<Address>(EXTRA_ADDRESS)
    private val title by extra<String>(EXTRA_TITLE)
    private val iconUrl by extra<String>(EXTRA_ICON_URL)
    private val image by extra<String>(EXTRA_IMAGE_URL)


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        map = findViewById(R.id.map)
        map.onCreate(savedInstanceState)

        configureToolbar()
        checkMyPermission()
        configureButton()
    }

    private fun callback(type: String) {
        when (type) {
            "waze" -> {
                openWazeNavigationToB(
                    this,
                    address.coordinates?.latitude!!,
                    address.coordinates?.longitude!!
                )
            }
            "maps" -> {
                openGoogleMapsNavigationToB(
                    this,
                    address.coordinates?.latitude!!,
                    address.coordinates?.longitude!!
                )
            }
        }
    }

    private fun configureButton() {
        btnWhereIs.setOnClickListener {
            var dialog = DriverAppSelectBottomSheetDialog(this::callback)
            dialog.show(supportFragmentManager, "selectapp")
        }

        btnWhereIsLabel.setOnClickListener {
            var dialog = DriverAppSelectBottomSheetDialog(this::callback)
            dialog.show(supportFragmentManager, "selectapp")
        }

        shareAddressBtn.setOnClickListener {
            var dialog = ShareAddressSheetDialog("Endereço", address.address_line1 ?: "") {
                when (it) {
                    "share" -> shareText()
                    "open_with" -> {
                        var dialog1 = DriverAppSelectBottomSheetDialog(this::callback)
                        dialog1.show(supportFragmentManager, "selectapp")
                    }
                }
            }
            dialog.show(supportFragmentManager, "share")
        }
    }

    private var isShowingShare = false

    private fun shareText() {
        if (isShowingShare.not()) {

            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "${address.address_line1}")
            startActivity(Intent.createChooser(sharingIntent, "Sharing Option"))

        }
    }

    private fun populateCard() {
        titleCard.text = title
        address1.text = address.address_line1
        address2.text = address.address_line2
        Glide.with(this).load(image).into(imageObject)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkMyPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            &&
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest
                        .permission.ACCESS_FINE_LOCATION
                ),
                SectionsFragment.REQUEST_ENABLE_LOCATION
            )
        } else {
            setupMap()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLastLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    val latLng = LatLng(it.latitude!!, it.longitude!!)
                    lastLocation = it

                    populateCard()
                    calculateRoute()
                }
            }
    }

    private fun setMarkSource(latLng: LatLng, drawer: Int) {
        var markerOpts = addMarkerPin(latLng, drawer)
        googleMap.addMarker(markerOpts)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == SectionsFragment.REQUEST_ENABLE_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults.size == 2
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                setupMap()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun configureToolbar() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.title = ""
        tvTitle.text = "Endereço"
        toolbar_layout.isTitleEnabled = false
    }

    @SuppressLint("MissingPermission")
    @ExperimentalCoroutinesApi
    private fun setupMap() {
        map.getMapAsync {

            googleMap = it

            googleMap.uiSettings.isMyLocationButtonEnabled = false
            googleMap.uiSettings.isZoomControlsEnabled = false
            googleMap.uiSettings.isMapToolbarEnabled = false
            googleMap.uiSettings.isCompassEnabled = false
            googleMap.uiSettings.isTiltGesturesEnabled = false
            googleMap.uiSettings.isIndoorLevelPickerEnabled = false
            googleMap.uiSettings.isRotateGesturesEnabled = false
            googleMap.isMyLocationEnabled = true
            googleMap.isTrafficEnabled = false
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            googleMap.setOnMapLoadedCallback {
                Glide.with(this)
                    .asBitmap()
                    .load(iconUrl)
                    .into(object : SimpleTarget<Bitmap?>() {
                        @RequiresApi(Build.VERSION_CODES.M)
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap?>?
                        ) {
                            bitmapSectionIcon = resource
                            requestLastLocation()
                        }
                    })
            }
        }
    }

    private fun addMarkerPin(latLng: LatLng, drawer: Int): MarkerOptions {

        val bitmap = createMarkerBitmapSelected(drawer)

        return MarkerOptions().position(latLng)
            .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
    }

    private fun createMarkerBitmapSelected(drawer: Int): Bitmap {
        var view = layoutInflater.inflate(
            drawer,
            null,
            false
        )


        val image = view.findViewById<ImageView>(R.id.exploreMapTooltipIcon)
        image?.let {
            it.setImageBitmap(bitmapSectionIcon)
        }
        return createBitmapFromView(view, 38, 41)
    }

    private fun createBitmapFromView(view: View, width: Int, height: Int): Bitmap {
        if (width > 0 && height > 0) {
            view.measure(
                View.MeasureSpec.makeMeasureSpec(dpTopPx(width), View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(
                    dpTopPx(height), View.MeasureSpec.EXACTLY
                )
            )
        }
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        val bitmap = Bitmap.createBitmap(
            view.measuredWidth,
            view.measuredHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        val background = view.background
        background?.draw(canvas)
        view.draw(canvas)
        return bitmap
    }

    private fun dpTopPx(dp: Int): Int {
        return (dp * resources.displayMetrics.density).toInt()
    }

    private fun calculateRoute() {
        val context = GeoApiContext.Builder()
            .apiKey("AIzaSyDdOh3FGDxEqbaxhjdMPcMRWR3RpvF6dvQ")
            .build()

        try {
            var result = DirectionsApi.getDirections(
                context,
                "${lastLocation.latitude},${lastLocation.longitude}",
                "${address.coordinates?.latitude},${address.coordinates?.longitude}"
            ).await()

            var totalDistance = 0L
            var totalDuration = 0L

            result.routes.forEach { route ->
                route.legs.forEach { leg ->
                    leg.steps.forEach { step ->

                        totalDistance += step.distance.inMeters
                        totalDuration += step.duration.inSeconds
                        var path = PolyUtil.decode(step.polyline.encodedPath)

                        googleMap.addPolyline(
                            PolylineOptions().addAll(path).color(ColorUtils.parse("#b72c2f"))
                        )
                    }
                }
            }

            var durationMin = (totalDuration / 60).toInt()
            var durationHours = (durationMin / 60).toInt()

            var differenceMin = durationMin - (durationHours * 60)

//            var formatter = RelativeDateTimeFormatter.getInstance()
//            var formatted = formatter.format(duration, RelativeDateTimeFormatter.Direction.NEXT, RelativeDateTimeFormatter.RelativeUnit.HOURS)

            var hourString = if (durationHours > 0) {
                "${durationHours}h${differenceMin}min"
            } else {
                "${durationMin}min"
            }

            eta.text = "${(totalDistance / 1000).toInt()} km • $hourString de carro"
            infoAddress.visibility = View.VISIBLE

            var offset = 0.00

            val sourceLatLng =
                LatLng(lastLocation.latitude + offset, lastLocation.longitude + offset)
            val targetLatLng = LatLng(
                address.coordinates?.latitude!! + offset,
                address.coordinates?.longitude!! + offset
            )

            //setMarkSource(sourceLatLng, R.layout.view_explore_address_target)
            setMarkSource(targetLatLng, R.layout.view_explore_location_tooltip_selected)

            val latLngBoundsBuilder =
                LatLngBounds.Builder().include(sourceLatLng).include(targetLatLng)

            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    latLngBoundsBuilder.build(),
                    dpTopPx(20)
                ), object : GoogleMap.CancelableCallback {
                    override fun onFinish() {
                        changeOffsetCenter100(
                            googleMap.cameraPosition.target.latitude,
                            googleMap.cameraPosition.target.longitude
                        )
                    }

                    override fun onCancel() {

                    }
                })


        } catch (e: ZeroResultsException) {
            populateCard()
        }
    }

    fun changeOffsetCenter100(latitude: Double, longitude: Double) {
        val mappoint = googleMap.projection.toScreenLocation(LatLng(latitude, longitude))
        mappoint.set(
            mappoint.x,
            mappoint.y
        ) // change these values as you need , just hard coded a value if you want you can give it based on a ratio like using DisplayMetrics  as well
        googleMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                googleMap.projection.fromScreenLocation(
                    mappoint
                ), (googleMap.cameraPosition.zoom - .65).toFloat()
            )
        )
    }

    private fun changeOffsetCenter(latitude: Double, longitude: Double) {
        val mappoint = googleMap.projection.toScreenLocation(LatLng(latitude, longitude))
        mappoint.set(
            mappoint.x,
            mappoint.y + 200
        ) // change these values as you need , just hard coded a value if you want you can give it based on a ratio like using DisplayMetrics  as well
        googleMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                googleMap.projection.fromScreenLocation(
                    mappoint
                ), 15f
            )
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }


    fun openWazeNavigationToB(
        context: Context,
        latitude: Double,
        longitude: Double
    ) {
        val wazeUrl = "https://waze.com/ul?ll=$latitude%2C$longitude&amp;navigate=yes"
        val uri = Uri.parse(wazeUrl)

        val intent = Intent(Intent.ACTION_VIEW, uri)
        context.startActivity(intent)
    }

    fun openGoogleMapsNavigationToB(
        context: Context,
        latitude: Double,
        longitude: Double
    ) {
        val googleMapsUrl = "google.navigation:q=$latitude,$longitude"
        val uri = Uri.parse(googleMapsUrl)

        val googleMapsPackage = "com.google.android.apps.maps"
        val intent = Intent(Intent.ACTION_VIEW, uri).apply {
            setPackage(googleMapsPackage)
        }

        context.startActivity(intent)
    }
}
