package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class AgreeRefuseTermsResponse(
        var ok: Boolean
) : Parcelable