package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SectionItem(
    @SerializedName("title") val title: String?,
    @SerializedName("data") val data: List<SectionsData>?
) : Parcelable