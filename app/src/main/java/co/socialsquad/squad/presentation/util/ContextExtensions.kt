package co.socialsquad.squad.presentation.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import androidx.core.content.ContextCompat

fun Context.intentDial(phone: String) {
    startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone")))
}

fun Context.intentMaps(latitude: Double, longitude: Double, location: String? = null) {
    val uri = Uri.parse("geo:0,0?q=$latitude,$longitude($location)")
    startActivity(Intent(Intent.ACTION_VIEW, uri))
}

fun Context.getColorCompat(colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun Context.getDrawableCompat(drawableRes: Int) = ContextCompat.getDrawable(this, drawableRes)

fun Context.convertDpToPixels(dp: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()
