package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ProductLocationNoPermissionViewModel

class ProductLocationNoPermissionViewHolder(itemView: View, val listener: Listener<ProductLocationNoPermissionViewModel>) : ViewHolder<ProductLocationNoPermissionViewModel>(itemView) {
    override fun bind(viewModel: ProductLocationNoPermissionViewModel) {
        itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    override fun recycle() {}
}
