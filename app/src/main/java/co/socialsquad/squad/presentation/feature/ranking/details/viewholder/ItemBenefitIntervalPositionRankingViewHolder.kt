package co.socialsquad.squad.presentation.feature.ranking.details.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.item_ranking_position.view.*

class ItemBenefitIntervalPositionRankingViewHolder(
        itemView: View,
        var onClickAction: ((item: ItemBenefitIntervalPositionRankingViewModel) -> Unit)) : ViewHolder<ItemBenefitIntervalPositionRankingViewModel>(itemView) {
    override fun bind(viewModel: ItemBenefitIntervalPositionRankingViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }
    }

    override fun recycle() {

    }

}