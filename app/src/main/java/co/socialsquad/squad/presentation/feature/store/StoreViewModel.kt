package co.socialsquad.squad.presentation.feature.store

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Item
import co.socialsquad.squad.data.entity.Link
import co.socialsquad.squad.data.entity.Product
import co.socialsquad.squad.data.entity.ProductDetail
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.entity.Store
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.Shareable
import co.socialsquad.squad.presentation.util.TextUtils
import java.io.Serializable

const val HEADER_VIEW_MODEL_ID = R.layout.view_list_header
const val FOOTER_VIEW_MODEL_ID = R.layout.view_list_footer
const val LOADING_VIEW_MODEL_ID = R.layout.view_list_loading
const val ITEM_VIEW_MODEL_ID = R.layout.view_list_item

class HeaderViewModel(
    val title: String? = null,
    val subtitle: String? = null,
    val titleResId: Int? = null,
    val subtitleResId: Int? = null,
    val hasTopMargin: Boolean = true
) : ViewModel

abstract class FooterViewModel : ViewModel

class LoadingViewModel(val search: String = "") : ViewModel

abstract class ItemViewModel(open val title: String) : ViewModel, Serializable

const val STORE_BANNER_LIST_VIEW_MODEL_ID = R.layout.view_store_banner_list
const val STORE_CATEGORY_LIST_VIEW_MODEL_ID = R.layout.view_store_category_list
const val STORE_CATEGORY_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_store_categories_list_item
const val STORE_PRODUCT_LIST_VIEW_MODEL_ID = R.layout.view_store_product_list
const val STORE_PRODUCT_LIST_ITEM_VIEW_MODEL_ID = R.layout.view_store_product_list_item
const val STORE_PRODUCT_VIEW_MODEL_ID = R.layout.view_store_product
const val STORE_PRODUCT_LIST_BANNER_VIEW_MODEL_ID = R.layout.view_store_product_list_banner
const val PRODUCT_VIEW_MODEL_ID = R.layout.view_product

class StoreBannerListViewModel(val banners: List<StoreBannerViewModel>) : ViewModel

class StoreBannerViewModel(
    val product: StoreProductViewModel? = null,
    val category: StoreCategoryListItemViewModel? = null,
    val productList: ProductListViewModel? = null,
    val image: String
) : ViewModel

class StoreFooterViewModel(val productList: ProductListViewModel) : FooterViewModel()

class StoreCategoryListViewModel(val categories: List<StoreCategoryListItemViewModel>) : ViewModel

class StoreCategoryListItemViewModel(
    val pk: Int,
    override val title: String,
    val icon: String?,
    val cover: String?,
    val hasSubcategories: Boolean
) : ItemViewModel(title), Serializable

class StoreProductListViewModel(val products: List<StoreProductViewModel>) : ViewModel

class StoreProductListBannerViewModel(val imageUrl: String) : ViewModel

class StoreProductViewModel(
    val pk: Int,
    val code: Int,
    val medias: List<Media>? = null,
    val name: String,
    val shortName: String,
    val description: String? = null,
    val price: Double? = null,
    val points: Int? = null,
    val category: String?
) : ViewModel, Serializable {
    constructor(product: Product) : this(
        product.pk,
        product.pid,
        product.medias?.mapNotNull { Media(it)?.takeIf { !it.url.isBlank() } },
        product.fullName,
        product.name,
        product.content,
        product.price,
        product.buyPoints,
        product.categories?.firstOrNull()?.name
    )
}

class StoreProductList(
    val completed: Boolean = false,
    val storeList: List<StoreProductViewModel>
)

class ProductListViewModel(val pk: Int, val title: String) : Serializable

const val PRODUCT_OVERVIEW_VIEW_MODEL_ID = R.layout.view_product_overview

class ProductOverviewViewModel(
    val pk: Int,
    val medias: List<Media>? = null,
    val price: Double? = null,
    val points: Int? = null,
    val code: Int,
    val name: String,
    val description: String? = null,
    val subdomain: String?
) : ViewModel, Shareable {
    override val shareViewModel get() = ShareViewModel(pk, name, description, medias?.firstOrNull()?.url, Share.Product, true, subdomain)

    constructor(storeProductViewModel: StoreProductViewModel, subdomain: String?) : this(
        storeProductViewModel.pk,
        storeProductViewModel.medias,
        storeProductViewModel.price,
        storeProductViewModel.points,
        storeProductViewModel.code,
        storeProductViewModel.name,
        storeProductViewModel.description,
        subdomain
    )
}

const val PRODUCT_DETAIL_GROUPS_VIEW_MODEL_ID = R.layout.view_product_detail_groups

class ProductDetailGroupsViewModel(val detailGroups: List<ProductDetailGroupViewModel>) : ViewModel

class ProductDetailGroupViewModel(
    override val title: String,
    val details: List<ProductDetailViewModel>,
    val cover: String,
    val media: Media?
) : ItemViewModel(title)

const val PRODUCT_DETAIL_VIEW_MODEL_ID = R.layout.view_product_detail

class ProductDetailViewModel(
    val title: String,
    val description: String
) : ViewModel, Serializable {
    constructor(productDetail: ProductDetail) : this(productDetail.name, productDetail.detail)
}

const val PRODUCT_LOCATION_LIST_VIEW_MODEL_ID = R.layout.view_product_location_list
const val PRODUCT_LOCATION_VIEW_MODEL_ID = R.layout.view_product_location
const val PRODUCT_LOCATION_LOADING_VIEW_MODEL_ID = R.layout.view_product_location_loading
const val PRODUCT_LOCATION_NO_PERMISSION_VIEW_MODEL_ID = R.layout.view_product_location_no_permission
const val PRODUCT_LOCATION_DISABLED_VIEW_MODEL_ID = R.layout.view_product_location_disabled
const val PRODUCT_LOCATION_NOT_AVAILABLE_VIEW_MODEL_ID = R.layout.view_product_location_not_available
const val PRODUCT_LOCATION_NETWORK_ERROR_VIEW_MODEL_ID = R.layout.view_product_location_network_error

class ProductLocationListViewModel(val list: List<StoreViewModel>) : ViewModel, Serializable

class StoreViewModel(
    val name: String,
    val image: String? = null,
    val address: String,
    val phone: String? = null,
    val amount: Int,
    val distance: Double?,
    val latitude: Double,
    val longitude: Double
) : ViewModel, Serializable {
    constructor(store: Store, latitude: Double?, longitude: Double?) : this(
        store.name,
        store.photo?.let { Media(it) }?.takeIf { !it.url.isBlank() }?.url,
        listOfNotNull(store.address, store.number).joinToString(", "),
        if (!store.phone.isNullOrBlank()) store.phone else null,
        store.count,
        if (latitude != null && longitude != null) TextUtils.calculateDistance(latitude, longitude, store.lat, store.lng) else null,
        store.lat,
        store.lng
    )
}

object ProductLocationLoadingViewModel : ViewModel

object ProductLocationNoPermissionViewModel : ViewModel

object ProductLocationDisabledViewModel : ViewModel

object ProductLocationNotAvailableViewModel : ViewModel

object ProductLocationNetworkErrorViewModel : ViewModel

class ProductLocationSeeAllViewModel(val list: ProductLocationListViewModel) : FooterViewModel()

class ProductSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class ImmobilesSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class LivesSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class EventsSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class VideosSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class AudiosSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class StoresSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class DocumentsSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}

class TripsSearchItemViewModel(
    val id: Int,
    val snippet: String,
    val shortName: String,
    val shortDescription: String,
    val customFields: String,
    val media: String,
    val link: Link
) : ViewModel {

    constructor(item: Item) : this(
        id = item.id ?: -1,
        snippet = item.snippet ?: "",
        shortName = item.shortTitle ?: "",
        shortDescription = item.shortDescription ?: "",
        customFields = item.customFields ?: "",
        media = item.media?.url ?: "",
        link = Link(
            id = item.link?.id,
            type = item.link?.type,
            url = item.link?.url
        )
    )
}
