package co.socialsquad.squad.presentation.feature.profile.edit.customview


import android.app.Dialog
import android.graphics.Outline
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.databinding.ItemStateOptionsBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_edit_profile_choose_state.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class ChooseStateSheetDialog(
    private val state: String?,
    private val onSelect: (uf: String, label: String) -> Unit
) :
    BottomSheetDialogFragment() {

    var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null

    override fun getTheme(): Int = R.style.FarmBlocks_BottomSheetDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_edit_profile_choose_state, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout?
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        }
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)



        return dialog
    }

    enum class EstadosBrasileiros(val nome: String) {
        AC("Acre"),
        AL("Alagoas"),
        AP("Amapá"),
        AM("Amazonas"),
        BA("Bahia"),
        CE("Ceará"),
        DF("Distrito Federal"),
        ES("Espírito Santo"),
        GO("Goiás"),
        MA("Maranhão"),
        MT("Mato Grosso"),
        MS("Mato Grosso do Sul"),
        MG("Minas Gerais"),
        PA("Pará"),
        PB("Paraíba"),
        PR("Paraná"),
        PE("Pernambuco"),
        PI("Piauí"),
        RJ("Rio de Janeiro"),
        RN("Rio Grande do Norte"),
        RS("Rio Grande do Sul"),
        RO("Rondônia"),
        RR("Roraima"),
        SC("Santa Catarina"),
        SP("São Paulo"),
        SE("Sergipe"),
        TO("Tocantins")
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setTopBordersRadius()
        super.onViewCreated(view, savedInstanceState)
        expandDialog()

        var scrollT = 0

        var states = EstadosBrasileiros.values().mapIndexed { index, it ->
            if (it.toString() == state) {
                scrollT = index
                StateInfo(it.toString(), it.nome, true)
            } else {
                StateInfo(it.toString(), it.nome, false)
            }
        }


        rvStates.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

            adapter = object : RecyclerView.Adapter<StateViewHolder>() {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StateViewHolder {
                    var binding = ItemStateOptionsBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                    return StateViewHolder(binding) { select ->
                        states.forEach {
                            it.isSelected = false
                        }
                        select.isSelected = true

                        onSelect.invoke(select.uf, select.name)
                        notifyDataSetChanged()
                        dismiss()
                    }
                }

                override fun onBindViewHolder(holder: StateViewHolder, position: Int) {
                    holder.bind(states[position])
                }

                override fun getItemCount() = states.size

            }
        }

        rvStates.smoothScrollToPosition(scrollT)
    }


    override fun onPause() {
        isShowing = false
        super.onPause()
    }

    override fun onResume() {
        isShowing = true
        super.onResume()

    }


    override fun dismiss() {
        isShowing = false
        super.dismiss()
    }

    override fun onDestroy() {
        isShowing = false
        super.onDestroy()
    }

    private fun setTopBordersRadius() {
        val radius =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)
        val container = view as ViewGroup

        container.apply {
            clipToOutline = true
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    if (view != null) outline?.setRoundRect(
                        0,
                        0,
                        view.width,
                        (view.height + 300).toInt(),
                        radius
                    )
                }
            }
        }
    }

    fun expandDialog() {
        GlobalScope.launch(context = Dispatchers.Main) {
            delay(300)
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        if (!isShowing) {
            isShowing = true
            super.show(manager, tag)
        }
    }

    companion object {
        var isShowing = false
    }

    class StateViewHolder(
        var binding: ItemStateOptionsBinding,
        var onSelect: (stateInfo: StateInfo) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(stateInfo: StateInfo) {
            binding.ccChecked.isChecked = stateInfo.isSelected
            binding.name.text = stateInfo.name
            binding.mainLayout.setOnClickListener {
                onSelect.invoke(stateInfo)
            }
        }
    }

    data class StateInfo(var uf: String, var name: String, var isSelected: Boolean = false)

}