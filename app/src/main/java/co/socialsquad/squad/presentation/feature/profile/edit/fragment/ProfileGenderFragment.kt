package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.dialog.ConfirmLeaveNoSaveDialog
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.fragment_edit_profile_gender.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3


class ProfileGenderFragment(
    var genderOriginal: String? = "",
    var fragments: List<Fragment>? = null,
    val isInCompleteProfile: Boolean? = false
) : Fragment() {
    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    private var radioComponents = listOf<RadioButton>()

    var gender: String? = ""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        radioComponents = mutableListOf<RadioButton>(radioMasc, radioFem, radioCustom, radioUnknown)

        configureButtons()
        configureObservables()
        configureInputs()

        viewModel.saveStatus.observe(requireActivity(), Observer {
            Log.i("gender", "status: $it")

            when (it) {
                "gender" -> {
                    viewModel.state.value = StateNav("next", true, this)
                    (activity as? CompleteProfileActivity)?.next()
                    hideLoading()
                }
            }
        })

        when (genderOriginal) {
            "M" -> radioMasc.isChecked = true
            "F" -> radioFem.isChecked = true
            "X" -> radioUnknown.isChecked = true
        }

        configureFragmentByPosition()
    }

    private var text_button = ""

    fun configureFragmentByPosition() {


        if (isInCompleteProfile!!) {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.light_mode_identity_profile_complete_your_profile_details))
            fragments?.forEachIndexed { index, fragment ->
                if (fragment == this) {
                    txtPostion.text = "${index + 1} de ${fragments?.size}"

                    if (index == (fragments?.size!! - 1)) {
                        btnSalvar.text = "Salvar"
                        text_button = "Salvar"
                    } else {
                        btnSalvar.text = "Próximo"
                        text_button = "Próximo"
                    }
                }
            }
            txtPostion.isVisible = true
            txtTitle.text = "Perfil"
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_gender))
            txtTitle.text = "Gênero"
        }
    }

    var mascListener = {
        radioMasc.isChecked = true
        radioFem.isChecked = false
        radioCustom.isChecked = false
        radioUnknown.isChecked = false

        btnSalvar.isEnabled = true

        gender = "M"

        hideKeyboard(requireActivity())

        itCustomGender.visibility = View.GONE

        wasChanged = gender != genderOriginal
    }

    var femListener = {
        radioFem.isChecked = true
        radioMasc.isChecked = false
        radioCustom.isChecked = false
        radioUnknown.isChecked = false

        btnSalvar.isEnabled = true

        gender = "F"

        hideKeyboard(requireActivity())

        itCustomGender.visibility = View.GONE

        wasChanged = gender != genderOriginal
    }

    var customListener = {
        radioCustom.isChecked = true
        radioMasc.isChecked = false
        radioFem.isChecked = false
        radioUnknown.isChecked = false

        if (radioCustom.isChecked) {
            itCustomGender.visibility = View.VISIBLE
            itCustomGender.requestFocus()
            itCustomGender.addEditTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    gender = s.toString()
                }

                override fun afterTextChanged(s: Editable?) {

                }

            })
        }

        btnSalvar.isEnabled = true

        wasChanged = gender != genderOriginal
    }

    var unkownListener = {
        radioUnknown.isChecked = true
        radioMasc.isChecked = false
        radioFem.isChecked = false
        radioCustom.isChecked = false

        btnSalvar.isEnabled = true
        hideKeyboard(requireActivity())

        gender = "X"

        itCustomGender.visibility = View.GONE

        wasChanged = gender != genderOriginal
    }

    private fun configureInputs() {

        radioMasc.setOnClickListener {
            mascListener.invoke()
        }

        mascLayout.setOnClickListener {
            mascListener.invoke()
        }

        radioFem.setOnClickListener {
            femListener.invoke()
        }

        femLayout.setOnClickListener {
            femListener.invoke()
        }

        radioCustom.setOnClickListener {
            customListener.invoke()
        }

        customLayout.setOnClickListener {
            customListener.invoke()
        }

        radioUnknown.setOnClickListener {
            unkownListener.invoke()
        }

        unknownLayout.setOnClickListener {
            unkownListener.invoke()
        }
    }

    private fun configureObservables() {
    }

    private fun hideSoftKeyboard(view: View) {
        val imm =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private var wasChanged = false

    private fun configureButtons() {
        btnSalvar.setOnClickListener {

            btnSalvar.isEnabled = false
            showLoading()

            viewModel.saveGender(gender)
        }

        btnBack.setOnClickListener {
            if (isInCompleteProfile!!) {
                (activity as? CompleteProfileActivity)?.previous()
            } else {
                if (wasChanged) {
                    var confirmDialog = ConfirmLeaveNoSaveDialog() {
                        when (it) {
                            "confirm" -> viewModel.state.postValue(StateNav("previous", true))
                        }
                    }
                    confirmDialog.show(childFragmentManager, "confirm")

                } else {
                    viewModel.state.postValue(StateNav("previous", true))
                }
            }
        }
    }


    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    private fun canSave(canSave: Boolean) {
        btnSalvar.isEnabled = true
    }

    private fun showLoading() {
        btnSalvar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }


    private fun hideLoading() {
        btnSalvar.text = text_button
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
        btnSalvar.isEnabled = true
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_gender, container, false)

    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}
