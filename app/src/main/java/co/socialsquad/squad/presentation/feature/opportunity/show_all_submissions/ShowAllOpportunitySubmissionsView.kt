package co.socialsquad.squad.presentation.feature.opportunity.show_all_submissions

import co.socialsquad.squad.presentation.feature.LoadingView
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListView

interface ShowAllOpportunitySubmissionsView : LoadingView, OpportunityListView
