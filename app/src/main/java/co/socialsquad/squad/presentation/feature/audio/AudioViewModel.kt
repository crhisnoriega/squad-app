package co.socialsquad.squad.presentation.feature.audio

import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Audio
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.Shareable
import java.io.Serializable
import java.util.concurrent.TimeUnit

const val AUDIO_VIEW_MODEL_ID = R.layout.view_audio_list_item

class AudioViewModel(
    val pk: Int,
    val title: String,
    val description: String?,
    val url: String,
    val author: String,
    val authorPk: Int,
    val qualification: String,
    val avatar: String?,
    val date: String,
    val scheduledDate: String?,
    val audienceType: String,
    val color: String?,
    var duration: Long? = 0,
    var liked: Boolean,
    var likeAvatars: List<String>?,
    var likeCount: Int = 0,
    var currentUserAvatar: String? = null,
    var playbackCount: Long = 0,
    var listened: Boolean = false,
    var canEdit: Boolean,
    var canDelete: Boolean,
    var canShare: Boolean,
    var canRecommend: Boolean,
    val subdomain: String?
) : ViewModel, Serializable, Shareable {
    override val shareViewModel get() = ShareViewModel(pk, author, title, avatar, Share.Audio, canShare, subdomain)

    constructor(audio: Audio, color: String? = "", userAvatar: String? = "", subdomain: String?) : this(
        audio.pk,
        audio.name,
        audio.content,
        audio.media?.url ?: "",
        audio.owner.user.fullName,
        audio.owner.pk,
        audio.owner.qualification,
        audio.owner.user.avatar,
        audio.createdAt,
        audio.scheduled?.startTime,
        audio.audienceType,
        color,
        audio.media?.timeLength?.let { TimeUnit.SECONDS.toMillis(it) },
        audio.liked,
        audio.likedUsersAvatars,
        audio.likesCount,
        userAvatar,
        audio.totalListenings,
        audio.listened,
        audio.canEdit,
        audio.canDelete,
        audio.canShare,
        audio.canRecommend,
        subdomain
    )

    var isPrepared = false
    var isPlaying = false
    var isFinished = false
    var position = 0L
        get() {
            if (isFinished) field = 0L
            return field
        }

    val listener = object : AudioManager.Listener {
        override fun onReady(isPlaying: Boolean) {
            this@AudioViewModel.isPlaying = isPlaying
            isFinished = false
        }

        override fun onBuffering() {
            this@AudioViewModel.isPlaying = false
            isFinished = false
        }

        override fun onEnded() {
            this@AudioViewModel.isPlaying = false
            isPrepared = false
            isFinished = true
            AudioManager.removeListener(this)
        }

        override fun onReleased() {
            this@AudioViewModel.isPlaying = false
            isPrepared = false
            AudioManager.removeListener(this)
        }

        override fun onPositionChanged(position: Long) {
            this@AudioViewModel.position = position
        }
    }

    fun resetState() {
        isPrepared = false
        isPlaying = false
        isFinished = false
        position = 0
    }
}
