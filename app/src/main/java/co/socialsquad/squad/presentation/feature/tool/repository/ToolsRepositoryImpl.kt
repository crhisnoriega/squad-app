package co.socialsquad.squad.presentation.feature.tool.repository

import co.socialsquad.squad.domain.model.*
import co.socialsquad.squad.presentation.feature.tool.repository.remote.ToolApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class ToolsRepositoryImpl(private val api: ToolApi) : ToolsRepository {
    override fun fetchOpportunitiesSubmission(link: String): Flow<Opportunity<Submission>?> = flow {
        emit(api.fetchOpportunitiesSubmission(link).data)
    }


    override fun fetchOpportunitiesTimetable(link: String): Flow<Opportunity<Timetable>?> =
            flow {
                emit(api.fetchOpportunitiesTimetable(link).data)
            }

    override fun fetchDetailsTimetable(submissionId: String): Flow<Timetable?> =
            flow {
                emit(api.fetchDetailsTimetable(submissionId).data)
            }


    override fun createNewOpportunity(
            objectId: Long,
            opportunityId: Long
    ): Flow<SubmissionCreationResponse> {
        return flow {
            emit(api.createNewOpportunity(objectId, opportunityId))
        }
    }

    override fun getNextWidget(submissionId: Long): Flow<OpportunityWidgetResponse> {
        return flow {
            emit(api.getNextWidget(submissionId))
        }
    }
}
