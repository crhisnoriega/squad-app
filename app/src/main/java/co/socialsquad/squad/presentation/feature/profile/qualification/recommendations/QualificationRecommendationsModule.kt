package co.socialsquad.squad.presentation.feature.profile.qualification.recommendations

import dagger.Binds
import dagger.Module

@Module
abstract class QualificationRecommendationsModule {
    @Binds
    abstract fun view(qualificationRecommendationsActivity: QualificationRecommendationsActivity): QualificationRecommendationsView
}
