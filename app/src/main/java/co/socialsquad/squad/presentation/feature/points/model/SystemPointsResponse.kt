package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SystemPointsResponse(
        @SerializedName("results") val results: List<SystemData>?,
        @SerializedName("count") val count: Int?,
        @SerializedName("next") val next: String?,
        @SerializedName("previous") val previous: String?,
        var first: Boolean = false
) : Parcelable