package co.socialsquad.squad.presentation.feature.explorev2.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Explore
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionLoadingState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemFragment
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_explore_item_detail.*
import kotlinx.android.synthetic.main.opportunity_page_1.view.*

class ExploreItemDetailActivity : AppCompatActivity(), ExploreItemFragment.OnItemLoadedListener {

    private val contentItem by extra<ContentItem>(ARGUMENT_ITEM)
    private val template by extra<SectionTemplate>(ARGUMENT_TEMPLATE) { SectionTemplate.getDefault() }
    private val loadingState by extra<List<SectionLoadingState>>(ARGUMENT_LOADING_STATE_LIST) { SectionLoadingState.getDefault() }
    private val emptyState by extra<EmptyState?>(ARGUMENT_EMPTY_STATE)

    companion object {
        const val ARGUMENT_ITEM = "ARGUMENT_ITEM"
        const val ARGUMENT_TEMPLATE = "ARGUMENT_TEMPLATE"
        const val ARGUMENT_LOADING_STATE_LIST = "ARGUMENT_LOADING_STATE_LIST"
        const val ARGUMENT_EMPTY_STATE = "ARGUMENT_EMPTY_STATE"

        fun start(
                context: Context,
                item: ContentItem,
                template: SectionTemplate = SectionTemplate.getDefault(),
                loadingState: List<SectionLoadingState> = SectionLoadingState.getDefault(),
                emptyState: EmptyState? = null
        ) {
            context.startActivity(
                    Intent(context, ExploreItemDetailActivity::class.java).apply {
                        putExtra(ARGUMENT_ITEM, item)
                        putExtra(ARGUMENT_TEMPLATE, template)
                        putParcelableArrayListExtra(ARGUMENT_LOADING_STATE_LIST, ArrayList(loadingState))
                        putExtra(ARGUMENT_EMPTY_STATE, emptyState)
                    }
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore_item_detail)
        setupToolbar()

        supportFragmentManager.beginTransaction().add(
                R.id.detailFragment,
                ExploreItemFragment.newInstance(
                        "/endpoints${contentItem.link?.url}",
                        template,
                        loadingState,
                        emptyState,
                        contentItem.title
                )
        ).commit()
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is ExploreItemFragment) {
            fragment.setItemLoadedListener(this)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = ""
    }

    override fun onItemLoaded(explore: Explore) {
        loadingToolbar.isVisible = false
        supportActionBar?.title = explore.title
        titleActivity.text = explore.title
    }
}
