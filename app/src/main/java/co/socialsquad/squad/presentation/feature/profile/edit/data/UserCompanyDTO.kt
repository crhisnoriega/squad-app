package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
class UserCompanyDTO(
    @SerializedName("user") var user: UserDTO? = null,
    @SerializedName("salesforce_id") var salesforce_id: String? = null
) : Parcelable {
}