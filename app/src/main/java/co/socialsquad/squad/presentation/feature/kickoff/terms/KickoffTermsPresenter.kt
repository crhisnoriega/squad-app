package co.socialsquad.squad.presentation.feature.kickoff.terms

import android.os.Bundle
import co.socialsquad.squad.data.encryption.SimpleWordEncrypt
import co.socialsquad.squad.data.entity.RegisterIntegrationRequest
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.data.utils.TagWorker
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class KickoffTermsPresenter @Inject
internal constructor(
    private val kickoffTermsView: co.socialsquad.squad.presentation.feature.kickoff.terms.KickoffTermsView,
    private val signupRepository: SignupRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker,
    private val simpleWordEncrypt: SimpleWordEncrypt,
    private val preferences: Preferences
) {

    private val compositeDisposable = CompositeDisposable()

    fun onAgreeClicked(arguments: Bundle) {
        val id = arguments.getString(CREDENTIALS_HINODE_ID)
        val password = arguments.getString(CREDENTIALS_HINODE_PASSWORD)
        val email = signupRepository.email

        val registerIntegrationRequest = RegisterIntegrationRequest(java.lang.Long.valueOf(id!!), password!!, email!!)

        compositeDisposable.add(
            signupRepository.registerIntegration(registerIntegrationRequest)
                .doOnNext { storeCredentials(registerIntegrationRequest.password) }
                .doOnSubscribe { disposable -> kickoffTermsView.showLoading() }
                .subscribe(
                    { (token, userCompany) ->
                        loginRepository.token = token
                        loginRepository.subdomain = userCompany.company.subdomain
                        loginRepository.userCompany = userCompany
                        loginRepository.squadColor = userCompany.company.primaryColor

                        kickoffTermsView.hideLoading()
                        kickoffTermsView.showNavigation()

                        tagWorker.tagSignUpEvent()
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        kickoffTermsView.hideLoading()
                        kickoffTermsView.showErrorMessageFailedToRegisterUser()
                    }
                )
        )
    }

    private fun storeCredentials(password: String) {
        val encrypted = simpleWordEncrypt.encrypt(password)
        preferences.externalSystemPassword = encrypted?.encryptedMessage
        preferences.externalSystemPasswordIV = encrypted?.iv
    }

    internal fun onDestroyView() {
        compositeDisposable.dispose()
    }

    companion object {

        val CREDENTIALS_HINODE_ID = "CREDENTIALS_HINODE_ID"
        val CREDENTIALS_HINODE_PASSWORD = "CREDENTIALS_HINODE_PASSWORD"
    }
}
