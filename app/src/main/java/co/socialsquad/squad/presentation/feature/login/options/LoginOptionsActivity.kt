package co.socialsquad.squad.presentation.feature.login.options

import android.content.Intent
import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.feature.login.alternative.EXTRA_LOGIN_ALTERNATIVE_TYPE
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativeActivity
import co.socialsquad.squad.presentation.feature.login.alternative.LoginAlternativePresenter
import co.socialsquad.squad.presentation.feature.login.email.LoginEmailActivity
import co.socialsquad.squad.presentation.util.Analytics
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import kotlinx.android.synthetic.main.activity_login_options.*
import javax.inject.Inject

class LoginOptionsActivity : BaseDaggerActivity() {

    @Inject
    lateinit var analytics: Analytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_options)
        setClickListeners()
        setFlagLayoutFullscreen()

        analytics.sendEvent(Analytics.SIGN_SCREEN_VIEW)
    }

    private fun setClickListeners() {
        ibClose.setOnClickListener { finish() }

        bMagicLink.setOnClickListener {
            analytics.sendEvent(Analytics.SIGN_TAP_MAGIG_LINK)
            val intent = Intent(this, LoginAlternativeActivity::class.java)
                .putExtra(EXTRA_LOGIN_ALTERNATIVE_TYPE, LoginAlternativePresenter.Type.MAGIC_LINK)
            startActivity(intent)
        }

        bManually.setOnClickListener {
            analytics.sendEvent(Analytics.SIGN_TAP_ENTER_MANUALLY)
            startActivity(Intent(this, LoginEmailActivity::class.java))
        }
    }
}
