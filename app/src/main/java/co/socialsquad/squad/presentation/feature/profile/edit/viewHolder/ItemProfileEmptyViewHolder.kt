package co.socialsquad.squad.presentation.feature.profile.edit.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder


class ItemProfileEmptyViewHolder(val itemView: View) :
    ViewHolder<ItemProfileEmptyViewModel>(itemView) {

    override fun bind(viewModel: ItemProfileEmptyViewModel) {
    }

    override fun recycle() {
    }
}