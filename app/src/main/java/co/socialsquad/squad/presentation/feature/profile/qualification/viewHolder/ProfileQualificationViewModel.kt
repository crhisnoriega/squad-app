package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import co.socialsquad.squad.data.entity.Requirement
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.entity.SuccessStories
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.ShareViewModel
import co.socialsquad.squad.presentation.util.Shareable
import java.io.Serializable
import java.util.concurrent.TimeUnit

class ProfileQualificationViewModel(
    val name: String,
    val icon: String,
    val progress: Int,
    val actual: Boolean,
    val target: Boolean,
    val progressBarColor: String,
    val requirements: List<RequirementViewModel>,
    val recomendations: List<RecommendationViewModel>,
    val prizes: List<PrizeViewModel>,
    val stories: List<SuccessStoriesViewModel>,
    val bonus: String?,
    var focused: Boolean = false
) : ViewModel

class PrizeViewModel(
    val category: String,
    val name: String,
    val description: String,
    val photo: String,
    val tintColor: String
) : ViewModel

class RequirementViewModel(
    val pk: Int,
    val name: String,
    val description: String,
    val icon: String,
    val targetValue: Float,
    val value: Float,
    val percentage: Int,
    val tintColor: String?
) : ViewModel, Serializable {
    constructor(requirement: Requirement, color: String?) : this(
        requirement.pk ?: 0,
        requirement.name,
        requirement.description,
        requirement.icon,
        requirement.promotion_value,
        requirement.value,
        ((requirement.value / requirement.promotion_value) * 100).toInt(),
        color
    )
}

class SuccessStoriesViewModel(
    val pk: Long,
    val title: String,
    val description: String,
    val author: String,
    val authorPk: Int,
    val visualization: Long,
    val cover: String,
    val duration: Long?,
    val mediaUrl: String?,
    val mediaPk: Long?,
    val canShare: Boolean = true,
    val tintColor: String,
    val subdomain: String?
) : ViewModel, Shareable {
    override val shareViewModel get() = ShareViewModel(pk.toInt(), title, description, cover, Share.Feed, canShare, subdomain)

    constructor(successStories: SuccessStories, color: String, subdomain: String?) : this(
        successStories.pk,
        successStories.title ?: "",
        successStories.content ?: "",
        successStories.author.user.fullName,
        successStories.author.pk,
        successStories.medias?.firstOrNull()?.viewsCount ?: 0L,
        successStories.cover?.url
            ?: successStories.author.user.avatar ?: "",
        successStories.medias?.firstOrNull()?.timeLength?.let { TimeUnit.SECONDS.toMillis(it) },
        successStories.medias?.firstOrNull()?.streamings?.firstOrNull()?.playlistUrl,
        successStories.medias?.firstOrNull()?.pk?.toLong(),
        true,
        color,
        subdomain
    )
}

class RecommendationViewModel(
    val pk: Int,
    val name: String,
    val description: String,
    val type: String,
    val icon: String,
    val targetValue: Int,
    val value: Int,
    val percentage: Int
) : ViewModel
