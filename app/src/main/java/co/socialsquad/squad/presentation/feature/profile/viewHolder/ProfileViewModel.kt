package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.data.entity.*
import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileViewModel(
        val pk: Int,
        val avatar: String?,
        val firstName: String,
        val lastName: String,
        val role: String?,
        val qualification: String?,
        val qualificationBadge: String?,
        val bio: String?,
        var canSeeQualification: Boolean = false,
        var canCall: Boolean?,
        val chatId: String?,
        val ranking: RankingProfileModel?,
        val wallet_tools: WalletProfileModel?,
        val recognition2: RecognitionProfileModel?,
        val recognition: RecognitionData,
        val personal_points: PersonalPointsData?,
        val completeYourProfile: CompleteYourProfile
) : ViewModel {
    constructor(userCompany: UserCompany) : this(
            userCompany.pk,
            userCompany.user.avatar,
            userCompany.user.firstName ?: "",
            userCompany.user.lastName ?: "",
            userCompany.roleDisplay,
            userCompany.qualification,
            userCompany.qualificationBadge,
            userCompany.bio,
            userCompany.canSeeQualification,
            userCompany.canCall(),
            userCompany.user.chatId,
            userCompany.ranking,
            userCompany.wallet_tools,
            userCompany.recognition2,
            userCompany.recognition,
            userCompany.personal_points,
            userCompany.complete_your_profile
    )
}
