package co.socialsquad.squad.presentation.feature.searchv2.results

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Section
import co.socialsquad.squad.presentation.feature.searchv2.SearchListener
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.SearchEmptyAdapter
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.SearchLoadingAdapter
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.SearchNotFoundAdapter
import co.socialsquad.squad.presentation.feature.searchv2.results.adapter.SearchResultsAdapter
import kotlinx.android.synthetic.main.fragment_searchv2_results.*

private const val EXTRA_SECTION = "ARGUMENT_SECTION"
private const val EXTRA_COLOR = "ARGUMENT_COLOR"

class SearchResultsFragment : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance(
                sectionData: Section?,
                companyColor: String
        ) = SearchResultsFragment().apply {
            arguments = bundleOf(EXTRA_SECTION to sectionData, EXTRA_COLOR to companyColor)
        }
    }

    private var sectionData: Section? = null
    private lateinit var companyColor: String
    private lateinit var emptyAdapter: SearchEmptyAdapter
    private lateinit var notFoundAdapter: SearchNotFoundAdapter
    private lateinit var loadingAdapter: SearchLoadingAdapter
    private lateinit var resultsAdapter: SearchResultsAdapter

    var didSearch: Boolean = false

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_searchv2_results, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sectionData = arguments?.getParcelable(EXTRA_SECTION) as Section?
        companyColor = arguments?.getString(EXTRA_COLOR) as String
        rvResults.layoutManager = LinearLayoutManager(view.context)
        rvResults.setHasFixedSize(true)
        setupEmptyState()
    }

    internal fun setupEmptyState() {
        didSearch = false
        if (!this::emptyAdapter.isInitialized) {
            emptyAdapter = SearchEmptyAdapter(sectionData?.emptyState)
        }
        rvResults.adapter = emptyAdapter
    }

    internal fun setupNotFoundState(query: String) {
        didSearch = false
        if (!this::notFoundAdapter.isInitialized) {
            notFoundAdapter = SearchNotFoundAdapter(query)
        } else {
            notFoundAdapter.updateSearchTerm(query)
        }
        rvResults.adapter = notFoundAdapter
    }

    internal fun setupLoadingState(searchTerm: String?) {
        if (!this::loadingAdapter.isInitialized) {
            loadingAdapter = SearchLoadingAdapter(searchTerm)
        } else {
            Log.i("search", "updateSearchTerm $searchTerm")
            loadingAdapter.updateSearchTerm(searchTerm)
        }

        rvResults.adapter = loadingAdapter
    }

    internal fun setupResults(
            sections: List<Section>,
            hasHeaderAndFooter: Boolean,
            listener: SearchListener,
            isLast: Boolean = false
    ) {
        didSearch = true
        resultsAdapter = SearchResultsAdapter(sections, hasHeaderAndFooter, listener, companyColor)
        rvResults.adapter = resultsAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            sectionData = savedInstanceState.getParcelable(EXTRA_SECTION) as Section?
            companyColor = savedInstanceState.getString(EXTRA_COLOR) as String
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (sectionData != null) {
            outState.putParcelable(EXTRA_SECTION, sectionData)
        }
        if (!companyColor.isNullOrEmpty()) {
            outState.putString(EXTRA_COLOR, companyColor)
        }
    }
}
