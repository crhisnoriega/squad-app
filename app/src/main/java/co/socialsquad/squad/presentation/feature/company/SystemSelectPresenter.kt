package co.socialsquad.squad.presentation.feature.company

import co.socialsquad.squad.data.entity.CustomFieldRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SignupRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_SYSTEM_SELECTION
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private const val SYSTEM_CUSTOM_FIELD = "sistema"

class SystemSelectPresenter @Inject constructor(
    private val systemSelectView: SystemSelectView,
    private val signupRepository: SignupRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {

    private val compositeDisposable = CompositeDisposable()
    private var selectedItemValue: String? = null
    private val items: MutableList<SelectionItemViewModel> = mutableListOf()

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_SYSTEM_SELECTION)
        getSystemList()
    }

    fun selectItem(item: SelectionItemViewModel) {
        if (item.title != null) {
            items.forEach { it.selected = it.title == item.title }
            selectedItemValue = item.title
            systemSelectView.setListItems(items)
            systemSelectView.setSaveButtonEnabled(true)
        }
    }

    private fun storeSystem(system: String) {
        val userCompany = loginRepository.userCompany
        userCompany?.let {
            it.multilevelSystem = system
            loginRepository.userCompany = it
        }
    }

    fun onSaveClicked() {
        val value = selectedItemValue
        value?.let { saveSystem(value) }
    }

    private fun saveSystem(system: String) {
        compositeDisposable.add(
            signupRepository.setCustomField(SYSTEM_CUSTOM_FIELD, CustomFieldRequest(system))
                .doOnSubscribe { systemSelectView.showLoading() }
                .doOnTerminate {
                    systemSelectView.hideLoading()
                    systemSelectView.closeScreen()
                }
                .subscribe(
                    { storeSystem(system) },
                    { it.printStackTrace() }
                )
        )
    }

    private fun getSystemList() {
        compositeDisposable.add(
            signupRepository.getSystemList()
                .doOnSubscribe { systemSelectView.showLoading() }
                .doOnTerminate { systemSelectView.hideLoading() }
                .subscribe(
                    {
                        it.systems.forEach {
                            items.add(SelectionItemViewModel(it.title, it.imageUrl, false))
                        }

                        loginRepository.userCompany?.multilevelSystem?.let { currentSystem ->
                            selectedItemValue = currentSystem
                            items.forEach { it.selected = it.title == currentSystem }
                        }

                        systemSelectView.setListItems(items)
                    },
                    { it.printStackTrace() }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onCreateOptionsMenu() {
        val isSystemSelected = loginRepository.userCompany?.multilevelSystem != null
        systemSelectView.setSaveButtonEnabled(isSystemSelected)
    }
}
