package co.socialsquad.squad.presentation.feature.chat.chatroom

import dagger.Binds
import dagger.Module

@Module
abstract class ChatRoomModule {
    @Binds
    abstract fun view(chatRoomActivity: ChatRoomActivity): ChatRoomView
}
