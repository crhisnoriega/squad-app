package co.socialsquad.squad.presentation.custom

interface InputView {
    fun setOnInputListener(onInputListener: OnInputListener)
    fun isValid(valid: Boolean)

    interface OnInputListener {
        fun onInput()

        fun setInputViews(inputViews: List<InputView>) {
            for (inputView in inputViews) inputView.setOnInputListener(this)
        }
    }
}
