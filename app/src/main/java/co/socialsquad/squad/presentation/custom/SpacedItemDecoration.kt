package co.socialsquad.squad.presentation.custom

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
class SpacedItemDecoration(private val spaceWidth: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            left = spaceWidth
            right = spaceWidth
        }
    }
}
