package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class EventData(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("amount") val amount: String? = "",
    @SerializedName("earning_message") val earning_message: String? = "",
    @SerializedName("icon") val icon: String? = ""
) : Parcelable