package co.socialsquad.squad.presentation.feature.live.list

interface LiveListView {
    fun showMessage(textResId: Int, onDismissListener: () -> Unit? = {})
    fun addItems(lives: List<LiveListItemViewModel>, clear: Boolean)
    fun startLoading()
    fun stopLoading()
    fun showEmptyState()
    fun openVideo(url: String)
}
