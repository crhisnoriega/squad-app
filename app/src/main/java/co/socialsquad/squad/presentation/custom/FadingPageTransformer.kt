package co.socialsquad.squad.presentation.custom

import android.view.View
import androidx.viewpager.widget.ViewPager

class FadingPageTransformer : ViewPager.PageTransformer {
    override fun transformPage(view: View, position: Float) {
        if (position <= -1f || position >= 1f) {
            view.translationX = view.width * position
            view.alpha = 0f
        } else if (position == 0f) {
            view.translationX = view.width * position
            view.alpha = 1f
        } else {
            view.translationX = view.width * -position
            view.alpha = 1f - Math.abs(position)
        }
    }
}
