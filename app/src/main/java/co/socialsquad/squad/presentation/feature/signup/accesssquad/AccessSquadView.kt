package co.socialsquad.squad.presentation.feature.signup.accesssquad

import co.socialsquad.squad.data.entity.Company

interface AccessSquadView {
    fun showLoading()
    fun hideLoading()
    fun addPrivateCompanies(companies: List<Company>)
    fun addPublicCompanies(companies: List<Company>)
    fun showTryAgain()
    fun hideTryAgain()
    fun showListContent()
    fun openNavigation()
    fun openCompleteRegistration()
}
