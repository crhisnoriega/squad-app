package co.socialsquad.squad.presentation.feature.social.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.SocialCreateItemViewModel
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.view_social_create_item.view.*

class SocialCreateItemViewHolder(
    itemView: View,
    private val listener: Listener<SocialCreateItemViewModel>
) : ViewHolder<SocialCreateItemViewModel>(itemView) {
    override fun bind(viewModel: SocialCreateItemViewModel) {
        with(viewModel) {
            itemView.apply {
                short_title_lead.text = context.getString(textResId)
                ivIcon.setImageResource(iconResId)
                if (enabled) {
                    alpha = 1f
                    setOnClickListener { listener.onClick(this@with) }
                } else {
                    alpha = .5f
                    setOnClickListener { ViewUtils.showDialog(context, context.getString(errorTextResId), null) }
                }
            }
        }
    }

    override fun recycle() {}
}
