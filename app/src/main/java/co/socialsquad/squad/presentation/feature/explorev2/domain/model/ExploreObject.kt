package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreObject(
        @SerializedName("id") val id: Int,
        @SerializedName("short_title") val shortTitle: String,
        @SerializedName("short_description") val shortDescription: String?,
        @SerializedName("snippet") val snippet: String?,
        @SerializedName("custom_fields") val customFields: String?,
        @SerializedName("media") val media: SimpleMedia?,
        @SerializedName("link") val link: Link,
        @SerializedName("location") val location: PinLocation,
        var template: SectionTemplate?,
        var last: Boolean = false,
        var first:Boolean = false


) : Parcelable
