package co.socialsquad.squad.presentation.feature.profile.edit

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileEditModule {

    @Binds
    abstract fun profileEditView(profileEditActivity: ProfileEditActivity): ProfileEditView
}
