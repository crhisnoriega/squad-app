package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.loading_view_qualification_header_item.view.*

const val PROFILE_QUALIFICATION_HEADER_LOADING_VIEW_HOLDER_ID = R.layout.loading_view_qualification_header_item

class ProfileQualificationHeaderLoadingViewHolder(itemView: View) : ViewHolder<ProfileQualificationHeaderLoadingViewModel>(itemView) {
    override fun bind(viewModel: ProfileQualificationHeaderLoadingViewModel) {
        itemView.shimmerQualification.startShimmer()
    }

    override fun recycle() {
        itemView.shimmerQualification.stopShimmer()
    }
}
