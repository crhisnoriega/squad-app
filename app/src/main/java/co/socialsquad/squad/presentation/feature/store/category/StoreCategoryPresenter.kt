package co.socialsquad.squad.presentation.feature.store.category

import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.store.EXTRA_CATEGORY
import co.socialsquad.squad.presentation.feature.store.ProductListViewModel
import co.socialsquad.squad.presentation.feature.store.StoreCategoryListItemViewModel
import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class StoreCategoryPresenter @Inject constructor(private val storeCategoryView: StoreCategoryView, private val storeInteractor: StoreInteractor) {
    private val compositeDisposable = CompositeDisposable()
    private var page = 1
    private var pk = 0
    private var title = ""

    fun onCreate(intent: Intent) {
        val category = intent.getSerializableExtra(EXTRA_CATEGORY) as StoreCategoryListItemViewModel

        storeCategoryView.apply {
            setupToolbar(category.title)
            category.cover?.let { setupCover(it) }
            setupList()
            setupSwipeRefresh()
        }

        pk = category.pk
        title = category.title

        getSubcategories()
    }

    fun onRefresh() {
        page = 1
        subscribe()
    }

    private fun getSubcategories() {
        subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            storeInteractor.getSubcategories(pk, page)
                .doOnSubscribe { storeCategoryView.showLoading() }
                .doOnTerminate { storeCategoryView.hideLoading() }
                .subscribe(
                    {
                        storeCategoryView.setItems(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        storeCategoryView.showMessage(R.string.store_category_error_get_subcategories)
                    }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onSubcategoryClicked(category: StoreCategoryListItemViewModel) {
        if (category.hasSubcategories) storeCategoryView.openCategory(category)
        else storeCategoryView.openProducts(ProductListViewModel(category.pk, category.title))
    }
}
