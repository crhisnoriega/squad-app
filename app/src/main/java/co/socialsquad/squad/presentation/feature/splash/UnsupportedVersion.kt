package co.socialsquad.squad.presentation.feature.splash

class UnsupportedVersion : Throwable() {
    override fun getLocalizedMessage(): String = "Unsupported version"
}
