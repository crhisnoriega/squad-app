package co.socialsquad.squad.presentation.feature.revenue.model

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class StreamsResponse(
    @SerializedName("results") val results: List<StreamData>?,
    @SerializedName("count") val count: Int?,
    @SerializedName("next") val next: String?,
    @SerializedName("previous") val previous: String?,
    var first: Boolean = false


    ) : Parcelable