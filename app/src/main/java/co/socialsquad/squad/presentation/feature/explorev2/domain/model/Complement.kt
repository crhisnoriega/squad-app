package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import co.socialsquad.squad.data.typeAdapter.ComplementTypeAdapter
import co.socialsquad.squad.domain.model.explore.ComplementType
import co.socialsquad.squad.domain.model.explore.Contact
import co.socialsquad.squad.domain.model.explore.Price
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Complement(
    @SerializedName("type")
    @JsonAdapter(ComplementTypeAdapter::class)
    val type: ComplementType,
    @SerializedName("address")
    val address: Address?,
    @SerializedName("links")
    val links: LinkGroup?,
    @SerializedName("resource")
    val resource: ResourceGroup?,
    @SerializedName("price")
    val price: Price?,
    @SerializedName("contact")
    val contact: Contact?
) : Parcelable
