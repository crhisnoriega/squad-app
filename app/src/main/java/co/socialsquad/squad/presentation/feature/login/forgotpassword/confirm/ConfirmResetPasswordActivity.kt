package co.socialsquad.squad.presentation.feature.login.forgotpassword.confirm

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Company
import co.socialsquad.squad.presentation.feature.kickoff.KickoffActivity
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.feature.onboarding.OnboardingActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.setGradientBackground
import co.socialsquad.squad.presentation.util.showKeyboard
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_forgot_password.*
import javax.inject.Inject

class ConfirmResetPasswordActivity : BaseDaggerActivity(), ConfirmResetPasswordView {

    companion object {
        const val extraSubdomain = "extra_subdomain"
        const val extraCompany = "extra_company"
        const val extraPassword = "extra_password"
        const val extraToken = "extra_token"
        const val extraEmail = "extra_email"
    }

    @Inject
    lateinit var presenter: ConfirmResetPasswordPresenter

    private var isLoginClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        presenter.onCreate()

        header.visibility = View.VISIBLE
        iltInput.visibility = View.VISIBLE
        nextContainer.visibility = View.VISIBLE
        loading.visibility = View.GONE

        val subdomain = intent?.extras?.getString(extraSubdomain)
        subdomain?.apply {
            iltInput.setHelpText("Confirmar nova senha para $this")
        }

        iltInput.setHintInput("Confirmar nova senha")
        iltInput.addTextChangedListener {
            next.isEnabled = it.length >= 6
            iltInput.isValid(true)
        }

        if (intent.hasExtra(extraCompany)) {
            val gson = Gson()
            val company = gson.fromJson(intent.extras!!.getString(extraCompany), Company::class.java)
            setupHeader(company)
        }

        setupNextButton()
        setupBackButton()
    }

    private fun setupNextButton() {
        next.setText(R.string.reset_password_change_pass)

        next.isEnabled = false
        next.setOnClickListener {
            val subdomain = intent?.extras?.getString(extraSubdomain)
            val token = intent?.extras?.getString(extraToken)
            val password = intent?.extras?.getString(extraPassword)
            password?.let { it1 -> presenter.resetPassword(it1, iltInput.getFieldText().toString(), subdomain!!, token!!) }
        }
    }

    override fun onResume() {
        super.onResume()
        iltInput.requestFocus()
        iltInput.showKeyboard()
    }

    override fun showMessage(resId: Int) {
        iltInput.setErrorText(getString(resId))
    }

    override fun showLoading() {
        iltInput.loading(true)
    }

    override fun hideLoading() {
        iltInput.loading(false)
    }

    override fun showSuccessDialog() {
        presenter.onShowSuccessDialog()

        val dialogBuilder = AlertDialog.Builder(this, R.style.TransparentDialog)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_informative_button, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val alertDialog = dialogBuilder.create()

        val backLogin: TextView = dialogView.findViewById(R.id.backLogin)
        backLogin.setOnClickListener {
            presenter.onBackToHome()
            val intent = Intent(this, OnboardingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        val enterManually: TextView = dialogView.findViewById(R.id.enterManually)
        enterManually.setOnClickListener {
            presenter.onEnterAutomatic()
            isLoginClicked = true
            val email = intent?.extras?.getString(extraEmail)
            val subdomain = intent?.extras?.getString(extraSubdomain)

            presenter.doLogin(email!!, iltInput.getFieldText().toString(), subdomain!!)
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    override fun openProfileScreen() {
        val intent = Intent(this, KickoffActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun openNavigation() {
        hideKeyboard()
        val intent = Intent(this, NavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun setupBackButton() {
        back.setImageResource(R.drawable.ic_back_feedback)
        back.setOnClickListener { finish() }
    }

    private fun setupHeader(company: Company) {
        if (company.isPublic != null && company.isPublic!!) {
            company.aboutGradient?.apply {
                if (this.isNotEmpty()) {
                    setColorsConfiguration(this[0], this[1], company.aboutFooterBackground!!, company.textColor!!)
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        squadHeader.visibility = View.VISIBLE
        squadHeader.bindCompany(company)
    }

    private fun setColorsConfiguration(startColor: String, endColor: String, footerColor: String, textColor: String) {
        container.setGradientBackground(startColor, endColor)
        nextContainer.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))
        footerSeparator.setBackgroundColor(ColorUtils.parse(footerColor))

        val color = ColorUtils.parse(textColor)
        back.setColorFilter(color)

        iltInput.setColors(textColor)

        window.statusBarColor = ColorUtils.parse(startColor)

        setStatusBarColor()
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }
    }
}
