package co.socialsquad.squad.presentation.feature.opportunityv2.model

import android.os.Parcelable
import co.socialsquad.squad.data.entity.opportunity.Issue
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class OpportunityStage(
        @SerializedName("id") val id: String? = null,
        @SerializedName("title") val title: String? = null,
        @SerializedName("description") val description: String? = null,
        @SerializedName("timestamp") val timestamp: String? = null,
        @SerializedName("comission_formatted") val comission_formatted: String? = null,
        @SerializedName("status") val status: String? = null,
        @SerializedName("issue") val issue: Issue? = null,

        ) : Parcelable