package co.socialsquad.squad.presentation.feature.event.detail

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.EventRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_DROP_DETAIL
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_EVENT_CANCEL_ATTENDANCE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_EVENT_CONFIRM_ATTENDANCE
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_EVENT_SHARE
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EventDetailPresenter @Inject constructor(
    private val view: EventDetailView,
    private val tagWorker: TagWorker,
    private val eventRepository: EventRepository,
    loginRepository: LoginRepository,
    private val fusedLocationProviderClient: FusedLocationProviderClient
) {
    private val compositeDisposable = CompositeDisposable()
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    private var eventViewModel: EventViewModel? = null
    private val currentUserPk = loginRepository.userCompany?.user?.pk ?: 0
    private val companyColor = loginRepository.userCompany?.company?.primaryColor
    private val subdomain = loginRepository.subdomain

    fun onCreate(intent: Intent) {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_DROP_DETAIL)
        if (intent.hasExtra(EventDetailActivity.EXTRA_EVENT_VIEW_MODEL)) {
            eventViewModel = intent.getSerializableExtra(EventDetailActivity.EXTRA_EVENT_VIEW_MODEL) as EventViewModel
            showEvent()
        } else if (intent.hasExtra(EventDetailActivity.EXTRA_EVENT_PK)) {
            val pk = intent.extras?.getInt(EventDetailActivity.EXTRA_EVENT_PK) ?: return
            getEvent(pk)
        }
    }

    fun openCalendarClick() {
        eventViewModel?.let { view.showCalendarApp(it) }
    }

    private fun showEvent() {
        view.showActions(true)
        eventViewModel?.let {
            view.showEvent(it)
            view.updateAttendance(it, currentUserPk)
            changeButtonState(it.isAttending)
        }

        if (eventViewModel?.latitude != null && eventViewModel?.longitude != null) {
            if (view.checkPermissions(permissions)) getLastLocation()
            else view.requestPermissions(147, permissions)
        }
    }

    private fun changeButtonState(isAttending: Boolean) {
        view.setButtonEnabled(!isAttending)
        view.setButtonConfirmText(
            if (isAttending)
                R.string.scheduled_live_detail_attendance_confirmed
            else
                R.string.scheduled_live_detail_confirm_attendance
        )
    }

    private fun getEvent(pk: Int) {
        compositeDisposable.add(
            eventRepository.getEvent(pk.toLong())
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        eventViewModel = EventViewModel(it, companyColor, subdomain)
                        showEvent()
                    },
                    {
                        it.printStackTrace()
                        view.showMessage(R.string.event_detail_error_get_info) { view.close() }
                    }
                )
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { onLastLocationReceived(it) }
    }

    private fun onLastLocationReceived(location: Location?) {
        location?.let {
            view.setupDistance(it.latitude, it.longitude, eventViewModel!!)
        } ?: getFineLocation()
    }

    private fun getFineLocation() {
        view.createLocationRequest()
    }

    fun onRequestPermissionsResult(grantResults: IntArray) {
        if (grantResults.isEmpty() || grantResults.any { it == PackageManager.PERMISSION_DENIED }) {
            onLocationRequestFailed()
        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun onLocationRequestCreated(locationRequest: LocationRequest, pendingIntent: PendingIntent?) {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, pendingIntent).addOnSuccessListener {
            getLastLocation()
        }.addOnFailureListener {
            onLocationRequestFailed()
        }
    }

    private fun onLocationRequestFailed() {
        view.showMessage(R.string.event_detail_location_permission_denied)
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_EVENT_SHARE)
    }

    fun onConfirmAttendanceClicked() {
        if (eventViewModel?.isAttending == true) tagWorker.tagEvent(TAG_TAP_EVENT_CANCEL_ATTENDANCE)
        else tagWorker.tagEvent(TAG_TAP_EVENT_CONFIRM_ATTENDANCE)

        eventViewModel?.let {
            compositeDisposable.add(
                eventRepository.confirmAttendance(it.pk.toLong())
                    .doOnSubscribe { view.showLoading() }
                    .doOnTerminate { view.hideLoading() }
                    .subscribe(
                        {
                            eventViewModel = EventViewModel(it, companyColor, subdomain)
                            eventViewModel?.let {
                                view.updateAttendance(it, currentUserPk)
                                view.showSuccessDialog(it)
                                changeButtonState(true)
                            }
                        },
                        { it.printStackTrace() }
                    )
            )
        }
    }
}
