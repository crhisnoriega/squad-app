package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class LevelData(
        @SerializedName("id") val id: Int?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("subtitle") val subtitle: String?,
        @SerializedName("user_info") val user_info: UserLevelData?,
        @SerializedName("requirements") val requirements: RequirementsList?,
        @SerializedName("bonus") val bonus: BonusList?,
        @SerializedName("prizes") val prizes: PrizesList?,
        @SerializedName("benefits") val benefits: BenefitsList?,
        var first: Boolean = false,
        var isSelected: Boolean = false,
        var isBlocked: Boolean = false
) : Parcelable