package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.ProfileEditActivity
import co.socialsquad.squad.presentation.feature.profile.edit.ProfilePictureActivity
import kotlinx.android.synthetic.main.fragment_edit_profile_company_type.*
import org.koin.android.viewmodel.ext.android.viewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3


class ProfileCompanyTypeFragment(
    val title: String? = null,
    val subtitle: String? = null,
    val iconRes: Int? = null,
    val buttonName: String? = "Próximo",
    val isInCompleteProfile: Boolean? = false,
    val value: String = "PF"
) : Fragment() {
    private val viewModel by viewModel<ProfileEditViewModel>()

    private var radioComponents = listOf<RadioButton>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_company_type, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureButtons()
        if (isInCompleteProfile!!) {
            configureForNext()
        } else {
            configureForSave()
        }
        configureObservables()

        when (value) {
            "PF" -> {
                radioPF.isChecked = true
                radioPJ.isChecked = false
            }

            "PJ" -> {
                radioPF.isChecked = false
                radioPJ.isChecked = true
            }
        }

        if (isInCompleteProfile) {

        } else {
            imgLead.setImageResource(iconRes!!)
            txtTitle.text = title
            txtHelpText.text = subtitle
            btnConfirmar.text = buttonName
        }
    }


    private fun configureObservables() {
        viewModel.saveStatus.observe(requireActivity(), Observer {
            when (it) {
                "regime" -> {
                    ProfileEditActivity.reLoad = true
                    activity?.finish()
                }
            }
        })
    }

    private fun hideSoftKeyboard(view: View) {
        val imm =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun configureForSave() {
        btnConfirmar.setOnClickListener {
            showLoading()
            btnConfirmar.isEnabled = false
            viewModel.saveAccountType(if (radioPF.isChecked) "PF" else "PJ")
        }
    }

    private fun configureForNext() {
        btnConfirmar.setOnClickListener {
            if (radioPF.isChecked) {
                var intent =
                    Intent(activity, CompleteProfileActivity::class.java).apply {
                        putExtra(
                            ProfilePictureActivity.EDIT_TYPE_FORM,
                            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_PF
                        )
                    }
                activity?.startActivityForResult(intent, 2001)
            }

            if (radioPJ.isChecked) {

                var intent =
                    Intent(activity, CompleteProfileActivity::class.java).apply {
                        putExtra(
                            ProfilePictureActivity.EDIT_TYPE_FORM,
                            ProfilePictureActivity.Companion.EDIT_TYPE.COMPLETE_YOUR_PAYMENT_PJ
                        )
                    }
                activity?.startActivityForResult(intent, 2001)
            }
        }
    }

    private fun configureButtons() {


        btnBack.setOnClickListener {
            activity?.finish()
        }

        pfLayout.setOnClickListener {
            radioPF.isChecked = true
            radioPJ.isChecked = false

            btnConfirmar.isEnabled = true
        }

        pjLayout.setOnClickListener {
            radioPF.isChecked = false
            radioPJ.isChecked = true

            btnConfirmar.isEnabled = true
        }

        radioPF.setOnClickListener {
            radioPF.isChecked = true
            radioPJ.isChecked = false

            btnConfirmar.isEnabled = true
        }

        radioPJ.setOnClickListener {
            radioPF.isChecked = false
            radioPJ.isChecked = true

            btnConfirmar.isEnabled = true
        }
    }


    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }


    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private var buttonText = ""
    private fun showLoading() {
        buttonText = btnConfirmar.text.toString()
        btnConfirmar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        btnConfirmar.text = buttonText
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
        btnConfirmar.isEnabled = true
    }

}
