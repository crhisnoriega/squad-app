package co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.ValueViewModel

interface SegmentValuesView {
    fun setupToolbar(title: String)
    fun setupList(metric: MetricViewModel)
    fun setupSwipeRefresh()
    fun showErrorMessage(textResId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun setItems(values: List<ValueViewModel>)
    fun addItems(values: List<ValueViewModel>)
    fun enableRefreshing()
    fun disableRefreshing()
    fun stopRefreshing()
    fun showLoading()
    fun hideLoading()
    fun setAddEnabled(enabled: Boolean)
    fun finishWithResult(resultCode: Int, intent: Intent)
    fun updateSelected(viewModel: ValueViewModel)
}
