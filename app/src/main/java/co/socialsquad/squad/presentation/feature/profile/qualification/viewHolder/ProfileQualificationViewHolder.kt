package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.animation.ValueAnimator
import android.os.Handler
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.CENTER_RESIZE_MIN_SCALE
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_qualification_progress_item.view.*

const val QUALIFICATION_PROGRESS_VIEW_HOLDER_ID = R.layout.view_qualification_progress_item

class ProfileQualificationViewHolder(itemView: View) : ViewHolder<ProfileQualificationViewModel>(itemView) {
    private var animator: ValueAnimator? = null

    override fun bind(viewModel: ProfileQualificationViewModel) {
        itemView.progressBar2.progressTintList = ColorUtils.stateListOf(viewModel.progressBarColor)

        if (viewModel.target) {
            Handler().postDelayed({ animate(viewModel.progress) }, 400)
            itemView.alpha = 1F
            itemView.progressBar2.visibility = View.VISIBLE
        } else {
            itemView.progressBar2.visibility = View.GONE
            if (viewModel.progress == 0) itemView.alpha = 0.3F
            else itemView.imageView.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
        }

        Glide.with(itemView.context)
            .load(viewModel.icon)
            .circleCrop()
            .into(itemView.imageView)

        val scale = if (viewModel.focused) 1F else CENTER_RESIZE_MIN_SCALE
        itemView.scaleX = scale
        itemView.scaleY = scale
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.imageView)
    }

    fun animate(progress: Int) {
        animator?.cancel()
        animator = ValueAnimator.ofInt(itemView.progressBar2.progress, progress)?.apply {
            duration = 500
            interpolator = FastOutSlowInInterpolator()
            addUpdateListener {
                val increment = it.animatedValue as Int - itemView.progressBar2.progress
                itemView.progressBar2.incrementProgressBy(increment)
            }
            start()
        }
    }
}
