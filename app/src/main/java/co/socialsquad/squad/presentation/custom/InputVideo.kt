package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_input_video.view.*

class InputVideo(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    interface Listener {
        fun onClick()
    }

    private var listener: Listener? = null

    init {
        View.inflate(context, R.layout.view_input_video, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputVideo)) {
            short_title_lead.text = getString(R.styleable.InputVideo_name)
            ivImage.setImageResource(getResourceId(R.styleable.InputVideo_image, 0))
            recycle()
        }
    }

    private fun setupViews() {
        rootView.setOnClickListener { listener?.onClick() }
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }
}
