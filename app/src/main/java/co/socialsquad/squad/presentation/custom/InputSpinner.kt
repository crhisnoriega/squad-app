package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.view_input_spinner.view.*
import java.util.ArrayList

class InputSpinner<T : InputSpinner.Item>(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    interface Item {
        val title: String?
    }

    interface ItemEnum : Item {
        val titleResId: Int
        override var title: String

        companion object {
            fun <T : ItemEnum> values(context: Context, values: Array<T>): List<T> {
                val items = ArrayList<T>()
                for (t in values) {
                    t.title = context.resources.getString(t.titleResId)
                    items.add(t)
                }
                return items
            }
        }
    }

    private var onInputListener: InputView.OnInputListener? = null

    private val onItemSelectedListeners = arrayListOf<AdapterView.OnItemSelectedListener>()

    private val labelAnimator = ObjectAnimator
        .ofFloat(12f, 11f)
        .apply {
            duration = 200L
            addUpdateListener { tvLabel.textSize = it.animatedValue as Float }
        }

    var items = listOf<T>()
        set(value) {
            field = value
            sList.adapter = ArrayAdapter<String>(context, R.layout.view_input_spinner_item, value.map { it.title })
            sList.isSelected = false
            sList.setSelection(0, false)
            sList.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (tvSelection.visibility == View.GONE) {
                        labelAnimator.start()
                        tvSelection.visibility = View.VISIBLE
                    }

                    tvSelection.text = sList.selectedItem?.toString()

                    selectedItem = items.getOrNull(selectedItemPosition)

                    onInputListener?.onInput()
                    onItemSelectedListeners.forEach { it.onItemSelected(parent, view, position, id) }
                }
            }
            selectedItem = null
        }

    var selectedItem: T? = null
        set(value) {
            if (items.contains(value)) {
                if (tvSelection.visibility == View.GONE) {
                    labelAnimator.start()
                    tvSelection.visibility = View.VISIBLE
                }
                field = value
            } else field = null
        }

    var selectedItemPosition
        get() = sList.selectedItemPosition
        set(value) {
            sList.setSelection(value)

            if (tvSelection.visibility == View.GONE) {
                labelAnimator.start()
                tvSelection.visibility = View.VISIBLE
            }
        }

    init {
        View.inflate(context, R.layout.view_input_spinner, this)
        setupStyledAttributes(attrs)
        rootView.setOnClickListener { sList.performClick() }
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputSpinner)) {
            tvLabel.text = getString(R.styleable.InputSpinner_hint)
            recycle()
        }
    }

    fun show() {
        sList.performClick()
    }

    fun clear() {
        selectedItem = null
        sList.adapter = null
        sList.onItemSelectedListener = null

        if (tvSelection.visibility == View.VISIBLE) {
            tvSelection.visibility = View.GONE
            with(labelAnimator) {
                end()
                reverse()
            }
        }

        onInputListener?.onInput()
    }

    fun addOnItemSelectedListener(onItemSelectedListener: AdapterView.OnItemSelectedListener) {
        onItemSelectedListeners.add(onItemSelectedListener)
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    override fun isValid(valid: Boolean) {}
}
