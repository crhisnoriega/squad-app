package co.socialsquad.squad.presentation.feature.audio.detail

import co.socialsquad.squad.presentation.feature.audio.AudioViewModel

interface AudioDetailView {
    fun showInvalidPk()
    fun showAudio(audioViewModel: AudioViewModel)
    fun showMessage(textResId: Int, onDismissListener: (() -> Unit) = {})
    fun close()
    fun startLoading()
    fun stopLoading()
}
