package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import android.webkit.MimeTypeMap
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionIcon
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RichTextResourcesDetails(
    @SerializedName("icon")
    val icon: SectionIcon?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("url")
    val url: String?
) : Parcelable {
    fun getMimeType(): String {
        return try {
            url?.let {
                var type: String? = null
                val extension = MimeTypeMap.getFileExtensionFromUrl(url)
                if (extension != null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
                }
                type?.let { it } ?: run { "*/*" }
            } ?: run { "*/*" }
        } catch (e: Exception) {
            "*/*"
        }
    }
}
