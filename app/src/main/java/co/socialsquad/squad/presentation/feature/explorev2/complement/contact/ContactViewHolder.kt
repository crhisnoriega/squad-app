package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder

class ContactViewHolder(
    itemView: View,
    private var onClick: (model: ContactViewHolderModel) -> Unit = {}
) : ViewHolder<ContactViewHolderModel>(itemView) {

    private val imgIcon: ImageView = itemView.findViewById(R.id.imgIcon)
    private val tvTitle:TextView = itemView.findViewById(R.id.tvTitle)
    private val tvTitleBold:TextView = itemView.findViewById(R.id.tvTitleBold)

    companion object{
        const val VIEW_ITEM_POSITION_ID = R.layout.item_complement_options_dialog
    }

    override fun bind(viewModel: ContactViewHolderModel) {
        itemView.setOnClickListener {
            onClick.invoke(viewModel)
        }
        if(viewModel.phone != null) {
            tvTitleBold.text = itemView.context.getString(R.string.contact_options_call_bold)
            tvTitle.text = itemView.context.getString(R.string.contact_options_call, viewModel.phone)
            imgIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_action_sheet_options_action_call))
        }
        if(viewModel.email != null) {
            tvTitleBold.text = itemView.context.getString(R.string.contact_options_mail_bold)
            tvTitle.text = itemView.context.getString(R.string.contact_options_mail, viewModel.email)
            imgIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_i_os_light_mode_action_sheet_options_action_e_mail))
        }
    }

    override fun recycle() {
    }
}