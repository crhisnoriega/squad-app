package co.socialsquad.squad.presentation.custom.multimedia

import android.os.Handler
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.ViewMediaType

interface ViewTypeDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?): MediaViewHolder

    fun onBindViewHolder(holder: MediaViewHolder, item: ViewMediaType, label: String? = "", isFirst: Boolean = false, showLabel: Boolean = true) {
        Handler().postDelayed({
            holder.bind(item as Media, label, isFirst)
        }, 200)
    }
}
