package co.socialsquad.squad.presentation.feature.existinglead.adapter.loading

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_lead_not_found_state.view.*

class LeadNotFoundViewHolder(val parent: ViewGroup, val searchTerm: String) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_lead_not_found_state)) {

    fun bind() {
        itemView.tvDescription.text = parent.context.resources.getString(R.string.search_no_results_description_lead, searchTerm)
    }
}
