package co.socialsquad.squad.presentation.feature.settings

interface SettingsView : SharingAppView {
    fun showWalkthroughAfterLogout()
}
