package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class LinkGroup(
        @SerializedName("title") val title: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("multiple_links") val multiple_links: Boolean?,
        @SerializedName("links") val links: List<LinkV2>?,
) : Parcelable
