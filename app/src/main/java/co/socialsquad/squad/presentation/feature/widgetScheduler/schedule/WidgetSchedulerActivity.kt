package co.socialsquad.squad.presentation.feature.widgetScheduler.schedule

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.Header
import co.socialsquad.squad.domain.model.Position
import co.socialsquad.squad.domain.model.PositionItem
import co.socialsquad.squad.domain.model.ScaleShift
import co.socialsquad.squad.presentation.App
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.SnapHelperOneByOne
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.widgetScheduler.di.WidgetScheduleModule
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleShiftVO
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleTimeVO
import co.socialsquad.squad.presentation.feature.widgetScheduler.scheduleSummary.ScheduleSummaryViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_widget_scheduler.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

private const val EXTRA_TIMETABLE_ID = "extra_timetable_id"
private const val EXTRA_OBJECT_ID = "extra_object_id"
private const val EXTRA_SELECTED_DATE = "extra_selected_date"
private const val EXTRA_SELECTED_SHIFT = "extra_selected_shift"
private const val EXTRA_SELECTED_POSITION = "extra_selected_position"
private const val TIMEBOX_DISABLED_VIEW_ID = R.layout.item_widget_scheduler_timebox_disabled
private const val TIMEBOX_ITEM_VIEW_ID = R.layout.item_widget_scheduler_timebox
private const val TIMEBOX_HEADER_ITEM_VIEW_ID = R.layout.item_widget_scheduler_timebox_header
private const val DAY_ITEM_VIEW_ID = R.layout.item_widget_scheduler_day
private const val WEEK_SIZE = 7

class WidgetSchedulerActivity : BaseActivity() {

    override val modules: List<Module> = listOf(WidgetScheduleModule.instance)
    override val contentView = R.layout.activity_widget_scheduler

    private val viewModel by viewModel<ScheduleSummaryViewModel>()
    private val timetableId by extra<Long>(EXTRA_TIMETABLE_ID)
    private val objectId by extra<Long>(EXTRA_OBJECT_ID)
    private val extraSelectedPosition by extra<PositionItem?>(EXTRA_SELECTED_POSITION)
    private val extraSelectedDate by extra<Date?>(EXTRA_SELECTED_DATE)
    private val extraSelectedShift by extra<ScaleShift?>(EXTRA_SELECTED_SHIFT)
    private val snapHelper = SnapHelperOneByOne()
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    private var positionSelected: PositionItem? = null
    private var dateSelected: Date? = null
    private var navigationDate: Calendar? = null
    private var shiftSelected: ScaleShiftVO? = null

    private val timeboxChecked: (scaleSfhitVO: ScaleShiftVO, index: Int) -> Unit = { scaleSfhitVO, index ->
        weekDayAdapter.items.forEach {
            if (it is WeekDayViewHolderModel) {
                it.times.forEach { scaleTimeVO ->
                    scaleTimeVO.shifts.forEach { shiftVO ->
                        shiftVO.isSelected = false
                    }
                }
            }
        }

        var startDate = Calendar.getInstance()
        startDate.time = scaleSfhitVO.scaleShift.startDate

        if ((startDate.timeInMillis < Calendar.getInstance().timeInMillis)) {
            timeboxAdapter.items.removeAt(index)
            timeboxAdapter.items.add(index, GenericViewHolderModel())
        } else {
            timeboxAdapter.items.forEach {
                if (it is TimeboxViewHolderModel) {
                    it.shift.isSelected = it.shift == scaleSfhitVO
                    if (it.shift.isSelected) {
                        shiftSelected = scaleSfhitVO
                    }
                }
            }
        }




        Handler().postDelayed({
            timeboxAdapter.notifyDataSetChanged()
        }, 1)
        checkButtonNextEnable()
    }

    private val timeboxAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is TimeboxViewHolderModel -> TIMEBOX_ITEM_VIEW_ID
            is TimeboxHeaderViewHolderModel -> TIMEBOX_HEADER_ITEM_VIEW_ID
            is GenericViewHolderModel -> TIMEBOX_DISABLED_VIEW_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            TIMEBOX_ITEM_VIEW_ID -> TimeboxViewHolder(view, timeboxChecked)
            TIMEBOX_HEADER_ITEM_VIEW_ID -> TimeboxHeaderViewHolder(view)
            TIMEBOX_DISABLED_VIEW_ID -> GenericViewHolder(view)
            else -> throw IllegalArgumentException()
        }
    })

    private val weekDayAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel) = when (viewModel) {
            is WeekDayViewHolderModel -> DAY_ITEM_VIEW_ID
            else -> throw IllegalArgumentException()
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            DAY_ITEM_VIEW_ID -> WeekDayViewHolder(view, dayClick)
            else -> throw IllegalArgumentException()
        }
    })

    private fun isHourAfterNow(calendar: Calendar): Boolean {
        Log.i("tag4", calendar.timeInMillis.toString())
        Log.i("tag4", Calendar.getInstance().timeInMillis.toString())
        return calendar.timeInMillis > Calendar.getInstance()
                .timeInMillis
    }

    private fun isSameDay(cal1: Calendar, cal2: Calendar) =
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)

    private val dayClick: (scaleTime: ScaleTimeVO) -> Unit = {
        Log.i("tag2", Gson().toJson(it))

        dateSelected = it.date

        txtDay.text = it.formattedDate

        Handler().postDelayed({
            weekDayAdapter.setItems(weekDayAdapter.items.map { viewModel ->
                if (viewModel is WeekDayViewHolderModel) {
                    viewModel.times.map { stVO ->
                        stVO.isSelected = stVO == it
                        stVO.firstClickPerformed = true
                    }
                }
                viewModel
            })
        }, 100)

        val itens = mutableListOf<ViewModel>()
        it.shifts.forEachIndexed { index, scaleShiftVO ->
            var startDate = Calendar.getInstance()
            startDate.time = scaleShiftVO.scaleShift.startDate

            itens.add(TimeboxHeaderViewHolderModel(scaleShiftVO.scaleShift.startDate))

            val isAfter = isHourAfterNow(startDate)

            Log.i("tag4", "isAvalilable: " + scaleShiftVO.scaleShift.available + " isAfter: " + isAfter + " dateEnd: " + startDate.time)

            if (scaleShiftVO.scaleShift.available && (startDate.timeInMillis >= Calendar.getInstance().timeInMillis)) {
                itens.add(TimeboxViewHolderModel(scaleShiftVO, itens.size))
            } else {
                itens.add(GenericViewHolderModel())
            }

            // end date
            if (index + 1 == it.shifts.size) {
                itens.add(TimeboxHeaderViewHolderModel(scaleShiftVO.scaleShift.endDate))
            }
        }
        timeboxAdapter.setItems(itens)
    }

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigationDate = Calendar.getInstance()

        if (savedInstanceState == null) {
            extraSelectedDate?.let {
                dateSelected = it
                navigationDate!!.time = it
            }
            extraSelectedPosition?.let {
                positionSelected = it
            }
            extraSelectedShift?.let {
                shiftSelected = ScaleShiftVO(it, true)
            }
        }

        setupObservables()
        setupObservablesPositions()
        setupBackButton()
        checkButtonNextEnable()

        gotoNext.setOnClickListener {
            goToNextWeek()
        }
        goToBack.setOnClickListener {
            goToBackWeek()
        }

        rvTimebox.adapter = timeboxAdapter
        rvDays.adapter = weekDayAdapter
        snapHelper.attachToRecyclerView(rvDays)

        fetchScaleDatePositions()
    }

    private var currentDay = 0
    private fun goToNextWeek() {
        navigationDate?.let {
            currentDay += 7
            it.add(Calendar.DATE, 7)
            fetchScaleDate()
        }
    }

    private fun goToBackWeek() {
        navigationDate?.let {
            currentDay -= 7
            it.add(Calendar.DATE, -7)
            fetchScaleDate()
        }
    }

    private fun checkButtonNextEnable() {
        btnAction.isEnabled = dateSelected != null && shiftSelected != null
    }

    @ExperimentalCoroutinesApi
    private fun fetchScaleDatePositions() {
        lifecycleScope.launch {
            viewModel.fetchScaleDatePositions(
                    timetableId = timetableId,
                    objectId = objectId
            )
        }
    }

    @ExperimentalCoroutinesApi
    private fun fetchScaleDate() {
        lifecycleScope.launch {
            viewModel.fetchScaleDate(
                    timetableId = timetableId,
                    objectId = objectId,
                    position = positionSelected?.id,
                    date = navigationDate?.let {
                        dateFormat.format(it.time)
                    }
            )
        }
    }

    private fun setupBackButton() {
        ibClose.setOnClickListener {
            finish()
        }
    }


    @ExperimentalCoroutinesApi
    private fun setupObservables() {
        createFlow(viewModel.scaleDate)
                .error {
                    llLoading.isVisible = false
                    // TODO
                }
                .loading {
                    llLoading.isVisible = true
                }
                .success {
                    llLoading.isVisible = false
                    it?.let { scale ->
                        setHeader(scale.header)
                        setupPosition(scale.position)
                        setupNextButton(scale.button)

                        val timeVOs = scale.dates.map { time ->


                            var ddd = Calendar.getInstance();
                            ddd.time = time.date
                            ddd.add(Calendar.DAY_OF_MONTH, 1)

                            time.date = ddd.time

                            Log.i("tag3", Gson().toJson(time))


                            var dateScale = Calendar.getInstance()
                            dateScale.time = time.date

                            var dateScaleAfter = Calendar.getInstance()
                            dateScaleAfter.time = time.date
                            dateScaleAfter.add(Calendar.DAY_OF_MONTH, +1)

                            val isEnablePastDate =
                                    time.shifts.isNotEmpty() && time.formattedDate.isNotBlank() &&
                                            dateScaleAfter.after(Calendar.getInstance())

                            val isSameDayNavigation = isSameDay(navigationDate?.let { cal ->
                                cal
                            } ?: run {
                                Calendar.getInstance()
                            }, dateScale)
                            val isSameDayActual = isSameDay(Calendar.getInstance(), dateScale)

                            ScaleTimeVO(
                                    time.date,
                                    time.formattedDate,
                                    time.shifts.flatMap { scaleShift ->
                                        listOf(ScaleShiftVO(scaleShift, false))
                                    },
                                    isEnablePastDate,
                                    isSameDayNavigation,
                                    isSameDayActual,
                                    false
                            )
                        }.chunked(WEEK_SIZE)

                        val viewModels = timeVOs.map { scaleTimes ->
                            WeekDayViewHolderModel(scaleTimes, App.companyColor)
                        }
                        weekDayAdapter.setItems(viewModels)


                        if (currentDay == 0) {
                            goToBack.visibility = View.GONE
                        } else {
                            goToBack.visibility = View.VISIBLE
                        }

                        if (currentDay >= 14) {
                            gotoNext.visibility = View.GONE
                        } else {
                            gotoNext.visibility = View.VISIBLE
                        }

                        llLoading.isVisible = false
                    }
                }
                .launch()
    }

    @ExperimentalCoroutinesApi
    private fun setupObservablesPositions() {
        createFlow(viewModel.scaleDatePositions)
                .error {
                    llLoading.isVisible = false
                    // TODO
                }
                .loading {
                    llLoading.isVisible = true
                }
                .success {
                    Log.i("tag", "setupObservablesPositions")
                    llLoading.isVisible = false
                    it?.let { scale ->
                        positionSelected?.let { positionItem ->
                            selectPositionById(scale.position, positionItem.id)
                        } ?: run {
                            scale.position.items.firstOrNull()?.let { positionItem ->
                                selectPositionById(scale.position, positionItem.id)
                            }
                        }
                    }
                }
                .launch()
    }

    private fun setupNextButton(button: String) {
        btnAction.text = button
        btnAction.setOnClickListener {
            if (dateSelected != null && shiftSelected != null) {
                setResult(RESULT_OK, Intent().apply {
                    putExtra(RESULT_SELECTED_DATE, dateSelected!!.time)
                    putExtra(RESULT_SELECTED_SHIFT, shiftSelected!!.scaleShift!!)
                    positionSelected?.let {
                        putExtra(RESULT_SELECTED_POSITION, it)
                    }
                })
                finish()
            }
        }
    }

    private fun setupPosition(position: Position) {

        bgPosition.setOnClickListener {
            val items = position.items.map { item ->
                PositionViewHolderModel(
                        item.id,
                        item.icon,
                        item.title,
                        item.description,
                        positionSelected?.id == item.id
                )
            }.toMutableList()

            val dialog = PositionBottomDialog.newInstance(items)
            dialog.isCancelable = false
            dialog.show(supportFragmentManager, null)
            dialog.onClick = {
                dialog.dismiss()
                if (it.id != 0L) {
                    selectPositionById(position, it.id)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun selectPositionById(position: Position, id: Long) {
        lifecycleScope.launch {
            positionSelected = position.items.firstOrNull { item ->
                item.id == id
            }
            positionSelected?.let { item ->
                Log.i("tag", Gson().toJson(item))
                showPosition(item)
                fetchScaleDate()
                checkButtonNextEnable()
            }
            if (position.items.size == 1) {
                icPositionDropDown.visibility = View.GONE
            } else {
                icPositionDropDown.visibility = View.VISIBLE
            }
        }
    }

    private fun showPosition(item: PositionItem) {
        item.icon?.let {
            Glide.with(this)
                    .load(it.url)
                    .crossFade()
                    .into(imgPosition)
        }
        txtPosition.text = item.title
        txtPosition.visibility = View.VISIBLE
        imgPosition.visibility = View.VISIBLE
        bgPosition.visibility = View.VISIBLE
    }

    private fun setHeader(header: Header) {
        txtTitle.text = header.title
        txtSubtitle.text = header.subtitle
    }

    companion object {
        const val RESULT_SELECTED_DATE = "selected_date"
        const val RESULT_SELECTED_POSITION = "selected_position"
        const val RESULT_SELECTED_SHIFT = "selected_shift"
        const val REQUEST_SELECT_DATE = 22234
        fun startForResult(
                activity: Activity,
                timetableId: Long,
                objectId: Long,
                selectedDate: Date?,
                selectedPositionItem: PositionItem?,
                selectedShift: ScaleShift?,
                code: Int
        ) {
            activity.startActivityForResult(
                    Intent(activity, WidgetSchedulerActivity::class.java).apply {
                        putExtra(EXTRA_TIMETABLE_ID, timetableId)
                        putExtra(EXTRA_OBJECT_ID, objectId)
                        selectedDate?.let {
                            putExtra(EXTRA_SELECTED_DATE, selectedDate)
                        }
                        selectedPositionItem?.let {
                            putExtra(EXTRA_SELECTED_POSITION, selectedPositionItem)
                        }
                        selectedShift?.let {
                            putExtra(EXTRA_SELECTED_SHIFT, it)
                        }
                    }, code
            )
        }
    }
}
