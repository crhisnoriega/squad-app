package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.feature.video.VideoManager
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ui.PlayerView
import kotlinx.android.synthetic.main.view_video.view.*

@Deprecated("use co.socialsquad.squad.presentation.custom.multimedia.video.VideoView instead")
class VideoView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private val rlVideo: RelativeLayout
    private val sepvVideo: PlayerView
    private val ivThumbnail: ImageView
    private val ivPlay: ImageView
    private val ibMute: ImageButton

    val video get() = sepvVideo

    var audioEnabled = true

    private var uri: Uri? = null

    init {
        inflate(context, R.layout.view_video, this)

        rlVideo = video_rl_video
        sepvVideo = video_sepv_video
        ivThumbnail = video_iv_thumbnail
        ivPlay = video_iv_play
        ibMute = video_ib_mute

        rlVideo.setOnClickListener {
            if (VideoManager.isPlaying()) {
                VideoManager.pause()
                val intent = Intent(context, VideoActivity::class.java)
                intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, uri.toString())
                context.startActivity(intent)

                ivThumbnail.visibility = View.VISIBLE
                ivPlay.visibility = View.VISIBLE
                ibMute.visibility = View.GONE
            } else {
                VideoManager.play()

                ivThumbnail.visibility = View.GONE
                ivPlay.visibility = View.GONE
                ibMute.visibility = View.VISIBLE
            }
        }

        ibMute.setImageResource(R.drawable.ic_audio_on)

        ibMute.setOnClickListener {
            if (audioEnabled) {
                VideoManager.mute()
                ibMute.setImageResource(R.drawable.ic_audio_off)
            } else {
                VideoManager.unmute()
                ibMute.setImageResource(R.drawable.ic_audio_on)
            }

            audioEnabled = !audioEnabled
        }
    }

    fun prepare(uri: Uri, position: Long? = null) {
        this.uri = uri
        VideoManager.prepare(context, uri, sepvVideo, audioEnabled, position)
        ibMute.setImageResource(if (audioEnabled) R.drawable.ic_audio_on else R.drawable.ic_audio_off)
        VideoManager.listener = object : VideoManager.Listener {
            override fun onPlayerEnded() {
                ivThumbnail.visibility = View.VISIBLE
                ivPlay.visibility = View.VISIBLE
                ibMute.visibility = View.GONE
            }
        }
    }

    fun pause() {
        VideoManager.pause()
        ivPlay.visibility = View.VISIBLE
        ibMute.visibility = View.GONE
    }

    fun release() {
        VideoManager.release()
    }

    fun loadThumbnail(url: String?) {
        url?.let {
            Glide.with(context).load(url).into(ivThumbnail)
        }
    }

    fun play() {
        rlVideo.performClick()
    }
}
