package co.socialsquad.squad.presentation.feature.hub

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.getScreenWidth
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_hub.*
import javax.inject.Inject

class HubFragment : Fragment(), HasSupportFragmentInjector, HubView {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var hubPresenter: HubPresenter

    private var backgroundColor: Int? = R.color.white

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? = fragmentDispatchingAndroidInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_hub, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        hubPresenter.onViewCreated()
    }



    override fun setupToolbar(logo: String?, backgroundColor: String?) {
        Glide.with(this)
                .load(logo)
                .crossFade()
                .into(ivIcon)

        backgroundColor?.let { Color.parseColor(it) }?.let {
            this.backgroundColor = it
            tToolbar.setBackgroundColor(it)
            setStatusBarColor()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        view?.visibility = if (!isVisibleToUser) View.INVISIBLE else View.VISIBLE
        if (!isVisibleToUser) clearStatusBarColor() else setStatusBarColor()
    }

    override fun onResume() {
        super.onResume()
        // activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        activity?.window?.decorView?.systemUiVisibility = 0
    }

    private fun setStatusBarColor() {
        activity?.window?.apply {
            backgroundColor?.let { statusBarColor = it }
            //decorView.systemUiVisibility = 0
        }
    }

    private fun clearStatusBarColor() {
//        activity?.window?.apply {
//            statusBarColor = ContextCompat.getColor(context, R.color.white)
//            decorView.systemUiVisibility = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR else 0
//        }0
    }

    override fun setupTabs(itemsPermitted: MutableList<HubItem>, indicatorColor: String?, textColor: String?) {
        indicatorColor?.let { tlTabs.setSelectedTabIndicatorColor(ColorUtils.parse(it)) }

        val selectedTextColor = Color.parseColor(textColor ?: indicatorColor)
        val defaultTextColor = Color.argb(128, Color.red(selectedTextColor), Color.green(selectedTextColor), Color.blue(selectedTextColor))
        tlTabs.setTabTextColors(defaultTextColor, selectedTextColor)

        val fragments = itemsPermitted.map { it.fragment }.toTypedArray()

        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(selectedTextColor)
                            TextUtils.applyTypeface(context, "fonts/gotham_bold.ttf", text.toString(), text.toString())
                        } else {
                            setTextColor(defaultTextColor)
                            TextUtils.applyTypeface(context, "fonts/gotham_medium.ttf", text.toString(), text.toString())
                        }
                    }
                }
            }
        })

        vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                tlTabs.getTabAt(position)?.select()
            }
        })

        itemsPermitted.forEach {
            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_hub_tab, tlTabs, false).apply {
                    findViewById<TextView>(R.id.tvTitle)?.text = getString(it.titleResId)
                    findViewById<TextView>(R.id.tvTitle)?.setTextColor(defaultTextColor)
                    (layoutParams as ViewGroup.MarginLayoutParams)
                            .setMargins(resources.getDimensionPixelSize(R.dimen.dashboard_tab_spacing), 0, resources.getDimensionPixelSize(R.dimen.dashboard_tab_spacing), 0)
                }
                view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                tlTabs.layoutParams.height = view.measuredHeight
                customView = view
            }
            tlTabs.addTab(tab)
        }

        tlTabs.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if (tlTabs.measuredWidth < activity?.getScreenWidth() ?: 0) {
            tlTabs.setPadding(0, 0, 0, 0)
        }

        vpContent.adapter = HubFragmentPagerAdapter(childFragmentManager, fragments)
        vpContent.offscreenPageLimit = itemsPermitted.size

        if (itemsPermitted.size > 1) {
            tlTabs.visibility = View.VISIBLE
        }
    }

    class HubFragmentPagerAdapter : FragmentPagerAdapter {

        private var fragments: Array<Fragment>? = null
        private var titles: Array<String>? = null

        constructor(fm: FragmentManager, fragments: Array<Fragment>) : super(fm) {
            this.fragments = fragments
        }

        constructor(fm: FragmentManager, fragments: Array<Fragment>, titles: Array<String>) : super(fm) {
            this.fragments = fragments
            this.titles = titles
        }

        override fun getItem(position: Int): Fragment {
            return fragments!![position]
        }

        override fun getCount(): Int {
            return fragments!!.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return if (titles != null) {
                titles!![position]
            } else super.getPageTitle(position)
        }
    }
}
