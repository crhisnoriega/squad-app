package co.socialsquad.squad.presentation.feature.social

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import co.socialsquad.squad.presentation.custom.ViewModel
import java.io.File

interface SocialView {
    fun setupList(audioEnabled: Boolean)
    fun requestPermissions(requestCode: Int, permissions: Array<String>)
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun hideDeleteDialog()
    fun setItems(posts: List<SocialViewModel>)
    fun addItems(posts: List<SocialViewModel>)
    fun addItems(position: Int, socialViewModel: SocialViewModel)
    fun addPost(post: SocialViewModel)
    fun enableRefreshing()
    fun startRefreshing()
    fun stopRefreshing()
    fun startLoading()
    fun stopLoading()
    fun removePost(viewModel: ViewModel)
    fun showPhotoChooser(file: File, requestCode: Int)
    fun showVideoChooser(file: File, requestCode: Int)
    fun showGalleryChooser(requestCode: Int)
    fun showAudioChooser(requestCode: Int)
    fun showCreateActionButton(isVisible: Boolean)
    fun showCreateLive(intent: Intent)
    fun showCreateFromPhoto(intent: Intent, requestCode: Int)
    fun showCreateFromVideo(intent: Intent, requestCode: Int)
    fun showCreateFromGallery(intent: Intent, requestCode: Int)
    fun showEditMedia(post: PostViewModel)
    fun showEditLive(post: PostLiveViewModel)
    fun showLoading()
    fun hideLoading()
    fun showUploadContainer(thumbnailMedia: Bitmap, isVideoMedia: Boolean, currentUpload: Int = 1, totalUpload: Int = 1)
    fun hideUploadContainer()
    fun startUpload(text: String)
    fun setUploadError(text: String)
    fun showErrorMessageWaitCurrentUpload()
    fun incrementUploadProgressBy(increment: Int)
    fun onReturnVideo(playbackPosition: Long)
    fun showCreateDialog(canCreateFromCamera: Boolean, canCreateFromGallery: Boolean, canCreateLive: Boolean, canCreateAudio: Boolean)
    fun updatePost(post: PostViewModel)
    fun updateFeed()
    fun scrollToTop()
    fun startUploadIndeterminate(string: String)
    fun openAudioCreate(uri: Uri)
    fun openAudioRecord()
    fun showMessagesBadge(messagesNumeber: Int)
    fun hideMessagesBadge()
    fun setMessagesBadgeColor(color: String)
    fun showChat()
    fun hideChat()
    fun showEmptyState()
    fun hideEmptyState()
}
