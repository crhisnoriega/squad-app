package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PositionData(
        @SerializedName("id") val id: String?,
        @SerializedName("position") val position: String?,
        @SerializedName("ranking") val ranking: Int?,
        @SerializedName("score") val score: String?,
        @SerializedName("tiebreaker") val tiebreaker: Int?,
        @SerializedName("user") val user: UserPositionData?
) : Parcelable