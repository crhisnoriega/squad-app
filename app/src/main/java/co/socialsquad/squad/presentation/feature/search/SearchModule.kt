package co.socialsquad.squad.presentation.feature.search

import co.socialsquad.squad.presentation.feature.search.results.SearchResultsFragment
import co.socialsquad.squad.presentation.feature.search.results.SearchResultsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface SearchModule {
    @ContributesAndroidInjector(modules = [SearchResultsModule::class])
    fun searchResultsFragment(): SearchResultsFragment
}
