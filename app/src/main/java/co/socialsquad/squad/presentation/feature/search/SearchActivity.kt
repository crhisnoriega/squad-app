package co.socialsquad.squad.presentation.feature.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.feature.search.results.SearchResultsFragment
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.getScreenWidth
import com.google.android.material.tabs.TabLayout
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.view_search_toolbar.*
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import javax.inject.Inject

class SearchActivity : BaseDaggerActivity(), HasSupportFragmentInjector {

    interface Listener {
        fun onQueryChanged(query: String)
    }

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var loginRepository: LoginRepository

    private val itemsPermitted by lazy {
        mutableListOf<SearchItem>().apply {
            add(SearchItem.All)
            val company = loginRepository.userCompany?.company
            if (company?.exploreProducts == true) add(SearchItem.Products)
            if (company?.exploreImmobile == true) add(SearchItem.Immobiles)
            if (company?.exploreEvents == true) add(SearchItem.Events)
            if (company?.exploreLives == true) add(SearchItem.Lives)
            if (company?.exploreAudios == true) add(SearchItem.Audios)
            if (company?.exploreVideos == true) add(SearchItem.Videos)
            if (company?.exploreStores == true) add(SearchItem.Stores)
            if (company?.exploreDocuments == true) add(SearchItem.Documents)
            if (company?.exploreTrips == true) add(SearchItem.Trips)
        }
    }

    private val fragments by lazy {
        itemsPermitted.map { it.fragment }.toTypedArray()
    }

    override fun supportFragmentInjector() = fragmentInjector

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setupToolbar()
        setupTabs()
    }

    private fun setupToolbar() {
        setSupportActionBar(tToolbar)

        ibClear.visibility = View.VISIBLE
        ibClear.setOnClickListener { etSearch.setText("") }

        etSearch.apply {
            requestFocus()
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    searchQuery(s.toString())
                }
            })
        }
    }

    private fun setupTabs() {
        tlTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabSelected(tab: TabLayout.Tab) {
                vpContent.currentItem = tab.position
                repeat(tlTabs.tabCount) {
                    tlTabs.getTabAt(it)?.customView?.findViewById<TextView>(R.id.tvTitle)?.apply {
                        if (it == tab.position) {
                            setTextColor(ContextCompat.getColor(context, R.color.darkjunglegreen_28))
                            TextUtils.applyTypeface(context, "fonts/gotham_bold.ttf", text.toString(), text.toString())
                        } else {
                            setTextColor(ContextCompat.getColor(context, R.color.oldlavender))
                            TextUtils.applyTypeface(context, "fonts/gotham_medium.ttf", text.toString(), text.toString())
                        }
                    }
                }
            }
        })

        vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                tlTabs.getTabAt(position)?.select()
            }
        })

        itemsPermitted.forEach {
            val tab = tlTabs.newTab().apply {
                val view = layoutInflater.inflate(R.layout.view_search_tab, tlTabs, false).apply {
                    findViewById<TextView>(R.id.tvTitle)?.text = getString(it.titleResId)
                }
                view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                tlTabs.layoutParams.height = view.measuredHeight
                customView = view
            }
            tlTabs.addTab(tab)
        }

        tlTabs.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        if (tlTabs.measuredWidth < getScreenWidth()) {
            tlTabs.setPadding(0, 0, 0, 0)
        }

        vpContent.adapter = ExplorePagerAdapter(
            supportFragmentManager,
            fragments.apply {
                forEach {
                    (it as SearchResultsFragment).listener = object : SearchResultsFragment.Listener {
                        override fun onSeeAllClicked(searchItem: SearchItem) {
                            vpContent.currentItem = searchItem.ordinal
                        }
                    }
                }
            }
        )

        loginRepository.userCompany?.company?.primaryColor?.apply {
            tlTabs.setSelectedTabIndicatorColor(ColorUtils.parse(this))
        }

        vpContent.offscreenPageLimit = itemsPermitted.size
    }

    private fun searchQuery(query: String) {
        fragments.forEach { if (it is Listener) it.onQueryChanged(query) }
    }
}
