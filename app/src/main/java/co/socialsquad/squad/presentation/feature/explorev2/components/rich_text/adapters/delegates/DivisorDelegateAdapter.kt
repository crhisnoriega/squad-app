package co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewTypeDelegateAdapter
import co.socialsquad.squad.presentation.util.inflate

class DivisorDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        DivisorViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) = Unit

    private inner class DivisorViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.item_rich_text_divisor)
    )
}
