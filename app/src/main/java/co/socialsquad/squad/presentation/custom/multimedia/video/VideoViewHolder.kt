package co.socialsquad.squad.presentation.custom.multimedia.video

import android.net.Uri
import android.util.Log
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Media
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.util.inflate

class VideoViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?) : MediaViewHolder(parent.inflate(R.layout.view_multi_media_pager_video), lifecycleOwner) {

    private val mVideoView: VideoView = view.findViewById(R.id.videoView)

    private var mUri: Uri? = null
    private var mVisible: Boolean = false

    override fun bind(media: Media, label: String?, isFirst: Boolean, showLabel: Boolean) {
        mUri = null
        mVisible = media.isViewHolderVisible

        mVideoView.release()

        mVideoView.apply {

            media.streamings?.let { streaming ->

                if (streaming.isNotEmpty()) {

                    streaming[0].apply {
                        thumbnailUrl?.apply {
                            loadThumbnail(this)

                            playlistUrl?.apply {
                                mUri = Uri.parse(this)
                                prepareToRun()
                            }
                        }
                    }
                }
            }

            showLabel?.let {
                if (it) setLabel(label)
            }

        }
    }

    private fun prepareToRun() {
        mUri?.let {
            mVideoView.apply {
                if (mVisible) {
                    prepare(it)
                    play(0L, muted = true)
                } else {
                    pause()
                }
            }
        }
    }

    override fun onResume() {
        Log.i("tag11", "onResume: " + this)
    }

    override fun onPause() {
        Log.i("tag11", "onPause: " + this)
        mVideoView.apply {
            pause()
        }
    }

    override fun onStop() {
        mVideoView.apply {
            pause()
        }
    }

    override fun onDestroy() {
        mVideoView.apply {
            release()
        }
    }
}
