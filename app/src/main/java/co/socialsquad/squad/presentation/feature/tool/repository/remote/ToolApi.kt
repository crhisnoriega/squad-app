package co.socialsquad.squad.presentation.feature.tool.repository.remote

import co.socialsquad.squad.domain.model.Opportunity
import co.socialsquad.squad.domain.model.OpportunityWidgetResponse
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.domain.model.SubmissionCreationResponse
import co.socialsquad.squad.domain.model.Timetable
import co.socialsquad.squad.domain.model.squad.SquadResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Url

interface ToolApi {
    @GET
    suspend fun fetchOpportunitiesSubmission(@Url path: String): SquadResponse<Opportunity<Submission>>

    @GET("/endpoints/timetable/submission/{submission_id}")
    suspend fun fetchDetailsTimetable(@Path("submission_id") submission_id: String): SquadResponse<Timetable>

    @GET
    suspend fun fetchOpportunitiesTimetable(@Url path: String):
            SquadResponse<Opportunity<Timetable>>

    @POST("/endpoints/opportunity/{opportunityId}/object/{objectId}/submission")
    suspend fun createNewOpportunity(
            @Path("objectId") objectId: Long,
            @Path("opportunityId") opportunityId: Long
    ): SubmissionCreationResponse

    @GET("/endpoints/opportunity/submission/{submissionId}/widget")
    suspend fun getNextWidget(
            @Path("submissionId") submissionId: Long
    ): OpportunityWidgetResponse
}
