package co.socialsquad.squad.presentation.feature.search

import co.socialsquad.squad.presentation.custom.ViewModel

class SearchNoResultsViewModel(val query: String) : ViewModel
