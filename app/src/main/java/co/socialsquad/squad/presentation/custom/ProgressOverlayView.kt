package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.view.isGone
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import kotlinx.android.synthetic.main.progress_overlay.view.*

class ProgressOverlayView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    init {
        inflate(context, R.layout.progress_overlay, this)
    }

    fun show(show: Boolean) {
        if (show) {
            show()
        } else {
            hide()
        }
    }

    private fun show() {
        flOverlay.isVisible = true
    }

    private fun hide() {
        flOverlay.isGone = true
    }
}
