package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.item_tool_lead_status.view.*

class ResourceMultipleViewHolder(itemView: View) : ViewHolder<ResourceSingleViewHolderModel>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_tool_lead_status

    }

    override fun bind(viewModel: ResourceSingleViewHolderModel) {
        itemView.lbl_lead_title.text = viewModel.complement.resource?.title
    }

    override fun recycle() {

    }
}