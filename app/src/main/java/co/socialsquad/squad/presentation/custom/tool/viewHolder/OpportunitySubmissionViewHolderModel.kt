package co.socialsquad.squad.presentation.custom.tool.viewHolder

import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.Submission
import co.socialsquad.squad.presentation.custom.ViewModel

data class OpportunitySubmissionViewHolderModel(val submission: Submission, val companyColor: CompanyColor?, val isLast: Boolean) : ViewModel
