package co.socialsquad.squad.presentation.feature.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Onboarding
import kotlinx.android.synthetic.main.fragment_onboarding.*

class OnboardingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(extra_title).takeIf { it.isNullOrEmpty() }?.apply {
            squad_logo.visibility = View.VISIBLE
        } ?: kotlin.run {
            object_type_lead.text = arguments?.getString(extra_title)
        }

        status.text = arguments?.getString(extra_description)
    }

    companion object {

        private const val extra_title = "title"
        private const val extra_description = "description"

        @JvmStatic
        fun newInstance(onboarding: Onboarding): OnboardingFragment {
            return OnboardingFragment().apply {
                arguments = Bundle().apply {
                    putString(extra_title, onboarding.title)
                    putString(extra_description, onboarding.description)
                }
            }
        }
    }
}
