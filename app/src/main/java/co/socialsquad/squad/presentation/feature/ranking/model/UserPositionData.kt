package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserPositionData(
        @SerializedName("id") val id: Int?,
        @SerializedName("full_name") val full_name: String?,
        @SerializedName("avatar") val avatar: String?

) : Parcelable