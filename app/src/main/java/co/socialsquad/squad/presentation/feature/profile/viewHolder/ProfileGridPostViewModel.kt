package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileGridPostViewModel(val url: String?) : ViewModel
