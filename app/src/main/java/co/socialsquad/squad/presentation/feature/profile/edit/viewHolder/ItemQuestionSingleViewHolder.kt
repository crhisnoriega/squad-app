package co.socialsquad.squad.presentation.feature.profile.edit.viewHolder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_profile_question_context_single.view.*


class ItemQuestionSingleViewHolder(
    val itemView: View,
    val onSelect: (item: ItemQuestionSingleViewModel) -> Unit
) :
    ViewHolder<ItemQuestionSingleViewModel>(itemView) {

    override fun bind(viewModel: ItemQuestionSingleViewModel) {
        itemView.topDivider22.isVisible = viewModel.isFirst!!
        itemView.bankName.text = viewModel.question

        itemView.rbChecked.setOnClickListener {
            onSelect.invoke(viewModel)
        }

        itemView.mainLayout.setOnClickListener {
            onSelect.invoke(viewModel)
        }


        itemView.rbChecked.isChecked = viewModel.isSelected
    }

    override fun recycle() {
    }
}