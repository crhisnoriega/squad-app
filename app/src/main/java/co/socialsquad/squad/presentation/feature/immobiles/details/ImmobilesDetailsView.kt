package co.socialsquad.squad.presentation.feature.immobiles.details

import co.socialsquad.squad.data.entity.ImmobileDetail
import co.socialsquad.squad.data.entity.opportunity.Opportunity
import co.socialsquad.squad.data.entity.opportunity.OpportunityAction
import co.socialsquad.squad.presentation.feature.LoadingView
import co.socialsquad.squad.presentation.feature.opportunity.opportunity_list.OpportunityListView

interface ImmobilesDetailsView :
    LoadingView,
    OpportunityListView {

    fun setupDetails(filteredDetails: MutableList<ImmobileDetail>)
    fun setupShare(subdomain: String?)
    fun onOpportunityActionClick(opportunity: Opportunity, opportunityAction: OpportunityAction)
    fun setupOpportunitiesSubmissions(opportunities: List<Opportunity>)
    fun showActions(show: Boolean)
}
