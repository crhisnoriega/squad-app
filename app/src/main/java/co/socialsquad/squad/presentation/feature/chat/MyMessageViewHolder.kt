package co.socialsquad.squad.presentation.feature.chat

import android.view.View
import co.socialsquad.squad.presentation.custom.PagedViewHolder
import kotlinx.android.synthetic.main.view_chat_message_right.view.*

class MyMessageViewHolder(
    itemView: View
) : PagedViewHolder<MyMessageViewModel>(itemView) {

    override fun bind(viewModel: MyMessageViewModel) {
        itemView.tvMessage.text = viewModel.message
    }

    override fun recycle() {}
}
