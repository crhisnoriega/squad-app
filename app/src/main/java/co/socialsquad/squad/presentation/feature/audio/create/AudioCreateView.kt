package co.socialsquad.squad.presentation.feature.audio.create

import co.socialsquad.squad.presentation.feature.audio.AudioViewModel

interface AudioCreateView {
    fun setSaveButtonEnabled(conditionsMet: Boolean)
    fun updateUploadStatus(percentage: Int)
    fun uploadSuccess()
    fun showError(stringId: Int)
    fun showPreparingFile()
    fun showProgress()
    fun hideProgress()
    fun setupPlayer(audioViewModel: AudioViewModel?)
    fun showOpeningError(message: Int)
    fun setupAudienceColor(squadColor: String?)
}
