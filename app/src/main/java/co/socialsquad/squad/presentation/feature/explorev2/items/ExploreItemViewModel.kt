package co.socialsquad.squad.presentation.feature.explorev2.items

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Explore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class ExploreItemViewModel(
    private val exploreRepository: ExploreRepository
) : ViewModel() {

    private var nextPage: String? = null

    @ExperimentalCoroutinesApi
    private val exploreItem = MutableStateFlow<Resource<Explore>>(Resource.loading(null))

    fun fetchExplore(link: String, geoLocation: Boolean) {
        viewModelScope.launch {
            exploreItem.value = Resource.loading(null)
            exploreRepository.getExplore(link)
                .catch { e ->
                    e.printStackTrace()
                    exploreItem.value = Resource.error(e.toString(), null)
                }
                .flowOn(Dispatchers.IO)
                .collect {
                    if (it.contents.isEmpty() && it.list.count == 0) {
                        exploreItem.value = Resource.empty()
                    } else {
                        if (geoLocation) {
                            it.list.items = it.list.items.filter { item ->
                                item.pinLocation?.latitude != 0.0 && item.pinLocation?.latitude != null
                            }
                        }

//                            it.contents?.forEach { item1 ->
//                                item1.items = item1.items.filter { item ->
//                                    item.pinLocation?.latitude != 0.0 && item.pinLocation?.latitude != null
//                                }
//                            }

                        nextPage =
                            if (it.list.next.isNullOrBlank()) null else "/endpoints${it.list.next}"
                        exploreItem.value = Resource.success(it)
                    }
                }
        }
    }


    var isLoading = false
    fun getNextPage() {
        nextPage?.let { page ->
            isLoading = true
            viewModelScope.launch {
                exploreRepository.getExplore(page)
                    .catch { e ->
                        e.printStackTrace()
                        exploreItem.value = Resource.error(e.toString(), null)
                    }
                    .flowOn(Dispatchers.IO)
                    .collect {
                        if (it.contents.isEmpty() && it.list.count == 0) {
                            exploreItem.value = Resource.empty()
                            isLoading = false
                        } else {
                            nextPage =
                                if (it.list.next.isNullOrBlank()) null else "/endpoints${it.list.next}"
                            exploreItem.value = Resource.success(it)
                            isLoading = false
                        }
                    }
            }
        }
    }

    @ExperimentalCoroutinesApi
    fun explore(): StateFlow<Resource<Explore>> {
        return exploreItem
    }

    fun setInitialPage(link: String) {
        this.nextPage = link
    }
}
