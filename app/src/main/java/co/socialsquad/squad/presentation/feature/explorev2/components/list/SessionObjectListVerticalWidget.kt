package co.socialsquad.squad.presentation.feature.explorev2.components.list

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.detail.ExploreItemDetailActivity
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ExploreList
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.objectitem.ExploreItemObjectActivity

class SessionObjectListVerticalWidget(context: Context, attrs: AttributeSet?) :
        LinearLayout(context, attrs) {

    private val view: View
    private var loading: Boolean = true
    private val rvItems: RecyclerView

    private var template: SectionTemplate = SectionTemplate.getDefault()

    init {
        setupStyledAttributes(attrs)
        view = changeTemplate(context)
        rvItems = view.findViewById(R.id.rvItems)
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.SessionObjectListVerticalWidget)) {
            loading = getBoolean(R.styleable.SessionObjectListVerticalWidget_loadingList, true)
        }
    }

    private fun changeTemplate(context: Context): View {
        return if (loading) {
            View.inflate(
                    context,
                    R.layout.view_session_list_vertical_loading_state,
                    this
            )
        } else {
            View.inflate(
                    context,
                    R.layout.view_session_object_list_vertical_widget,
                    this
            )
        }
    }

    fun bind(content: ExploreList?, template: SectionTemplate?, indicatorColor: String) {
        template?.let {
            this.template = it
        }
        if (loading) {
            rvItems.adapter = ObjectListItemsLoadingAdapter(this.template)
        } else {
            if (content != null) {
                rvItems.adapter = ObjectListItemsAdapter(
                        context, content.template, content.items, indicatorColor,
                        this@SessionObjectListVerticalWidget::onItemSelected
                )
            } else rvItems.adapter = null
        }
    }

    private fun onItemSelected(listItem: ListItem, template: SectionTemplate) {
        Log.i("log", "isObj: " + listItem.link)
        if (listItem.isObject()) {
            listItem.link?.let {
                context.startActivity(ExploreItemObjectActivity.newInstance(context, it))
            }
        } else {
            val contentItem = ContentItem(
                    id = listItem.id,
                    snippet = listItem.snippet.orEmpty(),
                    title = "",
                    media = listItem.media,
                    link = listItem.link,
                    shortTitle = listItem.shortTitle,
                    shortDescription = listItem.shortDescription,
                    customFields = listItem.customFields.orEmpty(),
                    pinLocation = null,
            )
            ExploreItemDetailActivity.start(
                    context = context,
                    item = contentItem,
                    template = template
            )
        }
    }
}
