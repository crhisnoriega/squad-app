package co.socialsquad.squad.presentation.feature.profile.qualification

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileQualificationModule {
    @Binds
    abstract fun view(profileQualificationActivity: ProfileQualificationActivity): ProfileQualificationView
}
