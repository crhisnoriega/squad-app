package co.socialsquad.squad.presentation.feature.newlead.di

import co.socialsquad.squad.presentation.feature.newlead.NewLeadSharedViewModel
import co.socialsquad.squad.presentation.feature.newlead.repository.NewLeadAPI
import co.socialsquad.squad.presentation.feature.newlead.repository.NewLeadRepository
import co.socialsquad.squad.presentation.feature.newlead.repository.NewLeadRepositoryImp
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object NewLeadModule {

    val repository = module {
        single {
            provideAPI(get())
        }

        single<NewLeadRepository> {
            NewLeadRepositoryImp(get())
        }
    }

    val instance = listOf(
        module {
            viewModel { NewLeadSharedViewModel(get()) }
        },
        repository
    )

    private fun provideAPI(retrofit: Retrofit): NewLeadAPI = retrofit.create(NewLeadAPI::class.java)
}
