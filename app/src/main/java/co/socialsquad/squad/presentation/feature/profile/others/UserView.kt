package co.socialsquad.squad.presentation.feature.profile.others

import android.content.DialogInterface
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModel

interface UserView {
    fun showProfile(profileViewModel: ProfileViewModel)
    fun showLoading()
    fun hideLoading()
    fun showPosts(posts: List<SocialViewModel>)
    fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener? = null)
    fun showLoadingMore()
    fun hideLoadingMore()
    fun showEditMedia(postViewModel: PostViewModel)
    fun showEditLive(postViewModel: PostLiveViewModel)
    fun hideDeleteDialog()
    fun removePost(viewModel: PostViewModel)
    fun updatePost(post: PostViewModel)
    fun onAudioToggled(audioEnabled: Boolean)
}
