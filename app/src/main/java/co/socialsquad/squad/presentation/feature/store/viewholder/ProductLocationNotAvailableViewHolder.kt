package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.ProductLocationNotAvailableViewModel

class ProductLocationNotAvailableViewHolder(itemView: View) : ViewHolder<ProductLocationNotAvailableViewModel>(itemView) {
    override fun bind(viewModel: ProductLocationNotAvailableViewModel) {}

    override fun recycle() {}
}
