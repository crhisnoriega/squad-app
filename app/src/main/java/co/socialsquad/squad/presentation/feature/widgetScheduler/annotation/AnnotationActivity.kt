package co.socialsquad.squad.presentation.feature.widgetScheduler.annotation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_annotation.*
import org.koin.core.module.Module

private const val EXTRA_NOTES = "extra_notes"

class AnnotationActivity : BaseActivity() {

    private val notes: String by extra(EXTRA_NOTES)
    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_annotation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        llLoading.isVisible = false
        setupBackButton()
        setupConfirmButton()
        setupContent(notes)
    }

    private fun setupConfirmButton(){
        btnConfirmar.setOnClickListener {
            edtField.text?.isNotBlank()?.let{
                setResult(RESULT_OK, Intent().apply {
                    putExtra(RESULT_NOTES, edtField.text.toString())
                })
                finish()
            }
        }
    }

    private fun checkValidField(value:String){
        btnConfirmar.isEnabled = value != null && value.trim().isNotEmpty()
    }

    private fun setupContent(notes:String){
        edtField.setEditOnEditorActionListener { view, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btnConfirmar.performClick()
            }
            false
        }
        edtField.addEditTextChangedListener(
            listener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(fieldValue: Editable?) {
                    checkValidField(fieldValue.toString())
                }
            }
        )
        edtField.setEditInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE)
        edtField.setInnerPadding(16, 5, 16, 5)
        edtField.hint = getString(R.string.anotacoes_hint)
        edtField.text = notes
    }

    private fun setupBackButton(){
        ibBack.setOnClickListener {
            finish()
        }
    }

    companion object {

        const val REQUEST_CODE = 1198
        const val RESULT_NOTES = "result_notes"

        fun startForResult(activity: Activity, notes: String) {
            activity.startActivityForResult(createIntent(activity, notes), REQUEST_CODE)
        }

        private fun createIntent(context: Context, notes: String): Intent {
            return Intent(context, AnnotationActivity::class.java).apply {
                putExtra(EXTRA_NOTES, notes)
            }
        }
    }
}