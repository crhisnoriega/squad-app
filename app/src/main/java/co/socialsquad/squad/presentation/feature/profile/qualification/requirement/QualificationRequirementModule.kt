package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import dagger.Binds
import dagger.Module

@Module
abstract class QualificationRequirementModule {
    @Binds
    abstract fun view(view: QualificationRequirementActivity): QualificationRequirementView
}
