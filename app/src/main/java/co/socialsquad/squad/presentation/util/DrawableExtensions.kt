package co.socialsquad.squad.presentation.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable

fun Context.getShapeWithColor(shapeRes: Int, colorHex: String): Drawable? {
    return getDrawable(shapeRes)?.mutate()?.apply {
        val color = Color.parseColor(colorHex)
        (this as GradientDrawable).setColor(color)
    }
}
