package co.socialsquad.squad.presentation.feature.recognition.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class BenefitData(
        @SerializedName("highlight") val highlight: String?,
        @SerializedName("media_cover") val media_cover: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("id") val id: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("details") val details: Boolean?,


        var first: Boolean = false
) : Parcelable