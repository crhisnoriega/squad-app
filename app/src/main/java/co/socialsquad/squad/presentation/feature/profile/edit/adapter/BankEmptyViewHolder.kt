package co.socialsquad.squad.presentation.feature.profile.edit.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate
import kotlinx.android.synthetic.main.item_bank_not_found_state.view.*

class BankEmptyViewHolder(val parent: ViewGroup) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_bank_not_found_state)) {

    fun bind(term: String) {
        itemView.tvDescription.text = "Não encontramos nenhum resultado para\n" +
                "\"$term\". Tente novamente!"
    }
}
