package co.socialsquad.squad.presentation.feature.profile.edit.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class BankInfo(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("code_number") var code_number: String? = null,
    @SerializedName("short_name") var short_name: String? = null,
    @SerializedName("complete_name") var complete_name: String? = null,

    var isChecked: Boolean = false
) : Parcelable {

}