package co.socialsquad.squad.presentation.feature.social

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import co.socialsquad.squad.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_social_delete.*

class SocialDeleteDialog(context: Context, private var listener: Listener) :
    BottomSheetDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_social_delete)

        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        social_delete_dialog_bsb_no.setOnClickListener {
            listener.onNoClicked()
            dismiss()
        }

        social_delete_dialog_bsb_yes.setOnClickListener {
            listener.onYesClicked()
            dismiss()
        }
    }

    interface Listener {
        fun onNoClicked()
        fun onYesClicked()
    }
}
