package co.socialsquad.squad.presentation.feature.recognition.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.recognition.model.PrizeData

class ItemPrizeRecognitionViewModel(
        val prizeData: PrizeData) : ViewModel {
}