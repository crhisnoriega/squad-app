package co.socialsquad.squad.presentation.feature.audio.record

import dagger.Binds
import dagger.Module

@Module
abstract class AudioRecordModule {
    @Binds
    abstract fun view(audioRecordActivity: AudioRecordActivity): AudioRecordView
}
