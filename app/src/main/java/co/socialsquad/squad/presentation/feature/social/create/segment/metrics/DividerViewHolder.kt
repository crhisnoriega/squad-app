package co.socialsquad.squad.presentation.feature.social.create.segment.metrics

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.social.DividerViewModel

class DividerViewHolder(itemView: View) : ViewHolder<DividerViewModel>(itemView) {
    override fun bind(viewModel: DividerViewModel) {}

    override fun recycle() {}
}
