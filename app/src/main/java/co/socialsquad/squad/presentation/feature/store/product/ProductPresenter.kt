package co.socialsquad.squad.presentation.feature.store.product

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_PRODUCT_DETAIL
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_DISTRIBUTOR_PHONE
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.EXTRA_PRODUCT
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.ProductDetailGroupsViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationDisabledViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationListViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationLoadingViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNetworkErrorViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNoPermissionViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationNotAvailableViewModel
import co.socialsquad.squad.presentation.feature.store.ProductLocationSeeAllViewModel
import co.socialsquad.squad.presentation.feature.store.ProductOverviewViewModel
import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreProductViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private const val REQUEST_CODE_PERMISSIONS_LOCATION = 77

class ProductPresenter @Inject constructor(
    private val productView: ProductView,
    private val storeInteractor: StoreInteractor,
    loginRepository: LoginRepository,
    private val fusedLocationProviderClient: FusedLocationProviderClient,
    private val tagWorker: TagWorker
) {
    companion object {
        const val EXTRA_PK = "EXTRA_PK"
    }

    private val compositeDisposable = CompositeDisposable()
    private val items = arrayListOf<ViewModel>()
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    private val color = storeInteractor.getColor()
    private val subdomain = loginRepository.subdomain
    private val isAvailabilityEnabled = loginRepository.userCompany?.company?.featureProdAvail ?: false
    private var product: StoreProductViewModel? = null
    private var locationViewModel: ViewModel = ProductLocationLoadingViewModel

    fun onViewCreated(intent: Intent) {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_PRODUCT_DETAIL)
        productView.setupList(color)

        if (intent.hasExtra(EXTRA_PRODUCT)) {
            product = intent.getSerializableExtra(EXTRA_PRODUCT) as? StoreProductViewModel
            product?.let { setupProduct(it) }
        } else if (intent.hasExtra(EXTRA_PK)) {
            val pk = intent.extras?.getInt(EXTRA_PK)
            pk?.let { getProduct(it) }
        }
    }

    private fun getProduct(pk: Int) {
        compositeDisposable.add(
            storeInteractor.getProduct(pk)
                .doOnSubscribe { productView.showLoading() }
                .doOnTerminate { productView.hideLoading() }
                .subscribe(
                    {
                        product = it
                        setupProduct(it)
                    },
                    {
                        it.printStackTrace()
                        productView.showMessage(R.string.product_error_get_product)
                    }
                )
        )
    }

    private fun setupProduct(product: StoreProductViewModel) {
        productView.setupTitle(product.shortName)

        setupOverview(product)
        setupDetails(product)
        if (isAvailabilityEnabled) setupAvailability()

        productView.setItems(items)

        storeInteractor.sendProductView(product.pk)
            .onErrorComplete()
            .subscribe()
    }

    private fun setupOverview(product: StoreProductViewModel) {
        items.add(ProductOverviewViewModel(product, subdomain))
    }

    private fun setupDetails(product: StoreProductViewModel) {
        val position = items.size
        compositeDisposable.add(
            storeInteractor.getProductDetailGroups(product.pk)
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            val header = HeaderViewModel(titleResId = R.string.product_header_details_title)
                            items.addAll(position, arrayListOf(header, ProductDetailGroupsViewModel(it)))
                            productView.setItems(items)
                        }
                    },
                    Throwable::printStackTrace
                )
        )
    }

    private fun setupAvailability() {
        items.add(HeaderViewModel(titleResId = R.string.product_header_locations_title, subtitleResId = R.string.product_header_locations_subtitle))
        items.add(locationViewModel)

        productView.verifyLocationServiceAvailability()
    }

    fun onRequestPermissionsClicked() {
        updateLocationListView(ProductLocationLoadingViewModel)
        productView.requestPermissions(REQUEST_CODE_PERMISSIONS_LOCATION, permissions)
    }

    fun onLocationDisabledClicked() {
        updateLocationListView(ProductLocationLoadingViewModel)
        productView.verifyLocationServiceAvailability()
    }

    fun onNetworkErrorClicked() {
        onRequestPermissionsClicked()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { onLastLocationReceived(it) }
    }

    fun onLocationServiceAvailability(enabled: Boolean) {
        if (enabled) {
            if (productView.checkPermissions(permissions)) getLastLocation()
            else updateLocationListView(ProductLocationNoPermissionViewModel)
        } else updateLocationListView(ProductLocationDisabledViewModel)
    }

    private fun onLastLocationReceived(location: Location?) {
        location?.let { getLocations(it.latitude, it.longitude) }
            ?: getFineLocation()
    }

    private fun getFineLocation() {
        productView.createLocationRequest()
    }

    fun onRequestPermissionsResult(grantResults: IntArray) {
        if (grantResults.isEmpty() || grantResults.any { it == PackageManager.PERMISSION_DENIED }) {
            updateLocationListView(ProductLocationNoPermissionViewModel)
        } else {
            getLastLocation()
        }
    }

    private fun getLocations(latitude: Double, longitude: Double) {
        product?.let {
            compositeDisposable.add(
                storeInteractor.getProductLocations(it.code, latitude, longitude)
                    .doOnSubscribe { updateLocationListView(ProductLocationLoadingViewModel) }
                    .subscribe(
                        {
                            if (it.isNotEmpty()) {
                                with(items) {
                                    val position = indexOf(ProductLocationLoadingViewModel)
                                    removeAt(position)
                                    add(position, ProductLocationListViewModel(it.subList(0, minOf(3, it.size))))
                                    add(position + 1, ProductLocationSeeAllViewModel(ProductLocationListViewModel(it)))
                                    productView.setItems(this)
                                }
                            } else {
                                updateLocationListView(ProductLocationNotAvailableViewModel)
                            }
                        },
                        {
                            it.printStackTrace()
                            updateLocationListView(ProductLocationNetworkErrorViewModel)
                        }
                    )
            )
        }
    }

    private fun updateLocationListView(viewModel: ViewModel) {
        val position = items.indexOf(locationViewModel)
        locationViewModel = viewModel
        items[position] = locationViewModel
        productView.setItems(items)
    }

    fun onOpenPhoneClicked() {
        tagWorker.tagEvent(TAG_TAP_DISTRIBUTOR_PHONE)
    }

    fun onDestroy() = compositeDisposable.dispose()

    @SuppressLint("MissingPermission")
    fun onLocationRequestCreated(locationRequest: LocationRequest, pendingIntent: PendingIntent?) {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, pendingIntent).addOnSuccessListener {
            getLastLocation()
        }.addOnFailureListener {
            updateLocationListView(ProductLocationNoPermissionViewModel)
        }
    }
}
