package co.socialsquad.squad.presentation.custom

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Typeface
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.showKeyboard
import kotlinx.android.synthetic.main.view_input_text.view.*

class InputText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs), InputView {

    private var onInputListener: InputView.OnInputListener? = null

    var text: String?
        get() = etInput.text.toString()
        set(value) {

            etInput.setText(value)

            if (etInput.visibility == View.GONE) {
                labelAnimator.start()
                etInput.visibility = View.VISIBLE
            }
        }

    val editText: EditText?
        get() = etInput

    val isBlank: Boolean
        get() = etInput.text.toString().isBlank()

    val length: Int
        get() = etInput.length()

    private val labelAnimator = ObjectAnimator
        .ofFloat(12f, 11f)
        .apply {
            duration = 200L
            addUpdateListener { tvLabel.textSize = it.animatedValue as Float }
        }

    var hint = ""
        set(value) {
            tvLabel.text = value
        }

    init {
        View.inflate(context, R.layout.view_input_text, this)
        setupStyledAttributes(attrs)
        setupViews()
    }

    private fun setupStyledAttributes(attrs: AttributeSet?) {
        with(context.obtainStyledAttributes(attrs, R.styleable.InputText)) {
            tvLabel.text = getString(R.styleable.InputText_hint)
            etInput.inputType = getInt(R.styleable.InputText_android_inputType, etInput.inputType)
            etInput.imeOptions =
                getInt(R.styleable.InputText_android_imeOptions, etInput.imeOptions)

            getInt(R.styleable.InputText_android_maxLength, Int.MAX_VALUE).let {
                etInput.filters = arrayOf(InputFilter.LengthFilter(it))
            }

            getDimensionPixelSize(R.styleable.InputText_android_textSize, 0)?.let {
                if (it > 0) {
                    val sp: Float = it / resources.displayMetrics.scaledDensity
                    etInput.textSize = sp
                }
            }

            getString(R.styleable.InputText_android_textStyle)?.let {
                when (it) {
                    "normal" ->
                        etInput.typeface = Typeface.DEFAULT
                    "bold" ->
                        etInput.typeface = Typeface.DEFAULT_BOLD
                }
            }

            if (getBoolean(R.styleable.InputText_android_singleLine, false)) {
                etInput.inputType = etInput.inputType xor InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }

            etInput.typeface = Typeface.createFromAsset(
                context.assets,
                "fonts/roboto_medium.ttf"
            )
            recycle()
        }
    }

    fun setInnerPadding(left: Int = 0, top: Int = 0, right: Int, bottom: Int) {
        clInputText.setPadding(dpTopPx(left), dpTopPx(top), dpTopPx(right), dpTopPx(bottom))
    }

    private fun dpTopPx(dp: Int): Int {
        return (dp * context.resources.displayMetrics.density).toInt()
    }

    fun setEditErrorMessage(error: String?) {
        error?.let {
            txtErrorMessage.text = it
            txtErrorMessage.isVisible = true
        } ?: run {
            txtErrorMessage.text = ""
            txtErrorMessage.isVisible = false
        }
    }


    fun setRequiredField(required: Boolean) {
        txtRequired.isVisible = required
    }

    fun setEditOnEditorActionListener(listener: TextView.OnEditorActionListener) {
        etInput.setOnEditorActionListener(listener)
    }

    fun addEditTextChangedListener(listener: TextWatcher) {
        etInput.addTextChangedListener(listener)
    }

    fun setEditInputType(inputType: Int) {
        etInput.inputType = inputType
    }

    fun setEditInputMask(mask: String) {
        MaskUtils.applyMask(etInput, mask)
    }

    private fun setupViews() {
        rootView.setOnClickListener { onFocusGained() }

        (etInput.parent as View).setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) onFocusGained()
        }

        etInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && isBlank) {
                etInput.setText("")
                etInput.visibility = View.GONE
                with(labelAnimator) {
                    end()
                    reverse()
                }
            }

            isFF = hasFocus
        }

        etInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                onInputListener?.onInput()
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    var isFF = false

    fun onFocusGained() {
        isFF = true
        if (etInput.visibility == View.GONE) {
            labelAnimator.start()
            etInput.visibility = View.VISIBLE
        }
        with(etInput) {
            requestFocus()
            showKeyboard()
        }
    }

    fun forceAnimation() {
        labelAnimator.start()
        etInput.visibility = View.VISIBLE
    }

    override fun setOnInputListener(onInputListener: InputView.OnInputListener) {
        this.onInputListener = onInputListener
    }

    override fun isValid(valid: Boolean) {}

    fun doClickable(onClick: () -> Unit) {
        etInput.isFocusable = false
        etInput.isClickable = true
        llInput.isClickable = true

        tvLabel.setOnClickListener {
            onClick.invoke()
        }
        etInput.setOnClickListener {
            onClick.invoke()
        }
        clInputText.setOnClickListener {
            onClick.invoke()
        }
        llInput.setOnClickListener {
            onClick.invoke()
        }
    }

}
