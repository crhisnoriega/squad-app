package co.socialsquad.squad.presentation.feature.points.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CriteriaList(
        @SerializedName("title") val title: String?,
        @SerializedName("data") val data: List<CriteriaData>?,
        var first: Boolean = false
) : Parcelable