package co.socialsquad.squad.presentation.feature.ranking.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_ranking_criteria.view.*

class ItemCriteriaRankingViewHolder(itemView: View, var onClickAction: ((item: ItemCriteriaRankingViewModel) -> Unit)) : ViewHolder<ItemCriteriaRankingViewModel>(itemView) {
    override fun bind(viewModel: ItemCriteriaRankingViewModel) {

        viewModel.showDetails?.let {
            if (it) {
                itemView.mainLayout.setOnClickListener {
                    onClickAction.invoke(viewModel)
                }
            }
            itemView.detailsArrow.isVisible = it
        }
        itemView.txtTitle.text = viewModel.title
        itemView.txtSubtitle.text = viewModel.description

        if (viewModel.isInRank) {
            itemView.txtSubtitle333.text = viewModel.points
        } else {
            itemView.txtSubtitle333.visibility = View.GONE
        }
        itemView.dividerItemTop.isVisible = viewModel.isFirst.not()

        Glide.with(itemView.context).load(viewModel.icon).circleCrop().into(itemView.imgIcon)
    }

    override fun recycle() {

    }

}