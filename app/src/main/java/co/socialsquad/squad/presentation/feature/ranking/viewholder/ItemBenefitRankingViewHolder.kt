package co.socialsquad.squad.presentation.feature.ranking.viewholder

import android.net.Uri
import android.preference.PreferenceManager
import android.view.View
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_ranking_benefit.view.*
import kotlinx.android.synthetic.main.item_ranking_benefit.view.cover
import kotlinx.android.synthetic.main.item_ranking_benefit.view.dividerItemTop
import kotlinx.android.synthetic.main.item_ranking_benefit.view.mainLayout
import kotlinx.android.synthetic.main.item_ranking_benefit.view.txtSubTitle
import kotlinx.android.synthetic.main.item_ranking_benefit.view.txtSubtitle2
import kotlinx.android.synthetic.main.item_ranking_benefit.view.txtTitle
import kotlinx.android.synthetic.main.item_recognition_benefit.view.*


class ItemBenefitRankingViewHolder(itemView: View, var onClickAction: (benefit: ItemBenefitRankingViewModel) -> Unit) : ViewHolder<ItemBenefitRankingViewModel>(itemView) {
    override fun bind(viewModel: ItemBenefitRankingViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }

        itemView.txtTitle.text = viewModel.positions
        itemView.txtTitle.isVisible = viewModel.positions.isNullOrBlank().not()

        itemView.txtTitle.setTextColor(ColorUtils.parse(getCompanyColor()!!))
        itemView.txtSubTitle.text = viewModel.title
        itemView.txtSubtitle2.text = viewModel.description
        itemView.dividerItemTop.isVisible = viewModel.isFirst.not()
        // Glide.with(itemView.context).load(viewModel.media_cover).into(itemView.cover)


        GlideToVectorYou.init().with(itemView.context).load(Uri.parse(viewModel.media_cover), itemView.cover)
    }

    private fun getCompanyColor(): String? {
        var userComnpanyJson = PreferenceManager.getDefaultSharedPreferences(itemView.context).getString("USER_COMPANY", null)
        val userCompany: UserCompany = Gson().fromJson(userComnpanyJson, UserCompany::class.java)

        return userCompany.company?.primaryColor
    }

    override fun recycle() {

    }

}