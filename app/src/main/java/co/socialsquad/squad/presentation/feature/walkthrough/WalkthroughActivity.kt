package co.socialsquad.squad.presentation.feature.walkthrough

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import co.socialsquad.squad.BuildConfig
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ExplorePagerAdapter
import co.socialsquad.squad.presentation.custom.PageIndicator
import co.socialsquad.squad.presentation.feature.login.options.LoginOptionsActivity
import co.socialsquad.squad.presentation.feature.signup.credentials.SignupCredentialsActivity
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import kotlinx.android.synthetic.main.activity_walkthrough.*

class WalkthroughActivity : BaseDaggerActivity() {

    companion object {
        val LAYOUT_RES_IDS = intArrayOf(
            R.layout.fragment_walkthrough_page_1,
            R.layout.fragment_walkthrough_page_2, R.layout.fragment_walkthrough_page_3,
            R.layout.fragment_walkthrough_page_4, R.layout.fragment_walkthrough_page_5
        )
    }

    private lateinit var vpPager: ViewPager
    private lateinit var piIndicator: PageIndicator
    private lateinit var bSignup: Button
    private lateinit var bLogin: Button
    private lateinit var vVersion: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFlagLayoutFullscreen()
        setContentView(R.layout.activity_walkthrough)

        vpPager = walkthrough_vp_pager
        piIndicator = walkthrough_pi_indicators
        bSignup = walkthrough_b_signup
        bLogin = walkthrough_b_login
        vVersion = walkthrough_v_version

        setupPager()
        setupOnClickListeners()
    }

    private fun setupPager() {
        val fragments = LAYOUT_RES_IDS.map { WalkthroughFragment.newInstance(it) }.toTypedArray<Fragment>()
        val fragmentPagerAdapter = ExplorePagerAdapter(supportFragmentManager, fragments)

        vpPager.adapter = fragmentPagerAdapter
        vpPager.offscreenPageLimit = LAYOUT_RES_IDS.size

        if (LAYOUT_RES_IDS.size > 1) setupIndicators()
    }

    private fun setupOnClickListeners() {
        bSignup.setOnClickListener {
            startActivity(Intent(this, SignupCredentialsActivity::class.java))
        }

        bLogin.setOnClickListener {
            startActivity(Intent(this, LoginOptionsActivity::class.java))
        }

        vVersion.setOnLongClickListener {
            var version = getString(R.string.version, BuildConfig.VERSION_NAME)
            if (BuildConfig.DEBUG || BuildConfig.FLAVOR != "production") {
                version = String.format("%s (%s)", version, BuildConfig.FLAVOR)
            }
            Toast.makeText(this, version, Toast.LENGTH_LONG)
                .show()
            return@setOnLongClickListener true
        }
    }

    private fun setupIndicators() {
        with(piIndicator) {
            viewPager = vpPager
            types = LAYOUT_RES_IDS.map { PageIndicator.Type.Image() }
            init()
        }
    }
}
