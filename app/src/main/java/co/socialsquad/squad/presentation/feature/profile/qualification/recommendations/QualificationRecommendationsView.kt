package co.socialsquad.squad.presentation.feature.profile.qualification.recommendations

import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.video.list.VideoListItemViewModel

interface QualificationRecommendationsView {
    fun startLoading()
    fun stopLoading()
    fun showMessage(textResId: Int, onDismissListener: () -> Unit?)
    fun addItems(items: List<ViewModel>, shouldClearList: Boolean)
    fun updateItem(viewModel: VideoListItemViewModel)
    fun stopRefreshing()
    fun openVideo(url: String)
    fun setupToolbar(titleId: Int, progress: Int, total: Int)
}
