package co.socialsquad.squad.presentation.feature.social.create.segment.metrics.values

import android.content.DialogInterface
import android.content.Intent
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Values
import co.socialsquad.squad.data.repository.SegmentRepository
import co.socialsquad.squad.presentation.feature.social.MetricViewModel
import co.socialsquad.squad.presentation.feature.social.ValueViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import io.reactivex.disposables.CompositeDisposable
import java.io.Serializable
import javax.inject.Inject

data class Value(
    val name: String,
    val icon: String?,
    val value: String,
    val members: Int
) : Serializable

class SegmentValuesPresenter
@Inject constructor(
    private val segmentValuesView: SegmentValuesView,
    private val segmentRepository: SegmentRepository
) {
    companion object {
        const val REQUEST_CODE_VALUES = 40
        const val RESULT_CODE_VALUES = 41
    }

    private val compositeDisposable = CompositeDisposable()

    private lateinit var metric: MetricViewModel

    private var page = 1
    private var complete = false
    private var selectedValues = mutableListOf<ValueViewModel>()

    fun onCreate(intent: Intent) {
        metric = intent.getSerializableExtra(EXTRA_METRICS) as MetricViewModel

        with(segmentValuesView) {
            setupToolbar(metric.name)
            setupList(metric)
            setupSwipeRefresh()
        }

        showValues(metric.values)
    }

    private fun showValues(previousSelected: List<ValueViewModel>) {
        compositeDisposable.add(
            segmentRepository.getValues(metric.field, page)
                .doOnSubscribe { segmentValuesView.showLoading() }
                .subscribe(
                    { values ->
                        with(segmentValuesView) {
                            hideLoading()
                            enableRefreshing()
                        }
                        onValuesReceived(values, false, previousSelected)
                        page++
                        complete = values.next == null
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        segmentValuesView.showErrorMessage(
                            R.string.segment_values_error_get_first_page,
                            DialogInterface.OnDismissListener {
                                segmentValuesView.hideLoading()
                            }
                        )
                    },
                    {
                        if (selectedValues.size > 0) {
                            segmentValuesView.setAddEnabled(true)
                        }
                    }
                )
        )
    }

    fun onRefresh() {
        complete = false

        compositeDisposable.clear()
        compositeDisposable.add(
            segmentRepository.getValues(metric.field, 1)
                .doOnTerminate { segmentValuesView.stopRefreshing() }
                .subscribe(
                    { values ->
                        onValuesReceived(values, false, selectedValues)
                        page = 2
                        complete = values.next == null
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        segmentValuesView.showErrorMessage(R.string.segment_values_error_refresh)
                    }
                )
        )
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (complete) return

        compositeDisposable.clear()
        compositeDisposable.add(
            segmentRepository.getValues(metric.field, page)
                .doOnSubscribe { segmentValuesView.showLoading() }
                .subscribe(
                    { values ->
                        segmentValuesView.hideLoading()
                        onValuesReceived(values, true, selectedValues)
                        page++
                        complete = values.next == null
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        segmentValuesView.showErrorMessage(
                            R.string.segment_values_error_get_next_page,
                            DialogInterface.OnDismissListener {
                                segmentValuesView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    private fun onValuesReceived(values: Values, add: Boolean = false, previousSelected: List<ValueViewModel>) {
        values.results.orEmpty().apply {
            if (isNotEmpty()) {
                val valuesViewModel = map { ValueViewModel(it.value, it.members, it.icon, it.displayValue, previousSelected.any { previous -> it.value == previous.value }) }
                if (add) segmentValuesView.addItems(valuesViewModel)
                else segmentValuesView.setItems(valuesViewModel)
                valuesViewModel.filter { it.selected }.forEach {
                    if (!selectedValues.contains(it)) {
                        selectedValues.add(it)
                    }
                }
            }
        }
    }

    fun onValueSelected(value: ValueViewModel) {
        if (selectedValues.contains(value)) {
            return
        }
        if (!metric.multipick) {
            selectedValues.forEach {
                it.selected = false
                segmentValuesView.updateSelected(it)
                selectedValues.remove(it)
            }
        }
        value.selected = true
        if (!selectedValues.contains(value)) selectedValues.add(value)
    }

    fun onValueDeselected(value: ValueViewModel) {
        selectedValues.remove(value)
        value.selected = false
        segmentValuesView.updateSelected(value)
    }

    fun onAddClicked() {
        val intent = Intent()
        metric.values = selectedValues
        intent.putExtra(EXTRA_METRICS, metric)
        segmentValuesView.finishWithResult(RESULT_CODE_VALUES, intent)
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
