package co.socialsquad.squad.presentation.feature.explorev2.components.collectionhorizontalmap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.sealedClasses.SectionTemplate
import co.socialsquad.squad.presentation.feature.explorev2.items.ExploreItemMapVM
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide

class CollectionHorizontalMapItemsAdapter(
        val context: Context,
        val template: SectionTemplate,
        val items: List<ExploreItemMapVM>,
        val indicatorColor: String,
        val onCollectionSelected: (ContentItem) -> Unit,
        val onCollectionSelectedList: (ListItem) -> Unit
) : RecyclerView.Adapter<CollectionHorizontalMapItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (template) {
            SectionTemplate.Square -> ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.view_session_collection_horizontal_square_item_map, parent,
                                    false)
            )
            SectionTemplate.Circle -> ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.view_session_collection_horizontal_circle_item_map, parent,
                                    false)
            )
            SectionTemplate.RectangleHorizontal -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_horizontal_rectangular_horizontal_item_map,
                            parent,
                            false
                    )
            )
            SectionTemplate.RectangleVertical -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_horizontal_rectangular_vertical_item_map,
                            parent,
                            false
                    )
            )
            else -> ViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.view_session_collection_horizontal_rectangular_vertical_item_map,
                            parent,
                            false
                    )
            )
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        item.contentItem?.let { contentItem ->
            contentItem.media?.let { Glide.with(context).load(it.url).centerCrop().crossFade().into(holder.image) }
            holder.stretch.text = contentItem.snippet
            holder.stretch.setTextColor(ColorUtils.parse(indicatorColor))
            holder.short.text = contentItem.shortTitle
            holder.subtitle.text = contentItem.shortDescription
            holder.customFields.text = contentItem.customFields

            if (contentItem.link != null) {
                holder.container.setOnClickListener { onCollectionSelected(contentItem) }
            }
        }

        item.listItem?.let { listItem ->
            listItem.media?.let { Glide.with(context).load(it.url).centerCrop().crossFade().into(holder.image) }
            holder.stretch.text = listItem.snippet
            holder.stretch.setTextColor(ColorUtils.parse(indicatorColor))
            holder.short.text = listItem.shortTitle
            holder.subtitle.text = listItem.shortDescription
            holder.customFields.text = listItem.customFields

            if (listItem.link != null) {
                holder.container.setOnClickListener { onCollectionSelectedList(listItem) }
            }
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: ViewGroup = itemView.findViewById(R.id.container)
        val image: ImageView = itemView.findViewById(R.id.image)
        val stretch: TextView = itemView.findViewById(R.id.stretch)
        val short: TextView = itemView.findViewById(R.id.tvShort)
        val subtitle: TextView = itemView.findViewById(R.id.description)
        val customFields: TextView = itemView.findViewById(R.id.customFields)
    }
}
