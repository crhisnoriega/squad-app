package co.socialsquad.squad.presentation.feature.lead

import co.socialsquad.squad.base.BaseViewModel
import co.socialsquad.squad.domain.model.lead.Lead
import co.socialsquad.squad.presentation.feature.base.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@ExperimentalCoroutinesApi
class LeadViewModel : BaseViewModel() {

    @ExperimentalCoroutinesApi
    val lead: StateFlow<Resource<Lead>>
        get() = _lead

    @ExperimentalCoroutinesApi
    private val _lead = MutableStateFlow(Resource.empty<Lead>())

    fun loadLead(lead: Lead) {
        this._lead.value = Resource.success(lead)
    }
}
