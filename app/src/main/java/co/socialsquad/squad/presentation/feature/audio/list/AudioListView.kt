package co.socialsquad.squad.presentation.feature.audio.list

import co.socialsquad.squad.presentation.feature.audio.AudioViewModel

interface AudioListView {
    fun showMessage(textResId: Int, onDismissListener: () -> Unit = {})
    fun setItems(items: List<AudioViewModel>)
    fun addItems(items: List<AudioViewModel>)
    fun removeItem(audioViewModel: AudioViewModel)
    fun startLoading()
    fun stopLoading()
    fun startRefreshing()
    fun stopRefreshing()
    fun showEmptyState()
    fun hideEmptyState()
}
