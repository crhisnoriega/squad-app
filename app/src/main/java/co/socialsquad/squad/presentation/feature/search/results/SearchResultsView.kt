package co.socialsquad.squad.presentation.feature.search.results

import co.socialsquad.squad.presentation.custom.ViewModel

interface SearchResultsView {
    fun startLoading(search: String = "")
    fun stopLoading()
    fun enableRefreshing()
    fun disableRefreshing()
    fun addItems(items: List<ViewModel>)
    fun setItems(items: List<ViewModel>)
    fun clearList()
    fun showEmptyState()
    fun showNoResults(query: String)
    fun createLocationRequest()
    fun setupList(secondayColor: String)
}
