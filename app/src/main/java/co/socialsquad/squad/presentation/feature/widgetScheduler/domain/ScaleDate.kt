package co.socialsquad.squad.presentation.feature.widgetScheduler.domain

import android.os.Parcelable
import co.socialsquad.squad.domain.model.Header
import co.socialsquad.squad.domain.model.Position
import co.socialsquad.squad.domain.model.ScaleTime
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScaleDate(
    @SerializedName("header") val header: Header,
    @SerializedName("positions") val position: Position,
    @SerializedName("dates") val dates: MutableList<ScaleTime> = mutableListOf(),
    @SerializedName("supplementary_text") val supplementary: String,
    @SerializedName("button") val button: String,
    @SerializedName("next_monday") val next_monday: String,
    @SerializedName("previous_monday") val previous_monday: String,
) : Parcelable
