package co.socialsquad.squad.presentation.feature.kickoff.inviteteam

import co.socialsquad.squad.data.entity.InviteMembersRequest
import co.socialsquad.squad.data.repository.InviteMemberRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class InviteYourTeamPresenter @Inject constructor(
    private val repository: InviteMemberRepository,
    private val view: InviteYourTeamView
) {

    private val compositeDisposable = CompositeDisposable()

    fun sendInvites(emails: List<InviteMembersRequest>) {
        compositeDisposable.add(
            repository.inviteMembers(emails)
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    {
                        view.moveToNextScreen()
                    },
                    {
                        it.printStackTrace()
                        view.showErrorMessage()
                    }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
