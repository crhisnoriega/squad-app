package co.socialsquad.squad.presentation.feature.points.listing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.custom.*
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.points.PointsActivity
import co.socialsquad.squad.presentation.feature.points.model.AreaData
import co.socialsquad.squad.presentation.feature.points.viewHolder.*
import co.socialsquad.squad.presentation.feature.points.viewModel.PersonalPointsViewModel
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.ranking.viewholder.*
import co.socialsquad.squad.presentation.util.extra
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.activity_points_elements_listing.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module


enum class POINTS_LIST_TYPE(val title: String) {
    SYSTEMS("Sistemas"), CRITERIA("Critérios"), AREA("Areas"), RANKINK_INFO("Rankings"), PRIZES(
        "Prêmios"
    )
}

class PointsElementsListingActivity : BaseActivity() {

    companion object {

        var doRefresh: Boolean = false

        const val ELEMENT_TYPE = "element_type"
        const val AREA_INFO = "AREA_INFO"

        const val ELEMENT_TITLE = "element_title"
        const val ITENS_SHIMMER = "ITEMS_SHIMMER"
        const val SYSTEM_DATA = "SYSTEM_DATA"

        fun start(
            context: Context,
            elementType: POINTS_LIST_TYPE,
            itemsShimmer: Int = 0,
            titleAPI: String,
            areaData: AreaData? = null
        ) {
            context.startActivity(Intent(context, PointsElementsListingActivity::class.java).apply {
                putExtra(ELEMENT_TYPE, elementType)
                putExtra(ELEMENT_TITLE, titleAPI)
                putExtra(ITENS_SHIMMER, itemsShimmer)
                putExtra(AREA_INFO, areaData)
            })
        }
    }

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_points_elements_listing

    private val elementType: POINTS_LIST_TYPE by extra(ELEMENT_TYPE)
    private val title: String by extra(ELEMENT_TITLE)
    private val itemsShimmer: Int by extra(ITENS_SHIMMER)
    private val areaData: AreaData by extra(AREA_INFO)

    private val viewModel by viewModel<PersonalPointsViewModel>()

    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        configureToolbar()
        configureObservers()

        itemsShimmer?.let {
            if (it > 0) configureAsLoading(it)
        }

        when (elementType) {
            POINTS_LIST_TYPE.SYSTEMS -> viewModel.fetchSystemPoints()
            POINTS_LIST_TYPE.CRITERIA -> configureCriteriaList()
        }
    }


    override fun onResume() {
        Log.i("list", "onResume: $doRefresh")
        super.onResume()
        if (doRefresh) {
            doRefresh = false

            factoryAdapter.items.clear()

            ProfileFragment.doRefresh = true
            when (elementType) {
                POINTS_LIST_TYPE.SYSTEMS -> viewModel.fetchSystemPoints()
            }
        }
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
        tvTitle.text = title
    }

    private val handler = Handler()
    private var endPage = false

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.systemsPoints
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        configureElementList()
                    }
                    Status.ERROR -> {
                    }
                }
            }
        }
    }

    private fun stopShimmer(shimmer: ShimmerFrameLayout?) {
        shimmer?.let {
            it.setShimmer(
                Shimmer.AlphaHighlightBuilder()
                    .setBaseAlpha(1f)
                    .setIntensity(0f)
                    .build()
            )
            it.stopShimmer()
            it.clearAnimation()
        }
    }

    private fun configureAsLoading(itens: Int) {

        rvElements.apply {
            layoutManager = LinearLayoutManager(
                this@PointsElementsListingActivity,
                LinearLayoutManager.VERTICAL,
                false
            )

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel): Int {
                    return when (viewModel) {
                        is ItemShowAllRankingViewModel -> R.layout.ranking_separator
                        is ItemShowAllRankingTopBottomViewModel -> R.layout.item_ranking_info_shimmer
                        else -> throw IllegalArgumentException()
                    }
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
                    }
                    R.layout.item_ranking_info_shimmer -> ItemSeparatorRankingTopBottomViewHolder(
                        view
                    ) {
                    }
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter

            factoryAdapter.setItems((0 until itens).map {
                ItemShowAllRankingTopBottomViewModel("", "") as ViewModel
            }.toMutableList().apply {
                add(0, ItemShowAllRankingViewModel("", ""))
            })
        }
    }


    private fun addToList() {

    }

    val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
        override fun getType(viewModel: ViewModel): Int {
            return when (viewModel) {
                is ItemHeaderListingViewModel -> R.layout.item_listing_header

                is ItemSystemPointsViewModel -> R.layout.item_points_system

                is ItemAreaPointsViewModel -> R.layout.item_points_area

                is ItemCriteriaPointsViewModel -> R.layout.item_points_criteria

                is ItemShowAllRankingViewModel -> R.layout.ranking_separator

                else -> throw IllegalArgumentException()
            }
        }

        override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
            R.layout.item_listing_header -> ItemHeaderPointsViewHolder(view)

            R.layout.item_points_criteria -> ItemCriteriaPointsViewHolder(view) {

            }

            R.layout.item_points_area -> ItemAreaPointsViewHolder(view) {

            }

            R.layout.item_points_system -> ItemSystemPointsViewHolder(view) {
                PointsActivity.start(this@PointsElementsListingActivity, it.systemData)

            }

            R.layout.ranking_separator -> ItemSeparatorRankingViewHolder(view) {
            }
            else -> throw IllegalArgumentException()
        }
    })

    private val _layoutManager =
        LinearLayoutManager(this@PointsElementsListingActivity, LinearLayoutManager.VERTICAL, false)

    private fun configureElementList() {
        stopShimmer(listShimmer)

        var systemData = viewModel.systemsPoints.value.data?.results?.get(0)

        tvTitle.text = systemData?.points_system_header?.title

        rvElements.apply {
            layoutManager = _layoutManager

            if (page == 1) {
                adapter = factoryAdapter
            }

            var elements = mutableListOf<ViewModel>()


            if (page == 1) {
                elements.add(ItemShowAllRankingViewModel("", ""))
            }

            elements.add(ItemHeaderListingViewModel("Sistemas"))

            when (elementType) {

                POINTS_LIST_TYPE.SYSTEMS -> {
                    viewModel.systemsPoints.value.data?.results?.forEachIndexed { index, systemData ->
                        elements.add(ItemSystemPointsViewModel(systemData))
                    }
                }
                else -> mutableListOf<ViewModel>()
            }


            factoryAdapter.addItems(elements)

            addOnScrollListener(EndlessScrollListener(20, _layoutManager, {
                when (elementType) {


                }
            }))
        }
    }

    private fun configureCriteriaList() {
        stopShimmer(listShimmer)

        var criterias = areaData.criteria?.data

        tvTitle.text = areaData?.title


        rvElements.apply {
            layoutManager = _layoutManager

            if (page == 1) {
                adapter = factoryAdapter
            }

            var elements = mutableListOf<ViewModel>()


            if (page == 1) {
                elements.add(ItemShowAllRankingViewModel("", ""))
            }

            elements.add(ItemHeaderListingViewModel("Critérios"))

            when (elementType) {
                POINTS_LIST_TYPE.CRITERIA -> {
                    criterias?.forEach {
                        elements.add(ItemCriteriaPointsViewModel(it))
                    }
                }
            }

            factoryAdapter.addItems(elements)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}