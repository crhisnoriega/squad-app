package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.content.Intent
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import co.socialsquad.squad.presentation.util.shareItem
import co.socialsquad.squad.presentation.util.toDurationString
import com.bumptech.glide.Glide
import io.branch.indexing.BranchUniversalObject
import kotlinx.android.synthetic.main.view_qualification_success_stories_item.view.*

const val QUALIFICATION_SUCCESS_STORIES_VIEW_HOLDER_ID = R.layout.view_qualification_success_stories_item

class QualificationSuccessStoriesViewHolder(
    itemView: View,
    private val onVideoClickedListener: Listener<SuccessStoriesViewModel>
) : ViewHolder<SuccessStoriesViewModel>(itemView) {

    override fun bind(viewModel: SuccessStoriesViewModel) {
        with(itemView) {
            Glide.with(this)
                .load(viewModel.cover)
                .roundedCorners(context)
                .crossFade()
                .into(ivCover)
            tvAuthor.text = viewModel.author
            tvAuthor.setTextColor(ColorUtils.stateListOf(viewModel.tintColor))
            tvAuthor.setOnClickListener {
                openUserActivity(viewModel.authorPk)
            }
            tvContent.text = viewModel.description
            tvTitle.text = viewModel.title
            val viewCountStringResId =
                if (viewModel.visualization == 1L) R.string.view_count_singular
                else R.string.view_count_plural
            tvVisualization.text = context.getString(viewCountStringResId, TextUtils.toUnitSuffix(viewModel.visualization))
            tvDuration.text = viewModel.duration?.toDurationString()
            setOnClickListener {
                onVideoClickedListener.onClick(viewModel)
                val intent = Intent(context, VideoActivity::class.java)
                intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, viewModel.mediaUrl)
                context.startActivity(intent)
            }
            ivShare.setOnClickListener {
                BranchUniversalObject().shareItem(viewModel.shareViewModel, Share.Feed, context)
            }
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivCover)
    }

    private fun openUserActivity(userPk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).apply {
            putExtra(USER_PK_EXTRA, userPk)
        }
        itemView.context.startActivity(intent)
    }
}
