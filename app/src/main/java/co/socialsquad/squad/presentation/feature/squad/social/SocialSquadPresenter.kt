package co.socialsquad.squad.presentation.feature.squad.social

import android.content.DialogInterface
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_COMPANY_FEED
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_COMPANY_LIKE_POST
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_TAP_COMPANY_SHARE_POST
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel
import co.socialsquad.squad.presentation.feature.social.LiveListViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialViewModelMapper
import co.socialsquad.squad.presentation.feature.squad.SquadRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SocialSquadPresenter @Inject constructor(
    private val socialSquadView: SocialSquadView,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository,
    private val squadRepository: SquadRepository,
    private val tagWorker: TagWorker
) {

    private var page = 1
    private var isLoading = false
    private var isComplete = false
    private val mapper = SocialViewModelMapper()
    private val compositeDisposable = CompositeDisposable()
    private val color = loginRepository.squadColor
    private val subdomain = loginRepository.subdomain

    fun onCreate() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_COMPANY_FEED)
        val isAudioEnabled = socialRepository.audioEnabled
        socialSquadView.onAudioToggled(isAudioEnabled)
        loginRepository.userCompany?.company?.let {
            socialSquadView.setupToolbar(
                it.name
                    ?: "Squad",
                it.textColor ?: "#FFFFFF", it.secondaryColor ?: "#FFFFFF"
            )
        }
        getSquadProfile()
        getSquadPosts()
    }

    private fun getSquadProfile() {
        loginRepository.userCompany?.company?.let {
            val squadProfile = SquadHeaderViewModel(
                it.name ?: "",
                it.businessTypeDisplay ?: "",
                it.avatar ?: "", it.secondaryColor ?: "#FFFFFF", it.textColor ?: "#FFFFFF"
            )
            socialSquadView.showProfile(squadProfile)
        }
    }

    private fun getSquadPosts() {
        compositeDisposable.add(
            squadRepository.getSquadFeed(page)
                .doOnSubscribe {
                    isLoading = true
                    socialSquadView.showLoadingMore()
                }
                .doOnTerminate {
                    isLoading = false
                    socialSquadView.hideLoadingMore()
                }
                .doOnNext { isComplete = it.next == null }
                .map { mapper.mapItems(it, socialRepository.audioEnabled, loginRepository.userCompany?.user?.avatar, color, subdomain) }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        socialSquadView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        socialSquadView.showMessage(R.string.profile_error_get_posts, null)
                    }
                )
        )
    }

    fun onAudioToggled(audioEnabled: Boolean) {
        socialRepository.audioEnabled = audioEnabled
    }

    fun onEditSelected(postViewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getSegmentation(postViewModel.pk)
                .doOnSubscribe { socialSquadView.showLoading() }
                .doOnTerminate { socialSquadView.hideLoading() }
                .subscribe(
                    {
                        postViewModel.audience = AudienceViewModel(postViewModel.audience.type, it)
                        when (postViewModel) {
                            is PostImageViewModel, is PostVideoViewModel -> socialSquadView.showEditMedia(postViewModel)
                            is PostLiveViewModel -> socialSquadView.showEditLive(postViewModel)
                        }
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        socialSquadView.showMessage(
                            R.string.social_error_failed_to_edit_post,
                            DialogInterface.OnDismissListener {
                                socialSquadView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun onDeleteClicked(viewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.deleteFeed(viewModel.pk)
                .doOnSubscribe { socialSquadView.showLoading() }
                .doOnTerminate {
                    socialSquadView.hideLoading()
                    socialSquadView.hideDeleteDialog()
                }.subscribe(
                    {
                        socialSquadView.removePost(viewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        socialSquadView.showMessage(
                            R.string.social_error_failed_to_delete_post,
                            DialogInterface.OnDismissListener {
                                socialSquadView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun onShareClicked() {
        tagWorker.tagEvent(TAG_TAP_COMPANY_SHARE_POST)
    }

    fun likePost(post: PostViewModel) {
        tagWorker.tagEvent(TAG_TAP_COMPANY_LIKE_POST)
        compositeDisposable.add(
            socialRepository.likePost(post.pk).subscribe(
                {
                    post.liked = it.liked
                    post.likeAvatars = it.likedUsersAvatars
                    post.likeCount = it.likesCount
                    socialSquadView.updatePost(post)
                },
                {
                    it.printStackTrace()
                    post.liked = !post.liked
                    socialSquadView.updatePost(post)
                },
                {}
            )
        )
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isComplete || isLoading) return

        compositeDisposable.add(
            squadRepository.getSquadFeed(page)
                .doOnSubscribe {
                    isLoading = true
                    socialSquadView.showLoadingMore()
                }
                .doOnTerminate {
                    isLoading = false
                }
                .doOnNext { isComplete = it.next == null }
                .map { mapper.mapItems(it, socialRepository.audioEnabled, loginRepository.userCompany?.user?.avatar, color, subdomain) }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        socialSquadView.hideLoadingMore()
                        socialSquadView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        socialSquadView.showMessage(
                            R.string.profile_error_get_posts,
                            DialogInterface.OnDismissListener {
                                socialSquadView.hideLoadingMore()
                            }
                        )
                    }
                )
        )
    }

    fun onRefresh() {
        compositeDisposable.clear()
        page = 1
        getSquadProfile()
        getSquadPosts()
    }
}
