package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExploreMedia(
    val url: String
) : Parcelable
