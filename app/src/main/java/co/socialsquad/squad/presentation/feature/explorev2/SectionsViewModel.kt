package co.socialsquad.squad.presentation.feature.explorev2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.data.local.Preferences
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionList
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class SectionsViewModel(
    private val exploreRepository: ExploreRepository,
    private val preferences: Preferences
) : ViewModel() {

    @ExperimentalCoroutinesApi
    private val sections = MutableStateFlow<Resource<SectionList>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    fun fetchAndSaveSections() {
        viewModelScope.launch {
            sections.value = Resource.loading(null)
            exploreRepository.getSections()
                .flowOn(Dispatchers.IO)
                .collect {
                    if (it.sections.isNotEmpty()) {
                        preferences.sections = Gson().toJson(it)
                    }
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchSections() {
        viewModelScope.launch {
            sections.value = Resource.loading(null)
            if (preferences.sections != null) {
                // TODO("remover persistencia dos dados da preferences")
                sections.value =
                    Resource.success(Gson().fromJson(preferences.sections, SectionList::class.java))
            } else {
                exploreRepository.getSections()
                    .catch { e ->
                        sections.value = Resource.error(e.toString(), null)
                    }
                    .flowOn(Dispatchers.IO)
                    .collect {
                        preferences.sections = Gson().toJson(it)
                        sections.value = Resource.success(it)
                    }
            }
        }
    }



    @ExperimentalCoroutinesApi
    fun sections(): StateFlow<Resource<SectionList>> {
        return sections
    }
}
