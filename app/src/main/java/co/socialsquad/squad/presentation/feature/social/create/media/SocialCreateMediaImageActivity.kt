package co.socialsquad.squad.presentation.feature.social.create.media

import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.setFlagTranslucentNavigationAndStatus
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_social_create_media_preview.*

class SocialCreateMediaImageActivity : BaseDaggerActivity() {

    companion object {
        const val EXTRA_URI = "EXTRA_URI"
    }

    private lateinit var ivImage: ImageView

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_create_media_preview)
        ivImage = social_create_media_preview_iv_image

        setupToolbar()

        val uri = intent.getParcelableExtra<Uri>(EXTRA_URI)
        setupImageFromUri(uri)
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
        setFlagTranslucentNavigationAndStatus()
    }

    private fun setupImageFromUri(uri: Uri) {
        Glide.with(this)
            .load(uri)
            .apply(
                RequestOptions
                    .skipMemoryCacheOf(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
            )
            .crossFade()
            .into(ivImage)
    }
}
