package co.socialsquad.squad.presentation.feature.immobiles.details.moredetails

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.ImmobileDetail
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_more_details.*

class MoreDetailsActivity : BaseDaggerActivity() {

    private var immobileDetail: ImmobileDetail? = null
    private var videoUri: Uri? = null

    companion object {
        const val extraDetails = "extra_details"
        const val extraPrimaryColor = "extra_primary_color"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_details)

        back.setOnClickListener { finish() }

        val jsonExtra = intent?.extras?.getString(extraDetails)
        jsonExtra?.apply {
            val gson = Gson()
            immobileDetail = gson.fromJson(jsonExtra, ImmobileDetail::class.java)

            immobileDetail?.apply {
                setupDetails(this.details)

                toolbarTitle.text = this.name

                if (this.media.url.isNotEmpty() && this.media.url.toLowerCase().contains("mp4")) {
                    videoUri?.let { videoView.prepare(it) }
                    videoView.apply {
                        visibility = View.VISIBLE
                        nestedScrollView.post {
                            nestedScrollView.fullScroll(View.FOCUS_UP)
                        }

                        videoUri = Uri.parse(immobileDetail!!.media.url)
                        prepare(videoUri!!)
                        startTimer()
                    }
                } else {
                    Glide.with(baseContext)
                        .load(this.media.url)
                        .into(imageView)

                    imageView.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun startTimer() {
        val thread = object : Thread() {

            override fun run() {
                sleep(300)
                runOnUiThread {
                    videoView.play()
                }
            }
        }

        thread.start()
    }

    private fun setupDetails(details: List<ImmobileDetail.Detail>) {
        detailsList.run {
            layoutManager = LinearLayoutManager(baseContext)
            adapter = MoreDetailsAdapter(details, intent.extras?.getString(extraPrimaryColor)!!)
        }
    }

    override fun onResume() {
        super.onResume()
        immobileDetail?.apply {
            if (this.media.url.contains("mp4")) {
                videoUri?.let { videoView.prepare(it) }
            }
        }
    }

    override fun onPause() {
        immobileDetail?.apply {
            if (this.media.url.contains("mp4")) {
                videoUri?.let { videoView.pause() }
            }
        }
        super.onPause()
    }
}
