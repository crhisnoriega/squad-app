package co.socialsquad.squad.presentation.feature.opportunity.detail

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.opportunity.ObjectStagesStatus
import co.socialsquad.squad.data.entity.opportunity.SubmissionStages

class SubmissionStagesAdapter(val context: Context, private val subimissionStages: List<SubmissionStages>) :
    RecyclerView.Adapter<SubmissionStagesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.subimission_stage_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = subimissionStages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val stage = subimissionStages[position]

        if (position == 0) {
            holder.lineTop.visibility = View.GONE
            holder.lineBottom.visibility = View.VISIBLE
            holder.separator.visibility = View.VISIBLE
        } else if (position == itemCount - 1) {
            holder.lineTop.visibility = View.VISIBLE
            holder.lineBottom.visibility = View.GONE
            holder.separator.visibility = View.GONE
        }

        holder.name.text = stage.name
        holder.description.text = stage.description
        holder.date.text = stage.timestamp

        val commission = stage.comission
        holder.commission.setTextColor(Color.parseColor(commission.color))
        holder.commission.text = commission.text

        imageStageByStatus(stage.status)?.let { holder.iconStatus.setImageResource(it) }

        handlerClosedBlockedContainer(holder, stage)

        holder.date.visibility = if (ObjectStagesStatus.PENDING == stage.status) View.GONE else View.VISIBLE
        holder.dot.visibility = if (ObjectStagesStatus.PENDING == stage.status) View.GONE else View.VISIBLE
    }

    private fun handlerClosedBlockedContainer(holder: ViewHolder, stage: SubmissionStages) {

        when (stage.status) {
            ObjectStagesStatus.CLOSED, ObjectStagesStatus.BLOCKED -> {
                stage.issue?.run {
                    holder.issueName.text = title
                    holder.issueDescription.text = description
                    holder.issueAction.text = actionText
                }
                holder.clesedContainer.visibility = View.VISIBLE
                holder.arrowAccess.visibility = View.GONE

                val color = if (stage.status == ObjectStagesStatus.BLOCKED)
                    ContextCompat.getColor(context, R.color.brownish_orange) else ContextCompat.getColor(context, R.color._c7292b)

                holder.issueName.setTextColor(color)
                holder.issueStatus.setBackgroundColor(color)
            }
            else -> {
                holder.clesedContainer.visibility = View.GONE
                holder.arrowAccess.visibility = View.VISIBLE
            }
        }

        if (ObjectStagesStatus.CLOSED == stage.status || ObjectStagesStatus.BLOCKED == stage.status) {
        } else {
            holder.clesedContainer.visibility = View.GONE
            holder.arrowAccess.visibility = View.VISIBLE
        }
    }

    private fun imageStageByStatus(status: ObjectStagesStatus): Int? {
        return when (status) {
            ObjectStagesStatus.COMPLETED -> {
                R.drawable.ic_submission_status_completed
            }
            ObjectStagesStatus.CLOSED -> {
                R.drawable.ic_status_closed
            }
            ObjectStagesStatus.BLOCKED -> {
                R.drawable.ic_status_blocked
            }
            else -> {
                null
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.short_title_lead)
        val description: TextView = itemView.findViewById(R.id.tvDescription)
        val date: TextView = itemView.findViewById(R.id.tvDate)
        val commission: TextView = itemView.findViewById(R.id.tvCommission)
        val dot: TextView = itemView.findViewById(R.id.tvDot)
        val iconStatus: ImageView = itemView.findViewById(R.id.ivIconStatus)
        val arrowAccess: ImageView = itemView.findViewById(R.id.ibArrowAccess)
        val lineTop: View = itemView.findViewById(R.id.vLineTop)
        val lineBottom: View = itemView.findViewById(R.id.vLineBottom)
        val separator: View = itemView.findViewById(R.id.vSeparator)
        val clesedContainer: ConstraintLayout = itemView.findViewById(R.id.clClosedContainer)
        val issueStatus: View = itemView.findViewById(R.id.vIssue)
        val issueName: TextView = itemView.findViewById(R.id.tvIssueName)
        val issueDescription: TextView = itemView.findViewById(R.id.tvIssueDescription)
        val issueAction: TextView = itemView.findViewById(R.id.tvIssueAction)
    }
}
