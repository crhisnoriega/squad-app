package co.socialsquad.squad.presentation.feature.social.comment

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel

const val COMMENT_VIEW_ID = R.layout.view_comment_list_item

class CommentViewModel(
    var pk: Long,
    var userPk: Int,
    var userAvatar: String?,
    var userName: String,
    var text: String,
    var time: String?,
    var canDelete: Boolean = false,
    var canEdit: Boolean = false
) : ViewModel
