package co.socialsquad.squad.presentation.feature.squad.social

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.PrefetchingLinearLayoutManager
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.social.AudioToggledViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostLiveViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SOCIAL_POST_IMAGE_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.SOCIAL_POST_VIDEO_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.SocialDeleteDialog
import co.socialsquad.squad.presentation.feature.social.SocialFragment
import co.socialsquad.squad.presentation.feature.social.SocialPresenter
import co.socialsquad.squad.presentation.feature.social.SocialViewModel
import co.socialsquad.squad.presentation.feature.social.create.live.SocialCreateLiveActivity
import co.socialsquad.squad.presentation.feature.social.create.media.SocialCreateMediaActivity
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.feature.social.viewholder.PostImageViewHolder
import co.socialsquad.squad.presentation.feature.social.viewholder.PostVideoViewHolder
import co.socialsquad.squad.presentation.feature.squad.social.viewHolder.SQUAD_HEADER_VIEW_ID
import co.socialsquad.squad.presentation.feature.squad.social.viewHolder.SquadHeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.feature.video.VideoActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.setFlagLayoutFullscreen
import kotlinx.android.synthetic.main.activity_social_squad.*
import javax.inject.Inject

const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X"
const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"

class SocialSquadActivity : BaseDaggerActivity(), SocialSquadView {
    @Inject
    lateinit var socialSquadPresenter: SocialSquadPresenter

    private var socialAdapter: RecyclerViewAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var currentMostVisibleItemPosition = -1
    private var revealX: Int? = null
    private var revealY: Int? = null

    private var socialDeleteDialog: SocialDeleteDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFlagLayoutFullscreen()
        setContentView(R.layout.activity_social_squad)
        animateStart(savedInstanceState)
        setupList()
        setupSwipeRefresh()
        socialSquadPresenter.onCreate()
    }

    private fun animateStart(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) return
        revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, -1)
        revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, -1)
        if (revealX == null || revealY == null || revealX!! < 0 || revealY!! < 0) return
        val viewTreeObserver = rootLayout.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    circularRevealActivity()
                    rootLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }
    }

    private var newMenuColor: String = "#121212"

    override fun setupToolbar(title: String, textColor: String, backgroundColor: String) {
        abBar.setBackgroundColor(Color.parseColor(backgroundColor))
        tToolbar.setTitleTextColor(Color.parseColor(textColor))
        setSupportActionBar(tToolbar)
        val upArrow = ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp)
        upArrow?.setColorFilter(Color.parseColor(textColor), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.apply {
            this.title = title
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(upArrow)
        }
        newMenuColor = textColor
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupSwipeRefresh() {
        srlContainer.setOnRefreshListener { socialSquadPresenter.onRefresh() }
    }

    private fun setupList() {
        socialAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is SquadHeaderViewModel -> SQUAD_HEADER_VIEW_ID
                is PostVideoViewModel -> SOCIAL_POST_VIDEO_VIEW_MODEL_ID
                is PostImageViewModel -> SOCIAL_POST_IMAGE_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    SQUAD_HEADER_VIEW_ID -> SquadHeaderViewHolder(view)
                    SOCIAL_POST_VIDEO_VIEW_MODEL_ID -> PostVideoViewHolder(
                        view,
                        onVideoClickedListener,
                        onAudioClickedListener,
                        onDeleteListener,
                        onEditListener,
                        onLikeClickListener,
                        onUsersLikedListener,
                        onCommentListener,
                        onCommentListListener,
                        onShareListener
                    )
                    SOCIAL_POST_IMAGE_VIEW_MODEL_ID -> PostImageViewHolder(
                        view,
                        onDeleteListener,
                        onEditListener,
                        onLikeClickListener,
                        onUsersLikedListener,
                        onCommentListener,
                        onCommentListListener,
                        onShareListener
                    )
                    LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                    else -> throw IllegalArgumentException()
                }
            }
        })
        linearLayoutManager = PrefetchingLinearLayoutManager(this)
        rvPosts?.apply {
            adapter = socialAdapter
            layoutManager = linearLayoutManager
            setItemViewCacheSize(0)
            addOnScrollListener(EndlessScrollListener())
            setHasFixedSize(true)
            isFocusable = false
        }
    }

    override fun showProfile(profileViewModel: SquadHeaderViewModel) {
        socialAdapter?.setItems(listOf(profileViewModel))
    }

    override fun showPosts(posts: List<SocialViewModel>) {
        socialAdapter?.addItems(posts)
    }

    private val onCommentListListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@SocialSquadActivity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL, viewModel)
                    is PostVideoViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL, viewModel)
                    is PostImageViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL, viewModel)
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
            }
            startActivity(intent)
        }
    }

    private val onCommentListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@SocialSquadActivity, PostDetailsActivity::class.java).apply {
                when (viewModel) {
                    is PostLiveViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_LIVE_VIEW_MODEL, viewModel)
                    is PostVideoViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_VIDEO_VIEW_MODEL, viewModel)
                    is PostImageViewModel -> putExtra(PostDetailsActivity.POST_DETAIL_IMAGE_VIEW_MODEL, viewModel)
                    else -> putExtra(PostDetailsActivity.POST_DETAIL_PK, viewModel.pk)
                }
                putExtra(PostDetailsActivity.IS_COMMENTING, true)
            }
            startActivity(intent)
        }
    }

    private val onUsersLikedListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            val intent = Intent(this@SocialSquadActivity, UserListActivity::class.java).apply {
                putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.POST_LIKE)
            }
            startActivity(intent)
        }
    }

    private val onDeleteListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onDeleteSelected(viewModel)
        }
    }

    fun onDeleteSelected(viewModel: PostViewModel) {
        socialDeleteDialog = SocialDeleteDialog(
            this,
            object : SocialDeleteDialog.Listener {
                override fun onNoClicked() {
                    hideDeleteDialog()
                }

                override fun onYesClicked() {
                    socialSquadPresenter.onDeleteClicked(viewModel)
                    setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
                }
            }
        )

        if (!this.isFinishing) socialDeleteDialog?.show()
    }

    private val onShareListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            socialSquadPresenter.onShareClicked()
        }
    }

    private val onLikeClickListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            socialSquadPresenter.likePost(viewModel)
        }
    }

    private val onEditListener = object : ViewHolder.Listener<PostViewModel> {
        override fun onClick(viewModel: PostViewModel) {
            onEditSelected(viewModel)
        }
    }

    fun onEditSelected(viewModel: PostViewModel) {
        socialSquadPresenter.onEditSelected(viewModel)
    }

    private val onVideoClickedListener = object : ViewHolder.Listener<PostVideoViewModel> {
        override fun onClick(viewModel: PostVideoViewModel) {
            onVideoClicked(viewModel.hlsUrl)
        }
    }

    fun onVideoClicked(url: String) {
        setMostVisibleItemPosition(-1, currentMostVisibleItemPosition)
        val intent = Intent(this, VideoActivity::class.java)
        intent.putExtra(VideoActivity.EXTRA_VIDEO_URL, url)
        startActivityForResult(intent, SocialPresenter.REQUEST_CODE_VIDEO_FULLSCREEN)
    }

    override fun showEditMedia(post: PostViewModel) {
        val intent = Intent(this, SocialCreateMediaActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_MEDIA)
    }

    override fun showEditLive(post: PostLiveViewModel) {
        val intent = Intent(this, SocialCreateLiveActivity::class.java)
        intent.putExtra(SocialCreateMediaActivity.EXTRA_POST, post)
        startActivityForResult(intent, SocialFragment.REQUEST_CODE_EDIT_LIVE)
    }

    override fun removePost(viewModel: ViewModel) {
        socialAdapter?.remove(viewModel)
    }

    override fun updatePost(post: PostViewModel) {}

    override fun showMessage(resId: Int, onDismissListener: DialogInterface.OnDismissListener?) {
        ViewUtils.showDialog(this, getString(resId), onDismissListener)
    }

    private val onAudioClickedListener = object : ViewHolder.Listener<AudioToggledViewModel> {
        override fun onClick(viewModel: AudioToggledViewModel) {
            onAudioToggled(viewModel.audioEnabled)
            val items = socialAdapter?.getItemFromType(PostVideoViewModel::class.java)
            items?.forEach {
                it.audioEnabled = viewModel.audioEnabled
                socialAdapter?.update(it, false)
            }
        }
    }

    override fun onAudioToggled(audioEnabled: Boolean) {
        socialSquadPresenter.onAudioToggled(audioEnabled)
    }

    override fun stopRefreshing() {
        srlContainer.isRefreshing = false
    }

    private fun setMostVisibleItemPosition(mostVisibleItemPosition: Int, previousMostVisibleItemPosition: Int) {
        stopVideo(previousMostVisibleItemPosition)
        playVideo(mostVisibleItemPosition)
    }

    private fun stopVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.stop()
        }
    }

    private fun playVideo(position: Int) {
        if (position == -1) return
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play()
        }
    }

    private fun continueVideo(position: Int, playbackPosition: Long) {
        rvPosts?.let {
            val viewHolder = it.findViewHolderForAdapterPosition(position) as? PostVideoViewHolder
            viewHolder?.play(playbackPosition)
        }
    }

    override fun showLoading() {
        llLoading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        llLoading.visibility = View.GONE
        srlContainer.visibility = View.VISIBLE
    }

    override fun hideDeleteDialog() {
        socialDeleteDialog?.dismiss()
    }

    override fun showLoadingMore() {
        socialAdapter?.startLoading()
    }

    override fun hideLoadingMore() {
        socialAdapter?.stopLoading()
        srlContainer.isRefreshing = false
    }

    fun circularRevealActivity() {
        overridePendingTransition(R.anim.none, R.anim.none)
        rootLayout.visibility = View.GONE
        val finalRadius = Math.max(rootLayout.width, rootLayout.height)
        val circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, revealX!!, revealY!!, 0F, finalRadius.toFloat())
        circularReveal.duration = 400
        rootLayout.visibility = View.VISIBLE
        circularReveal.start()
    }

    fun circularUnrevealActivity() {
        overridePendingTransition(R.anim.none, R.anim.none)
        val startRatdius = Math.max(rootLayout.width, rootLayout.height) * 1.1F
        val circularReveal = ViewAnimationUtils.createCircularReveal(
            rootLayout,
            revealX
                ?: 0,
            revealY ?: 0, startRatdius, 0F
        )
        circularReveal.duration = 400
        circularReveal.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                rootLayout.visibility = View.INVISIBLE
                setResult(Activity.RESULT_OK)
                finish()
                overridePendingTransition(R.anim.none, R.anim.none)
            }
        })
        circularReveal.start()
    }

    override fun onBackPressed() {
        circularUnrevealActivity()
    }

    private inner class EndlessScrollListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 8

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            linearLayoutManager?.apply {
                if (findLastVisibleItemPosition() > itemCount - visibleThreshold) {
                    socialSquadPresenter.onScrolledBeyondVisibleThreshold()
                }

                val rect = Rect()
                recyclerView.getGlobalVisibleRect(rect)
                val containerHeight = rect.height()
                val referencePoint = containerHeight / 2 + rect.top

                val firstVisibleItemPosition = findFirstVisibleItemPosition()
                val lastVisibleItemPosition = findLastVisibleItemPosition()

                val previousMostVisibleItemPosition = currentMostVisibleItemPosition

                var mostVisibleItemPosition = firstVisibleItemPosition
                var leastDifferenceFromReferencePoint = containerHeight
                for (i in firstVisibleItemPosition..lastVisibleItemPosition) {
                    val viewHolder = recyclerView.findViewHolderForAdapterPosition(i)
                    if (viewHolder != null) {
                        val r = Rect()
                        viewHolder.itemView.getGlobalVisibleRect(r)
                        val difference = Math.abs(r.centerY() - referencePoint)
                        if (difference < leastDifferenceFromReferencePoint) {
                            leastDifferenceFromReferencePoint = difference
                            mostVisibleItemPosition = i
                        }
                    }
                }

                if (previousMostVisibleItemPosition != mostVisibleItemPosition) {
                    currentMostVisibleItemPosition = mostVisibleItemPosition
                    setMostVisibleItemPosition(mostVisibleItemPosition, previousMostVisibleItemPosition)
                }
            }
        }
    }
}
