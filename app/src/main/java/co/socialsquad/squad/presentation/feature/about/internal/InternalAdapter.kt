package co.socialsquad.squad.presentation.feature.about.internal

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.AboutResponse
import co.socialsquad.squad.presentation.util.ColorUtils
import com.bumptech.glide.Glide

class InternalAdapter(val items: List<AboutResponse.Topics.Item>, val textColor: String?) : RecyclerView.Adapter<InternalAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.about_internal_item, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.title.text = item.title
        holder.description.text = item.description
        Glide.with(holder.itemView.context).load(item.icon).into(holder.icon)
        if (position == items.size - 1) {
            holder.separator.visibility = View.GONE
        }

        textColor?.apply {
            val color = ColorUtils.parse(this)
            holder.title.setTextColor(color)
            holder.description.setTextColor(color)
            holder.container.setBackgroundColor(ColorUtils.parse("#19000000"))
            holder.separator.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, android.R.color.transparent))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.object_type_lead)
        val description: TextView = itemView.findViewById(R.id.status)
        val icon: ImageView = itemView.findViewById(R.id.icon)
        val separator: View = itemView.findViewById(R.id.separator)
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
    }
}
