package co.socialsquad.squad.presentation.feature.resources

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.data.utils.DownloadUtils
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.domain.model.SimpleMedia
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.custom.resource.viewHolder.ResourceSingleViewHolder2
import co.socialsquad.squad.presentation.custom.resource.viewHolder.ResourceSingleViewHolderModel2
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Resource
import co.socialsquad.squad.presentation.feature.resources.di.ResourceModule
import co.socialsquad.squad.presentation.feature.resources.viewmodel.ResourcesViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import com.google.gson.Gson
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_resources_list.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import java.io.File
import java.util.concurrent.TimeUnit


private const val RESOURCES_LIST = "resources_list"
private const val RESOURCES_TITLE = "resources_title"
private const val RESOURCES_SUBTITLE = "resources_subtitle"

class ResourcesListActivity : BaseActivity() {

    companion object {
        fun start(context: Context, title: String, subtitle: String, resources: List<Resource>) {
            context.startActivity(
                    Intent(context, ResourcesListActivity::class.java).apply {
                        putExtra(RESOURCES_LIST, Gson().toJson(resources))
                        putExtra(RESOURCES_TITLE, title)
                        putExtra(RESOURCES_SUBTITLE, subtitle)
                    }
            )
        }
    }

    private val jsonResources2 by extra<String>(RESOURCES_LIST)
    private val title by extra<String>(RESOURCES_TITLE)
    private val subtitle by extra<String>(RESOURCES_SUBTITLE)
    private val viewModel by viewModel<ResourcesViewModel>()
    private val companyColor by inject<CompanyColor>()
    private val defaultEmptyState by lazy {
        EmptyState(
                SimpleMedia(""),
                getString(R.string.opportunity_empty_state_msg),
                getString(R.string.opportunity_empty_state_button)
        )
    }

    override val modules: List<Module> = listOf(ResourceModule.instance)
    override val contentView = R.layout.activity_resources_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tvTitle.text = title

        setupViewModelObservers()
        setupRecyclerView()
        setupAdapterInitialData()
    }

    private fun setupViewModelObservers() {
        viewModel.statusMutable.observe(this, Observer {

        })
    }

    private fun hideEmptyState() {
        group_empty.isVisible = false
    }

    private fun showEmptyState(emptyState: EmptyState) {
        Glide.with(this)
                .load(emptyState.icon.url)
                .crossFade()
                .into(img_lead_empty)

        lbl_empty_lead_message.text = emptyState.title
        btn_empty_action.text = emptyState.subtitle
        group_empty.isVisible = true
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG
        ).show()
    }

    private fun setupAdapterInitialData() {

        rvResources.apply {
            layoutManager = LinearLayoutManager(context)

            val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
                override fun getType(viewModel: ViewModel) = when (viewModel) {
                    is ResourceSingleViewHolderModel2 -> ResourceSingleViewHolder2.ITEM_VIEW_MODEL_ID
                    else -> throw IllegalArgumentException()
                }

                override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                    ResourceSingleViewHolder2.ITEM_VIEW_MODEL_ID -> ResourceSingleViewHolder2(view, {
                        when (it.type) {
                            "folder" -> start(context, it.title!!, it.description!!, java.util.ArrayList(it.resources))
                            "file" -> {
                                // download file
                                var optionsDialog = ResourceActionsBottomSheetDialog()
                                optionsDialog?.show(this@ResourcesListActivity.supportFragmentManager, "")
                            }
                        }
                    }, {
                        var dialog = ResourceDownloadDialog()
                        dialog.show(this@ResourcesListActivity.supportFragmentManager, "")

                        val uri = Uri.parse(it.link!!)

                        val targetFile = File(context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS)[0], uri.lastPathSegment)
                        Log.i("download", "link: " + it.link!!)
                        Log.i("download", "file: " + targetFile.absoluteFile)

                        var utils = DownloadUtils()
                        var disposable = utils.download(it.link!!, targetFile)
                                .throttleFirst(5, TimeUnit.MINUTES)
                                .toFlowable(BackpressureStrategy.LATEST)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    Log.i("download", "$it% Downloaded")
                                }, {
                                    Log.i("download", it.message, it)
                                }, {
                                    Log.i("download", "completed")
                                    dialog.dismiss()

                                    val map = MimeTypeMap.getSingleton()
                                    val ext = MimeTypeMap.getFileExtensionFromUrl(targetFile.getName())
                                    var type = map.getMimeTypeFromExtension(ext)

                                    if (type == null) type = "*/*"

                                    val intent = Intent(Intent.ACTION_VIEW)
                                    val data: Uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", targetFile);

                                    intent.setDataAndType(data, type);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                    startActivity(intent);
                                })
                    })
                    else -> throw IllegalArgumentException()
                }
            })

            adapter = factoryAdapter.apply {
                var counter = 0
                var resources: List<Resource> = Gson().fromJson(jsonResources2, Array<Resource>::class.java).toList()
                resources.forEach {
                    addItem(ResourceSingleViewHolderModel2(it), counter++)
                }
            }
        }
    }


    private fun setupRecyclerView() {

    }
}
