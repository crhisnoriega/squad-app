package co.socialsquad.squad.presentation.feature.audio.list

import dagger.Binds
import dagger.Module

@Module
abstract class AudioListModule {
    @Binds
    abstract fun view(audioListFragment: AudioListFragment): AudioListView
}
