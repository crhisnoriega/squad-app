package co.socialsquad.squad.presentation.feature.document.list

import android.content.Intent
import android.net.Uri
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import co.socialsquad.squad.presentation.feature.profile.others.USER_PK_EXTRA
import co.socialsquad.squad.presentation.feature.profile.others.UserActivity
import co.socialsquad.squad.presentation.feature.users.UserListActivity
import co.socialsquad.squad.presentation.feature.users.UserListPresenter
import co.socialsquad.squad.presentation.util.TextUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.elapsedTime
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_document_list_item.view.*

class DocumentListItemViewHolder(
    itemView: View,
    private val onDocumentClickedListener: ViewHolder.Listener<DocumentViewModel>
) : ViewHolder<DocumentViewModel>(itemView) {
    override fun bind(viewModel: DocumentViewModel) {
        itemView.apply {
            Glide.with(context)
                .load(viewModel.cover)
                .roundedCorners(context)
                .crossFade()
                .into(ivImage)

            tvTitle.text = viewModel.title

            tvDescription.text = viewModel.description

            tvAuthor.apply {
                text = viewModel.author
                viewModel.authorPk?.let { pk -> setOnClickListener { openUser(pk) } }
            }

            val viewCountResId = if (viewModel.viewCount == 1) R.string.view_count_singular else R.string.view_count_plural
            val viewCount = context.getString(viewCountResId, TextUtils.toUnitSuffix(viewModel.viewCount.toLong()))
            val timestamp = viewModel.timestamp?.elapsedTime(context)
            tvViewCountAndTimestamp.text = listOfNotNull(viewCount, timestamp).joinToString(" • ")
            tvViewCountAndTimestamp.setOnClickListener {
                val intent = Intent(itemView.context, UserListActivity::class.java).apply {
                    putExtra(UserListActivity.QUERY_ITEM_PK, viewModel.pk)
                    putExtra(UserListActivity.USER_LIST_TYPE, UserListPresenter.Type.DOCUMENTS_VISUALIZATION)
                }
                itemView.context.startActivity(intent)
            }
            viewModel.fileUrl?.let { url ->
                setOnClickListener {
                    openExternalBrowser(url)
                    onDocumentClickedListener.onClick(viewModel)
                }
            }
        }
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(itemView.ivImage)
    }

    private fun openUser(pk: Int) {
        val intent = Intent(itemView.context, UserActivity::class.java).putExtra(USER_PK_EXTRA, pk)
        itemView.context.startActivity(intent)
    }

    private fun openExternalBrowser(fileUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(fileUrl))
        itemView.context.startActivity(intent)
    }
}
