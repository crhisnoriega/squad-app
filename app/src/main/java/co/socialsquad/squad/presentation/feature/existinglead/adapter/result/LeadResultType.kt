package co.socialsquad.squad.presentation.feature.existinglead.adapter.result

import android.os.Parcelable
import co.socialsquad.squad.domain.model.leadsearch.ExistingLeadType
import kotlinx.android.parcel.Parcelize

sealed class LeadResultType(val value: Int) : Parcelable {

    companion object {
        fun getByValue(value: ExistingLeadType) = when (value) {
            ExistingLeadType.Item -> Item
            ExistingLeadType.Separator -> Separator
            else -> Invalid
        }
    }

    @Parcelize
    object Invalid : LeadResultType(99)

    @Parcelize
    object Separator : LeadResultType(0)

    @Parcelize
    object Item : LeadResultType(1)
}
