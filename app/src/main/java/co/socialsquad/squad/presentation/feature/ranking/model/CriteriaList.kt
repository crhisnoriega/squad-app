package co.socialsquad.squad.presentation.feature.ranking.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CriteriaList(
        @SerializedName("data") val list: List<CriteriaData>?,
        @SerializedName("show_all") val show_all: Boolean?,
        @SerializedName("title") val title: String?,
) : Parcelable