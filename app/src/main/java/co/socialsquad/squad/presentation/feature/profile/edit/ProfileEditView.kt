package co.socialsquad.squad.presentation.feature.profile.edit

import co.socialsquad.squad.data.entity.User
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.City
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.Neighborhood
import co.socialsquad.squad.presentation.feature.profile.edit.viewmodel.State

interface ProfileEditView {
    fun setupInfo(user: User, bio: String?)
    fun showStates(states: List<State>)
    fun showCities(cities: List<City>)
    fun showNeighborhoods(neighborhoods: List<Neighborhood>)
    fun setSaveButtonEnabled(enabled: Boolean)
    fun showLoading()
    fun hideLoading()
    fun showErrorMessageFailedToGetStates()
    fun showErrorMessageFailedToGetCities()
    fun showErrorMessageFailedToGetNeighborhoods()
    fun showErrorMessageFailedToRegisterUser()
    fun finishAfterSuccess()
}
