package co.socialsquad.squad.presentation.feature.social.create.media

import android.content.Intent
import android.net.Uri
import co.socialsquad.squad.presentation.feature.social.AudienceViewModel

interface SocialCreateMediaView {
    fun setupToolbar(titleResId: Int)
    fun setupOnClickListeners()
    fun setupKeyboardActions()
    fun setupImageFromUri(uri: Uri, width: Int? = null, height: Int? = null)
    fun setupVideoFromUri(videoUri: Uri, thumbnailUri: Uri, width: Int? = null, height: Int? = null)
    fun setupVideoFromFile(uri: Uri)
    fun setupAudience(audience: AudienceViewModel)
    fun showMedia(isVideo: Boolean = false)
    fun finishWithResult(intent: Intent? = null)
    fun setupDescription(description: String)
    fun showLoading()
    fun hideLoading()
    fun showMessage(textResId: Int)
    fun setupActionBarActionTitle(stringId: Int)
    fun enableButton()
    fun setupAudienceColor(color: String)
    fun hideSchedule()
}
