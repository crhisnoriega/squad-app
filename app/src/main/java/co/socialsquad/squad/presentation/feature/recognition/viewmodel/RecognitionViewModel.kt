package co.socialsquad.squad.presentation.feature.recognition.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.ranking.model.RankingResponse
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import co.socialsquad.squad.presentation.feature.recognition.model.PlanData
import co.socialsquad.squad.presentation.feature.recognition.model.PlanSummaryResponse
import co.socialsquad.squad.presentation.feature.recognition.repository.RecognitionRepository
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import org.json.JSONObject

@ExperimentalCoroutinesApi
class RecognitionViewModel(
        var recognitionRepository: RecognitionRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    val fetchPlanSummary = MutableStateFlow<Resource<PlanSummaryResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val fetchPlan = MutableStateFlow<Resource<PlanData>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val refuseTerms = MutableStateFlow<Resource<AgreeRefuseTermsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val agreeTerms = MutableStateFlow<Resource<AgreeRefuseTermsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    fun fetchPlanSummary() {
        viewModelScope.launch {
            recognitionRepository.fetchPlanSummary()
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rek", e.message, e)
                        fetchPlanSummary.value = Resource.error("", null)
                    }
                    .collect {
                        Log.i("rek", Gson().toJson(it))
                        fetchPlanSummary.value = Resource.success(it)
                    }
        }
    }

    lateinit var planData: PlanData

    @ExperimentalCoroutinesApi
    fun fetchPlan(planId: String) {
        viewModelScope.launch {
            recognitionRepository.fetchPlan(planId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rek", e.message, e)
                        fetchPlan.value = Resource.error("", null)
                    }
                    .collect {
                        Log.i("rek", Gson().toJson(it))
                        planData = it
                        fetchPlan.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun refuseRecognitionTerms(planId: String) {
        viewModelScope.launch {
            recognitionRepository.refuseRecongTerms(planId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rek", e.message, e)
                    }
                    .collect {
                        Log.i("rek", "refuse: $it")
                        refuseTerms.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun agreeRecognitionTerms(planId: String) {
        viewModelScope.launch {
            recognitionRepository.agreeRecongTerms(planId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rek", e.message, e)
                    }
                    .collect {
                        Log.i("rek", "agree: $it")
                        agreeTerms.value = Resource.success(it)
                    }
        }
    }

}
