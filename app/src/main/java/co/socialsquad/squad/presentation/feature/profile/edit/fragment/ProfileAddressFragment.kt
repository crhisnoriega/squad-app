package co.socialsquad.squad.presentation.feature.profile.edit.fragment

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.User
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.ProfileEditViewModel
import co.socialsquad.squad.presentation.feature.kickoff.viewModel.StateNav
import co.socialsquad.squad.presentation.feature.profile.edit.CompleteProfileActivity
import co.socialsquad.squad.presentation.feature.profile.edit.customview.ChooseStateSheetDialog
import co.socialsquad.squad.presentation.feature.profile.edit.dialog.ConfirmLeaveNoSaveDialog
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.fragment_edit_profile_address.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

private const val REQUEST_CODE_PICK_IMAGE = 1
private const val REQUEST_TAKE_PICTURE = 2
private const val REQUEST_PERMISSION_PICTURE = 3


class ProfileAddressFragment(
    val user: User? = null,
    var fragments: List<Fragment>? = null,
    var isInCompleteProfile: Boolean? = false
) : Fragment() {

    private val viewModel by sharedViewModel<ProfileEditViewModel>()

    var listener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            btnSalvar.isEnabled = itNumero.text.isNullOrBlank().not() &&
                    inputCEP.text.isNullOrBlank().not() &&
                    itLogradouro.text.isNullOrBlank().not() &&
                    itCidade.text.isNullOrBlank().not() &&
                    itBairro.text.isNullOrBlank().not() &&
                    itNumero.text.isNullOrBlank().not() &&
                    itEstado.text.isNullOrBlank().not()
        }

        override fun afterTextChanged(s: Editable?) {

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureButtons()
        configureObservables()

        populateData()


        btnSalvar.isEnabled = false
        configureInputs()

        inputCEP.addEditTextChangedListener(listener)
        itLogradouro.addEditTextChangedListener(listener)
        itNumero.addEditTextChangedListener(listener)
        itComplemento.addEditTextChangedListener(listener)
        itBairro.addEditTextChangedListener(listener)
        itEstado.addEditTextChangedListener(listener)
        itCidade.addEditTextChangedListener(listener)

        configureStateList()

        setupKeyboardListener(nestedScroll)

        viewModel.saveStatus.observe(requireActivity(), Observer {
            when (it) {
                "address" -> {
                    viewModel.state.value = StateNav("next", true, this)
                    (activity as? CompleteProfileActivity)?.next()
                    hideLoading()
                    btnSalvar.isEnabled = true
                }
            }
        })

        configureFragmentByPosition()
    }

    fun configureFragmentByPosition() {
        if (isInCompleteProfile!!) {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.light_mode_identity_profile_complete_your_profile_details))
            fragments?.forEachIndexed { index, fragment ->
                if (fragment == this)
                    txtPostion.text = "${index + 1} de ${fragments?.size}"
            }
            txtPostion.isVisible = true
            btnSalvar.text = "Próximo"
            txtTitle.text = "Perfil"
        } else {
            imgLead.setImageDrawable(context?.getDrawable(R.drawable.identity_recognition_requirements_location))
            txtTitle.text = "Endereço"
        }
    }

    override fun onResume() {
        super.onResume()


    }

    private fun setupKeyboardListener(view: View) {
        view.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            view.getWindowVisibleDisplayFrame(r)
            if (Math.abs(view.rootView.height - (r.bottom - r.top)) > 100) { // if more than 100 pixels, its probably a keyboard...
                onKeyboardShow()
            }
        }
    }

    private fun onKeyboardShow() {
        Handler().postDelayed({
            itCidade?.isFF?.let {
                if (it) nestedScroll.scrollToBottomWithoutFocusChange(50)
            }

            itComplemento?.isFF?.let {
                if (it) nestedScroll.scrollToBottomWithoutFocusChange(50)
            }
        }, 500)
    }

    fun NestedScrollView.scrollToBottomWithoutFocusChange(offset: Int) {
        smoothScrollBy(0, offset)
    }

    private fun configureStateList() {
        itEstado.doClickable {
            var stateDialog = ChooseStateSheetDialog(itEstado.text) { uf, state ->
                itEstado.text = uf
                btnSalvar.isEnabled = true
            }
            stateDialog.show(childFragmentManager, "state")
        }
    }

    private fun configureInputs() {
        inputCEP.setEditInputMask(MaskUtils.Mask.CEP)
        inputCEP.setEditInputType(InputType.TYPE_CLASS_NUMBER)
        inputCEP.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var cep = MaskUtils.unmask(s.toString(), MaskUtils.Mask.CEP)
                if (cep.length == 8) {
                    showLoading2()
                    Handler().postDelayed({ viewModel.queryCEP(cep) }, 1000L)
                } else {
                    stopAnimation2()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        itNumero.setEditInputType(InputType.TYPE_CLASS_NUMBER)
    }

    private var buttonText = ""
    private fun showLoading() {
        buttonText = btnSalvar.text.toString()
        btnSalvar.text = ""
        buttonIcon.visibility = View.VISIBLE

        val rotation = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation.repeatCount = Animation.INFINITE
        buttonIcon.startAnimation(rotation)
    }

    private fun hideLoading() {
        if (isInCompleteProfile!!) {
            btnSalvar.text = "Próximo"
        } else {
            btnSalvar.text = "Salvar"
        }
        buttonIcon.visibility = View.GONE

        buttonIcon.clearAnimation()
    }

    var rotation1: Animation? = null

    private fun showLoading2() {
        search_cep.visibility = View.GONE
        buttonIcon2.visibility = View.VISIBLE

        rotation1 = AnimationUtils.loadAnimation(requireContext(), R.anim.clockwise_rotation)
        rotation1?.repeatCount = Animation.INFINITE
        buttonIcon2.startAnimation(rotation1)
    }

    private fun stopAnimation2() {
        rotation1?.cancel()
        buttonIcon2.visibility = View.GONE
        search_cep.visibility = View.GONE
    }

    private var wasChanged = false
    private fun populateData() {
        user?.let {
            itLogradouro.text = it.street
            itNumero.text = it.number
            itEstado.text = it.address_state
            itCidade.text = it.address_city
            itBairro.text = it.address_neighborhood
            inputCEP.text = it.zipcode
            itComplemento.text = it.complement
        }

        itLogradouro.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.street
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        itNumero.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.number
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        inputCEP.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.zipcode
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        itComplemento.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.complement
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        itCidade.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.address_city
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        itBairro.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.address_neighborhood
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        itEstado.addEditTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                wasChanged = s.toString() != user?.address_state
            }

            override fun afterTextChanged(s: Editable?) {}
        })
    }

    private fun configureObservables() {
        viewModel.cepResponse.observe(requireActivity(), androidx.lifecycle.Observer {

            stopAnimation2()

            if (it.erro != null) {
                if (it.erro!!) {
                    itLogradouro.text = ""
                    itBairro.text = ""
                    itCidade.text = ""
                    itEstado.text = ""
                    itComplemento.text = ""
                } else {
                    itLogradouro.text = it.logradouro
                    itBairro.text = it.bairro
                    itCidade.text = it.localidade
                    itEstado.text = it.uf
                }
            } else {
                itLogradouro.text = it.logradouro
                itBairro.text = it.bairro
                itCidade.text = it.localidade
                itEstado.text = it.uf
            }

            hideSoftKeyboard(inputCEP)
            hideLoading()

        })


    }

    private fun hideSoftKeyboard(view: View) {
        val imm =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun configureButtons() {
        btnSalvar.setOnClickListener {
            btnSalvar.isEnabled = false

            showLoading()

            viewModel.saveAddress(
                street = itLogradouro.text,
                cep = inputCEP.text,
                number = itNumero.text,
                address_city = itCidade.text,
                address_state = itEstado.text,
                address_neighborhood = itBairro.text,
                complement = itComplemento.text
            )
        }

        btnBack.setOnClickListener {

            if (isInCompleteProfile!!) {
                (activity as? CompleteProfileActivity)?.previous()
            } else {
                if (wasChanged) {
                    var confirmDialog = ConfirmLeaveNoSaveDialog() {
                        when (it) {
                            "confirm" -> {
                                viewModel.state.postValue(StateNav("previous", true))
                                (activity as? CompleteProfileActivity)?.previous()
                            }
                        }
                    }
                    confirmDialog.show(childFragmentManager, "confirm")

                } else {
                    viewModel.state.postValue(StateNav("previous", true))
                    (activity as? CompleteProfileActivity)?.previous()
                }
            }
        }


    }


    private fun checkPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

    private fun canSave(canSave: Boolean) {
        btnSalvar.isEnabled = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_edit_profile_address, container, false)

    private fun requestGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(
            Intent.createChooser(intent, null),
            REQUEST_CODE_PICK_IMAGE
        )

    }


}
