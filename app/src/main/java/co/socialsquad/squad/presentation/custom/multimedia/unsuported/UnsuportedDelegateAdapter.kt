package co.socialsquad.squad.presentation.custom.multimedia.unsuported

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import co.socialsquad.squad.presentation.custom.multimedia.MediaViewHolder
import co.socialsquad.squad.presentation.custom.multimedia.ViewTypeDelegateAdapter

class UnsuportedDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, lifecycleOwner: LifecycleOwner?): MediaViewHolder =
        UnsuportedViewHolder(parent, lifecycleOwner)
}
