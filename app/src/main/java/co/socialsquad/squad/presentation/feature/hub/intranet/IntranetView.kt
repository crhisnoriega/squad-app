package co.socialsquad.squad.presentation.feature.hub.intranet

import android.webkit.WebViewClient
import co.socialsquad.squad.data.entity.VirtualOfficeResponse

interface IntranetView {
    fun setupWebView(webUrl: String, client: WebViewClient)
    fun showMessage(textResId: Int, onDismissListener: () -> Unit = {})
    fun showCompanyLoginScreen()
    fun showLoading()
    fun hideLoading()
    fun setupOffices(tools: MutableList<VirtualOfficeResponse.VirtualOffice.Tool>, secondaryColor: String?)
}
