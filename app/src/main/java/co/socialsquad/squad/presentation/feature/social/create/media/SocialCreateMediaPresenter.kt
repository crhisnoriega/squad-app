package co.socialsquad.squad.presentation.feature.social.create.media

import android.content.Intent
import android.net.Uri
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.FeedRequest
import co.socialsquad.squad.data.entity.SegmentationQueryRequest
import co.socialsquad.squad.data.entity.SegmentationRequest
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.PostImageViewModel
import co.socialsquad.squad.presentation.feature.social.PostVideoViewModel
import co.socialsquad.squad.presentation.feature.social.PostViewModel
import co.socialsquad.squad.presentation.feature.social.SocialFragment
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SocialCreateMediaPresenter
@Inject constructor(
    private val socialCreateMediaView: SocialCreateMediaView,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository,
    private val mixpanelAPI: MixpanelAPI
) {

    private val compositeDisposable = CompositeDisposable()
    private var post: PostViewModel? = null
    private var initialDescription: String? = null

    fun onCreate(intent: Intent) {
        socialCreateMediaView.setupOnClickListeners()
        socialCreateMediaView.setupKeyboardActions()
        socialCreateMediaView.setupAudienceColor(loginRepository.squadColor ?: "")

        post = intent.getSerializableExtra(SocialCreateMediaActivity.EXTRA_POST) as PostViewModel?
        if (post != null) {
            mixpanelAPI.track("Social / Edit / Media")
            setupEditPost()
        } else {
            mixpanelAPI.track("Social / Create / Media")
            setupNewMedia(intent)
        }
    }

    private fun setupEditPost() {
        socialCreateMediaView.setupToolbar(R.string.social_create_media_title_edit)
        socialCreateMediaView.setupActionBarActionTitle(R.string.social_create_media_button_save)
        when (post) {
            is PostImageViewModel -> with(post as PostImageViewModel) {
                description?.let {
                    initialDescription = it
                    socialCreateMediaView.setupDescription(it)
                }
                socialCreateMediaView.setupImageFromUri(Uri.parse(imageUrl), width, height)
                socialCreateMediaView.showMedia()
            }
            is PostVideoViewModel -> with(post as PostVideoViewModel) {
                description?.let {
                    initialDescription = it
                    socialCreateMediaView.setupDescription(it)
                }
                socialCreateMediaView.setupVideoFromUri(Uri.parse(hlsUrl), Uri.parse(imageUrl), width, height)
                socialCreateMediaView.showMedia(true)
            }
        }

        socialCreateMediaView.setupAudience(post!!.audience)
        socialCreateMediaView.hideSchedule()
    }

    private fun setupNewMedia(intent: Intent) {
        socialCreateMediaView.setupToolbar(R.string.social_create_media_title_new)
        val type = intent.getIntExtra(SocialCreateMediaActivity.EXTRA_TYPE, SocialCreateMediaActivity.TYPE_IMAGE)
        socialRepository.createMedia?.let {
            if (type == SocialCreateMediaActivity.TYPE_IMAGE) {
                socialCreateMediaView.setupImageFromUri(it)
                socialCreateMediaView.showMedia()
            } else if (type == SocialCreateMediaActivity.TYPE_VIDEO) {
                socialCreateMediaView.setupVideoFromFile(it)
                socialCreateMediaView.showMedia(true)
            }
        }
    }

    fun onPostClicked(description: String?, metrics: MetricsViewModel?, members: IntArray?, downline: Boolean, scheduledDate: Long?) {
        if (post != null) {
            val segmentationRequest = when {
                metrics != null && metrics.selected.isNotEmpty() -> {
                    val queries = metrics.selected.map { SegmentationQueryRequest(it.field, it.values.map { it.value }) }
                    SegmentationRequest(queries = queries)
                }
                members != null && members.isNotEmpty() -> SegmentationRequest(members = members.toList())
                downline -> SegmentationRequest(downline = true)
                else -> null
            }

            val feedRequest = FeedRequest(content = description, type = post!!.type, segmentation = segmentationRequest)

            compositeDisposable.add(
                socialRepository.editPost(post!!.pk, feedRequest)
                    .doOnSubscribe { socialCreateMediaView.showLoading() }
                    .doOnTerminate { socialCreateMediaView.hideLoading() }
                    .subscribe(
                        {
                            mixpanelAPI.track("Social / Edit / Media / Success ")
                            socialCreateMediaView.finishWithResult()
                        },
                        { throwable ->
                            throwable.printStackTrace()
                            mixpanelAPI.track("Social / Edit / Media / Failure ")
                            socialCreateMediaView.showMessage(R.string.social_create_media_error_failed_to_edit)
                        }
                    )
            )
        } else {
            val intent = Intent()
            intent.putExtra(SocialFragment.EXTRA_UPLOAD_DESCRIPTION, description)
            intent.putExtra(SocialFragment.EXTRA_UPLOAD_METRIC_VALUE, metrics)
            intent.putExtra(SocialFragment.EXTRA_UPLOAD_MEMBERS, members)
            intent.putExtra(SocialFragment.EXTRA_UPLOAD_DOWNLINE, downline)
            intent.putExtra(SocialFragment.EXTRA_UPLOAD_SCHEDULE, scheduledDate)

            socialCreateMediaView.finishWithResult(intent)
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onInput(editedText: String) {
        if (editedText != initialDescription) {
            socialCreateMediaView.enableButton()
        }
    }
}
