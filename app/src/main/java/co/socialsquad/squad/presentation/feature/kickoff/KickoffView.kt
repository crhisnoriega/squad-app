package co.socialsquad.squad.presentation.feature.kickoff

interface KickoffView {
    fun finishScreen()
}
