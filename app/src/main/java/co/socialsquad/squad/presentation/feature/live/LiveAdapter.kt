package co.socialsquad.squad.presentation.feature.live

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Comment
import co.socialsquad.squad.data.entity.Participant
import co.socialsquad.squad.data.entity.QueueItem
import co.socialsquad.squad.presentation.util.TextUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.relativelayout_live_queue_item_comment.view.*
import kotlinx.android.synthetic.main.relativelayout_live_queue_item_participant.view.*
import java.util.ArrayList

private const val TYPE_PARTICIPANT = 1
private const val TYPE_COMMENT = 0

class LiveAdapter(
    private val context: Context,
    private val layoutInflater: LayoutInflater
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val glide = Glide.with(context)
    private val queueItems = ArrayList<QueueItem>()

    override fun getItemViewType(position: Int): Int {
        val type = queueItems[position].type
        return when {
            type.equals(QueueItem.COMMENT, true) -> TYPE_COMMENT
            type.equals(QueueItem.PARTICIPANT, true) -> TYPE_PARTICIPANT
            else -> -1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_COMMENT -> {
                val view = layoutInflater.inflate(R.layout.relativelayout_live_queue_item_comment, parent, false)
                CommentViewHolder(view)
            }
            TYPE_PARTICIPANT -> {
                val view = layoutInflater.inflate(R.layout.relativelayout_live_queue_item_participant, parent, false)
                ParticipantViewHolder(view)
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val queueItem = queueItems[position]

        when (queueItem) {
            is Comment -> {
                val commentViewHolder = holder as CommentViewHolder
                val user = queueItem.participant.userCompany.user

                user.avatar?.let {
                    glide.load(it)
                        .circleCrop()
                        .into(commentViewHolder.ivAvatar)
                }

                val name = context.getString(
                    R.string.user_name, TextUtils.toCamelcase(user.firstName),
                    TextUtils.toCamelcase(user.lastName)
                )
                commentViewHolder.tvAuthor.text = name

                commentViewHolder.tvMessage.text = queueItem.text
            }
            is Participant -> {
                val participantViewHolder = holder as ParticipantViewHolder
                val user = queueItem.participant.userCompany.user

                user.avatar?.let {
                    glide.load(it)
                        .circleCrop()
                        .into(participantViewHolder.ivAvatar)
                }

                val message = context.getString(
                    R.string.live_queue_item_has_joined,
                    TextUtils.toCamelcase(user.firstName), TextUtils.toCamelcase(user.lastName)
                )

                participantViewHolder.tvMessage.text = message
            }
        }
    }

    override fun getItemCount() = queueItems.size

    fun addQueueItem(queueItem: QueueItem) {
        queueItems.add(queueItem)
        notifyItemInserted(queueItems.size)
    }

    private inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar: ImageView = itemView.live_queue_item_comment_iv_avatar
        val tvAuthor: TextView = itemView.live_queue_item_comment_tv_author
        val tvMessage: TextView = itemView.live_queue_item_comment_tv_message
    }

    private inner class ParticipantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar: ImageView = itemView.live_queue_item_participant_iv_avatar
        val tvMessage: TextView = itemView.live_queue_item_participant_tv_message
    }
}
