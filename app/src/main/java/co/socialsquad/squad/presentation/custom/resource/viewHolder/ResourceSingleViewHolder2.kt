package co.socialsquad.squad.presentation.custom.resource.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.Resource
import co.socialsquad.squad.presentation.util.isVisible
import kotlinx.android.synthetic.main.item_resource_list.view.*

class ResourceSingleViewHolder2(
        itemView: View,
        var callback: (resource: Resource) -> Unit,
        var callback2: (resource: Resource) -> Unit)
    : ViewHolder<ResourceSingleViewHolderModel2>(itemView) {
    companion object {
        const val ITEM_VIEW_MODEL_ID = R.layout.item_resource_list
    }

    override fun bind(viewModel: ResourceSingleViewHolderModel2) {
        itemView.tvTitle.text = viewModel.resource?.title
        itemView.tvModified.text = viewModel.resource?.description

        when (viewModel.resource?.type) {
            "folder" -> {
                itemView.ivInformationIcon.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    callback.invoke(viewModel.resource)
                }
            }
            "file" -> {
                itemView.ivOptions.visibility = View.VISIBLE
                itemView.ivInformationIcon.setOnClickListener {
                    callback.invoke(viewModel.resource)
                }
                itemView.setOnClickListener {
                    callback2.invoke(viewModel.resource)
                }
            }
        }

        itemView.bottomDivider.isVisible = viewModel.isLast.not()
        itemView.dividerTop.isVisible = false
    }

    override fun recycle() {

    }
}