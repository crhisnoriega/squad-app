package co.socialsquad.squad.presentation.feature.login.forgotpassword

import co.socialsquad.squad.data.entity.Company

interface ResetPasswordView {

    fun hideLoading()
    fun showLoading()
    fun setupHeader(company: Company)
}
