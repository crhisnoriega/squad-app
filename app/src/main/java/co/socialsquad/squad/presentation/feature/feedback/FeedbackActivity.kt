package co.socialsquad.squad.presentation.feature.feedback

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.util.ViewUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.hideKeyboard
import co.socialsquad.squad.presentation.util.showKeyboard
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_feedback.*
import javax.inject.Inject

class FeedbackActivity : BaseDaggerActivity(), FeedbackView {

    companion object {
        const val EXTRA_FEEDBACK_VIEW_MODEL = "EXTRA_FEEDBACK_VIEW_MODEL"
    }

    @Inject
    lateinit var presenter: FeedbackPresenter

    private lateinit var adapter: RecyclerViewAdapter

    private var loading: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        setupList()
        setupCloseButton()
        setupNextButton()
        presenter.onCreate(intent)
    }

    override fun setSection(sectionViewModel: FeedbackSectionViewModel) {
        tvToolbarTitle.text = sectionViewModel.title
        Glide.with(this)
            .load(sectionViewModel.imageUrl)
            .circleCrop()
            .crossFade()
            .into(ivIcon)
    }

    override fun setQuestion(questionViewModel: FeedbackQuestionViewModel, questionNumber: Int, numberOfQuestions: Int, shouldDisableNextButton: Boolean) {
        tvToolbarSubtitle.text = getString(R.string.progress, questionNumber, numberOfQuestions)
        if (questionViewModel.title.isNullOrBlank()) {
            tvTitle.visibility = View.GONE
        } else {
            tvTitle.visibility = View.VISIBLE
            tvTitle.text = questionViewModel.title
        }
        tvDescription.text = questionViewModel.description
        adapter.setItems(questionViewModel.items)
        rv.isLayoutFrozen = questionViewModel.items.all { it is FeedbackItemViewModel.FeedbackTextItemViewModel }
        if (shouldDisableNextButton) {
            disableNextButton()
        } else {
            enableNextButton()
        }
        showKeyboardIfTextItem(questionViewModel.items)
    }

    override fun showFinishButton() {
        bNext.text = getString(R.string.feedback_finish)
    }

    override fun finishView() {
        finish()
    }

    override fun changeBackButtonIcon() {
        ibClose.setImageResource(R.drawable.ic_back_feedback)
    }

    override fun changeCloseButtonIcon() {
        ibClose.setImageResource(R.drawable.ic_close_feedback)
    }

    private fun showKeyboardIfTextItem(items: List<FeedbackItemViewModel>) {
        if (items.all { it is FeedbackItemViewModel.FeedbackTextItemViewModel }) {
            rv.requestFocus()
            rv.showKeyboard()
        } else {
            rv.hideKeyboard()
        }
    }

    private fun setupCloseButton() {
        ibClose.setOnClickListener { presenter.onCloseClicked() }
    }

    private fun setupNextButton() {
        bNext.setOnClickListener { presenter.onNextClicked() }
    }

    private fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is FeedbackItemViewModel.FeedbackTextItemViewModel -> R.layout.view_feedback_text_item
                is FeedbackItemViewModel.FeedbackOptionItemViewModel -> R.layout.view_feedback_option_item
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                R.layout.view_feedback_text_item -> FeedbackTextItemViewHolder(view, onTextChanged())
                R.layout.view_feedback_option_item -> FeedbackOptionItemViewHolder(view, onOptionSelected())
                else -> throw IllegalArgumentException()
            }
        })
        rv.apply {
            adapter = this@FeedbackActivity.adapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(ListDividerItemDecoration(context))
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    private fun onTextChanged() = { viewModel: FeedbackItemViewModel.FeedbackTextItemViewModel ->
        presenter.onTextChanged(viewModel)
    }

    private fun onOptionSelected() = { viewModel: FeedbackItemViewModel.FeedbackOptionItemViewModel ->
        presenter.onOptionSelected(viewModel)
    }

    override fun updateItem(viewModel: FeedbackItemViewModel) {
        adapter.update(viewModel)
    }

    override fun enableNextButton() {
        bNext.isEnabled = true
        bNext.setTextColor(ContextCompat.getColor(this, R.color._f5a623))
    }

    override fun disableNextButton() {
        bNext.isEnabled = false
        bNext.setTextColor(ContextCompat.getColor(this, R.color.darkjunglegreen_28_40))
    }

    override fun showMessage(message: Int, onDismissListener: (() -> Unit)) {
        ViewUtils.showDialog(this, getString(message), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun close() {
        finish()
    }

    override fun showLoading() {
        loading = loading ?: ViewUtils.createLoadingOverlay(this)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
