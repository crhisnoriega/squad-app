package co.socialsquad.squad.presentation.feature.ranking.details.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import kotlinx.android.synthetic.main.item_ranking_position.view.*

class ItemBenefitPositionRankingViewHolder(
        itemView: View, var onClickAction: ((item: ItemBenefitPositionRankingViewModel) -> Unit)) : ViewHolder<ItemBenefitPositionRankingViewModel>(itemView) {
    override fun bind(viewModel: ItemBenefitPositionRankingViewModel) {
        itemView.mainLayout.setOnClickListener {
            onClickAction.invoke(viewModel)
        }
    }

    override fun recycle() {

    }

}