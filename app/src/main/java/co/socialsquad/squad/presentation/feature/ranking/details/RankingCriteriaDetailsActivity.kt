package co.socialsquad.squad.presentation.feature.ranking.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.lifecycleScope
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.presentation.feature.base.Status
import co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel.RankingListViewModel
import co.socialsquad.squad.presentation.feature.ranking.model.CriteriaData
import co.socialsquad.squad.presentation.util.extra
import co.socialsquad.squad.presentation.util.isVisible
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_ranking_details_criteria.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class RankingCriteriaDetailsActivity : BaseActivity() {

    companion object {
        const val CRITERIA_ID = "criteria_id"
        const val AREA_ID = "AREA_ID"
        const val IS_IN_RANK = "is_in_rank"

        fun start(
            context: Context,
            criteriaId: String? = null,
            isInRank: Boolean,
            areaId: String? = null
        ) {
            context.startActivity(
                Intent(
                    context,
                    RankingCriteriaDetailsActivity::class.java
                ).apply {
                    putExtra(CRITERIA_ID, criteriaId)
                    putExtra(IS_IN_RANK, isInRank)
                    putExtra(AREA_ID, areaId)
                })
        }
    }

    private val viewModel by viewModel<RankingListViewModel>()

    override val modules: List<Module> = listOf()
    override val contentView: Int = R.layout.activity_ranking_details_criteria

    private val criteriaId: String by extra(CRITERIA_ID)
    private val areaId: String by extra(AREA_ID)
    private val isInRank: Boolean by extra(IS_IN_RANK)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvTitle.text = "Critério"

        setupToolbar()
        configureObservers()

        viewModel.fetchCriteriaDetails(intent.extras?.getString(CRITERIA_ID), intent.extras?.getString(AREA_ID))
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.title = ""
    }

    private fun configureObservers() {
        lifecycleScope.launch {
            val value = viewModel.criteriaDetails
            value.collect {

                when (it.status) {
                    Status.SUCCESS -> {
                        populateScreen(it.data!!)
                    }
                }

            }
        }
    }

    private fun populateScreen(criteria: CriteriaData) {
        app_bar.isVisible = true
        nestedScroll.isVisible = true
        shimmerLayout.isVisible = false

        levelName.text = criteria.title
        levelDescription.text = criteria.description

        description.text = criteria.points

        isInRank?.let {
            if (it.not()) {
                description.visibility = View.GONE
            }
        }

        Glide.with(this).load(criteria.icon).into(imgCover)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}