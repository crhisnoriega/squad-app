package co.socialsquad.squad.presentation.feature.profile.qualification

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.PROFILE_QUALIFICATION_HEADER_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.PrizeViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationHeaderViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationHeaderViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QUALIFICATION_BONUS_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QUALIFICATION_PRIZES_LIST_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QUALIFICATION_RECOMMENDATION_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QUALIFICATION_REQUIREMENT_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QUALIFICATION_SUCCESS_STORIES_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationBonusViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationBonusViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationPrizesListViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationPrizesListViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationRecommendationViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationRequirementViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.QualificationSuccessStoriesViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.RecommendationViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.RequirementViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.SuccessStoriesViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.PROFILE_QUALIFICATION_HEADER_LOADING_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.PROFILE_QUALIFICATION_LIST_ITEM_LOADING_VIEW_HOLDER_ID
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.ProfileQualificationHeaderLoadingViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.ProfileQualificationHeaderLoadingViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.QualificationLoadingItemViewHolder
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.loading.QualificationLoadingItemViewModel
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LOADING_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import kotlinx.android.synthetic.main.activity_qualification.*
import javax.inject.Inject

class ProfileQualificationActivity : BaseDaggerActivity(), ProfileQualificationView {

    companion object {
        const val EXTRA_QUALIFICATION_PK = "EXTRA_QUALIFICATION_PK"
    }

    private lateinit var profileQualificationAdapter: RecyclerViewAdapter

    @Inject
    lateinit var profileQualificationPresenter: ProfileQualificationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualification)
        setupToolbar(R.string.qualification_title)
        setupQualificationDetailRecycler()
        val qualificationPk = intent?.extras?.getInt(EXTRA_QUALIFICATION_PK)
        profileQualificationPresenter.onCreate(qualificationPk)
    }

    fun setupToolbar(titleResId: Int) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(titleResId)
    }

    private fun setupQualificationDetailRecycler() {
        profileQualificationAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel): Int = when (viewModel) {
                is ProfileQualificationHeaderViewModel -> PROFILE_QUALIFICATION_HEADER_VIEW_HOLDER_ID
                is ProfileQualificationHeaderLoadingViewModel -> PROFILE_QUALIFICATION_HEADER_LOADING_VIEW_HOLDER_ID
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is RequirementViewModel -> QUALIFICATION_REQUIREMENT_VIEW_MODEL_ID
                is RecommendationViewModel -> QUALIFICATION_RECOMMENDATION_VIEW_MODEL_ID
                is QualificationLoadingItemViewModel -> PROFILE_QUALIFICATION_LIST_ITEM_LOADING_VIEW_HOLDER_ID
                is QualificationPrizesListViewModel -> QUALIFICATION_PRIZES_LIST_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> LOADING_VIEW_MODEL_ID
                is QualificationBonusViewModel -> QUALIFICATION_BONUS_VIEW_HOLDER_ID
                is SuccessStoriesViewModel -> QUALIFICATION_SUCCESS_STORIES_VIEW_HOLDER_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PROFILE_QUALIFICATION_HEADER_VIEW_HOLDER_ID -> ProfileQualificationHeaderViewHolder(view, windowManager, onUpdateCurrentQualificationListener)
                PROFILE_QUALIFICATION_HEADER_LOADING_VIEW_HOLDER_ID -> ProfileQualificationHeaderLoadingViewHolder(view)
                HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                QUALIFICATION_REQUIREMENT_VIEW_MODEL_ID -> QualificationRequirementViewHolder(view)
                QUALIFICATION_RECOMMENDATION_VIEW_MODEL_ID -> QualificationRecommendationViewHolder(view)
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                QUALIFICATION_PRIZES_LIST_VIEW_MODEL_ID -> QualificationPrizesListViewHolder(view)
                LOADING_VIEW_MODEL_ID -> GenericLoadingViewHolder<Nothing>(view)
                QUALIFICATION_BONUS_VIEW_HOLDER_ID -> QualificationBonusViewHolder(view)
                QUALIFICATION_SUCCESS_STORIES_VIEW_HOLDER_ID -> QualificationSuccessStoriesViewHolder(view, onVideoClicked)
                PROFILE_QUALIFICATION_LIST_ITEM_LOADING_VIEW_HOLDER_ID -> QualificationLoadingItemViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvQualificationDetail.apply {
            adapter = profileQualificationAdapter
            layoutManager = LinearLayoutManager(this@ProfileQualificationActivity)
            addItemDecoration(ListDividerItemDecoration(this@ProfileQualificationActivity))
            setHasFixedSize(false)
            setItemViewCacheSize(0)
        }
    }

    override fun setQualifications(qualifications: ProfileQualificationHeaderViewModel) {
        profileQualificationAdapter.setItems(listOf(qualifications))
        qualifications.qualifications.find { it.actual }?.let { updateQualificationDetails(it) }
    }

    private fun updateQualificationDetails(qualification: ProfileQualificationViewModel) {
        profileQualificationPresenter.updateDetails(qualification.name)
        profileQualificationAdapter.removeAllFromPosition(1)

        with(qualification) {
            when {
                target -> {
                    showRequirements(requirements)
                    showRecommendations(recomendations)
                    bonus?.let { showBonus(it) }
                }
                progress == 0 -> {
                    bonus?.let { showBonus(it) }
                }
                else -> {
                    showRequirements(requirements)
                }
            }
            showPrizes(prizes)
            showSuccessStories(stories)
        }
    }

    private fun showBonus(bonus: String) {
        with(profileQualificationAdapter) {
            addItems(
                listOf(
                    HeaderViewModel(getString(R.string.qualification_bonus)),
                    QualificationBonusViewModel(bonus)
                )
            )
        }
    }

    private fun showPrizes(prizes: List<PrizeViewModel>) {
        if (prizes.isNotEmpty()) {
            val stringId = if (prizes.size > 1) R.string.qualification_prizes else R.string.qualification_prize
            profileQualificationAdapter.addItems(
                listOf(
                    HeaderViewModel(getString(stringId)),
                    QualificationPrizesListViewModel(prizes),
                    DividerViewModel
                )
            )
        }
    }

    private fun showSuccessStories(stories: List<SuccessStoriesViewModel>) {
        if (stories.isNotEmpty()) {
            val stringId = if (stories.size > 1) R.string.qualification_success_stories else R.string.qualification_success_story
            profileQualificationAdapter.addItems(listOf(HeaderViewModel(getString(stringId))))
            profileQualificationAdapter.addItems(stories)
            profileQualificationAdapter.addItems(listOf(DividerViewModel))
        }
    }

    private fun showRequirements(requirements: List<RequirementViewModel>) {
        if (requirements.isNotEmpty()) {
            val stringId = if (requirements.size > 1) R.string.qualification_requirements else R.string.qualification_requirement
            profileQualificationAdapter.addItems(
                listOf(HeaderViewModel(getString(stringId)))
            )
            profileQualificationAdapter.addItems(requirements)
            profileQualificationAdapter.addItems(listOf(DividerViewModel))
        }
    }

    private fun showRecommendations(recommendations: List<RecommendationViewModel>) {
        if (recommendations.isNotEmpty()) {
            val stringId = if (recommendations.size > 1) R.string.qualification_recommendations else R.string.qualification_recommendation
            profileQualificationAdapter.addItems(listOf(HeaderViewModel(getString(stringId))))
            profileQualificationAdapter.addItems(recommendations)
            profileQualificationAdapter.addItems(listOf(DividerViewModel))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun startLoading() {
        profileQualificationAdapter.setItems(
            listOf(
                ProfileQualificationHeaderLoadingViewModel(),
                HeaderViewModel(getString(R.string.qualification_requirements)),
                QualificationLoadingItemViewModel(),
                QualificationLoadingItemViewModel(),
                QualificationLoadingItemViewModel()
            )
        )
    }

    val onUpdateCurrentQualificationListener = object : ViewHolder.Listener<ProfileQualificationViewModel> {
        override fun onClick(viewModel: ProfileQualificationViewModel) {
            updateQualificationDetails(viewModel)
        }
    }

    val onVideoClicked = object : ViewHolder.Listener<SuccessStoriesViewModel> {
        override fun onClick(viewModel: SuccessStoriesViewModel) {
            profileQualificationPresenter.onVideoClicked(viewModel.mediaPk)
        }
    }
}
