package co.socialsquad.squad.presentation.feature.trip.list

import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.TripRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.data.utils.TagWorker.Companion.TAG_SCREEN_VIEW_TRIP_LIST
import co.socialsquad.squad.presentation.feature.trip.TripViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TripListPresenter @Inject constructor(
    private val view: TripListView,
    private val tagWorker: TagWorker,
    private val tripRepository: TripRepository,
    loginRepository: LoginRepository
) {

    private val compositeDisposable = CompositeDisposable()
    private var page = 1
    private var isLoading = true
    private var isComplete = false
    private val companyColor = loginRepository.userCompany?.company?.primaryColor

    fun onViewCreated() {
        tagWorker.tagEvent(TAG_SCREEN_VIEW_TRIP_LIST)
        view.setupList(companyColor)
        subscribe()
    }

    fun onRefresh() {
        isComplete = false
        page = 1
        compositeDisposable.clear()
        subscribe()
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe()
    }

    private fun subscribe() {
        compositeDisposable.add(
            tripRepository.getTrips()
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading()
                }.doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }
                .doOnNext { if (it.next == null) isComplete = true }
                .map { it.results.orEmpty().map { TripViewModel(it, companyColor) } }
                .subscribe(
                    {
                        it.takeIf { it.isNullOrEmpty() && page == 1 }?.apply {
                            view.showEmptyState()
                        } ?: kotlin.run {
                            val shouldClearList = page == 1
                            view.addItems(it, shouldClearList)
                            page++
                        }
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    fun onDestroyView() {
        compositeDisposable.dispose()
    }
}
