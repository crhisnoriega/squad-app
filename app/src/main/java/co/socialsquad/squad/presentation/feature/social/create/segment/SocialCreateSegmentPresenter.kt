package co.socialsquad.squad.presentation.feature.social.create.segment

import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SegmentRepository
import co.socialsquad.squad.data.utils.TagWorker
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SocialCreateSegmentPresenter @Inject constructor(
    private val view: SocialCreateSegmentView,
    private val tagWorker: TagWorker,
    private val segmentRepository: SegmentRepository,
    loginRepository: LoginRepository
) {
    private val compositeDisposable = CompositeDisposable()
    private val networkCount = loginRepository.userCompany?.networkCount

    fun onCreate() {
        getTypes()
    }

    private fun getTypes() {
        compositeDisposable.add(
            segmentRepository.getTypes()
                .doOnSubscribe { view.showLoading() }
                .doOnTerminate { view.hideLoading() }
                .subscribe(
                    { view.showOptions(it.results.orEmpty()) },
                    { it.printStackTrace() }
                )
        )
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onDownlineClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_POST_SEGMENT_DOWNLINE)
        view.finishAfterDownlineClicked(networkCount ?: 0)
    }

    fun onPublicClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_POST_SEGMENT_PUBLIC)
    }

    fun onMetricsClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_POST_SEGMENT_METRICS)
    }

    fun onMembersClicked() {
        tagWorker.tagEvent(TagWorker.TAG_TAP_POST_SEGMENT_MEMBERS)
    }
}
