package co.socialsquad.squad.presentation.feature.store.viewholder

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreProductListBannerViewModel
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_store_product_list_banner.view.*

class StoreProductListBannerViewHolder(itemView: View) : ViewHolder<StoreProductListBannerViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreProductListBannerViewModel) {
        glide.load(viewModel.imageUrl)
            .crossFade()
            .into(itemView.ivImage)
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }
}
