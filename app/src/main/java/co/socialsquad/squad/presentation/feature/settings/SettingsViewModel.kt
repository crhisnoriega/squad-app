package co.socialsquad.squad.presentation.feature.settings

import co.socialsquad.squad.presentation.feature.store.ItemViewModel

class SettingsViewModel(title: String, val onClick: () -> Unit) : ItemViewModel(title)
