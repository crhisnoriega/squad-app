package co.socialsquad.squad.presentation.feature.profile.qualification

import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.ProfileQualificationHeaderViewModel

interface ProfileQualificationView {
    fun setQualifications(qualifications: ProfileQualificationHeaderViewModel)
    fun startLoading()
}
