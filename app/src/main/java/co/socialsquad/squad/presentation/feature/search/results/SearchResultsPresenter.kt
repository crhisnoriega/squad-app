package co.socialsquad.squad.presentation.feature.search.results

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.Item
import co.socialsquad.squad.data.entity.SearchRequest
import co.socialsquad.squad.data.entity.SearchRequest.Type.*
import co.socialsquad.squad.data.entity.SearchV2
import co.socialsquad.squad.data.onDefaultSchedulers
import co.socialsquad.squad.data.repository.DocumentRepository
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SearchRepository
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import co.socialsquad.squad.presentation.feature.search.SearchItem
import co.socialsquad.squad.presentation.feature.search.SearchSeeAllViewModel
import co.socialsquad.squad.presentation.feature.search.results.SearchResultsFragment.Companion.EXTRA_TYPE
import co.socialsquad.squad.presentation.feature.store.AudiosSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.DocumentsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.EventsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.ImmobilesSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.LivesSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.ProductSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.StoresSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.TripsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.VideosSearchItemViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchResultsPresenter @Inject constructor(
    private val view: SearchResultsView,
    private val repository: SearchRepository,
    private val documentRepository: DocumentRepository,
    private val fusedLocationProviderClient: FusedLocationProviderClient,
    private val loginRepository: LoginRepository
) {
    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null
    private var page = 1
    private var isLoading = false
    private var isComplete = true
    private var type = All
    private var latitude: Double? = null
    private var longitude: Double? = null
    private var query = ""
    private val company = loginRepository.userCompany?.company
    private val subdomain = loginRepository.subdomain

    fun onViewCreated(arguments: Bundle?) {
        type = arguments?.getSerializable(EXTRA_TYPE) as SearchRequest.Type
        if (listOf(All, Stores).contains(type)) getLastLocation()

        loginRepository.userCompany?.company?.secondaryColor?.apply {
            view.setupList(this)
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun onRefresh() {
        page = 1
        isComplete = false
        compositeDisposable.clear()
        view.disableRefreshing()
        view.clearList()
        subscribe(true)
    }

    fun onQueryChanged(text: String) {
        query = text
        page = 1
        isComplete = false
        compositeDisposable.clear()
        disposable?.apply { if (!isDisposed) dispose() }

        view.disableRefreshing()

        disposable = Completable.complete().delay(600, TimeUnit.MILLISECONDS)
            .onDefaultSchedulers()
            .subscribe(
                {
                    view.clearList()
                    subscribe(true)
                },
                {}
            )
    }

    fun onQueryCleared() {
        compositeDisposable.clear()
        isComplete = true
        disposable?.apply { if (!isDisposed) dispose() }
        view.apply {
            view.showEmptyState()
            disableRefreshing()
        }
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (!isLoading && !isComplete) subscribe(false)
    }

    private fun subscribe(clear: Boolean) {
        compositeDisposable.add(
            repository.search(query)
                .doOnSubscribe {
                    isLoading = true
                    view.startLoading(query)
                }
                .doOnTerminate {
                    view.stopLoading()
                    isLoading = false
                }
                .map { search ->
                    when (type) {
                        Products -> parseData(search, 1) { ProductSearchItemViewModel(it) }
                        Immobiles -> parseData(search, 2) { ImmobilesSearchItemViewModel(it) }
                        Lives -> parseData(search, 11) { LivesSearchItemViewModel(it) }
                        Events -> parseData(search, 9) { EventsSearchItemViewModel(it) }
                        Videos -> parseData(search, 15) { VideosSearchItemViewModel(it) }
                        Audios -> parseData(search, 13) { AudiosSearchItemViewModel(it) }
                        Stores -> parseData(search, 10) { StoresSearchItemViewModel(it) }
                        Documents -> parseData(search, 7) { DocumentsSearchItemViewModel(it) }
                        Trips -> parseData(search, 14) { TripsSearchItemViewModel(it) }
                        All -> with(search) {
                            val list = mutableListOf<ViewModel>()

                            val products = search.data.orEmpty().find { it.id == 1 }
                            if (!products?.items.isNullOrEmpty() && company?.exploreProducts == true) {
                                list.add(HeaderViewModel(titleResId = R.string.search_item_products, hasTopMargin = false))
                                products?.items?.subList(0, minOf(3, products.items.size))?.map { ProductSearchItemViewModel(it) }?.let { list.addAll(it) }
                                if (products?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Products))
                            }
                            val immobiles = search.data.orEmpty().find { it.id == 2 }
                            if (!immobiles?.items.isNullOrEmpty() && company?.exploreImmobile == true) {
                                val items = immobiles?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.immobiles, hasTopMargin = false))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { ImmobilesSearchItemViewModel(it) })
                                if (immobiles?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Immobiles))
                            }

                            val events = search.data.orEmpty().find { it.id == 2 }
                            if (!events?.items.isNullOrEmpty() && company?.exploreEvents == true) {
                                val items = events?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_events))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { EventsSearchItemViewModel(it) })
                                if (events?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Events))
                            }

                            val lives = search.data.orEmpty().find { it.id == 11 }
                            if (!lives?.items.isNullOrEmpty() && company?.exploreLives == true) {
                                val items = lives?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_lives))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { LivesSearchItemViewModel(it) })
                                if (lives?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Lives))
                            }

                            val audios = search.data.orEmpty().find { it.id == 15 }
                            if (!audios?.items.isNullOrEmpty() && company?.exploreAudios == true) {
                                val items = audios?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_audios))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { AudiosSearchItemViewModel(it) })
                                if (audios?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Audios))
                            }

                            val videos = search.data.orEmpty().find { it.id == 13 }
                            if (!videos?.items.isNullOrEmpty() && company?.exploreVideos == true) {
                                val items = videos?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_videos))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { VideosSearchItemViewModel(it) })
                                if (videos?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Videos))
                            }

                            val stores = search.data.orEmpty().find { it.id == 11 }
                            if (!stores?.items.isNullOrEmpty() && company?.exploreStores == true) {
                                val items = stores?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_stores))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { StoresSearchItemViewModel(it) })
                                if (stores?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Stores))
                            }

                            val documents = search.data.orEmpty().find { it.id == 7 }
                            if (!documents?.items.isNullOrEmpty() && company?.exploreDocuments == true) {
                                val items = documents?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_documents))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { DocumentsSearchItemViewModel(it) })
                                if (documents?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Documents))
                            }

                            val trips = search.data.orEmpty().find { it.id == 14 }
                            if (!trips?.items.isNullOrEmpty() && company?.exploreTrips == true) {
                                val items = trips?.items.orEmpty()
                                list.add(HeaderViewModel(titleResId = R.string.search_item_trips))
                                list.addAll(items.subList(0, minOf(3, items.size)).map { TripsSearchItemViewModel(it) })
                                if (documents?.items?.count() ?: 0 > 3) list.add(SearchSeeAllViewModel(SearchItem.Trips))
                            }

                            list
                        }
                    }
                }
                .subscribe(
                    { list: List<ViewModel> ->
                        if (list.isNotEmpty()) {
                            if (clear) view.setItems(list)
                            else view.addItems(list)
                            page++
                        } else {
                            isComplete = true
                            if (page == 1) view.showNoResults(query)
                        }
                        view.enableRefreshing()
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }

    private fun <T> parseData(search: SearchV2, id: Int, data: (Item) -> T): List<T> =
        search.data
            .orEmpty()
            .find { it.id == id }
            ?.items.orEmpty().map { item -> data(item) }

    fun onDocumentClicked(viewModel: DocumentViewModel) {
        compositeDisposable.add(
            documentRepository.markDocumentViewed(viewModel.pk)
                .subscribe({}, { it.printStackTrace() })
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { onLastLocationReceived(it) }
    }

    private fun onLastLocationReceived(location: Location?) {
        location?.let {
            latitude = it.latitude
            longitude = it.longitude
        } ?: getFineLocation()
    }

    private fun getFineLocation() {
        view.createLocationRequest()
    }

    fun onRequestPermissionsResult(grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun onLocationRequestCreated(locationRequest: LocationRequest, pendingIntent: PendingIntent?) {
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, pendingIntent).addOnSuccessListener {
            getLastLocation()
        }.addOnFailureListener {
            it.printStackTrace()
        }
    }
}
