package co.socialsquad.squad.presentation.feature.store.product.details

import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.ListDividerItemDecoration
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.VideoView
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.store.PRODUCT_DETAIL_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.ProductDetailViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductDetailViewHolder
import co.socialsquad.squad.presentation.feature.video.VideoManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_product_detail_group.*
import javax.inject.Inject

class ProductDetailGroupActivity : BaseDaggerActivity(), ProductDetailGroupView {

    @Inject
    lateinit var productDetailGroupPresenter: ProductDetailGroupPresenter

    private lateinit var rvDetails: RecyclerView
    private lateinit var ivImage: ImageView
    private lateinit var vvVideo: VideoView

    private val glide by lazy { Glide.with(this) }
    private var videoUri: Uri? = null

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        VideoManager.hide()
        super.onPause()
    }

    override fun onDestroy() {
        VideoManager.release()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        videoUri?.let { vvVideo.prepare(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail_group)

        rvDetails = product_detail_group_rv_details
        ivImage = product_detail_group_iv_cover
        vvVideo = product_detail_group_vv_video

        productDetailGroupPresenter.onCreate(intent)
    }

    override fun setupToolbar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTitle(title)
    }

    override fun setupList(items: List<ViewModel>, color: String?) {
        val viewHolderFactory = object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ProductDetailViewModel -> PRODUCT_DETAIL_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                PRODUCT_DETAIL_VIEW_MODEL_ID -> ProductDetailViewHolder(view, color)
                else -> throw IllegalArgumentException()
            }
        }

        with(rvDetails) {
            adapter = RecyclerViewAdapter(viewHolderFactory).apply { setItems(items) }
            addItemDecoration(ListDividerItemDecoration(context))
            setHasFixedSize(true)
            isFocusable = false
        }
    }

    override fun showImage(imageUrl: String) {
        ivImage.apply {
            visibility = View.VISIBLE
            glide.load(imageUrl).into(this)
        }
    }

    override fun showVideo(thumbnailUrl: String, url: String) {
        vvVideo.apply {
            visibility = View.VISIBLE

            loadThumbnail(thumbnailUrl)

            videoUri = Uri.parse(url)
            prepare(videoUri!!)
        }
    }
}
