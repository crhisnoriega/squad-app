package co.socialsquad.squad.presentation.feature.terms

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.data.entity.TermsResponse
import co.socialsquad.squad.presentation.util.ColorUtils

class TermsAdapter(val items: List<TermsResponse.Result.Item>) : RecyclerView.Adapter<TermsAdapter.ViewHolder>() {

    private var textColor: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.terms_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        if (position == 0) {
            holder.title.visibility = View.GONE
        } else {
            holder.title.text = item.title
        }

        holder.description.text = item.description

        textColor?.apply {
            val color = ColorUtils.parse(this)
            holder.title.setTextColor(color)
            holder.description.setTextColor(color)
        }
    }

    fun textColor(textColor: String) {
        this.textColor = textColor
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.object_type_lead)
        val description = itemView.findViewById<TextView>(R.id.status)
    }
}
