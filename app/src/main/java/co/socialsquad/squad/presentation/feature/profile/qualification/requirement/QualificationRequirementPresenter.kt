package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import android.content.Intent
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.profile.qualification.requirement.QualificationRequirementActivity.Companion.EXTRA_REQUIREMENT_VIEW_MODEL
import co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder.RequirementViewModel
import co.socialsquad.squad.presentation.util.FileUtils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class QualificationRequirementPresenter @Inject constructor(
    private val view: QualificationRequirementView,
    private val profileRepository: ProfileRepository,
    private val tagWorker: TagWorker
) {

    private var compositeDisposable = CompositeDisposable()

    fun onCreate(intent: Intent) {
        tagWorker.tagEvent(TagWorker.TAG_SCREEN_VIEW_QUALIFICATION_REQUIREMENT)

        if (intent.hasExtra(EXTRA_REQUIREMENT_VIEW_MODEL)) {
            val requirementViewModel = intent.getSerializableExtra(EXTRA_REQUIREMENT_VIEW_MODEL) as RequirementViewModel
            view.setupToolbar(requirementViewModel.name)
            getQualificationRequirement(requirementViewModel.pk)
        }
    }

    private fun getQualificationRequirement(pk: Int) {
        compositeDisposable.add(
            profileRepository.getQualificationRequirement(pk)
                .doOnSubscribe { view.startLoading() }
                .doOnComplete { view.stopLoading() }
                .map {
                    val list = mutableListOf<ViewModel>()
                    val header = QualificationRequirementHeaderViewModel(
                        it.feed.title ?: "",
                        it.feed.content ?: "",
                        it.feed.medias?.firstOrNull()?.let { if (FileUtils.isVideoMedia(it)) it.url else "" },
                        it.feed.medias?.firstOrNull()?.let { if (FileUtils.isVideoMedia(it)) it.streamings?.firstOrNull()?.thumbnailUrl else "" }
                    )
                    list.add(header)
                    val tips = it.hints.map {
                        QualificationRequirementTipListItemViewModel(
                            it.order + 1,
                            it.title,
                            it.description
                        )
                    }.sortedBy { it.number }
                    list.addAll(tips)
                    list
                }
                .subscribe(
                    {
                        view.setItems(it)
                    },
                    {
                        it.printStackTrace()
                    }
                )
        )
    }
}
