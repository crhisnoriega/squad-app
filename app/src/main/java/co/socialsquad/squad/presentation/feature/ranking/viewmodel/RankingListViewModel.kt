package co.socialsquad.squad.presentation.feature.opportunityv2.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.ranking.model.*
import co.socialsquad.squad.presentation.feature.ranking.repository.RankingRepository
import co.socialsquad.squad.presentation.feature.recognition.model.AgreeRefuseTermsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class RankingListViewModel(
        var rankingRepository: RankingRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    val rankingItem = MutableLiveData<Resource<RankingItem>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val newRankingList = MutableLiveData<Resource<RankListResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val agreeRankTerms = MutableLiveData<Resource<AgreeRefuseTermsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val refuseRankTerms = MutableLiveData<Resource<AgreeRefuseTermsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val positionList = MutableStateFlow<Resource<PositionsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val criteriaList = MutableStateFlow<Resource<CriteriasResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val benefitList = MutableStateFlow<Resource<BenefitsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val rankingList = MutableStateFlow<Resource<BenefitsResponse>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val criteriaDetails = MutableStateFlow<Resource<CriteriaData>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    val benefitDetails = MutableStateFlow<Resource<BenefitData>>(Resource.loading(null))


    @ExperimentalCoroutinesApi
    fun fetchRankingList() {
        viewModelScope.launch {

            rankingRepository.fetchRankings()
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rank", e.message, e)
                        newRankingList.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        newRankingList.value = Resource.success(it)
                    }

        }
    }


    @ExperimentalCoroutinesApi
    fun fetchRankDetail(rankingId: String) {
        viewModelScope.launch {
            Log.i("rank ", "ranking id $rankingId")
            rankingRepository.fetchRankDetails(rankingId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("rank ", "ranking id $rankingId")
                        rankingItem.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        rankingItem.value = Resource.success(it)
                    }

        }
    }

    @ExperimentalCoroutinesApi
    fun agreeRankTerms(rankingId: String) {
        viewModelScope.launch {
            Log.i("rank ", "ranking id $rankingId")
            rankingRepository.agreeRankTerms(rankingId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        agreeRankTerms.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        agreeRankTerms.value = Resource.success(it)
                    }

        }
    }

    @ExperimentalCoroutinesApi
    fun refuseRankTerms(rankingId: String) {
        viewModelScope.launch {
            Log.i("rank ", "ranking id $rankingId")
            rankingRepository.refuseRankTerms(rankingId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        refuseRankTerms.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        refuseRankTerms.value = Resource.success(it)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun fetchBenefits() {
        viewModelScope.launch {

            rankingRepository.fetchBenefits()
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("list", e.message, e)
                        benefitList.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        benefitList.value = Resource.success(it)
                    }

        }
    }

    @ExperimentalCoroutinesApi
    fun fetchCriterias(rankId: String, nextUrl: String) {
        viewModelScope.launch {

            rankingRepository.fetchCriterias(rankId, nextUrl)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("list", e.message, e)
                        criteriaList.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        criteriaList.value = Resource.success(it)
                    }

        }
    }


    var isLoadding  = false
    @ExperimentalCoroutinesApi
    fun fetchPositions(rankingId: String, nextUrl: String) {
        viewModelScope.launch {
            isLoadding = true
            rankingRepository.fetchPositions(rankingId, nextUrl)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("list", e.message, e)
                        positionList.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        isLoadding = false
                        positionList.value = Resource.success(it)
                    }

        }
    }


    @ExperimentalCoroutinesApi
    fun fetchBenefitDetails(benefitId: String, rankingId: String, level:String) {
        viewModelScope.launch {

            rankingRepository.fetchBenefitDetails(benefitId, rankingId, level)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("list", e.message, e)
                        benefitDetails.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        benefitDetails.value = Resource.success(it)
                    }

        }
    }

    @ExperimentalCoroutinesApi
    fun fetchCriteriaDetails(criteriaId: String?, areaId:String?) {
        viewModelScope.launch {

            rankingRepository.fetchCriteriaDetails(criteriaId, areaId)
                    .flowOn(Dispatchers.IO)
                    .catch { e ->
                        Log.i("list", e.message, e)
                        criteriaDetails.value = Resource.error(e.toString(), null)
                    }
                    .collect {
                        criteriaDetails.value = Resource.success(it)
                    }

        }
    }


}
