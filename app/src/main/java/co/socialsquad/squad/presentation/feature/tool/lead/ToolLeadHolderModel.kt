package co.socialsquad.squad.presentation.feature.tool.lead

import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewModel

const val TOOL_LEAD_VIEW_HOLDER_MODEL_ID = R.layout.item_tool_lead

class ToolLeadHolderModel(
    val title: String,
    val timestamp: String,
    val imageUrl: String,
    var selected: Boolean = false
) : ViewModel
