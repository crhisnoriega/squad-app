package co.socialsquad.squad.presentation.feature.store.product.details

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ProductDetailGroupModule {
    @Binds
    abstract fun view(productDetailGroupActivity: ProductDetailGroupActivity): ProductDetailGroupView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
