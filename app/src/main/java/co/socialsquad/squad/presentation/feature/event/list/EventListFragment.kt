package co.socialsquad.squad.presentation.feature.event.list

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.event.EventViewModel
import co.socialsquad.squad.presentation.feature.event.mapper.EventMapper
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_event_list.*
import javax.inject.Inject

class EventListFragment : Fragment(), EventListView {

    @Inject
    lateinit var presenter: EventListPresenter

    private lateinit var adapter: RecyclerViewAdapter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_event_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupSwipeRefresh()
        presenter.onViewCreated()
    }

    override fun hideShimmer() {
        adapter.setItems(emptyList())
    }

    override fun showShimmer() {
        adapter.setItems(EventMapper.mapLoadingContent())
    }

    private fun setupSwipeRefresh() {
        srlLoading.isEnabled = false
        srlLoading.setOnRefreshListener {
            srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color._eeeff4))
            empty_container.visibility = View.GONE
            presenter.onRefresh()
        }
    }

    override fun setupList() {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> EVENT_LIST_ITEM_LOADING
                is EventViewModel -> EVENT_LIST_ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                EVENT_LIST_ITEM_LOADING -> GenericLoadingViewHolder<Nothing>(view)
                EVENT_LIST_ITEM_VIEW_MODEL_ID -> EventListItemViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        rvEvents.apply {
            adapter = this@EventListFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(
                EndlessScrollListener(
                    4,
                    layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    override fun startLoading() {
        adapter.startLoading()
        if (!srlLoading.isRefreshing) srlLoading.isEnabled = false
    }

    override fun stopLoading() {
        adapter.stopLoading()
        srlLoading.apply {
            isEnabled = true
            isRefreshing = false
        }
    }

    override fun addItems(events: List<EventViewModel>, shouldClearList: Boolean) {
        with(adapter) {
            if (shouldClearList) {
                setItems(events)
            } else {
                addItems(events)
            }
        }
    }

    override fun showMessage(textResId: Int, onDismissListener: () -> Unit?) {
        ViewUtils.showDialog(activity, getString(textResId), DialogInterface.OnDismissListener { onDismissListener() })
    }

    override fun onDestroyView() {
        presenter.onDestroyView()
        super.onDestroyView()
    }

    override fun showEmptyState() {
        srlLoading.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
        empty_container.visibility = View.VISIBLE
        space.visibility = View.GONE
    }
}
