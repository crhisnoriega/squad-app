package co.socialsquad.squad.presentation.feature.kickoff.terms

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.navigation.NavigationActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class KickoffTermsFragment : Fragment(), KickoffTermsView {

    @Inject
    lateinit var kickoffTermsPresenter: KickoffTermsPresenter

    private var loading: AlertDialog? = null
    private var listener: Listener? = null

    interface Listener {
        fun onErrorMessageDismissed()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        listener = activity as? Listener
        super.onAttach(context)
    }

    override fun onDestroyView() {
        kickoffTermsPresenter.onDestroyView()
        super.onDestroyView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_kickoff_terms, container, false)
        view.findViewById<View>(R.id.kickoff_terms_b_agree).setOnClickListener { onAgreeClicked() }
        return view
    }

    private fun onAgreeClicked() {
        arguments?.let {
            kickoffTermsPresenter.onAgreeClicked(it)
        }
    }

    override fun showNavigation() {
        val intent = Intent(activity, NavigationActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        ActivityCompat.finishAfterTransition(requireActivity())
    }

    override fun showErrorMessageFailedToRegisterUser() {
        ViewUtils.showDialog(activity, getString(R.string.kickoff_terms_error_failed_to_register_user), DialogInterface.OnDismissListener { listener?.onErrorMessageDismissed() })
    }

    override fun showLoading() {
        if (loading == null) loading = ViewUtils.createLoadingOverlay(activity)
        if (isVisible) loading!!.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }
}
