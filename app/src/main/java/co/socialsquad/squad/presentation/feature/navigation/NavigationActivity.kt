package co.socialsquad.squad.presentation.feature.navigation

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.view.Window
import androidx.fragment.app.Fragment
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.data.entity.Share
import co.socialsquad.squad.data.service.CHAT_SERVICE_ACTION_START
import co.socialsquad.squad.data.service.ChatService
import co.socialsquad.squad.presentation.feature.audio.detail.AudioDetailActivity
import co.socialsquad.squad.presentation.feature.company.SystemSelectActivity
import co.socialsquad.squad.presentation.feature.explorev2.SectionsFragment
import co.socialsquad.squad.presentation.feature.feedback.FeedbackDialog
import co.socialsquad.squad.presentation.feature.feedback.FeedbackViewModel
import co.socialsquad.squad.presentation.feature.hub.HubFragment
import co.socialsquad.squad.presentation.feature.immobiles.details.ImmobilesDetailsActivity
import co.socialsquad.squad.presentation.feature.live.creator.LiveCreatorActivity
import co.socialsquad.squad.presentation.feature.live.scheduled.ScheduledLiveDetailsActivity
import co.socialsquad.squad.presentation.feature.live.viewer.LiveViewerActivity
import co.socialsquad.squad.presentation.feature.notification.NotificationFragment
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.social.details.PostDetailsActivity
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.InAppUpdateListener
import co.socialsquad.squad.presentation.util.InAppUpdaterUtils
import co.socialsquad.squad.presentation.util.getShapeWithColor
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.model.InstallStatus
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.linearlayout_bottom_bar.*
import kotlinx.android.synthetic.main.view_notification_badge.*
import javax.inject.Inject

class NavigationActivity :
    BaseDaggerActivity(),
    HasSupportFragmentInjector,
    NavigationView,
    BottomNavigationView.OnNavigationItemSelectedListener,
    NotificationFragment.NotificationFragmentListener {

    private lateinit var utils: InAppUpdaterUtils

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var navigationPresenter: NavigationPresenter

    private val navigationItems = NavigationItem.values()
    private var currentFragment: Fragment? = null

    private var hubIconInactive: Drawable? = null
    private var hubIconActive: Drawable? = null

    companion object {
        const val extraVoila = "extra_voila"
        var loadFirstTime = true
        val fragments = mutableMapOf<NavigationItem, Fragment>()
    }

    override fun supportFragmentInjector() = fragmentInjector


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        navigationPresenter.onCreate()
        socialTabSelected(null)

        utils = InAppUpdaterUtils(this)
        configureInAppUpdate()
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        return true
    }

    @Suppress
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    private fun configureInAppUpdate() {
        utils.listener = object : InAppUpdateListener {

            private fun showMessage(message: String) {

            }

            override fun onUpdateState(state: InstallState) {

                var message: String = ""
                when (state.installStatus()) {

                    InstallStatus.DOWNLOADING -> {
                        message = "Downloading..."
                        showMessage(message)
                    }
                    InstallStatus.PENDING -> {
                        message = "Pending"
                        showMessage(message)
                    }

                    InstallStatus.DOWNLOADED -> {
                        utils.doComplete()
                        return
                    }

                    InstallStatus.INSTALLING -> {
                        message = "Installing"
                        showMessage(message)
                    }

                    InstallStatus.FAILED -> {

                    }

                    InstallStatus.INSTALLED, InstallStatus.UNKNOWN -> {

                    }
                }
            }

            override fun onError(exception: Exception) {

            }

            override fun onAppUpdateInfo(appUpdateInfo: AppUpdateInfo) {
                when (appUpdateInfo.installStatus()) {
                    InstallStatus.DOWNLOADED -> {
                        utils.doComplete()
                    }
                }
                utils.startDownloadUpdate(appUpdateInfo)
            }

            override fun nothingToUpdate(updateAvailability: Int) {
                // if necessary implement some specific behaviour when not update available
            }
        }

        utils.checkAvailableUpdate()
    }


    private fun setFragmentAction(navigationItem: NavigationItem): Boolean {
        supportFragmentManager.executePendingTransactions()

        var isNewFragment = false
        var fragment = supportFragmentManager.findFragmentByTag(navigationItem.name)

        if (fragment == null) {
            isNewFragment = true
            fragment = navigationItem.getFragment()
        }

        if (fragment == currentFragment) {
            doubleTouchAction(fragment)
            return true
        }

        supportFragmentManager.beginTransaction().apply {
            if (!isNewFragment) {

                supportFragmentManager.fragments.forEach {
                    hide(it)
                }
                show(fragment)

                var toolbarColor = 0

                toolbarColor = when (fragment) {
                    is HubFragment -> {
                        //window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        window?.decorView?.systemUiVisibility = 0
                        ColorUtils.parse(ColorUtils.getCompanyColor(this@NavigationActivity)!!)
                    }
                    else -> {
                        window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        Color.WHITE
                    }
                }


                // replace(R.id.frame, fragment, navigationItem.name)
                (navigationItem.getFragment() as? NotificationFragment)?.let {
                    reloadNotificationFragment(
                        it
                    )
                }

                (navigationItem.getFragment() as? SectionsFragment)?.let {

                }

                (fragment as? ProfileFragment)?.let {
                    it.doRefresh()
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val window: Window = window
                    window.statusBarColor = toolbarColor
                }


            } else {
                add(R.id.frame, fragment, navigationItem.name)
            }
            setPrimaryNavigationFragment(fragment)
            setReorderingAllowed(true)
            commit()
        }



        currentFragment = fragment
        invalidateOptionsMenu()
        navigationItem.let { navigationPresenter.onFragmentSet(navigationItem) }
        return true
    }

    private fun reloadNotificationFragment(notificationFragment: NotificationFragment) {
        notificationFragment.let { if (onUnreadNotificationsCheck()) it.refresh() }
    }

    private fun doubleTouchAction(fragment: Fragment) {
        if (fragment is ReselectedListener) fragment.onReselected()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        try {
            super.onRestoreInstanceState(savedInstanceState)
        } catch (e: Exception) {
            FirebaseCrashlytics.getInstance().log("onRestoreInstanceState error")
            FirebaseCrashlytics.getInstance().recordException(e)
        }
    }

    override fun onRestart() {
        super.onRestart()
        navigationPresenter.onRestart()
    }

    override fun onResume() {
        super.onResume()
        navigationPresenter.onResume()
    }

    private fun onUnreadNotificationsCheck() = navigationPresenter.onUnreadNotificationsCheck()

    override fun hideNotificationBadge() {
        rlNotificationBadge.visibility = View.GONE
    }

    override fun showNotificationBadge(primaryColor: String?) {
        rlNotificationBadge.visibility = View.VISIBLE
        primaryColor?.let {
            val badgeBg = getShapeWithColor(R.drawable.shape_notification_badge, it)
            vNotificationBadge.background = badgeBg
        }
    }

    override fun setupHub(inactiveImageUrl: String?, activeImageUrl: String?) {
        rlHubButtonLayout.visibility = View.VISIBLE
        Glide.with(this)
            .asDrawable()
            .load(inactiveImageUrl)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean = false

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    ivHubButtonImage.setImageDrawable(resource)
                    hubIconInactive = resource
                    return true
                }
            })
            .into(ivHubButtonImage)

        Glide.with(this)
            .asDrawable()
            .load(activeImageUrl)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean = false

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    hubIconActive = resource
                    return true
                }
            })
            .submit()
    }

    override fun openFeedback(feedbackViewModel: FeedbackViewModel) {
        val showFeedback = {
            if (!supportFragmentManager.isStateSaved && !isFinishing) {
                val feedbackDialog = FeedbackDialog.newInstance(feedbackViewModel)
                feedbackDialog.isCancelable = false
                feedbackDialog.show(supportFragmentManager, null)
            }
        }
        Handler().postDelayed(showFeedback, 5000)
    }

    override fun openLiveCreator(
        pk: Int,
        rtmpUrl: String,
        commentQueue: String,
        startedAt: String?
    ) {
        val intent = Intent(this, LiveCreatorActivity::class.java)
        intent.putExtra(LiveCreatorActivity.EXTRA_PK, pk)
        intent.putExtra(LiveCreatorActivity.EXTRA_RTMP_URL, rtmpUrl)
        intent.putExtra(LiveCreatorActivity.EXTRA_COMMENT_QUEUE, commentQueue)
        intent.putExtra(LiveCreatorActivity.EXTRA_STARTED_AT, startedAt)
        startActivity(intent)
    }

    override fun openSharedActivity(shareModel: String, sharePK: Int) {
        val nextIntent = when (shareModel) {
            Share.Feed.name -> Intent(this, PostDetailsActivity::class.java).apply {
                putExtra(
                    PostDetailsActivity.POST_DETAIL_PK,
                    sharePK
                )
            }
            Share.ScheduledLive.name -> Intent(
                this,
                ScheduledLiveDetailsActivity::class.java
            ).apply { putExtra(ScheduledLiveDetailsActivity.SCHEDULED_LIVE_PK, sharePK) }
            Share.Live.name -> Intent(this, LiveViewerActivity::class.java).apply {
                putExtra(
                    LiveViewerActivity.PK,
                    sharePK
                )
            }
            Share.Audio.name -> Intent(this, AudioDetailActivity::class.java).apply {
                putExtra(
                    AudioDetailActivity.EXTRA_AUDIO_PK,
                    sharePK
                )
            }
            Share.Immobile.name -> Intent(
                this,
                ImmobilesDetailsActivity::class.java
            ).apply { putExtra(ImmobilesDetailsActivity.extraImmobilePk, sharePK) }
            else -> return
        }
        // startActivity(nextIntent)
    }

    override fun onDestroy() {
        navigationPresenter.onDestroy()
        super.onDestroy()
    }

    override fun showSystemSelection() {
        startActivity(Intent(this, SystemSelectActivity::class.java))
    }

    override fun onUnreadNotificationCleared() {
        navigationPresenter.onNotificationSynced()
    }

    private fun unselectButtons() {
        ivSocialButtonImage.setImageResource(R.drawable.ic_navigation_home_inactive)
        ivExploreButtonImage.setImageResource(R.drawable.ic_navigation_explore_inactive)
        ivHubButtonImage.setImageDrawable(hubIconInactive)
        ivNotificationsButtonImage.setImageResource(R.drawable.ic_navigation_notifications_inactive)
        ivProfileButtonImage.setImageResource(R.drawable.ic_navigation_profile_inactive)
    }

    override fun socialTabSelected(v: View?) {
        setFragmentAction(NavigationItem.SOCIAL)
        unselectButtons()
        ivSocialButtonImage.setImageResource(R.drawable.ic_navigation_home_active)
    }

    override fun exploreTabSelected(v: View?) {
        setFragmentAction(NavigationItem.EXPLORE)
        unselectButtons()
        ivExploreButtonImage.setImageResource(R.drawable.ic_navigation_explore_active)
    }

    override fun hubTabSelected(v: View?) {
        setFragmentAction(NavigationItem.HUB)
        unselectButtons()
        ivHubButtonImage.setImageDrawable(hubIconActive)
    }

    override fun notificationsTabSelected(v: View?) {
        setFragmentAction(NavigationItem.NOTIFICATION)
        unselectButtons()
        ivNotificationsButtonImage.setImageResource(R.drawable.ic_navigation_notifications_active)
    }

    override fun profileTabSelected(v: View?) {
        setFragmentAction(NavigationItem.PROFILE)
        unselectButtons()
        ivProfileButtonImage.setImageResource(R.drawable.ic_navigation_profile_active)
    }

    override fun startChatService(chatId: String) {
        val intent = Intent(this, ChatService::class.java).apply {
            action = CHAT_SERVICE_ACTION_START
        }
        startService(intent)
    }
}
