package co.socialsquad.squad.presentation.feature.video.list

import dagger.Binds
import dagger.Module

@Module
abstract class VideoListModule {
    @Binds
    abstract fun view(videoListFragment: VideoListFragment): VideoListView
}
