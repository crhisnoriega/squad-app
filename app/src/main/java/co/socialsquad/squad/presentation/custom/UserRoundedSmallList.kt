package co.socialsquad.squad.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.crossFade
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_small_user_list_images.view.*

class UserRoundedSmallList(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private var userBadgesViews: List<ImageView>
    private var userBadgesBackground: List<View>

    init {
        View.inflate(context, R.layout.view_small_user_list_images, this) as ConstraintLayout
        userBadgesViews = listOf(imageView1, imageView2, imageView3)
        userBadgesBackground = listOf(container1, container2, container3)
    }

    fun setUserImages(imagesUrl: List<String>?) {
        if (imagesUrl.orEmpty().isEmpty()) {
            imagesContainer.visibility = View.GONE
        } else {
            imagesContainer.visibility = View.VISIBLE
        }
        userBadgesBackground.forEach {
            it.visibility = View.GONE
        }
        imagesUrl?.take(3)?.forEachIndexed { index, url ->
            userBadgesBackground[index].visibility = View.VISIBLE
            Glide.with(userBadgesViews[index])
                .load(url)
                .circleCrop()
                .crossFade()
                .into(userBadgesViews[index])
        }
    }

    fun setText(text: String) {
        tvDescription.text = text
    }

    fun recycle() {
        try {
            Glide.with(context).apply { userBadgesViews.forEach { clear(it) } }
        } catch (throwable: Throwable) {
        }
    }
}
