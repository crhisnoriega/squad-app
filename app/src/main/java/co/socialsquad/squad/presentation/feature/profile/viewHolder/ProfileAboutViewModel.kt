package co.socialsquad.squad.presentation.feature.profile.viewHolder

import co.socialsquad.squad.presentation.custom.ViewModel

class ProfileAboutViewModel(val userPk: Int, val title: String, val type: String, val image: String, val isClickable: Boolean) : ViewModel
