package co.socialsquad.squad.presentation.feature.store.product

import co.socialsquad.squad.presentation.feature.store.StoreInteractor
import co.socialsquad.squad.presentation.feature.store.StoreInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ProductModule {
    @Binds
    abstract fun view(productActivity: ProductActivity): ProductView

    @Binds
    abstract fun interactor(storeInteractorImpl: StoreInteractorImpl): StoreInteractor
}
