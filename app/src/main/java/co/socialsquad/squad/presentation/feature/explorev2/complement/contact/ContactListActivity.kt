package co.socialsquad.squad.presentation.feature.explorev2.complement.contact

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseActivity
import co.socialsquad.squad.domain.model.explore.Contact
import co.socialsquad.squad.domain.model.explore.ContactGroupItem
import co.socialsquad.squad.domain.model.explore.ContactItemType
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.resources.di.ResourceModule
import co.socialsquad.squad.presentation.util.extra
import kotlinx.android.synthetic.main.activity_contact_list.*
import org.koin.core.module.Module

private const val RESOURCES_CONTACT = "resources_CONTACT"

class ContactListActivity: BaseActivity() {

    companion object {
        fun start(context: Context, contact:Contact) {
            context.startActivity(
                Intent(context, ContactListActivity::class.java).apply {
                    putExtra(RESOURCES_CONTACT, contact)
                }
            )
        }
    }

    private val contact by extra<Contact>(RESOURCES_CONTACT)
    override val modules: List<Module> = listOf(ResourceModule.instance)
    override val contentView = R.layout.activity_contact_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ibBack.setOnClickListener {
            finish()
        }

        setupHeader()
        setupPricesList()
    }

    private fun setupHeader(){
        tvTitle.text = contact.title
    }

    private fun setupPricesList(){
        val factoryAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ContactDividerViewHolderModel -> ContactDividerViewHolder.VIEW_ITEM_POSITION_ID
                is ContactHeaderViewHolderModel -> ContactHeaderViewHolder.VIEW_ITEM_POSITION_ID
                is ContactSingleViewHolderModel -> ContactSingleViewHolder.ITEM_VIEW_MODEL_ID
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                ContactDividerViewHolder.VIEW_ITEM_POSITION_ID -> ContactDividerViewHolder(view)
                ContactHeaderViewHolder.VIEW_ITEM_POSITION_ID -> ContactHeaderViewHolder(view)
                ContactSingleViewHolder.ITEM_VIEW_MODEL_ID -> ContactSingleViewHolder(view, true) {
                    val dialog = ContactOptionsDialog.newInstance(it)
                    dialog.show(supportFragmentManager, "complement_email_phone_options")
                }
                else -> throw IllegalArgumentException()
            }
        })

        rvResources.adapter = factoryAdapter.apply {
            var itens = mutableListOf<ViewModel>()
            contact.items?.phone?.let {
                itens.add(ContactDividerViewHolderModel())
                itens.add(ContactHeaderViewHolderModel(it.title))
                itens.addAll(it.items.filter {contactItem->
                    contactItem.type == ContactItemType.Phone
                }.flatMap {contactItem->
                    listOf(
                        ContactSingleViewHolderModel(
                            Contact(
                                contactItem.title ?: "",
                                "",
                                contactItem.value,
                            null,
                            ContactGroupItem(null, null)
                            ))
                    )
                })
            }
            contact.items?.email?.let {
                itens.add(ContactDividerViewHolderModel())
                itens.add(ContactHeaderViewHolderModel(it.title))
                itens.addAll(it.items.filter {contactItem->
                    contactItem.type == ContactItemType.Email
                }.flatMap {contactItem->
                    listOf(
                        ContactSingleViewHolderModel(
                            Contact(
                                contactItem.title ?: "",
                                "",
                                null,
                                contactItem.value,
                                ContactGroupItem(null, null)
                            ))
                    )
                })
            }
            addItems(itens)
        }
    }
}
