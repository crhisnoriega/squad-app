package co.socialsquad.squad.presentation.feature.audio.record

import co.socialsquad.squad.presentation.feature.audio.AudioViewModel

interface AudioRecordView {
    fun setupPlayer(audioViewModel: AudioViewModel)
    fun showRecordButton()
    fun showPlayer()
    fun showStopButton()
    fun openAudioCreateActivity(audioUri: String)
    fun enableNextOption()
    fun disableNextOption()
}
