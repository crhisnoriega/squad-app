package co.socialsquad.squad.presentation.feature.event.detail

import dagger.Binds
import dagger.Module

@Module
abstract class EventDetailModule {
    @Binds
    abstract fun view(eventDetailActivity: EventDetailActivity): EventDetailView
}
