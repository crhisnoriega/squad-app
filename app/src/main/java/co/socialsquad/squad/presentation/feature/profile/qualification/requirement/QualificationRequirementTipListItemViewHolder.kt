package co.socialsquad.squad.presentation.feature.profile.qualification.requirement

import android.view.View
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import kotlinx.android.synthetic.main.view_qualification_requirement_tip_list_item.view.*
import javax.inject.Inject

class QualificationRequirementTipListItemViewHolder @Inject constructor(
    itemView: View,
    private val color: String?
) : ViewHolder<QualificationRequirementTipListItemViewModel>(itemView) {
    override fun bind(viewModel: QualificationRequirementTipListItemViewModel) {
        itemView.apply {
            tvTitle.text = viewModel.title

            tvDescription.text = viewModel.description

            tvNumber.text = viewModel.number.toString()
            color?.let { tvNumber.backgroundTintList = ColorUtils.stateListOf(it) }
        }
    }

    override fun recycle() {}
}
