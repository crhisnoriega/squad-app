package co.socialsquad.squad.presentation.feature.notification

import dagger.Binds
import dagger.Module

@Module
abstract class NotificationModule {
    @Binds
    abstract fun view(notificationFragment: NotificationFragment): NotificationView
}
