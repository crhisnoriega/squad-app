package co.socialsquad.squad.presentation.feature.widgetScheduler.validation.adapter

import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.util.inflate

class TimetableValidationViewHolder(
    val parent: ViewGroup,
    private val timetableValidationCheckedCallback: TimetableValidationCheckedCallback
) :
    RecyclerView.ViewHolder(parent.inflate(R.layout.item_timetable_validation)) {

    private val tvName: TextView = itemView.findViewById(R.id.short_title_lead)
    private val tvSubTitle: TextView = itemView.findViewById(R.id.short_description_lead)
    private val tvContactText: TextView = itemView.findViewById(R.id.tvContactText)
    private val cbConfirmation: CheckBox = itemView.findViewById(R.id.cbConfirmation)

    fun bind(termVM: TermVM) {
        tvName.text = termVM.term.title
        tvSubTitle.text = termVM.term.description
        tvContactText.text = termVM.term.item

        itemView.setOnClickListener(null)
        cbConfirmation.setOnCheckedChangeListener(null)

        cbConfirmation.isChecked = termVM.checked

        itemView.setOnClickListener {
            timetableValidationCheckedCallback.onChecked(!cbConfirmation.isChecked, termVM.term)
        }

        cbConfirmation.setOnCheckedChangeListener { _, checked ->
            timetableValidationCheckedCallback.onChecked(checked, termVM.term)
        }
    }
}