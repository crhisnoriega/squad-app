package co.socialsquad.squad.presentation.feature.search

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.feature.store.StoreViewModel
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.intentDial
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_search_stores_item.view.*

class SearchStoreViewHolder(itemView: View) : ViewHolder<StoreViewModel>(itemView) {
    private val glide = Glide.with(itemView)

    override fun bind(viewModel: StoreViewModel) {
        with(viewModel) {
            glide.load(image)
                .roundedCorners(itemView.context)
                .crossFade()
                .into(itemView.ivImage)

            itemView.short_title_lead.text = name

            itemView.tvAddress.text = address

            itemView.tvDistanceAndPhone.text = listOfNotNull(
                distance?.let { itemView.context.getString(R.string.distance, it) },
                phone
            ).joinToString(" • ")

            if (phone != null) {
                itemView.setOnClickListener { itemView.context.intentDial(phone) }
            } else {
                itemView.setOnClickListener(null)
            }
        }
    }

    override fun recycle() {
        glide.clear(itemView.ivImage)
    }
}
