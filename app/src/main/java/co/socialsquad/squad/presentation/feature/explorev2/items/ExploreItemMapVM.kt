package co.socialsquad.squad.presentation.feature.explorev2.items

import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ContentItem
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.ListItem

data class ExploreItemMapVM(val contentItem: ContentItem? = null, val listItem: ListItem? = null)