package co.socialsquad.squad.presentation.feature.searchv2

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.socialsquad.squad.presentation.feature.base.Resource
import co.socialsquad.squad.presentation.feature.explorev2.domain.ExploreRepository
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.SectionList
import co.socialsquad.squad.presentation.feature.searchv2.model.ExploreSearchV2SectionItems
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class SearchViewModel(
        private val exploreRepository: ExploreRepository
) : ViewModel() {

    @ExperimentalCoroutinesApi
    private val results = MutableStateFlow<Resource<SectionList>>(Resource.loading(null))

    @ExperimentalCoroutinesApi
    private val resultsBySection = MutableStateFlow<Resource<ExploreSearchV2SectionItems>>(Resource.loading(null))


    val myresul = MutableLiveData<Resource<ExploreSearchV2SectionItems>>()

    @ExperimentalCoroutinesApi
    fun fetchResutsBySection(searchTerm: String, sectionId: Int? = null, currentTab: Int) {
        viewModelScope.launch {
            results.value = Resource.loading(null)
            sectionId?.let {
                exploreRepository.searchBySection(sectionId, searchTerm)
                        .flowOn(Dispatchers.IO)
                        .collect { res ->
                            if (!res.list.items.isNullOrEmpty()) {
                                resultsBySection.value = Resource.success(res)
                            } else {
                                resultsBySection.value = Resource.notFound()
                                emptySectionId.value = Resource.emptyWithData(currentTab)
                                myresul.value = Resource.notFound()
                            }
                        }
            } ?: run {
                exploreRepository.search(searchTerm)
                        .flowOn(Dispatchers.IO)
                        .collect { res ->
                            if (!res.data.sections.isNullOrEmpty()) {
                                results.value = Resource.success(res.data)
                            } else {
                                results.value = Resource.notFound()
                            }
                        }
            }
        }
    }

    @ExperimentalCoroutinesApi
    private val emptySectionId = MutableStateFlow<Resource<Int>>(Resource.loading(null))


    @ExperimentalCoroutinesApi
    fun emptySection(): StateFlow<Resource<Int>> {
        return emptySectionId
    }


    @ExperimentalCoroutinesApi
    fun results(): StateFlow<Resource<SectionList>> {
        return results
    }


    @ExperimentalCoroutinesApi
    fun resultsBySection(): StateFlow<Resource<ExploreSearchV2SectionItems>> {
        return resultsBySection
    }
}
