package co.socialsquad.squad.presentation.feature.audio.create

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.InputAudience
import co.socialsquad.squad.presentation.custom.InputSchedule
import co.socialsquad.squad.presentation.custom.InputView
import co.socialsquad.squad.presentation.feature.audio.AudioViewModel
import co.socialsquad.squad.presentation.feature.schedule.CreateScheduleActivity
import co.socialsquad.squad.presentation.feature.schedule.CreateSchedulePresenter
import co.socialsquad.squad.presentation.feature.social.MetricsViewModel
import co.socialsquad.squad.presentation.feature.social.UserViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.PotentialReachViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.SocialCreateSegmentActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.members.SegmentGroupMembersActivity
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_METRICS
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.EXTRA_POTENTIAL_REACH
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.SegmentMetricsActivity
import co.socialsquad.squad.presentation.util.ViewUtils
import kotlinx.android.synthetic.main.activity_audio_create.*
import java.util.ArrayList
import java.util.Calendar
import java.util.TimeZone
import javax.inject.Inject

const val REQUEST_CODE_AUDIO_FILE = 71

private const val REQUEST_CODE_AUDIENCE = 5
private const val REQUEST_CODE_SCHEDULE = 6

class AudioCreateActivity : BaseDaggerActivity(), AudioCreateView, InputView.OnInputListener {

    companion object {
        const val EXTRA_AUDIO_GALLERY_FILE_URI = "EXTRA_AUDIO_GALLERY_FILE_URI"
        const val EXTRA_AUDIO_RECORDED_FILE_URI = "EXTRA_AUDIO_RECORDED_FILE_URI"
    }

    private var miSave: MenuItem? = null

    @Inject
    lateinit var presenter: AudioCreatePresenter

    private var segmentResultRequestCode: Int? = null
    private var segmentResultResultCode: Int? = null
    private var segmentResultData: Intent? = null

    private var metrics: MetricsViewModel? = null
    private var members: IntArray? = null
    private var downline = false

    private var date: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_create)
        setupToolbar()
        setupOnClickListeners()
        setupInputListener()

        ViewUtils.showDialog(this, getString(R.string.audio_create_warning_limited_audience), null)

        presenter.onCreate(intent)
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        title = getString(R.string.audio_create_title)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupOnClickListeners() {
        iAudience.setOnClickListener { openSegment() }
        iaAudience.setOnClickListener { openSegment() }

        iaAudience.listener = object : InputAudience.Listener {
            override fun onRemoveClicked() {
                setAudienceAsPublic()
            }
        }

        iSchedule.setOnClickListener { openSchedule() }
        isSchedule.setOnClickListener { openSchedule() }

        isSchedule.listener = object : InputSchedule.Listener {
            override fun onPlayClicked() {}

            override fun onRemoveClicked() {
                resetSchedule()
            }
        }
    }

    private fun setupInputListener() {
        setInputViews(listOf(itTitle, itDescription))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_audio_create, menu)
        miSave = menu.findItem(R.id.audio_create_mi_save)
        miSave?.setOnMenuItemClickListener {
            miSave?.isEnabled = false
            presenter.onSaveClicked(itTitle.text, itDescription.text, metrics, members, downline, date)
            true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED) return

        segmentResultRequestCode = requestCode
        segmentResultResultCode = resultCode
        segmentResultData = data

        when (requestCode to resultCode) {
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_PUBLIC -> {
                setAudienceAsPublic()
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_DOWNLINE -> {
                data?.apply {
                    metrics = null
                    members = null
                    downline = true
                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    potentialReach?.let { reach ->
                        val membersText =
                            if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                            else getString(R.string.social_create_segment_item_description_member)

                        iAudience.visibility = View.GONE
                        with(iaAudience) {
                            visibility = View.VISIBLE
                            isEditable = false
                            audience = getString(R.string.audience_downline)
                            audienceImage = R.drawable.ic_audience_detail_downline
                            detail1 = getString(R.string.audience_potential_reach)
                            detail2 = membersText
                            imageResId = R.drawable.ic_audience_detail_metrics_reach
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_METRIC -> {
                data?.apply {
                    downline = false
                    members = null
                    metrics = data.getSerializableExtra(EXTRA_METRICS) as? MetricsViewModel
                    val potentialReach = data.getSerializableExtra(EXTRA_POTENTIAL_REACH) as? PotentialReachViewModel
                    metrics?.let {
                        potentialReach?.let { reach ->
                            val membersText =
                                if (reach.reachedMembers > 1) getString(R.string.social_create_segment_item_description_members, reach.reachedMembers)
                                else getString(R.string.social_create_segment_item_description_member)

                            iAudience.visibility = View.GONE
                            with(iaAudience) {
                                visibility = View.VISIBLE
                                isEditable = true
                                audience = getString(R.string.audience_metrics)
                                audienceImage = R.drawable.ic_audience_detail_metrics
                                detail1 = getString(R.string.audience_potential_reach)
                                detail2 = membersText
                                this.potentialReach = reach.circles
                            }
                        }
                    }
                }
            }
            REQUEST_CODE_AUDIENCE to SocialCreateSegmentActivity.RESULT_CODE_MEMBERS -> {
                data?.apply {
                    metrics = null
                    downline = false
                    @Suppress("UNCHECKED_CAST")
                    val selectedMembers = (data.extras?.getSerializable(SegmentGroupMembersActivity.EXTRA_MEMBERS) as? ArrayList<UserViewModel>).orEmpty()
                    members = selectedMembers.map { it.pk }.toIntArray()

                    val membersText = when {
                        selectedMembers.size == 1 -> getString(R.string.segment_members_one_member_selected)
                        selectedMembers.size == 2 -> getString(R.string.segment_members_two_members_selected)
                        selectedMembers.size > 2 -> getString(
                            R.string.segment_members_n_members_selected,
                            selectedMembers.size - 1
                        )
                        else -> ""
                    }

                    val iconUrls = selectedMembers.map { it.imageUrl }

                    iAudience.visibility = View.GONE
                    with(iaAudience) {
                        visibility = View.VISIBLE
                        isEditable = true
                        audience = getString(R.string.audience_members)
                        audienceImage = R.drawable.ic_audience_detail_members
                        detail1 = "${selectedMembers[0].firstName} ${selectedMembers[0].lastName}"
                        detail2 = membersText
                        imageUrl = iconUrls[0]
                    }
                }
            }
            REQUEST_CODE_SCHEDULE to CreateSchedulePresenter.RESULT_CODE_SCHEDULE -> {
                data?.apply {
                    miSave?.title = getString(R.string.social_create_live_button_schedule)

                    date = data.extras?.getLong(CreateSchedulePresenter.EXTRA_DATE)

                    iSchedule.visibility = View.GONE
                    with(isSchedule) {
                        visibility = View.VISIBLE
                        isEditable = true
                        scheduledDay = DateFormat.getLongDateFormat(context).format(date)
                        val timeString = DateFormat.getTimeFormat(context).format(date)
                        scheduledTime = timeString + " " + Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.LONG)
                    }
                }
            }
        }
    }

    private fun setAudienceAsPublic() {
        iaAudience.visibility = View.GONE
        iAudience.visibility = View.VISIBLE

        segmentResultRequestCode = null
        segmentResultResultCode = null
        segmentResultData = null

        metrics = null
        members = null
        downline = false
    }

    private fun openSegment() {
        when {
            metrics != null -> {
                val intent = Intent(this, SegmentMetricsActivity::class.java).apply {
                    putExtra(EXTRA_METRICS, metrics)
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            members?.isNotEmpty() ?: false -> {
                val intent = Intent(this, SegmentGroupMembersActivity::class.java).apply {
                    putExtra(SegmentGroupMembersActivity.EXTRA_MEMBERS, members!!.toTypedArray())
                }
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
            else -> {
                val intent = Intent(this, SocialCreateSegmentActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE_AUDIENCE)
            }
        }
    }

    private fun openSchedule() {
        val intent = Intent(this, CreateScheduleActivity::class.java)
        date?.let { intent.putExtra(CreateSchedulePresenter.EXTRA_DATE, it) }
        startActivityForResult(intent, REQUEST_CODE_SCHEDULE)
    }

    private fun resetSchedule() {
        iSchedule.visibility = View.VISIBLE
        isSchedule.visibility = View.GONE
        miSave?.title = getString(R.string.audio_create_menu_save)
        date = null
    }

    override fun setSaveButtonEnabled(conditionsMet: Boolean) {
        miSave?.isEnabled = conditionsMet
    }

    override fun onInput() {
        presenter.onInput(itTitle.text)
    }

    override fun updateUploadStatus(percentage: Int) {
        runOnUiThread {
            if (percentage < 1) {
                progressBar.isIndeterminate = true
            } else {
                progressBar.isIndeterminate = false
                progressBar.progress = percentage
            }
        }
    }

    override fun showProgress() {
        tvProgressText.text = getString(R.string.audio_create_uploading)
        llUploading.visibility = View.VISIBLE
        progressBar.progress = 0
        progressBar.isIndeterminate = true
    }

    override fun hideProgress() {
        llUploading.visibility = View.GONE
        progressBar.progress = 0
        progressBar.isIndeterminate = true
    }

    override fun showError(stringId: Int) {
        ViewUtils.showDialog(this, getString(stringId), null)
        presenter.onInput(itTitle.text)
    }

    override fun showPreparingFile() {
        tvProgressText.text = getString(R.string.audio_create_preparing_file)
        llUploading.visibility = View.VISIBLE
        progressBar.progress = 0
        progressBar.isIndeterminate = true
    }

    override fun setupPlayer(audioViewModel: AudioViewModel?) {
        audioViewModel?.let { apAudio.init(it) }
    }

    override fun uploadSuccess() {
        ViewUtils.showDialog(
            this,
            getString(R.string.audio_create_upload_success),
            getString(R.string.dialog_ok),
            DialogInterface.OnDismissListener { dialog ->
                dialog?.dismiss()
                finish()
            }
        )
    }

    override fun showOpeningError(message: Int) {
        ViewUtils.showDialog(
            this,
            getString(message),
            getString(R.string.dialog_ok),
            DialogInterface.OnDismissListener { dialog ->
                dialog?.dismiss()
                finish()
            }
        )
    }

    override fun setupAudienceColor(squadColor: String?) {
        squadColor?.let { iaAudience.color = it }
    }
}
