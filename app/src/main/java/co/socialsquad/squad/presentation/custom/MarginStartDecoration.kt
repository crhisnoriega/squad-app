package co.socialsquad.squad.presentation.custom

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R

class MarginStartDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val margin = view.context.resources.getDimension(R.dimen.default_list_start_margin)
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                left = margin.toInt()
            }
        }
    }
}
