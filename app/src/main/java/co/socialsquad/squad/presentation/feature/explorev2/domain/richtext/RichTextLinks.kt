package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import co.socialsquad.squad.presentation.feature.explorev2.components.rich_text.adapters.ViewType
import kotlinx.android.parcel.Parcelize

@Parcelize
class RichTextLinks(var title: String? = null, var content: List<RichTextLinkDetails>? = null) : ViewType, Parcelable {
    constructor(richText: RichTextVO) : this() {
        this.title = richText.link?.title
        this.content = richText.link?.content
    }

    override fun getViewType(): Int {
        return RichTextDataType.LINK.value
    }
}
