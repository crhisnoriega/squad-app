package co.socialsquad.squad.presentation.feature.searchv2.results.adapter.viewHolder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.feature.explorev2.domain.model.EmptyState
import com.bumptech.glide.Glide

class SearchEmptyViewHolder(itemView: View, val context: Context) :
        RecyclerView.ViewHolder(itemView) {

    internal fun bind(emptyState: EmptyState?) {
        if (emptyState == null) {
            itemView.findViewById<TextView>(R.id.tvTitle)?.text =
                    context.getString(R.string.search_empty_state_title)
            itemView.findViewById<TextView>(R.id.tvDescription)?.text =
                    context.getString(R.string.search_empty_state_description)
            itemView.findViewById<ImageView>(R.id.ivIcon)
                    ?.setImageDrawable(context.getDrawable(R.drawable.ic_search_empty))
        } else {
            itemView.findViewById<TextView>(R.id.tvTitle)?.text = emptyState.title
            itemView.findViewById<TextView>(R.id.tvDescription)?.text = emptyState.subtitle
            if (!emptyState.icon?.url.isNullOrEmpty()) {
                itemView.findViewById<ImageView>(R.id.ivIcon)
                        ?.let { Glide.with(context).load(emptyState.icon?.url).into(it) }
            } else {
                itemView.findViewById<ImageView>(R.id.ivIcon)
                        ?.setImageDrawable(context.getDrawable(R.drawable.ic_search_empty))
            }
        }
    }
}
