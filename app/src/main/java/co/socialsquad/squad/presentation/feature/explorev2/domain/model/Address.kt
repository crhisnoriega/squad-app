package co.socialsquad.squad.presentation.feature.explorev2.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(
        @SerializedName("location") val location: String?,
        @SerializedName("coordinates") val coordinates: PinLocation?,
        @SerializedName("address_line1") val address_line1: String?,
        @SerializedName("address_line2") val address_line2: String?,
):Parcelable