package co.socialsquad.squad.presentation.feature.widgetScheduler.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.getColor
import androidx.core.graphics.drawable.DrawableCompat
import co.socialsquad.squad.R
import co.socialsquad.squad.domain.model.CompanyColor
import co.socialsquad.squad.presentation.feature.widgetScheduler.domain.ScaleTimeVO
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.getDrawableCompat
import co.socialsquad.squad.presentation.util.setTextColorFromRes
import kotlinx.android.synthetic.main.widget_scheduler_week_day.view.*
import java.util.Calendar
import java.util.Locale

class WidgetScheduleWeekView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    init {
        inflateView()
    }

    var onClick: (scaleTime: ScaleTimeVO) -> Unit = {}

    private fun inflateView() = View.inflate(context, R.layout.widget_scheduler_week_day, this)

    fun bind(scaleTimes: List<ScaleTimeVO>, companyColor: CompanyColor) {
        scaleTimes.forEachIndexed { index, it ->
            setTime(it, index, companyColor)
        }
    }

    private fun setTime(
        scaleTime: ScaleTimeVO,
        index: Int,
        companyColor: CompanyColor
    ) {
        val scaleCalendar = Calendar.getInstance()
        scaleCalendar.time = scaleTime.date
        val dayOfWeek = scaleCalendar.getDisplayName(
            Calendar.DAY_OF_WEEK,
            Calendar.LONG,
            Locale.getDefault()
        )
        val dayOfMonth = scaleCalendar.get(Calendar.DAY_OF_MONTH)

        when (index) {
            0 -> {
                setDay(tvDayInitial1, dayOfWeek, tvDay1, dayOfMonth)
                setState(tvDay1, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay1, companyColor, scaleTime.isActualDay)
                    performclick(tvDay1, scaleTime)
                }
            }
            1 -> {
                setDay(tvDayInitial2, dayOfWeek, tvDay2, dayOfMonth)
                setState(tvDay2, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay2, companyColor, scaleTime.isActualDay)
                    performclick(tvDay2, scaleTime)
                }
            }
            2 -> {
                setDay(tvDayInitial3, dayOfWeek, tvDay3, dayOfMonth)
                setState(tvDay3, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay3, companyColor, scaleTime.isActualDay)
                    performclick(tvDay3, scaleTime)
                }
            }
            3 -> {
                setDay(tvDayInitial4, dayOfWeek, tvDay4, dayOfMonth)
                setState(tvDay4, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay4, companyColor, scaleTime.isActualDay)
                    performclick(tvDay4, scaleTime)
                }
            }
            4 -> {
                setDay(tvDayInitial5, dayOfWeek, tvDay5, dayOfMonth)
                setState(tvDay5, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay5, companyColor, scaleTime.isActualDay)
                    performclick(tvDay5, scaleTime)
                }
            }
            5 -> {
                setDay(tvDayInitial6, dayOfWeek, tvDay6, dayOfMonth)
                setState(tvDay6, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay6, companyColor, scaleTime.isActualDay)
                    performclick(tvDay6, scaleTime)
                }
            }
            6 -> {
                setDay(tvDayInitial7, dayOfWeek, tvDay7, dayOfMonth)
                setState(tvDay7, scaleTime, companyColor)
                if (scaleTime.isSelected) {
                    setSelected(tvDay7, companyColor, scaleTime.isActualDay)
                    performclick(tvDay7, scaleTime)
                }
            }
        }
    }

    private fun setState(textView: TextView, scaleTime: ScaleTimeVO, companyColor: CompanyColor) {
        textView.setOnClickListener(null)
        if (scaleTime.isEnabled) {
            setEnabled(textView, scaleTime, companyColor)
            textView.setOnClickListener {
                onClick(scaleTime)
            }
        } else {
            setDisabled(textView)
        }
    }

    private fun setDay(tvInitials: TextView, dayOfWeek: String, tvDay: TextView, dayOfMonth: Int) {
        tvInitials.text = dayOfWeek[0].toUpperCase().toString()
        tvDay.text = dayOfMonth.toString()
    }

    private fun getBackgroundForDay(color: Int): Drawable {
        val drawable =
            context.getDrawableCompat(R.drawable.bg_widget_scheduler_day) as LayerDrawable
        val bgDrawable = drawable.findDrawableByLayerId(R.id.backgroundMonthDay)
            .mutate() as Drawable
        val wrappedDrawable = DrawableCompat.wrap(bgDrawable)
        DrawableCompat.setTint(wrappedDrawable, color)
        return wrappedDrawable
    }

    private fun setDisabled(textView: TextView) {
        textView.background = getBackgroundForDay(
            getColor(
                context, R.color
                    .scheduler_date_day_bg_white
            )
        )
        textView.setTextColorFromRes(textView.context, R.color.scheduler_date_day_text_disabled)
    }

    private fun setEnabled(textView: TextView, scaleTime: ScaleTimeVO, companyColor: CompanyColor) {
        textView.background = getBackgroundForDay(
            getColor(
                context, R.color
                    .scheduler_date_day_bg_white
            )
        )
        if(scaleTime.isActualDay){
            textView.setTextColor(companyColor.primaryColor.let { ColorUtils.parse(it) })
        }else {
            textView.setTextColorFromRes(textView.context, R.color.scheduler_date_day_text_enabled)
        }
    }

    private fun setSelected(textView: TextView, companyColor: CompanyColor, isActualDay:Boolean) {
        if(isActualDay) {
            textView.background =
                getBackgroundForDay(companyColor.primaryColor.let { ColorUtils.parse(it) })
        }else{
            textView.background = getBackgroundForDay(
                getColor(
                    context, R.color
                        .scheduler_date_day_text_enabled
                )
            )
        }
        textView.setTextColorFromRes(textView.context, R.color.scheduler_date_day_text_active)
    }

    private fun performclick(textView: TextView, scaleTime: ScaleTimeVO) {
        if (!scaleTime.firstClickPerformed) {
            textView.performClick()
        }
    }
}
