package co.socialsquad.squad.presentation.feature.explorev2.domain.richtext

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PinLocationVO(
    var lat: Float? = null,
    var lng: Float? = null,
    var pin: String? = null
) : Parcelable
