package co.socialsquad.squad.presentation.feature.live.creator

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import co.socialsquad.squad.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.linearlayout_live_creator_options_dialog.*

class LiveCreatorOptionsDialog(context: Context, private val listener: Listener) : BottomSheetDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.linearlayout_live_creator_options_dialog)

        window?.apply {
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            setDimAmount(0f)
        }

        setCancelable(false)

        live_creator_options_dialog_bsb_post.setOnClickListener { listener.onPostClicked() }
        live_creator_options_dialog_bsb_delete.setOnClickListener { listener.onDeleteClicked() }
    }

    interface Listener {
        fun onPostClicked()
        fun onDeleteClicked()
    }
}
