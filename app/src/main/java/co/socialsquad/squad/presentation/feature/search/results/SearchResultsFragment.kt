package co.socialsquad.squad.presentation.feature.search.results

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.EndlessScrollListener
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.document.DocumentViewModel
import co.socialsquad.squad.presentation.feature.search.SearchActivity
import co.socialsquad.squad.presentation.feature.search.SearchAudioViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchDocumentViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchEmptyStateViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchEmptyStateViewModel
import co.socialsquad.squad.presentation.feature.search.SearchEventViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchImmobileViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchItem
import co.socialsquad.squad.presentation.feature.search.SearchLiveViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchNoResultsViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchNoResultsViewModel
import co.socialsquad.squad.presentation.feature.search.SearchSeeAllViewModel
import co.socialsquad.squad.presentation.feature.search.SearchStoreViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchTripViewHolder
import co.socialsquad.squad.presentation.feature.search.SearchVideoViewHolder
import co.socialsquad.squad.presentation.feature.store.AudiosSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.DocumentsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.EventsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.ImmobilesSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.LivesSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.ProductSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.StoresSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.TripsSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.VideosSearchItemViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.FooterViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.LoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.ProductViewHolder
import co.socialsquad.squad.presentation.util.createLocationRequest
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_search_results.*
import javax.inject.Inject

class SearchResultsFragment : Fragment(), SearchResultsView, SearchActivity.Listener {

    companion object {
        const val EXTRA_TYPE = "EXTRA_TYPE"
    }

    interface Listener {
        fun onSeeAllClicked(searchItem: SearchItem)
    }

    @Inject
    lateinit var presenter: SearchResultsPresenter

    private var adapter: RecyclerViewAdapter? = null
    var listener: Listener? = null

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_search_results, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        setupList()
        setupSwipeRefresh()
        presenter.onViewCreated(arguments)
    }

    override fun onQueryChanged(query: String) {
        if (query.isBlank()) presenter.onQueryCleared()
        else presenter.onQueryChanged(query)
    }

    override fun setupList(secondayColor: String) {
        adapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is ProductSearchItemViewModel -> R.layout.view_search_products_item
                is LivesSearchItemViewModel -> R.layout.view_search_lives_item
                is EventsSearchItemViewModel -> R.layout.view_search_events_item
                is VideosSearchItemViewModel -> R.layout.view_search_videos_item
                is AudiosSearchItemViewModel -> R.layout.view_search_audios_item
                is StoresSearchItemViewModel -> R.layout.view_search_stores_item
                is ImmobilesSearchItemViewModel -> R.layout.view_immobile_list_item
                is DocumentsSearchItemViewModel -> R.layout.view_search_documents_item
                is TripsSearchItemViewModel -> R.layout.view_search_trips_item
                is SearchEmptyStateViewModel -> R.layout.view_search_empty_state
                is SearchNoResultsViewModel -> R.layout.view_search_no_results
                is SearchSeeAllViewModel -> R.layout.view_list_footer
                is HeaderViewModel -> R.layout.view_list_header
                is LoadingViewModel -> R.layout.view_search_loading
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> = when (viewType) {
                R.layout.view_search_products_item -> ProductViewHolder(view)
                R.layout.view_search_lives_item -> SearchLiveViewHolder(view)
                R.layout.view_search_events_item -> SearchEventViewHolder(view)
                R.layout.view_search_videos_item -> SearchVideoViewHolder(view)
                R.layout.view_immobile_list_item -> SearchImmobileViewHolder(view, secondayColor)
                R.layout.view_search_audios_item -> SearchAudioViewHolder(view)
                R.layout.view_search_stores_item -> SearchStoreViewHolder(view)
                R.layout.view_search_documents_item -> SearchDocumentViewHolder(view, onDocumentClickedListener)
                R.layout.view_search_trips_item -> SearchTripViewHolder(view)
                R.layout.view_search_empty_state -> SearchEmptyStateViewHolder(view)
                R.layout.view_search_no_results -> SearchNoResultsViewHolder(view)
                R.layout.view_list_footer -> FooterViewHolder(view, onFooterClicked)
                R.layout.view_list_header -> HeaderViewHolder(view)
                R.layout.view_search_loading -> LoadingViewHolder(view)
                else -> throw IllegalArgumentException()
            }
        })
        with(rv) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@SearchResultsFragment.adapter?.apply { setItems(listOf(SearchEmptyStateViewModel)) }
            addOnScrollListener(
                EndlessScrollListener(
                    3,
                    rv.layoutManager as LinearLayoutManager,
                    presenter::onScrolledBeyondVisibleThreshold
                )
            )
            setItemViewCacheSize(0)
            setHasFixedSize(true)
        }
    }

    val onFooterClicked = object : ViewHolder.Listener<SearchSeeAllViewModel> {
        override fun onClick(viewModel: SearchSeeAllViewModel) {
            listener?.onSeeAllClicked(viewModel.searchItem)
        }
    }

    private val onDocumentClickedListener = object : ViewHolder.Listener<DocumentViewModel> {
        override fun onClick(viewModel: DocumentViewModel) {
            presenter.onDocumentClicked(viewModel)
        }
    }

    override fun showEmptyState() {
        adapter?.setItems(listOf(SearchEmptyStateViewModel))
    }

    override fun showNoResults(query: String) {
        adapter?.setItems(listOf(SearchNoResultsViewModel(query)))
    }

    private fun setupSwipeRefresh() {
        srl.isEnabled = false
        srl.setOnRefreshListener {
            presenter.onRefresh()
        }
    }

    override fun startLoading(search: String) {
        adapter?.startLoading(search)
    }

    override fun stopLoading() {
        adapter?.stopLoading()
    }

    override fun enableRefreshing() {
        srl.isEnabled = true
    }

    override fun disableRefreshing() {
        srl.isRefreshing = false
        srl.isEnabled = false
    }

    override fun addItems(items: List<ViewModel>) {
        adapter?.addItems(items)
    }

    override fun setItems(items: List<ViewModel>) {
        adapter?.setItems(items)
    }

    override fun clearList() {
        adapter?.setItems(listOf())
    }

    override fun createLocationRequest() {
        activity?.createLocationRequest { request, intent ->
            presenter.onLocationRequestCreated(request, intent)
        }
    }
}
