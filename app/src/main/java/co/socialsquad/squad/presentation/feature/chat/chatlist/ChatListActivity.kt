package co.socialsquad.squad.presentation.feature.chat.chatlist

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import co.socialsquad.squad.R
import co.socialsquad.squad.base.BaseDaggerActivity
import co.socialsquad.squad.presentation.custom.RecyclerViewAdapter
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.custom.ViewHolderFactory
import co.socialsquad.squad.presentation.custom.ViewModel
import co.socialsquad.squad.presentation.feature.chat.chatroom.CHAT_ROOM_ITEM_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomActivity
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomViewHolder
import co.socialsquad.squad.presentation.feature.chat.chatroom.ChatRoomViewModel
import co.socialsquad.squad.presentation.feature.chat.chatroom.EXTRA_CHAT_ROOM
import co.socialsquad.squad.presentation.feature.social.DIVIDER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.social.DividerViewModel
import co.socialsquad.squad.presentation.feature.social.create.segment.metrics.DividerViewHolder
import co.socialsquad.squad.presentation.feature.store.HEADER_VIEW_MODEL_ID
import co.socialsquad.squad.presentation.feature.store.HeaderViewModel
import co.socialsquad.squad.presentation.feature.store.LoadingViewModel
import co.socialsquad.squad.presentation.feature.store.viewholder.GenericLoadingViewHolder
import co.socialsquad.squad.presentation.feature.store.viewholder.HeaderViewHolder
import co.socialsquad.squad.presentation.util.hideKeyboard
import kotlinx.android.synthetic.main.activity_chat_list.*
import kotlinx.android.synthetic.main.view_chat_toolbar.*
import kotlinx.android.synthetic.main.view_search_toolbar_input.*
import javax.inject.Inject

const val REQUEST_CODE_CHAT_ROOM = 1

class ChatListActivity : BaseDaggerActivity(), ChatListView {
    @Inject
    lateinit var presenter: ChatListPresenter

    private var chatListAdapter: RecyclerViewAdapter? = null
    private val linearLayoutManager = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        setupToolbar()
        setupChatList()
        presenter.onCreate(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupToolbar() {
        title = getString(R.string.chat_list_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        etSearch.apply {
            setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    bCancel.visibility = View.VISIBLE
                    ibClear.visibility = View.VISIBLE
                    showSearchEmpty()
                }
            }
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    presenter.onQueryChanged(s.toString())
                }
            })
        }

        ibClear.setOnClickListener { etSearch.setText("") }

        bCancel.setOnClickListener {
            etSearch.setText("")
            etSearch.hideKeyboard()
            bCancel.visibility = View.GONE
            ibClear.visibility = View.GONE
            rvContacts.requestFocus()
            presenter.onCancelClicked()
        }
    }

    override fun showNoResults(query: String) {
        llSearch.visibility = View.VISIBLE
        ivSearchIcon.setImageResource(R.drawable.ic_search_no_results)
        tvSearchTitle.text = getString(R.string.chat_no_results_title)
        tvSearchDescription.text = getString(R.string.chat_no_results_description, query)
    }

    override fun showSearchEmpty() {
        llSearch.visibility = View.VISIBLE
        ivSearchIcon.setImageResource(R.drawable.ic_search_empty)
        tvSearchTitle.text = getString(R.string.chat_empty_state_title)
        tvSearchDescription.text = getString(R.string.chat_empty_state_description)
    }

    private val onRoomItemClickListener = object : ViewHolder.Listener<ChatRoomViewModel> {
        override fun onClick(viewModel: ChatRoomViewModel) {
            openChatRoom(viewModel.pk)
        }
    }

    private fun openChatRoom(roomPk: Int) {
        startActivityForResult(Intent(this, ChatRoomActivity::class.java).putExtra(EXTRA_CHAT_ROOM, roomPk), REQUEST_CODE_CHAT_ROOM)
    }

    private fun setupChatList() {
        chatListAdapter = RecyclerViewAdapter(object : ViewHolderFactory {
            override fun getType(viewModel: ViewModel) = when (viewModel) {
                is HeaderViewModel -> HEADER_VIEW_MODEL_ID
                is ChatRoomViewModel -> CHAT_ROOM_ITEM_VIEW_MODEL_ID
                is DividerViewModel -> DIVIDER_VIEW_MODEL_ID
                is LoadingViewModel -> R.layout.view_list_loading
                else -> throw IllegalArgumentException()
            }

            override fun getHolder(viewType: Int, view: View): ViewHolder<*> {
                return when (viewType) {
                    HEADER_VIEW_MODEL_ID -> HeaderViewHolder(view)
                    CHAT_ROOM_ITEM_VIEW_MODEL_ID -> ChatRoomViewHolder(view, onRoomItemClickListener)
                    DIVIDER_VIEW_MODEL_ID -> DividerViewHolder(view)
                    R.layout.view_list_loading -> GenericLoadingViewHolder<Nothing>(view)
                    else -> throw IllegalArgumentException()
                }
            }
        })

        rvContacts?.apply {
            adapter = chatListAdapter
            layoutManager = linearLayoutManager
        }
    }

    override fun setItems(items: List<ViewModel>) {
        llSearch.visibility = View.GONE
        chatListAdapter?.setItems(items)
    }

    override fun updateItem(section: ViewModel) {
        chatListAdapter?.update(section)
    }

    override fun showLoading() {
        llSearch.visibility = View.GONE
        chatListAdapter?.startLoading()
    }

    override fun hideLoading() {
        chatListAdapter?.stopLoading()
    }

    override fun hideSearch() {
        llSearch.visibility = View.GONE
    }
}
