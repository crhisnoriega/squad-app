package co.socialsquad.squad.presentation.feature.profile

import android.content.DialogInterface
import android.util.Log
import co.socialsquad.squad.R
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.SocialRepository
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileToCompleteViewModel
import co.socialsquad.squad.presentation.feature.profile.viewHolder.ProfileViewModel
import co.socialsquad.squad.presentation.feature.social.*
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProfilePresenter @Inject constructor(
    private val profileView: ProfileView,
    private val profileInteractor: ProfileInteractor,
    private val socialRepository: SocialRepository,
    private val loginRepository: LoginRepository
) {
    private var page = 2
    private var isLoading = false
    private var isComplete = false
    private val mapper = SocialViewModelMapper()
    private val color = loginRepository.squadColor
    private val subdomain = loginRepository.subdomain
    private val compositeDisposable = CompositeDisposable()

    private val featureQualification by lazy {
        loginRepository.userCompany?.company?.featureQualification == true
    }

    fun onViewCreated() {
        val isAudioEnabled = socialRepository.audioEnabled
        profileView.onAudioToggled(isAudioEnabled)
        page = 1
        getProfileAndPosts()
    }

    fun reloadInfo() {
        profileView.showProfile(ProfileViewModel(loginRepository.userCompany!!), false)
    }


    fun isCompleted() =
        ProfileToCompleteViewModel(loginRepository.userCompany?.complete_your_profile!!).isComplete()

    private fun getProfileAndPosts() {
        compositeDisposable.add(
            profileInteractor.getProfile()
                .flatMap {
                    val profileViewModel = it.apply { canCall = false }
                    profileView.showProfile(profileViewModel, featureQualification)
                    profileInteractor.getPosts(page)
                }
                .doOnSubscribe {
                    isLoading = true
                    profileView.showLoading()
                }
                .doOnTerminate {
                    isLoading = false
                    profileView.hideLoading()
                }
                .doOnNext { isComplete = it.next == null }
                .map {
                    Log.i("feeds", Gson().toJson(it))
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        profileView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        profileView.showMessage(R.string.profile_error_get_posts)
                    }
                )
        )
    }

    fun onDestroy() = compositeDisposable.dispose()

    fun onSettingsSelected() = profileView.openSettings()

    fun onAudioToggled(audioEnabled: Boolean) {
        socialRepository.audioEnabled = audioEnabled
    }

    fun onRefresh() {
        compositeDisposable.clear()
        compositeDisposable.add(
            profileInteractor.getProfile()
                .flatMap {
                    profileView.showProfile(it, featureQualification)
                    profileInteractor.getPosts(1)
                }
                .doOnSubscribe {
                    isLoading = true
                }
                .doOnTerminate {
                    isLoading = false
                    profileView.stopRefreshing()
                }
                .doOnNext { isComplete = it.next == null }
                .map {
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        profileView.showPosts(it)
                        page = 2
                    },
                    {
                        it.printStackTrace()
                        profileView.showMessage(R.string.profile_error_get_posts)
                    }
                )
        )
    }

    fun onScrolledBeyondVisibleThreshold() {
        if (isComplete || isLoading) return

        compositeDisposable.add(
            profileInteractor.getPosts(page)
                .doOnSubscribe {
                    isLoading = true
                    profileView.showLoadingMore()
                }
                .doOnTerminate {
                    isLoading = false
                }
                .doOnNext { isComplete = it.next == null }
                .map {
                    mapper.mapItems(
                        it,
                        socialRepository.audioEnabled,
                        loginRepository.userCompany?.user?.avatar,
                        color,
                        subdomain
                    )
                }
                .map { it.filter { post -> post !is LiveListViewModel } }
                .subscribe(
                    {
                        profileView.hideLoadingMore()
                        profileView.showPosts(it)
                        page++
                    },
                    {
                        it.printStackTrace()
                        profileView.showMessage(
                            R.string.profile_error_get_posts,
                            DialogInterface.OnDismissListener {
                                profileView.hideLoadingMore()
                            }
                        )
                    }
                )
        )
    }

    fun onEditSelected(postViewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.getSegmentation(postViewModel.pk)
                .doOnSubscribe { profileView.showLoading() }
                .doOnTerminate { profileView.hideLoading() }
                .subscribe(
                    {
                        postViewModel.audience = AudienceViewModel(postViewModel.audience.type, it)
                        when (postViewModel) {
                            is PostImageViewModel, is PostVideoViewModel -> profileView.showEditMedia(
                                postViewModel
                            )
                            is PostLiveViewModel -> profileView.showEditLive(postViewModel)
                        }
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        profileView.showMessage(
                            R.string.social_error_failed_to_edit_post,
                            DialogInterface.OnDismissListener {
                                profileView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun onDeleteClicked(viewModel: PostViewModel) {
        compositeDisposable.clear()
        compositeDisposable.add(
            socialRepository.deleteFeed(viewModel.pk)
                .doOnSubscribe { profileView.showLoading() }
                .doOnTerminate {
                    profileView.hideLoading()
                    profileView.hideDeleteDialog()
                }.subscribe(
                    {
                        profileView.removePost(viewModel)
                    },
                    { throwable ->
                        throwable.printStackTrace()
                        profileView.showMessage(
                            R.string.social_error_failed_to_delete_post,
                            DialogInterface.OnDismissListener {
                                profileView.hideLoading()
                            }
                        )
                    }
                )
        )
    }

    fun likePost(post: PostViewModel) {
        compositeDisposable.add(
            socialRepository.likePost(post.pk).subscribe(
                {
                    post.liked = it.liked
                    post.likeAvatars = it.likedUsersAvatars
                    post.likeCount = it.likesCount
                    profileView.updatePost(post)
                },
                {
                    it.printStackTrace()
                    post.liked = !post.liked
                    profileView.updatePost(post)
                },
                {}
            )
        )
    }
}
