package co.socialsquad.squad.presentation.feature.navigation

import co.socialsquad.squad.presentation.feature.explore.ExploreFragment
import co.socialsquad.squad.presentation.feature.explore.ExploreModule
import co.socialsquad.squad.presentation.feature.hub.HubFragment
import co.socialsquad.squad.presentation.feature.hub.HubModule
import co.socialsquad.squad.presentation.feature.notification.NotificationFragment
import co.socialsquad.squad.presentation.feature.notification.NotificationModule
import co.socialsquad.squad.presentation.feature.profile.ProfileFragment
import co.socialsquad.squad.presentation.feature.profile.ProfileModule
import co.socialsquad.squad.presentation.feature.social.SocialFragment
import co.socialsquad.squad.presentation.feature.social.SocialModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NavigationModule {

    @Binds
    abstract fun view(navigationActivity: NavigationActivity): NavigationView

    @ContributesAndroidInjector(modules = [SocialModule::class])
    abstract fun socialFragment(): SocialFragment

    @ContributesAndroidInjector(modules = [ExploreModule::class])
    abstract fun exploreFragment(): ExploreFragment

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun profileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = [NotificationModule::class])
    abstract fun notificationFragment(): NotificationFragment

    @ContributesAndroidInjector(modules = [HubModule::class])
    abstract fun hubFragment(): HubFragment
}
