package co.socialsquad.squad.presentation.feature.profile.qualification.viewHolder

import android.view.View
import co.socialsquad.squad.R
import co.socialsquad.squad.presentation.custom.ViewHolder
import co.socialsquad.squad.presentation.util.ColorUtils
import co.socialsquad.squad.presentation.util.crossFade
import co.socialsquad.squad.presentation.util.roundedCorners
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_qualification_prize_item.view.*

const val QUALIFICATION_PRIZE_VIEW_MODEL_ID = R.layout.view_qualification_prize_item

class QualificationPrizeViewHolder(itemView: View) : ViewHolder<PrizeViewModel>(itemView) {

    override fun bind(viewModel: PrizeViewModel) {
        if (adapterPosition != 0) {
            itemView.vlbiDivider.visibility = View.VISIBLE
        }
        with(itemView) {
            Glide.with(this)
                .load(viewModel.photo)
                .roundedCorners(context)
                .crossFade()
                .into(ivPrizePhoto)
            tvPrizeCategory.text = viewModel.category
            tvPrizeCategory.setTextColor(ColorUtils.stateListOf(viewModel.tintColor))
            tvPrizeName.text = viewModel.name
            tvPrizeDescription.text = viewModel.description
        }
    }

    override fun recycle() {
        Glide.with(itemView.context)
            .clear(itemView.ivPrizePhoto)
        itemView.vlbiDivider.visibility = View.GONE
    }
}
