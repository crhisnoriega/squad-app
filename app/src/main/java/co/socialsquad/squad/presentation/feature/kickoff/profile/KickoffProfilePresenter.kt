package co.socialsquad.squad.presentation.feature.kickoff.profile

import android.net.Uri
import co.socialsquad.squad.data.entity.CompleteProfileRequest
import co.socialsquad.squad.data.entity.Phone
import co.socialsquad.squad.data.entity.UserCompany
import co.socialsquad.squad.data.repository.LoginRepository
import co.socialsquad.squad.data.repository.ProfileRepository
import co.socialsquad.squad.data.utils.TagWorker
import co.socialsquad.squad.presentation.util.MaskUtils
import co.socialsquad.squad.presentation.util.TextUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import javax.inject.Inject

class KickoffProfilePresenter @Inject constructor(
    private val kickoffProfileView: KickoffProfileView,
    private val profileRepository: ProfileRepository,
    private val loginRepository: LoginRepository,
    private val tagWorker: TagWorker
) {

    private val compositeDisposable = CompositeDisposable()

    fun onVisible() {
        loginRepository.userCompany?.let {
            kickoffProfileView.setupInfo(it.user)
        }
    }

    fun start() {
        loginRepository.userCompany?.user?.email?.apply {
            kickoffProfileView.email(this)
        }
    }

    fun onResourceReady(avatarUri: Uri) {
        profileRepository.avatar = avatarUri
    }

    fun onInput(firstName: String?, lastName: String?, email: String?, mobile: String?) {

        val isMobileValid = mobile?.let { it.length >= 14 } ?: false
        val conditionsMet = (
            !firstName.isNullOrBlank() &&
                !lastName.isNullOrBlank() &&
                TextUtils.isValidEmail(email) &&
                isMobileValid
            )
        kickoffProfileView.setSaveButtonEnabled(conditionsMet)
    }

    fun onSaveClicked(firstName: String, lastName: String, email: String, mobile: String) {
        val userCompany = loginRepository.userCompany
        val user = userCompany?.user
        user?.firstName = firstName
        user?.lastName = lastName
        user?.contactEmail = email

        val phones = ArrayList<Phone>()
        phones.add(Phone(MaskUtils.unmask(mobile, MaskUtils.Mask.MOBILE), "M"))
        user?.phones = phones
        loginRepository.userCompany = userCompany

        val phone = mutableListOf<CompleteProfileRequest.User.Phone>()
        phone.add(CompleteProfileRequest.User.Phone("M", mobile))
        val editUserObservable = profileRepository.completeProfile(
            loginRepository.userCompany!!.pk,
            CompleteProfileRequest(CompleteProfileRequest.User(firstName, lastName, phone, email), loginRepository.userCompany!!.pk)
        )

        val getUserCompanyConsumer = Consumer<UserCompany> {
            loginRepository.userCompany = it
            kickoffProfileView.showInviteMembrers()
            profileRepository.avatar = null
        }

        val throwableConsumer = Consumer<Throwable> {
            it.printStackTrace()
            kickoffProfileView.showErrorMessageFailedToRegisterUser()
        }

        val avatarUri = profileRepository.avatar

        if (avatarUri != null) {
            compositeDisposable.add(
                profileRepository.uploadAvatar(userCompany!!.user.pk, avatarUri)
                    .doOnSubscribe { kickoffProfileView.showLoading() }
                    .doOnTerminate { kickoffProfileView.hideLoading() }
                    .andThen(editUserObservable)
                    .subscribe(getUserCompanyConsumer, throwableConsumer)
            )
        } else {
            compositeDisposable.add(
                editUserObservable
                    .doOnSubscribe { kickoffProfileView.showLoading() }
                    .doOnTerminate { kickoffProfileView.hideLoading() }
                    .subscribe(getUserCompanyConsumer, throwableConsumer)
            )
        }
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}
