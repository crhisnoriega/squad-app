package co.socialsquad.squad

import co.socialsquad.squad.presentation.util.TextUtils
import org.junit.Test

class TextUtilsTest {

    @Test
    fun testUnitSufix() {
        val entry = listOf(0, 5, 999, 1_000, -5_820, 10_500, -101_800, 2_000_000, -7_800_000, 92_150_000, 123_200_000, 9_999_999, 999_999_999_999_999_999L, 1_230_000_000_000_000L, Long.MIN_VALUE, Long.MAX_VALUE)
        val expected = arrayOf("0", "5", "999", "1k", "-5.82k", "10.5k", "-101.8k", "2M", "-7.8M", "92.15M", "123.2M", "9.99M", "999.99P", "1.23P", "-9.22E", "9.22E")
        for (i in 0 until entry.size) {
            val n = entry[i]
            val formatted = TextUtils.toUnitSuffix(n)
            if (formatted != expected[i]) throw AssertionError("Expected: " + expected[i] + " but found: " + formatted)
        }
    }
}
