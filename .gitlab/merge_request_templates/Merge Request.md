- [ ] Revisão das alterações enviadas.
- [ ] Internacionalização.
- [ ] Revisado por um membro da equipe.
- [ ] Aprovado pelo PO.

*Comentários adicionais.*